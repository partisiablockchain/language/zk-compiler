#!/usr/bin/env bash

set -e

CARGO_PATH="target/cargo-test"
ZK_TEST_PATH="src/test/resources/com/partisiablockchain/language/zkcompiler/valid"

# shellcheck source=dependencies.txt
# shellcheck disable=SC1091
source <(awk -v ci="$CI" 'ci { gsub("ssh://git@","https://gitlab-ci-token:${CI_JOB_TOKEN}@"); } 1' dependencies.txt)

echo "Using SDK $CONTRACT_SDK"

rm -rf "${CARGO_PATH}"
mkdir -p "${CARGO_PATH}"
pushd "${CARGO_PATH}"
cargo init
cargo add pbc_zk --git "${CONTRACT_SDK%%#*}" --tag "${CONTRACT_SDK##*#}"
cargo add pbc_contract_common --git "${CONTRACT_SDK%%#*}" --tag "${CONTRACT_SDK##*#}"
cargo add pbc_traits --git "${CONTRACT_SDK%%#*}" --tag "${CONTRACT_SDK##*#}"
mkdir tests
popd

for zk_test_path in "$ZK_TEST_PATH/"*.rs; do
  if grep -q -F "//! assert eval tests" "${zk_test_path}"; then
    zk_test_name="$(basename "${zk_test_path}")"
    echo "Copying test ${zk_test_name}"
    mod_name=${zk_test_name//-/_}
    cp "${zk_test_path}" "${CARGO_PATH}/tests/${mod_name}"
  fi
done

pushd "${CARGO_PATH}"
cargo test
popd
