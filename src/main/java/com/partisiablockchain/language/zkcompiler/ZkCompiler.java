package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.irpass.IrPass;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassCallOperatorToTerminator;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassCommonInstructionElimination;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassConstantFolding;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassDeadCodeElimination;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassMergeLinearBlocks;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassMergeSecretBranches;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassPeepholes;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassRemoveUnusedBlocks;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassSelectReduction;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassShortCircuitProxyBlocks;
import com.partisiablockchain.language.zkcompiler.irpass.IrPassSplitBigConstants;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitNormalizer;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitSerializer;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitValidator;
import com.partisiablockchain.language.zkparser.RustZk;
import com.secata.stream.SafeDataOutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Main driving class of the ZkRust compiler, translating from a set of input files ({@link
 * ProgramInput}) to the {@link MetaCircuit} format. Will produce exceptions if the input program
 * does not satisfy the type requirements.
 *
 * @see #compileRustToMetaCircuit
 */
public final class ZkCompiler {

  private ZkCompiler() {}

  /** List of passes to apply during compilation. */
  public static final List<IrPass> IR_PASSES =
      List.<IrPass>of(
          new IrPassShortCircuitProxyBlocks(),
          new IrPassMergeSecretBranches(),
          new IrPassDeadCodeElimination(),
          new IrPassRemoveUnusedBlocks(),
          new IrPassMergeLinearBlocks(),
          new IrPassConstantFolding(),
          new IrPassSplitBigConstants(),
          new IrPassPeepholes(),
          new IrPassCommonInstructionElimination(),
          new IrPassPeepholes(),
          new IrPassSelectReduction(),
          new IrPassCallOperatorToTerminator(),
          new IrPassDeadCodeElimination());

  /**
   * Produces {@link MetaCircuit} bytecode from given Rust code.
   *
   * @param programInput Rust code to parse and compile
   * @param config Translation configuration, including optimizations and source name.
   * @return {@link MetaCircuit} bytecode. Never null
   */
  public static byte[] compileRustToMetaCircuitBytecode(
      final ProgramInput programInput, final TranslationConfig config) {
    final MetaCircuit metaCircuit = compileRustToMetaCircuit(programInput, config).metaCircuit();
    final MetaCircuit.Normalized normalizedMetaCircuit =
        MetaCircuitNormalizer.normalizeMetaCircuit(metaCircuit);
    return SafeDataOutputStream.serialize(
        stream ->
            MetaCircuitSerializer.serializeMetaCircuitToStream(stream, normalizedMetaCircuit));
  }

  /**
   * Produces {@link MetaCircuit} from given Rust code.
   *
   * @param programInput Rust code to parse and compile
   * @param config Translation configuration, including optimizations and source name.
   * @return {@link MetaCircuit}. Never null
   */
  public static Result compileRustToMetaCircuit(
      final ProgramInput programInput, final TranslationConfig config) {

    final ErrorPresenter.Colors colors =
        config.colorOutput() ? ErrorPresenter.Colors.ANSI : ErrorPresenter.Colors.NONE;

    // Parse
    final List<RustZk.ParseResult> parseResults =
        programInput.files().stream().map(ZkCompiler::parseToAst).toList();

    final List<TypeCheck.ProgramFile> typeCheckFileInputs = new ArrayList<>();
    for (int fileIdx = 0; fileIdx < parseResults.size(); fileIdx++) {
      final ProgramInput.FileInput fileInput = programInput.files().get(fileIdx);
      final RustZk.ParseResult parseResult = parseResults.get(fileIdx);

      if (!parseResult.syntaxErrors().isEmpty()) {
        final List<ErrorPresenter.ErrorLine> errorLines =
            syntaxErrorsToStandardErrors(fileInput.sourcePath(), parseResult.syntaxErrors());
        final String output = ErrorPresenter.presentToString(errorLines, programInput, colors);
        if (config.errorOutput() != null) {
          config.errorOutput().println(output);
        }
        throw new ParseException(errorLines);
      }

      typeCheckFileInputs.add(
          new TypeCheck.ProgramFile(parseResult.program(), fileInput.sourcePath()));
    }

    // Typecheck
    final TypeCheck.Result typeCheckResult =
        TypeCheck.typeCheckProgram(List.copyOf(typeCheckFileInputs), config);

    if (!typeCheckResult.errors().isEmpty()) {
      final String output =
          ErrorPresenter.presentToString(typeCheckResult.errors(), programInput, colors);
      if (config.errorOutput() != null) {
        config.errorOutput().println(output);
      }
      throw new TypeCheckException(typeCheckResult.errors());
    }

    config.hooks().debugTypedAst(typeCheckResult.program());

    // Translate to Ir
    AstToIrTranslator.Result irResult =
        AstToIrTranslator.translate(
            typeCheckResult.program(), typeCheckResult.testInformation(), config);
    Ir.Circuit irCircuit = irResult.circuit();
    config.hooks().debugIrInitial(irCircuit);

    // Perform Ir passes
    int passNumber = 1;
    for (final IrPass pass : IR_PASSES) {
      final Ir.Circuit irCircuitOfPreviousPass = irCircuit;
      irCircuit = pass.applyPass(irCircuit, config);
      // Detect changes applied by current pass.
      final int thisPassNumber = passNumber++;
      final PassInformation passInfo =
          new PassInformation(pass, thisPassNumber, thisPassNumber == IR_PASSES.size());
      config.hooks().debugIrPass(passInfo, irCircuit, !irCircuit.equals(irCircuitOfPreviousPass));
    }

    // Translate to MetaCircuit / Output Format
    final MetaCircuit metaCircuit = IrToMetaCircuitTranslator.translate(irCircuit, config);
    config.hooks().debugMetaCircuit(metaCircuit);

    // Return MetaCircuit
    return new Result(metaCircuit, irResult.testInformations());
  }

  private static List<ErrorPresenter.ErrorLine> syntaxErrorsToStandardErrors(
      final Path sourcePath, final List<RustZk.ErrorLine> syntaxErrors) {
    return syntaxErrors.stream()
        .map(e -> ErrorPresenter.ErrorLine.from(sourcePath, e.position(), e.message()))
        .toList();
  }

  /**
   * Produces Abstract syntax tree from given Rust code.
   *
   * @param fileInput Rust file to parse and compile.
   * @return Parse result for program. Never null
   */
  static RustZk.ParseResult parseToAst(final ProgramInput.FileInput fileInput) {
    return RustZk.parse(fileInput.rustSourceCode(), fileInput.sourcePath().toString());
  }

  /**
   * Result of a compilation, containing the compiled {@link MetaCircuit} and the test information
   * required to run the tests.
   *
   * @param metaCircuit the compiled {@link MetaCircuit}. Must be valid.
   * @param testInformation information about the tests of the {@link MetaCircuit}. Not nullable.
   */
  record Result(MetaCircuit metaCircuit, List<TestInformation> testInformation) {

    /** Constructor for {@link Result}. */
    public Result {
      // Check invariants
      MetaCircuitValidator.validateMetaCircuit(metaCircuit);

      // Perform null checks
      Objects.requireNonNull(testInformation);
    }
  }
}
