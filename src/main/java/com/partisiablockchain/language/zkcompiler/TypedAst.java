package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkparser.ZkAst;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/** Namespace class for Typed AST elements. */
public record TypedAst() {

  /**
   * Position with filepath. {@link TypedAst} elements may be mixed from several source files, hence
   * the need for the name of the original file.
   *
   * @param filepath Path to the file the AST element occured in.
   * @param position Position within the file the AST element occured.
   */
  public record PosInterval(Path filepath, ZkAst.PosInterval position)
      implements ZkAst.HasPosition {

    /**
     * Constructor for {@link PosInterval}.
     *
     * @param filepath Path to the file the AST element occured in.
     * @param position Position within the file the AST element occured.
     */
    public PosInterval {
      requireNonNull(position);
    }
  }

  /** Variable identifier used by the typed AST. */
  public record VariableId(EnvKind variableKind, NamePath.Absolute fullNamePath) {

    /**
     * Constructor.
     *
     * @param variableKind Variable kind of the variable, indicating which context the variable have
     *     been defined.
     * @param fullNamePath Raw identifier of the variable, full path.
     */
    public VariableId {
      requireNonNull(variableKind);
      requireNonNull(fullNamePath);
    }

    /**
     * Produces a new unique variable id with the given raw identifier.
     *
     * @param variableKind Variable kind of the variable.
     * @param fullNamePath Identifier path strings
     * @return Newly created unique variable identifier.
     */
    public static VariableId newFor(final EnvKind variableKind, final String... fullNamePath) {
      return new VariableId(variableKind, new NamePath.Absolute(fullNamePath));
    }

    /**
     * The last name element of the variable id's path.
     *
     * @return Last path element.
     */
    public String last() {
      return fullNamePath.last();
    }

    /**
     * To string method with optional capitalization.
     *
     * @param capitalizeFirst Whether to capitalize the first character of the output string.
     * @return String representation.
     */
    public String toString(boolean capitalizeFirst) {
      String kindName = variableKind.humanKindName();
      if (!capitalizeFirst) {
        kindName = kindName.toLowerCase(Locale.ROOT);
      }
      return "%s %s".formatted(kindName, fullNamePath);
    }

    @Override
    public String toString() {
      return toString(true);
    }
  }

  /** Variable kind, declaring which kind of AST element is binding the variable. */
  public enum EnvKind {
    /** Variables bound by let-statements. */
    VARIABLE_LET("Variable"),
    /** Variables bound within for-loop-statements. */
    VARIABLE_LOOP("Loop-variable"),
    /** Constant variables bound by constant items. */
    CONSTANT("Constant"),
    /** Parameter variables bound within the body of a function. */
    FUNCTION_PARAMETER("Function parameter"),
    /** Function variables bound by function definitions. */
    FUNCTION("Function"),
    /** Function variables bound by the set of builtin functions. */
    FUNCTION_BUILTIN("Builtin function"),
    /** Type created by user. */
    TYPE_USER_DEFINED("User-defined type"),
    /** Type in primitive environment. */
    TYPE_BUILTIN("Builtin type");

    /** Human visible name of the kind. */
    private final String humanKindName;

    EnvKind(String humanKindName) {
      this.humanKindName = humanKindName;
    }

    /**
     * Human visible name of the kind. Useful for various error messages.
     *
     * @return The name.
     */
    public String humanKindName() {
      return humanKindName;
    }
  }

  /** Type identifier used by the typed AST. */
  public record TypeId(NamePath.Absolute fullNamePath) {

    /**
     * Constructor.
     *
     * @param fullNamePath Raw identifier of the type, full path.
     */
    public TypeId {
      requireNonNull(fullNamePath);
    }

    /**
     * Produces a new unique type id with the given raw identifier.
     *
     * @param fullNamePath Identifier path strings
     * @return Newly created unique type identifier.
     */
    public static TypeId newFor(final String... fullNamePath) {
      return new TypeId(new NamePath.Absolute(fullNamePath));
    }

    @Override
    public String toString() {
      return fullNamePath.toString();
    }
  }

  /** Represents entire Zk program. */
  public record Program(
      List<Function> functionItems,
      List<StructDefinition> structItems,
      List<ConstantDefinition> constantItems) {

    /**
     * Constructor.
     *
     * @param functionItems Program functions definitions.
     * @param structItems Type definitions.
     * @param constantItems Constant definitions.
     */
    public Program {
      requireNonNull(functionItems);
      requireNonNull(structItems);
      requireNonNull(constantItems);
    }

    /**
     * Combines all programs in the given collection to a single program.
     *
     * @param subprograms Programs to combine.
     * @return Combined program.
     */
    public static Program combine(final Collection<Program> subprograms) {
      return new Program(
          subprograms.stream().map(Program::functionItems).flatMap(List::stream).toList(),
          subprograms.stream().map(Program::structItems).flatMap(List::stream).toList(),
          subprograms.stream().map(Program::constantItems).flatMap(List::stream).toList());
    }
  }

  /**
   * Struct definition item. Struct type is generic iff {@code !typeParameterNames.isEmpty()}. Field
   * types can contain references to {@code typeParameterNames}.
   */
  public record StructDefinition(
      TypeId typeId, List<String> typeParameterNames, List<StructField> fields) {

    /**
     * Constructor.
     *
     * @param fields Fields of struct.
     * @param typeParameterNames Names of type parameters.
     * @param typeId Id of type definition.
     */
    public StructDefinition {
      requireNonNull(typeId);
      requireNonNull(typeParameterNames);
      requireNonNull(fields);
    }

    /** Individual fields. */
    public record StructField(String fieldName, Type fieldTypeWithTypeParameters) {

      /**
       * Constructor.
       *
       * @param fieldName Name of field.
       * @param fieldTypeWithTypeParameters Type of field.
       */
      public StructField {
        requireNonNull(fieldName);
        requireNonNull(fieldTypeWithTypeParameters);
      }
    }
  }

  /** Represents function in Zk program. */
  public record Function(
      VariableId identifier,
      List<Parameter> parameters,
      Type returnType,
      TypedExpression body,
      ZkAst.ItemVisibility visibility,
      Integer explicitExportId,
      boolean isTest) {

    /**
     * Constructor for function definition.
     *
     * @param identifier Function identifier.
     * @param parameters Function parameters.
     * @param returnType Function return types.
     * @param body Function body.
     * @param visibility Function visibility.
     * @param explicitExportId Function's explicit ExportId. Null if not given.
     * @param isTest Whether the function is a test function
     */
    public Function {
      requireNonNull(identifier);
      requireNonNull(parameters);
      requireNonNull(returnType);
      requireNonNull(body);
      requireNonNull(visibility);
    }
  }

  /** Constant definition in ZK program. */
  public record ConstantDefinition(
      VariableId identifier, TypedExpression initExpr, PosInterval position) {

    /**
     * Constructor for constructor definition.
     *
     * @param identifier Bound constant definition identifier.
     * @param initExpr Initializer expression.
     * @param position Original position associated with the untyped AST element.
     */
    public ConstantDefinition {
      requireNonNull(identifier);
      requireNonNull(initExpr);
      requireNonNull(position);
    }
  }

  /** Represents parameter in Zk function. */
  public record Parameter(VariableId identifier, Type type) {

    /**
     * Constructor.
     *
     * @param identifier Variable id to assign parameter to.
     * @param type Type of parameter.
     */
    public Parameter {
      requireNonNull(identifier);
      requireNonNull(type);
    }
  }

  /** Represents various types in the program. */
  public sealed interface Type {

    /**
     * Checks whether this given type is an error type.
     *
     * @return {@code false} for all but {@link ErrorType}.
     */
    default boolean isError() {
      return false;
    }

    /** Nameable types which can have a type id taken. */
    sealed interface NameableType extends Type {
      /**
       * Determines the {@link TypeId} of the given type, such that it is possible to use it by
       * absolute path.
       *
       * @return Type id
       */
      TypeId typeId();
    }

    /** Tuple type containing multiple values. */
    public record Tuple(List<Type> subtypes) implements Type {
      /**
       * Constructor.
       *
       * @param subtypes Component types.
       */
      public Tuple {
        requireNonNull(subtypes);
      }

      @Override
      public String toString() {
        if (subtypes.size() == 1) {
          return "(%s,)".formatted(subtypes.get(0));
        }
        return subtypes.stream().map(Object::toString).collect(Collectors.joining(", ", "(", ")"));
      }

      /**
       * Unit type with a single value.
       *
       * <p>Identical to the empty tuple.
       */
      public static final Tuple UNIT = new Tuple(List.of());
    }

    /** Integer types with explicit bitwidth and signedness. */
    public record Int(int bitwidth, boolean isSigned) implements NameableType {

      @Override
      public String toString() {
        return typeId().toString();
      }

      @Override
      public TypeId typeId() {
        return TypedAst.TypeId.newFor("%s%d".formatted(isSigned ? "i" : "u", bitwidth));
      }

      /** Usize type. */
      public static final Int USIZE = new Int(32, false);

      /** 32-bit signed integer. */
      public static final Int I32 = new Int(32, true);

      /**
       * Boolean type.
       *
       * <p>Identical to u1.
       */
      public static final Int BOOL = new Int(1, false);
    }

    /** Error type. */
    public record ErrorType() implements Type {
      @Override
      public boolean isError() {
        return true;
      }

      @Override
      public String toString() {
        return "???";
      }
    }

    /**
     * Secret Binary Integer types with explicit bitwidth.
     *
     * @param bitwidth Bitwidth of the secret-shared integer.
     * @param isSigned Whether the secret-shared integer is signed or unsigned.
     */
    public record Sbi(int bitwidth, boolean isSigned) implements NameableType {
      @Override
      public String toString() {
        return typeId().toString();
      }

      @Override
      public TypeId typeId() {
        final String name = "Sb%s%d".formatted(isSigned ? "i" : "u", bitwidth);
        return TypedAst.TypeId.newFor("pbc_zk", name);
      }

      /**
       * Secret Boolean type.
       *
       * <p>Identical to u1.
       */
      public static final Sbi BOOL = new Sbi(1, false);
    }

    /** Named type. This is an instance of a generic type if {@code !typeArguments().isEmpty()}. */
    public record NamedType(TypeId typeId, List<Type> typeArguments) implements NameableType {

      /**
       * Constructor for named type.
       *
       * @param typeId Id to reference type by.
       * @param typeArguments Type arguments given to instantiate a generic type.
       */
      public NamedType {
        requireNonNull(typeId);
        requireNonNull(typeArguments);
      }

      /**
       * Create non-generic named type with the given {@code typeId}.
       *
       * @param typeId Id to reference type by.
       * @return Non-generic type.
       */
      public static Type simple(TypeId typeId) {
        return new NamedType(typeId, List.of());
      }

      @Override
      public String toString() {
        String s = typeId().toString();
        if (!typeArguments.isEmpty()) {
          final List<String> typeArgumentStrings =
              typeArguments.stream().map(Type::toString).toList();
          s += "<" + String.join(", ", typeArgumentStrings) + ">";
        }
        return s;
      }
    }

    /** Array type. */
    public record Array(Type elementType, int size) implements Type {

      /**
       * Constructor for {@link Array}.
       *
       * @param elementType Type of individual elements.
       * @param size Number of elements in array.
       */
      public Array {
        requireNonNull(elementType);
      }

      @Override
      public String toString() {
        return "[%s; %d]".formatted(elementType.toString(), size);
      }
    }
  }

  /** Expressions in Zk programs. */
  public record TypedExpression(
      Expression expr,
      Type resultType,
      boolean isConstantExpression,
      boolean containsGuessedType,
      PosInterval position) {
    /**
     * Constructor.
     *
     * @param expr Expression itself.
     * @param resultType Type that expression will produce.
     * @param isConstantExpression True if this expression is const in <a
     *     href="https://doc.rust-lang.org/reference/const_eval.html#constant-expressions">the way
     *     specified by the reference</a>. Notably that it can be used to define const variables and
     *     in other const contexts. Does not mean that expression will be inlined and optimized.
     * @param containsGuessedType True if the type of the expression was inferred by the compiler,
     *     and not explicitly annotated. Can be used to overwrite the type, if the inferred type
     *     doesn't match a later inference.
     * @param position Original position associated with the untyped AST element.
     */
    public TypedExpression {
      requireNonNull(expr);
      requireNonNull(resultType);
      requireNonNull(position);
    }
  }

  /** Expressions in Zk programs. */
  public sealed interface Expression {
    /** Block expression, containing statements. */
    public record Block(List<Statement> statements, TypedExpression resultExpr)
        implements Expression {

      /**
       * Constructor.
       *
       * @param statements Statements in block.
       * @param resultExpr Result expression for block.
       */
      public Block {
        requireNonNull(statements);
        requireNonNull(resultExpr);
      }
    }

    /** Unary operation on expression. */
    public record Unary(ZkAst.Unop operation, TypedExpression expr1) implements Expression {
      /**
       * Constructor.
       *
       * @param operation Operation performed.
       * @param expr1 Sub-expression to operation.
       */
      public Unary {
        requireNonNull(operation);
        requireNonNull(expr1);
      }
    }

    /** Binary operation on expressions. */
    public record Binary(ZkAst.Binop operation, TypedExpression expr1, TypedExpression expr2)
        implements Expression {
      /**
       * Constructor.
       *
       * @param operation Operation performed.
       * @param expr1 First sub-expression to operation.
       * @param expr2 Second sub-expression to operation.
       */
      public Binary {
        requireNonNull(operation);
        requireNonNull(expr1);
        requireNonNull(expr2);
      }
    }

    /** Local variable expression, fetching value from scope. */
    public record LocalVariable(VariableId variableIdentifier) implements Expression {
      /**
       * Constructor.
       *
       * @param variableIdentifier Identifier for variable.
       */
      public LocalVariable {
        requireNonNull(variableIdentifier);
      }
    }

    /** Local variable expression, fetching value from scope. */
    public record GlobalVariable(VariableId variableIdentifier) implements Expression {
      /**
       * Constructor.
       *
       * @param variableIdentifier Identifier for variable.
       */
      public GlobalVariable {
        requireNonNull(variableIdentifier);
      }
    }

    /** Int literal. */
    public record IntLiteral(BigInteger value) implements Expression {
      /**
       * Constructor.
       *
       * @param value Value of integer literal
       */
      public IntLiteral {
        requireNonNull(value);
      }

      /**
       * Constructor of Typed IntLiteral from int.
       *
       * @param value Value of int literal.
       */
      public IntLiteral(int value) {
        this(BigInteger.valueOf(value));
      }
    }

    /** Tuple constructor. */
    public record TupleConstructor(List<TypedExpression> exprs) implements Expression {
      /**
       * Constructor.
       *
       * @param exprs Component expressions.
       */
      public TupleConstructor {
        requireNonNull(exprs);
      }

      /** Unit constant. */
      public static final TupleConstructor UNIT = new TupleConstructor(List.of());
    }

    /** Tuple indexing . */
    public record TupleAccess(TypedExpression tupleExpr, int index) implements Expression {
      /**
       * Constructor.
       *
       * @param tupleExpr Sub-expression producing tuple.
       * @param index Index to index into.
       */
      public TupleAccess {
        requireNonNull(tupleExpr);
      }
    }

    /** Function call. */
    public record CallDirectly(VariableId functionId, List<TypedExpression> argumentExprs)
        implements Expression {
      /**
       * Constructor.
       *
       * @param functionId Id for call function.
       * @param argumentExprs Argument expressions to function.
       */
      public CallDirectly {
        requireNonNull(functionId);
        requireNonNull(argumentExprs);
      }
    }

    /** If-expression. */
    public record If(TypedExpression condExpr, TypedExpression thenExpr, TypedExpression elseExpr)
        implements Expression {
      /**
       * Constructor.
       *
       * @param condExpr Condition expression.
       * @param thenExpr Expression run if condition evaluates to true.
       * @param elseExpr Expression run if condition evaluates to false.
       */
      public If {
        requireNonNull(condExpr);
        requireNonNull(thenExpr);
        requireNonNull(elseExpr);
      }
    }

    /** For-expression. */
    public record ForInRange(
        VariableId iteratorVariableIdentifier,
        TypedExpression intoIteratorExpr,
        TypedExpression bodyExpr,
        TypedAst.VariableId methodIdIntoIterator,
        TypedAst.VariableId methodIdNext)
        implements Expression {
      /**
       * Constructor.
       *
       * @param iteratorVariableIdentifier Variable identifier iterated over.
       * @param intoIteratorExpr Expression producing value to iterate over.
       * @param bodyExpr Body of for-loop.
       * @param methodIdIntoIterator Method id for method turning {@code intoIteratorExpr} into an
       *     iterator.
       * @param methodIdNext Method id for updating iterator.
       */
      public ForInRange {
        requireNonNull(iteratorVariableIdentifier);
        requireNonNull(intoIteratorExpr);
        requireNonNull(bodyExpr);
        requireNonNull(methodIdIntoIterator);
        requireNonNull(methodIdNext);
      }
    }

    /** Assignment operator. */
    public record Assign(TypedExpression placeExpr, TypedExpression valueExpr)
        implements Expression {
      /**
       * Constructor.
       *
       * @param placeExpr Place expression to determine assignment location from.
       * @param valueExpr Expression to evaluate to determine assigned value.
       */
      public Assign {
        requireNonNull(placeExpr);
        requireNonNull(valueExpr);
      }
    }

    /** Type-cast operator. */
    public record TypeCast(TypedExpression expr1, Type resultType) implements Expression {
      /**
       * Constructor.
       *
       * @param expr1 Expression to evaluate to determine assigned value.
       * @param resultType Target type of cast.
       */
      public TypeCast {
        requireNonNull(expr1);
        requireNonNull(resultType);
      }
    }

    /** Struct construction expression. */
    public record StructConstructor(Type.NamedType structName, List<FieldInit> fieldInits)
        implements Expression {

      /**
       * Constructor.
       *
       * @param structName Id of the constructed type.
       * @param fieldInits Fields of struct.
       */
      public StructConstructor {
        requireNonNull(structName);
        requireNonNull(fieldInits);
      }

      /** Individual field initializer within a {@link StructConstructor}. */
      public record FieldInit(String fieldName, TypedExpression fieldInitExpr) {
        /**
         * Constructor.
         *
         * @param fieldName Name of field to initialize.
         * @param fieldInitExpr Expression to evaluate to determine initial value of field.
         */
        public FieldInit {
          requireNonNull(fieldName);
          requireNonNull(fieldInitExpr);
        }
      }
    }

    /** Struct access. */
    public record FieldAccess(TypedExpression structExpr, String fieldName) implements Expression {
      /**
       * Constructor.
       *
       * @param structExpr Expression producing value to index into.
       * @param fieldName Name of field to index into.
       */
      public FieldAccess {
        requireNonNull(structExpr);
        requireNonNull(fieldName);
      }
    }

    /** Constructor for an array with the same value repeated many times. */
    public record ArrayConstructorRepeat(
        TypedExpression repeatExpression, TypedExpression sizeExpression) implements Expression {

      /**
       * Constructor.
       *
       * @param repeatExpression Expression producing repeated value.
       * @param sizeExpression Expression producing size value.
       */
      public ArrayConstructorRepeat {
        requireNonNull(repeatExpression);
        requireNonNull(sizeExpression);
      }
    }

    /**
     * Explicit array constructor, where values are given directly, and the size of the array is
     * based on the number of values.
     */
    public record ArrayConstructorExplicit(List<TypedExpression> elementExpressions)
        implements Expression {

      /**
       * Constructor for {@link ArrayConstructorExplicit}.
       *
       * @param elementExpressions Individual element expressions.
       */
      public ArrayConstructorExplicit {
        requireNonNull(elementExpressions);
      }
    }

    /** Continue expression. */
    public record Continue() implements Expression {}

    /** Break expression. */
    public record Break() implements Expression {}
  }

  /** Statements in Zk programs. */
  public sealed interface Statement {

    /** Let statement, binding variable to it's initial value. */
    public record Let(VariableId identifier, TypedExpression initExpr) implements Statement {
      /**
       * Constructor.
       *
       * @param identifier Variable to assign to.
       * @param initExpr Expression to evaluate to determine assigned value.
       */
      public Let {
        requireNonNull(identifier);
        requireNonNull(initExpr);
      }
    }

    /** Return statement, exiting from current function, returning value of given expression. */
    public record Return(TypedExpression resultExpr, PosInterval position) implements Statement {
      /**
       * Constructor.
       *
       * @param resultExpr Expression to return from function.
       * @param position Original position associated with the untyped AST element.
       */
      public Return {
        requireNonNull(resultExpr);
      }
    }

    /** TypedExpression statement, wrapping expression, and discarding result. */
    public record Expr(TypedExpression expr) implements Statement {
      /**
       * Constructor.
       *
       * @param expr Expression to evaluate for side effects.
       */
      public Expr {
        requireNonNull(expr);
      }
    }
  }
}
