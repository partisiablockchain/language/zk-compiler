package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkparser.ZkAst;
import java.math.BigInteger;
import java.util.List;

/**
 * Test information for a specific macro test.
 *
 * @param result expected result of the test. Null when the test returns unit.
 * @param secretInputs secret inputs given to the test. Never null.
 * @param secretOutputs expected additional secret outputs of the test. Never null.
 * @param position position of the test in the source file. Never null.
 * @param identifier identifier of the test function. Never null.
 * @param functionId function id of the meta circuit function corresponding to the test. Null until
 *     it is determined during Ir generation. Never null afterwards.
 */
public record TestInformation(
    BigInteger result,
    List<BigInteger> secretInputs,
    List<BigInteger> secretOutputs,
    ZkAst.PosInterval position,
    TypedAst.VariableId identifier,
    FunctionId functionId) {

  /**
   * Constructor.
   *
   * @param result expected result of the test. Null when the test returns unit.
   * @param secretInputs secret inputs given to the test. Never null.
   * @param secretOutputs expected additional secret outputs of the test. Never null.
   * @param position position of the test in the source file. Never null.
   * @param identifier identifier of the test function. Never null.
   * @param functionId function id of the meta circuit function corresponding to the test. Null
   *     until it is determined during Ir generation. Never null afterwards.
   */
  public TestInformation {
    requireNonNull(secretInputs);
    requireNonNull(secretOutputs);
    requireNonNull(position);
    requireNonNull(identifier);
  }
}
