package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility for recursively replacing type parameters in generic types.
 *
 * <p>Example: For struct {@code struct A<B> { my_field: B }}, if we want to refer to access field
 * {@code my_field} from a variable with type {@code A<u32>}, we need to be able to replace {@code
 * B} with {@code u32}, which is what this component helps with.
 */
public record TypeReplacementMap(Map<TypedAst.TypeId, TypedAst.Type> internalMap) {

  /**
   * Creates map for replacing free types.
   *
   * @param typeParameterIds Type parameter names to replace.
   * @param typeArguments Types to bind pairwise to parameter names.
   * @return Map from parameter names to types.
   */
  public static TypeReplacementMap from(
      List<TypedAst.TypeId> typeParameterIds, List<? extends TypedAst.Type> typeArguments) {
    return new TypeReplacementMap(zipToMap(typeParameterIds, typeArguments));
  }

  /**
   * Replaces type parameters with type arguments in the given type.
   *
   * @param uncheckedType Type to replace in.
   * @return Type where type names have been replaced.
   */
  public TypedAst.Type replaceParametersInType(final TypedAst.Type uncheckedType) {

    // Named types
    if (uncheckedType instanceof TypedAst.Type.NamedType type) {
      final TypedAst.Type replacement = internalMap().get(type.typeId());
      if (replacement != null) {
        return replacement;
      }

      final List<TypedAst.Type> typeArguments =
          type.typeArguments().stream().map(this::replaceParametersInType).toList();

      return new TypedAst.Type.NamedType(type.typeId(), typeArguments);

      // Tuple types
    } else if (uncheckedType instanceof TypedAst.Type.Tuple type) {
      return new TypedAst.Type.Tuple(
          type.subtypes().stream().map(this::replaceParametersInType).toList());

      // Array types
    } else if (uncheckedType instanceof TypedAst.Type.Array type) {
      return new TypedAst.Type.Array(replaceParametersInType(type.elementType()), type.size());

      // Any other type is kept as is.
    } else {
      return uncheckedType;
    }
  }

  private static <K, V> Map<K, V> zipToMap(List<K> keys, List<? extends V> values) {
    final Map<K, V> map = new LinkedHashMap<>();
    for (int idx = 0; idx < values.size(); idx++) {
      map.put(keys.get(idx), values.get(idx));
    }
    return Map.copyOf(map);
  }
}
