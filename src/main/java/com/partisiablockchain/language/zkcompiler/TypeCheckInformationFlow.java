package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.TypeCheck.ErrorAccum;
import com.partisiablockchain.language.zkcompiler.math.Lattice;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Additional type checking step after {@link TypeCheck} for checking that <a
 * href="https://en.wikipedia.org/wiki/Information_flow_(information_theory)">Information Flow</a>
 * is satisfied on a type level.
 *
 * <p>The base semantics for Information Flow in ZkRust is:
 *
 * <ul>
 *   <li>Context level starts at public.
 *   <li>Side effects must be at least the same level as the context level.
 *   <li>Bodies of if-statements will be raised to the level of the condition.
 *   <li>Non-special cased control flow is only allowed at the public context level. These include:
 *       {@link TypedAst.Statement.Return}, {@link TypedAst.Expression.Continue}, {@link
 *       TypedAst.Expression.Break}.
 *   <li>Notably: {@link TypedAst.Expression.If} and {@link TypedAst.Expression.CallDirectly} can be
 *       used in non-public contexts, due to the de-branching deoptimizations performed on those
 *       expressions.
 * </ul>
 *
 * <p>Terminology:
 *
 * <ul>
 *   <li>Security label: Label on a piece of information data describes what security clearance is
 *       needed to view the information.
 *   <li>Context level: The label assigned to the given scope.
 * </ul>
 *
 * @see <a href="https://en.wikipedia.org/wiki/Information_flow_(information_theory)">Information
 *     Flow, Wikipedia</a>
 */
public final class TypeCheckInformationFlow {

  private TypeCheckInformationFlow() {}

  /** Label of the security level of some type. */
  enum SecurityLabel implements Lattice<SecurityLabel> {
    /** Public information. */
    PUBLIC,
    /** Secret information which should not be accessible at the public level. */
    SECRET,
    /** Security level is unknown. */
    UNKNOWN;

    @Override
    public SecurityLabel leastUpperBound(SecurityLabel other) {
      if (this == UNKNOWN || other == UNKNOWN) {
        return UNKNOWN;
      }
      return other == SECRET ? other : this;
    }

    /**
     * Returns whether information at this level can flow to the other {@link SecurityLabel}.
     *
     * @param other Security label to check flow for.
     * @return true if data can flow to the other label.
     */
    public boolean flowsTo(SecurityLabel other) {
      if (this == UNKNOWN || other == UNKNOWN) {
        return false;
      }
      return leastUpperBound(other) == other;
    }
  }

  /**
   * Environment for checking information flow.
   *
   * @param structDefinitions Definitions of structs; used for checking types.
   * @param contextLevel The label of the context. Can only be raised through the {@link
   *     #raiseContextLevel} method.
   * @param contextLevelRaisePosition Indicates the position of the last position raise.
   */
  record Env(
      Map<TypedAst.TypeId, TypedAst.StructDefinition> structDefinitions,
      SecurityLabel contextLevel,
      TypedAst.PosInterval contextLevelRaisePosition) {

    /**
     * Increases the context level with the given additional label.
     *
     * @param additionalLabel Level to increase with.
     * @param raisePosition Position to mark as the place performing the raise.
     */
    public Env raiseContextLevel(
        SecurityLabel additionalLabel, TypedAst.PosInterval raisePosition) {
      final var newLevel = contextLevel.leastUpperBound(additionalLabel);
      if (newLevel != contextLevel) {
        return new Env(structDefinitions, newLevel, raisePosition);
      } else {
        return this;
      }
    }
  }

  /**
   * Performs information flow control check on given program. Expects that the program is complete,
   * with no external references.
   *
   * @param errorsAccum List of errors. An empty list indicates that there is nothing wrong with the
   *     program, and that it satisfies all relevant semantics.
   * @param program Program to check.
   */
  public static void checkProgram(ErrorAccum errorsAccum, TypedAst.Program program) {
    final Map<TypedAst.TypeId, TypedAst.StructDefinition> structDefinitions = new LinkedHashMap<>();
    for (final TypedAst.StructDefinition structDef : program.structItems()) {
      structDefinitions.put(structDef.typeId(), structDef);
    }

    final Env env = new Env(structDefinitions, SecurityLabel.PUBLIC, null);

    for (final TypedAst.Function function : program.functionItems()) {
      checkExpression(errorsAccum, env, function.body());
    }
  }

  private static void checkExpression(
      final ErrorAccum errorsAccum, final Env env, final TypedAst.TypedExpression exprTyped) {

    // Base cases: Continue, Break

    if (exprTyped.expr() instanceof TypedAst.Expression.Continue expr) {
      assertContextLevelIsPublic(
          errorsAccum, env, exprTyped.position(), "Continue requires PUBLIC context level");

    } else if (exprTyped.expr() instanceof TypedAst.Expression.Break expr) {
      assertContextLevelIsPublic(
          errorsAccum, env, exprTyped.position(), "Break requires PUBLIC context level");

    } else if (exprTyped.expr() instanceof TypedAst.Expression.ForInRange expr) {
      assertContextLevelIsPublic(
          errorsAccum, env, exprTyped.position(), "For-expressions requires PUBLIC context level");
      checkExpression(errorsAccum, env, expr.intoIteratorExpr());
      checkExpression(errorsAccum, env, expr.bodyExpr());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.If expr) {
      checkExpression(errorsAccum, env, expr.condExpr());

      final Env envBody =
          env.raiseContextLevel(
              securityLabelOf(expr.condExpr().resultType(), env), expr.condExpr().position());

      checkExpression(errorsAccum, envBody, expr.thenExpr());
      checkExpression(errorsAccum, envBody, expr.elseExpr());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.Assign expr) {

      final var placeExprLabel = securityLabelOf(expr.placeExpr().resultType(), env);
      if (placeExprLabel != SecurityLabel.UNKNOWN) {
        assertContextLevelIsAtMost(
            errorsAccum,
            env,
            placeExprLabel,
            exprTyped.position(),
            "Assignments require a context level at most the level of the place expression");
        checkExpression(errorsAccum, env, expr.placeExpr());
        checkExpression(errorsAccum, env, expr.valueExpr());
      }

      // Recursive cases:

    } else if (exprTyped.expr() instanceof TypedAst.Expression.Block expr) {
      expr.statements().forEach(s -> checkStatement(errorsAccum, env, s));
      checkExpression(errorsAccum, env, expr.resultExpr());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.Unary expr) {
      checkExpression(errorsAccum, env, expr.expr1());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.Binary expr) {
      checkExpression(errorsAccum, env, expr.expr1());
      checkExpression(errorsAccum, env, expr.expr2());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.TupleConstructor expr) {
      expr.exprs().forEach(e -> checkExpression(errorsAccum, env, e));

    } else if (exprTyped.expr() instanceof TypedAst.Expression.TupleAccess expr) {
      checkExpression(errorsAccum, env, expr.tupleExpr());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.CallDirectly expr) {
      expr.argumentExprs().forEach(e -> checkExpression(errorsAccum, env, e));

    } else if (exprTyped.expr() instanceof TypedAst.Expression.TypeCast expr) {
      checkExpression(errorsAccum, env, expr.expr1());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.StructConstructor expr) {
      expr.fieldInits().forEach(f -> checkExpression(errorsAccum, env, f.fieldInitExpr()));

    } else if (exprTyped.expr() instanceof TypedAst.Expression.FieldAccess expr) {
      checkExpression(errorsAccum, env, expr.structExpr());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.ArrayConstructorRepeat expr) {
      checkExpression(errorsAccum, env, expr.repeatExpression());
      checkExpression(errorsAccum, env, expr.sizeExpression());

    } else if (exprTyped.expr() instanceof TypedAst.Expression.ArrayConstructorExplicit expr) {
      expr.elementExpressions().forEach(e -> checkExpression(errorsAccum, env, e));
    }
  }

  private static void checkStatement(
      ErrorAccum errorsAccum, Env env, TypedAst.Statement uncheckedStatement) {
    // Base case

    if (uncheckedStatement instanceof TypedAst.Statement.Return statement) {

      assertContextLevelIsPublic(
          errorsAccum, env, statement.position(), "Return requires PUBLIC context level");

      checkExpression(errorsAccum, env, statement.resultExpr());

      // Recursive cases:

    } else if (uncheckedStatement instanceof TypedAst.Statement.Let statement) {
      checkExpression(errorsAccum, env, statement.initExpr());

    } else /*if (uncheckedStatement instanceof TypedAst.Statement.Expr statement)*/ {
      final TypedAst.Statement.Expr statement = (TypedAst.Statement.Expr) uncheckedStatement;
      checkExpression(errorsAccum, env, statement.expr());
    }
  }

  /**
   * Determines an appropriate {@link SecurityLabel} for the given type. An environment is needed to
   * resolve structs.
   *
   * <p>Not all types can produce useful security labels, for example if they contain mixed data, or
   * if they contain no data (like {@link TypedAst.Type.ErrorType}).
   *
   * @param uncheckedType Type to determine {@link SecurityLabel} for.
   * @param env Environment needed to resolve structs.
   * @return Security label.
   */
  static SecurityLabel securityLabelOf(TypedAst.Type uncheckedType, final Env env) {

    // Base cases

    if (uncheckedType instanceof TypedAst.Type.Int type) {
      return SecurityLabel.PUBLIC;
    } else if (uncheckedType instanceof TypedAst.Type.Sbi type) {
      return SecurityLabel.SECRET;
    } else if (uncheckedType instanceof TypedAst.Type.ErrorType type) {
      return SecurityLabel.UNKNOWN;

      // Recursive cases

    } else if (uncheckedType instanceof TypedAst.Type.Array type) {
      return securityLabelOf(type.elementType(), env);

    } else if (uncheckedType instanceof TypedAst.Type.Tuple type) {
      return securityLabelOf(type.subtypes(), env);

    } else /* if (uncheckedType instanceof TypedAst.Type.NamedType type)*/ {
      final TypedAst.Type.NamedType type = (TypedAst.Type.NamedType) uncheckedType;

      final TypedAst.StructDefinition structDefinition = env.structDefinitions().get(type.typeId());

      final TypeReplacementMap typeAssignments =
          TypeReplacementMap.from(
              structDefinition.typeParameterNames().stream().map(TypedAst.TypeId::newFor).toList(),
              type.typeArguments());

      final var subtypes =
          structDefinition.fields().stream()
              .map(TypedAst.StructDefinition.StructField::fieldTypeWithTypeParameters)
              .map(typeAssignments::replaceParametersInType)
              .toList();
      return securityLabelOf(subtypes, env);
    }
  }

  private static SecurityLabel securityLabelOf(Collection<TypedAst.Type> types, final Env env) {
    final var labels = types.stream().map(t -> securityLabelOf(t, env)).collect(Collectors.toSet());

    if (labels.isEmpty()) {
      return SecurityLabel.PUBLIC;
    } else if (labels.size() == 1) {
      return List.copyOf(labels).get(0);
    } else {
      return SecurityLabel.UNKNOWN;
    }
  }

  private static void assertContextLevelIsPublic(
      final ErrorAccum errorsAccum,
      final Env env,
      final TypedAst.PosInterval position,
      final String msg) {
    assertContextLevelIsAtMost(errorsAccum, env, SecurityLabel.PUBLIC, position, msg);
  }

  private static void assertContextLevelIsAtMost(
      final ErrorAccum errorsAccum,
      final Env env,
      final SecurityLabel wantedSecurityLabel,
      final TypedAst.PosInterval position,
      final String msg) {
    if (!env.contextLevel().flowsTo(wantedSecurityLabel)) {
      errorsAccum.add(
          new ErrorPresenter.ErrorLine(
              ErrorPresenter.Line.from(
                  position,
                  "%s. Context level was %s, but expected %s"
                      .formatted(msg, env.contextLevel(), wantedSecurityLabel)),
              List.of(
                  ErrorPresenter.Line.from(
                      env.contextLevelRaisePosition(),
                      "context level raised to %s by expression".formatted(env.contextLevel())))));
    }
  }
}
