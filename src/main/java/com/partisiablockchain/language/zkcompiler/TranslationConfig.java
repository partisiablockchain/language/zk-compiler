package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.PrintStream;

/**
 * Contains configuration information about the translation.
 *
 * <ul>
 *   <li>{@code sourcePath}: Name of the original code input source.
 *   <li>{@code hooks}: Translation hooks, providing debugging information for intermediate formats.
 *   <li>{@code errorOutput}: Stream to write error output to.
 *   <li>{@code colorOutput}: True if output should be colored, false otherwise.
 * </ul>
 */
public record TranslationConfig(
    TranslationHooks hooks, PrintStream errorOutput, boolean colorOutput, boolean cfgTests) {

  /** Configuration with default compilation options. */
  public static final TranslationConfig DEFAULT =
      new TranslationConfig(new TranslationHooks.Default(), null, false, false);

  /**
   * Produces new translation config with a new error output.
   *
   * @param errorOutput Replacement error output.
   * @param colorOutput Outputs colored text if and only if true.
   * @return Newly created translation config.
   */
  public TranslationConfig withErrorOutput(
      final PrintStream errorOutput, final boolean colorOutput) {
    return new TranslationConfig(this.hooks, errorOutput, colorOutput, false);
  }
}
