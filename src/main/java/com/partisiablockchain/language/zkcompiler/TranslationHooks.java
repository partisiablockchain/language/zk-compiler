package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.irpass.IrPass;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;

/** Interface responsible for handling the intermediate formats in the translation. */
public interface TranslationHooks {

  /**
   * Debug information for {@link TypedAst}, produced by {@link AstToIrTranslator}.
   *
   * @param program the root ast program.
   */
  default void debugTypedAst(TypedAst.Program program) {}

  /**
   * Debug information for initial {@link Ir.Circuit}, produced by {@link AstToIrTranslator}.
   *
   * @param circuit the initial ir circuit.
   */
  default void debugIrInitial(Ir.Circuit circuit) {}

  /**
   * Debug information for {@link MetaCircuit}, produced by {@link IrToMetaCircuitTranslator}.
   *
   * @param metaCircuit the generated metacircuit.
   */
  default void debugMetaCircuit(MetaCircuit metaCircuit) {}

  /**
   * Debug information after an IR Phase ({@link IrPass}.)
   *
   * @param passInfo information about the ir pass.
   * @param circuit the circuit generated for the current pass.
   * @param isDifferentFromPrevious whether the ir produced by the pass is different from the input
   *     ir.
   */
  default void debugIrPass(
      PassInformation passInfo, Ir.Circuit circuit, boolean isDifferentFromPrevious) {}

  /** Default Translation Hook implementation with no behaviour. */
  record Default() implements TranslationHooks {}
}
