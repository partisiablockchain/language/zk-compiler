package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.irpass.IdGenerator;
import java.util.ArrayList;
import java.util.List;

/**
 * Instruction accumulator, where block-level instructions are accumulated. Possess a variety of
 * convience functions for adding instructions.
 */
public final class InstructionAccum {

  private Ir.BlockId currentBlockId;
  private ArrayList<Ir.Instruction> currentInstructions;
  private List<Ir.Input> currentInputs;
  private boolean currentBlockIsEntryPoint = false;

  private IdGenerator<Ir.VariableId> variableIdGenerator;
  private IdGenerator<Ir.BlockId> blockIdGenerator;
  private final ArrayList<Ir.Block> blocks = new ArrayList<Ir.Block>();

  /**
   * Create new {@link InstructionAccum}, using the given id generators.
   *
   * @param variableIdGenerator Id generator for {@link Ir.VariableId}.
   * @param blockIdGenerator Id generator for {@link Ir.BlockId}.
   */
  public InstructionAccum(
      IdGenerator<Ir.VariableId> variableIdGenerator, IdGenerator<Ir.BlockId> blockIdGenerator) {
    this.variableIdGenerator = variableIdGenerator;
    this.blockIdGenerator = blockIdGenerator;
  }

  /** Create new empty {@link InstructionAccum}. */
  public InstructionAccum() {
    this(IdGenerator.forVariableIds(0), IdGenerator.forBlockIds(0));
  }

  /**
   * Create new unique {@link Ir.BlockId}.
   *
   * @param tmpName Block name
   * @return Newly created BlockId.
   */
  public Ir.BlockId newBlockId(final String tmpName) {
    return blockIdGenerator.newId(tmpName);
  }

  /**
   * Create new unique {@link Ir.VariableId}.
   *
   * @param tmpName Variable name
   * @return Newly created VariableId.
   */
  public Ir.VariableId newVariableId(final String tmpName) {
    return variableIdGenerator.newId(tmpName);
  }

  /**
   * Create new unique {@link Ir.VariableId}.
   *
   * @param tmpName Variable name
   * @return Newly created VariableId.
   */
  Ir.VariableId newVariableId(final TmpName tmpName) {
    return newVariableId(tmpName.determineTmpName());
  }

  //// Blocks

  /**
   * Adds given {@link Ir.Terminator} to the accumulator, closing the current block.
   *
   * @param terminator Terminator to add.
   * @throws InternalCompilerException When no block is open.
   */
  public void addTerminator(final Ir.Terminator terminator) {
    if (currentBlockId == null) {
      throw new InternalCompilerException("Ending a block that haven't been started");
    }

    blocks.add(
        new Ir.Block(
            currentBlockId,
            List.copyOf(currentInputs),
            List.copyOf(currentInstructions),
            terminator,
            currentBlockIsEntryPoint));
    currentBlockId = null;
    currentInstructions = null;
    currentInputs = null;
    currentBlockIsEntryPoint = false;
  }

  /**
   * Initializes a new block.
   *
   * @param blockId Id of block. Never null.
   * @param inputs Expected inputs for block. Never null.
   * @param isEntryPoint If true, the block is the entry point of the circuit, where execution
   *     starts.
   * @throws InternalCompilerException When a block is already open.
   */
  public void newBlock(
      final Ir.BlockId blockId, final List<Ir.Input> inputs, boolean isEntryPoint) {
    if (currentBlockId != null) {
      throw new InternalCompilerException(
          "Starting new block before closing the previous block%n  New block: %s%n  Old block: %s",
          blockId, currentBlockId);
    }

    currentBlockId = requireNonNull(blockId);
    currentInstructions = new ArrayList<Ir.Instruction>();
    currentInputs = requireNonNull(inputs);
    currentBlockIsEntryPoint = isEntryPoint;
  }

  /**
   * Produces copy of all closed blocks.
   *
   * @return Unmodifiable list of closed blocks.
   */
  public List<Ir.Block> blocks() {
    return List.copyOf(blocks);
  }

  //// Instructions

  /**
   * Adds instruction to accumulator.
   *
   * <p>Guarenteed to include the instruction with no optimizations.
   *
   * @param insn Instruction to add.
   * @return Operand of instruction result.
   */
  public Operand.Variable add(final Ir.Instruction insn) {
    currentInstructions.add(insn);
    return new Operand.Variable(insn.resultId(), insn.resultType());
  }

  /**
   * Adds instruction to accumulator.
   *
   * <p>Guarenteed to include the instruction with no optimizations.
   *
   * @param tmpName Temporary name to use for variable.
   * @param resultType Type produced by operation. Never null.
   * @param operation Operation to perform. Never null.
   * @return Operand of instruction result.
   */
  public Operand.Variable add(
      final TmpName tmpName, final Ir.Type resultType, final Ir.Operation operation) {
    return add(newVariableId(tmpName), resultType, operation);
  }

  /**
   * Adds instruction to accumulator.
   *
   * <p>Guarenteed to include the instruction with no optimizations.
   *
   * @param variableId Variable id to use for result. Never null.
   * @param resultType Type produced by operation. Never null.
   * @param operation Operation to perform. Never null.
   * @return Operand of instruction result.
   */
  public Operand.Variable add(
      final Ir.VariableId variableId, final Ir.Type resultType, final Ir.Operation operation) {
    return add(new Ir.Instruction(variableId, resultType, operation));
  }

  /**
   * Produces an instruction sequence, which itself assignes the given operand to a variable id, if
   * it isn't already.
   *
   * @param forcedType Type to produce.
   * @param uncheckedOper Operand to produce variable id for.
   * @return Variable operand.
   */
  public Ir.VariableId addIdentity(final Ir.Type forcedType, final Operand.Basic uncheckedOper) {
    requireNonNull(uncheckedOper);
    if (uncheckedOper instanceof Operand.Array c) {
      return addIdentity(forcedType, c.underlyingRepresentation());
    } else if (uncheckedOper instanceof Operand.Constant c) {
      final Ir.VariableId resultId = newVariableId(c.tmpName());
      addAssign(resultId, forcedType, c);
      return resultId;
    } else {
      return ((Operand.Variable) uncheckedOper).variable();
    }
  }

  /**
   * Produces an instruction sequence, which itself assignes the given operand to a variable id, if
   * it isn't already.
   *
   * @param uncheckedOper Operand to produce variable id for.
   * @return Variable operand.
   */
  public Ir.VariableId addIdentity(final Operand.Basic uncheckedOper) {
    return addIdentity(uncheckedOper.type(), uncheckedOper);
  }

  /**
   * Produces an instruction that assigns the given operand to the given variable id. Cannot be
   * optimized.
   *
   * @param forcedId Id for result variable.
   * @param forcedType Type to produce.
   * @param uncheckedOper Operand to assign.
   */
  public void addAssign(
      final Ir.VariableId forcedId, final Ir.Type forcedType, final Operand.Basic uncheckedOper) {
    requireNonNull(uncheckedOper);
    if (uncheckedOper instanceof Operand.Constant c) {
      add(forcedId, forcedType, new Ir.Operation.Constant(forcedType, c.value()));
    } else {
      final Ir.VariableId variableId = ((Operand.Variable) uncheckedOper).variable();
      add(forcedId, forcedType, new Ir.Operation.Extract(variableId, forcedType, 0));
    }
  }

  Operand.Basic addCastFromPublicToSecret(
      final Ir.Type.SecretBinaryInteger resultType,
      final TmpName tmpName,
      final Operand.Basic oper) {
    final Ir.VariableId operVariable = addIdentity(oper);
    return add(tmpName, resultType, new Ir.Operation.PublicToSecret(resultType, operVariable));
  }

  Operand.Basic addBinop(
      final Ir.Type type,
      final TmpName tmpName,
      final Ir.Operation.BinaryOp binop,
      final Operand.Basic oper1,
      final Operand.Basic oper2) {
    requireNonNull(binop);
    final Ir.VariableId operVariable1 = addIdentity(oper1);
    final Ir.VariableId operVariable2 = addIdentity(oper2);
    return add(tmpName, type, new Ir.Operation.Binary(binop, operVariable1, operVariable2));
  }

  Operand.Basic addUnop(
      final Ir.Type type,
      final TmpName tmpName,
      final Ir.Operation.UnaryOp unop,
      final Operand.Basic oper1) {
    requireNonNull(unop);
    final Ir.VariableId operVariable1 = addIdentity(oper1);
    return add(tmpName, type, new Ir.Operation.Unary(unop, operVariable1));
  }

  Operand.Basic addNullary(
      final Ir.Type type, final TmpName tmpName, final Ir.Operation.NullaryOp nulop) {
    return add(tmpName, type, new Ir.Operation.Nullary(nulop));
  }

  Operand.Basic addExtract(
      final Ir.Type resultType,
      final TmpName tmpName,
      final Operand.Basic operToExtractFrom,
      final int bitOffset) {
    final Ir.VariableId variableToExtractFrom = addIdentity(operToExtractFrom);
    return add(
        tmpName,
        resultType,
        new Ir.Operation.Extract(variableToExtractFrom, resultType, bitOffset));
  }

  Operand.Basic addExtract(
      final Ir.Type resultType,
      final TmpName tmpName,
      final Operand.Basic operToExtractFrom,
      final Operand.Basic operOffsetFromLow) {
    final Ir.VariableId variableToExtractFrom = addIdentity(operToExtractFrom);
    final Ir.VariableId variableOffsetFromLow = addIdentity(operOffsetFromLow);
    return add(
        tmpName,
        resultType,
        new Ir.Operation.ExtractDyn(variableToExtractFrom, resultType, variableOffsetFromLow));
  }

  Operand.Basic addInsert(
      final Ir.Type resultType,
      final TmpName tmpName,
      final Operand.Basic operInsertInto,
      final Operand.Basic operInsert,
      final Operand.Basic operOffsetFromLow) {
    final Ir.VariableId variableInsertInto = addIdentity(operInsertInto);
    final Ir.VariableId variableInsert = addIdentity(operInsert);
    final Ir.VariableId variableOffsetFromLow = addIdentity(operOffsetFromLow);
    return add(
        tmpName,
        resultType,
        new Ir.Operation.InsertDyn(variableInsertInto, variableInsert, variableOffsetFromLow));
  }

  Operand.Basic addCall(
      final Ir.Type resultType,
      final TmpName tmpName,
      final Ir.FunctionId functionId,
      final List<Operand.Basic> argumentOperands,
      final int numReturnValues) {
    final List<Ir.VariableId> variableIds =
        argumentOperands.stream().map(Operand::expectBasic).map(this::addIdentity).toList();
    return add(
        tmpName,
        resultType,
        new Ir.Operation.CallDirectly(functionId, variableIds, numReturnValues));
  }
}
