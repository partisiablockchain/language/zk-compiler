package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.ir.IrPretty;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Structured value data for individual phases of the compiler intended to produce {@link Ir}.
 *
 * <p>Represents either a constant value {@link Operand.Constant}, a variable ({@link
 * Operand.Variable}) or a recursively defined tuple ({@link Operand.Tuple}).
 */
public sealed interface Operand {

  /**
   * Returns the component operands of this operand as a list. For {@link Basic} operands, this will
   * return a singleton list with the operand itself. May also return an empty list.
   *
   * @return List of basic components of operand.
   */
  List<Operand.Basic> baseOperands();

  /**
   * Asserts that this operand is {@link Basic}.
   *
   * @return This operand as a Basic operand.
   * @exception InternalCompilerException If the operand is not a basic operand.
   */
  default Basic expectBasic() {
    throw new InternalCompilerException("%s is not Basic", this);
  }

  /**
   * Asserts that this operand is {@link Tuple}.
   *
   * @return This operand as a Tuple operand.
   * @exception InternalCompilerException If the operand is not a tuple operand.
   */
  default List<Operand> expectTuple() {
    throw new InternalCompilerException("%s is not Tuple", this);
  }

  /**
   * Asserts that this operand is {@link Constant}.
   *
   * @return This operand as a {@link Constant} operand.
   * @exception InternalCompilerException If the operand is not a {@link Constant} operand.
   */
  default Operand.Constant expectConstant() {
    throw new InternalCompilerException("%s is not Constant", this);
  }

  /**
   * Asserts that this operand is {@link Array}.
   *
   * @return This operand as a {@link Array} operand.
   * @exception InternalCompilerException If the operand is not a {@link Array} operand.
   */
  default Operand.Array expectArray() {
    throw new InternalCompilerException("%s is not Array", this);
  }

  /**
   * Type of the base operand.
   *
   * @return Type of the operand.
   */
  OperandType operandType();

  /** Basic operands are those that can fit within a single word. */
  sealed interface Basic extends Operand {
    /**
     * Type of the base operand.
     *
     * @return Type of the operand.
     */
    Ir.Type type();

    @Override
    default OperandType operandType() {
      return new OperandType.BasicType(this.type());
    }

    @Override
    default List<Operand.Basic> baseOperands() {
      return List.of(this);
    }

    @Override
    default Operand.Basic expectBasic() {
      return this;
    }
  }

  /**
   * A value that have been computed at runtime. Can be produced directly by the AST (for example by
   * {@link TypedAst.Expression.IntLiteral}), or by various optimizations.
   *
   * @param value Value of constant.
   * @param type Type of constant.
   * @param tmpName Temporary name of constant.
   */
  record Constant(BigInteger value, Ir.Type type, TmpName tmpName) implements Operand, Basic {

    /**
     * Constructor for {@link Constant} from BigInteger.
     *
     * @param value Unsigned representation of constant value.
     * @param type Type of constant.
     * @param tmpName Temporary name of constant.
     */
    public Constant {
      requireNonNull(value);
      requireNonNull(type);
      requireNonNull(tmpName);
      if (value.signum() < 0) {
        throw new IllegalArgumentException(
            "Operand.Constant does not allow negative values: %s".formatted(value));
      }
    }

    /**
     * Constructor for {@link Constant} from int.
     *
     * @param value Value of constant.
     * @param type Type of constant.
     * @param tmpName Temporary name of constant.
     */
    public Constant(int value, Ir.Type type, TmpName tmpName) {
      this(BigInteger.valueOf(value), type, tmpName);
    }

    @Override
    public String toString() {
      return "" + value;
    }

    @Override
    public Operand.Constant expectConstant() {
      return this;
    }
  }

  /**
   * A variable.
   *
   * @param variable Actual variable id.
   * @param type Type of variable.
   */
  record Variable(Ir.VariableId variable, Ir.Type type) implements Operand, Basic {

    /**
     * Constructor for {@link Variable}.
     *
     * @param variable Actual variable id.
     * @param type Type of variable.
     */
    public Variable {
      requireNonNull(variable);
      requireNonNull(type);
    }

    @Override
    public String toString() {
      return IrPretty.prettyVariable(variable);
    }
  }

  /**
   * Tuple operand containing zero or more component operands.
   *
   * @param componentOperands Component operands of operand.
   */
  record Tuple(List<Operand> componentOperands) implements Operand {

    /**
     * Constructor for {@link Tuple}.
     *
     * @param componentOperands Component operands of operand.
     */
    public Tuple {
      requireNonNull(componentOperands);
    }

    @Override
    public OperandType.TupleType operandType() {
      return new OperandType.TupleType(
          componentOperands.stream().map(Operand::operandType).toList());
    }

    @Override
    public List<Operand.Basic> baseOperands() {
      return componentOperands.stream().map(Operand::baseOperands).flatMap(List::stream).toList();
    }

    @Override
    public List<Operand> expectTuple() {
      return componentOperands;
    }

    @Override
    public String toString() {
      return componentOperands.stream()
          .map(Object::toString)
          .collect(Collectors.joining(", ", "(", ")"));
    }

    /** Useful constant for a tuple operand with no components. */
    public static final Tuple UNIT = new Tuple(List.of());
  }

  /**
   * Array operand containing several elements of identical type.
   *
   * @param underlyingRepresentation The type used to actually store the array information.
   * @param operandType Type of this operation.
   */
  record Array(Operand.Basic underlyingRepresentation, OperandType.ArrayType operandType)
      implements Basic {

    /**
     * Constructor for {@link Array}.
     *
     * @param underlyingRepresentation The type used to actually store the array information.
     * @param operandType Type of this operation.
     */
    public Array {
      requireNonNull(underlyingRepresentation);
      requireNonNull(operandType);
    }

    @Override
    public Ir.Type type() {
      return underlyingRepresentation.type();
    }

    @Override
    public Operand.Array expectArray() {
      return this;
    }

    @Override
    public List<Operand.Basic> baseOperands() {
      return List.of(underlyingRepresentation);
    }

    @Override
    public String toString() {
      return "%s as %s".formatted(underlyingRepresentation, operandType);
    }
  }
}
