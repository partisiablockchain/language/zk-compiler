package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.math.BigInteger;

/**
 * Utility class for outputting special-cased instruction sequences.
 *
 * <p>The emit methods in this class does not guarentee any specific instruction sequence, just that
 * semantics are correct.
 */
public final class OperandUtil {

  private OperandUtil() {}

  /**
   * Emits code for transforming given operand to the desired output type, by truncating if too
   * input was too large, or by zero-extending if smaller.
   *
   * @param insnAccum Instruction accumulator.
   * @param outputType Desired output type.
   * @param operandIn Given operand.
   * @param tmpName Temporary name for output.
   * @return Operand with same size at outputType.
   */
  public static Operand.Basic truncateOrZeroExtend(
      final InstructionAccum insnAccum,
      final Ir.Type outputType,
      final Operand.Basic operandIn,
      final TmpName tmpName) {

    final int widthOut = outputType.width();
    final int widthIn = operandIn.type().width();

    // Truncate
    if (widthOut <= widthIn) {
      return addExtract(
          insnAccum, outputType, tmpName, operandIn, new Operand.Constant(0, Ir.Type.I32, tmpName));

      // Zero-extend
    } else {
      final Operand.Constant paddingConstant =
          new Operand.Constant(0, ofSize(outputType, widthOut - widthIn), tmpName);
      return insnAccum.addBinop(
          outputType, tmpName, Ir.Operation.BinaryOp.BIT_CONCAT, paddingConstant, operandIn);
    }
  }

  /**
   * Creates a new {@link Ir.Type} based upon the given type with the given size.
   *
   * @param <T> Type to base off of.
   * @param uncheckedType Instance of type to base off of.
   * @param size Size for new type.
   * @return Newly created type. Guarenteed to be same type as {@code uncheckedType}.
   */
  @SuppressWarnings("unchecked")
  public static <T extends Ir.Type> T ofSize(T uncheckedType, int size) {
    if (uncheckedType instanceof Ir.Type.SecretBinaryInteger type) {
      return (T) new Ir.Type.SecretBinaryInteger(size);
    } else {
      return (T) new Ir.Type.PublicInteger(size);
    }
  }

  /**
   * Emits instruction sequence for extracting from an operand with a non-constant operand.
   *
   * @param insnAccum instruction accumulator to add to.
   * @param type Type of result.
   * @param tmpName Temporary name.
   * @param baseOperand Base operand for extracting from.
   * @param offsetOperand Non-constant operand used to index with.
   * @return Operand produced by the indexing operation.
   */
  public static Operand.Basic addExtract(
      final InstructionAccum insnAccum,
      final Ir.Type type,
      final TmpName tmpName,
      final Operand.Basic baseOperand,
      final Operand.Basic offsetOperand) {
    if (type.width() == 0) {
      return new Operand.Constant(0, ofSize(type, 0), tmpName);
    }
    if (offsetOperand instanceof Operand.Constant constant) {
      final int offset = constant.value().intValue();
      final boolean sameBitsize = type.equals(baseOperand.type());
      if (sameBitsize && offset == 0) {
        return baseOperand;
      } else {
        return insnAccum.addExtract(type, tmpName, baseOperand, offset);
      }
    } else {
      return insnAccum.addExtract(type, tmpName, baseOperand, offsetOperand);
    }
  }

  /**
   * Emits instruction sequence for left shifting.
   *
   * @param insnAccum instruction accumulator to add to.
   * @param type Type of result.
   * @param tmpName Temporary name.
   * @param oper1 Operand to shift.
   * @param oper2 Operand of shift amount.
   * @return Operand produced by the shift operand.
   */
  public static Operand.Basic addShiftLeft(
      final InstructionAccum insnAccum,
      final Ir.Type type,
      final TmpName tmpName,
      final Operand.Basic oper1,
      final Operand.Basic oper2) {
    return insnAccum.addBinop(
        type, tmpName, Ir.Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL, oper1, oper2);
  }

  /**
   * Emits instruction sequence for multiplying an operand with a constant. Multiplying with a
   * constant is often much more efficient than multiplying with with a non-constant. This method
   * will choose the most efficient method, based on the constant.
   *
   * @param insnAccum instruction accumulator to add to.
   * @param tmpName Temporary name.
   * @param oper1 Operand to multiply
   * @param const2 Constant multiplication factor operand.
   * @return Operand produced by multiplication.
   */
  public static Operand.Basic addMultiplyConstant(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final Operand.Basic oper1,
      final int const2) {
    if (oper1 instanceof Operand.Constant const1) {
      return new Operand.Constant(
          const1.value().multiply(BigInteger.valueOf(const2)), oper1.type(), tmpName);
    } else if (isPowerOfTwo(const2)) {
      final int power = Integer.numberOfTrailingZeros(const2);
      final Ir.Type shiftAmountType = ofSize(Ir.Type.I32, oper1.type().width());
      return addShiftLeft(
          insnAccum,
          oper1.type(),
          tmpName,
          oper1,
          new Operand.Constant(power, shiftAmountType, tmpName));
    }
    return insnAccum.addBinop(
        oper1.type(),
        tmpName,
        Ir.Operation.BinaryOp.MULT_SIGNED_WRAPPING,
        oper1,
        new Operand.Constant(const2, oper1.type(), tmpName));
  }

  /**
   * Emits instruction sequence for adding two operands. May constant optimize when given constant
   * operands.
   *
   * @param insnAccum instruction accumulator to add to.
   * @param tmpName Temporary name.
   * @param oper1 First operand
   * @param oper2 Second operand
   * @return Operand produced by addition.
   */
  public static Operand.Basic addAdd(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final Operand.Basic oper1,
      final Operand.Basic oper2) {
    if (oper1 instanceof Operand.Constant const1 && const1.value().equals(BigInteger.ZERO)) {
      return oper2;
    } else if (oper2 instanceof Operand.Constant const2 && const2.value().equals(BigInteger.ZERO)) {
      return oper1;
    } else if (oper1 instanceof Operand.Constant const1
        && oper2 instanceof Operand.Constant const2) {
      return new Operand.Constant(const1.value().add(const2.value()), const1.type(), tmpName);
    }
    return insnAccum.addBinop(
        oper1.type(), tmpName, Ir.Operation.BinaryOp.ADD_WRAPPING, oper1, oper2);
  }

  /**
   * Emits instruction sequence for concatting two operands. May constant optimize when given
   * 0-sized operands.
   *
   * @param insnAccum instruction accumulator to add to.
   * @param tmpName Temporary name.
   * @param operTop Operand for the top bits.
   * @param operBottom Operand for the bottom bits.
   * @return Operand produced by addition.
   */
  public static Operand.Basic addConcat(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final Operand.Basic operTop,
      final Operand.Basic operBottom) {
    final int bitsTop = operTop.type().width();
    final int bitsBottom = operBottom.type().width();
    final Ir.Type resultType = ofSize(operTop.type(), bitsTop + bitsBottom);

    if (bitsTop == 0 && bitsBottom == 0) {
      return new Operand.Constant(0, resultType, tmpName);
    } else if (bitsTop == 0) {
      return operBottom;
    } else if (bitsBottom == 0) {
      return operTop;
    }
    return insnAccum.addBinop(
        resultType, tmpName, Ir.Operation.BinaryOp.BIT_CONCAT, operTop, operBottom);
  }

  /**
   * Determines whether the given integer is a power of two.
   *
   * @param n Integer to check.
   * @return Whether the integer is a power of two or not.
   */
  static boolean isPowerOfTwo(final int n) {
    return Integer.bitCount(n) == 1;
  }
}
