package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;

/** Type check exception that occurs when type checking fails. */
public abstract class CompilationException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  /** Individual error lines. */
  private final ArrayList<ErrorPresenter.ErrorLine> errors;

  /**
   * Creates a new exception with the given errors.
   *
   * @param errors Individual error lines.
   */
  public CompilationException(final List<ErrorPresenter.ErrorLine> errors) {
    super(formatMessage(errors));
    this.errors = new ArrayList<>(errors);
  }

  private static String formatMessage(final List<ErrorPresenter.ErrorLine> errors) {
    final var strBuilder = new StringBuilder();
    strBuilder.append("Errors in program:");
    for (final ErrorPresenter.ErrorLine error : errors) {
      strBuilder.append(
          "%n    %s:%s : %s"
              .formatted(
                  error.errorLine().lineNumber(),
                  error.errorLine().column(),
                  error.errorLine().message()));
    }
    return strBuilder.toString();
  }

  /**
   * Individual error lines.
   *
   * @return List of individual error lines.
   */
  public List<ErrorPresenter.ErrorLine> errors() {
    return errors;
  }
}
