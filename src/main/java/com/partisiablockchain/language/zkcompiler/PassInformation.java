package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcompiler.irpass.IrPass;

/** Stores information about the current pass, and it's position in the pipeline. */
public record PassInformation(IrPass irPass, int passNumber, boolean isFinalPass) {

  /**
   * Constructor for pass information.
   *
   * @param irPass The current ir pass.
   * @param passNumber Indicator of how far in the pipeline this pass is.
   * @param isFinalPass Whether the pass was the last of the IR passes.
   */
  public PassInformation {
    requireNonNull(irPass);
  }
}
