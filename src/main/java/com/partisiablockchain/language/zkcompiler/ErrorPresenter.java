package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkparser.ZkAst;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/** Utility class for presenting type checking errors. */
final class ErrorPresenter {

  private ErrorPresenter() {}

  /**
   * Single line of a presented error message. This line can either be a primary error line, or a
   * note line.
   *
   * @param filepath Path to the file to highlight. Not nullable.
   * @param lineNumber Line of the file to highlight.
   * @param column Start of the column span to highlight.
   * @param columnWidth Width of column span to highlight.
   * @param message Message for highlight. Not nullable.
   */
  record Line(Path filepath, int lineNumber, int column, int columnWidth, String message) {

    /** Constructor for {@link Line}. */
    public Line {
      Objects.requireNonNull(filepath);
      Objects.requireNonNull(message);
    }

    /**
     * Create {@link Line} from a position and a message.
     *
     * @param filepath Filepath of the error.
     * @param position Position for error.
     * @param message Message for error.
     * @return Newly created {@link Line}. Never null.
     */
    public static Line from(
        final Path filepath, final ZkAst.PosInterval position, final String message) {
      return new Line(
          filepath,
          position.lineIdxStart(),
          position.columnIdxStart(),
          position.columnIdxEndExclusive() - position.columnIdxStart(),
          message);
    }

    /**
     * Create {@link Line} from a position and a message.
     *
     * @param position Typed position for error.
     * @param message Message for error.
     * @return Newly created {@link Line}. Never null.
     */
    public static Line from(final TypedAst.PosInterval position, final String message) {
      return from(position.filepath(), position.position(), message);
    }
  }

  /**
   * A compilation error, with code highlighting, and possibly notes, each with their own highlight.
   *
   * @param errorLine The primary error line, indicating the AST element where the error was
   *     detected.
   * @param notes Supplementary information with highlighting.
   */
  record ErrorLine(Line errorLine, List<Line> notes) {

    /** Constructor for {@link ErrorLine}. */
    public ErrorLine {
      Objects.requireNonNull(errorLine);
      Objects.requireNonNull(notes);
    }

    /**
     * Create {@link ErrorLine} from a position and a message.
     *
     * @param filepath Filepath of the error.
     * @param position Position for error.
     * @param message Message for error.
     * @return Newly created {@link ErrorLine}. Never null.
     */
    public static ErrorLine from(
        final Path filepath, final ZkAst.PosInterval position, final String message) {
      return new ErrorLine(Line.from(filepath, position, message), List.of());
    }
  }

  /**
   * Produces string representation of the given type errors.
   *
   * @param errors Type errors to format.
   * @param programInput Program to format error for.
   * @param colors Color configuration.
   * @return Newly formatted error message as string.
   */
  public static String presentToString(
      final List<ErrorLine> errors, final ProgramInput programInput, final Colors colors) {
    final StringBuilder builder = new StringBuilder();

    final Map<Path, String[]> linesInFiles = preprocessLinesInFiles(programInput);

    for (int idx = 0; idx < errors.size(); idx++) {
      final ErrorLine errorLine = errors.get(idx);

      formatLine(builder, linesInFiles, colors, errorLine.errorLine(), false);
      for (final Line note : errorLine.notes()) {
        builder.append("\n");
        formatLine(builder, linesInFiles, colors, note, true);
      }

      // Format trailing line
      if (idx < errors.size() - 1) {
        builder.append("\n\n");
      }
    }

    // Remove trailing newlines
    builder.append(colors.normal());
    return builder.toString();
  }

  private static void formatLine(
      final StringBuilder builder,
      final Map<Path, String[]> linesInFiles,
      final Colors colors,
      final Line line,
      final boolean isNote) {
    // Determine what to display
    final String lineIdxAsStr = Integer.toString(line.lineNumber());
    final String indent = " ".repeat(lineIdxAsStr.length());
    final String extractErrorText =
        linesInFiles.get(line.filepath())[Integer.max(0, line.lineNumber() - 1)];

    final String colorUnderline;

    // Format message
    if (isNote) {
      colorUnderline = colors.note();
      builder.append(colorUnderline).append("note").append(colors.noteMessage());
    } else {
      colorUnderline = colors.error();
      builder.append(colorUnderline).append("error").append(colors.errorMessage());
    }

    builder.append(": ").append(line.message()).append("\n");

    // Format display of line
    builder
        .append(indent)
        .append("--> ")
        .append(colors.normal())
        .append(prettyPath(line.filepath()))
        .append(colors.errorMessage())
        .append("\n")
        .append(indent)
        .append(" |")
        .append("\n")
        .append(lineIdxAsStr)
        .append(" | ")
        .append(colors.normal())
        .append(extractErrorText);

    // Format error underline
    builder
        .append(colors.errorMessage())
        .append("\n")
        .append(indent)
        .append(" | ")
        .append(" ".repeat(Integer.max(0, line.column())))
        .append(colorUnderline)
        .append("^".repeat(Integer.max(1, line.columnWidth())));

    if (isNote) {
      builder.append(" ").append(line.message());
    }

    // Trailing "code" line
    builder.append(colors.errorMessage()).append("\n").append(indent).append(" |");
  }

  /**
   * Produces a map from file paths to their lines.
   *
   * @param programInput Program to determine lines for.
   * @return Map of program paths to lines.
   */
  private static Map<Path, String[]> preprocessLinesInFiles(final ProgramInput programInput) {
    return programInput.files().stream()
        .collect(
            Collectors.toUnmodifiableMap(
                ProgramInput.FileInput::sourcePath, f -> f.rustSourceCode().split("\\R", -1)));
  }

  private static String prettyPath(Path path) {
    return path.toString().replaceAll("\\\\", "/");
  }

  //// Color support

  /** Color configuration for the presenter. */
  public record Colors(
      String error, String errorMessage, String normal, String note, String noteMessage) {

    /** Configuration with ANSI escaped color output. */
    public static Colors ANSI =
        new Colors("\033[31;1m", "\033[0;1m", "\033[0m", "\033[0;1m", "\033[0m");

    /** Configuration with no color output. */
    public static Colors NONE = new Colors("", "", "", "", "");
  }
}
