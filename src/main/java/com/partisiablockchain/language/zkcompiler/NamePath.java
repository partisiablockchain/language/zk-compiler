package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.stream.Stream;

/** Represents an arbitrary path in {@link TypedAst}. */
public sealed interface NamePath {

  /**
   * Represents relative path in {@link TypedAst}.
   *
   * @param path List of path elements.
   */
  record Relative(List<String> path) implements NamePath {

    /**
     * Create new relative path.
     *
     * @param path List of path elements.
     */
    public Relative {
      requireNonNull(path);
      if (path.isEmpty()) {
        throw new IllegalArgumentException("Relative path cannot be empty!");
      }
    }

    /**
     * Create new relative path.
     *
     * @param path Array of path elements.
     */
    public Relative(String... path) {
      this(List.of(path));
    }

    @Override
    public String toString() {
      return String.join("::", path);
    }
  }

  /**
   * Represents absolute path in {@link TypedAst}.
   *
   * @param path List of path elements.
   */
  record Absolute(List<String> path) implements NamePath {

    /**
     * Create new absolute path.
     *
     * @param path List of path elements.
     */
    public Absolute {
      requireNonNull(path);
    }

    /**
     * Create new relative path.
     *
     * @param path Array of path elements.
     */
    public Absolute(String... path) {
      this(List.of(path));
    }

    @Override
    public String toString() {
      return String.join("::", path);
    }

    /**
     * Relativizes the given item path based on this path.
     *
     * @param otherPath Item path to relativize.
     * @return Newly relative path.
     */
    public NamePath.Relative relativize(NamePath.Absolute otherPath) {
      if (!this.isPrefixOf(otherPath)) {
        throw new IllegalArgumentException(
            "Path %s is not a strict prefix of other path %s".formatted(this, otherPath));
      }
      return new NamePath.Relative(otherPath.path().subList(path.size(), otherPath.path().size()));
    }

    /**
     * Create new path based on this path appended with given name.
     *
     * @param name Sub-path name.
     * @return Newly created path.
     */
    public NamePath.Absolute resolve(String name) {
      return new NamePath.Absolute(Stream.of(path, List.of(name)).flatMap(List::stream).toList());
    }

    /**
     * The last name element of the variable id's path.
     *
     * @return Last path element.
     */
    public String last() {
      return path.get(path.size() - 1);
    }

    /**
     * Checks whether this path is a strict prefix of the other given path.
     *
     * <p>Note that {@code !path.isPrefixOf(path)}.
     *
     * @param otherPath Path to check is a suffix.
     * @return true if this path was a prefix of the given path.
     */
    public boolean isPrefixOf(NamePath.Absolute otherPath) {
      if (path.size() >= otherPath.path().size()) {
        return false;
      }
      for (int idx = 0; idx < path.size(); idx++) {
        if (!path.get(idx).equals(otherPath.path().get(idx))) {
          return false;
        }
      }
      return true;
    }
  }
}
