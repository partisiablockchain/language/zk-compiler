package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Storage class for certain built-in functions. */
public final class BuiltinFunctions {

  private BuiltinFunctions() {}

  /**
   * Retrieves a module definition with a given path, if it exists.
   *
   * @param modulePath Path of the module.
   * @return Module definition, or null if no modules were know with that name.
   */
  public static ModuleDefinition lookupModule(final NamePath.Absolute modulePath) {

    // Determine contained elements
    final List<FnDefinition> functions =
        FUNCTIONS_LIST.stream()
            .filter(f -> modulePath.isPrefixOf(f.uniqueFunctionId().fullNamePath()))
            .toList();
    final List<TypeCheck.TypeAliasInfo> types =
        TYPES.stream().filter(t -> modulePath.isPrefixOf(t.typeId().fullNamePath())).toList();
    final List<TypedAst.StructDefinition> structs =
        STRUCTS.stream().filter(t -> modulePath.isPrefixOf(t.typeId().fullNamePath())).toList();

    final int numFoundItems = Stream.of(functions, types, structs).mapToInt(List::size).sum();

    // Return module if it exists
    if (numFoundItems > 0) {
      return new ModuleDefinition(modulePath, functions, types, structs);
    } else {
      return null;
    }
  }

  /**
   * Retrieve a specific function, using the function's typed variable id.
   *
   * @param fnName Function's typed variable id.
   * @return Relevant function definition, or null if not defined.
   */
  public static FnDefinition getFunction(final TypedAst.VariableId fnName) {
    return FUNCTIONS.get(fnName);
  }

  /**
   * Imported build-in module definition.
   *
   * @param modulePath Module path of module.
   * @param functionItems Function items of imported module.
   * @param typeItems Type items of imported module.
   * @param structItems Struct items of imported module.
   */
  public record ModuleDefinition(
      NamePath.Absolute modulePath,
      List<FnDefinition> functionItems,
      List<TypeCheck.TypeAliasInfo> typeItems,
      List<TypedAst.StructDefinition> structItems) {}

  /**
   * Built-in function definition.
   *
   * @param typeInfo Generic function type information
   * @param callGenerator Built-in function call generator
   */
  public record FnDefinition(
      TypeCheck.FunctionInfoGeneric typeInfo,
      CallGenerator callGenerator,
      boolean modifyFirstArgument) {

    static FnDefinition from(
        TypedAst.VariableId uniqueFunctionId,
        List<String> typeArgumentNames,
        List<TypedAst.Type> valueArguments,
        TypedAst.Type returnType,
        CallGenerator callGenerator,
        boolean modifyFirstArgument) {
      return new FnDefinition(
          new TypeCheck.FunctionInfoGeneric(
              uniqueFunctionId,
              typeArgumentNames,
              valueArguments,
              returnType,
              TypeCheck.POSITION_UNKNOWN),
          callGenerator,
          modifyFirstArgument);
    }

    /**
     * Unique function id shortcut.
     *
     * @return The unique function id of this function.
     */
    public TypedAst.VariableId uniqueFunctionId() {
      return typeInfo.uniqueVariableId();
    }
  }

  /** Functional interface for the functions that produces function call sequences. */
  @FunctionalInterface
  public interface CallGenerator {

    /**
     * Generate a call sequence for this specific function.
     *
     * @param insnAccum Accumulator for instructions.
     * @param paramOperands Parameters given to function. Already guarenteed to fit the parameter
     *     list in {@link FnDefinition}.
     * @param tmpName Naming info for the output variable.
     * @param resultOperandType Expected operand type of the output variable.
     * @return Function output operand.
     */
    Operand emit(
        InstructionAccum insnAccum,
        TmpName tmpName,
        List<Operand> paramOperands,
        OperandType resultOperandType);
  }

  private static final List<FnDefinition> FUNCTIONS_LIST =
      Stream.<Stream<FnDefinition>>of(
              BuiltinFunctionsSbi.FUNCTIONS_LIST.stream(),
              BuiltinFunctionsRustStd.FUNCTIONS_LIST.stream(),
              BuiltinFunctionsVec.FUNCTIONS_LIST.stream())
          .flatMap(Function.identity())
          .toList();

  static final List<TypedAst.StructDefinition> STRUCTS =
      Stream.of(
              BuiltinFunctionsRustStd.STRUCTS.stream(),
              BuiltinFunctionsSbi.STRUCTS.stream(),
              BuiltinFunctionsVec.STRUCTS.stream())
          .flatMap(Function.identity())
          .toList();

  private static final List<TypeCheck.TypeAliasInfo> TYPES =
      Stream.of(
              BuiltinFunctionsRustStd.TYPES.stream(),
              BuiltinFunctionsSbi.TYPES.stream(),
              BuiltinFunctionsVec.TYPES.stream())
          .flatMap(Function.identity())
          .toList();

  private static final Map<TypedAst.VariableId, FnDefinition> FUNCTIONS =
      FUNCTIONS_LIST.stream()
          .collect(
              Collectors.toUnmodifiableMap(FnDefinition::uniqueFunctionId, Function.identity()));
}
