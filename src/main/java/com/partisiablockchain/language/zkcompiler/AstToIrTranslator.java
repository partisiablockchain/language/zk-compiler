package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Map.entry;
import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.CheckReturnValue;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.irpass.IdGenerator;
import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkparser.ZkAst;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Utility class for translating an AST program to a Ir.Circuit for later transformation and
 * eventual translation to a MetaCircuit.
 *
 * @see IrToMetaCircuitTranslator
 */
public final class AstToIrTranslator {

  private AstToIrTranslator() {}

  /**
   * Translate entire program to a Ir-Circuit.
   *
   * @param program Program to translate.
   * @param testInformations test information for program.
   * @param config Translation config with various optimization configurations.
   * @return Produced Ir-Circuit.
   */
  public static Result translate(
      final TypedAst.Program program,
      List<TestInformation> testInformations,
      final TranslationConfig config) {

    final int highestReservedId =
        program.functionItems().stream()
            .map(TypedAst.Function::explicitExportId)
            .filter(Objects::nonNull)
            .mapToInt(Integer::intValue)
            .max()
            .orElse(-1);
    final IdGenerator<Ir.FunctionId> functionIdGenerator =
        IdGenerator.forFunctionIds(highestReservedId + 1);

    // Create environments
    final Map<TypedAst.TypeId, TypedAst.StructDefinition> structEnvironment =
        program.structItems().stream()
            .collect(
                Collectors.toUnmodifiableMap(
                    TypedAst.StructDefinition::typeId, Function.identity()));

    final Map<TypedAst.VariableId, TypedAst.ConstantDefinition> constantEnvironment =
        program.constantItems().stream()
            .collect(
                Collectors.toUnmodifiableMap(
                    TypedAst.ConstantDefinition::identifier, Function.identity()));

    final List<TypedAst.Function> functionItems =
        program.functionItems().stream()
            .sorted(Comparator.comparing(TypedAst.Function::visibility).reversed())
            .toList();

    final Map<TypedAst.VariableId, Ir.FunctionId> functionIdentifiers =
        functionItems.stream()
            .collect(
                Collectors.toUnmodifiableMap(
                    TypedAst.Function::identifier,
                    fnDef -> {
                      final String name = fnDef.identifier().fullNamePath().toString();
                      return fnDef.explicitExportId() != null
                          ? new Ir.FunctionId(fnDef.explicitExportId(), name)
                          : functionIdGenerator.newId(name);
                    }));

    final Environments env =
        new Environments(structEnvironment, constantEnvironment, functionIdentifiers);

    // Translate functions
    final List<Ir.Function> functions = new ArrayList<>();
    for (final TypedAst.Function function : functionItems) {
      functions.add(translateFunction(function, env));
    }

    // Get functionId of test functions
    List<TestInformation> updatedTestInformation =
        testInformations.stream()
            .map(
                testInfo -> {
                  int functionId = env.functionIdentifiers.get(testInfo.identifier()).id();
                  return new TestInformation(
                      testInfo.result(),
                      testInfo.secretInputs(),
                      testInfo.secretOutputs(),
                      testInfo.position(),
                      testInfo.identifier(),
                      new FunctionId(functionId));
                })
            .toList();

    // Finalize
    return new Result(new Ir.Circuit(List.copyOf(functions)), updatedTestInformation);
  }

  /**
   * Result of translating to Ir.
   *
   * @param circuit the IrCircuit
   * @param testInformations information about the tests
   */
  public record Result(Ir.Circuit circuit, List<TestInformation> testInformations) {}

  /**
   * Translate function to a Ir-Circuit block.
   *
   * @param function Function to translate.
   * @param env Environments to compile function in.
   * @return Produced block.
   */
  private static Ir.Function translateFunction(
      final TypedAst.Function function, final Environments env) {
    final Ir.FunctionId functionId = env.functionIdentifiers().get(function.identifier());

    // Initialize accumulator
    final InstructionAccum insnAccum = new InstructionAccum();

    // Translate input
    final var inputs = new ArrayList<Ir.Input>();
    final var paramOperandTypeList = new ArrayList<OperandType>();
    for (final TypedAst.Parameter param : function.parameters()) {
      final OperandType paramOperandType = translateType(param.type(), env);
      final Ir.Type inputTypeAmalgam = IrAmalgamUtility.typeAmalgam(paramOperandType, Ir.Type.UNIT);

      final TmpName tmpName = TmpName.root(param.identifier().last());
      inputs.add(new Ir.Input(insnAccum.newVariableId(tmpName), inputTypeAmalgam));
      paramOperandTypeList.add(paramOperandType);
    }

    // Initialize block with unpacking inputs
    insnAccum.newBlock(insnAccum.newBlockId(function.identifier().last()), inputs, true);

    // Extract operands from amalgam inputs
    final var inputAmalgams = new ArrayList<Operand>();
    for (int idx = 0; idx < inputs.size(); idx++) {
      final TmpName tmpName = TmpName.root(function.parameters().get(idx).identifier().last());
      final Operand.Basic operAmalgam = operandFromInput(inputs.get(idx));
      inputAmalgams.add(
          IrAmalgamUtility.emitAmalgamUnpacking(
              paramOperandTypeList.get(idx), operAmalgam, insnAccum, tmpName));
    }

    // Construct body
    final Context ctx = Context.fromInputs(function.parameters(), inputAmalgams);
    final List<Ir.Type> irReturnTypes = translateTypeReturnToExternal(function.returnType(), env);
    final ExprResult bodyResult =
        translateExpression(function.body(), insnAccum, ctx, TmpName.root("body"), env);

    translateReturnToExternal(insnAccum, bodyResult.operand(), function.returnType());

    // Finalize
    return new Ir.Function(
        functionId,
        insnAccum.blocks(),
        irReturnTypes,
        function.visibility() == ZkAst.ItemVisibility.PUBLIC);
  }

  /**
   * Produced result after translating an expression. Contains operand containing the result value
   * of the expression, and output context, tracking changes to the context that changed in the
   * expression.
   *
   * @param operand Operand result.
   * @param outContext Produced context.
   */
  private record ExprResult(Operand operand, Context outContext) {

    public ExprResult {
      requireNonNull(operand);
      requireNonNull(outContext);
    }
  }

  private record ExprResults(List<Operand> operands, Context outContext) {
    ExprResults {
      requireNonNull(operands);
      requireNonNull(outContext);
    }

    Operand operand(int index) {
      return operands.get(index);
    }
  }

  /**
   * Translates an expression.
   *
   * @param typedExpr Expression to translate.
   * @param insnAccum Accumulator for instructions.
   * @param ctx Current context right before entering the expression.
   * @param tmpName Naming info for generated temporary variables.
   * @param env Environment.
   * @return Expression result
   */
  static ExprResult translateExpression(
      final TypedAst.TypedExpression typedExpr,
      final InstructionAccum insnAccum,
      final Context ctx,
      final TmpName tmpName,
      final Environments env) {

    final TypedAst.Expression uncheckedExpr = typedExpr.expr();

    // Ir.Block expression
    if (uncheckedExpr instanceof TypedAst.Expression.Block expr) {
      return translateBlock(expr, insnAccum, ctx, tmpName, env);

      // Unary expression
    } else if (uncheckedExpr instanceof TypedAst.Expression.Unary expr) {
      final Ir.Operation.UnaryOp operation = UNOP_MAPPING.get(expr.operation());
      final ExprResult exprResult1 =
          translateExpression(expr.expr1(), insnAccum, ctx, tmpName.with("inner"), env);
      final Operand.Basic operandIn = exprResult1.operand().expectBasic();
      final Operand.Basic operandOut =
          insnAccum.addUnop(operandIn.type(), tmpName, operation, operandIn);
      return new ExprResult(operandOut, exprResult1.outContext());

      // Binary expression
    } else if (uncheckedExpr instanceof TypedAst.Expression.Binary expr) {

      // Index special case
      if (expr.operation() == ZkAst.Binop.INDEX) {
        return translatePlaceLookup(typedExpr, insnAccum, ctx, tmpName, env);
      }

      // Translate subexpressions
      final ExprResults exprResults =
          translateExpressions(
              List.of(expr.expr1(), expr.expr2()),
              insnAccum,
              ctx,
              List.of(tmpName.with("left"), tmpName.with("right")),
              env);

      // Determine binop
      final BinopDef opInfo = determineBinop(expr.operation(), expr.expr1().resultType());

      final Operand resultOperand =
          translateBinop(
              insnAccum, opInfo, exprResults.operand(0), exprResults.operand(1), tmpName);
      return new ExprResult(resultOperand, exprResults.outContext());

      // Integer literal
    } else if (uncheckedExpr instanceof TypedAst.Expression.IntLiteral expr) {
      final Ir.Type integerType = translateType(typedExpr.resultType(), env).expectBasic();
      final BigInteger unsignedValue = unsignedBigInteger(integerType.width(), expr.value());
      return new ExprResult(new Operand.Constant(unsignedValue, integerType, tmpName), ctx);

      // Tuple constructor expression
    } else if (uncheckedExpr instanceof TypedAst.Expression.TupleConstructor expr) {
      return translateTupleExpression(expr, insnAccum, ctx, tmpName, env);

      // Tuple dot expression
    } else if (uncheckedExpr instanceof TypedAst.Expression.TupleAccess expr) {

      final ExprResult tupleExprResult =
          translateExpression(expr.tupleExpr(), insnAccum, ctx, tmpName.with("tuple"), env);

      final Operand.Tuple tupleOperand = (Operand.Tuple) tupleExprResult.operand();
      final Operand resultOperand = tupleOperand.componentOperands().get(expr.index());

      return new ExprResult(resultOperand, tupleExprResult.outContext());

      // If-expression
    } else if (uncheckedExpr instanceof TypedAst.Expression.If expr) {
      return translateIfExpression(expr, insnAccum, ctx, tmpName, env);

      // For-expression
    } else if (uncheckedExpr instanceof TypedAst.Expression.ForInRange expr) {
      return translateForInExpression(expr, insnAccum, ctx, env);

      // Assignments
    } else if (uncheckedExpr instanceof TypedAst.Expression.Assign expr) {
      final var valueTmpName = getTmpNameForPlaceExpression(expr.placeExpr().expr());

      final ExprResult valueResult =
          translateExpression(expr.valueExpr(), insnAccum, ctx, valueTmpName, env);

      return translateAssignExpression(
          expr.placeExpr(), valueResult.operand(), insnAccum, valueResult.outContext(), env);

      // Call directly
    } else if (uncheckedExpr instanceof TypedAst.Expression.CallDirectly expr) {
      return translateCallExpression(expr, typedExpr.resultType(), insnAccum, ctx, tmpName, env);

      // Type-cast expression
    } else if (uncheckedExpr instanceof TypedAst.Expression.TypeCast expr) {
      final ExprResult exprResult1 =
          translateExpression(expr.expr1(), insnAccum, ctx, tmpName.with("inner"), env);
      final Ir.Type outputType = translateType(expr.resultType(), env).expectBasic();
      final Operand.Basic operandIn = exprResult1.operand().expectBasic();
      final Operand.Basic operandOut =
          OperandUtil.truncateOrZeroExtend(insnAccum, outputType, operandIn, tmpName);
      return new ExprResult(operandOut, exprResult1.outContext());

      // Struct constructor
    } else if (uncheckedExpr instanceof TypedAst.Expression.StructConstructor expr) {
      return translateStructConstructorExpression(expr, insnAccum, ctx, tmpName, env);

      // Struct field access
    } else if (uncheckedExpr instanceof TypedAst.Expression.FieldAccess expr) {
      return translatePlaceLookup(typedExpr, insnAccum, ctx, tmpName, env);

      // Continue
    } else if (uncheckedExpr instanceof TypedAst.Expression.Continue expr) {
      final Context unreachableContext =
          emitContextBranch(insnAccum, ctx, Context::getLoopIdNext, false);
      return newUnreachableBlock(insnAccum, unreachableContext, tmpName);

      // Break
    } else if (uncheckedExpr instanceof TypedAst.Expression.Break expr) {
      final Context unreachableContext =
          emitContextBranch(insnAccum, ctx, Context::getLoopIdEnd, true);
      return newUnreachableBlock(insnAccum, unreachableContext, tmpName);

      // Array constructor: Repeat
    } else if (uncheckedExpr instanceof TypedAst.Expression.ArrayConstructorRepeat expr) {
      final ExprResults exprResults =
          translateExpressions(
              List.of(expr.repeatExpression(), expr.sizeExpression()),
              insnAccum,
              ctx,
              List.of(tmpName.with("init"), tmpName.with("size")),
              env);

      final int numElements = exprResults.operand(1).expectConstant().value().intValue();

      final Operand elementOperand = exprResults.operand(0);
      final List<Operand> repeated =
          Stream.generate(() -> elementOperand).limit(numElements).toList();
      final Ir.Type repeatedAmalgamType =
          IrAmalgamUtility.typeAmalgam(elementOperand.operandType(), Ir.Type.UNIT);
      final Operand.Basic resultInternal =
          IrAmalgamUtility.emitAmalgamPacking(
              new Operand.Tuple(repeated), insnAccum, repeatedAmalgamType);

      final Operand.Array result =
          new Operand.Array(
              resultInternal, new OperandType.ArrayType(elementOperand.operandType(), numElements));
      return new ExprResult(result, exprResults.outContext());

      // Array constructor: Explicit
    } else if (uncheckedExpr instanceof TypedAst.Expression.ArrayConstructorExplicit expr) {
      final ExprResults exprResults =
          translateExpressions(expr.elementExpressions(), insnAccum, ctx, tmpName, env);

      final int numElements = exprResults.operands().size();
      final Operand elementOperand = exprResults.operand(0);
      final Ir.Type repeatedAmalgamType =
          IrAmalgamUtility.typeAmalgam(elementOperand.operandType(), Ir.Type.UNIT);

      final Operand.Basic resultInternal =
          IrAmalgamUtility.emitAmalgamPacking(
              new Operand.Tuple(exprResults.operands()), insnAccum, repeatedAmalgamType);

      final Operand result =
          new Operand.Array(
              resultInternal, new OperandType.ArrayType(elementOperand.operandType(), numElements));

      return new ExprResult(result, exprResults.outContext());

      // Variable Expression
    } else if (uncheckedExpr instanceof TypedAst.Expression.LocalVariable expr) {

      // Defined variable
      return translatePlaceLookup(typedExpr, insnAccum, ctx, tmpName, env);

      // Is constant?
    } else {
      final TypedAst.Expression.GlobalVariable expr =
          (TypedAst.Expression.GlobalVariable) uncheckedExpr;

      final TypedAst.ConstantDefinition constant =
          env.constantDefinitions().get(expr.variableIdentifier());
      final TypedAst.Expression.IntLiteral initExpr =
          (TypedAst.Expression.IntLiteral) constant.initExpr().expr();
      final Ir.Type constantIrType =
          translateType(constant.initExpr().resultType(), env).expectBasic();
      final Operand operandConstant =
          new Operand.Constant(
              initExpr.value(), constantIrType, TmpName.root(expr.variableIdentifier().last()));
      return new ExprResult(operandConstant, ctx);
    }
  }

  /**
   * Translates the given assign expression.
   *
   * @param insnAccum Accumulator for instructions.
   * @param ctx Current context right before entering the expression.
   * @param env Environment.
   * @return Expression result
   */
  private static ExprResult translateAssignExpression(
      final TypedAst.TypedExpression placeExpression,
      final Operand valueOperand,
      final InstructionAccum insnAccum,
      final Context ctx,
      final Environments env) {

    final PlaceResult placeResult =
        translatePlace(placeExpression, insnAccum, ctx, TmpName.root("place"), env);

    final TypedAst.VariableId placeVariable = placeResult.placeVariable();

    final TmpName tmpName = TmpName.root(placeResult.placeVariable().last());

    final Operand preAssignmentOperand = placeResult.outContext().get(placeVariable);
    final Operand assignedThroughOperand =
        replaceInOperandByPath(
            preAssignmentOperand, placeResult.indexOperands(), valueOperand, insnAccum, tmpName);

    return new ExprResult(
        Operand.Tuple.UNIT, placeResult.outContext().assign(placeVariable, assignedThroughOperand));
  }

  /**
   * Produces a usable {@link TmpName} for the given {@link TypedAst.Expression}.
   *
   * @param uncheckedExpression Expression to determine useful {@link TmpName} for.
   * @return Produced {@link TmpName}.
   */
  static TmpName getTmpNameForPlaceExpression(TypedAst.Expression uncheckedExpression) {
    final List<String> path = new ArrayList<>();

    while (true) {
      if (uncheckedExpression instanceof TypedAst.Expression.LocalVariable expr) {
        path.add(expr.variableIdentifier().fullNamePath().toString());
        break;
      } else if (uncheckedExpression instanceof TypedAst.Expression.FieldAccess expr) {
        path.add(expr.fieldName());
        uncheckedExpression = expr.structExpr().expr();
      } else if (uncheckedExpression instanceof TypedAst.Expression.TupleAccess expr) {
        path.add("" + expr.index());
        uncheckedExpression = expr.tupleExpr().expr();
      } else if (uncheckedExpression instanceof TypedAst.Expression.Binary expr
          && expr.operation() == ZkAst.Binop.INDEX) {
        path.add("IDX");
        uncheckedExpression = expr.expr1().expr();
      } else {
        throw new UnsupportedOperationException(
            "Cannot determine TmpName for given expression: " + uncheckedExpression);
      }
    }

    Collections.reverse(path);
    return TmpName.root(path.stream().collect(Collectors.joining("_")));
  }

  /**
   * Emits instruction sequence for producing the wanted {@link Operand} by indexing into the given
   * root operand by the given indexes.
   *
   * @param outputType Type of the wanted operand.
   * @param rootOperand Root operand to index through.
   * @param indexOperands Index operands
   * @param insnAccum Instruction accumulator.
   * @param tmpName Temporary names.
   * @return Produced operand.
   */
  static Operand getOperandByPath(
      final OperandType outputType,
      final Operand rootOperand,
      final List<IndexAndSize> indexOperands,
      final InstructionAccum insnAccum,
      final TmpName tmpName) {
    final PlaceLocation assignmentLocation =
        determinePlaceLocation(rootOperand, indexOperands, insnAccum, tmpName);
    if (assignmentLocation.offset() == null) {
      return assignmentLocation.rootOperand();
    } else {
      return IrAmalgamUtility.emitAmalgamExtraction(
          outputType,
          assignmentLocation.rootOperand().expectBasic(),
          assignmentLocation.offset(),
          insnAccum,
          tmpName);
    }
  }

  /**
   * Determines place location for indexing into the root operand using the index operands.
   *
   * @param rootOperand Root operand to index through.
   * @param indexOperands Index operands
   * @param insnAccum Instruction accumulator.
   * @param tmpName Temporary names.
   * @return Produced operand.
   */
  static PlaceLocation determinePlaceLocation(
      final Operand rootOperand,
      final List<IndexAndSize> indexOperands,
      final InstructionAccum insnAccum,
      final TmpName tmpName) {
    Operand indexedOperand = rootOperand;

    // Index through operands
    final List<Integer> indexes = new ArrayList<>();
    for (final var indexAndSize : indexOperands) {
      final Operand.Basic index = indexAndSize.index();
      if (!(indexedOperand instanceof Operand.Tuple)) {
        break;
      }
      final int indexValue = index.expectConstant().value().intValue();
      indexes.add(indexValue);
      indexedOperand = indexedOperand.expectTuple().get(indexValue);
    }

    // Index through amalgam
    Operand.Basic offset = null;
    if (indexedOperand instanceof Operand.Basic operand) {
      offset = new Operand.Constant(0, Ir.Type.I32, tmpName);
      OperandType operandType = operand.operandType();
      for (final IndexAndSize indexAndSize :
          indexOperands.subList(indexes.size(), indexOperands.size())) {
        // Tuple
        if (operandType instanceof OperandType.TupleType tupleOperandType) {
          final int fieldIndex = indexAndSize.index().expectConstant().value().intValue();

          int fieldOffset = 0;
          for (int prevFieldIndex = 0; prevFieldIndex < fieldIndex; prevFieldIndex++) {
            final OperandType fieldType = tupleOperandType.componentTypes().get(prevFieldIndex);
            final int fieldSize = IrAmalgamUtility.operandTypeBitwidth(fieldType);
            fieldOffset += fieldSize;
          }
          offset =
              OperandUtil.addAdd(
                  insnAccum,
                  tmpName,
                  offset,
                  new Operand.Constant(fieldOffset, Ir.Type.I32, tmpName));
          operandType = tupleOperandType.componentTypes().get(fieldIndex);

          // Assume array
        } else {
          final OperandType.ArrayType arrayOperandType = (OperandType.ArrayType) operandType;
          final Operand.Basic index = indexAndSize.index();
          final Operand.Basic indexBySize =
              OperandUtil.addMultiplyConstant(
                  insnAccum, tmpName, index, indexAndSize.elementTypeSize());
          offset = OperandUtil.addAdd(insnAccum, tmpName, offset, indexBySize);
          operandType = arrayOperandType.subtype();
        }
      }
    }

    return new PlaceLocation(indexedOperand, indexes, offset);
  }

  /**
   * Tracks location for some given place. A location is determined by indexing through the
   * rootOperand and then offseting into the resulting operand. The offset comes into account when
   * an array is indexed through.
   *
   * <p>Example: The {@link PlaceLocation} of {@code %b} in root operand {@code ((%a, %b), (%c,
   * %d))}, would be {@code indexes = [0, 1]} with {@code offset = 0}.
   *
   * <p>Example 2: The {@link PlaceLocation} of {@code %a3} in root operand {@code [(%a1, %b1),
   * (%a2, %b2), (%a3, %b3)]}, would be {@code indexes = []} with {@code offset = 2 * |2word| + 0 *
   * |word|}.
   *
   * @param rootOperand Operand to index and offset through. Never null.
   * @param indexes Indexes to index through operand. Only used if rootOperand is a tuple. Never
   *     null.
   * @param offset Bit offset where place is located. May be null.
   */
  private record PlaceLocation(Operand rootOperand, List<Integer> indexes, Operand.Basic offset) {
    PlaceLocation {
      requireNonNull(indexes);
      requireNonNull(rootOperand);
    }
  }

  /**
   * Performs an replacement/assignment to the sub-operand determined by a root operand and index
   * operands.
   *
   * <p>Examples, using a very informal syntax:
   *
   * <ul>
   *   <li>In {@code %a}, replace {@code []} with {@code %x}: {@code %x}.
   *   <li>In {@code (%a, %b)}, replace {@code [1]} with {@code %x}: {@code (%a, %x)}.
   *   <li>In {@code ((%a, %b), (%c, %d))}, replace {@code [0, 1]} with {@code %x}: {@code ((%a,
   *       %x), (%c, %d))}.
   *   <li>In {@code ([%a1, %a2, %a3, %a4], (%c, %d))}, replace {@code [0, 3]} with {@code %x}:
   *       {@code ([%a1, %a2, %a3, %x], (%c, %d))}.
   *   <li>In {@code [(%a1, %b1), (%a2, %b2), (%a3, %b3)]}, replace {@code [1, 1]} with {@code %x}:
   *       {@code [(%a1, %b1), (%a2, %x), (%a3, %b3)]}.
   * </ul>
   *
   * @param rootOperand Root operand to index through.
   * @param indexOperands Index operands.
   * @param valueOperand Operand to assign.
   * @param insnAccum Instruction accumulator.
   * @param tmpName Temporary names.
   * @return Operand with the replace value.
   */
  private static Operand replaceInOperandByPath(
      final Operand rootOperand,
      final List<IndexAndSize> indexOperands,
      final Operand valueOperand,
      final InstructionAccum insnAccum,
      final TmpName tmpName) {
    final PlaceLocation assignmentLocation =
        determinePlaceLocation(rootOperand, indexOperands, insnAccum, tmpName);
    return replaceInOperandByPathInternal(
        rootOperand, assignmentLocation, 0, valueOperand, insnAccum, tmpName);
  }

  /**
   * Internal work-horse for {@link replaceInOperandByPath}. Performs an replacement/assignment to
   * the sub-operand determined by {@link PlaceLocation}.
   *
   * @param uncheckedBaseOperand Operand to index into
   * @param assignmentLocation Place location for designed replacement.
   * @param indexIndex index of current place location index ({@link PlaceLocation#indexes}).
   * @param valueOperand Operand to assign.
   * @param insnAccum Instruction accumulator.
   * @param tmpName Temporary names.
   * @return Operand with the replace value.
   * @see replaceInOperandByPath
   */
  private static Operand replaceInOperandByPathInternal(
      final Operand uncheckedBaseOperand,
      final PlaceLocation assignmentLocation,
      int indexIndex,
      final Operand valueOperand,
      final InstructionAccum insnAccum,
      final TmpName tmpName) {
    if (indexIndex == assignmentLocation.indexes().size()) {
      if (uncheckedBaseOperand instanceof Operand.Array baseOperand) {
        return IrAmalgamUtility.emitAmalgamSplice(
            baseOperand,
            valueOperand.expectBasic(),
            assignmentLocation.offset(),
            tmpName,
            insnAccum);
      } else if (uncheckedBaseOperand instanceof Operand.Basic baseOperand) {
        return IrAmalgamUtility.emitAmalgamSplice(
            baseOperand,
            valueOperand.expectBasic(),
            assignmentLocation.offset(),
            tmpName,
            insnAccum);
      } else {
        return valueOperand;
      }
    }

    final List<Operand> tupleEntries = new ArrayList<>(uncheckedBaseOperand.expectTuple());
    final int currentIndex = assignmentLocation.indexes().get(indexIndex);
    tupleEntries.set(
        currentIndex,
        replaceInOperandByPathInternal(
            tupleEntries.get(currentIndex),
            assignmentLocation,
            indexIndex + 1,
            valueOperand,
            insnAccum,
            tmpName));

    return new Operand.Tuple(tupleEntries);
  }

  /**
   * Contains information about a location indicated by a place expression.
   *
   * @param placeVariable Basis variable that owns this location.
   * @param indexOperands Tuple indexOperands for the variable that should be updated.
   * @param outContext Produced context.
   */
  record PlaceResult(
      TypedAst.VariableId placeVariable,
      ArrayList<IndexAndSize> indexOperands,
      Context outContext) {}

  /** Individual part of an indexing path. */
  record IndexAndSize(Operand.Basic index, int elementTypeSize) {}

  /**
   * Determines how to update location indicated by the given place expression.
   *
   * @param uncheckedExpression Expression to translate
   * @param insnAccum Instruction accumulator.
   * @param ctx Context to translate in.
   * @param tmpName TmpName.
   * @param env Environments.
   * @return Place result indicating the operand pointed to by the place. Owned by callee.
   */
  static PlaceResult translatePlace(
      final TypedAst.TypedExpression uncheckedExpression,
      final InstructionAccum insnAccum,
      final Context ctx,
      final TmpName tmpName,
      final Environments env) {

    // Base variable
    if (uncheckedExpression.expr() instanceof TypedAst.Expression.LocalVariable expr) {
      return new PlaceResult(expr.variableIdentifier(), new ArrayList<>(), ctx);

      // Index operation
    } else if (uncheckedExpression.expr() instanceof TypedAst.Expression.Binary expr
        && ZkAst.Binop.INDEX.equals(expr.operation())) {
      final PlaceResult placeResult = translatePlace(expr.expr1(), insnAccum, ctx, tmpName, env);
      final ExprResult indexResult =
          translateExpression(expr.expr2(), insnAccum, placeResult.outContext(), tmpName, env);
      final int elementTypeSize =
          IrAmalgamUtility.operandTypeBitwidth(
              translateType(uncheckedExpression.resultType(), env));

      placeResult
          .indexOperands()
          .add(new IndexAndSize(indexResult.operand().expectBasic(), elementTypeSize));
      return new PlaceResult(
          placeResult.placeVariable(), placeResult.indexOperands(), indexResult.outContext());

      // Tuple access
    } else if (uncheckedExpression.expr() instanceof TypedAst.Expression.TupleAccess expr) {
      final PlaceResult result = translatePlace(expr.tupleExpr(), insnAccum, ctx, tmpName, env);
      final int elementTypeSize =
          IrAmalgamUtility.operandTypeBitwidth(
              translateType(uncheckedExpression.resultType(), env));
      result
          .indexOperands()
          .add(
              new IndexAndSize(
                  new Operand.Constant(expr.index(), Ir.Type.I32, tmpName), elementTypeSize));
      return result;

      // Struct field access
    } else if (uncheckedExpression.expr() instanceof TypedAst.Expression.FieldAccess expr) {
      final PlaceResult result = translatePlace(expr.structExpr(), insnAccum, ctx, tmpName, env);

      // Determine index to extract
      final TypedAst.Type.NamedType structType =
          (TypedAst.Type.NamedType) expr.structExpr().resultType();
      final TypeLayout typeLayout = translateStructDefinition(structType, env);
      final int fieldIndex = typeLayout.fieldPlacement().indexOf(expr.fieldName());
      final int elementTypeSize =
          IrAmalgamUtility.operandTypeBitwidth(
              translateType(uncheckedExpression.resultType(), env));
      result
          .indexOperands()
          .add(
              new IndexAndSize(
                  new Operand.Constant(fieldIndex, Ir.Type.I32, tmpName), elementTypeSize));
      return result;
    } else {
      throw new UnsupportedOperationException(
          "translatePlace not implemented for "
              + uncheckedExpression.expr().getClass().getSimpleName()
              + ". Assign structures to variables before assignment.");
    }
  }

  /**
   * Determines the from context and target block to perform a jump for, before emitting the jump.
   * Leaves the instruction accumulator outside of a block.
   *
   * @param insnAccum Instruction accumulator.
   * @param ctx The current context.
   * @param contextBlockId Function to determine whether the desired context have been found.
   *     Designed to work with {@link Context#getLoopIdEnd} and {@link Context#getLoopIdNext}.
   * @param parentContextOfTarget Whether to jump to the parent context.
   * @return The jumping-from context.
   */
  private static Context emitContextBranch(
      final InstructionAccum insnAccum,
      final Context ctx,
      final Function<Context, Ir.BlockId> contextBlockId,
      final boolean parentContextOfTarget) {

    // Determine context to branch from, and where to
    Context contextToBranchFrom = ctx;
    while (contextBlockId.apply(contextToBranchFrom) == null) {
      contextToBranchFrom = contextToBranchFrom.parentContext();
    }
    final Ir.BlockId blockToBranchTo = contextBlockId.apply(contextToBranchFrom);
    if (parentContextOfTarget) {
      contextToBranchFrom = contextToBranchFrom.parentContext();
    }

    // Create branch
    final BranchEnvInfo branchingInfo = prepareForBranching(insnAccum, contextToBranchFrom);

    insnAccum.addTerminator(
        new Ir.BranchAlways(new Ir.BranchInfo(blockToBranchTo, branchingInfo.arguments())));

    return ctx;
  }

  /**
   * Emits lookup instruction sequence for the given location expression.
   *
   * @param uncheckedExpression Expression to use as location expression.
   * @param insnAccum Instruction accumulator.
   * @param ctx Context to translate in.
   * @param tmpName Temporary name generator.
   * @param env Environment to use.
   * @return Expression result
   */
  private static ExprResult translatePlaceLookup(
      final TypedAst.TypedExpression uncheckedExpression,
      final InstructionAccum insnAccum,
      final Context ctx,
      final TmpName tmpName,
      final Environments env) {

    final PlaceResult placeResult =
        translatePlace(uncheckedExpression, insnAccum, ctx, tmpName, env);
    final TypedAst.VariableId placeVariable = placeResult.placeVariable();

    final OperandType outputType = translateType(uncheckedExpression.resultType(), env);

    final Operand result =
        getOperandByPath(
            outputType, ctx.get(placeVariable), placeResult.indexOperands(), insnAccum, tmpName);
    return new ExprResult(result, placeResult.outContext());
  }

  private static Operand.Basic translateBinop(
      final InstructionAccum insnAccum,
      final BinopDef opInfo,
      final Operand operand1,
      final Operand operand2,
      final TmpName tmpName) {

    final Ir.Type resultType =
        opInfo.isComparison()
            ? IrAmalgamUtility.comparisonResultType(operand1)
            : operand1.expectBasic().type();

    final Operand.Basic operandResult;

    // Equality special case
    if (opInfo.operation().equals(Ir.Operation.BinaryOp.EQUAL)) {
      operandResult =
          translateStructuralEquality(insnAccum, resultType, operand1, operand2, tmpName);

      // Equality special case
    } else if (opInfo.operation().equals(Ir.Operation.BinaryOp.SUBTRACT_WRAPPING)) {
      final Operand.Basic operand2Negated =
          insnAccum.addUnop(
              resultType, tmpName, Ir.Operation.UnaryOp.NEGATE, operand2.expectBasic());

      operandResult =
          insnAccum.addBinop(
              resultType,
              tmpName,
              Ir.Operation.BinaryOp.ADD_WRAPPING,
              operand1.expectBasic(),
              operand2Negated);

      // General case
    } else {
      operandResult =
          insnAccum.addBinop(
              resultType,
              tmpName,
              opInfo.operation(),
              operand1.expectBasic(),
              operand2.expectBasic());
    }

    // Negate operation if applicable
    return !opInfo.negateOperation()
        ? operandResult
        : insnAccum.addUnop(resultType, tmpName, Ir.Operation.UnaryOp.BITWISE_NOT, operandResult);
  }

  /**
   * Produces a instruction sequence for comparing the values represented by the two operands.
   *
   * @param insnAccum Accumulator for instructions.
   * @param resultType Expected result type.
   * @param operand1 First operand to compare.
   * @param operand2 Second operand to compare.
   * @param tmpName Naming info for generated temporary variables.
   * @return Basic operand representing the result of the comparison.
   * @exception InternalCompilerException If the operands are of inequal types.
   */
  static Operand.Basic translateStructuralEquality(
      final InstructionAccum insnAccum,
      final Ir.Type resultType,
      final Operand operand1,
      final Operand operand2,
      final TmpName tmpName) {

    assertSameStructure(operand1, operand2);

    final List<Operand.Basic> baseOperands1 = operand1.baseOperands();
    final List<Operand.Basic> baseOperands2 = operand2.baseOperands();

    Operand.Basic resultSoFar = new Operand.Constant(1, resultType, tmpName);

    for (int idx = 0; idx < baseOperands1.size(); idx++) {
      final Operand.Basic eqResult =
          insnAccum.addBinop(
              resultType,
              tmpName,
              Ir.Operation.BinaryOp.EQUAL,
              baseOperands1.get(idx),
              baseOperands2.get(idx));
      if (idx == 0) {
        resultSoFar = eqResult;
      } else {
        resultSoFar =
            insnAccum.addBinop(
                resultType, tmpName, Ir.Operation.BinaryOp.BITWISE_AND, eqResult, resultSoFar);
      }
    }

    return resultSoFar;
  }

  private static ExprResult translateTupleExpression(
      final TypedAst.Expression.TupleConstructor expr,
      final InstructionAccum insnAccum,
      final Context ctx,
      final TmpName tmpName,
      final Environments env) {

    final ExprResults subExpressionResults =
        translateExpressions(expr.exprs(), insnAccum, ctx, tmpName, env);
    return new ExprResult(
        new Operand.Tuple(subExpressionResults.operands()), subExpressionResults.outContext());
  }

  /**
   * Contains information for how to carry the Rust-level variable environment into a IR-level
   * block.
   */
  record BranchEnvInfo(List<Ir.Input> inputs, List<Ir.VariableId> arguments, Context context) {

    /**
     * Constructor.
     *
     * <ul>
     *   <li>inputs: Input definitions for the new block.
     *   <li>arguments: Variables that the newly created block should be called with.
     *   <li>context: The compilation context that the body of the block should be compiled in.
     * </ul>
     */
    public BranchEnvInfo {
      requireNonNull(inputs);
      requireNonNull(arguments);
      requireNonNull(context);
    }
  }

  /**
   * Emits instructions for bridging a Rust-level variable environment into a new IR-level block,
   * and returns information on how the environment is structured.
   *
   * <p>Handles both the branching and the merging stages of compiling an if-expression.
   *
   * @param insnAccum Accumulator for instructions.
   * @param ctx Current context right before branching.
   */
  private static BranchEnvInfo prepareForBranching(
      final InstructionAccum insnAccum, final Context ctx) {
    final var branchingInputs = new ArrayList<Ir.Input>();
    final var branchingArguments = new ArrayList<Ir.VariableId>();

    final Context branchingCtx =
        ctx.mapAllVariablesInAllScopes(
            operand ->
                translateBranchJump(insnAccum, branchingInputs, branchingArguments, operand));

    return new BranchEnvInfo(
        List.copyOf(branchingInputs), List.copyOf(branchingArguments), branchingCtx);
  }

  private static ExprResult newUnreachableBlock(
      final InstructionAccum insnAccum, final Context ctx, final TmpName tmpName) {
    final Ir.BlockId blockIdUnreachable = insnAccum.newBlockId("unreachable_after_branch");
    insnAccum.newBlock(blockIdUnreachable, List.of(), false);
    final Context zeroEnvironment =
        ctx.mapAllVariablesInAllScopes(o -> IrAmalgamUtility.zeroOperand(o.operandType(), tmpName));
    return new ExprResult(Operand.Tuple.UNIT, zeroEnvironment);
  }

  /**
   * Emits instructions for bridging a single operand into a new IR-level block, and returns a new
   * operand in the new block.
   *
   * @param insnAccum Accumulator for instructions.
   * @param inputAccum Accumulator for inputs for the new block.
   * @param argumentAccum Accumulator for argument to the branch info.
   * @param uncheckedOperand Operand to bridge into the new block.
   * @return Newly created operand in the new block's context.
   */
  private static Operand translateBranchJump(
      final InstructionAccum insnAccum,
      final List<Ir.Input> inputAccum,
      final List<Ir.VariableId> argumentAccum,
      final Operand uncheckedOperand) {
    // Tuple
    if (uncheckedOperand instanceof Operand.Tuple operand) {
      return new Operand.Tuple(
          operand.componentOperands().stream()
              .map(o -> translateBranchJump(insnAccum, inputAccum, argumentAccum, o))
              .toList());

      // Array
    } else if (uncheckedOperand instanceof Operand.Array operand) {
      final Operand representation =
          translateBranchJump(
              insnAccum, inputAccum, argumentAccum, operand.underlyingRepresentation());
      return new Operand.Array((Operand.Basic) representation, operand.operandType());

      // Other basics
    } else {
      final Operand.Basic operand = (Operand.Basic) uncheckedOperand;
      final Ir.VariableId operandVar = insnAccum.addIdentity(operand);
      final Ir.VariableId idInBranching = insnAccum.newVariableId(TmpName.root(operandVar.name()));
      inputAccum.add(new Ir.Input(idInBranching, operand.type()));
      argumentAccum.add(operandVar);
      return new Operand.Variable(idInBranching, operand.type());
    }
  }

  /**
   * Translates an if-expression.
   *
   * @param expr Expression to translate.
   * @param insnAccum Accumulator for instructions.
   * @param outerContext Current context right before entering the expression.
   * @param tmpName Naming info for generated temporary variables.
   * @return Expression result
   */
  private static ExprResult translateIfExpression(
      final TypedAst.Expression.If expr,
      final InstructionAccum insnAccum,
      final Context outerContext,
      final TmpName tmpName,
      final Environments env) {

    final Context ifContext = outerContext.childContext(null, null);

    // Translate condition
    final ExprResult exprResultCond =
        translateExpression(expr.condExpr(), insnAccum, ifContext, tmpName.with("cond"), env);
    final Operand.Basic exprResultCondOperand = exprResultCond.operand().expectBasic();

    // Determine which variables should be carried into the branches.
    final BranchEnvInfo branchingInfoForThen =
        prepareForBranching(insnAccum, exprResultCond.outContext());
    final BranchEnvInfo branchingInfoForElse =
        prepareForBranching(insnAccum, exprResultCond.outContext());

    // Add branch
    final Ir.BlockId blockIdThen = insnAccum.newBlockId("then");
    final Ir.BlockId blockIdElse = insnAccum.newBlockId("else");
    final Ir.BlockId blockIdMerge = insnAccum.newBlockId("merge");

    translateBranchIf(
        insnAccum,
        exprResultCondOperand,
        blockIdThen,
        branchingInfoForThen,
        blockIdElse,
        branchingInfoForElse);

    // Translate 'then' branch
    insnAccum.newBlock(blockIdThen, branchingInfoForThen.inputs(), false);
    final ExprResult exprResultThen =
        translateExpression(
            expr.thenExpr(), insnAccum, branchingInfoForThen.context(), tmpName.with("then"), env);

    final TemporaryVariableId ifResultId = exprResultThen.outContext().newTempId();

    final BranchEnvInfo branchingInfoAfterThen =
        prepareForBranching(
            insnAccum, exprResultThen.outContext().define(ifResultId, exprResultThen.operand()));

    insnAccum.addTerminator(
        new Ir.BranchAlways(new Ir.BranchInfo(blockIdMerge, branchingInfoAfterThen.arguments())));

    // Translate 'else' branch
    insnAccum.newBlock(blockIdElse, branchingInfoForElse.inputs(), false);
    final ExprResult exprResultElse =
        translateExpression(
            expr.elseExpr(), insnAccum, branchingInfoForElse.context(), tmpName.with("else"), env);

    final BranchEnvInfo branchingInfoAfterElse =
        prepareForBranching(
            insnAccum, exprResultElse.outContext().define(ifResultId, exprResultElse.operand()));

    insnAccum.addTerminator(
        new Ir.BranchAlways(new Ir.BranchInfo(blockIdMerge, branchingInfoAfterElse.arguments())));

    // Resume with the 'merge' block
    insnAccum.newBlock(blockIdMerge, branchingInfoAfterThen.inputs(), false);
    return new ExprResult(
        branchingInfoAfterThen.context.get(ifResultId),
        branchingInfoAfterThen.context.parentContext());
  }

  private static void translateBranchIf(
      final InstructionAccum insnAccum,
      final Operand.Basic operCondition,
      final Ir.BlockId blockIdThen,
      final BranchEnvInfo branchingInfoForThen,
      final Ir.BlockId blockIdElse,
      final BranchEnvInfo branchingInfoForElse) {
    insnAccum.addTerminator(
        new Ir.BranchIf(
            operCondition.type() instanceof Ir.Type.SecretBinaryInteger,
            insnAccum.addIdentity(operCondition),
            new Ir.BranchInfo(blockIdThen, branchingInfoForThen.arguments()),
            new Ir.BranchInfo(blockIdElse, branchingInfoForElse.arguments())));
  }

  /**
   * Translates an for-in-range-expression.
   *
   * @param forInExpr Expression to translate.
   * @param insnAccum Accumulator for instructions.
   * @param outerContext Current context right before entering the expression.
   * @param env Environment to translate within.
   * @return Expression result
   */
  private static ExprResult translateForInExpression(
      final TypedAst.Expression.ForInRange forInExpr,
      final InstructionAccum insnAccum,
      final Context outerContext,
      final Environments env) {

    // Determine functions for iteration
    final BuiltinFunctions.FnDefinition iterableIntoIterator =
        BuiltinFunctions.getFunction(forInExpr.methodIdIntoIterator());
    final BuiltinFunctions.FnDefinition iteratorNext =
        BuiltinFunctions.getFunction(forInExpr.methodIdNext());

    // Block ids
    final Ir.BlockId blockIdNext = insnAccum.newBlockId("next");
    final Ir.BlockId blockIdBody = insnAccum.newBlockId("body");
    final Ir.BlockId blockIdAfter = insnAccum.newBlockId("after");

    final TypedAst.VariableId iteratorVariableIdentifier = forInExpr.iteratorVariableIdentifier();
    final TmpName tmpNameIterator = TmpName.root(iteratorVariableIdentifier.last());

    // Into iterator block
    final ExprResult exprResultIteratedOver =
        translateExpression(
            forInExpr.intoIteratorExpr(), insnAccum, outerContext, tmpNameIterator, env);

    final Operand operandIterator =
        iterableIntoIterator
            .callGenerator()
            .emit(insnAccum, tmpNameIterator, List.of(exprResultIteratedOver.operand()), null);

    final Context loopContext1 =
        exprResultIteratedOver.outContext().childContext(blockIdNext, blockIdAfter);
    final TemporaryVariableId tmpIdIterator = loopContext1.newTempId();
    final TemporaryVariableId tmpIdOutputElement = loopContext1.newTempId();

    final Context loopContext = loopContext1.define(tmpIdIterator, operandIterator);

    final BranchEnvInfo branchingInfoBeforeToNext = prepareForBranching(insnAccum, loopContext);

    // Branch from into_iterator to cond
    // (branch-always #cond ...))
    insnAccum.addTerminator(
        new Ir.BranchAlways(new Ir.BranchInfo(blockIdNext, branchingInfoBeforeToNext.arguments())));

    // Translate cond
    // (block #cond ...
    insnAccum.newBlock(blockIdNext, branchingInfoBeforeToNext.inputs(), false);

    // Translate comparison
    // (i1 $cond (equals $iter $range_high))
    final List<Operand> operandNextResult =
        iteratorNext
            .callGenerator()
            .emit(
                insnAccum,
                tmpNameIterator,
                List.of(branchingInfoBeforeToNext.context().get(tmpIdIterator)),
                null)
            .expectTuple();

    final Operand.Basic operandShouldContinue = operandNextResult.get(1).expectBasic();

    // Branch to body or after
    // (branch-if $cond (0 #after ...) (1 #body ...)))
    final BranchEnvInfo branchingInfoNextToBody =
        prepareForBranching(
            insnAccum,
            branchingInfoBeforeToNext
                .context()
                .define(tmpIdIterator, operandNextResult.get(0))
                .childContext(null, null)
                .define(tmpIdOutputElement, operandNextResult.get(2)));
    final BranchEnvInfo branchingInfoNextToAfter =
        prepareForBranching(insnAccum, branchingInfoBeforeToNext.context().parentContext());

    translateBranchIf(
        insnAccum,
        operandShouldContinue,
        blockIdBody,
        branchingInfoNextToBody,
        blockIdAfter,
        branchingInfoNextToAfter);

    // Translate body
    // (block #body ...
    insnAccum.newBlock(blockIdBody, branchingInfoNextToBody.inputs(), false);

    final ExprResult exprResultBody =
        translateExpression(
            forInExpr.bodyExpr(),
            insnAccum,
            branchingInfoNextToBody
                .context()
                .childContext(null, null)
                .define(
                    iteratorVariableIdentifier,
                    branchingInfoNextToBody.context().get(tmpIdOutputElement)),
            TmpName.root("for_body"),
            env);

    emitContextBranch(insnAccum, exprResultBody.outContext(), Context::getLoopIdNext, false);

    // Translate after
    // (block #after ...
    insnAccum.newBlock(blockIdAfter, branchingInfoNextToAfter.inputs(), false);
    return new ExprResult(Operand.Tuple.UNIT, branchingInfoNextToAfter.context());
  }

  private static ExprResult translateBlock(
      final TypedAst.Expression.Block block,
      final InstructionAccum insnAccum,
      final Context oldCtx,
      final TmpName tmpName,
      final Environments env) {

    Context ctx = oldCtx.childContext(null, null);

    for (final TypedAst.Statement uncheckedStatement : block.statements()) {
      // Expression statement
      if (uncheckedStatement instanceof TypedAst.Statement.Expr statement) {

        ctx =
            translateExpression(statement.expr(), insnAccum, ctx, TmpName.root("ø"), env)
                .outContext();

        // Let statement
      } else if (uncheckedStatement instanceof TypedAst.Statement.Let statement) {

        final ExprResult result =
            translateExpression(
                statement.initExpr(),
                insnAccum,
                ctx,
                TmpName.root(statement.identifier().last()),
                env);

        ctx = result.outContext().define(statement.identifier(), result.operand());

        // Return statement
      } else {
        final TypedAst.Statement.Return statement = (TypedAst.Statement.Return) uncheckedStatement;

        // Translate return expression
        final ExprResult result =
            translateExpression(
                statement.resultExpr(), insnAccum, ctx, TmpName.root("return"), env);

        final BranchEnvInfo branchingInfoForUnreachable =
            prepareForBranching(insnAccum, result.outContext());

        // Branch to end
        translateReturnToExternal(insnAccum, result.operand(), statement.resultExpr().resultType());

        // Create new block to contain unreachable code
        final Ir.BlockId blockIdUnreachable = insnAccum.newBlockId("unreachable_after_return");
        insnAccum.newBlock(blockIdUnreachable, branchingInfoForUnreachable.inputs(), false);

        ctx = branchingInfoForUnreachable.context();
      }
    }

    // Result operand. Remove assignments
    final ExprResult exprResult =
        translateExpression(block.resultExpr(), insnAccum, ctx, tmpName, env);
    return new ExprResult(exprResult.operand(), exprResult.outContext().parentContext());
  }

  /**
   * Emits a sequence of instructions in order to return from an publicly-exposed function.
   *
   * @param insnAccum Accumulator for instructions.
   * @param returnOperand Return operand containing return data.
   * @param returnType Type of return data.
   */
  private static void translateReturnToExternal(
      final InstructionAccum insnAccum,
      final Operand returnOperand,
      final TypedAst.Type returnType) {

    // Determine operands to return
    final List<Operand> subOperands;
    if (TypedAst.Type.Tuple.UNIT.equals(returnType)) {
      subOperands = List.of();
    } else if (returnType instanceof TypedAst.Type.Tuple) {
      subOperands = ((Operand.Tuple) returnOperand).componentOperands();
    } else {
      subOperands = List.of(returnOperand);
    }

    // Determine variables to branch with
    final List<Ir.VariableId> resultVariables = new ArrayList<>();
    for (final Operand oper : subOperands) {
      final Operand.Basic operandPacked =
          IrAmalgamUtility.emitAmalgamPacking(oper, insnAccum, Ir.Type.UNIT);
      resultVariables.add(insnAccum.addIdentity(operandPacked));
    }

    insnAccum.addTerminator(
        new Ir.BranchAlways(new Ir.BranchInfo(Ir.BlockId.RETURN, List.copyOf(resultVariables))));
  }

  /** Translates call expressions. */
  static ExprResult translateCallExpression(
      final TypedAst.Expression.CallDirectly expr,
      final TypedAst.Type resultType,
      final InstructionAccum insnAccum,
      final Context ctx,
      final TmpName tmpName,
      final Environments env) {

    // Translate parameter expressions
    final ExprResults parameterExprResults =
        translateExpressions(
            expr.argumentExprs(), insnAccum, ctx, tmpName.with(expr.functionId().last()), env);

    // Check if builtin function
    final BuiltinFunctions.FnDefinition builtinDefinition =
        BuiltinFunctions.getFunction(expr.functionId());

    // Builtin functions
    if (builtinDefinition != null) {
      ExprResult result =
          translateCallExpressionBuiltin(
              insnAccum, tmpName, builtinDefinition, parameterExprResults, resultType, env);

      if (builtinDefinition.modifyFirstArgument()) {
        final Operand.Tuple resultWithArgumentModification = (Operand.Tuple) result.operand();

        // Overwrite the original first argument
        final ExprResult assignResult =
            translateAssignExpression(
                expr.argumentExprs().get(0),
                resultWithArgumentModification.componentOperands().get(0),
                insnAccum,
                result.outContext(),
                env);

        return new ExprResult(
            resultWithArgumentModification.componentOperands().get(1), assignResult.outContext());
      } else {
        return result;
      }

      // Programmer defined functions
    } else {
      final List<Ir.Type> resultOperandType = translateTypeReturnToExternal(resultType, env);
      if (resultOperandType.size() > 1) {
        throw new UnsupportedOperationException(
            "Calls with tuple return types: " + resultOperandType);
      }

      final Ir.Type irResultType =
          resultOperandType.isEmpty() ? Ir.Type.UNIT : resultOperandType.get(0);
      final Ir.FunctionId functionId = env.functionIdentifiers().get(expr.functionId());

      // Current call protocol requires all operands to be packed.
      final List<Operand.Basic> packedArguments =
          parameterExprResults.operands().stream()
              .map(o -> IrAmalgamUtility.emitAmalgamPacking(o, insnAccum, Ir.Type.UNIT))
              .toList();

      final Operand.Basic resultOperandAmalgam =
          insnAccum.addCall(
              irResultType, tmpName, functionId, packedArguments, resultOperandType.size());

      final Operand resultOperand =
          IrAmalgamUtility.emitAmalgamUnpacking(
              translateType(resultType, env), resultOperandAmalgam, insnAccum, tmpName);

      return new ExprResult(resultOperand, parameterExprResults.outContext());
    }
  }

  /** Translates builtin calls. */
  private static ExprResult translateCallExpressionBuiltin(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final BuiltinFunctions.FnDefinition builtinDefinition,
      final ExprResults parameterExprResults,
      final TypedAst.Type resultType,
      final Environments env) {

    final OperandType resultOperandType = translateType(resultType, env);

    // Output bodies
    final Operand resultOperand =
        builtinDefinition
            .callGenerator()
            .emit(insnAccum, tmpName, parameterExprResults.operands(), resultOperandType);
    return new ExprResult(resultOperand, parameterExprResults.outContext());
  }

  static ExprResult translateStructConstructorExpression(
      final TypedAst.Expression.StructConstructor expr,
      final InstructionAccum insnAccum,
      final Context ctx,
      final TmpName tmpName,
      final Environments env) {

    // Translate input expressions
    final ExprResults inputInitExprResults =
        translateExpressions(
            expr.fieldInits().stream().map(x -> x.fieldInitExpr()).toList(),
            insnAccum,
            ctx,
            tmpName,
            env);

    // Determine field names to operands.
    final var fieldNamesToOperands = new HashMap<String, Operand>();
    for (int idx = 0; idx < expr.fieldInits().size(); idx++) {
      final String name = expr.fieldInits().get(idx).fieldName();
      final Operand operand = inputInitExprResults.operands().get(idx);
      fieldNamesToOperands.put(name, operand);
    }

    final TypeLayout typeLayout = translateStructDefinition(expr.structName(), env);

    final Operand resultOperand =
        new Operand.Tuple(
            typeLayout.fieldPlacement().stream().map(fieldNamesToOperands::get).toList());

    // Output bodies
    return new ExprResult(resultOperand, inputInitExprResults.outContext());
  }

  private static ExprResults translateExpressions(
      final List<TypedAst.TypedExpression> exprs,
      final InstructionAccum insnAccum,
      final Context initialContext,
      final TmpName tmpName,
      final Environments env) {
    final var tmpNames =
        IntStream.range(0, exprs.size()).mapToObj(Integer::toString).map(tmpName::with).toList();
    return translateExpressions(exprs, insnAccum, initialContext, tmpNames, env);
  }

  /**
   * Translates expressions to execute in a sequence from first to last element. Accounts for
   * maintaining the operand value across block boundries; this is preferable to using {@link
   * translateExpression} by hand.
   *
   * @param exprs Expressions to translate.
   * @param insnAccum Instruction accumulator.
   * @param initialContext Context used to translate the first expression in the sequence.
   * @param tmpNames Temporary names for each individual element in the {@code exprs} list.
   * @param env Environments to translate in.
   * @return Expression result container.
   */
  private static ExprResults translateExpressions(
      final List<TypedAst.TypedExpression> exprs,
      final InstructionAccum insnAccum,
      final Context initialContext,
      final List<TmpName> tmpNames,
      final Environments env) {

    Context ctx = initialContext.childContext(null, null);

    final var exprResultsIds = new ArrayList<TemporaryVariableId>();
    for (int exprIdx = 0; exprIdx < exprs.size(); exprIdx++) {
      final TypedAst.TypedExpression expr = exprs.get(exprIdx);
      final ExprResult exprResult =
          translateExpression(expr, insnAccum, ctx, tmpNames.get(exprIdx), env);

      final TemporaryVariableId resultOperandId = exprResult.outContext().newTempId();

      ctx = exprResult.outContext().define(resultOperandId, exprResult.operand());
      exprResultsIds.add(resultOperandId);
    }

    final List<Operand> exprResultOperands = exprResultsIds.stream().map(ctx::get).toList();
    return new ExprResults(exprResultOperands, ctx.parentContext());
  }

  /** Unary operation mapping from {@link ZkAst} to {@link Ir.Operation}. */
  private static final Map<ZkAst.Unop, Ir.Operation.UnaryOp> UNOP_MAPPING =
      Map.ofEntries(
          entry(ZkAst.Unop.BITWISE_NOT, Ir.Operation.UnaryOp.BITWISE_NOT),
          entry(ZkAst.Unop.NEGATE, Ir.Operation.UnaryOp.NEGATE));

  //// Binary operands

  /**
   * Determines which binary operation is being used for the given binop and type. Selects between
   * signed and unsigned versions of operations.
   *
   * @param binop Binop being used.
   * @param argumentType Argument type of operands.
   * @return Definition of operation.
   */
  private static BinopDef determineBinop(
      final ZkAst.Binop binop, final TypedAst.Type argumentType) {
    // Determine signeness
    final boolean isSigned = isSignedType(argumentType);

    // Check signedness map first
    final Map<ZkAst.Binop, BinopDef> signednessMap =
        isSigned ? BINOP_MAPPING_SIGNED : BINOP_MAPPING_UNSIGNED;
    if (signednessMap.containsKey(binop)) {
      return signednessMap.get(binop);
    }

    // Otherwise, try common binop mapping.
    if (BINOP_MAPPING_SIGN_AGNOSTIC.containsKey(binop)) {
      return BINOP_MAPPING_SIGN_AGNOSTIC.get(binop);
    }
    throw new UnsupportedOperationException("Unsupported binop: " + binop);
  }

  /**
   * Determines whether the given type indicates a type with a sign dimension.
   *
   * @param uncheckedType Type to determine for.
   * @return true if and only if the type is an integer with signedness. False if not an integer
   *     type.
   */
  static boolean isSignedType(TypedAst.Type uncheckedType) {
    if (uncheckedType instanceof TypedAst.Type.Int type) {
      return type.isSigned();
    } else if (uncheckedType instanceof TypedAst.Type.Sbi type) {
      return type.isSigned();
    } else {
      return false;
    }
  }

  /**
   * Contains declaration for an binary operation.
   *
   * @param operation Ir-Circuit Ir.Operation enum value to translate to.
   * @param negateOperation Some Rust-level operations are implemented by negating Ir-Circuit-level
   *     operations.
   * @param isComparison Whether the binop is a comparison, and thus that it should return booleans.
   */
  @SuppressWarnings("UnusedVariable")
  private record BinopDef(
      Ir.Operation.BinaryOp operation, boolean negateOperation, boolean isComparison) {}

  /** Contains common binops used on all integers, no matter their signedness. */
  private static final Map<ZkAst.Binop, BinopDef> BINOP_MAPPING_SIGN_AGNOSTIC =
      Map.ofEntries(
          // Bit operations
          entry(
              ZkAst.Binop.BITWISE_AND,
              new BinopDef(Ir.Operation.BinaryOp.BITWISE_AND, false, false)),
          entry(
              ZkAst.Binop.BITWISE_OR, new BinopDef(Ir.Operation.BinaryOp.BITWISE_OR, false, false)),
          entry(
              ZkAst.Binop.BITWISE_XOR,
              new BinopDef(Ir.Operation.BinaryOp.BITWISE_XOR, false, false)),
          entry(
              ZkAst.Binop.BITSHIFT_RIGHT,
              new BinopDef(Ir.Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL, false, false)),
          entry(
              ZkAst.Binop.BITSHIFT_LEFT,
              new BinopDef(Ir.Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL, false, false)),

          // Logical operations
          entry(
              ZkAst.Binop.LOGICAL_AND,
              new BinopDef(Ir.Operation.BinaryOp.BITWISE_AND, false, false)),
          entry(
              ZkAst.Binop.LOGICAL_OR, new BinopDef(Ir.Operation.BinaryOp.BITWISE_OR, false, false)),

          // Arithmetic operations
          entry(ZkAst.Binop.PLUS, new BinopDef(Ir.Operation.BinaryOp.ADD_WRAPPING, false, false)),
          entry(
              ZkAst.Binop.MINUS,
              new BinopDef(Ir.Operation.BinaryOp.SUBTRACT_WRAPPING, false, false)),

          // Relational operations
          entry(ZkAst.Binop.EQUAL, new BinopDef(Ir.Operation.BinaryOp.EQUAL, false, true)),
          entry(ZkAst.Binop.NOT_EQUAL, new BinopDef(Ir.Operation.BinaryOp.EQUAL, true, true)));

  /** Contains binops only relevant for signed integers. */
  private static final Map<ZkAst.Binop, BinopDef> BINOP_MAPPING_SIGNED =
      Map.ofEntries(
          // Arithmetic operations
          entry(
              ZkAst.Binop.MULT,
              new BinopDef(Ir.Operation.BinaryOp.MULT_SIGNED_WRAPPING, false, false)),

          // Relational operations

          entry(
              ZkAst.Binop.LESS_THAN,
              new BinopDef(Ir.Operation.BinaryOp.LESS_THAN_SIGNED, false, true)),
          entry(
              ZkAst.Binop.LESS_THAN_OR_EQUAL,
              new BinopDef(Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED, false, true)),
          entry(
              ZkAst.Binop.GREATER_THAN,
              new BinopDef(Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED, true, true)),
          entry(
              ZkAst.Binop.GREATER_THAN_OR_EQUAL,
              new BinopDef(Ir.Operation.BinaryOp.LESS_THAN_SIGNED, true, true)));

  /** Contains binops only relevant for unsigned integers. */
  private static final Map<ZkAst.Binop, BinopDef> BINOP_MAPPING_UNSIGNED =
      Map.ofEntries(
          // Arithmetic operations
          entry(
              ZkAst.Binop.MULT,
              new BinopDef(Ir.Operation.BinaryOp.MULT_UNSIGNED_WRAPPING, false, false)),

          // Relational operations
          entry(
              ZkAst.Binop.LESS_THAN,
              new BinopDef(Ir.Operation.BinaryOp.LESS_THAN_UNSIGNED, false, true)),
          entry(
              ZkAst.Binop.LESS_THAN_OR_EQUAL,
              new BinopDef(Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED, false, true)),
          entry(
              ZkAst.Binop.GREATER_THAN,
              new BinopDef(Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED, true, true)),
          entry(
              ZkAst.Binop.GREATER_THAN_OR_EQUAL,
              new BinopDef(Ir.Operation.BinaryOp.LESS_THAN_UNSIGNED, true, true)));

  //// Types

  /**
   * Translates individual type definitions, producing a type layout.
   *
   * @param structType Name of struct to translate, including type arguments.
   * @param env Translation environment.
   * @return Layout of the given type definition.
   */
  private static TypeLayout translateStructDefinition(
      final TypedAst.Type.NamedType structType, final Environments env) {

    final TypedAst.StructDefinition structDef = env.getStructDefinition(structType.typeId());

    final List<TypedAst.TypeId> typeParameterIds =
        structDef.typeParameterNames().stream()
            .map(List::of)
            .map(NamePath.Absolute::new)
            .map(TypedAst.TypeId::new)
            .toList();
    final TypeReplacementMap typeReplacementMap =
        TypeReplacementMap.from(typeParameterIds, structType.typeArguments());

    final List<OperandType> componentTypes = new ArrayList<>();
    final List<String> fields = new ArrayList<>();
    for (final TypedAst.StructDefinition.StructField field : structDef.fields()) {
      fields.add(field.fieldName());
      final TypedAst.Type fieldType =
          typeReplacementMap.replaceParametersInType(field.fieldTypeWithTypeParameters());
      componentTypes.add(translateType(fieldType, env));
    }

    return new TypeLayout(new OperandType.TupleType(componentTypes), List.copyOf(fields));
  }

  /**
   * Determines types of ir operands to return for a publicly-exposed function.
   *
   * @param uncheckedType Type to translate.
   * @param env Translation environment.
   * @return Operand type of the given type.
   */
  private static List<Ir.Type> translateTypeReturnToExternal(
      final TypedAst.Type uncheckedType, final Environments env) {

    List<TypedAst.Type> types = List.of(uncheckedType);
    // Tuple types
    if (uncheckedType instanceof TypedAst.Type.Tuple type) {
      types = type.subtypes();
    }

    //
    return types.stream()
        .map(x -> translateType(x, env))
        .map(t -> IrAmalgamUtility.typeAmalgam(t, Ir.Type.UNIT))
        .toList();
  }

  /**
   * Translates individual type, producing the operand type representing how the type is stored at
   * runtime.
   *
   * @param uncheckedType Type to translate.
   * @param env Translation environment.
   * @return Operand type of the given type.
   */
  private static OperandType translateType(
      final TypedAst.Type uncheckedType, final Environments env) {
    // Tuple types
    if (uncheckedType instanceof TypedAst.Type.Tuple type) {
      return new OperandType.TupleType(
          type.subtypes().stream().map(x -> translateType(x, env)).toList());

      // Struct types
    } else if (uncheckedType instanceof TypedAst.Type.NamedType type) {
      return translateStructDefinition(type, env).operandType();

      // Sbi types
    } else if (uncheckedType instanceof TypedAst.Type.Sbi type) {
      return new OperandType.BasicType(new Ir.Type.SecretBinaryInteger(type.bitwidth()));

      // Array types
    } else if (uncheckedType instanceof TypedAst.Type.Array type) {
      return new OperandType.ArrayType(translateType(type.elementType(), env), type.size());

      // Integer types
    } else {
      final TypedAst.Type.Int type = (TypedAst.Type.Int) uncheckedType;
      return new OperandType.BasicType(new Ir.Type.PublicInteger(type.bitwidth()));
    }
  }

  //// Utility

  record TemporaryVariableId(int rawId) {}

  /**
   * The Context tracks the mapping of variable names to operands by a method of recursively nested
   * maps. Each nesting represents a different scope; a new scope can be created with {@link
   * #childContext(Ir.BlockId, Ir.BlockId)}, while a scope can be exited with {@link
   * #parentContext()}.
   *
   * <p>The context can either be changed with {@link #define(TypedAst.VariableId, Operand)} or
   * {@link #assign(TypedAst.VariableId, Operand)}, with {@link #define(TypedAst.VariableId,
   * Operand)} simply adding new variables to a newly created context, while {@link
   * #assign(TypedAst.VariableId, Operand)} may modify parent contexts, creating new parents. These
   * methods invalidates the self argument, enforcing ownership of the context to the new context.
   */
  static final class Context {

    /**
     * Mapping from variable paths in {@link TypedAst} to {@link Ir} operands. Nulled when map is
     * updated, to invalidate old context and enforce ownership.
     */
    private Map<TypedAst.VariableId, Operand> variables;

    /**
     * Mapping from temporary variable ids to {@link Ir} operands. Nulled when map is updated, to
     * invalidate old context and enforce ownership.
     */
    private Map<TemporaryVariableId, Operand> hiddenVariables;

    private final Context parentContext;
    private int nextTemporaryVariableId = 0;
    private final Ir.BlockId loopIdNext;
    private final Ir.BlockId loopIdEnd;

    private Context(
        final Map<TypedAst.VariableId, Operand> variables,
        final Map<TemporaryVariableId, Operand> hiddenVariables,
        final Context parentContext,
        final int nextTemporaryVariableId,
        final Ir.BlockId loopIdNext,
        final Ir.BlockId loopIdEnd) {
      this.variables = requireNonNull(variables);
      this.hiddenVariables = requireNonNull(hiddenVariables);
      this.parentContext = parentContext;
      this.nextTemporaryVariableId = nextTemporaryVariableId;
      this.loopIdNext = loopIdNext;
      this.loopIdEnd = loopIdEnd;
    }

    /**
     * Produces new empty context.
     *
     * @return Newly created empty context.
     */
    @CheckReturnValue
    public static Context empty() {
      return fromInputs(List.of(), List.of());
    }

    /**
     * Creates a new child context, with the current context as parent.
     *
     * <p>The context this method is called on should not be called afterwards.
     *
     * @return Newly created child context.
     */
    @CheckReturnValue
    public Context childContext(final Ir.BlockId loopIdNext, final Ir.BlockId loopIdEnd) {
      return new Context(
          new LinkedHashMap<>(),
          new LinkedHashMap<>(),
          this,
          nextTemporaryVariableId,
          loopIdNext,
          loopIdEnd);
    }

    /**
     * Fetches the parent context of this context.
     *
     * @return Parent context.
     */
    @CheckReturnValue
    public Context parentContext() {
      return parentContext;
    }

    /**
     * Defines a new Rust-level variable into a new updated context.
     *
     * <p>This method is linear and the called {@link Context} will be {@link invalidate}d.
     *
     * @param variableName Name of the Rust-level variable.
     * @param result Operand to assign to variable.
     * @return New updated context.
     */
    @CheckReturnValue
    public Context define(final TypedAst.VariableId variableName, final Operand result) {
      final Map<TypedAst.VariableId, Operand> variables = this.variables;
      variables.put(variableName, result);
      final Context newContext =
          new Context(
              variables,
              this.hiddenVariables,
              parentContext,
              nextTemporaryVariableId,
              loopIdNext,
              loopIdEnd);
      invalidate();
      return newContext;
    }

    /**
     * Defines a new hidden-from-Rust temporary variable into a new updated context.
     *
     * <p>This method is linear and the called {@link Context} will be {@link invalidate}d.
     *
     * @param variableId Id of the temporary variable.
     * @param result Operand to assign to variable.
     * @return New updated context.
     */
    @CheckReturnValue
    public Context define(final TemporaryVariableId variableId, final Operand result) {
      final Map<TemporaryVariableId, Operand> hiddenVariables = this.hiddenVariables;
      hiddenVariables.put(variableId, result);
      final Context newContext =
          new Context(
              this.variables,
              hiddenVariables,
              parentContext,
              nextTemporaryVariableId,
              loopIdNext,
              loopIdEnd);
      invalidate();
      return newContext;
    }

    /** Invalidate this context, ensuring that further uses will result in an error. */
    private void invalidate() {
      this.variables = null;
      this.hiddenVariables = null;
    }

    /**
     * Creates a new temporary variable, with id generated from this context.
     *
     * @return New temporary variable id.
     */
    @CheckReturnValue
    public TemporaryVariableId newTempId() {
      return new TemporaryVariableId(nextTemporaryVariableId++);
    }

    /**
     * Assigns to the given Rust-level variable into a new updated context.
     *
     * <p>This method is linear and the called {@link Context} will be {@link invalidate}d.
     *
     * @param variableName Name of the Rust-level variable.
     * @param result Operand to assign to variable.
     * @return New updated context.
     */
    @CheckReturnValue
    public Context assign(final TypedAst.VariableId variableName, final Operand result) {
      final Operand variableOperand = variables.get(variableName);
      if (variableOperand != null) {
        return define(variableName, result);
      }
      final var newContext =
          new Context(
              variables,
              this.hiddenVariables,
              parentContext.assign(variableName, result),
              nextTemporaryVariableId,
              loopIdNext,
              loopIdEnd);
      invalidate();
      return newContext;
    }

    /**
     * Retrieves the operand assigned to the Rust-level variable name in the context or in the
     * parent context stack.
     *
     * @param variableName Name of the Rust-level variable.
     * @return Assigned operand. Null if never defined.
     */
    @CheckReturnValue
    public Operand get(final TypedAst.VariableId variableName) {
      final Operand variableOperand = variables.getOrDefault(variableName, null);
      if (variableOperand != null) {
        return variableOperand;
      } else if (parentContext != null) {
        return parentContext.get(variableName);
      } else {
        return null;
      }
    }

    /**
     * Retrieves the operand assigned to the temporary variable in the context or in the parent
     * context stack.
     *
     * @param variableId Temporary variable id.
     * @return Assigned operand.
     */
    @CheckReturnValue
    public Operand get(final TemporaryVariableId variableId) {
      return hiddenVariables.get(variableId);
    }

    /**
     * Retrieves the block id for the loop next iteration block.
     *
     * @return Block id or null.
     */
    @CheckReturnValue
    public Ir.BlockId getLoopIdNext() {
      return loopIdNext;
    }

    /**
     * Retrieves the block id for the loop end iteration block.
     *
     * @return Block id or null.
     */
    @CheckReturnValue
    public Ir.BlockId getLoopIdEnd() {
      return loopIdEnd;
    }

    /**
     * Creates a new context based on the list of block inputs.
     *
     * @param variableIds Parameter information.
     * @param inputAmalgams Input amalgams to associate with each parameter.
     * @return Newly created context.
     */
    @CheckReturnValue
    public static Context fromInputs(
        final List<TypedAst.Parameter> variableIds, List<Operand> inputAmalgams) {

      final var variables = new LinkedHashMap<TypedAst.VariableId, Operand>();

      for (int idx = 0; idx < variableIds.size(); idx++) {
        variables.put(variableIds.get(idx).identifier(), inputAmalgams.get(idx));
      }

      return new Context(variables, new LinkedHashMap<>(), null, 0, null, null);
    }

    /**
     * Transforms all variable in the context by applying the given mapping function.
     *
     * <p>The context this method is called on should not be called afterwards.
     *
     * @param mapper Mapping function.
     * @return Newly created context with mapped operands.
     */
    @CheckReturnValue
    public Context mapAllVariablesInAllScopes(final Function<Operand, Operand> mapper) {
      final var mappedVariables = new LinkedHashMap<TypedAst.VariableId, Operand>();
      for (final Map.Entry<TypedAst.VariableId, Operand> entry : variables.entrySet()) {
        mappedVariables.put(entry.getKey(), mapper.apply(entry.getValue()));
      }
      final var mappedHiddenVariables = new LinkedHashMap<TemporaryVariableId, Operand>();
      for (final Map.Entry<TemporaryVariableId, Operand> entry : hiddenVariables.entrySet()) {
        mappedHiddenVariables.put(entry.getKey(), mapper.apply(entry.getValue()));
      }
      return new Context(
          mappedVariables,
          mappedHiddenVariables,
          parentContext != null ? parentContext.mapAllVariablesInAllScopes(mapper) : null,
          nextTemporaryVariableId,
          loopIdNext,
          loopIdEnd);
    }

    @Override
    public String toString() {
      return Stream.concat(
                  variables.entrySet().stream().map(x -> x.getKey().last() + ": " + x.getValue()),
                  hiddenVariables.entrySet().stream()
                      .map(x -> x.getKey().rawId() + ": " + x.getValue()))
              .collect(Collectors.joining(", ", "{ ", " }"))
          + (parentContext != null ? " -> " + parentContext : "");
    }
  }

  /**
   * Container for information about individual types, both the operand type, and the fields
   * placement.
   *
   * <ul>
   *   <li>operandType: Operand type of the given type.
   *   <li>fieldPlacement: Location of the fields, relative to the other fields in the type.
   * </ul>
   */
  private record TypeLayout(OperandType operandType, List<String> fieldPlacement) {
    public TypeLayout {
      requireNonNull(operandType);
      requireNonNull(fieldPlacement);
    }
  }

  /** Container for environment information. */
  record Environments(
      Map<TypedAst.TypeId, TypedAst.StructDefinition> structDefinitions,
      Map<TypedAst.VariableId, TypedAst.ConstantDefinition> constantDefinitions,
      Map<TypedAst.VariableId, Ir.FunctionId> functionIdentifiers) {

    public Environments {
      requireNonNull(structDefinitions);
      requireNonNull(constantDefinitions);
      requireNonNull(functionIdentifiers);
    }

    /**
     * Retrieves a struct definition.
     *
     * @param structId Struct type id.
     * @return Struct definition, or null if non-existent.
     */
    public TypedAst.StructDefinition getStructDefinition(TypedAst.TypeId structId) {
      final TypedAst.StructDefinition def = structDefinitions().get(structId);
      if (def == null) {
        throw new InternalCompilerException(
            "Could not find struct with type id '%s'", structId.fullNamePath());
      } else {
        return def;
      }
    }
  }

  private static Operand.Variable operandFromInput(Ir.Input input) {
    return new Operand.Variable(input.assignedVariable(), input.variableType());
  }

  /**
   * Checks that the two operand types possesses the same recursive structure, and throws an error
   * if this is not the case.
   *
   * @param failMessage Message to give if the check fails.
   * @param type1 First type.
   * @param type2 Second type.
   */
  static void assertSameStructure(
      final String failMessage, final OperandType type1, final OperandType type2) {
    if (!type1.equals(type2)) {
      throw new InternalCompilerException("%s:%n      %s%n  vs. %s", failMessage, type1, type2);
    }
  }

  /**
   * Checks that the two operands possesses the same recursive structure, and throws an error if
   * this is not the case.
   *
   * @param operand1 First operand.
   * @param operand2 Second operand.
   */
  static void assertSameStructure(final Operand operand1, final Operand operand2) {
    assertSameStructure(
        "Expected operands to possess same type", operand1.operandType(), operand2.operandType());
  }

  /**
   * Transforms a BigInteger into an unsigned equivalent of specific bit width. Useful for later
   * stages concatenation of BigInteger bits.
   *
   * @param bitWidth the bit width of the resulting unsigned BigInteger. Only used when {@code
   *     value} is negative.
   * @param value the value to transform to its unsigned equivalent.
   * @return a BigInteger with the same bit representation as {@code value}, but always positive.
   */
  private static BigInteger unsignedBigInteger(int bitWidth, BigInteger value) {
    if (value.signum() >= 0) {
      return value;
    }

    return BigInteger.ONE.shiftLeft(bitWidth).add(value);
  }
}
