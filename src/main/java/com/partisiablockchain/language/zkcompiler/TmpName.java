package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a temporary variable name by its own part, and a parent name.
 *
 * @param namePart specific part important to this variable name.
 * @param parent potential parent name.
 */
@SuppressWarnings("UnusedVariable")
public record TmpName(String namePart, TmpName parent) {

  /**
   * Create a new root temporary variable name.
   *
   * @param rootPart initial temporary variable name.
   * @return temporary variable name with no parent.
   */
  public static TmpName root(final String rootPart) {
    return new TmpName(rootPart, null);
  }

  /**
   * Add a new part to the temporary variable name.
   *
   * @param newPart new variable name part to be added.
   * @return new {@code TmpName} with calling name as parent.
   */
  public TmpName with(final String newPart) {
    return new TmpName(newPart, this);
  }

  /**
   * Determines full temporary name of variable.
   *
   * @return a concatenation of name parts along with parent name.
   */
  public String determineTmpName() {
    final var parts = new ArrayList<String>();
    allPartsToAccum(parts);
    final StringBuilder out = new StringBuilder();
    int idx = 0;
    for (final String part : parts) {
      if (idx == 0) {
        out.append(part);
      } else if (idx == parts.size() - 1) {
        out.append("_").append(part);
      } else {
        if (idx == 1) {
          out.append("_");
        }
        out.append(part.substring(0, 1));
      }
      idx++;
    }
    return out.toString();
  }

  private void allPartsToAccum(final List<String> partsOut) {
    if (parent != null) {
      parent().allPartsToAccum(partsOut);
    }
    partsOut.add(namePart());
  }
}
