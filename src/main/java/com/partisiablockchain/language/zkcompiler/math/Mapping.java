package com.partisiablockchain.language.zkcompiler.math;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Implements the mathematical concept of a permutation: A bijection from integer to integer;
 * sometimes also treated as a group.
 */
public record Mapping(List<Integer> permutation) {

  /**
   * Constructor for permutation.
   *
   * @param permutation The permutation mapping, from the index of the entry to the value of the
   *     entry. Never null.
   */
  public Mapping {
    requireNonNull(permutation);
  }

  /**
   * Create new identity permutation of the given size.
   *
   * @param size Size of the permutation.
   * @return Identity permutation of the given size. Never null.
   */
  public static Mapping identity(final int size) {
    final var permutation = new ArrayList<Integer>();
    for (int idx = 0; idx < size; idx++) {
      permutation.add(idx);
    }
    return new Mapping(List.copyOf(permutation));
  }

  /**
   * Permutes given list with this permutation.
   *
   * @param <T> Type of the elements to map.
   * @param arguments Given list.
   * @return Permuted list. Never null.
   */
  public <T> List<T> apply(final List<T> arguments) {
    return permutation().stream()
        .map(i -> i < arguments.size() ? arguments.get(i) : null)
        .filter(Objects::nonNull)
        .toList();
  }

  /**
   * Composes two permutations together to create a new permutation.
   *
   * @param other Other permutation.
   * @return Permuted permutation. Never null.
   */
  public Mapping andThen(final Mapping other) {
    return new Mapping(other.apply(permutation()));
  }
}
