package com.partisiablockchain.language.zkcompiler.math;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayDeque;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;

/**
 * Mini-library for performing data-flow-style analysis.
 *
 * @see <a href="https://en.wikipedia.org/wiki/Data-flow_analysis">Data-flow analysis, Wikipedia</a>
 * @see <a href="https://en.wikipedia.org/wiki/Lattice_(order)">Lattice Order, Wikipedia</a>
 */
public final class DataFlowAnalysis {

  private DataFlowAnalysis() {}

  /**
   * Mutable graph for traversal in data-flow analysis.
   *
   * @param <NodeT> Graph node type.
   */
  public record Graph<NodeT>(
      LinkedHashMap<NodeT, LinkedHashSet<NodeT>> forwardEdges,
      LinkedHashMap<NodeT, LinkedHashSet<NodeT>> backwardEdges) {

    /**
     * Creates new empty graph.
     *
     * @param <NodeT> Graph node type.
     * @return New empty graph.
     */
    public static <NodeT> Graph<NodeT> empty() {
      return new Graph<NodeT>(new LinkedHashMap<>(), new LinkedHashMap<>());
    }

    /**
     * Adds new node to the graph.
     *
     * @param node New node to create.
     */
    public void addNode(final NodeT node) {
      forwardEdges.computeIfAbsent(node, k -> new LinkedHashSet<NodeT>());
      backwardEdges.computeIfAbsent(node, k -> new LinkedHashSet<NodeT>());
    }

    /**
     * Adds new edge to the graph. Will also add both related nodes to the graph.
     *
     * @param origin Origin node for edge.
     * @param target Target node for edge.
     */
    public void addEdge(final NodeT origin, final NodeT target) {
      addNode(origin);
      addNode(target);
      forwardEdges.get(origin).add(target);
      backwardEdges.get(target).add(origin);
    }
  }

  /**
   * Fixpoint computation performed for each individual node. This computation is performed
   * repeatedly for individual nodes until a fixpoint is reached for the entire graph.
   *
   * <p>The computation must be designed to eventual converge to a fixpoint, in order to guarantee
   * the termination of the analysis. A useful way to construct these computations is by defining
   * them as monotone operations over a <a
   * href="https://en.wikipedia.org/wiki/Lattice_(order)">lattice</a>.
   *
   * @param <NodeT> Graph node type.
   * @param <ResultT> Per-node computation result type.
   */
  public interface FixpointComputation<NodeT, ResultT> {

    /**
     * Compute the result for a single node given the previously calculated results for predecessor
     * nodes.
     *
     * @param node The node to compute for.
     * @param predecessorResults Results for the predecessor nodes.
     * @return The result for the node. This can be null.
     */
    ResultT compute(NodeT node, Map<NodeT, ResultT> predecessorResults);
  }

  /**
   * Performs forward analysis over given graph, with the given fixpoint computation.
   *
   * <p>The computation of a result for a node can only depend on the results from nodes that has an
   * edge <b>going to</b> the node. The analysis runs the computation repeatedly, by calculating the
   * output from the inputs locally at each node until the whole system stabilizes, i.e. it reaches
   * a fixpoint.
   *
   * <p>The computation must be designed to eventual converge to a fixpoint, in order to guarentee
   * the termination of the analysis. A useful way to construct these computations is by defining
   * them as monotone operations over a <a
   * href="https://en.wikipedia.org/wiki/Lattice_(order)">lattice</a>.
   *
   * @param <NodeT> Graph node type.
   * @param <ResultT> Per-node computation result type.
   * @param graph Graph to perform analysis over.
   * @param initialValueComputation Function to determine initial value for each individual node in
   *     graph. Is allowed to return null.
   * @param fixpointComputation Fixpoint computation to perform at each individual node. Computation
   *     is given the node, and a map of values for predecessor nodes. Is allowed to return null.
   * @return Map from nodes to the fixpoint result. Might contain null values.
   */
  public static <NodeT, ResultT> Map<NodeT, ResultT> forwardAnalysis(
      final Graph<NodeT> graph,
      final Function<NodeT, ResultT> initialValueComputation,
      final FixpointComputation<NodeT, ResultT> fixpointComputation) {
    return globalAnalysis(
        graph.forwardEdges(),
        initialValueComputation,
        onlyPredecessorNodes(graph.backwardEdges(), fixpointComputation));
  }

  /**
   * Performs backward analysis over given graph, with the given fixpoint computation.
   *
   * <p>The computation of a result for a node can only depend on the results from nodes that has an
   * edge <b>going from</b> the node. The analysis runs the computation repeatedly, by calculating
   * the output from the inputs locally at each node until the whole system stabilizes, i.e. it
   * reaches a fixpoint.
   *
   * <p>The computation must be designed to eventual converge to a fixpoint, in order to guarentee
   * the termination of the analysis. A useful way to construct these computations is by defining
   * them as monotone operations over a <a
   * href="https://en.wikipedia.org/wiki/Lattice_(order)">lattice</a>.
   *
   * @param <NodeT> Graph node type.
   * @param <ResultT> Per-node computation result type.
   * @param graph Graph to perform analysis over.
   * @param fixpointComputation Fixpoint computation to perform at each individual node. Computation
   *     is given the node, and a map of values for successor nodes. Is allowed to return null.
   * @return Map from nodes to the fixpoint result. Might contain null values.
   */
  public static <NodeT, ResultT> Map<NodeT, ResultT> backwardAnalysis(
      final Graph<NodeT> graph, final FixpointComputation<NodeT, ResultT> fixpointComputation) {
    return globalAnalysis(
        graph.backwardEdges(),
        x -> null,
        onlyPredecessorNodes(graph.forwardEdges(), fixpointComputation));
  }

  /**
   * Performs backward analysis over given graph, with the given fixpoint computation.
   *
   * <p>The computation of a result for a node can depend upon any other node in the graph. The
   * analysis runs the computation repeatedly, by calculating the output from the inputs locally at
   * each node until the whole system stabilizes, i.e. it reaches a fixpoint.
   *
   * <p>The computation must be designed to eventual converge to a fixpoint, in order to guarentee
   * the termination of the analysis. A useful way to construct these computations is by defining
   * them as monotone operations over a <a
   * href="https://en.wikipedia.org/wiki/Lattice_(order)">lattice</a>.
   *
   * <p>This analysis function allows the computation to consider the entire graph when performing
   * the analysis, for example if the analysis requires non-local state.
   *
   * @param <NodeT> Graph node type.
   * @param <ResultT> Per-node computation result type.
   * @param graph Graph to perform analysis over.
   * @param initialValueComputation Function to determine initial value for each individual node in
   *     graph. Is allowed to return null.
   * @param fixpointComputation Fixpoint computation to perform at each individual node. Computation
   *     is given the node, and a map of values for all nodes, allowing the analysis to inspect more
   *     than just its neighboring nodes. Is allowed to return null.
   * @return Map from nodes to the fixpoint result. Might contain null values.
   */
  public static <NodeT, ResultT> Map<NodeT, ResultT> backwardAnalysisGlobalProperty(
      final Graph<NodeT> graph,
      final Function<NodeT, ResultT> initialValueComputation,
      final FixpointComputation<NodeT, ResultT> fixpointComputation) {
    return globalAnalysis(graph.backwardEdges(), initialValueComputation, fixpointComputation);
  }

  /**
   * Wraps a {@link FixpointComputation} such that the inner computation only receives the result of
   * the predecessor nodes for any given node. This is used for most flow-based analysises.
   *
   * @param <NodeT> Graph node type.
   * @param <ResultT> Per-node computation result type.
   * @param predecessorEdges The graph used to determine the predecessor nodes.
   * @param fixpointComputation The inner computation to wrap.
   * @return Wrapped computation.
   */
  static <NodeT, ResultT> FixpointComputation<NodeT, ResultT> onlyPredecessorNodes(
      final Map<NodeT, ? extends Set<NodeT>> predecessorEdges,
      final FixpointComputation<NodeT, ResultT> fixpointComputation) {
    return (node, resultsForAllNodes) -> {
      final LinkedHashMap<NodeT, ResultT> resultsForPredecessorNodes =
          new LinkedHashMap<NodeT, ResultT>();
      for (final NodeT prevNode : predecessorEdges.get(node)) {
        resultsForPredecessorNodes.put(prevNode, resultsForAllNodes.get(prevNode));
      }
      return fixpointComputation.compute(node, resultsForPredecessorNodes);
    };
  }

  private static <NodeT, ResultT> Map<NodeT, ResultT> globalAnalysis(
      final Map<NodeT, ? extends Set<NodeT>> computationDirectionEdges,
      final Function<NodeT, ResultT> initialValueComputation,
      final FixpointComputation<NodeT, ResultT> fixpointComputation) {

    final LinkedHashMap<NodeT, ResultT> resultsForAllNodes = new LinkedHashMap<NodeT, ResultT>();

    // Initialize each node and add them to the queue.
    final Queue<NodeT> queue = new ArrayDeque<NodeT>();
    for (final NodeT node : computationDirectionEdges.keySet()) {
      resultsForAllNodes.put(node, initialValueComputation.apply(node));
      queue.add(node);
    }

    // Iterate using queue
    while (!queue.isEmpty()) {
      final NodeT node = queue.remove();
      final ResultT prevResult = resultsForAllNodes.get(node);
      final ResultT nodeResult = fixpointComputation.compute(node, resultsForAllNodes);

      if (!safeEquals(prevResult, nodeResult)) {
        resultsForAllNodes.put(node, nodeResult);
        for (final NodeT nextNode : computationDirectionEdges.get(node)) {
          queue.add(nextNode);
        }
      }
    }

    return resultsForAllNodes;
  }

  /**
   * Null-safe equality checker.
   *
   * @param <T> Type to equals.
   * @param e1 First value.
   * @param e2 Second value.
   * @return True if and only if both are null, or if both are not null and equals.
   */
  static <T> boolean safeEquals(final T e1, final T e2) {
    if (e1 == null && e2 == null) {
      return true;
    }
    return e1 != null && e1.equals(e2);
  }
}
