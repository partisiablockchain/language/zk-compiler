package com.partisiablockchain.language.zkcompiler.math;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Map;

/** Implements an alias map; a map where values default to their keys, if not otherwise set. */
public final class AliasMap<T> {

  private final Map<T, T> aliases;

  /**
   * Constructor.
   *
   * @param aliases The alias mapping itself. Never null.
   */
  public AliasMap(final Map<T, T> aliases) {
    this.aliases = aliases;
  }

  /**
   * Adds another alias mapping.
   *
   * @param key Given key.
   * @param value Given value.
   */
  public void put(final T key, final T value) {
    aliases.put(key, value);
  }

  /**
   * Returns the alias mapping; if no value was explicitly set for a key, it defaults to the key
   * itself.
   *
   * @param key Given key
   * @return Alias.
   */
  public T get(final T key) {
    return aliases.getOrDefault(key, key);
  }
}
