package com.partisiablockchain.language.zkcompiler.math;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Partially ordered set wherein every pair of elements have a least upper bound.
 *
 * @see <a href="https://en.wikipedia.org/wiki/Lattice_(order)">Lattice (order), Wikipedia</a>
 * @see <a href="https://cs.au.dk/~amoeller/spa/3-lattices-and-fixpoints.pdf">Anders Møller's Static
 *     Program Analysis Slides</a>
 */
public interface Lattice<T extends Lattice<T>> {

  /**
   * Determines the least upper bound for this and other.
   *
   * @param other Other element to determine LUB with.
   * @return Least upper bound.
   */
  T leastUpperBound(T other);
}
