package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.ir.IrPretty;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents the type of an {@link Operand} during individual phases of the compiler intended to
 * produce {@link Ir}. Describes show operands should be represented during runtime.
 */
sealed interface OperandType {
  /**
   * Asserts that this operand type is {@link BasicType}.
   *
   * @return This basic type's internal IR type.
   * @exception InternalCompilerException If the operand type is not {@link BasicType}.
   */
  default Ir.Type expectBasic() {
    throw new InternalCompilerException("%s is not BasicType", this);
  }

  /**
   * Asserts that this operand type is {@link ArrayType}.
   *
   * @return The array type itself.
   * @exception InternalCompilerException If the operand type is not {@link ArrayType}.
   */
  default OperandType.ArrayType expectArray() {
    throw new InternalCompilerException("%s is not ArrayType", this);
  }

  /**
   * Returns the component operand types of this operand as a list. For {@link BasicType}, this will
   * return a singleton list with the operand itself. May also return an empty list.
   *
   * @return List of basic operand types
   */
  List<Ir.Type> baseTypes();

  /** Represents a basic operand. */
  record BasicType(Ir.Type type) implements OperandType {

    public BasicType {
      requireNonNull(type);
    }

    @Override
    public Ir.Type expectBasic() {
      return type();
    }

    @Override
    public List<Ir.Type> baseTypes() {
      return List.of(type());
    }

    @Override
    public String toString() {
      return IrPretty.prettyType(type);
    }
  }

  /** Represents a composite type with known fields of differing sizes. */
  record TupleType(List<OperandType> componentTypes) implements OperandType {

    public TupleType {
      requireNonNull(componentTypes);
    }

    @Override
    public List<Ir.Type> baseTypes() {
      return componentTypes().stream().map(OperandType::baseTypes).flatMap(List::stream).toList();
    }

    @Override
    public String toString() {
      return componentTypes().stream()
          .map(Object::toString)
          .collect(Collectors.joining(", ", "(", ")"));
    }
  }

  /** Represents a composite type with an known amount of fields of the same size. */
  record ArrayType(OperandType subtype, int size) implements OperandType {

    public ArrayType {
      requireNonNull(subtype);
      if (size < 0) {
        throw new IllegalArgumentException(
            "OperandType.ArrayType cannot have negative size " + size);
      }
    }

    @Override
    public ArrayType expectArray() {
      return this;
    }

    @Override
    public List<Ir.Type> baseTypes() {
      final List<Ir.Type> subtypeBaseTypes = subtype.baseTypes();
      return Stream.generate(() -> subtypeBaseTypes).limit(size).flatMap(List::stream).toList();
    }

    @Override
    public String toString() {
      return "[%s; %d]".formatted(subtype, size());
    }
  }
}
