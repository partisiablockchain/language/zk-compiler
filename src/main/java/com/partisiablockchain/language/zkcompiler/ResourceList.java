package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ThrowingSupplier;
import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/** Lists resources available from the classpath. */
public final class ResourceList {
  /** Functional interface consuming properties and their corresponding file name. */
  public interface PropertiesFileConsumer {

    /**
     * Accept a properties file.
     *
     * @param sourceName name of the properties list source. Not guarenteed to be a path as string.
     * @param properties the properties
     */
    void accept(String sourceName, Properties properties);
  }

  private ResourceList() {}

  /**
   * Consumes properties and corresponding names of resource files from the class path matching
   * <code>*-git.properties</code>in the order they are found.
   *
   * @param consumer consumes matching properties and their corresponding file name
   */
  static void consumeGitProperties(PropertiesFileConsumer consumer) {
    Pattern filePattern = Pattern.compile(".*-git\\.properties");
    consumePropertiesFromResources(consumer, filePattern);
  }

  private static List<Path> classPaths() {
    String classPath = System.getProperty("java.class.path", ".");
    String[] classPathElements = classPath.split(System.getProperty("path.separator"), -1);
    return Stream.of(classPathElements).map(Path::of).toList();
  }

  /**
   * Consumes properties and corresponding names of resources of class path matching a pattern in
   * the order they are found.
   *
   * @param consumer consumes matching properties and their corresponding file name
   * @param filePattern the pattern to match
   */
  public static void consumePropertiesFromResources(
      PropertiesFileConsumer consumer, Pattern filePattern) {
    for (Path filePath : classPaths()) {
      consumePropertiesFromResources(filePath, consumer, filePattern);
    }
  }

  private static void consumePropertiesFromResources(
      Path filePath, PropertiesFileConsumer consumer, Pattern filePattern) {
    final File file = filePath.toFile();
    if (file.exists()) {
      if (file.isDirectory()) {
        consumePropertiesOfDirectory(file, consumer, filePattern);
      } else {
        consumePropertiesFromJarFile(file, consumer, filePattern);
      }
    }
  }

  private static void consumePropertiesFromJarFile(
      File file, PropertiesFileConsumer consumer, Pattern filePattern) {
    WithResource.accept(
        () -> new ZipFile(file),
        zipFile -> {
          Enumeration<? extends ZipEntry> entries = zipFile.entries();
          while (entries.hasMoreElements()) {
            ZipEntry zipEntry = entries.nextElement();
            if (filePattern.matcher(zipEntry.getName()).matches()) {
              Properties properties = loadProperties(() -> zipFile.getInputStream(zipEntry));
              consumer.accept(zipEntry.getName(), properties);
            }
          }
        },
        "Could not get resources from jar: " + file.getName());
  }

  private static void consumePropertiesOfDirectory(
      File directory, PropertiesFileConsumer consumer, Pattern filePattern) {
    File[] files = Objects.requireNonNullElse(directory.listFiles(), new File[0]);
    for (File file : files) {
      if (file.isDirectory()) {
        consumePropertiesOfDirectory(file, consumer, filePattern);
      } else {
        if (filePattern.matcher(file.getName()).matches()) {
          Properties properties = loadProperties(() -> new FileInputStream(file));
          consumer.accept(file.getName(), properties);
        }
      }
    }
  }

  private static Properties loadProperties(ThrowingSupplier<InputStream> inputStream) {
    return WithResource.apply(
        inputStream,
        stream -> {
          Properties properties = new Properties();
          properties.load(stream);
          return properties;
        },
        "Could not load properties");
  }
}
