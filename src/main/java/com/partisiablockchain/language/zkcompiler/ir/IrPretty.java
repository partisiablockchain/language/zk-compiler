package com.partisiablockchain.language.zkcompiler.ir;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Utility class for pretty-printing ir-Circuits for debugging. */
public final class IrPretty {

  private IrPretty() {}

  /**
   * Produces a prettified ir-circuit.
   *
   * @param irCircuit ir-Circuit to prettify.
   * @return Prettified string. never null.
   */
  public static String pretty(final Ir.Circuit irCircuit) {
    final StringBuilder out = new StringBuilder();
    out.append("(ircircuit");

    for (final Ir.Function fn : irCircuit.functions()) {
      out.append(String.format("%n  ("));
      if (fn.exported()) {
        out.append("exported ");
      }
      out.append("function ").append(prettyFunctionId(fn.functionId()));

      // End block and output variables
      out.append(String.format("%n    (output"));
      for (final Ir.Type outputType : fn.outputVariableTypes()) {
        out.append(" ").append(prettyType(outputType));
      }
      out.append(")");

      // Display blocks
      for (final Ir.Block block : fn.blocks()) {
        prettyBlock(out, block);
      }
      out.append(")");
    }

    out.append(")");

    return out.toString();
  }

  private static void prettyBlock(final StringBuilder out, final Ir.Block block) {
    out.append(String.format("%n    ("));
    if (block.isEntryPoint()) {
      out.append("entry ");
    }
    out.append("block ").append(prettyBlockId(block.blockId()));

    // input variables
    out.append(String.format("%n      (inputs"));
    for (final Ir.Input input : block.inputs()) {
      out.append(String.format("%n        ("))
          .append(prettyType(input.variableType()))
          .append(" ")
          .append(prettyVariable(input.assignedVariable()));
      out.append(")");
    }
    out.append(")");

    // Instructions
    for (final Ir.Instruction insn : block.instructions()) {
      out.append(String.format("%n      "));
      prettyInstruction(out, insn);
    }

    // Branching info
    prettyTerminator(out, block.terminator());

    out.append(")");
  }

  private static void prettyTerminator(
      final StringBuilder out, final Ir.Terminator uncheckedTerminator) {
    // Branch if
    if (uncheckedTerminator instanceof Ir.BranchIf terminator) {
      out.append(String.format("%n      (branch-if "))
          .append(prettyVariable(terminator.conditionVariable()))
          .append(String.format("%n        (0 "));
      prettyBranchInfo(out, terminator.branchFalse());
      out.append(")").append(String.format("%n        (1 "));
      prettyBranchInfo(out, terminator.branchTrue());
      out.append("))");

    } else if (uncheckedTerminator instanceof Ir.CallAndBranch terminator) {
      out.append(String.format("%n      (call ")).append(prettyFunctionId(terminator.functionId()));
      prettyVariables(out, terminator.functionArguments());
      out.append(String.format("%n        ("));
      prettyBranchInfo(out, terminator.branchToAfterReturn());
      out.append("))");

      // Branch always
    } else {
      final Ir.BranchAlways terminator = (Ir.BranchAlways) uncheckedTerminator;
      out.append(String.format("%n      (branch-always "));
      prettyBranchInfo(out, terminator.branch());
      out.append(")");
    }
  }

  private static void prettyBranchInfo(final StringBuilder out, final Ir.BranchInfo branch) {
    out.append(prettyBlockId(branch.targetBlock()));
    prettyVariables(out, branch.branchInputVariables());
  }

  private static void prettyVariables(
      final StringBuilder out, final Iterable<Ir.VariableId> variables) {
    for (final Ir.VariableId variable : variables) {
      out.append(" ").append(prettyVariable(variable));
    }
  }

  private static void prettyInstruction(final StringBuilder out, final Ir.Instruction insn) {
    out.append(
        "(%-6s %-20s ".formatted(prettyType(insn.resultType()), prettyVariable(insn.resultId())));
    prettyOperation(out, insn.operation());
    out.append(")");
  }

  /**
   * Produces a prettified type.
   *
   * @param uncheckedType Type to prettify.
   * @return Prettified string. never null.
   */
  public static String prettyType(final Ir.Type uncheckedType) {
    if (uncheckedType instanceof Ir.Type.PublicInteger type) {
      return "i%d".formatted(type.width());
    } else {
      final Ir.Type.SecretBinaryInteger type = (Ir.Type.SecretBinaryInteger) uncheckedType;
      return "sbi%d".formatted(type.width());
    }
  }

  private static void prettyOperation(final StringBuilder out, final Ir.Operation uncheckedOp) {
    if (uncheckedOp instanceof Ir.Operation.Constant operation) {
      out.append("(constant %s %d)".formatted(prettyType(operation.type()), operation.const1()));
    } else if (uncheckedOp instanceof Ir.Operation.PublicToSecret operation) {
      out.append(
          "(to_sbi %s %s)"
              .formatted(prettyType(operation.type()), prettyVariable(operation.varIdx1())));
    } else if (uncheckedOp instanceof Ir.Operation.Nullary operation) {
      out.append("(%s)".formatted(operation.operation().prettyName()));
    } else if (uncheckedOp instanceof Ir.Operation.Binary operation) {
      out.append(
          "(%s %s %s)"
              .formatted(
                  operation.operation().prettyName(),
                  prettyVariable(operation.varIdx1()),
                  prettyVariable(operation.varIdx2())));
    } else if (uncheckedOp instanceof Ir.Operation.Select operation) {
      out.append(
          "(select %s %s %s)"
              .formatted(
                  prettyVariable(operation.varCondition()),
                  prettyVariable(operation.varIfTrue()),
                  prettyVariable(operation.varIfFalse())));
    } else if (uncheckedOp instanceof Ir.Operation.Extract operation) {
      out.append(
          "(extract %s %s %d)"
              .formatted(
                  prettyVariable(operation.varToExtractFrom()),
                  prettyType(operation.typeToExtract()),
                  operation.bitOffsetFromLow()));
    } else if (uncheckedOp instanceof Ir.Operation.ExtractDyn operation) {
      out.append(
          "(extractdyn %s %s %s)"
              .formatted(
                  prettyVariable(operation.varToExtractFrom()),
                  prettyType(operation.typeToExtract()),
                  prettyVariable(operation.varOffsetFromLow())));
    } else if (uncheckedOp instanceof Ir.Operation.InsertDyn operation) {
      out.append(
          "(insertdyn %s %s %s)"
              .formatted(
                  prettyVariable(operation.varTarget()),
                  prettyVariable(operation.varToInsert()),
                  prettyVariable(operation.varOffsetIntoTarget())));
    } else if (uncheckedOp instanceof Ir.Operation.CallDirectly operation) {
      out.append(
          "(call %s %s)"
              .formatted(
                  prettyFunctionId(operation.functionId()),
                  String.join(
                      " ",
                      operation.functionArguments().stream()
                          .map(IrPretty::prettyVariable)
                          .toList())));
    } else {
      final Ir.Operation.Unary operation = (Ir.Operation.Unary) uncheckedOp;
      out.append(
          "(%s %s)"
              .formatted(operation.operation().prettyName(), prettyVariable(operation.varIdx1())));
    }
  }

  /**
   * Produces a prettified variable id.
   *
   * @param variableId Variable to prettify.
   * @return Prettified string. Never null.
   */
  public static String prettyVariable(final Ir.VariableId variableId) {
    return "$%d_%s".formatted(variableId.id(), variableId.name());
  }

  /**
   * Produces a prettified block id.
   *
   * @param blockId Block id to prettify.
   * @return Prettified string. Never null.
   */
  public static String prettyBlockId(final Ir.BlockId blockId) {
    if (Ir.BlockId.RETURN.equals(blockId)) {
      return "#return";
    }
    return "#%d_%s".formatted(blockId.id(), blockId.name());
  }

  /**
   * Produces a prettified Function id.
   *
   * @param functionId Function id to prettify.
   * @return Prettified string. Never null.
   */
  public static String prettyFunctionId(final Ir.FunctionId functionId) {
    return "%%%d_%s".formatted(functionId.id(), functionId.name());
  }
}
