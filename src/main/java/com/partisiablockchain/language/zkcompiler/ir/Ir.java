package com.partisiablockchain.language.zkcompiler.ir;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.zkcompiler.irpass.IrPass;
import java.math.BigInteger;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/** Namespace class for the Intermediary-Representation model. */
@Immutable
public record Ir() {

  /**
   * Program circuit, containing all functions of the program.
   *
   * @param functions Function of the program circuit.
   */
  public record Circuit(List<Function> functions) {

    /**
     * Constructor for {@link Circuit}.
     *
     * @param functions Function of the program circuit.
     */
    public Circuit {
      requireNonNull(functions);
    }
  }

  /**
   * The control flow graph of a function.
   *
   * @param functionId Id of the function. Never null.
   * @param blocks Basic blocks of the function. Never null.
   * @param outputVariableTypes Types of function output values. Never null.
   * @param exported Whether function is exported and visible as a computation from the public
   *     contract.
   */
  public record Function(
      FunctionId functionId, List<Block> blocks, List<Type> outputVariableTypes, boolean exported) {

    /**
     * Constructor.
     *
     * @param functionId Id of the function. Never null.
     * @param blocks Basic blocks of the function. Never null.
     * @param outputVariableTypes Types of function output values. Never null.
     * @param exported Whether function is exported and visible as a computation from the public
     *     contract.
     */
    public Function {
      requireNonNull(functionId);
      requireNonNull(blocks);
      requireNonNull(outputVariableTypes);
    }

    /**
     * Produces map from id to blocks.
     *
     * @return Newly created map. Never null.
     */
    public Map<BlockId, Block> toBlockMap() {
      return blocks().stream()
          .collect(
              Collectors.toUnmodifiableMap(Block::blockId, java.util.function.Function.identity()));
    }

    /**
     * Replaces the blocks in the functions.
     *
     * @param newBlocks the new blocks
     * @return A function with the blocks replaced
     */
    public Function replaceBlocks(List<Block> newBlocks) {
      return new Function(functionId, newBlocks, outputVariableTypes, exported);
    }
  }

  /** Id of an temporary variable in the circuit. */
  @Immutable
  public record VariableId(int id, String name) {

    /**
     * Constructor.
     *
     * @param id Unique identifier for block id.
     * @param name Pretty name. Never null.
     */
    public VariableId {
      requireNonNull(name);
    }

    @Override
    public String toString() {
      return IrPretty.prettyVariable(this);
    }
  }

  /** Id of a block in the circuit. */
  @Immutable
  public record BlockId(int id, String name) {
    /**
     * Constructor.
     *
     * @param id Unique identifier for block id.
     * @param name Pretty name. Never null.
     */
    public BlockId {
      requireNonNull(name);
    }

    @Override
    public String toString() {
      return IrPretty.prettyBlockId(this);
    }

    /**
     * Id of the return block. Branching to this block will trigger returning from the current
     * function.
     */
    public static final BlockId RETURN = new BlockId(-1, "return");
  }

  /** Id of a function in the circuit. */
  @Immutable
  public record FunctionId(int id, String name) {
    /**
     * Constructor.
     *
     * @param id Unique identifier for block id.
     * @param name Pretty name. Never null.
     */
    public FunctionId {
      requireNonNull(name);
    }

    @Override
    public String toString() {
      return IrPretty.prettyFunctionId(this);
    }
  }

  /**
   * Basic block within a Zero-knowledge Ir-circuit, containing some instructions, and possibly a
   * branching operation.
   */
  public record Block(
      BlockId blockId,
      List<Input> inputs,
      List<Instruction> instructions,
      Terminator terminator,
      boolean isEntryPoint) {

    /**
     * Constructor.
     *
     * @param blockId Id of block. Never null.
     * @param inputs Expected inputs for block. Never null.
     * @param instructions Instructions for block to contain. Never null.
     * @param terminator Terminator for block. Never null.
     * @param isEntryPoint If true, the block is the entry point of the circuit, where execution
     *     starts.
     */
    public Block {
      requireNonNull(blockId);
      requireNonNull(inputs);
      requireNonNull(instructions);
      requireNonNull(terminator);
    }
  }

  /** Represents what to do when block have finished computing. */
  public sealed interface Terminator {}

  /** Unconditional branch. */
  public record BranchAlways(BranchInfo branch) implements Terminator {

    /**
     * Constructor.
     *
     * @param branch Jump information. Never null.
     */
    public BranchAlways {
      requireNonNull(branch);
    }
  }

  /** Conditional branching. */
  public record BranchIf(
      boolean isBranchOnSecret,
      VariableId conditionVariable,
      BranchInfo branchTrue,
      BranchInfo branchFalse)
      implements Terminator {

    /**
     * Constructor.
     *
     * @param isBranchOnSecret Whether the branch is based on a secret variable.
     * @param conditionVariable Variable to branch upon. Never null. Must be public when {@code
     *     isBranchOnSecret} and secret otherwise.
     * @param branchTrue Jump information for case when condition evaluates to true. Never null.
     * @param branchFalse Jump information for case when condition evaluates to false. Never null.
     */
    public BranchIf {
      requireNonNull(conditionVariable);
      requireNonNull(branchTrue);
      requireNonNull(branchFalse);
    }
  }

  /**
   * Represents a direct call to a function in a {@link Terminator} position.
   *
   * <p>This variant is used in for some late {@link IrPass}. Contrast with {@link
   * Operation.CallDirectly} which represents a direct call in an {@link Operation} position, but
   * which is only supported in early {@link IrPass}.
   *
   * @param functionId Id of function to call.
   * @param functionArguments Arguments to call function with.
   * @param branchToAfterReturn Branch to perform when function returns.
   * @param numReturnValues Number of variables returned by function.
   */
  public record CallAndBranch(
      FunctionId functionId,
      List<Ir.VariableId> functionArguments,
      BranchInfo branchToAfterReturn,
      int numReturnValues)
      implements Terminator {

    /**
     * Constructor for {@link CallAndBranch}.
     *
     * @param functionId Id of function to call.
     * @param functionArguments Arguments to call function with.
     * @param branchToAfterReturn Branch to perform when function returns.
     * @param numReturnValues Number of variables returned by function.
     */
    public CallAndBranch {
      requireNonNull(functionId);
      requireNonNull(functionArguments);
      requireNonNull(branchToAfterReturn);
    }
  }

  /** Represents the information used in order to jump to a block. */
  public record BranchInfo(BlockId targetBlock, List<VariableId> branchInputVariables) {

    /**
     * Constructor.
     *
     * @param targetBlock Id of the branch to jump to. Never null.
     * @param branchInputVariables Variables to use for input variables. Never null.
     */
    public BranchInfo {
      requireNonNull(targetBlock);
      requireNonNull(branchInputVariables);
    }
  }

  /** Represents input arguments for block. */
  @Immutable
  public record Input(VariableId assignedVariable, Type variableType) {

    /**
     * Constructor.
     *
     * @param assignedVariable Variable id of input. Never null.
     * @param variableType Type of input variable. Never null.
     */
    public Input {
      requireNonNull(assignedVariable);
      requireNonNull(variableType);
    }
  }

  /** Types in Zero-knowledge Ir-Circuits. */
  @Immutable
  public sealed interface Type {

    /**
     * Bitwidth used by the given type.
     *
     * @return Bitwidth as integer.
     */
    int width();

    //// Well-known types

    /** Secret-shared bit. */
    SecretBinaryInteger Sbit = new SecretBinaryInteger(1);

    /** Secret-scared 32-bit binary integer. */
    SecretBinaryInteger Sbi32 = new SecretBinaryInteger(32);

    /** Public bit. */
    PublicInteger PublicBool = new PublicInteger(1);

    /** Public 32-bit binary integer. */
    PublicInteger I32 = new PublicInteger(32);

    /** Public 0-bit unit type. */
    PublicInteger UNIT = new PublicInteger(0);

    /** Secret-shared 0-bit unit type. */
    SecretBinaryInteger UNIT_SBI = new SecretBinaryInteger(0);

    //// Subtypes

    /**
     * Sign-agnostic integer composed by several secret-shared bits. Supports a width of up to
     * 255-bits.
     */
    @Immutable
    record SecretBinaryInteger(int width) implements Type {}

    /** Public integer, not secret-shared. Supports a width of up to 255-bits. */
    @Immutable
    record PublicInteger(int width) implements Type {}
  }

  /**
   * Instruction within a block of the IR circuit. Instructions consist of an assigned variable id,
   * the assigned type of the variable, and an operation.
   */
  public record Instruction(VariableId resultId, Type resultType, Operation operation) {
    /**
     * Constructor.
     *
     * @param resultId Variable to assign operation result to. Never null.
     * @param resultType Type of variable. Never null.
     * @param operation Operation to perform. Never null.
     */
    public Instruction {
      requireNonNull(resultId);
      requireNonNull(resultType);
      requireNonNull(operation);
    }
  }

  /** Operation within a Block of Zero-knowledge Circuit. */
  public sealed interface Operation {

    /**
     * Decides the specific operation a Nullary instruction should compute.
     *
     * @see Nullary
     */
    enum NullaryOp {
      /** Returns number of variables available to the Zk computation. */
      NUM_VARIABLES;

      /**
       * Pretty name.
       *
       * @return Pretty name, lowercase. Never null.
       */
      public String prettyName() {
        return toString().toLowerCase(Locale.ENGLISH);
      }
    }

    /**
     * Decides the specific operation a Unary instruction should compute.
     *
     * @see Unary
     */
    enum UnaryOp {
      /** Bit-flip instruction. Each individual bit of input variable is flipped. */
      BITWISE_NOT,
      /**
       * Arithmetic sign change / additive inverse / negation instruction.
       *
       * <p>For binary numbers, this is defined as two's complement.
       */
      NEGATE,

      /** Loads variable as result type. */
      LOAD_VARIABLE,

      /** Loads variable metadata as result type. */
      LOAD_METADATA,

      /**
       * Saves variable. Saved variables will be produced by the resulting meta-circuit, allowing
       * the programmer to outputting a variable amount of variables.
       */
      SAVE_VARIABLE,

      /**
       * Assuming given variable indicates a variable id, finds the next variable id in the
       * sequence.
       *
       * <p>Sequence order is undefined.
       */
      NEXT_VARIABLE_ID,

      /**
       * Represents an unknown unop operation. Should not be present in generated IR, and must only
       * be used for coverage testing of unknown binop branches.
       */
      UNKNOWN;

      /**
       * Pretty name.
       *
       * @return Pretty name, lowercase. Never null.
       */
      public String prettyName() {
        return toString().toLowerCase(Locale.ENGLISH);
      }
    }

    /**
     * Decides the specific operation a Binary instruction should compute.
     *
     * @see Binary
     */
    enum BinaryOp {

      //// Bit operations

      /** Bitwise And operation. */
      BITWISE_AND,
      /** Bitwise Or operation. */
      BITWISE_OR,
      /** Bitwise Xor operation. */
      BITWISE_XOR,
      /** Shift Right/Down Logical. */
      BITSHIFT_RIGHT_LOGICAL,
      /** Shift Left/Up Logical. */
      BITSHIFT_LEFT_LOGICAL,
      /**
       * Concatenate two operands together. Left hand becomes the high bits, and right side becomes
       * the low bits.
       */
      BIT_CONCAT,

      //// Arithmetic operations

      /** Arithmetic addition operation. */
      ADD_WRAPPING,
      /** Arithmetic subtraction operation. */
      SUBTRACT_WRAPPING,

      /**
       * Arithmetic multiplication operation, wrapping around to negative numbers if larger than
       * type range.
       */
      MULT_SIGNED_WRAPPING,

      /** Arithmetic multiplication operation, wrapping around if larger than type range. */
      MULT_UNSIGNED_WRAPPING,

      //// Relational operations

      /** Equality relation. */
      EQUAL,
      /**
       * Signed Less than relation. For public integers, the bitpatterns are interpreted as
       * two's-complement.
       */
      LESS_THAN_SIGNED,
      /**
       * Signed Less than or equal relation. For public integers, the bitpatterns are interpreted as
       * two's-complement.
       */
      LESS_THAN_OR_EQUAL_SIGNED,
      /**
       * Unsigned Less than relation. For public integers, the bitpatterns are interpreted as
       * unsigned integers.
       */
      LESS_THAN_UNSIGNED,
      /**
       * Unsigned Less than or equal relation. For public integers, the bitpatterns are interpreted
       * as unsigned integers.
       */
      LESS_THAN_OR_EQUAL_UNSIGNED,

      /**
       * Represents an unknown binop operation. Should not be present in generated IR, and must only
       * be used for coverage testing of unknown binop branches.
       */
      UNKNOWN;

      /**
       * Pretty name.
       *
       * @return Pretty name, lowercase. Never null.
       */
      public String prettyName() {
        return toString().toLowerCase(Locale.ENGLISH);
      }
    }

    /**
     * Unary Operation taking one variable. The {@code operation} field decides which operation.
     *
     * @see UnaryOp
     */
    public record Unary(UnaryOp operation, VariableId varIdx1) implements Operation {
      /**
       * Constructor.
       *
       * @param operation Specific operation to perform.
       * @param varIdx1 Only argument to operation.
       */
      public Unary {
        requireNonNull(operation);
        requireNonNull(varIdx1);
      }
    }

    /**
     * Nullary Operation taking no variables. The {@code operation} field decides the operation.
     *
     * @see NullaryOp
     */
    public record Nullary(NullaryOp operation) implements Operation {
      /**
       * Constructor.
       *
       * @param operation Specific operation to perform.
       */
      public Nullary {
        requireNonNull(operation);
      }
    }

    /**
     * Constant Operation taking no variables; instead directly producing a value with the constant
     * value.
     *
     * @param type Type of constant.
     * @param const1 Unsigned representation of constant value.
     */
    public record Constant(Type type, BigInteger const1) implements Operation {
      /**
       * Constructor.
       *
       * @param type Type of constant.
       * @param const1 Unsigned representation of constant value.
       */
      public Constant {
        requireNonNull(const1);
        if (const1.signum() < 0) {
          throw new IllegalArgumentException(
              "Ir.Operation.Constant does not allow negative values: %s".formatted(const1));
        }
      }
    }

    /**
     * Operation for converting a public integer to a secret. The public and the secret must have
     * the same bitwidth.
     */
    public record PublicToSecret(Type.SecretBinaryInteger type, VariableId varIdx1)
        implements Operation {}

    /**
     * Binary Operation taking two variables. The {@code operation} field decides which operation.
     *
     * @see BinaryOp
     */
    public record Binary(BinaryOp operation, VariableId varIdx1, VariableId varIdx2)
        implements Operation {
      /**
       * Constructor.
       *
       * @param operation Specific operation to perform.
       * @param varIdx1 First argument to operation.
       * @param varIdx2 Second argument to operation.
       */
      public Binary {
        requireNonNull(operation);
        requireNonNull(varIdx1);
        requireNonNull(varIdx2);
      }
    }

    /** Select Operation, taking three variables. */
    public record Select(VariableId varCondition, VariableId varIfTrue, VariableId varIfFalse)
        implements Operation {

      /**
       * Constructor.
       *
       * @param varCondition Variable to branch on.
       * @param varIfTrue Variable to produce if varCondition evaluates to true.
       * @param varIfFalse Variable to produce if varCondition evaluates to false.
       */
      public Select {
        requireNonNull(varCondition);
        requireNonNull(varIfTrue);
        requireNonNull(varIfFalse);
      }
    }

    /**
     * Extract Operation, for extracting bits from {@code varToExtractFrom} at bit offset {@code
     * bitOffsetFromLow}. Will extract {@code typeToExtract.width()} bits, enough to create a new
     * operand of type {@code typeToExtract}.
     *
     * @param varToExtractFrom Variable to extract from.
     * @param typeToExtract Type to extract from the bit-sequence.
     * @param bitOffsetFromLow Bit offset from the low bits.
     */
    public record Extract(VariableId varToExtractFrom, Ir.Type typeToExtract, int bitOffsetFromLow)
        implements Operation {

      /**
       * Create Extract operation.
       *
       * @param varToExtractFrom Variable to extract from.
       * @param typeToExtract Type to extract from the bit-sequence.
       * @param bitOffsetFromLow Bit offset from the low bits.
       */
      public Extract {
        requireNonNull(varToExtractFrom);
        requireNonNull(typeToExtract);
        if (bitOffsetFromLow < 0) {
          throw new IllegalArgumentException(
              "Bit offset (%d) must be non-negative".formatted(bitOffsetFromLow));
        }
      }
    }

    /**
     * Extract Operation, for extracting bits from {@code varToExtractFrom} at dynamic bit offset
     * {@code varOffsetFromLow}. Will extract {@code typeToExtract.width()} bits, enough to create a
     * new operand of type {@code typeToExtract}.
     *
     * @param varToExtractFrom Variable to extract from.
     * @param typeToExtract Type to extract from the bit-sequence.
     * @param varOffsetFromLow Bit offset from the low bits.
     */
    public record ExtractDyn(
        VariableId varToExtractFrom, Ir.Type typeToExtract, VariableId varOffsetFromLow)
        implements Operation {

      /**
       * Create {@link ExtractDyn} operation.
       *
       * @param varToExtractFrom Variable to extract from.
       * @param typeToExtract Type to extract from the bit-sequence.
       * @param varOffsetFromLow Bit offset from the low bits.
       */
      public ExtractDyn {
        requireNonNull(varToExtractFrom);
        requireNonNull(typeToExtract);
        requireNonNull(varOffsetFromLow);
      }
    }

    /**
     * Insert Operation, for overwriting a bit range of {@code varTarget} with the contents of
     * {@code varToInsert}. The overwritten range starts at {@code varOffsetIntoTarget} and has
     * width with {@code varToInsert}.
     *
     * @param varTarget Variable to insert into.
     * @param varToInsert Variable to insert. The entire variable will be used.
     * @param varOffsetIntoTarget Bit offset to insert into {@code varTarget}. Indexes from low
     *     bits.
     */
    public record InsertDyn(
        VariableId varTarget, VariableId varToInsert, VariableId varOffsetIntoTarget)
        implements Operation {

      /**
       * Create {@link InsertDyn} operation.
       *
       * @param varTarget Variable to insert into.
       * @param varToInsert Variable to insert. The entire variable will be used.
       * @param varOffsetIntoTarget Bit offset to insert into {@code varTarget}. Indexes from low
       *     bits.
       */
      public InsertDyn {
        requireNonNull(varTarget);
        requireNonNull(varToInsert);
        requireNonNull(varOffsetIntoTarget);
      }
    }

    /**
     * Represents a direct call to a function in a {@link Operation} position.
     *
     * <p>This variant is used in for some early {@link IrPass}. Contrast with {@link
     * Ir.CallAndBranch } which represents a direct call in an {@link Terminator} position, but
     * which is only supported in late {@link IrPass}.
     *
     * @param functionId Id of function to call.
     * @param functionArguments Arguments to call function with.
     * @param numReturnValues Number of return values that the function will return.
     */
    public record CallDirectly(
        FunctionId functionId, List<Ir.VariableId> functionArguments, int numReturnValues)
        implements Operation {

      /**
       * Constructor for {@link CallDirectly}.
       *
       * @param functionId Id of function to call.
       * @param functionArguments Arguments to call function with.
       * @param numReturnValues Number of return values that the function will return.
       */
      public CallDirectly {
        requireNonNull(functionId);
        requireNonNull(functionArguments);
      }
    }
  }
}
