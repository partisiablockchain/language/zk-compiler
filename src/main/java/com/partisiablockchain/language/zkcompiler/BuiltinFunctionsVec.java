package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.math.BigInteger;
import java.util.List;

/** Storage class for certain built-in functions. */
public final class BuiltinFunctionsVec {

  private BuiltinFunctionsVec() {}

  static final TypedAst.TypeId STRUCT_DEFINITION_VEC_ID =
      TypedAst.TypeId.newFor("std", "vec", "Vec");

  private static final TypedAst.Type.NamedType TYPE_VEC_ELEMENT =
      new TypedAst.Type.NamedType(TypedAst.TypeId.newFor("ElementT"), List.of());

  private static final TypedAst.Type.NamedType TYPE_VEC =
      new TypedAst.Type.NamedType(STRUCT_DEFINITION_VEC_ID, List.of(TYPE_VEC_ELEMENT));

  private static final int VEC_CAPACITY = 32;

  private static TypedAst.VariableId methodFor(TypedAst.Type.NameableType type, String methodName) {
    return new TypedAst.VariableId(
        TypedAst.EnvKind.FUNCTION_BUILTIN, type.typeId().fullNamePath().resolve(methodName));
  }

  /** The struct definition of {@code std.vec.Vec}. */
  public static final TypedAst.StructDefinition STRUCT_DEFINITION_VEC =
      new TypedAst.StructDefinition(
          STRUCT_DEFINITION_VEC_ID,
          List.of(TYPE_VEC_ELEMENT.toString()),
          List.of(
              new TypedAst.StructDefinition.StructField("vec_size", TypedAst.Type.Int.USIZE),
              new TypedAst.StructDefinition.StructField("vec_capacity", TypedAst.Type.Int.USIZE),
              new TypedAst.StructDefinition.StructField(
                  "vec_alloc", new TypedAst.Type.Array(TYPE_VEC_ELEMENT, VEC_CAPACITY))));

  static Operand vecNew(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType uncheckedResultOperandType) {

    final OperandType.TupleType resultOperandType =
        (OperandType.TupleType) uncheckedResultOperandType;
    final OperandType arrayOperandType = resultOperandType.componentTypes().get(2);

    final Operand size = new Operand.Constant(0, Ir.Type.I32, tmpName);
    final Operand capacity = new Operand.Constant(VEC_CAPACITY, Ir.Type.I32, tmpName);
    final Operand vecArray = IrAmalgamUtility.zeroOperand(arrayOperandType, tmpName);

    return new Operand.Tuple(List.of(size, capacity, vecArray));
  }

  static Operand vecLen(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType uncheckedResultOperandType) {
    final Operand.Tuple vec = (Operand.Tuple) paramOperands.get(0);
    return vec.componentOperands().get(0);
  }

  static Operand vecPush(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType uncheckedResultOperandType) {

    // Get old operands
    final Operand.Tuple oldVec = (Operand.Tuple) paramOperands.get(0);
    final Operand newElement = paramOperands.get(1);
    final Operand.Basic oldSize = oldVec.componentOperands().get(0).expectBasic();
    final Operand.Basic oldCapacity = oldVec.componentOperands().get(1).expectBasic();
    final Operand.Array oldArray = oldVec.componentOperands().get(2).expectArray();

    // Increment
    final var newSize =
        insnAccum.addBinop(
            oldSize.type(),
            TmpName.root("vec_size"),
            Ir.Operation.BinaryOp.ADD_WRAPPING,
            oldSize,
            new Operand.Constant(1, oldSize.type(), TmpName.root("vec_size")));

    final int elementTypeSize = IrAmalgamUtility.operandTypeBitwidth(newElement.operandType());

    final Operand.Basic newElementAmalgam =
        IrAmalgamUtility.emitAmalgamPacking(newElement, insnAccum, Ir.Type.UNIT);
    final var indexOffset =
        insnAccum.addBinop(
            oldSize.type(),
            TmpName.root("index"),
            Ir.Operation.BinaryOp.MULT_SIGNED_WRAPPING,
            oldSize,
            new Operand.Constant(elementTypeSize, oldSize.type(), TmpName.root("type_size")));

    final var newArray =
        IrAmalgamUtility.emitAmalgamSplice(
            oldArray, newElementAmalgam, indexOffset, tmpName, insnAccum);

    // Create result
    final var updatedVec = new Operand.Tuple(List.of(newSize, oldCapacity, newArray));
    final var pushResult = new Operand.Tuple(List.of());
    return new Operand.Tuple(List.of(updatedVec, pushResult));
  }

  static Operand vecPop(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType uncheckedResultOperandType) {

    // Get old operands
    final Operand.Tuple oldVec = (Operand.Tuple) paramOperands.get(0);
    final Operand.Basic oldSize = oldVec.componentOperands().get(0).expectBasic();
    final Operand.Basic oldCapacity = oldVec.componentOperands().get(1).expectBasic();
    final Operand.Basic oldArray = oldVec.componentOperands().get(2).expectBasic();

    // Decrement
    final BigInteger decAmount =
        BigInteger.ONE.shiftLeft(oldSize.type().width()).subtract(BigInteger.ONE);
    final var newSize =
        insnAccum.addBinop(
            oldSize.type(),
            TmpName.root("vec_size"),
            Ir.Operation.BinaryOp.ADD_WRAPPING,
            oldSize,
            new Operand.Constant(decAmount, oldSize.type(), TmpName.root("vec_size")));

    final int elementTypeSize = IrAmalgamUtility.operandTypeBitwidth(uncheckedResultOperandType);
    final var indexOffset =
        insnAccum.addBinop(
            oldSize.type(),
            TmpName.root("index"),
            Ir.Operation.BinaryOp.MULT_SIGNED_WRAPPING,
            newSize,
            new Operand.Constant(elementTypeSize, oldSize.type(), TmpName.root("type_size")));

    final var popResult =
        IrAmalgamUtility.emitAmalgamExtraction(
            uncheckedResultOperandType, oldArray, indexOffset, insnAccum, tmpName);

    // Create result
    final var updatedVec = new Operand.Tuple(List.of(newSize, oldCapacity, oldArray));
    return new Operand.Tuple(List.of(updatedVec, popResult));
  }

  private static final BuiltinFunctions.FnDefinition VEC_NEW =
      BuiltinFunctions.FnDefinition.from(
          methodFor(TYPE_VEC, "new"),
          List.of(TYPE_VEC_ELEMENT.toString()),
          List.of(),
          TYPE_VEC,
          BuiltinFunctionsVec::vecNew,
          false);

  private static final BuiltinFunctions.FnDefinition VEC_PUSH =
      BuiltinFunctions.FnDefinition.from(
          methodFor(TYPE_VEC, "push"),
          List.of(TYPE_VEC_ELEMENT.toString()),
          List.of(TYPE_VEC, TYPE_VEC_ELEMENT),
          TypedAst.Type.Tuple.UNIT,
          BuiltinFunctionsVec::vecPush,
          true);

  private static final BuiltinFunctions.FnDefinition VEC_POP =
      BuiltinFunctions.FnDefinition.from(
          methodFor(TYPE_VEC, "pop"),
          List.of(TYPE_VEC_ELEMENT.toString()),
          List.of(TYPE_VEC),
          TYPE_VEC_ELEMENT,
          BuiltinFunctionsVec::vecPop,
          true);

  private static final BuiltinFunctions.FnDefinition VEC_LEN =
      BuiltinFunctions.FnDefinition.from(
          methodFor(TYPE_VEC, "len"),
          List.of(TYPE_VEC_ELEMENT.toString()),
          List.of(TYPE_VEC),
          TypedAst.Type.Int.USIZE,
          BuiltinFunctionsVec::vecLen,
          false);

  /** Standard library function definitions. */
  static final List<BuiltinFunctions.FnDefinition> FUNCTIONS_LIST =
      List.of(VEC_NEW, VEC_PUSH, VEC_POP, VEC_LEN);

  /** Standard library struct definitions. */
  public static final List<TypedAst.StructDefinition> STRUCTS = List.of(STRUCT_DEFINITION_VEC);

  /** Standard library types. */
  public static final List<TypeCheck.TypeAliasInfo> TYPES = List.of();
}
