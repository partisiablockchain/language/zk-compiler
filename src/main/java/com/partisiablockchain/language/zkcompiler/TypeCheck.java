package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.FormatMethod;
import com.partisiablockchain.language.zkparser.ZkAst;
import com.partisiablockchain.language.zkparser.ZkAstPretty;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Validates that Rust input AST is well-typed.
 *
 * <p>Note on terminology:
 *
 * <ul>
 *   <li><b>Parameter</b>: Names of value and type inputs. Item {@code fn abc<T>(x: T, y: T)}
 *       contains three parameters: {@code T}, {@code x} and {@code y}.
 *   <li><b>Argument</b>: Specific values or types that are given as input. Expression {@code
 *       abc::<i32>(123, 531)} contains three arguments: {@code i32}, {@code 123} and {@code 531}.
 * </ul>
 */
public final class TypeCheck {

  private TypeCheck() {}

  /**
   * Encodes the location where a give variable or type have been defined, relative to the file
   * being type checked.
   */
  public enum SourceScope {
    /** Associated variable have been imported. Associated variables can overwrite each other. */
    IMPORT,
    /**
     * Associated variable have been defined at item-level in this file. Associated variables can
     * overwrite {@link #IMPORT} variables, but not {@link #ITEM_LEVEL} variables.
     */
    ITEM_LEVEL,
    /**
     * Associated variable have been defined within a function in this file. Associated variables
     * can overwrite everything else.
     */
    FUNCTION_LEVEL
  }

  private static final Set<List<String>> ALLOWED_PBC_ZK_TEST_EQ_NAMES =
      Set.of(List.of("pbc_zk", "test_eq"), List.of("test_eq"));

  /** Information about variable for type checking. */
  private interface VariableInfo {
    TypedAst.VariableId uniqueVariableId();

    ZkAst.PosInterval originalPosition();
  }

  record VariableInfoSimple(
      TypedAst.VariableId uniqueVariableId,
      TypedAst.Type type,
      boolean isMutable,
      ZkAst.PosInterval originalPosition)
      implements VariableInfo {
    public VariableInfoSimple {
      requireNonNull(uniqueVariableId);
      requireNonNull(type);
      requireNonNull(originalPosition);
    }
  }

  /** Information about a pre-monomorphed functions which may contain generic type parameters. */
  record FunctionInfoGeneric(
      TypedAst.VariableId uniqueVariableId,
      List<String> typeParameterNames,
      List<TypedAst.Type> valueParameterTypesWithTypeParameters,
      TypedAst.Type returnTypeWithTypeParameters,
      ZkAst.PosInterval originalPosition)
      implements VariableInfo {

    /**
     * Constructor.
     *
     * @param uniqueVariableId Unique function id with absolute path.
     * @param typeParameterNames Names of individual type arguments.
     * @param valueParameterTypesWithTypeParameters Types of value arguments. May include references
     *     to {@code typeParameterNames}.
     * @param returnTypeWithTypeParameters Type of return value. May include references to {@code
     *     typeParameterNames}.
     * @param originalPosition Original position that function was declared.
     */
    public FunctionInfoGeneric {
      requireNonNull(uniqueVariableId);
      requireNonNull(typeParameterNames);
      requireNonNull(valueParameterTypesWithTypeParameters);
      requireNonNull(returnTypeWithTypeParameters);
      requireNonNull(originalPosition);
    }
  }

  /**
   * Information about a monomorphed functions where all type parameters have been replaced with
   * concrete types.
   */
  private record FunctionInfoMonomorphed(
      TypedAst.VariableId uniqueVariableId,
      List<TypedAst.Type> valueParameterTypes,
      TypedAst.Type returnType) {

    /**
     * Constructor.
     *
     * @param uniqueVariableId Unique function id with absolute path.
     * @param valueParameterTypes Types of value arguments. May not include references to type
     *     generics.
     * @param returnType Type of return value. May not include references to type generics.
     */
    public FunctionInfoMonomorphed {
      requireNonNull(uniqueVariableId);
      requireNonNull(valueParameterTypes);
      requireNonNull(returnType);
    }
  }

  /** Information about possibly-generic type aliases. These are stored in the environment. */
  record TypeAliasInfo(
      TypedAst.TypeId typeId,
      List<TypedAst.TypeId> typeParameterNames,
      TypedAst.Type typeWithTypeParameters,
      ZkAst.PosInterval originalPosition) {

    /**
     * Constructor.
     *
     * @param typeId Unique id with absolute path.
     * @param typeParameterNames Names of individual type arguments.
     * @param typeWithTypeParameters The referenced type. May include references to {@code
     *     typeParameterNames}.
     */
    public TypeAliasInfo {
      requireNonNull(typeId);
      requireNonNull(typeParameterNames);
      requireNonNull(typeWithTypeParameters);
      requireNonNull(originalPosition);
    }
  }

  /** Information about monomorphed struct types. */
  private record StructInfoMonomorphed(
      TypedAst.Type.NamedType typeName, Map<String, TypedAst.Type> fieldByName) {

    /**
     * Constructor.
     *
     * @param typeName Type of structure. May not include references to type generics.
     * @param fieldByName Individual fields, keyed by their name. May not include references to type
     *     generics.
     */
    public StructInfoMonomorphed {
      requireNonNull(typeName);
      requireNonNull(fieldByName);
    }

    /**
     * Determines the type of a field, from that field's name.
     *
     * @param fieldName Name of the field.
     * @return Null if and only if the field does not exist in the struct definition.
     */
    public TypedAst.Type typeOfField(final String fieldName) {
      return fieldByName.get(fieldName);
    }
  }

  /**
   * Wrapper for {@link NamePath.Absolute} paths along with their kinds and source scopes.
   *
   * @param path Path of variable.
   * @param kind Kind of variable.
   * @param sourceScope Source of variable.
   */
  private record PathWithKind(
      NamePath.Absolute path, TypedAst.EnvKind kind, SourceScope sourceScope) {

    public PathWithKind {
      requireNonNull(path);
      requireNonNull(kind);
      requireNonNull(sourceScope);
    }
  }

  /** Context of the current file being type checked, including all top-level items. */
  private record FileContext(
      NamePath.Absolute fileModulePath,
      Map<NamePath.Relative, PathWithKind> variableIdsToAbsolutePaths,
      Map<NamePath.Absolute, VariableInfo> variableEnvironment,
      Map<NamePath.Absolute, TypeAliasInfo> typeEnvironment,
      Map<TypedAst.TypeId, TypedAst.StructDefinition> structDefinitions) {

    /** Immutable empty context. */
    public static FileContext empty(final NamePath.Absolute fileModulePath) {
      return new FileContext(
          fileModulePath, new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>());
    }

    /**
     * Create new {@link FileContext}.
     *
     * @param fileModulePath Module path for the module being type checked.
     * @param variableIdsToAbsolutePaths Mapping from relative names to absolute paths.
     * @param variableEnvironment Variable and function environment.
     * @param typeEnvironment Type environment; maps from string names to an abstract type, possibly
     *     a named type; these require further lookup in the structDefinitions environment.
     * @param structDefinitions Struct definition environment. Named types can be found here.
     */
    public FileContext {
      requireNonNull(fileModulePath);
      requireNonNull(variableIdsToAbsolutePaths);
      requireNonNull(variableEnvironment);
      requireNonNull(typeEnvironment);
      requireNonNull(structDefinitions);
    }

    /**
     * Create child context of this file context.
     *
     * @param returnType Expected return type in the given context.
     * @return Newly created context.
     */
    Context childContext(final TypedAst.Type returnType) {
      return new Context(false, new HashMap<>(), this, returnType);
    }

    /**
     * Set global variable.
     *
     * @param variableName Name of variable.
     * @param variableInfo Variable information.
     * @param errorsAccum Error accumulator used for throwing errors.
     */
    void setVariable(
        String variableName, final VariableInfo variableInfo, final ErrorAccum errorsAccum) {
      final NamePath.Relative relativePath = new NamePath.Relative(variableName);
      final NamePath.Absolute absolutePath = variableInfo.uniqueVariableId().fullNamePath();
      variableEnvironment().put(absolutePath, variableInfo);
      final var pathWithKind =
          new PathWithKind(
              absolutePath, variableInfo.uniqueVariableId().variableKind(), SourceScope.ITEM_LEVEL);

      putIntoVariableEnvironment(
          relativePath, pathWithKind, errorsAccum.withPos(variableInfo.originalPosition()));
    }

    /**
     * Gets information about the given variable with path.
     *
     * @param path Path of variable.
     * @return Variable information for given variable, or null.
     */
    VariableInfo getVariable(NamePath.Absolute path) {
      return variableEnvironment.get(path);
    }

    /**
     * Extends {@link #variableIdsToAbsolutePaths} with additional functions.
     *
     * @param kind Kind of the functions.
     * @param modulePath Path of the module these functions were defined in.
     * @param newFunctions The function information themselves.
     * @param errorsAccum Error accumulator used for throwing errors.
     */
    void addFunctionsRelative(
        final TypedAst.EnvKind kind,
        final NamePath.Absolute modulePath,
        final List<FunctionInfoGeneric> newFunctions,
        final ErrorAccum errorsAccum,
        SourceScope sourceScope) {
      for (final FunctionInfoGeneric funcInfo : newFunctions) {
        final NamePath.Absolute absolutePath = funcInfo.uniqueVariableId.fullNamePath();
        final NamePath.Relative relativePath = modulePath.relativize(absolutePath);
        final var pathWithKind = new PathWithKind(absolutePath, kind, sourceScope);
        putIntoVariableEnvironment(
            relativePath, pathWithKind, errorsAccum.withPos(funcInfo.originalPosition()));
      }
    }

    /**
     * Extends {@link #variableEnvironment} with additional functions.
     *
     * @param newFunctions Functions to add to environment.
     */
    void addFunctionsAbsolute(final List<FunctionInfoGeneric> newFunctions) {
      for (final FunctionInfoGeneric funcInfo : newFunctions) {
        final NamePath.Absolute absolutePath = funcInfo.uniqueVariableId.fullNamePath();
        variableEnvironment.put(absolutePath, funcInfo);
      }
    }

    /**
     * Extends {@link #structDefinitions} with additional structs.
     *
     * @param newStructDefinitions Structs to add to environment.
     */
    void extendStructDefinitions(final List<TypedAst.StructDefinition> newStructDefinitions) {
      for (final var def : newStructDefinitions) {
        structDefinitions.put(def.typeId(), def);
      }
    }

    /**
     * Extend context with a list of type aliases.
     *
     * @param additionalTypes Types to create for.
     * @param modulePath Module path to create relative to.
     */
    void extendTypeEnvironment(
        final TypedAst.EnvKind kind,
        final List<TypeCheck.TypeAliasInfo> additionalTypes,
        final NamePath.Absolute modulePath,
        final ErrorAccum errorsAccum,
        final SourceScope sourceScope) {
      for (final TypeCheck.TypeAliasInfo typeAlias : additionalTypes) {
        final NamePath.Absolute absolutePath = typeAlias.typeId().fullNamePath();
        final NamePath.Relative relativePath = modulePath.relativize(absolutePath);
        typeEnvironment().put(absolutePath, typeAlias);
        putIntoVariableEnvironment(
            relativePath,
            new PathWithKind(absolutePath, kind, sourceScope),
            errorsAccum.withPos(typeAlias.originalPosition()));
      }
    }

    /**
     * Extends the variable environment with the given variable, adding errors if variable have
     * already been defined at an item level.
     *
     * @param namePath Relative name path of the variable.
     * @param newPathWithKind Name path with kind.
     * @param errorsAccum Error accumulator.
     */
    private void putIntoVariableEnvironment(
        final NamePath.Relative namePath,
        final PathWithKind newPathWithKind,
        final ErrorAccum errorsAccum) {
      final PathWithKind oldPathWithKind =
          variableIdsToAbsolutePaths.put(namePath, newPathWithKind);
      final boolean overwriteAllowed =
          oldPathWithKind == null || oldPathWithKind.sourceScope() != SourceScope.ITEM_LEVEL;
      if (!overwriteAllowed) {
        errorsAccum.add(
            "%s '%s' conflicts with %s %s",
            newPathWithKind.kind().humanKindName(),
            namePath,
            oldPathWithKind.kind().humanKindName().toLowerCase(Locale.ROOT),
            oldPathWithKind.path());
      }
    }
  }

  /**
   * During type checking, context holds type information for variables in scope at a given point in
   * the program execution.
   */
  static final class Context {

    private final boolean inLoop;
    private final Map<NamePath.Relative, VariableInfo> variables;
    private final FileContext fileContext;
    private final TypedAst.Type returnType;

    /**
     * Constructor.
     *
     * @param inLoop Whether the current sub-tree is contained in a loop.
     * @param variables Standard variable and function environment.
     * @param fileContext Global context of the file being checked.
     * @param returnType Type required for return expressions. May be null in contexts where returns
     *     are disallowed.
     */
    public Context(
        final boolean inLoop,
        final Map<NamePath.Relative, VariableInfo> variables,
        final FileContext fileContext,
        final TypedAst.Type returnType) {
      this.inLoop = inLoop;
      this.variables = requireNonNull(variables);
      this.fileContext = requireNonNull(fileContext);
      this.returnType = returnType;
    }

    //// State retrievals

    Context childContext() {
      return childContext(this.inLoop);
    }

    Context childContext(boolean inLoop) {
      return new Context(inLoop, new HashMap<>(this.variables), this.fileContext, this.returnType);
    }

    TypedAst.Type returnType() {
      return this.returnType;
    }

    FileContext fileContext() {
      return this.fileContext;
    }

    boolean inLoop() {
      return this.inLoop;
    }

    private <T> T getInEnv(Function<NamePath.Absolute, T> getter, final ZkAst.Path astPath) {
      return getInEnv(getter, new NamePath.Relative(astPath.path()));
    }

    private <T> T getInEnv(
        Function<NamePath.Absolute, T> getter, final NamePath.Relative relativePath) {
      final PathWithKind absolutePathAndKind =
          fileContext.variableIdsToAbsolutePaths().get(relativePath);
      if (absolutePathAndKind == null) {
        return null;
      }
      return getter.apply(absolutePathAndKind.path());
    }

    VariableInfo getVariable(final ZkAst.Path path) {
      return getVariable(new NamePath.Relative(path.path()));
    }

    VariableInfo getVariable(final NamePath.Relative relativePath) {
      final var local = variables.get(relativePath);
      return local != null ? local : getInEnv(this::getVariableByAbsolute, relativePath);
    }

    VariableInfo getVariableByAbsolute(final NamePath.Absolute absolutePath) {
      return fileContext.getVariable(absolutePath);
    }

    /** Finds type information. Does <b>NOT</b> perform type substitution. */
    TypeAliasInfo getType(final ZkAst.Path typePath) {
      return getInEnv(fileContext.typeEnvironment()::get, typePath);
    }

    /** Finds method information. Does <b>NOT</b> perform type substitution. */
    VariableInfo getStaticField(
        ErrorAccum errorsAccum, TypedAst.Type uncheckedType, final String staticFieldName) {

      if (uncheckedType instanceof TypedAst.Type.NameableType type) {
        final NamePath.Absolute absolutePath =
            type.typeId().fullNamePath().resolve(staticFieldName);
        final VariableInfo method = getVariableByAbsolute(absolutePath);
        if (method != null) {
          return method;
        }
      }

      if (!(uncheckedType instanceof TypedAst.Type.ErrorType)) {
        errorsAccum.add("%s::%s is not in scope", uncheckedType, staticFieldName);
      }
      return null;
    }

    /**
     * Updates the context to point from the given variable name to the given variable information.
     *
     * @param variableName Variable name to set.
     * @param variableInfo New variable information to override with.
     * @param errorsAccum Error accumulator for possible errors.
     */
    void setVariable(
        String variableName, final VariableInfo variableInfo, final ErrorAccum errorsAccum) {
      final NamePath.Relative relativePath = new NamePath.Relative(variableName);
      variables.put(relativePath, variableInfo);
    }

    TypedAst.StructDefinition getStructDefinition(final TypedAst.TypeId typeId) {
      return fileContext.structDefinitions().get(typeId);
    }
  }

  /** Unknown position. */
  static final ZkAst.PosInterval POSITION_UNKNOWN = new ZkAst.PosInterval(-1, -1, -1, -1);

  /** Array length size type. */
  static final TypedAst.Type ARRAY_SIZE_TYPE = TypedAst.Type.Int.USIZE;

  /** Type to produce for invalid expressions. */
  public static TypedAst.Type INVALID_EXPRESSION_TYPE = new TypedAst.Type.ErrorType();

  /** Type to produce for invalid expressions. */
  public static TypedAst.TypedExpression INVALID_EXPRESSION_EXPR =
      new TypedAst.TypedExpression(
          TypedAst.Expression.TupleConstructor.UNIT,
          INVALID_EXPRESSION_TYPE,
          false,
          false,
          new TypedAst.PosInterval(null, POSITION_UNKNOWN));

  /**
   * Type checks entire Zk program, and produces a typed AST. Collects as many type errors as
   * feasible before throwing errors.
   *
   * @param programFiles Files in program to type check.
   * @param config translation config to type check under
   * @return Typed program AST.
   * @exception TypeCheckException In case of one or more type errors.
   */
  public static Result typeCheckProgram(
      final List<ProgramFile> programFiles, TranslationConfig config) {
    final ArrayList<ErrorPresenter.ErrorLine> errors = new ArrayList<>();

    // Type checking is done in reverse order.
    // A more correct implementation should perform dependency analysis over
    // modules, and type check them in order.
    final List<TypedAst.Program> typedSubPrograms = new ArrayList<>();
    final List<List<TestInformation>> programTestInformation = new ArrayList<>();
    final Map<NamePath.Absolute, TypedAst.Program> alreadyCheckedFiles = new HashMap<>();
    for (int idx = programFiles.size() - 1; idx >= 0; idx--) {

      // Type check individual file
      final ProgramFile progFile = programFiles.get(idx);
      final ErrorAccum errorsAccum =
          new ErrorAccum(progFile.sourcePath(), errors, POSITION_UNKNOWN);
      final Result typedProgramResult =
          typeCheckProgramFile(progFile, errorsAccum, alreadyCheckedFiles, config);

      // Bookkeeping
      typedSubPrograms.add(typedProgramResult.program());
      programTestInformation.add(typedProgramResult.testInformation());
      alreadyCheckedFiles.put(progFile.modulePath(), typedProgramResult.program());
    }

    // Add library structs
    typedSubPrograms.add(new TypedAst.Program(List.of(), BuiltinFunctions.STRUCTS, List.of()));

    // Combine programs
    final TypedAst.Program combinedProgram = TypedAst.Program.combine(typedSubPrograms);

    // Perform information flow check on full program
    final ErrorAccum errorsAccum = new ErrorAccum(Path.of("UNKNOWN"), errors, POSITION_UNKNOWN);
    TypeCheckInformationFlow.checkProgram(errorsAccum, combinedProgram);

    return new Result(
        combinedProgram,
        programTestInformation.stream().flatMap(List::stream).toList(),
        List.copyOf(errors));
  }

  /**
   * Individual program file with AST and module name.
   *
   * @param ast Untyped AST of program file.
   * @param sourcePath Source path of program file.
   */
  public record ProgramFile(ZkAst.Program ast, Path sourcePath) {

    /**
     * The module path of the program file.
     *
     * <p>The module path is defined as 'crate' followed by the suffix of the source path after the
     * last 'src' name.
     *
     * @return Module path of program file.
     */
    public NamePath.Absolute modulePath() {
      ArrayList<String> modulePath = new ArrayList<>();
      modulePath.add(moduleNameFromPath(sourcePath));
      for (int i = sourcePath.getNameCount() - 2; i >= 0; i--) {
        String moduleName = sourcePath.getName(i).toString();
        if (moduleName.equals("src")) {
          break;
        }
        modulePath.add(moduleName);
      }
      modulePath.add("crate");
      Collections.reverse(modulePath);
      return new NamePath.Absolute(modulePath);
    }
  }

  static String moduleNameFromPath(final Path path) {
    final String moduleName = path.getName(path.getNameCount() - 1).toString();
    return moduleName.substring(0, moduleName.length() - 3);
  }

  /**
   * Stores result of translating program. Always check errors field before using the program field;
   * the program might be malformed if any {@link #errors} occured.
   *
   * @param program Typed program. Possibly malformed if {@link #errors} occured. Not nullable.
   * @param testInformation Information about specific macro tests. Not nullable, possibly empty.
   * @param errors Errors that occured during type checking.
   */
  public record Result(
      TypedAst.Program program,
      List<TestInformation> testInformation,
      List<ErrorPresenter.ErrorLine> errors) {

    /**
     * Constructor for {@link Result}.
     *
     * @param program Typed program. Possibly malformed if {@link #errors} occured. Not nullable.
     * @param testInformation Information about specific macro tests. Not nullable, possibly empty.
     * @param errors Errors that occured during type checking.
     */
    public Result {
      requireNonNull(program);
      requireNonNull(testInformation);
    }
  }

  /**
   * Type checks entire Zk program.
   *
   * @param programFile Individual program file to type check.
   * @param errorsAccum Appendable list of errors.
   * @param alreadyCheckedFiles Map of library program files that have been parsed.
   * @param config translation config to type check under
   * @return Typed program AST.
   */
  private static Result typeCheckProgramFile(
      final ProgramFile programFile,
      final ErrorAccum errorsAccum,
      final Map<NamePath.Absolute, TypedAst.Program> alreadyCheckedFiles,
      TranslationConfig config) {

    final ZkAst.Program program = programFile.ast();

    // Create initial global context
    final FileContext fileContext = FileContext.empty(programFile.modulePath());
    fileContext.extendTypeEnvironment(
        TypedAst.EnvKind.TYPE_BUILTIN,
        BuiltinFunctionsRustStd.createPrimitiveTypeEnvironment(),
        new NamePath.Absolute(),
        errorsAccum,
        SourceScope.IMPORT);

    // Import every function to absolute path
    final var functionsWithAbsolutePath =
        BuiltinFunctions.lookupModule(new NamePath.Absolute()).functionItems().stream()
            .map(BuiltinFunctions.FnDefinition::typeInfo)
            .toList();
    fileContext.addFunctionsAbsolute(functionsWithAbsolutePath);

    // Initialize imported builtins
    final List<ZkAst.UseDeclaration> useDecls = new ArrayList<>();
    useDecls.add(new ZkAst.UseDeclaration(List.of(), List.of("std"), POSITION_UNKNOWN));
    useDecls.addAll(program.useDeclarations().stream().map(ZkAst.ProgramItem::item).toList());
    for (final ZkAst.UseDeclaration useDecl : useDecls) {
      final ErrorAccum errorsAccumUseDecl = errorsAccum.withPos(useDecl);
      final NamePath.Absolute modulePath = new NamePath.Absolute(useDecl.modulePath());

      // Builtin module?
      final BuiltinFunctions.ModuleDefinition module = BuiltinFunctions.lookupModule(modulePath);
      if (module != null) {
        importBuiltinModule(useDecl, module, fileContext, errorsAccumUseDecl);
        continue;
      }

      // User module?
      final TypedAst.Program userModule = alreadyCheckedFiles.get(modulePath);
      if (userModule != null) {
        importUserModule(useDecl, modulePath, userModule, fileContext, errorsAccumUseDecl);
        continue;
      }

      // Throw if neither builtin nor user-defined import exists
      errorsAccumUseDecl.add("Unknown module");
    }

    // Struct names to environment
    fileContext.extendTypeEnvironment(
        TypedAst.EnvKind.TYPE_USER_DEFINED,
        program.structs().stream()
            .map(ZkAst.ProgramItem::item)
            .map(s -> typeInfoForUntypedStruct(s, fileContext.fileModulePath()))
            .toList(),
        fileContext.fileModulePath(),
        errorsAccum,
        SourceScope.ITEM_LEVEL);

    // Type alias to environment
    fileContext.extendTypeEnvironment(
        TypedAst.EnvKind.TYPE_USER_DEFINED,
        program.typeAliases().stream()
            .map(ZkAst.ProgramItem::item)
            .map(defItem -> typeCheckTypeAlias(defItem, errorsAccum, fileContext))
            .toList(),
        fileContext.fileModulePath(),
        errorsAccum,
        SourceScope.ITEM_LEVEL);

    // Type check struct definition in environment, and replace environment
    final List<TypedAst.StructDefinition> structDefinitions =
        program.structs().stream()
            .map(defItem -> typeCheckStructDefinition(defItem, errorsAccum, fileContext))
            .toList();
    fileContext.extendStructDefinitions(structDefinitions);

    // Add const declaration to environment
    final List<VariableInfoSimple> constantDefinitionVariableInfos = new ArrayList<>();
    for (final ZkAst.ProgramItem<ZkAst.ConstantDefinition> constantItem : program.constants()) {
      final ZkAst.ConstantDefinition constant = constantItem.item();
      final var constantDef =
          new VariableInfoSimple(
              new TypedAst.VariableId(
                  TypedAst.EnvKind.CONSTANT,
                  fileContext.fileModulePath().resolve(constant.identifier())),
              typeCheckType(constant.type(), errorsAccum, fileContext.childContext(null)),
              false,
              constant.position());
      fileContext.setVariable(constant.identifier(), constantDef, errorsAccum);
      constantDefinitionVariableInfos.add(constantDef);
    }

    // Type check function declarations
    // Explicitly same order as program.functions, for later zipping.
    final List<FunctionInfoGeneric> functionDeclarations =
        program.functions().stream()
            .map(
                functionItem -> {
                  final ZkAst.Function function = functionItem.item();
                  final Context returnTypeContext = fileContext.childContext(null);
                  final TypedAst.Type functionReturnType =
                      typeCheckType(function.returnType(), errorsAccum, returnTypeContext);

                  final Context functionContext = fileContext.childContext(functionReturnType);
                  final List<TypedAst.Type> parameterTypes =
                      function.parameters().stream()
                          .map(x -> typeCheckType(x.type(), errorsAccum, functionContext))
                          .toList();

                  final TypedAst.VariableId uniqueVariableId =
                      new TypedAst.VariableId(
                          TypedAst.EnvKind.FUNCTION,
                          fileContext.fileModulePath().resolve(function.identifier()));

                  return new FunctionInfoGeneric(
                      uniqueVariableId,
                      List.of(),
                      parameterTypes,
                      functionReturnType,
                      function.position());
                })
            .toList();

    fileContext.addFunctionsRelative(
        TypedAst.EnvKind.FUNCTION,
        fileContext.fileModulePath(),
        functionDeclarations,
        errorsAccum,
        SourceScope.ITEM_LEVEL);
    fileContext.addFunctionsAbsolute(functionDeclarations);

    // Type check constants
    final List<TypedAst.ConstantDefinition> constantDefinitions = new ArrayList<>();
    for (int constantIdx = 0; constantIdx < program.constants().size(); constantIdx++) {

      final ZkAst.ProgramItem<ZkAst.ConstantDefinition> constantItem =
          program.constants().get(constantIdx);

      final ZkAst.ConstantDefinition constant = constantItem.item();
      final VariableInfoSimple definition = constantDefinitionVariableInfos.get(constantIdx);

      final TypedAst.TypedExpression typedInitExpr =
          typeCheckExpression(
              constant.initExpr(), errorsAccum, fileContext.childContext(null), definition.type());

      final NamePath.Absolute absolutePath =
          fileContext.fileModulePath().resolve(constant.identifier());

      assertIsSubtype(typedInitExpr.resultType(), definition.type(), errorsAccum.withPos(constant));

      if (!typedInitExpr.isConstantExpression()) {
        errorsAccum.add(constant.initExpr(), "Constant initializer expression must be constant");
      }

      constantDefinitions.add(
          new TypedAst.ConstantDefinition(
              new TypedAst.VariableId(TypedAst.EnvKind.CONSTANT, absolutePath),
              typedInitExpr,
              new TypedAst.PosInterval(programFile.sourcePath(), constant.position())));
    }

    // Type check functions definitions
    final List<TypedAst.Function> typedFunctions = new ArrayList<>();
    for (int uniqueVariableIdx = 0;
        uniqueVariableIdx < program.functions().size();
        uniqueVariableIdx++) {
      typedFunctions.add(
          typeCheckFunction(
              requireNonNull(program.functions().get(uniqueVariableIdx)),
              requireNonNull(functionDeclarations.get(uniqueVariableIdx)),
              errorsAccum,
              fileContext));
    }

    // Type check macro invocations
    final List<TestInformation> testInformations =
        typeCheckMacroItems(program.macroItems(), typedFunctions, config, errorsAccum, fileContext);

    return new Result(
        new TypedAst.Program(
            List.copyOf(typedFunctions),
            List.copyOf(structDefinitions),
            List.copyOf(constantDefinitions)),
        testInformations,
        null);
  }

  /**
   * Type checks top level macro invocations. Must be a pbc_zk::test_eq macro.
   *
   * @param macroInvocations the untyped macro invocations
   * @param typedFunctions list of typed functions. Expanded macro tests are added to this.
   * @param config translation config
   * @param errorsAccum errors accumulator
   * @param fileContext file context
   * @return list of test information for the program
   */
  private static List<TestInformation> typeCheckMacroItems(
      List<ZkAst.Expression.MacroInvocation> macroInvocations,
      List<TypedAst.Function> typedFunctions,
      TranslationConfig config,
      ErrorAccum errorsAccum,
      FileContext fileContext) {
    final List<TestInformation> testInformations = new ArrayList<>();
    for (final ZkAst.Expression.MacroInvocation untypedMacro : macroInvocations) {
      if (!ALLOWED_PBC_ZK_TEST_EQ_NAMES.contains(untypedMacro.macroIdentifier())) {
        errorsAccum.add(untypedMacro, "Unknown macro invocation");
        continue;
      }
      int argumentSize = untypedMacro.parameterExprs().size();
      if (argumentSize < 2 || 4 < argumentSize) {
        errorsAccum.add(
            untypedMacro, "pbc_zk::test_eq! requires 2-4 arguments - got %d", argumentSize);
        continue;
      }

      final TypedAst.TypedExpression testExpression =
          typeCheckExpression(
              untypedMacro.parameterExprs().get(0),
              errorsAccum,
              fileContext.childContext(null),
              null);
      typeCheckExpression(
          untypedMacro.parameterExprs().get(1), errorsAccum, fileContext.childContext(null), null);
      BigInteger result =
          secretExpressionToBigInt(untypedMacro.parameterExprs().get(1), errorsAccum);

      List<BigInteger> secretInputs = List.of();
      if (argumentSize >= 3) {
        typeCheckExpression(
            untypedMacro.parameterExprs().get(2),
            errorsAccum,
            fileContext.childContext(null),
            null);
        secretInputs =
            secretExpressionToBigIntList(untypedMacro.parameterExprs().get(2), errorsAccum);
      }
      List<BigInteger> secretOutputs = List.of();
      if (argumentSize == 4) {
        typeCheckExpression(
            untypedMacro.parameterExprs().get(3),
            errorsAccum,
            fileContext.childContext(null),
            null);
        secretOutputs =
            secretExpressionToBigIntList(untypedMacro.parameterExprs().get(3), errorsAccum);
      }

      // Expand macros
      if (config.cfgTests()) {
        final TypedAst.Function function = expandMacro(testExpression, untypedMacro);
        final TestInformation testInformation =
            new TestInformation(
                result,
                secretInputs,
                secretOutputs,
                untypedMacro.position(),
                function.identifier(),
                null);
        testInformations.add(testInformation);
        typedFunctions.add(function);
      }
    }
    return testInformations;
  }

  /**
   * Converts an expression to concrete constant value. Error if a constant value cannot be
   * determined.
   *
   * @param secretInput the expression to be converted
   * @param errorsAccum error accumulator
   * @return the {@link BigInteger}
   */
  private static BigInteger secretExpressionToBigInt(
      ZkAst.Expression secretInput, final ErrorAccum errorsAccum) {
    if (secretInput instanceof ZkAst.Expression.IntLiteral intLiteral) {
      return intLiteral.value();
    }
    if (secretInput instanceof ZkAst.Expression.Unary unary
        && unary.operation().equals(ZkAst.Unop.NEGATE)
        && unary.expr1() instanceof ZkAst.Expression.IntLiteral intLiteral) {
      return intLiteral.value().negate();
    }
    if (secretInput instanceof ZkAst.Expression.BoolLiteral boolLiteral) {
      return boolLiteral.value() ? BigInteger.ONE : BigInteger.ZERO;
    }
    if (secretInput instanceof ZkAst.Expression.TupleConstructor tuple
        && tuple.exprs().size() == 0) {
      return null;
    }

    errorsAccum.add(secretInput.position(), "Expected integer literal");
    return null;
  }

  /**
   * Coverts an array constructor expression to a list of constant values. Error if a constant
   * values cannot be determined.
   *
   * @param secretInput the expression to be converted
   * @param errorsAccum error accumulator
   * @return list of {@link BigInteger}s
   */
  private static List<BigInteger> secretExpressionToBigIntList(
      ZkAst.Expression secretInput, final ErrorAccum errorsAccum) {
    if (secretInput instanceof ZkAst.Expression.ArrayConstructorExplicit array) {
      return array.elementExpressions().stream()
          .map(s -> secretExpressionToBigInt(s, errorsAccum))
          .toList();
    } else if (secretInput instanceof ZkAst.Expression.ArrayConstructorRepeat array) {
      if (array.sizeExpression() instanceof ZkAst.Expression.IntLiteral sizeExpression) {
        final BigInteger value = secretExpressionToBigInt(array.repeatExpression(), errorsAccum);
        return Collections.<BigInteger>nCopies(sizeExpression.value().intValue(), value);
      }
      // Caught by the type checker
      return List.of();
    }

    errorsAccum.add(secretInput.position(), "Expected array constructor");
    return List.of();
  }

  /**
   * Converts the test expression of a test macro invocation into a typed function.
   *
   * @param typedExpression the typed test expression of a macro invocation
   * @param macroItem the untyped macro invocation
   * @return the typed test function
   */
  static TypedAst.Function expandMacro(
      TypedAst.TypedExpression typedExpression, ZkAst.Expression.MacroInvocation macroItem) {
    ZkAst.PosInterval pos = macroItem.position();
    String name = "test_%d_%d".formatted(pos.lineIdxStart(), pos.columnIdxStart());
    return new TypedAst.Function(
        TypedAst.VariableId.newFor(TypedAst.EnvKind.FUNCTION, name),
        List.of(),
        typedExpression.resultType(),
        typedExpression,
        ZkAst.ItemVisibility.PUBLIC,
        null,
        true);
  }

  private static void importBuiltinModule(
      final ZkAst.UseDeclaration useDecl,
      final BuiltinFunctions.ModuleDefinition module,
      final FileContext fileContext,
      final ErrorAccum errorsAccumUseDecl) {

    // Extend context with imported data
    final List<FunctionInfoGeneric> moduleFunctions =
        module.functionItems().stream()
            .map(BuiltinFunctions.FnDefinition::typeInfo)
            .filter(def -> isImportedElement(useDecl, def.uniqueVariableId().fullNamePath()))
            .toList();

    final List<TypeAliasInfo> moduleTypes =
        module.typeItems().stream()
            .filter(def -> isImportedElement(useDecl, def.typeId().fullNamePath()))
            .toList();

    // Check that all elements were found
    assertEverythingImported(
        useDecl,
        importedItemNames(module.modulePath(), moduleFunctions, moduleTypes),
        errorsAccumUseDecl);

    // Add to contexts
    fileContext.addFunctionsRelative(
        TypedAst.EnvKind.FUNCTION_BUILTIN,
        module.modulePath(),
        moduleFunctions,
        errorsAccumUseDecl,
        SourceScope.IMPORT);
    fileContext.extendTypeEnvironment(
        TypedAst.EnvKind.TYPE_BUILTIN,
        moduleTypes,
        module.modulePath(),
        errorsAccumUseDecl,
        SourceScope.IMPORT);
    fileContext.extendStructDefinitions(module.structItems());
    final List<TypeAliasInfo> structTypeItems =
        module.structItems().stream()
            .map(def -> typeInfoForTypedStruct(def, useDecl.position()))
            .toList();
    fileContext.extendTypeEnvironment(
        TypedAst.EnvKind.TYPE_BUILTIN,
        structTypeItems,
        module.modulePath(),
        errorsAccumUseDecl,
        SourceScope.IMPORT);
  }

  private static void importUserModule(
      final ZkAst.UseDeclaration useDecl,
      final NamePath.Absolute modulePath,
      final TypedAst.Program module,
      final FileContext fileContext,
      final ErrorAccum errorsAccumUseDecl) {

    // Extend context with imported data
    final List<FunctionInfoGeneric> moduleFunctions =
        module.functionItems().stream()
            .map(def -> functionInfoForTypedFunction(def, useDecl.position()))
            .filter(def -> isImportedElement(useDecl, def.uniqueVariableId().fullNamePath()))
            .toList();

    final List<TypeAliasInfo> structTypeItems =
        module.structItems().stream()
            .map(def -> typeInfoForTypedStruct(def, useDecl.position()))
            .filter(def -> isImportedElement(useDecl, def.typeId().fullNamePath()))
            .toList();

    // Check that all elements were found
    assertEverythingImported(
        useDecl,
        importedItemNames(modulePath, moduleFunctions, structTypeItems),
        errorsAccumUseDecl);

    // Add to contexts
    fileContext.addFunctionsAbsolute(moduleFunctions);
    fileContext.addFunctionsRelative(
        TypedAst.EnvKind.FUNCTION,
        modulePath,
        moduleFunctions,
        errorsAccumUseDecl,
        SourceScope.IMPORT);
    fileContext.extendTypeEnvironment(
        TypedAst.EnvKind.TYPE_USER_DEFINED,
        structTypeItems,
        modulePath,
        errorsAccumUseDecl,
        SourceScope.IMPORT);
    fileContext.extendStructDefinitions(module.structItems());
  }

  /**
   * Determines which item names have been imported.
   *
   * @param modulePath Path of the module the items where imported from.
   * @param moduleFunctions Imported functions.
   * @param moduleTypes Imported types.
   * @return Unmodifiable collection of used names.
   */
  private static Collection<String> importedItemNames(
      final NamePath.Absolute modulePath,
      final List<FunctionInfoGeneric> moduleFunctions,
      final List<TypeAliasInfo> moduleTypes) {
    return Stream.of(
            moduleFunctions.stream()
                .map(FunctionInfoGeneric::uniqueVariableId)
                .map(TypedAst.VariableId::fullNamePath),
            moduleTypes.stream().map(TypeAliasInfo::typeId).map(TypedAst.TypeId::fullNamePath))
        .flatMap(Function.identity())
        .map(modulePath::relativize)
        .map(x -> x.path().get(0))
        .toList();
  }

  /**
   * Checks that all requested items are imported.
   *
   * @param useDecl Use decl to check for.
   * @param importedItemNames String names of all found items.
   * @param errorsAccum Error accumulator.
   */
  private static void assertEverythingImported(
      final ZkAst.UseDeclaration useDecl,
      final Collection<String> importedItemNames,
      final ErrorAccum errorsAccum) {
    final List<String> importedModuleNames = useDecl.moduleElements();
    if (importedModuleNames != null) {
      final var unusedItems = new ArrayList<String>(importedModuleNames);
      unusedItems.removeAll(importedItemNames);

      if (!unusedItems.isEmpty()) {
        errorsAccum.add(
            "Could not find items '%s' in %s",
            String.join("', '", unusedItems), String.join("::", useDecl.modulePath()));
      }
    }
  }

  /**
   * Creates a new {@link FunctionInfoGeneric} for a typed {@link TypedAst.Function} item.
   *
   * @param def Function item to create type signature for.
   * @param originalPosition Position for signature.
   * @return Function signature.
   */
  private static FunctionInfoGeneric functionInfoForTypedFunction(
      final TypedAst.Function def, final ZkAst.PosInterval originalPosition) {
    return new FunctionInfoGeneric(
        def.identifier(),
        List.of(), // Used-defined generics not supported
        def.parameters().stream().map(TypedAst.Parameter::type).toList(),
        def.returnType(),
        originalPosition);
  }

  /**
   * Creates a new type alias for a typed struct.
   *
   * @param structDef Struct to create alias for
   * @param originalPosition Position to use.
   * @return Struct type alias.
   */
  private static TypeAliasInfo typeInfoForTypedStruct(
      TypedAst.StructDefinition structDef, ZkAst.PosInterval originalPosition) {
    final List<TypedAst.TypeId> typeParameterIds =
        structDef.typeParameterNames().stream().map(TypedAst.TypeId::newFor).toList();
    final List<TypedAst.Type> typeArguments =
        typeParameterIds.stream().map(TypedAst.Type.NamedType::simple).toList();

    return new TypeAliasInfo(
        structDef.typeId(),
        typeParameterIds,
        new TypedAst.Type.NamedType(structDef.typeId(), typeArguments),
        originalPosition);
  }

  /**
   * Creates a new type alias for an untyped struct.
   *
   * @param structDef Struct to create alias for
   * @param modulePath Module path for struct.
   * @return Struct type alias.
   */
  private static TypeAliasInfo typeInfoForUntypedStruct(
      final ZkAst.StructDefinition structDef, final NamePath.Absolute modulePath) {
    final TypedAst.TypeId typeId =
        new TypedAst.TypeId(modulePath.resolve(structDef.structIdentifier()));
    final List<TypedAst.TypeId> typeParameterIds =
        structDef.genericTypeParams().stream()
            .map(ZkAst.GenericTypeParam::boundName)
            .map(TypedAst.TypeId::newFor)
            .toList();
    final List<TypedAst.Type> typeArguments =
        typeParameterIds.stream().map(TypedAst.Type.NamedType::simple).toList();
    return new TypeAliasInfo(
        typeId,
        typeParameterIds,
        new TypedAst.Type.NamedType(typeId, typeArguments),
        structDef.position());
  }

  /**
   * Type checks type alias.
   *
   * @param itemDef Struct to create alias for
   * @param errorsAccum Appendable list of errors.
   * @param fileContext Context to type check function against.
   * @return Type alias information.
   */
  private static TypeAliasInfo typeCheckTypeAlias(
      final ZkAst.TypeAlias itemDef, final ErrorAccum errorsAccum, final FileContext fileContext) {
    final TypedAst.TypeId typeId =
        new TypedAst.TypeId(fileContext.fileModulePath().resolve(itemDef.aliasIdentifier()));
    final TypedAst.Type typeWithTypeParameters =
        typeCheckType(
            itemDef.type(),
            errorsAccum.withPos(itemDef.position()),
            fileContext.childContext(null));
    return new TypeAliasInfo(typeId, List.of(), typeWithTypeParameters, itemDef.position());
  }

  /**
   * Type checks entire function.
   *
   * @param functionItem Function to type check.
   * @param functionInfo Typed information about the function.
   * @param errorsAccum Appendable list of errors.
   * @param fileContext Context to type check function against.
   * @return Typed function.
   */
  private static TypedAst.Function typeCheckFunction(
      final ZkAst.ProgramItem<ZkAst.Function> functionItem,
      final FunctionInfoGeneric functionInfo,
      final ErrorAccum errorsAccum,
      final FileContext fileContext) {

    final ZkAst.Function function = functionItem.item();

    // Create context with function parameters
    final List<VariableInfoSimple> paramVariableInfo = new ArrayList<>();
    for (int paramIdx = 0; paramIdx < function.parameters().size(); paramIdx++) {
      final ZkAst.Parameter param = function.parameters().get(paramIdx);
      paramVariableInfo.add(
          new VariableInfoSimple(
              TypedAst.VariableId.newFor(TypedAst.EnvKind.FUNCTION_PARAMETER, param.identifier()),
              functionInfo.valueParameterTypesWithTypeParameters().get(paramIdx),
              param.isMutable(),
              param.position()));
    }

    // Check for duplicated parameter names
    final List<String> duplicateParameterNames =
        paramVariableInfo.stream()
            .collect(
                Collectors.groupingBy(
                    x -> x.uniqueVariableId().last(), Collectors.summingInt(x -> 1)))
            .entrySet()
            .stream()
            .filter(x -> x.getValue() > 1)
            .map(Map.Entry::getKey)
            .toList();

    if (duplicateParameterNames.size() > 0) {
      errorsAccum.add(
          function,
          "Function '%s' has duplicated parameters '%s'",
          function.identifier(),
          String.join("', '", duplicateParameterNames));
    }

    // Update body context with newly created variables
    final TypedAst.Type functionReturnType = functionInfo.returnTypeWithTypeParameters();
    final Context bodyContext = fileContext.childContext(functionReturnType);
    for (final VariableInfo paramDefinition : paramVariableInfo) {
      bodyContext.setVariable(
          paramDefinition.uniqueVariableId().last(), paramDefinition, errorsAccum);
    }

    // Type check body
    final TypedAst.TypedExpression typedExprBody =
        typeCheckExpression(function.body(), errorsAccum, bodyContext, functionReturnType);
    assertIsSubtype(
        typedExprBody.resultType(), functionReturnType, errorsAccum.withPos(function.body()));

    // Analyse Attributes
    final Map<ZkAst.MetaItem.Name, Map<ZkAst.MetaItem.Name, ZkAst.Expression>> attributeMapping =
        typeCheckOuterAttributes(
            errorsAccum.withPos(function),
            functionItem.attributes(),
            KnownAttribute::isAllowedOnFunction);
    final Integer explicitExportId =
        determineExplicitExportId(errorsAccum.withPos(function), attributeMapping);

    // Construct typed AST
    final List<TypedAst.Parameter> typedParameters =
        paramVariableInfo.stream()
            .map(
                paramDefinition ->
                    new TypedAst.Parameter(
                        paramDefinition.uniqueVariableId(), paramDefinition.type()))
            .toList();

    return new TypedAst.Function(
        new TypedAst.VariableId(
            TypedAst.EnvKind.FUNCTION, fileContext.fileModulePath().resolve(function.identifier())),
        typedParameters,
        functionReturnType,
        typedExprBody,
        functionItem.visibility(),
        explicitExportId,
        false);
  }

  /**
   * Basic information about an attribute.
   *
   * @param name Name of the attribute.
   * @param isAllowedOnFunction Whether the attribute is usable on {@link ZkAst.Function}.
   * @param isAllowedOnStruct Whether the attribute is usable on {@link ZkAst.StructDefinition}.
   * @param isAllowed Whether the attribute is even allowed in the ZK compiler.
   */
  record KnownAttribute(
      ZkAst.MetaItem.Name name,
      boolean isAllowedOnFunction,
      boolean isAllowedOnStruct,
      boolean isAllowed) {

    /**
     * Creates a new {@link KnownAttribute} which is entirely disallowed.
     *
     * @param name Name of disallowed attribute.
     * @return Newly created {@link KnownAttribute}.
     */
    public static KnownAttribute disallowed(String name) {
      return new KnownAttribute(new ZkAst.MetaItem.Name(List.of(name)), false, false, false);
    }
  }

  private static final ZkAst.MetaItem.Name ATTR_NAME_ZK_COMPUTE =
      new ZkAst.MetaItem.Name(List.of("zk_compute"));
  private static final ZkAst.MetaItem.Name ATTR_NAME_ZK_COMPUTE_SHORTNAME =
      new ZkAst.MetaItem.Name(List.of("shortname"));

  private static final Map<ZkAst.MetaItem.Name, KnownAttribute> KNOWN_ATTRIBUTES =
      List.of(
              // Custom PBC
              new KnownAttribute(ATTR_NAME_ZK_COMPUTE, true, false, true),

              // Known but ignored
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("allow")), true, true, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("warn")), true, true, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("deny")), true, true, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("forbid")), true, true, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("deprecated")), true, true, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("must_use")), true, true, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("repr")), false, true, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("doc")), true, true, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("inline")), true, false, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("cold")), true, false, true),
              new KnownAttribute(new ZkAst.MetaItem.Name(List.of("derive")), false, true, true),

              // disallowed: Testing
              KnownAttribute.disallowed("test"),
              KnownAttribute.disallowed("ignore"),
              KnownAttribute.disallowed("should_panic"),

              // disallowed: Macro use
              KnownAttribute.disallowed("macro_export"),
              KnownAttribute.disallowed("macro_use"),
              KnownAttribute.disallowed("proc_macro"),
              KnownAttribute.disallowed("proc_macro_derive"),
              KnownAttribute.disallowed("proc_macro_attribute"),

              // disallowed: Linking
              KnownAttribute.disallowed("link"),
              KnownAttribute.disallowed("link_name"),
              KnownAttribute.disallowed("link_ordinal"),
              KnownAttribute.disallowed("no_link"),
              KnownAttribute.disallowed("crate_type"),
              KnownAttribute.disallowed("no_main"),
              KnownAttribute.disallowed("export_name"),
              KnownAttribute.disallowed("link_section"),
              KnownAttribute.disallowed("no_mangle"),
              KnownAttribute.disallowed("used"),
              KnownAttribute.disallowed("crate_name"),

              // disallowed: Preludes
              KnownAttribute.disallowed("no_std"),
              KnownAttribute.disallowed("no_implicit_prelude"),

              // disallowed: Modules
              KnownAttribute.disallowed("path"),

              // disallowed: Limits
              KnownAttribute.disallowed("recursion_limit"),
              KnownAttribute.disallowed("type_length_limit"),

              // disallowed: Runtime
              KnownAttribute.disallowed("panic_handler"),
              KnownAttribute.disallowed("global_allocator"),
              KnownAttribute.disallowed("windows_subsystem"),

              // disallowed: Features
              KnownAttribute.disallowed("feature"))
          .stream()
          .collect(Collectors.toUnmodifiableMap(KnownAttribute::name, Function.identity()));

  /**
   * Determines name of the given {@link ZkAst.MetaItem}.
   *
   * @param uncheckedItem Meta item to determine name for.
   * @return Determined name.
   */
  private static ZkAst.MetaItem.Name nameOf(final ZkAst.MetaItem uncheckedItem) {
    if (uncheckedItem instanceof ZkAst.MetaItem.Assign item) {
      return item.name();
    } else if (uncheckedItem instanceof ZkAst.MetaItem.Call item) {
      return item.name();
    } else {
      return (ZkAst.MetaItem.Name) uncheckedItem;
    }
  }

  /**
   * Type checks the given attribute list in the given context.
   *
   * @param errorsAccum Error accumulator.
   * @param attributes Attributes to type check.
   * @param isAllowedOnThisItem Predicate for whether the given attribute is allowed on the item.
   * @return Simple attribute assignments.
   */
  private static Map<ZkAst.MetaItem.Name, Map<ZkAst.MetaItem.Name, ZkAst.Expression>>
      typeCheckOuterAttributes(
          final ErrorAccum errorsAccum,
          final List<ZkAst.OuterAttribute> attributes,
          final Predicate<KnownAttribute> isAllowedOnThisItem) {

    final Map<ZkAst.MetaItem.Name, Map<ZkAst.MetaItem.Name, ZkAst.Expression>> attributeMapping =
        new HashMap<>();

    for (final ZkAst.OuterAttribute attribute : attributes) {
      final ZkAst.MetaItem.Name name = nameOf(attribute.metaItem());
      final KnownAttribute known = KNOWN_ATTRIBUTES.get(name);
      if (known == null) {
        continue;
      }

      if (!known.isAllowed()) {
        errorsAccum.add("Attribute %s is not allowed", String.join("::", name.path()));
      } else if (!isAllowedOnThisItem.test(known)) {
        errorsAccum.add("Attribute %s cannot be used here", String.join("::", name.path()));
      }

      if (attribute.metaItem() instanceof ZkAst.MetaItem.Call item) {
        final Map<ZkAst.MetaItem.Name, ZkAst.Expression> arguments = new HashMap<>();
        for (final ZkAst.MetaItemInner uncheckedArg : item.arguments()) {
          if (uncheckedArg instanceof ZkAst.MetaItemInner.MetaItem arg
              && arg.metaItem() instanceof ZkAst.MetaItem.Assign assign) {
            arguments.put(assign.name(), assign.value());
          }
        }
        attributeMapping.put(name, Map.copyOf(arguments));
      }
    }

    return Map.copyOf(attributeMapping);
  }

  /**
   * Determines the {@link TypedAst.Function#explicitExportId} of a {@link ZkAst.Function}, by the
   * attribute mapping created by {@link #typeCheckOuterAttributes}.
   */
  private static Integer determineExplicitExportId(
      final ErrorAccum errorsAccum,
      final Map<ZkAst.MetaItem.Name, Map<ZkAst.MetaItem.Name, ZkAst.Expression>> attributeMapping) {
    final ZkAst.Expression uncheckedAttrDef =
        attributeMapping
            .getOrDefault(ATTR_NAME_ZK_COMPUTE, Map.of())
            .get(ATTR_NAME_ZK_COMPUTE_SHORTNAME);
    if (uncheckedAttrDef != null) {
      if (uncheckedAttrDef instanceof ZkAst.Expression.IntLiteral attrDef) {
        BigInteger exportId = attrDef.value();
        assertIntegerFits(exportId, new TypedAst.Type.Int(32, false), errorsAccum);
        return exportId.intValue();
      } else {
        errorsAccum.add("Attribute zk_compute shortname must be an integer");
      }
    }
    return null;
  }

  /**
   * Determines the function and performs monomorphization based on the given functionName.
   *
   * @param errorsAccum Appendable list of errors.
   * @param context Context to type check function against.
   * @param functionName Path to function.
   * @return Possibly null, if function was unknown.
   */
  private static FunctionInfoMonomorphed getMonomorphedFunction(
      final ErrorAccum errorsAccum, final Context context, final ZkAst.Path functionName) {
    final VariableInfo variableInfo = context.getVariable(functionName);

    if (variableInfo instanceof FunctionInfoGeneric functionInfo) {
      return monomorphFunction(
          errorsAccum.withPos(functionName), context, functionInfo, functionName.genericTypes());
    } else if (variableInfo == null) {
      final String variablePretty = ZkAstPretty.prettyPath(functionName, true);
      errorsAccum.add(functionName, "Unknown function %s", variablePretty);
      return null;
    } else {
      final String variablePretty = ZkAstPretty.prettyPath(functionName, true);
      errorsAccum.add(functionName, "%s is not a function", variablePretty);
      return null;
    }
  }

  /**
   * Performs monomorphization transformation for the given function.
   *
   * @param errorsAccum Appendable list of errors.
   * @param context Context to type check in.
   * @param functionInfo Generic function information.
   * @param typeArguments Type arguments for the generic function.
   * @return Never null.
   */
  private static FunctionInfoMonomorphed monomorphFunction(
      final ErrorAccum errorsAccum,
      final Context context,
      final FunctionInfoGeneric functionInfo,
      final List<ZkAst.Type> typeArguments) {
    return monomorphFunctionTypedArguments(
        errorsAccum,
        functionInfo,
        typeArguments.stream().map(t -> typeCheckType(t, errorsAccum, context)).toList());
  }

  /**
   * Performs monomorphization transformation for the given function.
   *
   * @param errorsAccum Appendable list of errors.
   * @param functionInfo Generic function information.
   * @param typeArguments Type arguments for the generic function.
   * @return Never null.
   */
  private static FunctionInfoMonomorphed monomorphFunctionTypedArguments(
      final ErrorAccum errorsAccum,
      final FunctionInfoGeneric functionInfo,
      final List<TypedAst.Type> typeArguments) {

    final TypeReplacementMap typeReplacementMap =
        typeReplacementMap(errorsAccum, functionInfo.typeParameterNames(), typeArguments);

    return new FunctionInfoMonomorphed(
        functionInfo.uniqueVariableId(),
        functionInfo.valueParameterTypesWithTypeParameters().stream()
            .map(typeReplacementMap::replaceParametersInType)
            .toList(),
        typeReplacementMap.replaceParametersInType(functionInfo.returnTypeWithTypeParameters()));
  }

  private static StructInfoMonomorphed getMonomorphedStruct(
      final ErrorAccum errorsAccum, final Context context, final TypedAst.Type uncheckedType) {

    final TypedAst.Type.NamedType structName;
    if (uncheckedType instanceof TypedAst.Type.NamedType type) {
      structName = type;
    } else {
      if (!uncheckedType.isError()) {
        errorsAccum.add("Expected struct type, but was %s", uncheckedType);
      }
      return null;
    }

    final TypedAst.StructDefinition structDefinition =
        context.getStructDefinition(structName.typeId());

    final TypeReplacementMap typeReplacementMap =
        typeReplacementMap(
            errorsAccum, structDefinition.typeParameterNames(), structName.typeArguments());

    // Replace types in fields
    final Map<String, TypedAst.Type> fieldMap =
        structDefinition.fields().stream()
            .collect(
                Collectors.toUnmodifiableMap(
                    field -> field.fieldName(),
                    field ->
                        typeReplacementMap.replaceParametersInType(
                            field.fieldTypeWithTypeParameters())));

    return new StructInfoMonomorphed(structName, fieldMap);
  }

  /**
   * Type checks expression.
   *
   * @param uncheckedExpression Expression to type check.
   * @param errorsAccum Appendable list of errors.
   * @param context Context to type check expression against.
   * @param inferredType Possible type of the given expression, inferred from outside context.
   *     Should be null if no type could be inferred.
   * @return Typed expression for the given expression and context.
   */
  static TypedAst.TypedExpression typeCheckExpression(
      final ZkAst.Expression uncheckedExpression,
      final ErrorAccum errorsAccum,
      final Context context,
      final TypedAst.Type inferredType) {

    // Block expression
    if (uncheckedExpression instanceof ZkAst.Expression.Block expr) {
      return typeCheckBlock(expr, errorsAccum, context, inferredType);

      // Unary expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.Unary expr) {
      if (expr.expr1() instanceof ZkAst.Expression.IntLiteral intLit
          && expr.operation() == ZkAst.Unop.NEGATE) {
        ZkAst.Expression coerced =
            new ZkAst.Expression.IntLiteral(
                intLit.value().negate(), intLit.explicitType(), intLit.position());
        return typeCheckExpression(coerced, errorsAccum, context, inferredType);
      }

      final TypedAst.TypedExpression typedExpr1 =
          typeCheckExpression(expr.expr1(), errorsAccum, context, inferredType);
      assertIsIntegerType(typedExpr1.resultType(), errorsAccum.withPos(expr.expr1()));
      final boolean isConstantExpression = false; // Pessimistic for now.

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.Unary(expr.operation(), typedExpr1),
          typedExpr1.resultType(),
          isConstantExpression,
          typedExpr1.containsGuessedType(),
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Binary expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.Binary expr) {
      return typeCheckBinaryExpression(expr, errorsAccum, context, inferredType);

      // Integer literal
    } else if (uncheckedExpression instanceof ZkAst.Expression.IntLiteral expr) {
      boolean containsGuessedType = false;

      // Determine type for literal.
      final TypedAst.Type resultType;
      if (expr.explicitType() != null) {
        resultType = typeCheckType(expr.explicitType(), errorsAccum, context);
      } else if (inferredType instanceof TypedAst.Type.Int) {
        resultType = inferredType;
      } else {
        resultType = new TypedAst.Type.Int(32, true);
        containsGuessedType = true;
      }

      assertIntegerFits(expr.value(), resultType, errorsAccum.withPos(expr.position()));

      final boolean isConstantExpression = true;

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.IntLiteral(expr.value()),
          resultType,
          isConstantExpression,
          containsGuessedType,
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Boolean literal
    } else if (uncheckedExpression instanceof ZkAst.Expression.BoolLiteral expr) {
      final TypedAst.Type resultType = TypedAst.Type.Int.BOOL;
      final boolean isConstantExpression = true;
      final boolean containsGuessedType = false;
      return new TypedAst.TypedExpression(
          new TypedAst.Expression.IntLiteral(expr.value() ? 1 : 0),
          resultType,
          isConstantExpression,
          containsGuessedType,
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Tuple constructor expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.TupleConstructor expr) {

      final List<TypedAst.TypedExpression> typedExpressions;
      if (inferredType instanceof TypedAst.Type.Tuple inferredTuple
          && inferredTuple.subtypes().size() == expr.exprs().size()) {
        typedExpressions =
            IntStream.range(0, expr.exprs().size())
                .mapToObj(
                    i ->
                        typeCheckExpression(
                            expr.exprs().get(i),
                            errorsAccum,
                            context,
                            inferredTuple.subtypes().get(i)))
                .toList();
      } else {
        typedExpressions =
            expr.exprs().stream()
                .map(subExpr -> typeCheckExpression(subExpr, errorsAccum, context, null))
                .toList();
      }

      final boolean isConstantExpression = false; // Pessimistic for now.
      final boolean containsGuessedType =
          typedExpressions.stream().anyMatch(TypedAst.TypedExpression::containsGuessedType);

      final TypedAst.Type tupleType =
          new TypedAst.Type.Tuple(
              typedExpressions.stream().map(TypedAst.TypedExpression::resultType).toList());

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.TupleConstructor(typedExpressions),
          tupleType,
          isConstantExpression,
          containsGuessedType,
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Tuple dot expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.TupleAccess expr) {
      final TypedAst.TypedExpression typedExprTuple =
          typeCheckExpression(expr.tupleExpr(), errorsAccum, context, inferredType);

      final int index = expr.index().intValue();

      if (typedExprTuple.resultType() instanceof TypedAst.Type.Tuple tupleType
          && index < tupleType.subtypes().size()) {
        final TypedAst.Type typeOfElement = tupleType.subtypes().get(index);
        final boolean isConstantExpression = false; // Pessimistic for now.
        return new TypedAst.TypedExpression(
            new TypedAst.Expression.TupleAccess(typedExprTuple, index),
            typeOfElement,
            isConstantExpression,
            typedExprTuple.containsGuessedType(),
            new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));
      }

      // Unsuccessful field
      errorsAccum.add(expr, "Unknown field %d in type %s", index, typedExprTuple.resultType());

      // If expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.If expr) {
      final TypedAst.TypedExpression typedExprCond =
          typeCheckExpression(expr.condExpr(), errorsAccum, context, TypedAst.Type.Int.BOOL);
      final var typedExpressions =
          typeCheckToSameExpectedType(
              List.of(expr.thenExpr(), expr.elseExpr()), inferredType, errorsAccum, context);
      final TypedAst.TypedExpression typedExprThen = typedExpressions.get(0);
      final TypedAst.TypedExpression typedExprElse = typedExpressions.get(1);
      assertIsBooleanType(typedExprCond.resultType(), errorsAccum.withPos(expr.condExpr()));
      assertTypesEquivalent(
          typedExprThen.resultType(), typedExprElse.resultType(), errorsAccum.withPos(expr));

      final boolean isConstantExpression = false; // Pessimistic for now.
      final boolean containsGuessedType =
          typedExpressions.stream().allMatch(TypedAst.TypedExpression::containsGuessedType);

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.If(typedExprCond, typedExprThen, typedExprElse),
          typedExprThen.resultType(),
          isConstantExpression,
          containsGuessedType,
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // For expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.For expr) {
      return typeCheckForExpression(errorsAccum, context, expr);

      // Variable Expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.Variable expr) {
      final VariableInfo variableInfoUnchecked = context.getVariable(expr.variablePath());

      // Found variable
      if (variableInfoUnchecked instanceof VariableInfoSimple variableInfo) {
        final boolean isConstantExpression = false; // Pessimistic for now.
        final boolean containsGuessedType = false;

        final var astElem =
            variableInfo.uniqueVariableId().variableKind() == TypedAst.EnvKind.CONSTANT
                ? new TypedAst.Expression.GlobalVariable(variableInfo.uniqueVariableId())
                : new TypedAst.Expression.LocalVariable(variableInfo.uniqueVariableId());

        return new TypedAst.TypedExpression(
            astElem,
            variableInfo.type,
            isConstantExpression,
            containsGuessedType,
            new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

        // Found function, not a variable
      } else if (variableInfoUnchecked instanceof FunctionInfoGeneric variableInfo) {
        final String variablePretty = ZkAstPretty.prettyPath(expr.variablePath(), true);
        errorsAccum.add(expr, "%s is a function", variablePretty);

        // Could not find variable
      } else {
        final String variablePretty = ZkAstPretty.prettyPath(expr.variablePath(), true);
        errorsAccum.add(expr, "Variable %s not in scope", variablePretty);
      }

      // Explicit type cast expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.TypeCast expr) {
      final TypedAst.TypedExpression typedValueExpr =
          typeCheckExpression(expr.valueExpr(), errorsAccum, context, null);
      final TypedAst.Type resultType = typeCheckType(expr.resultType(), errorsAccum, context);

      // Check that value can be casted to type.
      assertIsExplicitlyCastable(
          typedValueExpr.resultType(), resultType, errorsAccum.withPos(expr));

      final boolean isConstantExpression = false; // Pessimistic for now.

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.TypeCast(typedValueExpr, resultType),
          resultType,
          isConstantExpression,
          false,
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Variable-specific Assignments
    } else if (uncheckedExpression instanceof ZkAst.Expression.Assign expr) {
      return typeCheckExpressionAssign(expr, errorsAccum, context);

      // Call expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.Call expr) {
      return typeCheckExpressionFunctionCall(
          expr, errorsAccum.withPos(expr), context, inferredType);

      // Method call expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.MethodCall expr) {
      return typeCheckExpressionMethodCall(expr, errorsAccum.withPos(expr), context, inferredType);

      // Continue expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.Continue expr) {
      if (!context.inLoop()) {
        errorsAccum.add(expr, "Continue expressions cannot exist outside of loops");
      }

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.Continue(),
          TypedAst.Type.Tuple.UNIT,
          false,
          false,
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Break expression
    } else if (uncheckedExpression instanceof ZkAst.Expression.Break expr) {
      if (!context.inLoop()) {
        errorsAccum.add(expr, "Break expressions cannot exist outside of loops");
      }

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.Break(),
          TypedAst.Type.Tuple.UNIT,
          false,
          false,
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Struct constructor
    } else if (uncheckedExpression instanceof ZkAst.Expression.StructConstructor expr) {
      return typeCheckExpressionStructConstructor(expr, errorsAccum, context);

      // Array constructor repeated
    } else if (uncheckedExpression instanceof ZkAst.Expression.ArrayConstructorRepeat expr) {

      // Type check initial element
      final TypedAst.Type expectedElementType = extractElementTypeFromArrayOrNull(inferredType);
      final TypedAst.TypedExpression typedRepeatExpression =
          typeCheckExpression(expr.repeatExpression(), errorsAccum, context, expectedElementType);

      // Type check size expression
      final TypedAst.TypedExpression typedSizeExpression =
          typeCheckExpression(expr.sizeExpression(), errorsAccum, context, ARRAY_SIZE_TYPE);
      assertIsSubtype(
          typedSizeExpression.resultType(),
          ARRAY_SIZE_TYPE,
          errorsAccum.withPos(expr.sizeExpression()));

      // Extract to integer size
      final int arraySize =
          sizeOfArrayType(errorsAccum.withPos(expr.sizeExpression()), typedSizeExpression);
      final var arrayType = new TypedAst.Type.Array(typedRepeatExpression.resultType(), arraySize);

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.ArrayConstructorRepeat(
              typedRepeatExpression, typedSizeExpression),
          arrayType,
          false,
          typedRepeatExpression.containsGuessedType(),
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Array constructor explicit
    } else if (uncheckedExpression instanceof ZkAst.Expression.ArrayConstructorExplicit expr) {

      TypedAst.Type elementType = extractElementTypeFromArrayOrNull(inferredType);

      List<TypedAst.TypedExpression> typedExpressions =
          typeCheckToSameExpectedType(expr.elementExpressions(), elementType, errorsAccum, context);

      if (!typedExpressions.isEmpty()) {
        elementType = typedExpressions.get(0).resultType();
      }

      if (elementType == null) {
        elementType = INVALID_EXPRESSION_TYPE;
        errorsAccum.add(expr, "Could not determine type of array elements");
      }

      final TypedAst.Type arrayType = new TypedAst.Type.Array(elementType, typedExpressions.size());

      final boolean containsGuessedType =
          typedExpressions.stream().anyMatch(TypedAst.TypedExpression::containsGuessedType);
      return new TypedAst.TypedExpression(
          new TypedAst.Expression.ArrayConstructorExplicit(List.copyOf(typedExpressions)),
          arrayType,
          false,
          containsGuessedType,
          new TypedAst.PosInterval(errorsAccum.filepath(), uncheckedExpression.position()));

      // Field access
    } else if (uncheckedExpression instanceof ZkAst.Expression.FieldAccess expr) {
      final TypedAst.TypedExpression typedExprStruct =
          typeCheckExpression(expr.objectExpr(), errorsAccum, context, null);
      final String fieldName = expr.fieldIdentifier();
      return typeCheckFieldAccess(errorsAccum.withPos(expr), context, typedExprStruct, fieldName);

      // Unknown expression
    } else {
      errorsAccum.add(
          uncheckedExpression,
          "Unsupported expression: %s",
          uncheckedExpression.getClass().getSimpleName());
    }

    // For type error cases.
    return INVALID_EXPRESSION_EXPR;
  }

  /**
   * Type checks {@link ZkAst.Expression.Binary} expression.
   *
   * @param expr Expression to type check.
   * @param errorsAccum Appendable list of errors.
   * @param context Context to type check expression against.
   * @param inferredType Possible type of the given expression, inferred from outside context.
   *     Should be null if no type could be inferred.
   * @return Typed expression for the given expression and context.
   */
  private static TypedAst.TypedExpression typeCheckBinaryExpression(
      final ZkAst.Expression.Binary expr,
      final ErrorAccum errorsAccum,
      final Context context,
      final TypedAst.Type inferredType) {

    // Determine inferred types to be used for arguments.
    final TypedAst.Type argumentExpectedType1;
    final TypedAst.Type argumentExpectedType2;
    if (BINOPS_ARITH.contains(expr.operation()) || BINOPS_LOGICAL.contains(expr.operation())) {

      // Arithmetic binops and logical binops preserve types
      argumentExpectedType1 = inferredType;
      argumentExpectedType2 = inferredType;

    } else if (BINOPS_BITSHIFTS.contains(expr.operation())) {
      // Bitshifts preserve the left type
      argumentExpectedType1 = inferredType;
      argumentExpectedType2 = null;

    } else if (ZkAst.Binop.INDEX.equals(expr.operation())) {

      argumentExpectedType1 =
          inferredType != null ? new TypedAst.Type.Array(inferredType, 0) : null;
      argumentExpectedType2 = TYPE_OF_INDEX_RHS;

    } else {

      // Pessimistic: Inferred type could be determined from input inferredType.
      argumentExpectedType1 = null;
      argumentExpectedType2 = null;
    }

    final TypedAst.TypedExpression typedExpr1;
    final TypedAst.TypedExpression typedExpr2;
    if (binopRequiresSameType(expr.operation())) {
      final List<TypedAst.TypedExpression> typedExpressions =
          typeCheckToSameExpectedType(
              List.of(expr.expr1(), expr.expr2()), argumentExpectedType1, errorsAccum, context);
      typedExpr1 = typedExpressions.get(0);
      typedExpr2 = typedExpressions.get(1);
    } else {
      typedExpr1 = typeCheckExpression(expr.expr1(), errorsAccum, context, argumentExpectedType1);
      typedExpr2 = typeCheckExpression(expr.expr2(), errorsAccum, context, argumentExpectedType2);
    }

    final boolean containsGuessedType =
        typedExpr1.containsGuessedType() || typedExpr2.containsGuessedType();

    final TypedAst.Type typeLeft = typedExpr1.resultType();
    final TypedAst.Type typeRight = typedExpr2.resultType();

    final boolean unknownInputs = typeLeft.isError() || typeRight.isError();

    final BinopPosData posData =
        new BinopPosData(expr.position(), expr.expr1().position(), expr.expr2().position());

    final boolean isConstantExpression = false; // Pessimistic for now.

    // Range expresssion
    if (ZkAst.Binop.RANGE.equals(expr.operation())) {
      assertIsIntegerType(typeLeft, errorsAccum.withPos(expr.expr1()));

      final var fieldInits =
          List.of(
              new TypedAst.Expression.StructConstructor.FieldInit("start", typedExpr1),
              new TypedAst.Expression.StructConstructor.FieldInit("end", typedExpr2));

      final TypedAst.Type.NamedType structName =
          new TypedAst.Type.NamedType(
              BuiltinFunctionsRustStd.STRUCT_DEFINITION_RANGE_ID, List.of(typeLeft));
      final StructInfoMonomorphed structDefinition =
          getMonomorphedStruct(errorsAccum.withPos(expr), context, structName);

      return typeCheckExpressionStructConstructor(
          structDefinition, fieldInits, errorsAccum, expr.position());

      // Other binops
    } else {
      final TypedAst.Type resultType =
          typeCheckBinop(expr.operation(), errorsAccum, typeLeft, typeRight, posData);

      final TypedAst.Type resultType2 = !unknownInputs ? resultType : new TypedAst.Type.ErrorType();

      final TypedAst.Expression resultExpr =
          new TypedAst.Expression.Binary(expr.operation(), typedExpr1, typedExpr2);

      return new TypedAst.TypedExpression(
          resultExpr,
          resultType2,
          isConstantExpression,
          containsGuessedType,
          new TypedAst.PosInterval(errorsAccum.filepath(), expr.position()));
    }
  }

  /**
   * Returns whether {@code operation} requires its operands to be of the same type.
   *
   * @param operation The operation to check.
   * @return true if {@code operation} requires its operands to be of the same type, otherwise
   *     false.
   */
  private static boolean binopRequiresSameType(ZkAst.Binop operation) {
    return BINOPS_LOGICAL.contains(operation)
        || BINOPS_ARITH.contains(operation)
        || BINOPS_COMPARISON.contains(operation)
        || BINOPS_EQUALITY.contains(operation)
        || operation.equals(ZkAst.Binop.RANGE);
  }

  /**
   * Type check a list of expressions, with the expectation that they all produce the same type,
   * with {@code inferredType} being the initial guess for the result type.
   *
   * <p>If the resulting type of an expression was derived, the type check is re-done on all
   * expressions where the type was guessed and different from the derived type, now with the
   * derived type as the inferred type.
   *
   * <p>At the end, an error is reported for every type which doesn't match the derived type (if
   * present), or the type of the first expression if no derived type is present.
   *
   * @param expressions list of untyped expressions to type check.
   * @param inferredType expected type of the expressions.
   * @param errorAccum error accumulator used during type checking, and when validating final types.
   * @param context type checking context.
   * @return List of type checked expressions, in the same order as the untyped expressions.
   */
  private static List<TypedAst.TypedExpression> typeCheckToSameExpectedType(
      List<ZkAst.Expression> expressions,
      TypedAst.Type inferredType,
      ErrorAccum errorAccum,
      Context context) {

    // Initial type check pass.
    List<TypedAst.TypedExpression> typedExpressions =
        new ArrayList<>(
            expressions.stream()
                .map(expr -> typeCheckExpression(expr, errorAccum, context, inferredType))
                .toList());

    // Get any potential derived type.
    final TypedAst.Type derivedType =
        typedExpressions.stream()
            .filter(Predicate.not(TypedAst.TypedExpression::containsGuessedType))
            .map(TypedAst.TypedExpression::resultType)
            .findFirst()
            .orElse(null);

    // Re type check any expressions with guessed types.
    if (derivedType != null) {
      for (int i = 0; i < typedExpressions.size(); i++) {
        final TypedAst.TypedExpression typedCurr = typedExpressions.get(i);
        if (typedCurr.containsGuessedType()
            && !(typedCurr.resultType() != TypedAst.Type.Int.BOOL
                && derivedType == TypedAst.Type.Int.BOOL)) {
          final TypedAst.TypedExpression updatedTypedExpression =
              typeCheckExpression(expressions.get(i), errorAccum, context, derivedType);

          typedExpressions.set(i, updatedTypedExpression);
        }
      }
    }

    // Validate types.
    if (!typedExpressions.isEmpty()) {
      final TypedAst.Type requiredType =
          derivedType != null ? derivedType : typedExpressions.get(0).resultType();
      for (int i = 0; i < typedExpressions.size(); i++) {
        assertIsSubtype(
            typedExpressions.get(i).resultType(),
            requiredType,
            errorAccum.withPos(expressions.get(i)));
      }
    }

    return typedExpressions;
  }

  private static TypedAst.Type extractElementTypeFromArrayOrNull(
      final TypedAst.Type uncheckedType) {
    if (uncheckedType instanceof TypedAst.Type.Array type) {
      return type.elementType();
    }
    return null;
  }

  static TypedAst.TypedExpression typeCheckForExpression(
      final ErrorAccum errorsAccum, final Context context, final ZkAst.Expression.For expr) {

    final TypedAst.TypedExpression typedExprIterator =
        typeCheckExpression(expr.iteratorExpr(), errorsAccum, context, null);

    final TypedAst.Type typeOfPreIteratorUnchecked = typedExprIterator.resultType();

    final FunctionInfoGeneric methodIntoIteratorGeneric =
        (FunctionInfoGeneric)
            context.getStaticField(
                errorsAccum.withPos(expr.iteratorExpr()), typeOfPreIteratorUnchecked, "into_iter");
    if (methodIntoIteratorGeneric == null) {
      return INVALID_EXPRESSION_EXPR;
    }
    final TypedAst.Type.NamedType typeOfPreIterator =
        (TypedAst.Type.NamedType) typeOfPreIteratorUnchecked;

    // Type check method: into_iter
    final FunctionInfoMonomorphed methodIntoIterator =
        monomorphFunctionTypedArguments(
            errorsAccum, methodIntoIteratorGeneric, typeOfPreIterator.typeArguments());
    final TypedAst.TypedExpression checkedMethodIntoIterator =
        typeCheckFunctionCall(
            methodIntoIterator, List.of(typedExprIterator), errorsAccum.withPos(expr.position()));

    // Type check method: next
    final FunctionInfoGeneric methodNextGeneric =
        (FunctionInfoGeneric)
            context.getStaticField(
                errorsAccum.withPos(expr.iteratorExpr()),
                checkedMethodIntoIterator.resultType(),
                "next");
    final FunctionInfoMonomorphed methodNext =
        monomorphFunctionTypedArguments(
            errorsAccum, methodNextGeneric, typeOfPreIterator.typeArguments());
    final TypedAst.TypedExpression checkedMethodNext =
        typeCheckFunctionCall(
            methodNext, List.of(checkedMethodIntoIterator), errorsAccum.withPos(expr.position()));
    final TypedAst.Type typeOfIteratedValue =
        ((TypedAst.Type.Tuple) checkedMethodNext.resultType()).subtypes().get(2);

    // Position here is not very precise; needs position on variable
    // identifier itself, though that should be solved if we allow
    // pattern destruction in this position at some point.
    final ZkAst.PosInterval variablePosition = expr.position();
    final VariableInfo iteratorVariableInfo =
        new VariableInfoSimple(
            TypedAst.VariableId.newFor(
                TypedAst.EnvKind.VARIABLE_LOOP, expr.iteratorVariableIdentifier()),
            typeOfIteratedValue,
            false,
            variablePosition);

    final Context bodyContext = context.childContext(true);
    bodyContext.setVariable(expr.iteratorVariableIdentifier(), iteratorVariableInfo, errorsAccum);

    final TypedAst.TypedExpression typedExprBody =
        typeCheckExpression(expr.body(), errorsAccum, bodyContext, TypedAst.Type.Tuple.UNIT);

    final boolean isConstantExpression = false; // Pessimistic for now.

    return new TypedAst.TypedExpression(
        new TypedAst.Expression.ForInRange(
            iteratorVariableInfo.uniqueVariableId(),
            typedExprIterator,
            typedExprBody,
            methodIntoIterator.uniqueVariableId(),
            methodNext.uniqueVariableId()),
        TypedAst.Type.Tuple.UNIT,
        isConstantExpression,
        false,
        new TypedAst.PosInterval(errorsAccum.filepath(), expr.position()));
  }

  private static TypedAst.TypedExpression typeCheckExpressionAssign(
      final ZkAst.Expression.Assign expression,
      final ErrorAccum errorsAccum,
      final Context context) {

    final TypedAst.TypedExpression typedPlaceExpr =
        typeCheckExpression(expression.placeExpr(), errorsAccum, context, null);

    final VariableInfoSimple variableInfo;

    final TypedAst.TypedExpression baseVariableExpression = baseVariableExpression(typedPlaceExpr);
    if (baseVariableExpression.expr() instanceof TypedAst.Expression.LocalVariable baseVariable) {
      final var variablePath =
          new NamePath.Relative(baseVariable.variableIdentifier().fullNamePath().path());
      variableInfo = (VariableInfoSimple) context.getVariable(variablePath);

    } else if (baseVariableExpression.resultType() instanceof TypedAst.Type.ErrorType) {
      return INVALID_EXPRESSION_EXPR;

    } else {
      errorsAccum.add(expression.placeExpr(), "Not a place expression");
      return INVALID_EXPRESSION_EXPR;
    }

    // Immutable variable
    if (!variableInfo.isMutable()) {
      errorsAccum.add(expression.placeExpr(), "Cannot assign to immutable variable");
      return INVALID_EXPRESSION_EXPR;

      // Allowed
    } else {
      final TypedAst.TypedExpression typedValueExpr =
          typeCheckExpression(
              expression.valueExpr(), errorsAccum, context, typedPlaceExpr.resultType());
      assertIsSubtype(
          typedValueExpr.resultType(),
          typedPlaceExpr.resultType(),
          errorsAccum.withPos(expression));

      return new TypedAst.TypedExpression(
          new TypedAst.Expression.Assign(typedPlaceExpr, typedValueExpr),
          TypedAst.Type.Tuple.UNIT,
          false,
          false,
          new TypedAst.PosInterval(errorsAccum.filepath(), expression.position()));
    }
  }

  /**
   * Place expression base variable.
   *
   * <p>The base variable is an {@link TypedAst.Expression.LocalVariable} that can be read and
   * written to.
   *
   * <p>The base variable is often needed for assignments to specific subparts, such as during
   * assignments to fields or to arrays.
   */
  static TypedAst.TypedExpression baseVariableExpression(
      TypedAst.TypedExpression uncheckedExpression) {
    // Tuple access
    if (uncheckedExpression.expr() instanceof TypedAst.Expression.TupleAccess expr) {
      return baseVariableExpression(expr.tupleExpr());

      // Field access
    } else if (uncheckedExpression.expr() instanceof TypedAst.Expression.FieldAccess expr) {
      return baseVariableExpression(expr.structExpr());

      // Index
    } else if (uncheckedExpression.expr() instanceof TypedAst.Expression.Binary expr
        && expr.operation() == ZkAst.Binop.INDEX) {
      return baseVariableExpression(expr.expr1());

    } else {
      // Unknown, and LocalVariable
      return uncheckedExpression;
    }
  }

  static TypedAst.TypedExpression typeCheckFieldAccess(
      final ErrorAccum errorsAccum,
      final Context context,
      final TypedAst.TypedExpression typedExprStruct,
      final String fieldName) {

    // Find struct definition
    final StructInfoMonomorphed def =
        getMonomorphedStruct(errorsAccum, context, typedExprStruct.resultType());
    if (def == null) {
      return INVALID_EXPRESSION_EXPR;
    }

    // Find field
    final TypedAst.Type fieldType = def.typeOfField(fieldName);
    if (fieldType == null) {
      errorsAccum.add("Unknown field '%s' in type %s", fieldName, typedExprStruct.resultType());
      return INVALID_EXPRESSION_EXPR;
    }

    // Create typed ast
    final boolean isConstantExpression = false; // Pessimistic for now
    final boolean containsGuessedType = false;

    return new TypedAst.TypedExpression(
        new TypedAst.Expression.FieldAccess(typedExprStruct, fieldName),
        fieldType,
        isConstantExpression,
        containsGuessedType,
        typedExprStruct.position());
  }

  /**
   * Determines {@link TypeReplacementMap} from given {@code typeParameterNames} and {@code
   * typeArguments}.
   *
   * @param errorsAccum Error accumulator for potential errors.
   * @param typeParameterNames Names of type parameters to replace.
   * @param typeArguments Realized types to replace with.
   * @return Newly created {@link TypeReplacementMap}.
   */
  private static TypeReplacementMap typeReplacementMap(
      final ErrorAccum errorsAccum,
      final List<String> typeParameterNames,
      final List<TypedAst.Type> typeArguments) {
    final List<TypedAst.TypeId> typeParameterIds =
        typeParameterNames.stream().map(TypedAst.TypeId::newFor).toList();

    return typeReplacementMapFromIds(errorsAccum, typeParameterIds, typeArguments);
  }

  /**
   * Determines {@link TypeReplacementMap} from given {@code typeParameterIds } and {@code
   * typeArguments}.
   *
   * @param errorsAccum Error accumulator for potential errors.
   * @param typeParameterIds Names of type parameters to replace.
   * @param typeArguments Realized types to replace with.
   * @return Newly created {@link TypeReplacementMap}.
   */
  private static TypeReplacementMap typeReplacementMapFromIds(
      final ErrorAccum errorsAccum,
      final List<TypedAst.TypeId> typeParameterIds,
      List<TypedAst.Type> typeArguments) {
    if (typeParameterIds.size() != typeArguments.size()) {
      errorsAccum.add(
          "Wrong type argument count: Expected %d, but got %d",
          typeParameterIds.size(), typeArguments.size());
      typeArguments =
          shrinkOrPadToFixedSize(typeArguments, typeParameterIds.size(), INVALID_EXPRESSION_TYPE);
    }

    return TypeReplacementMap.from(typeParameterIds, typeArguments);
  }

  static <T> List<T> shrinkOrPadToFixedSize(List<T> ls, int size, T pad) {
    return Stream.concat(ls.stream(), Stream.generate(() -> pad)).limit(size).toList();
  }

  private static boolean assertGivenNeededArguments(
      final ErrorAccum errorsAccum,
      final FunctionInfoMonomorphed functionInfo,
      List<?> argumentList) {
    if (functionInfo.valueParameterTypes().size() != argumentList.size()) {
      errorsAccum.add(
          "Wrong argument count for %s: Expected %d, but given %d",
          functionInfo.uniqueVariableId().toString(false),
          functionInfo.valueParameterTypes().size(),
          argumentList.size());
      return false;
    }
    return true;
  }

  /** Type checks {@link ZkAst.Expression.Call} expressions. */
  static TypedAst.TypedExpression typeCheckExpressionFunctionCall(
      final ZkAst.Expression.Call expr,
      final ErrorAccum errorsAccum,
      final Context context,
      final TypedAst.Type functionResultExpectedType) {

    final ZkAst.Expression.Variable functionExpr;

    // Call expression in general are not supported
    if (expr.functionExpr() instanceof ZkAst.Expression.Variable checkedExpr) {
      functionExpr = checkedExpr;
    } else {
      errorsAccum.add(expr, "Unsupported expression: Indirect function call");
      return INVALID_EXPRESSION_EXPR;
    }

    final FunctionInfoMonomorphed functionInfo =
        getMonomorphedFunction(errorsAccum, context, functionExpr.variablePath());
    if (functionInfo == null) {
      return INVALID_EXPRESSION_EXPR;
    }

    final List<TypedAst.TypedExpression> argumentExpressions =
        translateArgumentExpressions(functionInfo, expr.valueArgumentExprs(), context, errorsAccum);

    return typeCheckFunctionCall(functionInfo, argumentExpressions, errorsAccum);
  }

  private static List<TypedAst.Type> inferTypeArgumentsToFunction(
      final FunctionInfoGeneric functionInfoGeneric,
      final List<TypedAst.TypedExpression> typedParameterExprs,
      final Context context,
      final ErrorAccum errorsAccum) {
    // Quick return if no type parameters are defined for function
    final List<TypedAst.Type> producedTypes =
        typedParameterExprs.stream().map(TypedAst.TypedExpression::resultType).toList();
    final List<TypedAst.Type> expectedTypes =
        functionInfoGeneric.valueParameterTypesWithTypeParameters();

    final Map<TypedAst.TypeId, TypedAst.Type> freeTypesUnification =
        TypeUtility.performTypeUnification(
            expectedTypes, producedTypes, context.fileContext().typeEnvironment().keySet());

    if (freeTypesUnification == null) {
      errorsAccum.add("Type unification of function arguments failed");
      return functionInfoGeneric.typeParameterNames().stream()
          .map(x -> (TypedAst.Type) new TypedAst.Type.ErrorType())
          .toList();
    }

    return functionInfoGeneric.typeParameterNames().stream()
        .map(n -> TypedAst.TypeId.newFor(n))
        .map(freeTypesUnification::get)
        .toList();
  }

  /**
   * Translates {@link ZkAst.Expression.MethodCall} expressions.
   *
   * <p>Mostly equivalent to {@link #typeCheckFunctionCall}, but with function scoping taken from
   * the object, and the object being used as the first argument.
   */
  static TypedAst.TypedExpression typeCheckExpressionMethodCall(
      final ZkAst.Expression.MethodCall expr,
      final ErrorAccum errorsAccum,
      final Context context,
      final TypedAst.Type functionResultExpectedType) {

    // Translate object expression
    final TypedAst.TypedExpression typedObjectExpr =
        typeCheckExpression(expr.objectExpr(), errorsAccum, context, null);

    // Determine method
    final FunctionInfoGeneric methodInfoGeneric =
        (FunctionInfoGeneric)
            context.getStaticField(
                errorsAccum, typedObjectExpr.resultType(), expr.methodIdentifier());

    if (methodInfoGeneric == null) {
      return INVALID_EXPRESSION_EXPR;
    }

    // Create arguments collection
    final List<TypedAst.TypedExpression> additionalArgumentExpressions =
        translateArgumentExpressions(null, expr.parameterExprs(), context, errorsAccum);

    final List<TypedAst.TypedExpression> argumentExpressions =
        Stream.concat(Stream.of(typedObjectExpr), additionalArgumentExpressions.stream()).toList();

    // Monomorph function
    final List<TypedAst.Type> typeArguments =
        inferTypeArgumentsToFunction(methodInfoGeneric, argumentExpressions, context, errorsAccum);

    final FunctionInfoMonomorphed functionInfo =
        monomorphFunctionTypedArguments(errorsAccum, methodInfoGeneric, typeArguments);

    // Type check function call itself.
    return typeCheckFunctionCall(functionInfo, argumentExpressions, errorsAccum);
  }

  /** Translate argument expressions, with type inference. Does not check types. */
  private static List<TypedAst.TypedExpression> translateArgumentExpressions(
      final FunctionInfoMonomorphed functionInfo,
      final List<ZkAst.Expression> valueArgumentExprs,
      final Context context,
      final ErrorAccum errorsAccum) {

    final List<TypedAst.TypedExpression> argumentExpressions = new ArrayList<>();
    for (int argumentIndex = 0; argumentIndex < valueArgumentExprs.size(); argumentIndex++) {

      final TypedAst.Type expectedArgumentType =
          functionInfo == null
              ? null
              : getFromListOrNull(functionInfo.valueParameterTypes(), argumentIndex);
      final ZkAst.Expression argumentExpr = valueArgumentExprs.get(argumentIndex);

      final TypedAst.TypedExpression typedArgumentExpr =
          typeCheckExpression(argumentExpr, errorsAccum, context, expectedArgumentType);

      argumentExpressions.add(typedArgumentExpr);
    }

    return List.copyOf(argumentExpressions);
  }

  private static <T> T getFromListOrNull(final List<T> list, final int index) {
    return index < list.size() ? list.get(index) : null;
  }

  private static TypedAst.TypedExpression typeCheckFunctionCall(
      final FunctionInfoMonomorphed functionInfo,
      final List<TypedAst.TypedExpression> argumentExpressions,
      final ErrorAccum errorsAccum) {

    // Check is called with the correct number of arguments
    final boolean hasNeededArguments =
        assertGivenNeededArguments(errorsAccum, functionInfo, argumentExpressions);
    if (!hasNeededArguments) {
      return INVALID_EXPRESSION_EXPR;
    }

    // Check each expression is of the correct type.
    for (int argumentIndex = 0;
        argumentIndex < functionInfo.valueParameterTypes().size();
        argumentIndex++) {
      assertIsSubtype(
          argumentExpressions.get(argumentIndex).resultType(),
          functionInfo.valueParameterTypes().get(argumentIndex),
          errorsAccum);
    }

    final boolean isConstantExpression = false; // Pessimistic for now

    return new TypedAst.TypedExpression(
        new TypedAst.Expression.CallDirectly(functionInfo.uniqueVariableId(), argumentExpressions),
        functionInfo.returnType(),
        isConstantExpression,
        false,
        positionUnknown(errorsAccum));
  }

  private static TypedAst.PosInterval positionUnknown(ErrorAccum errorsAccum) {
    return new TypedAst.PosInterval(errorsAccum.filepath(), POSITION_UNKNOWN);
  }

  static TypedAst.TypedExpression typeCheckExpressionStructConstructor(
      final ZkAst.Expression.StructConstructor expression,
      final ErrorAccum errorsAccum,
      final Context context) {

    // Check type of constructor type
    final ZkAst.PosInterval pos = expression.typePath().position();
    final TypedAst.Type uncheckedConstructorType =
        typeCheckType(
            new ZkAst.Type.Named(expression.typePath(), expression.position()),
            errorsAccum,
            context);

    final StructInfoMonomorphed structDefinition =
        getMonomorphedStruct(errorsAccum.withPos(pos), context, uncheckedConstructorType);

    // Check that fields are as expected by type
    final List<TypedAst.Expression.StructConstructor.FieldInit> fieldInits = new ArrayList<>();
    for (final ZkAst.Expression.StructConstructor.Field field : expression.fieldInits()) {
      final TypedAst.Type inferredType =
          structDefinition == null ? null : structDefinition.typeOfField(field.fieldName());

      final TypedAst.TypedExpression typedExpression =
          typeCheckExpression(field.fieldInitExpr(), errorsAccum, context, inferredType);

      fieldInits.add(
          new TypedAst.Expression.StructConstructor.FieldInit(field.fieldName(), typedExpression));
    }

    return structDefinition == null
        ? INVALID_EXPRESSION_EXPR
        : typeCheckExpressionStructConstructor(structDefinition, fieldInits, errorsAccum, pos);
  }

  private static TypedAst.TypedExpression typeCheckExpressionStructConstructor(
      final StructInfoMonomorphed structDefinition,
      final List<TypedAst.Expression.StructConstructor.FieldInit> fieldInits,
      final ErrorAccum errorsAccum,
      final ZkAst.PosInterval pos) {

    // Check that fields are as expected by type
    for (final TypedAst.Expression.StructConstructor.FieldInit typedField : fieldInits) {
      final TypedAst.Type expectedFieldType = structDefinition.typeOfField(typedField.fieldName());

      // Error check
      if (expectedFieldType == null) {
        errorsAccum.add(
            pos,
            "Unknown field '%s' in type %s",
            typedField.fieldName(),
            structDefinition.typeName());
        continue;
      }

      assertIsSubtype(
          typedField.fieldInitExpr().resultType(), expectedFieldType, errorsAccum.withPos(pos));
    }

    // Check that all type fields have been covered
    final Set<String> missingFields = new HashSet<>(structDefinition.fieldByName().keySet());
    missingFields.removeAll(fieldInits.stream().map(x -> x.fieldName()).toList());
    if (!missingFields.isEmpty()) {
      errorsAccum.add(
          pos,
          "%s constructor missing fields '%s'",
          structDefinition.typeName().typeId(),
          String.join("', '", missingFields));
    }

    final boolean isConstantExpression = false; // Pessimistic for now.
    final boolean containsGuessedType = false;

    // Create typed expression
    return new TypedAst.TypedExpression(
        new TypedAst.Expression.StructConstructor(
            structDefinition.typeName(), List.copyOf(fieldInits)),
        structDefinition.typeName(),
        isConstantExpression,
        containsGuessedType,
        new TypedAst.PosInterval(errorsAccum.filepath(), pos));
  }

  /**
   * Type checks types.
   *
   * @param uncheckedType Type to type check.
   * @param errorsAccum Appendable list of errors.
   * @param context Context to type check expression against.
   * @return Typed type for the given type and context.
   */
  static TypedAst.Type typeCheckType(
      final ZkAst.Type uncheckedType, final ErrorAccum errorsAccum, final Context context) {

    // Named types
    if (uncheckedType instanceof ZkAst.Type.Named namedType) {
      return typeCheckNamedType(namedType, errorsAccum, context);

      // Tuple types
    } else if (uncheckedType instanceof ZkAst.Type.Tuple type) {
      return new TypedAst.Type.Tuple(
          type.subtypes().stream().map(x -> typeCheckType(x, errorsAccum, context)).toList());

      // Array types
    } else if (uncheckedType instanceof ZkAst.Type.Array type) {

      // Ensure array sizes are correctly typed using usize
      final TypedAst.TypedExpression typedExprSize =
          typeCheckExpression(type.sizeExpr(), errorsAccum, context, ARRAY_SIZE_TYPE);
      assertIsSubtype(
          typedExprSize.resultType(), ARRAY_SIZE_TYPE, errorsAccum.withPos(type.sizeExpr()));

      final TypedAst.Type elementType = typeCheckType(type.baseType(), errorsAccum, context);
      final int arraySize = sizeOfArrayType(errorsAccum.withPos(type.sizeExpr()), typedExprSize);
      return new TypedAst.Type.Array(elementType, arraySize);

      // Unknown
    } else {
      errorsAccum.add(
          uncheckedType.position(),
          "Unsupported type: %s",
          prettyTypeForErrorMessage(uncheckedType));
      return INVALID_EXPRESSION_TYPE;
    }
  }

  private static int sizeOfArrayType(ErrorAccum errorsAccum, TypedAst.TypedExpression sizeExpr) {

    if (!sizeExpr.isConstantExpression()) {
      errorsAccum.add("Array size expression must be constant");
    }

    if (sizeExpr.expr() instanceof TypedAst.Expression.IntLiteral size) {
      return size.value().intValue();

    } else {
      errorsAccum.add("Non-literal sizes for array types are not supported");
      return 0;
    }
  }

  private static TypedAst.Type typeCheckNamedType(
      final ZkAst.Type.Named type, final ErrorAccum errorsAccum, final Context context) {

    // Check whether type is known
    final TypeAliasInfo referencedType = context.getType(type.namePath());
    if (referencedType == null) {
      errorsAccum.add(
          type.position(), "Unknown type %s", ZkAstPretty.prettyPath(type.namePath(), false));
      return INVALID_EXPRESSION_TYPE;
    }

    final List<TypedAst.Type> typedTypeParameters =
        type.namePath().genericTypes().stream()
            .map(t -> typeCheckType(t, errorsAccum, context))
            .toList();

    // Directly named types need their types replaced.
    final var typeReplacementMap =
        typeReplacementMapFromIds(
            errorsAccum.withPos(type), referencedType.typeParameterNames(), typedTypeParameters);
    return typeReplacementMap.replaceParametersInType(referencedType.typeWithTypeParameters());
  }

  private static String prettyTypeForErrorMessage(ZkAst.Type type) {
    return ZkAstPretty.prettyType(type)
        .replaceAll("\n", "  ")
        .replaceAll("\r", "")
        .replaceAll("  +", " ");
  }

  /**
   * Type checks struct type definition.
   *
   * @param structDefinitionItem Struct item definition to type check.
   * @param errorsAccum Appendable list of errors.
   * @param fileContext Context to type check function against.
   * @return Typed type definition.
   */
  private static TypedAst.StructDefinition typeCheckStructDefinition(
      final ZkAst.ProgramItem<ZkAst.StructDefinition> structDefinitionItem,
      final ErrorAccum errorsAccum,
      final FileContext fileContext) {

    final TypedAst.TypeId typeId =
        new TypedAst.TypeId(
            fileContext.fileModulePath().resolve(structDefinitionItem.item().structIdentifier()));

    final ZkAst.StructDefinition structDefinition = structDefinitionItem.item();

    // Generic arguments
    if (!structDefinition.genericTypeParams().isEmpty()) {
      errorsAccum.add(structDefinition, "Defining type generic structs is not supported");
    }

    // Check that all field names are unique
    final List<String> duplicatedFieldNames =
        structDefinition.fields().stream()
            .map(field -> field.fieldName())
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
            .entrySet()
            .stream()
            .filter(x -> x.getValue() > 1)
            .map(Map.Entry::getKey)
            .toList();

    if (!duplicatedFieldNames.isEmpty()) {
      errorsAccum.add(
          structDefinition,
          "Struct '%s' has duplicated field names '%s'",
          structDefinition.structIdentifier(),
          String.join("', '", duplicatedFieldNames));
    }

    typeCheckOuterAttributes(
        errorsAccum.withPos(structDefinition),
        structDefinitionItem.attributes(),
        KnownAttribute::isAllowedOnStruct);

    // Type check individual fields
    final var fields =
        structDefinition.fields().stream()
            .map(
                field ->
                    new TypedAst.StructDefinition.StructField(
                        field.fieldName(),
                        typeCheckType(
                            field.fieldType(), errorsAccum, fileContext.childContext(null))))
            .toList();

    final List<String> typeParameterNames = List.of();
    return new TypedAst.StructDefinition(typeId, typeParameterNames, fields);
  }

  static TypedAst.Type typeCheckBinop(
      final ZkAst.Binop operation,
      final ErrorAccum errorsAccum,
      final TypedAst.Type typeLeft,
      final TypedAst.Type typeRight,
      final BinopPosData pos) {
    // Arithmetic
    if (BINOPS_ARITH.contains(operation)) {
      final boolean valid =
          assertTypesEquivalent(
              typeLeft, typeRight, errorsAccum, TypeCheck::assertIsIntegerType, pos);
      return valid ? typeLeft : INVALID_EXPRESSION_TYPE;

      // Logical
    } else if (BINOPS_LOGICAL.contains(operation)) {
      final boolean valid =
          assertTypesEquivalent(
              typeLeft, typeRight, errorsAccum, TypeCheck::assertIsBooleanType, pos);
      return valid ? TypeUtility.comparisonResultType(typeLeft) : INVALID_EXPRESSION_TYPE;

      // Equality binops
    } else if (BINOPS_EQUALITY.contains(operation)) {
      final boolean valid =
          assertTypesEquivalent(typeLeft, typeRight, errorsAccum.withPos(pos.posOp()));
      return valid ? TypeUtility.comparisonResultType(typeLeft) : INVALID_EXPRESSION_TYPE;

      // Comparison binops
    } else if (BINOPS_COMPARISON.contains(operation)) {
      final boolean valid =
          assertTypesEquivalent(
              typeLeft, typeRight, errorsAccum, TypeCheck::assertIsIntegerType, pos);
      return valid ? TypeUtility.comparisonResultType(typeLeft) : INVALID_EXPRESSION_TYPE;

      // Bitshift operations
    } else if (BINOPS_BITSHIFTS.contains(operation)) {
      if (!isPublicInteger(typeRight)) {
        errorsAccum.add(
            pos.posOp(),
            "Unsupported expression: Bitshift with anything other than public integer as"
                + " right-hand side");
        return INVALID_EXPRESSION_TYPE;
      } else {
        return typeLeft;
      }

      // Index operation
    } else if (ZkAst.Binop.INDEX.equals(operation)) {
      assertIsSubtype(typeRight, TYPE_OF_INDEX_RHS, errorsAccum.withPos(pos.posRight()));

      // Determine inner type
      if (typeLeft instanceof TypedAst.Type.Array arr) {
        return arr.elementType();
      } else {
        if (!typeLeft.isError()) {
          errorsAccum.add(pos.posLeft(), "%s is not an array type", typeLeft);
        }
        return INVALID_EXPRESSION_TYPE;
      }

      // Unknown
    } else {
      errorsAccum.add(pos.posOp(), "Unsupported expression: %s", operation);
      return INVALID_EXPRESSION_TYPE;
    }
  }

  /** Type used for the index operand for indexing operations. */
  private static final TypedAst.Type TYPE_OF_INDEX_RHS = TypedAst.Type.Int.USIZE;

  /** Arithmetic binops. Has signature: T is integer type: T o T = T */
  private static final Set<ZkAst.Binop> BINOPS_ARITH =
      Set.of(
          ZkAst.Binop.MULT,
          ZkAst.Binop.DIV,
          ZkAst.Binop.REMAINDER,
          ZkAst.Binop.PLUS,
          ZkAst.Binop.MINUS,
          ZkAst.Binop.BITWISE_AND,
          ZkAst.Binop.BITWISE_XOR,
          ZkAst.Binop.BITWISE_OR);

  /** Logical binops. Has signature: T is boolean type: T o T = T */
  private static final Set<ZkAst.Binop> BINOPS_LOGICAL =
      Set.of(ZkAst.Binop.LOGICAL_AND, ZkAst.Binop.LOGICAL_OR);

  /** Equality binops. Has signature: T is any type: T o T = bool */
  private static final Set<ZkAst.Binop> BINOPS_EQUALITY =
      Set.of(ZkAst.Binop.EQUAL, ZkAst.Binop.NOT_EQUAL);

  /** Comparison binops. Has signature: T is integer type: T o T = bool */
  private static final Set<ZkAst.Binop> BINOPS_COMPARISON =
      Set.of(
          ZkAst.Binop.LESS_THAN,
          ZkAst.Binop.LESS_THAN_OR_EQUAL,
          ZkAst.Binop.GREATER_THAN,
          ZkAst.Binop.GREATER_THAN_OR_EQUAL);

  /**
   * Bitshift binops. Has signature: {@code T_1} is integer types, {@code T_2} is a public integer
   * type : {@code T_1 o T_2 = T_1}
   */
  private static final Set<ZkAst.Binop> BINOPS_BITSHIFTS =
      Set.of(ZkAst.Binop.BITSHIFT_RIGHT, ZkAst.Binop.BITSHIFT_LEFT);

  /**
   * Type checks block expression.
   *
   * @param blockExpression Block expression to type check.
   * @param errorsAccum Appendable list of errors.
   * @param parentContext Context to type check block expression against.
   * @param inferredType Possible type of the given expression, inferred from outside context.
   *     Should be null if no type could be inferred.
   * @return Typed expression for the given expression and context.
   */
  private static TypedAst.TypedExpression typeCheckBlock(
      final ZkAst.Expression.Block blockExpression,
      final ErrorAccum errorsAccum,
      final Context parentContext,
      final TypedAst.Type inferredType) {

    final var context = parentContext.childContext();

    // Type check statements
    final var typedStatements = new ArrayList<TypedAst.Statement>();
    for (final ZkAst.Statement uncheckedStatement : blockExpression.statements()) {

      final TypedAst.Statement typedStatement;

      // Expression statement
      if (uncheckedStatement instanceof ZkAst.Statement.Expr statement) {
        final TypedAst.TypedExpression expr =
            typeCheckExpression(statement.expr(), errorsAccum, context, null);

        typedStatement = new TypedAst.Statement.Expr(expr);

        // Let statement
      } else if (uncheckedStatement instanceof ZkAst.Statement.Let statement) {

        final TypedAst.Type declaredVariableType =
            statement.type() == null ? null : typeCheckType(statement.type(), errorsAccum, context);

        final TypedAst.TypedExpression typedExprInit =
            typeCheckExpression(statement.initExpr(), errorsAccum, context, declaredVariableType);

        if (declaredVariableType != null) {
          assertIsSubtype(
              typedExprInit.resultType(), declaredVariableType, errorsAccum.withPos(statement));
        }

        final VariableInfo variableInfo =
            new VariableInfoSimple(
                TypedAst.VariableId.newFor(TypedAst.EnvKind.VARIABLE_LET, statement.identifier()),
                typedExprInit.resultType(),
                statement.isMutable(),
                statement.position());
        context.setVariable(statement.identifier(), variableInfo, null);

        typedStatement = new TypedAst.Statement.Let(variableInfo.uniqueVariableId(), typedExprInit);

        // Return statement
      } else {
        final ZkAst.Statement.Return statement = (ZkAst.Statement.Return) uncheckedStatement;

        final TypedAst.TypedExpression typedResultExpr =
            typeCheckExpression(statement.resultExpr(), errorsAccum, context, context.returnType());

        if (context.returnType() == null) {
          errorsAccum.add(statement, "Return must not occur in return-less context");
        } else {
          assertIsSubtype(
              typedResultExpr.resultType(), context.returnType(), errorsAccum.withPos(statement));
        }

        typedStatement =
            new TypedAst.Statement.Return(
                typedResultExpr,
                new TypedAst.PosInterval(errorsAccum.filepath(), statement.position()));
      }

      typedStatements.add(typedStatement);
    }

    // Type check expression, directly returning the result of the expression.
    final TypedAst.TypedExpression typedExprResult =
        typeCheckExpression(blockExpression.returnExpr(), errorsAccum, context, inferredType);

    final boolean isConstantExpression = false; // Pessimistic for now.

    return new TypedAst.TypedExpression(
        new TypedAst.Expression.Block(List.copyOf(typedStatements), typedExprResult),
        typedExprResult.resultType(),
        isConstantExpression,
        typedExprResult.containsGuessedType(),
        new TypedAst.PosInterval(errorsAccum.filepath(), blockExpression.position()));
  }

  //// Asserters

  private static void assertIntegerFits(
      final BigInteger val, final TypedAst.Type type, final ErrorAccum errorsAccum) {
    if (type instanceof TypedAst.Type.Int intType) {
      final int valBitCount = val.bitLength();
      final int maxBits = intType.bitwidth() - (intType.isSigned() ? 1 : 0);
      final boolean isUnsignedNegative = val.signum() < 0 && !intType.isSigned();
      if (valBitCount > maxBits || isUnsignedNegative) {
        errorsAccum.add("Value %s doesn't fit in %s", val, type);
      }
    } else {
      errorsAccum.add("Non-integer type %s not allowed in integer literal", type);
    }
  }

  private static boolean assertIsIntegerType(
      final TypedAst.Type type, final ErrorAccum errorsAccum) {
    final boolean valid = isIntegerType(type);
    if (!valid) {
      errorsAccum.add("Expected integer type, but got %s", type);
    }
    return valid;
  }

  private static boolean assertIsBooleanType(
      final TypedAst.Type type, final ErrorAccum errorsAccum) {
    final boolean valid = isBooleanType(type);
    if (!valid) {
      errorsAccum.add("Expected boolean type, but got %s", type);
    }
    return valid;
  }

  private static void assertIsSubtype(
      final TypedAst.Type subtype, final TypedAst.Type supertype, final ErrorAccum errorsAccum) {
    requireNonNull(subtype);
    requireNonNull(supertype);
    final boolean isSubtype = isSubtype(subtype, supertype);
    if (!isSubtype) {
      errorsAccum.add("Expected %s, but got %s", supertype, subtype);
    }
  }

  private static void assertIsExplicitlyCastable(
      final TypedAst.Type originType,
      final TypedAst.Type targetType,
      final ErrorAccum errorsAccum) {
    // Public casting
    if (originType instanceof TypedAst.Type.Int && targetType instanceof TypedAst.Type.Int) {
      return;
    }

    // Sbi casting
    if (originType instanceof TypedAst.Type.Sbi && targetType instanceof TypedAst.Type.Sbi) {
      return;
    }

    errorsAccum.add("%s is not castable to %s", originType, targetType);
  }

  private static boolean assertTypesEquivalent(
      final TypedAst.Type type1, final TypedAst.Type type2, final ErrorAccum errorsAccum) {
    final boolean isEquivalent = isEquivalent(type1, type2);
    if (!isEquivalent) {
      errorsAccum.add("Expected identical types, but got %s and %s", type1, type2);
    }
    return isEquivalent;
  }

  private static boolean assertTypesEquivalent(
      final TypedAst.Type type1,
      final TypedAst.Type type2,
      final ErrorAccum errorsAccum,
      final BiPredicate<TypedAst.Type, ErrorAccum> pred,
      final BinopPosData pos) {
    final boolean type1Valid = pred.test(type1, errorsAccum.withPos(pos.posLeft()));
    final boolean type2Valid = pred.test(type2, errorsAccum.withPos(pos.posRight()));
    if (type1Valid && type2Valid) {
      return assertTypesEquivalent(type1, type2, errorsAccum.withPos(pos.posOp()));
    } else {
      return false;
    }
  }

  /**
   * Small container for {@link ZkAst.Binop} related position data, containing the position of the
   * operation symbol itself, and the left and right expressions.
   */
  record BinopPosData(
      ZkAst.PosInterval posOp, ZkAst.PosInterval posLeft, ZkAst.PosInterval posRight) {
    public BinopPosData {
      requireNonNull(posOp);
      requireNonNull(posLeft);
      requireNonNull(posRight);
    }
  }

  // Type Predicates

  private static boolean isEquivalent(final TypedAst.Type type1, final TypedAst.Type type2) {
    return isSubtype(type1, type2);
  }

  private static boolean isSubtype(final TypedAst.Type subtype, final TypedAst.Type supertype) {
    // Error type is always compatible
    if (subtype.isError() || supertype.isError()) {
      return true;

      // Tuple types
    } else if (subtype instanceof TypedAst.Type.Tuple tupl1
        && supertype instanceof TypedAst.Type.Tuple tupl2
        && tupl1.subtypes().size() == tupl2.subtypes().size()) {
      return pairwiseAllMatch(tupl1.subtypes(), tupl2.subtypes(), TypeCheck::isSubtype);

      // Other types
    } else {
      return subtype.equals(supertype);
    }
  }

  /**
   * Performs a pairwise allMatch operation.
   *
   * @param list1 First list.
   * @param list2 Second list.
   * @param pred BiPredicate to operate over lists.
   * @return Whether all elements pairwise matched the predicate
   */
  private static <T, S> boolean pairwiseAllMatch(
      final List<T> list1, final List<S> list2, final BiPredicate<T, S> pred) {
    final int maxIdx = Integer.min(list1.size(), list2.size());
    for (int idx = 0; idx < maxIdx; idx++) {
      final boolean success = pred.test(list1.get(idx), list2.get(idx));
      if (!success) {
        return false;
      }
    }
    return true;
  }

  private static boolean isIntegerType(final TypedAst.Type type) {
    if (type.isError()) {
      return true;
    }
    if (type instanceof TypedAst.Type.Int || type instanceof TypedAst.Type.Sbi) {
      return true;
    }
    return false;
  }

  private static boolean isPublicInteger(final TypedAst.Type type) {
    if (type.isError()) {
      return true;
    }
    return type instanceof TypedAst.Type.Int;
  }

  private static boolean isBooleanType(final TypedAst.Type uncheckedType) {
    if (uncheckedType.isError()) {
      return true;
    }
    return uncheckedType.equals(TypedAst.Type.Int.BOOL)
        || uncheckedType.equals(TypedAst.Type.Sbi.BOOL);
  }

  /**
   * Error accumulator for type errors.
   *
   * <p>Picks up errors that have been reported in {@link TypeCheck} along the way and aggregates
   * them. Adding new errors is done with one of the {@link ErrorAccum#add} methods. Each
   * accumulator possess an associated program location for easy formatting, and this can be
   * overwritten using {@link ErrorAccum#withPos}.
   *
   * @param filepath Path to the current file to emit errors for.
   * @param errors Extendable list which will end up storing any errors generated during type
   *     checking.
   * @param position Position within file to emit for.
   */
  public record ErrorAccum(
      Path filepath, List<ErrorPresenter.ErrorLine> errors, ZkAst.PosInterval position) {

    /**
     * Constructor for error accumulator.
     *
     * @param filepath Path to the current file to emit errors for.
     * @param errors Extendable list which will end up storing any errors generated during type
     *     checking.
     * @param position Position within file to emit for.
     */
    public ErrorAccum {
      requireNonNull(filepath);
      requireNonNull(errors);
      requireNonNull(position);
    }

    /**
     * Adds another error to the error accumulator, using the position of the AST element, and
     * formatting the message using the given args.
     *
     * @param element AST element to extract position from.
     * @param message Error message format string.
     * @param args Argument to error message.
     */
    @FormatMethod
    public void add(final ZkAst.HasPosition element, final String message, final Object... args) {
      add(element.position(), message, args);
    }

    /**
     * Adds another error to the error accumulator, using the given position , and formatting the
     * message using the given args.
     *
     * @param position Position of the error.
     * @param message Error message format string.
     * @param args Argument to error message.
     */
    @FormatMethod
    public void add(final ZkAst.PosInterval position, final String message, final Object... args) {
      add(ErrorPresenter.ErrorLine.from(filepath, position, message.formatted(args)));
    }

    /**
     * Adds another error to the error accumulator, using the given position , and formatting the
     * message using the given args.
     *
     * @param errorLine Manually constructed errorline.
     */
    public void add(final ErrorPresenter.ErrorLine errorLine) {
      errors.add(errorLine);
    }

    /**
     * Adds another error to the error accumulator, using the implicit position , and formatting the
     * message using the given args.
     *
     * @param message Error message format string.
     * @param args Argument to error message.
     */
    @FormatMethod
    public void add(final String message, final Object... args) {
      add(position(), message, args);
    }

    /**
     * Produces an error accumulator with position, for situations where giving the position
     * seperately is cumbersome.
     *
     * @param astElement AST element to determine position from.
     * @return Error accumulator with position, adding to this.
     */
    public ErrorAccum withPos(final ZkAst.HasPosition astElement) {
      return new ErrorAccum(filepath, errors, astElement.position());
    }

    /**
     * Produces an error accumulator with position, for situations where giving the position
     * seperately is cumbersome.
     *
     * @param position Position to use.
     * @return Error accumulator with position, adding to this.
     */
    public ErrorAccum withPos(final ZkAst.PosInterval position) {
      return new ErrorAccum(filepath, errors, position);
    }
  }

  /**
   * Determines whether the item with the given path have been imported by the given use
   * declaration.
   *
   * @param useDecl Use declaration.
   * @param itemPath Item name path for an item in the module.
   * @return True if the element should be imported.
   */
  private static boolean isImportedElement(
      ZkAst.UseDeclaration useDecl, NamePath.Absolute itemPath) {
    if (useDecl.moduleElements() == null) {
      return true;
    }
    return useDecl.moduleElements().contains(itemPath.path().get(useDecl.modulePath().size()));
  }
}
