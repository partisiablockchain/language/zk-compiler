package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** Utility class for working with {@link TypedAst.Type}. */
public final class TypeUtility {

  private TypeUtility() {}

  /**
   * Determines what type should be produced by comparing two elements of the given type.
   *
   * @param uncheckedType Type of compared elements.
   * @return Type resulting from comparison.
   */
  public static TypedAst.Type comparisonResultType(final TypedAst.Type uncheckedType) {
    return containsSbi(uncheckedType) ? TypedAst.Type.Sbi.BOOL : TypedAst.Type.Int.BOOL;
  }

  private static boolean containsSbi(final TypedAst.Type uncheckedType) {
    // Primitive type
    if (uncheckedType instanceof TypedAst.Type.Sbi primitiveType) {
      return true;
    } else if (uncheckedType instanceof TypedAst.Type.Int primitiveType) {
      return false;
    } else if (uncheckedType instanceof TypedAst.Type.Tuple type) {
      return type.subtypes().stream().anyMatch(TypeUtility::containsSbi);
    } else if (uncheckedType instanceof TypedAst.Type.NamedType type) {
      throw new UnsupportedOperationException("Named type comparison not supported");
    } else if (uncheckedType instanceof TypedAst.Type.ErrorType type) {
      return false;
    } else {
      final TypedAst.Type.Array type = (TypedAst.Type.Array) uncheckedType;
      return containsSbi(type.elementType());
    }
  }

  /**
   * Sub-system capable of simple type unification.
   *
   * <p>Type unification is the process of solving for the unknown types in a set of type
   * equalities.
   *
   * <p>The two type lists are pairwise set equals each other, and used to attempt to solve the
   * unification problem.
   *
   * @param uncheckedTypes1 First list of types.
   * @param uncheckedTypes2 Second list of types. Must be same length as {@code uncheckedTypes1}.
   * @param boundNames Well known type names.
   * @return Map from type ids to the bound types. Null if unification failed.
   */
  public static Map<TypedAst.TypeId, TypedAst.Type> performTypeUnification(
      final List<TypedAst.Type> uncheckedTypes1,
      final List<TypedAst.Type> uncheckedTypes2,
      final Set<NamePath.Absolute> boundNames) {
    final Map<TypedAst.TypeId, TypedAst.Type> assignments = new HashMap<>();
    final boolean couldUnify =
        performTypeUnification(assignments, uncheckedTypes1, uncheckedTypes2, boundNames);
    return couldUnify ? assignments : null;
  }

  private static boolean performTypeUnification(
      final Map<TypedAst.TypeId, TypedAst.Type> assignments,
      final List<TypedAst.Type> uncheckedTypes1,
      final List<TypedAst.Type> uncheckedTypes2,
      final Set<NamePath.Absolute> boundNames) {
    if (uncheckedTypes1.size() != uncheckedTypes2.size()) {
      return false;
    }

    for (int idx = 0; idx < uncheckedTypes1.size(); idx++) {
      final boolean couldUnify =
          performTypeUnification(
              assignments, uncheckedTypes1.get(idx), uncheckedTypes2.get(idx), boundNames);
      if (!couldUnify) {
        return false;
      }
    }

    return true;
  }

  private static boolean performTypeUnification(
      final Map<TypedAst.TypeId, TypedAst.Type> assignments,
      final TypedAst.Type uncheckedType1,
      final TypedAst.Type uncheckedType2,
      final Set<NamePath.Absolute> boundNames) {
    // Tuple
    if (uncheckedType1 instanceof TypedAst.Type.Tuple type1
        && uncheckedType2 instanceof TypedAst.Type.Tuple type2) {
      return performTypeUnification(assignments, type1.subtypes(), type2.subtypes(), boundNames);

      // Array
    } else if (uncheckedType1 instanceof TypedAst.Type.Array type1
        && uncheckedType2 instanceof TypedAst.Type.Array type2) {
      return performTypeUnification(
          assignments, type1.elementType(), type2.elementType(), boundNames);

      // Identical types. No information can be gained.
    } else if (uncheckedType1.equals(uncheckedType2)) {
      return true;

      // Data structures with generics
    } else if (uncheckedType1 instanceof TypedAst.Type.NamedType type1
        && uncheckedType2 instanceof TypedAst.Type.NamedType type2
        && type1.typeId().equals(type2.typeId())) {
      return performTypeUnification(
          assignments, type1.typeArguments(), type2.typeArguments(), boundNames);

      // Named type
    } else if (uncheckedType1 instanceof TypedAst.Type.NamedType type1
        && !boundNames.contains(type1.typeId().fullNamePath())) {
      final var existingAssignment = assignments.put(type1.typeId(), uncheckedType2);
      return existingAssignment == null || existingAssignment.equals(uncheckedType2);

      // Named type
    } else if (uncheckedType2 instanceof TypedAst.Type.NamedType type2
        && !boundNames.contains(type2.typeId().fullNamePath())) {
      final var existingAssignment = assignments.put(type2.typeId(), uncheckedType1);
      return existingAssignment == null || existingAssignment.equals(uncheckedType1);

      // Unknown types
    } else {
      return false;
    }
  }
}
