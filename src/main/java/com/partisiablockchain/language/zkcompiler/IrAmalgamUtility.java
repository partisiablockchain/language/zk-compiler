package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;

/**
 * Namespace class for utility methods for working with {@link Ir} amalgams.
 *
 * <p>An <b>Amalgam</b> is a composition pattern for operands, similar to {@link Operand.Tuple}.
 * Whereas a {@link Operand.Tuple} composes operands as an implicit collection while retaining their
 * original references, an <b>Amalgam</b> packs them together into a single {@link Operand.Basic}.
 */
public final class IrAmalgamUtility {

  private IrAmalgamUtility() {}

  /**
   * Emits an amalgam unpacking instruction sequence, extracting each individual field one-by-one
   * from the given amalgam, and produce a composite operand of the requested type.
   *
   * @param uncheckedOutputType Operand type to produce.
   * @param operAmalgam Amalgam operand to unpack from.
   * @param insnAccum Instruction accumulator to write instructions to.
   * @param tmpName Naming info for generated temporary variables.
   * @return Composite operand produced by instructions, as determined by the given {@code
   *     uncheckedOperandType}.
   * @see IrAmalgamUtility#typeAmalgam
   */
  public static Operand emitAmalgamUnpacking(
      final OperandType uncheckedOutputType,
      final Operand.Basic operAmalgam,
      final InstructionAccum insnAccum,
      final TmpName tmpName) {

    // Basic type
    if (uncheckedOutputType instanceof OperandType.BasicType) {
      return operAmalgam;

      // Array
    } else if (uncheckedOutputType instanceof OperandType.ArrayType operandType) {
      return new Operand.Array(operAmalgam, operandType);

      // Tuple
    } else {
      final OperandType.TupleType outputType = (OperandType.TupleType) uncheckedOutputType;
      final List<OperandType> fieldOperandTypes = outputType.componentTypes();
      final List<Operand> fieldOperands = new ArrayList<>();
      int typeOffset = 0;
      for (final OperandType fieldOperandType : fieldOperandTypes) {
        final Operand.Basic operShiftAmount =
            new Operand.Constant(typeOffset, Ir.Type.I32, tmpName);
        final Operand fieldOperand =
            emitAmalgamExtraction(
                fieldOperandType, operAmalgam, operShiftAmount, insnAccum, tmpName);

        fieldOperands.add(fieldOperand);
        typeOffset += operandTypeBitwidth(fieldOperand.operandType());
      }

      return new Operand.Tuple(List.copyOf(fieldOperands));
    }
  }

  /**
   * Emits an amalgam extract instruction sequence, extracting a single value at the given offset.
   *
   * @param outputType Output operand type to produce.
   * @param operAmalgam Amalgam operand to unpack from.
   * @param operShiftAmount Operand describing offset to load value from.
   * @param insnAccum Instruction accumulator to write instructions to.
   * @param tmpName Naming info for generated temporary variables.
   * @return Composite operand produced by instructions, as determined by the given {@code
   *     wantedOperandType}.
   * @see IrAmalgamUtility#typeAmalgam
   */
  public static Operand emitAmalgamExtraction(
      final OperandType outputType,
      final Operand.Basic operAmalgam,
      final Operand.Basic operShiftAmount,
      final InstructionAccum insnAccum,
      final TmpName tmpName) {

    final Ir.Type irOutputType = typeAmalgam(outputType, operAmalgam.type());

    final Operand.Basic operExtracted =
        OperandUtil.addExtract(insnAccum, irOutputType, tmpName, operAmalgam, operShiftAmount);

    return emitAmalgamUnpacking(outputType, operExtracted, insnAccum, tmpName);
  }

  /**
   * Emits an amalgam packing instruction sequence, taking the given operand and packing it into a
   * simple amalgam operand.
   *
   * @param uncheckedOperand Operand to pack.
   * @param insnAccum Instruction accumulator to write instructions to.
   * @param preferredUnitType Type to emit as a unit type. Should be one of {@link Ir.Type#UNIT} or
   *     {@link Ir.Type#UNIT_SBI}.
   * @return Amalgam basic operand.
   * @see IrAmalgamUtility#typeAmalgam
   */
  public static Operand.Basic emitAmalgamPacking(
      final Operand uncheckedOperand,
      final InstructionAccum insnAccum,
      final Ir.Type preferredUnitType) {
    // Basic operand
    if (uncheckedOperand instanceof Operand.Array operand) {
      return operand.underlyingRepresentation();
    } else if (uncheckedOperand instanceof Operand.Basic operand) {
      return operand;
    } else {
      final Operand.Tuple operand = (Operand.Tuple) uncheckedOperand;
      return emitAmalgamPackingTuple(operand, insnAccum, preferredUnitType);
    }
  }

  private static Operand.Basic emitAmalgamPackingTuple(
      final Operand.Tuple operand,
      final InstructionAccum insnAccum,
      final Ir.Type preferredUnitType) {

    final TmpName tmpName = TmpName.root("%spack".formatted(operand.componentOperands().size()));
    final Ir.Type outputType = typeAmalgam(operand.operandType(), preferredUnitType);

    // Tuple operand
    final List<Operand.Basic> operandsInput =
        operand.componentOperands().stream()
            .map(t -> emitAmalgamPacking(t, insnAccum, preferredUnitType))
            .toList();

    // Quick return for unit operands
    if (operandsInput.isEmpty()) {
      return new Operand.Constant(0, outputType, tmpName);
    }

    return concatOperands(operandsInput, insnAccum, tmpName);
  }

  /**
   * Concats all operands together into one large operand.
   *
   * @param <T> The type of {@link Operand.Basic} to concat. Allows the caller to use a list of more
   *     specific types of operands.
   * @param operands Operands in little endian order. Low bits should come first, followed by high
   *     bits.
   * @param insnAccum Instruction accum.
   * @param tmpName Temporary names.
   * @return Combined operand.
   */
  public static <T extends Operand.Basic> Operand.Basic concatOperands(
      final List<T> operands, final InstructionAccum insnAccum, final TmpName tmpName) {
    return IrAmalgamUtility.<Operand.Basic, T>foldToTree(
        operands,
        (bottomBits, topBits) -> OperandUtil.addConcat(insnAccum, tmpName, topBits, bottomBits));
  }

  /**
   * Fold over elements by pairwise applications of {@code fn} until only a single element is left.
   * This creates a tree of depth {@code log_2(elements.size()) + 1}, and at most {@code
   * elements.size()} number of calls to {@code fn}.
   *
   * @param elements Elements to fold over.
   * @param fn Function to apply pairwise.
   * @return The root of the created tree.
   */
  private static <T, E extends T> T foldToTree(
      final List<E> elements, final BiFunction<T, T, T> fn) {
    ArrayList<T> current = new ArrayList<>(elements);

    while (current.size() > 1) {
      final ArrayList<T> next = new ArrayList<>();

      for (int idx = 0; idx < current.size() - 1; idx += 2) {
        next.add(fn.apply(current.get(idx), current.get(idx + 1)));
      }
      if ((current.size() & 1) == 1) {
        next.add(current.get(current.size() - 1));
      }

      // Bookkeeping
      current = next;
    }

    return current.get(0);
  }

  /**
   * Emits an instruction sequence that inserts the given toBeInsertedOperand at offset into
   * targetOperand. Offset operand can be non-constant, which is useful for implementing array
   * assignment.
   *
   * @param targetOperand Array operand to place into.
   * @param toBeInsertedOperand Value operand to splice in.
   * @param offset Offset operand for where to splice into. Is allowed to be non-constant.
   * @param tmpName Temporary name
   * @param insnAccum Instruction accumulator to append to.
   * @return Produced operand (guarenteed array.)
   */
  public static Operand.Array emitAmalgamSplice(
      final Operand.Array targetOperand,
      final Operand.Basic toBeInsertedOperand,
      final Operand.Basic offset,
      final TmpName tmpName,
      final InstructionAccum insnAccum) {
    final Operand.Basic result =
        emitAmalgamSplice(
            targetOperand.underlyingRepresentation(),
            toBeInsertedOperand,
            offset,
            tmpName,
            insnAccum);
    return new Operand.Array(result, targetOperand.operandType());
  }

  /**
   * Emits an instruction sequence for inserting the given {@code toBeInsertedOperand} at {@code
   * offset} into {@code targetOperand}. Offset operand can be non-constant, which is useful for
   * implementing array assignment.
   *
   * @param targetOperand Value operand to place into.
   * @param toBeInsertedOperand Value operand to splice in.
   * @param offset Offset operand for where to splice into. Is allowed to be non-constant.
   * @param tmpName Temporary name
   * @param insnAccum Instruction accumulator to append to.
   * @return Produced operand.
   */
  public static Operand.Basic emitAmalgamSplice(
      final Operand.Basic targetOperand,
      final Operand.Basic toBeInsertedOperand,
      final Operand.Basic offset,
      final TmpName tmpName,
      final InstructionAccum insnAccum) {

    final Ir.Type targetOperandType = targetOperand.type();

    if (offset instanceof Operand.Constant c) {
      return emitAmalgamSpliceConstantOffset(
          targetOperand, toBeInsertedOperand, c, tmpName, insnAccum);
    }

    return insnAccum.addInsert(
        targetOperandType, tmpName, targetOperand, toBeInsertedOperand, offset);
  }

  /**
   * Emits an instruction sequence for inserting the given {@code toBeInsertedOperand} at a constant
   * {@code offset} into {@code targetOperand}.
   *
   * <p>Contrast with {@link emitAmalgamSplice} which produces more cumbersome outputs, but supports
   * non-constant offsets.
   *
   * @param targetOperand Value operand to place into.
   * @param toBeInsertedOperand Value operand to splice in.
   * @param offset Offset operand for where to splice into. Is allowed to be non-constant.
   * @param tmpName Temporary name
   * @param insnAccum Instruction accumulator to append to.
   * @return Produced operand.
   */
  private static Operand.Basic emitAmalgamSpliceConstantOffset(
      final Operand.Basic targetOperand,
      final Operand.Basic toBeInsertedOperand,
      final Operand.Constant offset,
      final TmpName tmpName,
      final InstructionAccum insnAccum) {

    final Ir.Type targetOperandType = targetOperand.type();
    final Ir.Type toInsertType = toBeInsertedOperand.type();

    if (offset.value().equals(BigInteger.ZERO) && targetOperandType.equals(toInsertType)) {
      return toBeInsertedOperand;
    }

    return insnAccum.addInsert(
        targetOperandType, tmpName, targetOperand, toBeInsertedOperand, offset);
  }

  /**
   * Determines the amalgam type of the given operand type.
   *
   * <p>The amalgam type is the type produced by packing all base types of the given operand into
   * the same basic operand.
   *
   * @param operandType Type to determine amalgam type of.
   * @param preferredUnitType Type to emit as a unit type. Should be one of {@link Ir.Type#UNIT} or
   *     {@link Ir.Type#UNIT_SBI}.
   * @return Amalgam type of the given operand type.
   */
  public static Ir.Type typeAmalgam(
      final OperandType operandType, final Ir.Type preferredUnitType) {
    final Set<TypeRepr> operandTypeReprs = operandTypeReprs(operandType);
    final int bitwidth = operandTypeBitwidth(operandType);
    assertAllSameIrType(operandType.baseTypes());
    return OperandUtil.ofSize(
        operandTypeReprs.isEmpty() ? preferredUnitType : comparisonResultType(operandTypeReprs),
        bitwidth);
  }

  /**
   * Creates an operand from an operand type, consisting exclusively of zero.
   *
   * @param uncheckedOperandType Operand type to create operand for.
   * @param tmpName TmpName to give zero operands.
   * @return Newly created zero operand.
   */
  public static Operand zeroOperand(final OperandType uncheckedOperandType, final TmpName tmpName) {
    if (uncheckedOperandType instanceof OperandType.TupleType operandType) {
      return new Operand.Tuple(
          operandType.componentTypes().stream().map(o -> zeroOperand(o, tmpName)).toList());
    } else if (uncheckedOperandType instanceof OperandType.ArrayType operandType) {

      final Operand.Basic underlyingRepresentation =
          zeroOperand(typeAmalgam(operandType, Ir.Type.UNIT), tmpName);
      return new Operand.Array(underlyingRepresentation, operandType);

    } else {
      final OperandType.BasicType operandType = (OperandType.BasicType) uncheckedOperandType;
      return zeroOperand(operandType.type(), tmpName);
    }
  }

  /**
   * Creates an operand from an IR type, consisting exclusively of zero.
   *
   * @param irType Type to create operand for.
   * @param tmpName TmpName to give zero operand.
   * @return Newly created zero operand.
   */
  public static Operand.Constant zeroOperand(final Ir.Type irType, final TmpName tmpName) {
    return new Operand.Constant(0, irType, tmpName);
  }

  /**
   * Determines the bitwidth of the given operand.
   *
   * @param operandType Operand type to determine for.
   * @return Width of given type
   */
  public static int operandTypeBitwidth(OperandType operandType) {
    return operandType.baseTypes().stream().mapToInt(Ir.Type::width).sum();
  }

  private static void assertAllSameIrType(List<Ir.Type> componentTypes) {
    if (!isAllComponentsOfSameIrType(componentTypes)) {
      throw new InternalCompilerException(
          "All component types must be of the same type, but was: %s", componentTypes);
    }
  }

  private static boolean isAllComponentsOfSameIrType(List<Ir.Type> componentTypes) {
    if (componentTypes.isEmpty()) {
      return true;
    }
    final Class<? extends Ir.Type> typeOfFirst = componentTypes.get(0).getClass();
    return componentTypes.stream().map(Object::getClass).allMatch(x -> x == typeOfFirst);
  }

  static Ir.Type comparisonResultType(final Operand operand1) {
    return comparisonResultType(operandTypeReprs(operand1));
  }

  static Ir.Type comparisonResultType(final Set<TypeRepr> operandTypeReprs) {
    return operandTypeReprs.contains(TypeRepr.SecretBinary) ? Ir.Type.Sbit : Ir.Type.PublicBool;
  }

  /**
   * Analogous to {@link Ir.Type}, but indicating how the type is represented by the executing VM.
   */
  enum TypeRepr {
    /**
     * Analogous to {@link Ir.Type.PublicInteger}. Indicates that a variable is represented by
     * public integers during execution.
     */
    Public,
    /**
     * Analogous to {@link Ir.Type.SecretBinaryInteger}. Indicates that a variable is represented by
     * secret-shared binary integers during execution.
     */
    SecretBinary
  }

  /**
   * Determines the representations used to represent the operand at runtime.
   *
   * @param uncheckedOperand Operand to determine representations for.
   * @return Set of {@link TypeRepr}. Never null; but might be empty.
   * @see operandTypeReprs
   */
  static Set<TypeRepr> operandTypeReprs(final Operand uncheckedOperand) {
    return operandTypeReprs(uncheckedOperand.operandType());
  }

  /**
   * Determines the set of {@link TypeRepr} indicating which representations are used to represent
   * the given {@link OperandType}. Might be empty for certain corner-case, like empty tuples. If
   * size is larger than 1 it indicates a mixed representation, which is not fully supported.
   *
   * @param uncheckedOperandType Type to determine representations for.
   * @return Set of {@link TypeRepr}. Never null; but might be empty.
   */
  static Set<TypeRepr> operandTypeReprs(final OperandType uncheckedOperandType) {
    final Set<TypeRepr> outputAccum = new HashSet<>();
    operandTypeReprsInternal(uncheckedOperandType, outputAccum);
    return outputAccum;
  }

  private static void operandTypeReprsInternal(
      final OperandType uncheckedOperandType, final Set<TypeRepr> outputAccum) {
    // Array
    if (uncheckedOperandType instanceof OperandType.ArrayType operandType) {
      operandTypeReprsInternal(operandType.subtype(), outputAccum);

      // Basic
    } else if (uncheckedOperandType instanceof OperandType.BasicType operandType) {
      outputAccum.add(
          operandType.type() instanceof Ir.Type.SecretBinaryInteger
              ? TypeRepr.SecretBinary
              : TypeRepr.Public);

      // Tuple
    } else {
      final OperandType.TupleType operandType = (OperandType.TupleType) uncheckedOperandType;
      for (final OperandType subType : operandType.componentTypes()) {
        operandTypeReprsInternal(subType, outputAccum);
      }
    }
  }
}
