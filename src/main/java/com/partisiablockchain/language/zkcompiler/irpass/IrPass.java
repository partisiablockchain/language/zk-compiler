package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.TranslationConfig;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.DataFlowAnalysis;
import java.util.function.Function;

/** One part of the {@link Ir} transformation chain. */
public interface IrPass {

  /**
   * Entry-point for transforming the circuit.
   *
   * @param originalCircuit Previous circuit generated in the IR chain.
   * @param config Compilation configuration.
   * @return New IR circuit.
   */
  Ir.Circuit applyPass(final Ir.Circuit originalCircuit, final TranslationConfig config);

  /**
   * Creates a data-flow graph from some blocks.
   *
   * @param blocks Blocks to create data-flow graph from.
   * @return Data-flow graph from the given blocks.
   */
  static DataFlowAnalysis.Graph<Ir.BlockId> dataFlowGraphFromBlocks(
      final Iterable<Ir.Block> blocks) {
    final DataFlowAnalysis.Graph<Ir.BlockId> graph = DataFlowAnalysis.Graph.empty();
    for (final Ir.Block block : blocks) {

      // Branch always
      if (block.terminator() instanceof Ir.BranchAlways terminator) {
        graph.addEdge(block.blockId(), terminator.branch().targetBlock());

        // Branch if
      } else {
        final Ir.BranchIf terminator = (Ir.BranchIf) block.terminator();
        graph.addEdge(block.blockId(), terminator.branchTrue().targetBlock());
        graph.addEdge(block.blockId(), terminator.branchFalse().targetBlock());
      }
    }
    return graph;
  }

  /**
   * Returns the name of the IR Pass.
   *
   * @return name of the IR Pass.
   */
  String getName();

  /**
   * Transforms the given circuit by applying the given function to each of the {@link Ir.Block}s.
   *
   * @param originalCircuit Circuit to modify.
   * @param blockMapper Function to modify blocks by.
   * @return Newly modified circuit.
   */
  static Ir.Circuit mapBlocks(
      final Ir.Circuit originalCircuit, final Function<Ir.Block, Ir.Block> blockMapper) {
    return new Ir.Circuit(
        originalCircuit.functions().stream().map(f -> mapBlocks(f, blockMapper)).toList());
  }

  /**
   * Transforms the given {@link Ir.Function} by applying the given function to each of the {@link
   * Ir.Block}s.
   *
   * @param originalFunction {@link Ir.Function} to modify.
   * @param blockMapper Function to modify blocks by.
   * @return Newly modified {@link Ir.Function}.
   */
  static Ir.Function mapBlocks(
      final Ir.Function originalFunction, final Function<Ir.Block, Ir.Block> blockMapper) {
    return originalFunction.replaceBlocks(
        originalFunction.blocks().stream().map(blockMapper).toList());
  }
}
