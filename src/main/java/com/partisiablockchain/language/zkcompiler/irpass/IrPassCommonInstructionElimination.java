package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.TranslationConfig;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.AliasMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Intrablock IR pass to apply copy propagation, and some common subexpression elimination.
 *
 * <p>Copy propagation rewrites usages of identity casts.
 *
 * <p>Common subexpression elimination rewrites instructions to point to identical operations that
 * occurred previous in the same block.
 *
 * <p>This pass may make certain instructions redudant, and a call to {@link
 * IrPassDeadCodeElimination} is required to remove any of these redundant operations.
 */
public final class IrPassCommonInstructionElimination implements IrPass {

  @Override
  public String getName() {
    return "ir-common-instruction-elim";
  }

  @Override
  public Ir.Circuit applyPass(final Ir.Circuit originalCircuit, final TranslationConfig config) {
    final AnalysisSideEffects.Result sideEffectsResult =
        AnalysisSideEffects.analyseFunctionSideEffects(originalCircuit);

    return IrPass.mapBlocks(originalCircuit, b -> applyPassToBlock(b, sideEffectsResult));
  }

  private static Ir.Block applyPassToBlock(
      final Ir.Block block, final AnalysisSideEffects.Result sideEffectsResult) {

    // Determine types of all variables
    final Map<Ir.VariableId, Ir.Type> variableTypes =
        block.instructions().stream()
            .collect(Collectors.toMap(Ir.Instruction::resultId, Ir.Instruction::resultType));
    variableTypes.putAll(
        block.inputs().stream()
            .collect(
                Collectors.toUnmodifiableMap(Ir.Input::assignedVariable, Ir.Input::variableType)));

    // Initialize iteration state

    // previouslySeenOperations stores the common operations themselves for
    // lookup. Keys must never contain an operation with {@link AnalysisSideEffects#hasSideEffect},
    // as these operations does not count as common.
    final LinkedHashMap<Ir.Operation, Ir.VariableId> previouslySeenOperations =
        new LinkedHashMap<Ir.Operation, Ir.VariableId>(block.instructions().size());

    // aliasMap stores aliases of variables. This allows us to skip the
    // aliasing operation entirely.
    final AliasMap<Ir.VariableId> aliasMap =
        new AliasMap<Ir.VariableId>(
            block.inputs().stream()
                .map(Ir.Input::assignedVariable)
                .collect(Collectors.toMap(Function.identity(), Function.identity())));

    // Initialize with instructions
    final List<Ir.Instruction> instructions =
        block.instructions().stream()
            .map(
                insn ->
                    optimizeInstruction(
                        insn, variableTypes, aliasMap, previouslySeenOperations, sideEffectsResult))
            .filter(Objects::nonNull)
            .toList();

    return new Ir.Block(
        block.blockId(),
        block.inputs(),
        instructions,
        replaceInTerminator(block.terminator(), aliasMap),
        block.isEntryPoint());
  }

  /**
   * Optimize individual instruction, replacing aliases and common operations with the most original
   * variable, and removing aliasing instructions.
   *
   * <p>If the replaced operation already exists, replace it with an identity cast.
   *
   * @param insn Instruction to optimize.
   * @param variableTypes Map from variable ids to their type.
   * @param aliasMap Alias map. Method may modify it.
   * @param previouslySeenOperations Alias map. Method may modify it.
   * @param sideEffectsResult Side-effect analysis result, indicating which functions calls can be
   *     removed or not.
   * @return Null if the instruction should be removed.
   */
  private static Ir.Instruction optimizeInstruction(
      final Ir.Instruction insn,
      final Map<Ir.VariableId, Ir.Type> variableTypes,
      final AliasMap<Ir.VariableId> aliasMap,
      final Map<Ir.Operation, Ir.VariableId> previouslySeenOperations,
      final AnalysisSideEffects.Result sideEffectsResult) {

    // Firstly we optimize usages in the operation, possibly
    // producing a different operation.
    final Ir.Operation replacedOperation =
        replaceInOperation(insn.resultType(), insn.operation(), aliasMap);

    // Determine whether a similar operation exists
    // earlier in this block.
    final Ir.VariableId idOfPreviousIdenticalOperation =
        previouslySeenOperations.get(replacedOperation);

    // If the replaced operation is an identity-cast, update the
    // alias map.
    final Ir.VariableId aliasFor =
        OperationUtility.isIdentityOperation(replacedOperation, variableTypes);
    if (aliasFor != null) {
      aliasMap.put(insn.resultId(), aliasMap.get(aliasFor));
      return null;
    }

    // If the replaced operation already exists, replace it with
    // an identity cast.
    if (idOfPreviousIdenticalOperation != null) {
      aliasMap.put(insn.resultId(), idOfPreviousIdenticalOperation);
      return null;
    }

    // If the replaced operation does not already exist in the
    // block: Emit and record for further replacements.
    if (!sideEffectsResult.hasSideEffect(replacedOperation)) {
      previouslySeenOperations.put(replacedOperation, insn.resultId());
    }
    return new Ir.Instruction(insn.resultId(), insn.resultType(), replacedOperation);
  }

  static Ir.Instruction replaceInInstruction(
      final Ir.Instruction insn, final AliasMap<Ir.VariableId> aliasMap) {
    return new Ir.Instruction(
        insn.resultId(),
        insn.resultType(),
        replaceInOperation(insn.resultType(), insn.operation(), aliasMap));
  }

  /**
   * Replaces variables used in the given operation using the given aliasMap.
   *
   * <p>Invariants:
   *
   * <ul>
   *   <li>Preserves semantics.
   *   <li>Notably, it will preserve side-effects.
   * </ul>
   *
   * @param resultType The expected result type of the operation.
   * @param uncheckedOperation Operation to replace in.
   * @param aliasMap Map to use to determine aliases of variables. Variable ids that should not be
   *     rewritten is expected to map to itself.
   * @return New operation with variables replaced.
   */
  private static Ir.Operation replaceInOperation(
      final Ir.Type resultType,
      final Ir.Operation uncheckedOperation,
      final AliasMap<Ir.VariableId> aliasMap) {

    if (uncheckedOperation instanceof Ir.Operation.Constant operation) {
      return operation;
    } else if (uncheckedOperation instanceof Ir.Operation.Nullary operation) {
      return operation;
    } else if (uncheckedOperation instanceof Ir.Operation.Unary operation) {
      return new Ir.Operation.Unary(operation.operation(), aliasMap.get(operation.varIdx1()));
    } else if (uncheckedOperation instanceof Ir.Operation.PublicToSecret operation) {
      return new Ir.Operation.PublicToSecret(operation.type(), aliasMap.get(operation.varIdx1()));
    } else if (uncheckedOperation instanceof Ir.Operation.Extract operation) {
      return new Ir.Operation.Extract(
          aliasMap.get(operation.varToExtractFrom()),
          operation.typeToExtract(),
          operation.bitOffsetFromLow());
    } else if (uncheckedOperation instanceof Ir.Operation.ExtractDyn operation) {
      return new Ir.Operation.ExtractDyn(
          aliasMap.get(operation.varToExtractFrom()),
          operation.typeToExtract(),
          aliasMap.get(operation.varOffsetFromLow()));
    } else if (uncheckedOperation instanceof Ir.Operation.InsertDyn operation) {
      return new Ir.Operation.InsertDyn(
          aliasMap.get(operation.varTarget()),
          aliasMap.get(operation.varToInsert()),
          aliasMap.get(operation.varOffsetIntoTarget()));
    } else if (uncheckedOperation instanceof Ir.Operation.Binary operation) {
      return OperationUtility.createBinary(
          resultType,
          operation.operation(),
          aliasMap.get(operation.varIdx1()),
          aliasMap.get(operation.varIdx2()));
    } else if (uncheckedOperation instanceof Ir.Operation.CallDirectly operation) {
      return new Ir.Operation.CallDirectly(
          operation.functionId(),
          operation.functionArguments().stream().map(aliasMap::get).toList(),
          operation.numReturnValues());
    } else {
      final Ir.Operation.Select operation = (Ir.Operation.Select) uncheckedOperation;
      return OperationUtility.createSelect(
          resultType,
          aliasMap.get(operation.varCondition()),
          aliasMap.get(operation.varIfTrue()),
          aliasMap.get(operation.varIfFalse()));
    }
  }

  /**
   * Replaces variables used in the given terminator using the given aliasMap.
   *
   * @param uncheckedTerminator Terminator to replace in.
   * @param aliasMap Map to use to determine aliases of variables. Variable ids that should not be
   *     rewritten is expected to map to itself.
   * @return New terminator with variables replaced.
   */
  static Ir.Terminator replaceInTerminator(
      final Ir.Terminator uncheckedTerminator, final AliasMap<Ir.VariableId> aliasMap) {
    // Branch always
    if (uncheckedTerminator instanceof Ir.BranchAlways terminator) {
      return new Ir.BranchAlways(replaceInBranchInfo(terminator.branch(), aliasMap));

      // Call and branch to
    } else if (uncheckedTerminator instanceof Ir.CallAndBranch terminator) {
      return new Ir.CallAndBranch(
          terminator.functionId(),
          replaceInList(terminator.functionArguments(), aliasMap),
          replaceInBranchInfo(terminator.branchToAfterReturn(), aliasMap),
          terminator.numReturnValues());

      // Branch if
    } else {
      final Ir.BranchIf terminator = (Ir.BranchIf) uncheckedTerminator;
      return OperationUtility.createBranchIf(
          terminator.isBranchOnSecret(),
          aliasMap.get(terminator.conditionVariable()),
          replaceInBranchInfo(terminator.branchTrue(), aliasMap),
          replaceInBranchInfo(terminator.branchFalse(), aliasMap));
    }
  }

  /**
   * Replaces variables used in the given branch using the given aliasMap.
   *
   * @param branchInfo Branch to replace in.
   * @param aliasMap Map to use to determine aliases of variables. Variable ids that should not be
   *     rewritten is expected to map to itself.
   * @return New branch info with variables replaced.
   */
  private static Ir.BranchInfo replaceInBranchInfo(
      final Ir.BranchInfo branchInfo, final AliasMap<Ir.VariableId> aliasMap) {
    return new Ir.BranchInfo(
        branchInfo.targetBlock(), replaceInList(branchInfo.branchInputVariables(), aliasMap));
  }

  private static List<Ir.VariableId> replaceInList(
      final List<Ir.VariableId> ls, final AliasMap<Ir.VariableId> aliasMap) {
    return ls.stream().map(aliasMap::get).toList();
  }
}
