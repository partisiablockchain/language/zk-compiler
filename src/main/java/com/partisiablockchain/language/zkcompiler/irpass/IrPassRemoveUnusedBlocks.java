package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.DataFlowAnalysis;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/** IR phase that removes unreachable blocks in the IR-circuit. */
public final class IrPassRemoveUnusedBlocks extends IrPassIntraFunction {

  @Override
  protected Ir.Function applyPassToFunction(
      final Ir.Function originalFunction, final IdGenerator<Ir.VariableId> idGenerator) {
    final Set<Ir.BlockId> entryPoints =
        originalFunction.blocks().stream()
            .filter(Ir.Block::isEntryPoint)
            .map(Ir.Block::blockId)
            .collect(Collectors.toUnmodifiableSet());
    final DataFlowAnalysis.Graph<Ir.BlockId> graph =
        IrPass.dataFlowGraphFromBlocks(originalFunction.blocks());

    final Map<Ir.BlockId, Boolean> reachabilityResult =
        DataFlowAnalysis.forwardAnalysis(
            graph,
            entryPoints::contains,
            (blockId, prevReach) -> isBlockReachable(blockId, prevReach, entryPoints));

    return originalFunction.replaceBlocks(
        originalFunction.blocks().stream()
            .filter(block -> reachabilityResult.get(block.blockId()))
            .toList());
  }

  @Override
  public String getName() {
    return "ir-remove-unused-blocks";
  }

  /**
   * Lattice fixpoint function for determining reachability of a block by inspecting whether the
   * previous blocks are themselves reachable.
   *
   * <p>A block is reachable, only if it is at the initial block, or if any of it's previous blocks
   * are reachable.
   *
   * <p>This operation is monotone over a boolean lattice.
   *
   * @param node The block to determine reachability for.
   * @param previousBlocksReachability Map containing information on previous blocks, and whether
   *     they are reachable from any entry points.
   * @param entryPoints The set of entry point block ids.
   * @return true if the block is reachable.
   */
  private static Boolean isBlockReachable(
      final Ir.BlockId node,
      final Map<Ir.BlockId, Boolean> previousBlocksReachability,
      final Set<Ir.BlockId> entryPoints) {
    return entryPoints.contains(node) || previousBlocksReachability.containsValue(true);
  }
}
