package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.AliasMap;
import com.partisiablockchain.language.zkcompiler.math.DataFlowAnalysis;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * IR phase that transforms branches over secret variables into selects over secret variables.
 *
 * <p>This is done with a fixpoint algorithm transforming individual diamond structured (a source
 * block, with branches and eventual collection into a convergence block), selecting over the input
 * variables of the convergence block.
 *
 * <p>As an example, imagine a meta-circuit with the following structure: A to B, B to T, A to C, C
 * to D, D to T, where A branches on a secret.
 *
 * <p>This meta-circuit can be transformed into: A, B, C, D, select results by secret from A, then a
 * jump to T.
 */
public final class IrPassMergeSecretBranches extends IrPassIntraFunction {

  @Override
  public String getName() {
    return "ir-merge-secret-branch";
  }

  @Override
  protected Ir.Function applyPassToFunction(
      final Ir.Function originalFunction, final IdGenerator<Ir.VariableId> idGenerator) {
    // Produce Temp-id generator, and determine flow graph.
    final Map<Ir.BlockId, Ir.Block> originalFunctionBlockMap = originalFunction.toBlockMap();

    final DataFlowAnalysis.Graph<Ir.BlockId> graph =
        IrPass.dataFlowGraphFromBlocks(originalFunction.blocks());

    // Stage 1: Fixpoint-analysis over blocks
    final Map<Ir.BlockId, Ir.Block> newBlockMap =
        DataFlowAnalysis.<Ir.BlockId, Ir.Block>backwardAnalysisGlobalProperty(
            graph,
            // originalFunctionBlockMap::get,
            blockId ->
                blockId.equals(Ir.BlockId.RETURN) ? null : originalFunctionBlockMap.get(blockId),
            (blockId, currentResultForAllNodes) -> {
              if (blockId.equals(Ir.BlockId.RETURN)) {
                return null;
              }

              final Ir.Block block = currentResultForAllNodes.get(blockId);
              final Ir.Block mergedBlock =
                  tryMergeBlock(
                      block,
                      currentResultForAllNodes,
                      idGenerator,
                      originalFunction.outputVariableTypes());
              return mergedBlock != null ? mergedBlock : block;
            });

    // Stage 2: Replace blocks if relevant
    final List<Ir.Block> newBlocks =
        originalFunction.blocks().stream().map(Ir.Block::blockId).map(newBlockMap::get).toList();
    return originalFunction.replaceBlocks(newBlocks);
  }

  /**
   * Merges blocks in diamond control flow, treating the given block as the condition block.
   *
   * @param cndBlock Block to treat as condition block of the diamond shaped control flow.
   * @param blockMap Map of ids to blocks.
   * @param idGenerator Id generator for creating new temporary variables.
   * @param outputVariableTypes Types require for returning.
   * @return New merged block, or null if this block couldn't be merged.
   */
  private static Ir.Block tryMergeBlock(
      final Ir.Block cndBlock,
      final Map<Ir.BlockId, Ir.Block> blockMap,
      final IdGenerator<Ir.VariableId> idGenerator,
      final List<Ir.Type> outputVariableTypes) {

    // Block merging is only relevant for blocks with BranchIf over secret
    // types.
    final Ir.BranchIf terminator = cndBlock.terminator() instanceof Ir.BranchIf term ? term : null;
    if (terminator == null || !terminator.isBranchOnSecret()) {
      return null;
    }

    // Attempt to determine convergence point
    List<Ir.BranchInfo> thenPath = branchAlwaysPath(terminator.branchTrue(), blockMap);
    List<Ir.BranchInfo> elsePath = branchAlwaysPath(terminator.branchFalse(), blockMap);
    final int convergesAtIndexFromEnd = indexFromEndWherePathsDiverge(thenPath, elsePath);

    // If there is no convergense point, for example if one path branches, we
    // need to wait for a later round to attempt again.
    if (convergesAtIndexFromEnd == 0) {
      return null;
    }

    // Determine precise paths and their convergence variables.
    final Ir.BranchInfo convergentBranchFromThen =
        thenPath.get(thenPath.size() - convergesAtIndexFromEnd);
    final Ir.BranchInfo convergentBranchFromElse =
        elsePath.get(elsePath.size() - convergesAtIndexFromEnd);
    thenPath = thenPath.subList(0, thenPath.size() - convergesAtIndexFromEnd);
    elsePath = elsePath.subList(0, elsePath.size() - convergesAtIndexFromEnd);

    // Create a merged block by combining all blocks on the path's
    // instrunctions.
    final var mergedInstructions = new ArrayList<Ir.Instruction>();
    mergedInstructions.addAll(cndBlock.instructions());
    for (final Ir.BranchInfo branchInfo : thenPath) {
      final Ir.Block branchBlock = blockMap.get(branchInfo.targetBlock());
      IrPassMergeLinearBlocks.assignInputs(
          mergedInstructions, branchBlock.inputs(), branchInfo.branchInputVariables());
      mergedInstructions.addAll(branchBlock.instructions());
    }
    for (final Ir.BranchInfo branchInfo : elsePath) {
      final Ir.Block branchBlock = blockMap.get(branchInfo.targetBlock());
      IrPassMergeLinearBlocks.assignInputs(
          mergedInstructions, branchBlock.inputs(), branchInfo.branchInputVariables());
      mergedInstructions.addAll(branchBlock.instructions());
    }

    // Produce sequence of instructions to select over arguments for
    // convergence block.
    final List<Ir.Type> typesOfInputsAfter =
        inputTypesForBlock(convergentBranchFromThen.targetBlock(), blockMap, outputVariableTypes);

    final List<Ir.VariableId> mergedArguments =
        mergeArguments(
            mergedInstructions,
            idGenerator,
            terminator.conditionVariable(),
            typesOfInputsAfter,
            convergentBranchFromThen.branchInputVariables(),
            convergentBranchFromElse.branchInputVariables(),
            IrPassMergeLinearBlocks.aliasMapFrom(mergedInstructions, cndBlock.inputs()));

    // Finally create merged terminator
    final Ir.Terminator mergedTerminator =
        new Ir.BranchAlways(
            new Ir.BranchInfo(convergentBranchFromThen.targetBlock(), mergedArguments));

    return new Ir.Block(
        cndBlock.blockId(),
        cndBlock.inputs(),
        List.copyOf(mergedInstructions),
        mergedTerminator,
        cndBlock.isEntryPoint());
  }

  private static List<Ir.VariableId> mergeArguments(
      final List<Ir.Instruction> insnAccum,
      final IdGenerator<Ir.VariableId> idGenerator,
      final Ir.VariableId condVariable,
      final List<Ir.Type> typeDefs,
      final List<Ir.VariableId> valuesThen,
      final List<Ir.VariableId> valuesElse,
      final AliasMap<Ir.VariableId> aliasMap) {
    final var newVariableIds = new ArrayList<Ir.VariableId>();
    final int maxValuesIndex = Integer.min(valuesThen.size(), valuesElse.size());
    for (int argIdx = 0; argIdx < maxValuesIndex; argIdx++) {

      final Ir.VariableId varThen = aliasMap.get(valuesThen.get(argIdx));
      final Ir.VariableId varElse = aliasMap.get(valuesElse.get(argIdx));

      final Ir.Type resultType = typeDefs.get(argIdx);

      if (varThen.equals(varElse)) {
        newVariableIds.add(varThen);
      } else {

        final Ir.VariableId varResult = idGenerator.newId(varThen.name());
        newVariableIds.add(varResult);

        insnAccum.add(
            new Ir.Instruction(
                varResult, resultType, new Ir.Operation.Select(condVariable, varThen, varElse)));
      }
    }
    return List.copyOf(newVariableIds);
  }

  private static List<Ir.Type> inputTypesForBlock(
      final Ir.BlockId blockId,
      final Map<Ir.BlockId, Ir.Block> blockMap,
      final List<Ir.Type> outputVariableTypes) {
    if (blockId.equals(Ir.BlockId.RETURN)) {
      return outputVariableTypes;
    }
    return blockMap.get(blockId).inputs().stream().map(Ir.Input::variableType).toList();
  }

  private static List<Ir.BranchInfo> branchAlwaysPath(
      Ir.BranchInfo branchInfo, final Map<Ir.BlockId, Ir.Block> blockMap) {
    final var paths = new ArrayList<Ir.BranchInfo>();
    paths.add(branchInfo);
    while (!branchInfo.targetBlock().equals(Ir.BlockId.RETURN)
        && blockMap.get(branchInfo.targetBlock()).terminator()
            instanceof Ir.BranchAlways terminator) {
      branchInfo = terminator.branch();
      paths.add(branchInfo);
    }

    return paths;
  }

  private static int indexFromEndWherePathsDiverge(
      final List<Ir.BranchInfo> path1, final List<Ir.BranchInfo> path2) {
    final int shortestPathLength = Math.min(path1.size(), path2.size());
    for (int idxFromEnd = 0; idxFromEnd < shortestPathLength; idxFromEnd++) {
      if (!path1
          .get(path1.size() - idxFromEnd - 1)
          .targetBlock()
          .equals(path2.get(path2.size() - idxFromEnd - 1).targetBlock())) {
        return idxFromEnd;
      }
    }
    return shortestPathLength;
  }
}
