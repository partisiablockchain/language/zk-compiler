package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.Mapping;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * IR Phase that changes jump targets from so-called "proxy blocks" (blocks with no instructions and
 * a single jump target.) to the proxy's own target, effectively making the proxy block unused.
 * These unused blocks are left in, to be removed by {@link IrPassRemoveUnusedBlocks}.
 */
public final class IrPassShortCircuitProxyBlocks extends IrPassIntraFunction {

  @Override
  protected Ir.Function applyPassToFunction(
      final Ir.Function originalFunction, final IdGenerator<Ir.VariableId> idGenerator) {
    // Analysis for how to collapse consecutive proxy-blocks and find the correct argument order
    // permutation
    final Map<Ir.BlockId, Ir.Block> blockMap = originalFunction.toBlockMap();
    final Map<Ir.BlockId, RedirectInfo> redirectMap =
        blockMap.keySet().stream()
            .collect(
                Collectors.toUnmodifiableMap(
                    Function.identity(),
                    blockId -> determineRedirectInfoForBlock(blockId, blockMap)));

    // Rewrite blocks to skip proxy blocks
    final List<Ir.Block> newBlocks =
        originalFunction.blocks().stream()
            .map(
                block ->
                    new Ir.Block(
                        block.blockId(),
                        block.inputs(),
                        block.instructions(),
                        replaceTerminator(block.terminator(), redirectMap),
                        block.isEntryPoint()))
            .toList();

    return originalFunction.replaceBlocks(newBlocks);
  }

  @Override
  public String getName() {
    return "ir-short-circuit";
  }

  /**
   * Determines which block a jump to the given blockId should actually redirect to. Might return
   * redirect info to itself.
   *
   * @param blockId Id of block to determine redirect for.
   * @param blockMap Map from block ids to the blocks themselves.
   * @return Information on which block the given block actually redirects to, and how to permute
   *     the inputs.
   */
  private static RedirectInfo determineRedirectInfoForBlock(
      Ir.BlockId blockId, final Map<Ir.BlockId, Ir.Block> blockMap) {

    // Initial
    Mapping inputMapping = Mapping.identity(blockMap.get(blockId).inputs().size());

    // Continue along branch-always path
    while (((Object) blockMap.get(blockId)) instanceof Ir.Block block
        && block.instructions().isEmpty()
        && block.terminator() instanceof Ir.BranchAlways terminator) {

      // Mapping is based upon how variables are routed through the block.
      final List<Ir.VariableId> blockInputVariableOrder =
          block.inputs().stream().map(Ir.Input::assignedVariable).toList();
      final Mapping thisBlockInputMapping =
          new Mapping(
              terminator.branch().branchInputVariables().stream()
                  .map(blockInputVariableOrder::indexOf)
                  .toList());

      // Update block and permutation
      blockId = terminator.branch().targetBlock();
      inputMapping = inputMapping.andThen(thisBlockInputMapping);
    }
    return new RedirectInfo(blockId, inputMapping);
  }

  /** Information on how blocks are proxied, and and how to permute inputs. */
  private record RedirectInfo(Ir.BlockId blockProxiedTo, Mapping inputOrderMapping) {

    public RedirectInfo {
      requireNonNull(blockProxiedTo);
      requireNonNull(inputOrderMapping);
    }
  }

  private static Ir.Terminator replaceTerminator(
      final Ir.Terminator uncheckedTerminator, final Map<Ir.BlockId, RedirectInfo> redirectInfo) {
    // Branch always
    if (uncheckedTerminator instanceof Ir.BranchAlways terminator) {
      return new Ir.BranchAlways(replaceBranchInfo(terminator.branch(), redirectInfo));

      // Branch if
    } else {
      final Ir.BranchIf terminator = (Ir.BranchIf) uncheckedTerminator;
      return OperationUtility.createBranchIf(
          terminator.isBranchOnSecret(),
          terminator.conditionVariable(),
          replaceBranchInfo(terminator.branchTrue(), redirectInfo),
          replaceBranchInfo(terminator.branchFalse(), redirectInfo));
    }
  }

  private static Ir.BranchInfo replaceBranchInfo(
      final Ir.BranchInfo branchInfo, final Map<Ir.BlockId, RedirectInfo> redirectInfo) {
    final RedirectInfo newBlockInfo = redirectInfo.getOrDefault(branchInfo.targetBlock(), null);
    if (newBlockInfo == null) {
      return branchInfo;
    }

    return new Ir.BranchInfo(
        newBlockInfo.blockProxiedTo(),
        newBlockInfo.inputOrderMapping().apply(branchInfo.branchInputVariables()));
  }
}
