package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.math.BigInteger;
import java.util.Map;
import java.util.Objects;

/** Evaluator for {@link SymbolicValue} expressions. */
public final class SymbolicValueEvaluator<SymbolicT extends SymbolicValue<SymbolicT>> {

  /** The top element of the given {@link SymbolicValue} type. */
  private final SymbolicT topSymbolicElement;

  /** Constructor for producing new elements of {@link SymbolicValue} from a constant value. */
  private final ConstantConstructor<SymbolicT> constantConstructor;

  /**
   * Functional interface for a constant constructor taking the constant value and a bitwidth.
   *
   * @param <SymbolicT> Type of produced value.
   */
  @FunctionalInterface
  interface ConstantConstructor<SymbolicT> {

    /**
     * Create new constant.
     *
     * @param value Value of constant.
     * @param bitwidth Bitwidth of constant.
     * @return Newly created constant.
     */
    SymbolicT newConstant(BigInteger value, int bitwidth);
  }

  /**
   * Constructor for symbolic value evaluator.
   *
   * @param topSymbolicElement Top element of the symbolic value lattice.
   * @param constantConstructor Constructor for constant elements of the lattice.
   */
  public SymbolicValueEvaluator(
      final SymbolicT topSymbolicElement,
      final ConstantConstructor<SymbolicT> constantConstructor) {
    this.topSymbolicElement = Objects.requireNonNull(topSymbolicElement);
    this.constantConstructor = Objects.requireNonNull(constantConstructor);
  }

  /**
   * Evaluate the given instruction in the given environment.
   *
   * @param instruction Instruction to evaluate.
   * @param environment Environment to evaluate in.
   * @return Symbolically evaluated environment.
   */
  public SymbolicT evaluateInstruction(
      final Ir.Instruction instruction, final Map<Ir.VariableId, SymbolicT> environment) {
    return fitInType(
        evaluateOperation(instruction.operation(), environment), instruction.resultType());
  }

  SymbolicT evaluateOperation(
      final Ir.Operation uncheckedOperation, final Map<Ir.VariableId, SymbolicT> environment) {
    Objects.requireNonNull(uncheckedOperation);
    Objects.requireNonNull(environment);

    // Constant
    if (uncheckedOperation instanceof Ir.Operation.Constant operation) {
      return constantConstructor.newConstant(operation.const1(), operation.type().width());

      // Binop
    } else if (uncheckedOperation instanceof Ir.Operation.Binary operation) {
      return evaluateBinop(
          operation.operation(),
          environment.get(operation.varIdx1()),
          environment.get(operation.varIdx2()));

      // Nullary
    } else if (uncheckedOperation instanceof Ir.Operation.Nullary operation) {
      return evaluateNullary(operation.operation(), environment);

      // Cast
    } else if (uncheckedOperation instanceof Ir.Operation.PublicToSecret operation) {
      return environment.get(operation.varIdx1());

      // Unop
    } else if (uncheckedOperation instanceof Ir.Operation.Unary operation) {
      return evaluateUnop(operation.operation(), environment.get(operation.varIdx1()), environment);

      // Extract
    } else if (uncheckedOperation instanceof Ir.Operation.Extract operation) {
      final SymbolicT offset =
          constantConstructor.newConstant(BigInteger.valueOf(operation.bitOffsetFromLow()), 32);
      return environment
          .get(operation.varToExtractFrom())
          .extract(operation.typeToExtract().width(), offset);

      // ExtractDyn
    } else if (uncheckedOperation instanceof Ir.Operation.ExtractDyn operation) {
      final SymbolicT offset = environment.get(operation.varOffsetFromLow());
      return environment
          .get(operation.varToExtractFrom())
          .extract(operation.typeToExtract().width(), offset);

      // InsertDyn
    } else if (uncheckedOperation instanceof Ir.Operation.InsertDyn operation) {
      return environment
          .get(operation.varTarget())
          .insert(
              environment.get(operation.varToInsert()),
              environment.get(operation.varOffsetIntoTarget()));

      // Call
    } else if (uncheckedOperation instanceof Ir.Operation.CallDirectly operation) {
      // Not a lot of optimizations to do for arbitrary function calls.
      return topSymbolicElement;

      // Select
    } else {
      final Ir.Operation.Select operation = (Ir.Operation.Select) uncheckedOperation;
      return environment
          .get(operation.varCondition())
          .selectBetween(
              environment.get(operation.varIfTrue()), environment.get(operation.varIfFalse()));
    }
  }

  SymbolicT evaluateNullary(
      final Ir.Operation.NullaryOp operation, final Map<Ir.VariableId, SymbolicT> environment) {
    // Unknown (safe default to top)
    return topSymbolicElement;
  }

  SymbolicT evaluateBinop(
      final Ir.Operation.BinaryOp operation, final SymbolicT value1, final SymbolicT value2) {

    // Bitwise binop
    if (operation.equals(Ir.Operation.BinaryOp.BITWISE_AND)) {
      return value1.bitwiseAnd(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.BITWISE_OR)) {
      return value1.bitwiseOr(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.BITWISE_XOR)) {
      return value1.bitwiseXor(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL)) {
      return value1.shiftRight(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL)) {
      return value1.shiftLeft(value2);

      // Bit binop
    } else if (operation.equals(Ir.Operation.BinaryOp.BIT_CONCAT)) {
      return value1.concatWith(value2);

      // Arithmetic binop
    } else if (operation.equals(Ir.Operation.BinaryOp.ADD_WRAPPING)) {
      return value1.arithmeticAdd(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.MULT_SIGNED_WRAPPING)) {
      return value1.arithmeticMultiply(value2);

      // Relational operations
    } else if (operation.equals(Ir.Operation.BinaryOp.EQUAL)) {
      return value1.compareEquals(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.LESS_THAN_SIGNED)) {
      return value1.compareLessThanSigned(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.LESS_THAN_UNSIGNED)) {
      return value1.compareLessThanUnsigned(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED)) {
      return value1.compareLessThanOrEqualsSigned(value2);
    } else if (operation.equals(Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED)) {
      return value1.compareLessThanOrEqualsUnsigned(value2);

      // Unknown (safe default to top)
    } else {
      return topSymbolicElement;
    }
  }

  SymbolicT evaluateUnop(
      final Ir.Operation.UnaryOp operation,
      final SymbolicT value1,
      final Map<Ir.VariableId, SymbolicT> environment) {
    // Bitwise
    if (operation.equals(Ir.Operation.UnaryOp.BITWISE_NOT)) {
      return value1.bitwiseNot();

      // Arithmetic
    } else if (operation.equals(Ir.Operation.UnaryOp.NEGATE)) {
      return value1.arithmeticNegation();

      // Unknown (safe default to top)
    } else {
      return topSymbolicElement;
    }
  }

  private SymbolicT fitInType(final SymbolicT number, final Ir.Type type) {
    final BigInteger maxPlusOne = BigInteger.ONE.shiftLeft(type.width());
    return number.arithmeticModulo(maxPlusOne);
  }
}
