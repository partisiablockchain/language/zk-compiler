package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.util.List;
import java.util.stream.Stream;

/**
 * Utility for generating various types of ids.
 *
 * @param <T> Id type.
 */
public final class IdGenerator<T> {

  /**
   * Functional interface for generating new ids.
   *
   * @param <T> Type of identifier.
   */
  @FunctionalInterface
  public interface IdFactory<T> {

    /**
     * Generate a new identifier with the given {@code rawId} and {@code name}.
     *
     * @param rawId Internal id of the identifier.
     * @param name Human readable name.
     * @return Newly created identifier. Must never be null.
     */
    T newId(int rawId, String name);
  }

  /** Internal counter for generating unique identifiers. */
  private int identifierCounter;

  /** Factory for creating identifiers of the generators type. */
  private final IdFactory<T> factory;

  IdGenerator(final int identifierCounter, final IdFactory<T> factory) {
    this.identifierCounter = identifierCounter;
    this.factory = factory;
  }

  /**
   * Create a new {@link IdGenerator} for {@link Ir.VariableId}.
   *
   * @param initialId Id to start with.
   * @return Newly created {@link IdGenerator}.
   */
  public static IdGenerator<Ir.VariableId> forVariableIds(final int initialId) {
    return new IdGenerator<>(initialId, (IdFactory<Ir.VariableId>) Ir.VariableId::new);
  }

  /**
   * Create a new {@link IdGenerator} for {@link Ir.BlockId}.
   *
   * @param initialId Id to start with.
   * @return Newly created {@link IdGenerator}.
   */
  public static IdGenerator<Ir.BlockId> forBlockIds(final int initialId) {
    return new IdGenerator<>(initialId, (IdFactory<Ir.BlockId>) Ir.BlockId::new);
  }

  /**
   * Create a new {@link IdGenerator} for {@link Ir.FunctionId}.
   *
   * @param initialId Id to start with.
   * @return Newly created {@link IdGenerator}.
   */
  public static IdGenerator<Ir.FunctionId> forFunctionIds(final int initialId) {
    return new IdGenerator<>(initialId, (IdFactory<Ir.FunctionId>) Ir.FunctionId::new);
  }

  /**
   * Produces a new id with the given name.
   *
   * @param name Name to give id.
   * @return Newly created unique id.
   */
  public T newId(final String name) {
    return factory.newId(identifierCounter++, name);
  }

  /**
   * Create new {@link Ir.BlockId} generator, guarenteed to produce ids that does not occur in the
   * given function.
   *
   * @param function Function to determine already used ids from.
   * @return Newly created id generator.
   */
  public static IdGenerator<Ir.BlockId> uniqueBlockIdGeneratorFor(final Ir.Function function) {
    final int largestIdInCircuit =
        function.blocks().stream()
            .map(Ir.Block::blockId)
            .map(Ir.BlockId::id)
            .max(java.util.Comparator.naturalOrder())
            .orElse(0);

    return forBlockIds(largestIdInCircuit + 100);
  }

  /**
   * Create new {@link Ir.VariableId} generator, guarenteed to produce ids that does not occur in
   * the given meta-circuit.
   *
   * @param irCircuit Circuit to determine already used ids from.
   * @return Newly created id generator.
   */
  public static IdGenerator<Ir.VariableId> uniqueVariableIdGeneratorFor(
      final Ir.Circuit irCircuit) {
    final int largestIdInCircuit =
        irCircuit.functions().stream()
            .map(Ir.Function::blocks)
            .flatMap(List::stream)
            .flatMap(IdGenerator::usedVariableIdsInBlock)
            .map(Ir.VariableId::id)
            .max(java.util.Comparator.naturalOrder())
            .orElse(0);

    return forVariableIds(largestIdInCircuit + 100);
  }

  /**
   * Determines list of variable ids used in block.
   *
   * @param block Block to determine used variables from.
   * @return Stream of used variables ids.
   */
  private static Stream<Ir.VariableId> usedVariableIdsInBlock(Ir.Block block) {
    return Stream.concat(
        block.inputs().stream().map(Ir.Input::assignedVariable),
        block.instructions().stream().map(Ir.Instruction::resultId));
  }
}
