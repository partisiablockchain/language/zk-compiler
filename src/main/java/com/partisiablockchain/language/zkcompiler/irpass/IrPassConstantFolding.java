package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.DataFlowAnalysis;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Interblock {@link IrPass} for performing constant folding over variables.
 *
 * <p>Constant folding is a partial compile-time evaluation of a program, such that simple
 * computations that are obvious at compile-time are reduced to integer constants.
 *
 * <p>A slightly contrived example: The program {@code (2 + 3) + x} can easily be constant folded to
 * {@code 5 + x}. Constant folding is useful for maintaining code readability without sacrifizing
 * performance.
 *
 * @see <a href="https://en.wikipedia.org/wiki/Constant_folding">Constant Folding, Wikipedia</a>
 */
public final class IrPassConstantFolding extends IrPassIntraFunction {

  @Override
  public String getName() {
    return "ir-constant-fold";
  }

  private static final SymbolicValueEvaluator<SymbolicValueSimple> evaluator =
      new SymbolicValueEvaluator<>(SymbolicValueSimple.TOP_VALUE, SymbolicValueSimple::constant);

  @Override
  protected Ir.Function applyPassToFunction(
      final Ir.Function originalFunction, final IdGenerator<Ir.VariableId> idGenerator) {

    // 1. Construct variable flow graph
    final DataFlowAnalysis.Graph<Ir.VariableId> graph =
        variableDataFlowGraphFromFunction(originalFunction);

    // 2. Find the instruction that computes each variable (except input variables which do not have
    // an instruction)
    final Map<Ir.VariableId, Ir.Instruction> instructionsByVariable =
        originalFunction.blocks().stream()
            .map(Ir.Block::instructions)
            .flatMap(List::stream)
            .collect(Collectors.toUnmodifiableMap(Ir.Instruction::resultId, Function.identity()));

    // 3. Determine symbolic value for each variable
    final Map<Ir.VariableId, SymbolicValueSimple> variableSymbolicValues =
        DataFlowAnalysis.forwardAnalysis(
            graph,
            variableId -> SymbolicValueSimple.BOTTOM_VALUE,
            (variableId, inputValues) ->
                computeConstantFolding(instructionsByVariable.get(variableId), inputValues));

    final Map<Ir.VariableId, BigInteger> variableConstantValues = new LinkedHashMap<>();
    for (final Map.Entry<Ir.VariableId, SymbolicValueSimple> symbolicValueMapping :
        variableSymbolicValues.entrySet()) {
      final SymbolicValueSimple value = symbolicValueMapping.getValue();
      if (value.value() != null) {
        variableConstantValues.put(symbolicValueMapping.getKey(), value.value());
      }
    }

    // 4. Replace variables with relevant ids.
    return replaceInFunction(originalFunction, idGenerator, variableConstantValues);
  }

  /**
   * Produces new interblock data flow graph from function
   *
   * <p>Edges are added from a variable to it's usages, including input parameters and branching
   * arguments, and the mapping between parameters and arguments.
   */
  private static DataFlowAnalysis.Graph<Ir.VariableId> variableDataFlowGraphFromFunction(
      final Ir.Function function) {

    // Block map for easy navigation
    final Map<Ir.BlockId, Ir.Block> blockMap = function.toBlockMap();

    // Initialize graph
    final DataFlowAnalysis.Graph<Ir.VariableId> graph = DataFlowAnalysis.Graph.empty();

    for (final Ir.Block block : function.blocks()) {

      // Instructions
      for (final Ir.Instruction insn : block.instructions()) {
        graph.addNode(insn.resultId());
        for (final Ir.VariableId usedVariable :
            IrPassDeadCodeElimination.usedVariableIds(insn.operation())) {
          graph.addEdge(usedVariable, insn.resultId());
        }
      }

      // Terminators
      if (block.terminator() instanceof Ir.BranchAlways terminator) {
        dataFlowFromBranchInfo(graph, terminator.branch(), blockMap);
      } else {
        final Ir.BranchIf terminator = (Ir.BranchIf) block.terminator();
        dataFlowFromBranchInfo(graph, terminator.branchTrue(), blockMap);
        dataFlowFromBranchInfo(graph, terminator.branchFalse(), blockMap);
      }
    }
    return graph;
  }

  private static void dataFlowFromBranchInfo(
      final DataFlowAnalysis.Graph<Ir.VariableId> graphAccum,
      final Ir.BranchInfo branchInfo,
      Map<Ir.BlockId, Ir.Block> blockMap) {
    if (branchInfo.targetBlock().equals(Ir.BlockId.RETURN)) {
      return;
    }

    final Ir.Block targetBlock = blockMap.get(branchInfo.targetBlock());
    for (int inputIdx = 0; inputIdx < branchInfo.branchInputVariables().size(); inputIdx++) {
      final var origin = branchInfo.branchInputVariables().get(inputIdx);
      final var target = targetBlock.inputs().get(inputIdx).assignedVariable();
      graphAccum.addEdge(origin, target);
    }
  }

  /**
   * Compute symbolic value that results from executing the node, whether that is an instruction or
   * a merge node.
   *
   * <p>Instruction nodes represent actual instructions. These nodes are evaluated over the symbolic
   * value.
   *
   * <p>Block input nodes are merge nodes. The runtime variable represented by these nodes could
   * assume any value represented by any of the predecessor nodes, thus the node needs to compute
   * the union of possibilities.
   *
   * @param insn Instruction to execute. If instruction is null, it indicates that variable is an
   *     block input merge node.
   * @param inputVariables Symbolic values for variables used in instruction / merge node.
   * @return Constant-folded symbolic value of executing the node.
   */
  private static SymbolicValueSimple computeConstantFolding(
      final Ir.Instruction insn, final Map<Ir.VariableId, SymbolicValueSimple> inputVariables) {
    // Instruction
    if (insn != null) {
      return evaluator.evaluateInstruction(insn, inputVariables);

      // Block input merge nodes
    } else {
      final boolean isEntryPoint = inputVariables.isEmpty();
      if (isEntryPoint) {
        return SymbolicValueSimple.TOP_VALUE;
      } else {
        return inputVariables.values().stream()
            .reduce(SymbolicValueSimple.BOTTOM_VALUE, SymbolicValueSimple::leastUpperBound);
      }
    }
  }

  /**
   * Replaces values in the given function, by loopup in the given map. Variables not in the map are
   * left alone.
   *
   * @param originalFunction Function to replace in.
   * @param variableConstantValues Map from variables to the constant to replace with.
   * @return Newly created function with replaced variables.
   */
  private static Ir.Function replaceInFunction(
      final Ir.Function originalFunction,
      final IdGenerator<Ir.VariableId> idGenerator,
      final Map<Ir.VariableId, BigInteger> variableConstantValues) {
    final List<Ir.Block> blocks =
        originalFunction.blocks().stream()
            .map(block -> replaceInBlock(block, variableConstantValues, idGenerator))
            .toList();

    return originalFunction.replaceBlocks(blocks);
  }

  private static Ir.Block replaceInBlock(
      final Ir.Block block,
      final Map<Ir.VariableId, BigInteger> variableConstantValues,
      final IdGenerator<Ir.VariableId> idGenerator) {

    // Construct inputs
    final ArrayList<Ir.Input> inputs = new ArrayList<>();
    final ArrayList<Ir.Instruction> insnsFromInputs = new ArrayList<>();
    for (final Ir.Input input : block.inputs()) {
      final BigInteger constantValue = variableConstantValues.get(input.assignedVariable());

      // Inputs variables with constant values are replaced by newly generated
      // variables, and assigned their constant value in the block body.
      if (constantValue != null) {
        final Ir.VariableId unusedVariable = idGenerator.newId("unused");
        inputs.add(new Ir.Input(unusedVariable, input.variableType()));
        insnsFromInputs.add(
            new Ir.Instruction(
                input.assignedVariable(),
                input.variableType(),
                new Ir.Operation.Constant(input.variableType(), constantValue)));

        // Input variables without constant values are left alone.
      } else {
        inputs.add(input);
      }
    }

    // Replace in instructions.
    final Stream<Ir.Instruction> insnsReplaced =
        block.instructions().stream()
            .map(
                insn -> {
                  final BigInteger constantValue = variableConstantValues.get(insn.resultId());
                  // Instructions that assign to constant variables have their
                  // operations replaced with constant.
                  if (constantValue != null) {
                    return new Ir.Instruction(
                        insn.resultId(),
                        insn.resultType(),
                        new Ir.Operation.Constant(insn.resultType(), constantValue));
                  }
                  return insn;
                });

    return new Ir.Block(
        block.blockId(),
        List.copyOf(inputs),
        Stream.concat(insnsFromInputs.stream(), insnsReplaced).toList(),
        replaceInTerminator(block.terminator(), variableConstantValues),
        block.isEntryPoint());
  }

  private static Ir.Terminator replaceInTerminator(
      final Ir.Terminator uncheckedTerminator,
      final Map<Ir.VariableId, BigInteger> variableConstantValues) {
    if (uncheckedTerminator instanceof Ir.BranchIf terminator) {
      return replaceInBranch(terminator, variableConstantValues);
    } else {
      return uncheckedTerminator;
    }
  }

  private static Ir.Terminator replaceInBranch(
      final Ir.BranchIf terminator, final Map<Ir.VariableId, BigInteger> variableConstantValues) {
    final BigInteger constantValue = variableConstantValues.get(terminator.conditionVariable());

    // No constant value is left unchanged
    if (constantValue == null) {
      return terminator;

      // Zero constant value is replaced
    } else if (constantValue.equals(BigInteger.ZERO)) {
      return new Ir.BranchAlways(terminator.branchFalse());

      // One constant value is replaced
    } else {
      return new Ir.BranchAlways(terminator.branchTrue());
    }
  }
}
