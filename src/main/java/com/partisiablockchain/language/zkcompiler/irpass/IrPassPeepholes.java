package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.TranslationConfig;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Intrablock IR phase performing simple per-instruction optimizations based on the instruction
 * semantics. This phase is limited to per-instruction replacements, and cannot produce
 * optimizations that result in additional instructions. The size of the IR is guarenteed to be
 * stable during this phase.
 *
 * <p>Following optimizations are performed:
 *
 * <ul>
 *   <li>{@link OperationUtility.BinopProperties#isIdempotent Idempotent binops} with identical
 *       arguments are optimized to identity operation.
 *   <li>Various other binops with identical arguments are optimized to small constants. For example
 *       {@code (sbiN $Y (bitwise_xor $X $X))} instruction can be replaced with {@code (sbiN $Y
 *       (constant sbiN 0))}.
 *   <li>Binops with an {@link OperationUtility.BinopProperties identity element} as an argument is
 *       optimized to identity operation.
 *   <li>Binops with an {@link OperationUtility.BinopProperties absorbing element} as an argument is
 *       optimized to a constant.
 *   <li>{@link OperationUtility.BinopProperties#isCommutative Commutative binops} have their
 *       arguments ordered by {@link Ir.VariableId}.
 *   <li>{@link Ir.BranchIf} with identical branches are optimized to {@link Ir.BranchAlways}.
 *   <li>{@link Ir.Operation.Select} with identical branches are optimized to identity operation.
 *   <li>{@link Ir.Operation.Select} with a negated condition is flipped to shorten critical path.
 *       For example {@code (select $B $X $Y)} where {@code B = (not $A)} can be replaced with
 *       {@code (select $A $Y $X)}.
 * </ul>
 *
 * @see <a href="https://en.wikipedia.org/wiki/Peephole_optimization">Peephole optimization,
 *     Wikipedia</a>
 */
public final class IrPassPeepholes implements IrPass {

  @Override
  public String getName() {
    return "ir-peephole-opt";
  }

  @Override
  public Ir.Circuit applyPass(final Ir.Circuit originalCircuit, final TranslationConfig config) {
    return IrPass.mapBlocks(originalCircuit, IrPassPeepholes::applyPassToBlock);
  }

  private static Ir.Block applyPassToBlock(final Ir.Block block) {

    // Initialize with instructions
    final List<Ir.Instruction> instructions = new ArrayList<>();
    final Map<Ir.VariableId, Ir.Type> variableTypes =
        block.instructions().stream()
            .collect(
                Collectors.toUnmodifiableMap(Ir.Instruction::resultId, Ir.Instruction::resultType));

    final Map<Ir.VariableId, Ir.Operation> operationMap = new LinkedHashMap<>();

    for (final Ir.Instruction originalInsn : block.instructions()) {
      final Ir.Instruction newInsn = optimizeInstruction(originalInsn, variableTypes, operationMap);
      operationMap.put(newInsn.resultId(), newInsn.operation());
      instructions.add(newInsn);
    }

    return new Ir.Block(
        block.blockId(),
        block.inputs(),
        List.copyOf(instructions),
        block.terminator(),
        block.isEntryPoint());
  }

  private static Ir.Instruction optimizeInstruction(
      final Ir.Instruction insn,
      final Map<Ir.VariableId, Ir.Type> variableTypes,
      final Map<Ir.VariableId, Ir.Operation> operationMap) {

    if (AnalysisSideEffects.hasPossibleSideEffect(insn.operation())) {
      return insn;
    }

    Ir.Operation optimizedOperation;
    if (insn.resultType().width() == 0) {
      // Optimize zero-width types
      optimizedOperation = new Ir.Operation.Constant(insn.resultType(), BigInteger.ZERO);

    } else {

      // Optimize operation
      optimizedOperation = optimizeOperation(insn.resultType(), insn.operation(), operationMap);
    }

    // Check if already aliased
    final Ir.VariableId operationAliasFor =
        OperationUtility.isIdentityOperation(optimizedOperation, variableTypes);
    if (operationAliasFor != null) {
      optimizedOperation = OperationUtility.createIdentity(insn.resultType(), operationAliasFor);
    }

    return new Ir.Instruction(insn.resultId(), insn.resultType(), optimizedOperation);
  }

  private static Ir.Operation optimizeOperation(
      final Ir.Type resultType,
      final Ir.Operation uncheckedOperation,
      final Map<Ir.VariableId, Ir.Operation> operationMap) {

    // Binary operations
    if (uncheckedOperation instanceof Ir.Operation.Binary operation) {
      return optimizeBinaryOperation(resultType, operation, operationMap);

      // Select
    } else if (uncheckedOperation instanceof Ir.Operation.Select operation) {
      return optimizeSelectOperation(resultType, operation, operationMap);
    }

    // Unknown / Unoptimizable operations
    return uncheckedOperation;
  }

  /**
   * Optimizes the given select operation. Producing a replacement operation that should be more
   * optimal than the previous instruction, or at least just as optimal as the input operation..
   *
   * @param resultType Result type of select operation. Must be preserved by optimization.
   * @param operation The Select operation to optimize.
   * @param operationMap Map of previously encountered operations.
   * @return Newly optimized instructions.
   */
  private static Ir.Operation optimizeSelectOperation(
      final Ir.Type resultType,
      final Ir.Operation.Select operation,
      final Map<Ir.VariableId, Ir.Operation> operationMap) {

    // Constant folding of select.
    final Ir.Operation cndOper = operationMap.get(operation.varCondition());
    final BigInteger cndConst = constValue(cndOper);
    if (cndConst != null) {
      final boolean cnd = !cndConst.equals(BigInteger.ZERO);
      final Ir.VariableId resultId = cnd ? operation.varIfTrue() : operation.varIfFalse();
      return OperationUtility.createIdentity(resultType, resultId);
    }

    // Flip condition if it is currently negated.
    if (cndOper instanceof Ir.Operation.Unary cndOperUnary
        && cndOperUnary.operation() == Ir.Operation.UnaryOp.BITWISE_NOT) {
      // Swap varIfTrue and varIfFalse.
      return new Ir.Operation.Select(
          cndOperUnary.varIdx1(),
          /* varIfTrue= */ operation.varIfFalse(),
          /* varIfFalse= */ operation.varIfTrue());
    }

    // No opt
    return operation;
  }

  private static boolean constantMatches(Ir.Operation operation, Integer i) {
    final BigInteger c = constValue(operation);
    if (c == null || i == null) {
      return false;
    }

    return Objects.equals(c, BigInteger.valueOf(i));
  }

  private static BigInteger constValue(Ir.Operation operation) {
    if (operation instanceof Ir.Operation.Constant c) {
      return c.const1();
    }
    return null;
  }

  private static Ir.Operation optimizeBinaryOperation(
      final Ir.Type resultType,
      final Ir.Operation.Binary uncheckedOperation,
      final Map<Ir.VariableId, Ir.Operation> operationMap) {

    final Ir.Operation.BinaryOp oper = uncheckedOperation.operation();
    final OperationUtility.BinopProperties properties = OperationUtility.getBinopProperties(oper);
    final Ir.Operation leftConstant = operationMap.get(uncheckedOperation.varIdx1());
    final Ir.Operation rightConstant = operationMap.get(uncheckedOperation.varIdx2());

    // Left identity
    if (constantMatches(leftConstant, properties.identityLeft())) {
      return OperationUtility.createIdentity(resultType, uncheckedOperation.varIdx2());

      // Right identity
    } else if (constantMatches(rightConstant, properties.identityRight())) {
      return OperationUtility.createIdentity(resultType, uncheckedOperation.varIdx1());

      // Left zero
    } else if (constantMatches(leftConstant, properties.absorbingLeft())) {
      return new Ir.Operation.Constant(resultType, BigInteger.ZERO);

      // Right zero
    } else if (constantMatches(rightConstant, properties.absorbingRight())) {
      return new Ir.Operation.Constant(resultType, BigInteger.ZERO);
    }

    // Unknown / Unoptimizable operations
    return OperationUtility.createBinary(
        resultType,
        uncheckedOperation.operation(),
        uncheckedOperation.varIdx1(),
        uncheckedOperation.varIdx2());
  }
}
