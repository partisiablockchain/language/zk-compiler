package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.InstructionAccum;
import com.partisiablockchain.language.zkcompiler.IrToMetaCircuitTranslator;
import com.partisiablockchain.language.zkcompiler.Operand;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.AliasMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * {@link IrPass} transforming {@link Ir.Operation.CallDirectly} operation to {@link
 * Ir.CallAndBranch } terminator.
 *
 * <p>{@link IrToMetaCircuitTranslator} requires {@link Ir.CallAndBranch } terminators, as these map
 * directly onto the representations used by meta-circuits, but the terminator representation is
 * more difficult to optimize, compared to the {@link Ir.Operation.CallDirectly} representation.
 * Hence the need for a pass that can transform from the operation to the terminator.
 */
public final class IrPassCallOperatorToTerminator extends IrPassIntraFunction {

  @Override
  public String getName() {
    return "ir-operator-call-to-terminator";
  }

  @Override
  protected Ir.Function applyPassToFunction(
      final Ir.Function originalFunction, final IdGenerator<Ir.VariableId> idGenerator) {
    final InstructionAccum insnAccum =
        new InstructionAccum(idGenerator, IdGenerator.uniqueBlockIdGeneratorFor(originalFunction));

    for (final Ir.Block block : originalFunction.blocks()) {
      applyPassToBlock(block, insnAccum);
    }

    // Stage 2: Replace blocks if relevant
    return originalFunction.replaceBlocks(insnAccum.blocks());
  }

  private static void applyPassToBlock(final Ir.Block block, final InstructionAccum insnAccum) {
    // Pass works by replace maintaining a list of variables that should be
    // saved across block transitions, and an alias map for keeping track of
    // previously saved variables.
    //
    // Once a call instruction occurs, variables in the maintain list will have
    // their value maintained by extending the return-to-block branch
    // information with these variables, and the input of the following block
    // will likewise contain new aliases for them.

    insnAccum.newBlock(block.blockId(), block.inputs(), block.isEntryPoint());

    final ArrayList<Operand.Variable> variablesToMaintain =
        new ArrayList<>(
            block.inputs().stream().map(IrPassCallOperatorToTerminator::operandFromInput).toList());
    final AliasMap<Ir.VariableId> aliasMap = new AliasMap<Ir.VariableId>(new HashMap<>());

    for (final Ir.Instruction insn : block.instructions()) {
      if (insn.operation() instanceof Ir.Operation.CallDirectly operation) {
        replaceCallOperationWithTerminator(
            insn.resultId(),
            insn.resultType(),
            operation,
            insnAccum,
            variablesToMaintain,
            aliasMap);
        if (operation.numReturnValues() == 1) {
          variablesToMaintain.add(new Operand.Variable(insn.resultId(), insn.resultType()));
        }

      } else {
        final Ir.Instruction insnReplaced =
            IrPassCommonInstructionElimination.replaceInInstruction(insn, aliasMap);
        final Operand.Variable operVar = insnAccum.add(insnReplaced);
        variablesToMaintain.add(operVar);
      }
    }

    insnAccum.addTerminator(
        IrPassCommonInstructionElimination.replaceInTerminator(block.terminator(), aliasMap));
  }

  /**
   * Outputs a call terminator sequence given the call operation. Main work-horse for this pass.
   *
   * @param returnResultId Variable to store call result.
   * @param returnResultType Type of result variable.
   * @param operation Call operation to replace.
   * @param insnAccum Instruction accumulator to add instruction sequence to.
   * @param variablesToMaintain List of variables that should be saved across block transitions.
   * @param aliasMap Alias map of saved variables.
   */
  private static void replaceCallOperationWithTerminator(
      final Ir.VariableId returnResultId,
      final Ir.Type returnResultType,
      final Ir.Operation.CallDirectly operation,
      final InstructionAccum insnAccum,
      final List<Operand.Variable> variablesToMaintain,
      final AliasMap<Ir.VariableId> aliasMap) {

    // Close the current block by calling into the function
    final Ir.BlockId intermediateBlockId =
        insnAccum.newBlockId(operation.functionId().name() + "_return");

    final List<Ir.VariableId> functionArguments =
        operation.functionArguments().stream().map(aliasMap::get).toList();

    final List<Ir.VariableId> variablesToMaintainInBranch =
        variablesToMaintain.stream().map(Operand.Variable::variable).toList();
    final Ir.Terminator terminatorWithVariablesToMaintain =
        new Ir.CallAndBranch(
            operation.functionId(),
            functionArguments,
            new Ir.BranchInfo(intermediateBlockId, variablesToMaintainInBranch),
            operation.numReturnValues());
    insnAccum.addTerminator(
        IrPassCommonInstructionElimination.replaceInTerminator(
            terminatorWithVariablesToMaintain, aliasMap));

    // Setup return block.
    final List<Ir.Input> inputs = new ArrayList<>();
    for (int idx = 0; idx < variablesToMaintain.size(); idx++) {
      final Operand.Variable oper = variablesToMaintain.get(idx);
      final Ir.VariableId newVarId = insnAccum.newVariableId(oper.variable().name());
      aliasMap.put(oper.variable(), newVarId);
      inputs.add(new Ir.Input(newVarId, oper.type()));
    }
    if (operation.numReturnValues() == 1) {
      inputs.add(new Ir.Input(returnResultId, returnResultType));
    }
    insnAccum.newBlock(intermediateBlockId, List.copyOf(inputs), false);
  }

  private static Operand.Variable operandFromInput(Ir.Input oper) {
    return new Operand.Variable(oper.assignedVariable(), oper.variableType());
  }
}
