package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.InternalCompilerException;
import java.math.BigInteger;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Simple symbolic value, representing at most one value.
 *
 * <p>Lattice is constructed as:
 *
 * <pre>
 *    -----  ⊤  -----
 *   /  /  / | \  \  \
 *  0  1  2  3  4  5  ....
 *   \  \  \ | /  /  /
 *    -----  ⊥  -----
 * </pre>
 *
 * <p>With the top (⊤) element representing that the position represented by the symbolic value
 * could assume _any_ value at runtime. Integer elements would represent the position only possesing
 * that singular value at runtime, and the bottom (⊥) element representing no value at runtime.
 *
 * <p>This lattice is only appropriate for constant folding, as it doesn't store enough information
 * for simple inferences.
 *
 * <p>Elements are encoded as:
 *
 * <ul>
 *   <li><b>Top (⊤)</b>: {@code value == null && !isBottom}
 *   <li><b>Bottom (⊥)</b>: {@code value == null && isBottom}
 *   <li><b>Integer</b>: {@code else}
 * </ul>
 */
public final class SymbolicValueSimple implements SymbolicValue<SymbolicValueSimple> {

  /**
   * Value of the operand. Only present if the value is an instance of the integer elements. Absence
   * indicates either top or bottom.
   */
  private final BigInteger value;

  /** Bitwidth of value. Always present, even for top and bottom. Used for concat operations. */
  private final int bitwidth;

  /** Whether element is bottom when value is null. */
  private final boolean isBottom;

  //// Constructors

  /**
   * Create new {@link SymbolicValueSimple}.
   *
   * @param value Value of the operand. Only present if the value is an instance of the integer
   *     elements. Absence indicates either top or bottom.
   * @param bitwidth Bitwidth of value. Always present, even for top and bottom. Used for concat
   *     operations.
   * @param isBottom Whether element is bottom when value is null.
   */
  private SymbolicValueSimple(BigInteger value, int bitwidth, boolean isBottom) {
    this.value = value;
    this.bitwidth = bitwidth;
    this.isBottom = isBottom;
  }

  /**
   * Create lattice element representing integer.
   *
   * @param constant Integer to create element for.
   * @param bitwidth Bitwidth of the produced value.
   * @return Newly created element.
   */
  public static SymbolicValueSimple constant(int constant, int bitwidth) {
    return constant(BigInteger.valueOf(constant), bitwidth);
  }

  /**
   * Create lattice element representing integer.
   *
   * @param constant Integer to create element for.
   * @param bitwidth Bitwidth of the produced value.
   * @return Newly created element.
   */
  public static SymbolicValueSimple constant(BigInteger constant, int bitwidth) {
    return new SymbolicValueSimple(constant, bitwidth, false);
  }

  /**
   * Value of the operand. Only present if the value is an instance of the integer elements. Absence
   * indicates either top or bottom.
   *
   * @return Value of the operand.
   */
  public BigInteger value() {
    return value;
  }

  @Override
  public boolean isBottom() {
    return isBottom;
  }

  @Override
  public boolean isTop() {
    return !isBottom && value == null;
  }

  @Override
  public boolean equals(final Object uncheckedOther) {
    if (uncheckedOther instanceof SymbolicValueSimple other) {
      return Objects.equals(this.value, other.value)
          && this.bitwidth == other.bitwidth
          && this.isBottom == other.isBottom;
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.value)
        ^ Integer.hashCode(this.bitwidth)
        ^ Boolean.hashCode(isBottom);
  }

  //// Lattice relations

  /** Also called the maximum/top element. */
  public static SymbolicValueSimple TOP_VALUE = new SymbolicValueSimple(null, 0, false);

  /** Also called the least/bottom element. */
  public static SymbolicValueSimple BOTTOM_VALUE = new SymbolicValueSimple(null, 0, true);

  /** Shorthand for zero element. */
  public static SymbolicValueSimple ZERO = new SymbolicValueSimple(BigInteger.ZERO, 1, false);

  /** Shorthand for one element. */
  public static SymbolicValueSimple ONE = new SymbolicValueSimple(BigInteger.ONE, 1, false);

  @Override
  public SymbolicValueSimple leastUpperBound(final SymbolicValueSimple other) {
    if (this.equals(other)) {
      return this;
    }
    if (this.isBottom()) {
      return other;
    }
    if (other.isBottom()) {
      return this;
    }
    return TOP_VALUE.withSize(bitwidth);
  }

  //// Symbolic value operations

  @Override
  public SymbolicValueSimple bitwiseAnd(final SymbolicValueSimple other) {
    return binary(other, bitwidth, BigInteger::and);
  }

  @Override
  public SymbolicValueSimple bitwiseOr(final SymbolicValueSimple other) {
    return binary(other, bitwidth, BigInteger::or);
  }

  @Override
  public SymbolicValueSimple bitwiseXor(final SymbolicValueSimple other) {
    return binary(other, bitwidth, BigInteger::xor);
  }

  @Override
  public SymbolicValueSimple shiftRight(final SymbolicValueSimple other) {
    return binary(other, bitwidth, (a, b) -> a.shiftRight(b.intValue()));
  }

  @Override
  public SymbolicValueSimple shiftLeft(final SymbolicValueSimple other) {
    return binary(other, bitwidth, (a, b) -> a.shiftLeft(b.intValue()));
  }

  @Override
  public SymbolicValueSimple arithmeticAdd(final SymbolicValueSimple other) {
    return binary(other, bitwidth, BigInteger::add);
  }

  @Override
  public SymbolicValueSimple arithmeticMultiply(final SymbolicValueSimple other) {
    return binary(other, bitwidth, BigInteger::multiply);
  }

  @Override
  public SymbolicValueSimple arithmeticModulo(final BigInteger other) {
    return unary(x -> x.mod(other));
  }

  @Override
  public SymbolicValueSimple compareEquals(final SymbolicValueSimple other) {
    return binary(other, 1, (a, b) -> a.compareTo(b) == 0 ? BigInteger.ONE : BigInteger.ZERO);
  }

  @Override
  public SymbolicValueSimple compareLessThanSigned(SymbolicValueSimple other) {
    final SymbolicValueSimple signedSelf = this.toSigned();
    final SymbolicValueSimple signedOther = other.toSigned();
    return signedSelf.binary(
        signedOther, 1, (a, b) -> a.compareTo(b) < 0 ? BigInteger.ONE : BigInteger.ZERO);
  }

  @Override
  public SymbolicValueSimple compareLessThanUnsigned(final SymbolicValueSimple other) {
    return binary(other, 1, (a, b) -> a.compareTo(b) < 0 ? BigInteger.ONE : BigInteger.ZERO);
  }

  @Override
  public SymbolicValueSimple compareLessThanOrEqualsSigned(SymbolicValueSimple other) {
    final SymbolicValueSimple signedSelf = this.toSigned();
    final SymbolicValueSimple signedOther = other.toSigned();
    return signedSelf.binary(
        signedOther, 1, (a, b) -> a.compareTo(b) <= 0 ? BigInteger.ONE : BigInteger.ZERO);
  }

  @Override
  public SymbolicValueSimple compareLessThanOrEqualsUnsigned(final SymbolicValueSimple other) {
    return binary(other, 1, (a, b) -> a.compareTo(b) <= 0 ? BigInteger.ONE : BigInteger.ZERO);
  }

  @Override
  public SymbolicValueSimple selectBetween(
      final SymbolicValueSimple ifTruthy, final SymbolicValueSimple ifFalsy) {
    if (this.isBottom()) {
      return BOTTOM_VALUE;
    } else if (this.isTop()) {
      return ifTruthy.leastUpperBound(ifFalsy);
    }
    return this.value.equals(BigInteger.ZERO) ? ifFalsy : ifTruthy;
  }

  @Override
  public SymbolicValueSimple bitwiseNot() {
    return unary(BigInteger::not);
  }

  @Override
  public SymbolicValueSimple arithmeticNegation() {
    return unary(BigInteger::negate);
  }

  @Override
  public SymbolicValueSimple concatWith(SymbolicValueSimple other) {
    return binary(other, bitwidth + other.bitwidth, (l, r) -> l.shiftLeft(other.bitwidth).xor(r));
  }

  private SymbolicValueSimple toSigned() {
    if (isTop() || isBottom()) {
      return this;
    }

    BigInteger val = value;
    final int width = bitwidth;

    if (val.testBit(width - 1)) {
      val = val.subtract(BigInteger.ONE.shiftLeft(width));
    }

    return new SymbolicValueSimple(val, width, false);
  }

  @Override
  public SymbolicValueSimple extract(int bitwidthToExtract, SymbolicValueSimple operOffsetFromLow) {
    return binary(
        operOffsetFromLow,
        bitwidthToExtract,
        (value, bitOffsetFromLow) -> {
          if (bitOffsetFromLow.intValue() + bitwidthToExtract > bitwidth) {
            throw new InternalCompilerException(
                "Constant fold extraction (%s bits at offset %s) extends outside operand size %s",
                bitwidthToExtract, bitOffsetFromLow, bitwidth);
          }
          return value
              .shiftRight(bitOffsetFromLow.intValue())
              .and(BigInteger.ONE.shiftLeft(bitwidthToExtract).subtract(BigInteger.ONE));
        });
  }

  @Override
  public SymbolicValueSimple insert(SymbolicValueSimple insertedValue, SymbolicValueSimple offset) {
    if (offset.isBottom()) {
      return BOTTOM_VALUE.withSize(bitwidth);
    } else if (offset.isTop()) {
      return TOP_VALUE.withSize(bitwidth);
    }

    final int bitOffset = offset.value.intValue();

    final SymbolicValueSimple lowBits = extract(bitOffset, constant(0, 32));
    final SymbolicValueSimple highBits =
        extract(
            bitwidth - insertedValue.bitwidth - lowBits.bitwidth,
            constant(bitOffset + insertedValue.bitwidth, 32));

    return highBits.concatWith(insertedValue).concatWith(lowBits);
  }

  private SymbolicValueSimple unary(final Function<BigInteger, BigInteger> operation) {
    if (this.isBottom()) {
      return BOTTOM_VALUE.withSize(bitwidth);
    } else if (this.isTop()) {
      return TOP_VALUE.withSize(bitwidth);
    }
    return new SymbolicValueSimple(operation.apply(this.value), bitwidth, false);
  }

  private SymbolicValueSimple binary(
      final SymbolicValueSimple other,
      final int bitwidth,
      final BiFunction<BigInteger, BigInteger, BigInteger> operation) {
    if (this.isBottom() || other.isBottom()) {
      return BOTTOM_VALUE.withSize(bitwidth);
    } else if (this.isTop() || other.isTop()) {
      return TOP_VALUE.withSize(bitwidth);
    }
    return new SymbolicValueSimple(operation.apply(this.value, other.value), bitwidth, false);
  }

  @Override
  public String toString() {
    if (this.isBottom()) {
      return "⊥";
    } else if (this.isTop()) {
      return "⊤";
    } else {
      return this.value.toString();
    }
  }

  /** Produces an extended instance of this {@link SymbolicValueSimple}, with the given width. */
  private SymbolicValueSimple withSize(final int width) {
    return new SymbolicValueSimple(value, width, isBottom);
  }
}
