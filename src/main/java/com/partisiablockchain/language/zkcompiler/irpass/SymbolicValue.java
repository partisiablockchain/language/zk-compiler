package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.math.Lattice;
import java.math.BigInteger;

/**
 * Symbolic Value Representation, for building static analysis passes.
 *
 * <p>Should be constructed like a {@link Lattice}, where the elements of the lattice determine the
 * interpretation of the non-symbolic value that occurs at this position at runtime:
 *
 * <ul>
 *   <li><b>The top (⊤) element</b>: Position could assume <i>any</i> value at runtime. Happens
 *       often, either due to unpredicable input, or due to losing precision in operations.
 *   <li><b>The bottom (⊥) element</b>: Position could assume <i>no</i> value at runtime. Happens
 *       rarely, and will mainly indicate unreachable code.
 *   <li><b>Other</b>: Some more restricted set of values, depending upon the implementation. Most
 *       valuable wrt. optimizations.
 * </ul>
 *
 * @see <a href="https://cs.au.dk/~amoeller/spa/3-lattices-and-fixpoints.pdf">Anders Møller's Static
 *     Program Analysis Slides</a>
 */
interface SymbolicValue<T extends SymbolicValue<T>> extends Lattice<T> {

  /**
   * Determine whether value is the top element.
   *
   * @return True if and only if the value is the top element.
   */
  boolean isTop();

  /**
   * Determine whether value is the bottom element.
   *
   * @return True if and only if the value is the bottom element.
   */
  boolean isBottom();

  /**
   * Perform bitwise AND between this and other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T bitwiseAnd(T other);

  /**
   * Perform bitwise OR between this and other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T bitwiseOr(T other);

  /**
   * Perform bitwise XOR between this and other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T bitwiseXor(T other);

  /**
   * Shift this right by the other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T shiftRight(T other);

  /**
   * Shift this left by the other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T shiftLeft(T other);

  /**
   * Perform arithmetic addition between this and other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T arithmeticAdd(T other);

  /**
   * Perform arithmetic multiplication between this and other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T arithmeticMultiply(T other);

  /**
   * Perform arithmeitc modulo between this and other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T arithmeticModulo(BigInteger other);

  /**
   * Compare this and other.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T compareEquals(T other);

  /**
   * Compare this and other, interpreting them as signed.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T compareLessThanSigned(T other);

  /**
   * Compare this and other, interpreting them as unsigned.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T compareLessThanUnsigned(T other);

  /**
   * Compare this and other, interpreting them as signed.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T compareLessThanOrEqualsSigned(T other);

  /**
   * Compare this and other, interpreting them as unsigned.
   *
   * @param other Right parameter of this operation.
   * @return Resulting value
   */
  T compareLessThanOrEqualsUnsigned(T other);

  /**
   * Perform selection over this value, producing truthy value or falsy value.
   *
   * @param ifTruthy Value to produce if this is truthy.
   * @param ifFalsy Value to produce if this is falsy.
   * @return Resulting value
   */
  T selectBetween(T ifTruthy, T ifFalsy);

  /**
   * Perform bitwise NOT for this.
   *
   * @return Resulting value
   */
  T bitwiseNot();

  /**
   * Perform arithmetic negation for this.
   *
   * @return Resulting value
   */
  T arithmeticNegation();

  /**
   * Extract value from this value.
   *
   * @return Resulting value
   */
  T extract(int bitsizeToExtract, T bitOffsetFromLow);

  /**
   * Insert another value into this value.
   *
   * @return Resulting value
   */
  T insert(T insertedValue, T offset);

  /**
   * Concat value with other value.
   *
   * @return Resulting value.
   */
  T concatWith(T other);
}
