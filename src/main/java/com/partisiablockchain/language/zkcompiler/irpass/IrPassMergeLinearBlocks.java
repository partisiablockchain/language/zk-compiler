package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.AliasMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * IR pass to merge series of linear blocks (blocks with only a single previous block, which itself
 * only have a single next block,) into composite blocks.
 *
 * <p>Block that are not heads of these series will be removed.
 */
public final class IrPassMergeLinearBlocks extends IrPassIntraFunction {

  @Override
  public String getName() {
    return "ir-merge-linear-blocks";
  }

  @Override
  protected Ir.Function applyPassToFunction(
      final Ir.Function originalFunction, final IdGenerator<Ir.VariableId> idGenerator) {
    // Analysis for how to collapse consecutive linear blocks
    final Map<Ir.BlockId, Ir.Block> blockMap = originalFunction.toBlockMap();
    final Map<Ir.BlockId, Integer> numIncomingBranchesByBlock =
        IrPass.dataFlowGraphFromBlocks(originalFunction.blocks())
            .backwardEdges()
            .entrySet()
            .stream()
            .collect(Collectors.toUnmodifiableMap(x -> x.getKey(), x -> x.getValue().size()));

    final Map<Ir.BlockId, List<Ir.BranchInfo>> linearPathsByBlock = new HashMap<>();
    for (final Ir.BlockId blockId : blockMap.keySet()) {
      final List<Ir.BranchInfo> linearPath =
          determineLinearPathForBlock(blockId, blockMap, numIncomingBranchesByBlock);
      linearPathsByBlock.put(blockId, linearPath);
    }

    // Determine which blocks will be merged into another block after this pass, so they can
    // be removed.
    final Set<Ir.BlockId> mergedBlocks =
        linearPathsByBlock.values().stream()
            .flatMap(List::stream)
            .map(Ir.BranchInfo::targetBlock)
            .collect(Collectors.toUnmodifiableSet());

    // Rewrite blocks to merge linear blocks paths and remove blocks merged
    // into other blocks.
    final List<Ir.Block> newBlocks =
        originalFunction.blocks().stream()
            .filter(block -> !mergedBlocks.contains(block.blockId()))
            .map(
                block ->
                    mergeBlocksInLinearPath(
                        block.blockId(), linearPathsByBlock.get(block.blockId()), blockMap))
            .toList();

    return originalFunction.replaceBlocks(newBlocks);
  }

  /**
   * Determines which series of linear blocks a given block can be expanded to include.
   *
   * @param initialBlockId Id of block to determine redirect for.
   * @param blockMap Map from block ids to the blocks themselves.
   * @param numIncomingBranchesByBlock Map from block ids to the number of branches are pointed
   *     towards this given block.
   * @return Information on which following blocks the given block redirects linearly to, and how to
   *     permute the inputs.
   */
  private static List<Ir.BranchInfo> determineLinearPathForBlock(
      final Ir.BlockId initialBlockId,
      final Map<Ir.BlockId, Ir.Block> blockMap,
      final Map<Ir.BlockId, Integer> numIncomingBranchesByBlock) {

    final var path = new ArrayList<Ir.BranchInfo>();
    Ir.BlockId blockId = initialBlockId;

    while (blockMap.get(blockId).terminator() instanceof Ir.BranchAlways terminator) {
      blockId = terminator.branch().targetBlock();
      if (blockId.equals(Ir.BlockId.RETURN)) {
        break;
      } else if (numIncomingBranchesByBlock.getOrDefault(blockId, 0) != 1) {
        break;
      }
      path.add(terminator.branch());
    }

    return List.copyOf(path);
  }

  /**
   * Merges blocks in given linear path into a new single block.
   *
   * @param linearPath Path of branches; assumed order is correct.
   * @param blockMap Map of ids to blocks for the IR-circuit.
   * @return Newly created merged block.
   */
  private static Ir.Block mergeBlocksInLinearPath(
      final Ir.BlockId initialBlockId,
      final List<Ir.BranchInfo> linearPath,
      final Map<Ir.BlockId, Ir.Block> blockMap) {

    final Ir.Block initialBlock = blockMap.get(initialBlockId);

    // Quick return if block is already merged.
    if (linearPath.isEmpty()) {
      return initialBlock;
    }

    // Begin merging
    final var mergedInstructions = new ArrayList<Ir.Instruction>();
    mergedInstructions.addAll(initialBlock.instructions());

    for (final Ir.BranchInfo branchInfo : linearPath) {
      final Ir.Block branchBlock = blockMap.get(branchInfo.targetBlock());
      assignInputs(mergedInstructions, branchBlock.inputs(), branchInfo.branchInputVariables());
      mergedInstructions.addAll(branchBlock.instructions());
    }

    // Merge terminator
    final Ir.Terminator lastBlockTerminator =
        blockMap.get(linearPath.get(linearPath.size() - 1).targetBlock()).terminator();
    final AliasMap<Ir.VariableId> aliasMap =
        aliasMapFrom(mergedInstructions, initialBlock.inputs());

    // Result block
    return new Ir.Block(
        initialBlock.blockId(),
        initialBlock.inputs(),
        List.copyOf(mergedInstructions),
        IrPassCommonInstructionElimination.replaceInTerminator(lastBlockTerminator, aliasMap),
        initialBlock.isEntryPoint());
  }

  /**
   * Produces IR sequence for assigning the given input to the given variable ids.
   *
   * @param insnAccum Instruction accumulator to add new instructions to.
   * @param inputs Input definitions for variable to assign.
   * @param argVars Parallel list for how to assign inputs.
   */
  static void assignInputs(
      final List<Ir.Instruction> insnAccum,
      final List<Ir.Input> inputs,
      final List<Ir.VariableId> argVars) {
    for (int inputIdx = 0; inputIdx < inputs.size(); inputIdx++) {
      final Ir.Input inputDef = inputs.get(inputIdx);
      final Ir.VariableId argVar = argVars.get(inputIdx);
      insnAccum.add(
          new Ir.Instruction(
              inputDef.assignedVariable(),
              inputDef.variableType(),
              OperationUtility.createIdentity(inputDef.variableType(), argVar)));
    }
  }

  /**
   * Determines variable aliases for the given block instructions and inputs.
   *
   * @param instructions Instructions in block.
   * @param inputs Inputs for block.
   * @return Map from ids to their source id.
   */
  static AliasMap<Ir.VariableId> aliasMapFrom(
      final List<Ir.Instruction> instructions, final List<Ir.Input> inputs) {

    final Map<Ir.VariableId, Ir.Type> variableTypes = typeMapFrom(instructions, inputs);

    final var aliasMap = new LinkedHashMap<Ir.VariableId, Ir.VariableId>();
    for (final Ir.Instruction insn : instructions) {
      final Ir.VariableId aliasFor =
          OperationUtility.isIdentityOperation(insn.operation(), variableTypes);
      if (aliasFor != null) {
        aliasMap.put(insn.resultId(), aliasMap.getOrDefault(aliasFor, aliasFor));
      }
    }
    return new AliasMap<>(Map.copyOf(aliasMap));
  }

  /**
   * Produces a map from variable ids to their types, for the given block instructions and inputs.
   *
   * @param instructions Instructions in block.
   * @param inputs Inputs for block.
   * @return Map from ids to types.
   */
  private static Map<Ir.VariableId, Ir.Type> typeMapFrom(
      final List<Ir.Instruction> instructions, final List<Ir.Input> inputs) {
    return Stream.concat(
            instructions.stream().map(insn -> Map.entry(insn.resultId(), insn.resultType())),
            inputs.stream().map(input -> Map.entry(input.assignedVariable(), input.variableType())))
        .collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
