package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.TranslationConfig;
import com.partisiablockchain.language.zkcompiler.ir.Ir;

/** {@link IrPass} operating over individual {@link Ir.Function}. */
public abstract class IrPassIntraFunction implements IrPass {

  @Override
  public Ir.Circuit applyPass(final Ir.Circuit originalCircuit, final TranslationConfig config) {
    final IdGenerator<Ir.VariableId> idGenerator =
        IdGenerator.uniqueVariableIdGeneratorFor(originalCircuit);
    return new Ir.Circuit(
        originalCircuit.functions().stream()
            .map(f -> applyPassToFunction(f, idGenerator))
            .toList());
  }

  /**
   * Entry-point for transforming the function.
   *
   * @param originalFunction Previous function generated in the IR chain.
   * @param idGenerator Id generator for producing new identifiers.
   * @return Transformed IR function.
   */
  protected abstract Ir.Function applyPassToFunction(
      Ir.Function originalFunction, IdGenerator<Ir.VariableId> idGenerator);
}
