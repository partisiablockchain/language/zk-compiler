package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.DataFlowAnalysis;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Utility call for performing side-effect analysis.
 *
 * <p>A <b>Side-effect</b> is any changes to the (invisible) global state. For example, pointer
 * assignments in the C programming language is an side effect.
 *
 * <p>ZK Rust is mostly Side-effect free, with a few notable exceptions. Side-effect analysis is a
 * key component in determining when the {@link IrPassCommonInstructionElimination} and {@link
 * IrPassDeadCodeElimination} passes are safe to perform.
 *
 * <p>Exposes several different definitions of side-effects:
 *
 * <ul>
 *   <li>{@link hasSideEffectIntrinsically}: Will return true when execution the operation will
 *       always produce a side-effect. Avoid using externally.
 *   <li>{@link hasPossibleSideEffect}: Will return true when execution the operation can produce a
 *       side-effect.
 *   <li>{@link Result#hasSideEffect}: Will return true when execution the operation can produce a
 *       side-effect, taking function-level side-effect analysis into account. Preferred version to
 *       use.
 * </ul>
 *
 * <p>The difference is most pronounced for function calls, which might or might not count as
 * side-effects depending upon which function is being called, and what instruction that function
 * uses and which functions the function itself calls. {@link hasPossibleSideEffect} is the safest,
 * followed by {@link Result#hasSideEffect}, followed by {@link hasSideEffectIntrinsically}.
 */
public final class AnalysisSideEffects {

  private AnalysisSideEffects() {}

  /** Result object for {@link AnalysisSideEffects}. */
  public static final class Result {

    /**
     * Function ids for functions that includes side effects. Calls to those functions cannot be
     * removed. Never null.
     */
    private final Set<Ir.FunctionId> functionsWithSideEffects;

    Result(final Set<Ir.FunctionId> functionsWithSideEffects) {
      this.functionsWithSideEffects = functionsWithSideEffects;
    }

    /**
     * Whether given instruction has any side effects when run, taking function side-effect analysis
     * into account.
     *
     * @param insn Instruction to check for side effects.
     * @return True if instruction has side effect. Might bias towards true, even for
     *     non-side-effect having operations.
     */
    public boolean hasSideEffect(Ir.Instruction insn) {
      return hasSideEffect(insn.operation());
    }

    /**
     * Whether this operation has any side effects when run, taking function side-effect analysis
     * into account.
     *
     * @param uncheckedOperation Operation to check for side effects.
     * @return True if operation has side effect. Might bias towards true, even for non-side-effect
     *     having instructions.
     */
    public boolean hasSideEffect(final Ir.Operation uncheckedOperation) {
      if (uncheckedOperation instanceof Ir.Operation.CallDirectly operation) {
        return hasSideEffect(operation.functionId());
      }
      return hasSideEffectIntrinsically(uncheckedOperation);
    }

    /**
     * Whether the function with the id has any side effects when run.
     *
     * @param functionId Function to check for side effects.
     * @return True if operation has side effect. Might bias towards true, even for non-side-effect
     *     having functions.
     * @see AnalysisSideEffects
     */
    public boolean hasSideEffect(final Ir.FunctionId functionId) {
      return functionsWithSideEffects.contains(functionId);
    }
  }

  /**
   * Performs function-level side-effect analysis over the given circuit.
   *
   * @param circuit Circuit to analyse.
   * @return Analysis result.
   */
  public static Result analyseFunctionSideEffects(final Ir.Circuit circuit) {
    final DataFlowAnalysis.Graph<AnalysisNode> graph = callGraphFromCircuit(circuit);
    final Map<AnalysisNode, Boolean> sideEffects =
        DataFlowAnalysis.backwardAnalysis(
            graph, AnalysisSideEffects::doesFunctionIncludeSideEffectOrCallOthersWithSideEffect);

    // Translate to useful structure
    final Set<Ir.FunctionId> functionsWithSideEffects =
        sideEffects.entrySet().stream()
            .filter(Map.Entry::getValue)
            .map(Map.Entry::getKey)
            .filter(node -> node instanceof AnalysisNode.Fn)
            .map(fn -> ((AnalysisNode.Fn) fn).functionId())
            .collect(Collectors.toUnmodifiableSet());

    return new Result(functionsWithSideEffects);
  }

  /**
   * A node in the variable effect analysis graph. This is either an function id or a special
   * "artificial" node which represents something that has an effect on the outcome of the
   * computation.
   */
  private sealed interface AnalysisNode {
    /**
     * Indicates that this node has a direct effect on the result of the computation, by modifying
     * global state.
     */
    record HasEffect() implements AnalysisNode {
      static final HasEffect SINGLETON = new HasEffect();
    }

    /**
     * Individual function analysis node.
     *
     * @param functionId Id of the associated function.
     */
    record Fn(Ir.FunctionId functionId) implements AnalysisNode {}
  }

  /**
   * Lattice fixpoint function for determining whether any given function possess global state
   * side-effects.
   *
   * <p>A node is required for execution, only if it is the artificial {@link
   * AnalysisNode.HasEffect}, or if the function calls another function with side effects.
   *
   * <p>This operation is monotone over a boolean lattice.
   */
  private static Boolean doesFunctionIncludeSideEffectOrCallOthersWithSideEffect(
      final AnalysisNode node, final Map<AnalysisNode, Boolean> calledFunctions) {
    return node instanceof AnalysisNode.HasEffect || calledFunctions.containsValue(true);
  }

  /**
   * Whether the given operation will always produce side effects. Will preferably bias towards
   * {@code true} to preserve soundness. This method does not take side-effect completeness into
   * account, in contrast to {@link Result#hasSideEffect}, which does. This divergence is most
   * notable for {@link Ir.Operation.Call} which will return {@code false} here, but might produce
   * {@code true} in {@link Result#hasSideEffect}.
   *
   * @param uncheckedOperation Operation to check.
   * @return Whether the operation has intrinsical side effects.
   * @see AnalysisSideEffects
   */
  static boolean hasSideEffectIntrinsically(final Ir.Operation uncheckedOperation) {
    if (uncheckedOperation instanceof Ir.Operation.Unary operation) {
      return operation.operation() == Ir.Operation.UnaryOp.UNKNOWN
          || operation.operation() == Ir.Operation.UnaryOp.SAVE_VARIABLE;
    }
    return false;
  }

  /**
   * Whether the given operation _can_ produce side effects. Will preferably bias towards {@code
   * true} to preserve soundness. This method does not take side-effect completeness into account,
   * in contrast to {@link Result#hasSideEffect}, which does. This divergence is most notable for
   * {@link Ir.Operation.Call} which will return {@code true} here, but might produce {@code false}
   * in {@link Result#hasSideEffect}.
   *
   * @param uncheckedOperation Operation to check.
   * @return Whether the operation has possible side effects.
   * @see AnalysisSideEffects
   */
  static boolean hasPossibleSideEffect(final Ir.Operation uncheckedOperation) {
    if (hasSideEffectIntrinsically(uncheckedOperation)) {
      return true;
    }
    return uncheckedOperation instanceof Ir.Operation.CallDirectly;
  }

  /**
   * Determine call graph from the given circuit.
   *
   * @param circuit Circuit to analyse.
   * @return Call graph.
   */
  private static DataFlowAnalysis.Graph<AnalysisNode> callGraphFromCircuit(
      final Ir.Circuit circuit) {
    // Initialize graph
    final DataFlowAnalysis.Graph<AnalysisNode> graphAccum = DataFlowAnalysis.Graph.empty();

    for (final Ir.Function function : circuit.functions()) {
      callGraphFromFunction(function, graphAccum);
    }

    return graphAccum;
  }

  /**
   * Determine call subgraph from the given function.
   *
   * @param function Function to analyse.
   * @param graphAccum Graph to append to.
   */
  private static void callGraphFromFunction(
      final Ir.Function function, final DataFlowAnalysis.Graph<AnalysisNode> graphAccum) {
    final var fnNode = new AnalysisNode.Fn(function.functionId());
    for (final Ir.Block block : function.blocks()) {

      // Instructions
      for (final Ir.Instruction insn : block.instructions()) {
        if (hasSideEffectIntrinsically(insn.operation())) {
          graphAccum.addEdge(fnNode, AnalysisNode.HasEffect.SINGLETON);
        } else if (insn.operation() instanceof Ir.Operation.CallDirectly operation) {
          graphAccum.addEdge(fnNode, new AnalysisNode.Fn(operation.functionId()));
        }
      }

      if (block.terminator() instanceof Ir.CallAndBranch terminator) {
        graphAccum.addEdge(fnNode, new AnalysisNode.Fn(terminator.functionId()));
      }
    }
  }
}
