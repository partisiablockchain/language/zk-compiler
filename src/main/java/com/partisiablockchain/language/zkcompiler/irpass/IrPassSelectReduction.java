package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.TranslationConfig;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Intrablock {@link IrPass} for reducing the scope of {@link Ir.Operation.Select} operations.
 *
 * <p>{@link Ir.Operation.Select} operations are notably difficult to tree-balance, and can often
 * break long chains of easy-to-balance operations. Limiting the scope of select operations can thus
 * help later passes.
 *
 * <p>Example:
 *
 * <pre>
 *  Z = X + Y
 *  D = B ? Z : Y
 *  </pre>
 *
 * <p>can be converted to:
 *
 * <pre>
 *  D = B ? X : 0
 *  Z = D + Y
 *  </pre>
 */
public final class IrPassSelectReduction implements IrPass {

  @Override
  public String getName() {
    return "ir-select-reduce";
  }

  @Override
  public Ir.Circuit applyPass(final Ir.Circuit originalCircuit, final TranslationConfig config) {
    final IdGenerator<Ir.VariableId> idGenerator =
        IdGenerator.uniqueVariableIdGeneratorFor(originalCircuit);
    return IrPass.mapBlocks(originalCircuit, b -> applyPassToBlock(b, idGenerator));
  }

  private static Ir.Block applyPassToBlock(
      final Ir.Block block, final IdGenerator<Ir.VariableId> idGenerator) {

    // Initialize with instructionAccum
    final List<Ir.Instruction> instructionAccum = new ArrayList<>();
    final Map<Ir.VariableId, Ir.Operation> operationMap = new LinkedHashMap<>();

    for (final Ir.Instruction originalInsn : block.instructions()) {

      // Try to optimize
      boolean optimized = false;
      if (originalInsn.operation() instanceof Ir.Operation.Select select) {
        final Ir.Operation uncheckedOperationIfTrue = operationMap.get(select.varIfTrue());
        if (uncheckedOperationIfTrue instanceof Ir.Operation.Binary operationIfTrue) {
          optimized =
              attemptOptimizeIfTrue(
                  instructionAccum,
                  idGenerator,
                  originalInsn.resultType(),
                  originalInsn.resultId(),
                  select,
                  operationIfTrue);
        }
      }

      // Output unoptimized, if optimization was unsuccessful.
      if (!optimized) {
        instructionAccum.add(originalInsn);
      }
      operationMap.put(
          instructionAccum.get(instructionAccum.size() - 1).resultId(),
          instructionAccum.get(instructionAccum.size() - 1).operation());
    }

    return new Ir.Block(
        block.blockId(),
        block.inputs(),
        List.copyOf(instructionAccum),
        block.terminator(),
        block.isEntryPoint());
  }

  /**
   * Attempts to perform the optimization described in the class description.
   *
   * @param instructionAccum Output instruction accumulator.
   * @param idGenerator Id generator for newly produced ids.
   * @param resultType Expected result type of the new instruction chain.
   * @param resultId Result id.
   * @param select Select to optimize over.
   * @param operationIfTrue Binary operation to optimize over.
   * @return true if and only if the optimization was performed and output to the instruction
   *     accumulator.
   */
  private static boolean attemptOptimizeIfTrue(
      final List<Ir.Instruction> instructionAccum,
      final IdGenerator<Ir.VariableId> idGenerator,
      Ir.Type resultType,
      Ir.VariableId resultId,
      Ir.Operation.Select select,
      Ir.Operation.Binary operationIfTrue) {

    final List<Ir.VariableId> argumentsOfIfTrue =
        new ArrayList<>(List.of(operationIfTrue.varIdx1(), operationIfTrue.varIdx2()));

    final Ir.Operation.Constant leftIdentity =
        leftIdentityOf(resultType, operationIfTrue.operation());

    if (leftIdentity != null && argumentsOfIfTrue.contains(select.varIfFalse())) {
      argumentsOfIfTrue.remove(select.varIfFalse());

      final Ir.VariableId idConstant = idGenerator.newId("0");
      final Ir.VariableId idSelect = idGenerator.newId("switcharoo");

      instructionAccum.add(new Ir.Instruction(idConstant, resultType, leftIdentity));
      instructionAccum.add(
          new Ir.Instruction(
              idSelect,
              resultType,
              OperationUtility.createSelect(
                  resultType, select.varCondition(), argumentsOfIfTrue.get(0), idConstant)));
      instructionAccum.add(
          new Ir.Instruction(
              resultId,
              resultType,
              OperationUtility.createBinary(
                  operationIfTrue.operation(), idSelect, select.varIfFalse())));

      return true;
    }

    return false;
  }

  /**
   * Determines the left identity of the given operation.
   *
   * @param type Expected type of the cosntant.
   * @param binop Binary operation to determine identity for.
   * @return null if no left identity exists.
   * @see OperationUtility#BinopProperties
   */
  private static Ir.Operation.Constant leftIdentityOf(Ir.Type type, Ir.Operation.BinaryOp binop) {
    final OperationUtility.BinopProperties properties = OperationUtility.getBinopProperties(binop);
    final Integer zero = 0;
    if (zero.equals(properties.identityLeft())) {
      return new Ir.Operation.Constant(type, BigInteger.ZERO);
    } else {
      return null;
    }
  }
}
