package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.InstructionAccum;
import com.partisiablockchain.language.zkcompiler.IrAmalgamUtility;
import com.partisiablockchain.language.zkcompiler.Operand;
import com.partisiablockchain.language.zkcompiler.OperandUtil;
import com.partisiablockchain.language.zkcompiler.TmpName;
import com.partisiablockchain.language.zkcompiler.TranslationConfig;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * IR Phase that splits big integer constants into multiple smaller constants, for MetaCircuit
 * support.
 *
 * <p>The ZkCompiler supports integer literals bigger than 32 bits, but this is currently not
 * supported by the MetaCircuit. To be compatible, big literals are split into smaller literals,
 * which are supported by the MetaCircuit, along with bit_concat instructions to combine them back
 * into a big integer at evaluation time.
 *
 * <p>As an example, if using 16 bits as the split size, the program {@code (constant i64 0x1234
 * 6789 ABCD EFFF} would be changed into {@code x (constant i16 0x1234) y (constant i16 0x6789) v
 * (bit_concat x y)} s (constant i16 0xABCD) t (constant i16 0xEFFF) k (bit_concat s t) z
 * (bit_concat k v)
 */
public final class IrPassSplitBigConstants implements IrPass {
  // How many bits every 'chunk' of the split integer will contain.
  private static final int SPLIT_BIT_SIZE = 32;

  // Maximum allowed value of integers before we split them.
  private static final BigInteger MAX_CONSTANT_BEFORE_SPLIT =
      BigInteger.ONE.shiftLeft(31).subtract(BigInteger.ONE);

  // Largest unsigned integer value.
  private static final BigInteger U32MAX = BigInteger.ONE.shiftLeft(32).subtract(BigInteger.ONE);

  // The smallest value that would require splitting into multiple literals.
  private static final BigInteger SMALLEST_CONST_REQUIRING_CONCAT = BigInteger.ONE.shiftLeft(32);

  private static IdGenerator<Ir.VariableId> idGen;

  @Override
  public Ir.Circuit applyPass(final Ir.Circuit originalCircuit, final TranslationConfig config) {
    idGen = IdGenerator.uniqueVariableIdGeneratorFor(originalCircuit);
    return IrPass.mapBlocks(originalCircuit, IrPassSplitBigConstants::applyPassToBlock);
  }

  @Override
  public String getName() {
    return "ir-split-big-constants";
  }

  private static Ir.Block applyPassToBlock(final Ir.Block block) {
    IdGenerator<Ir.BlockId> blockIdGen = IdGenerator.forBlockIds(0);
    InstructionAccum insnAccum = new InstructionAccum(idGen, blockIdGen);

    insnAccum.newBlock(block.blockId(), block.inputs(), block.isEntryPoint());

    for (final Ir.Instruction originalInsn : block.instructions()) {
      // Add split instructions
      final Operand.Basic operandResult = splitInstruction(originalInsn, insnAccum);

      if (operandResult != null) {
        // Save to original variable id
        insnAccum.addAssign(originalInsn.resultId(), originalInsn.resultType(), operandResult);
      } else {
        insnAccum.add(originalInsn);
      }
    }

    insnAccum.addTerminator(block.terminator());

    return insnAccum.blocks().get(0);
  }

  /**
   * Splits constant instructions if the associated literal is too large. Otherwise leave
   * instructions unchanged.
   *
   * @param originalInsn instruction to potentially split.
   * @param insnAccum accumulator to add instructions to.
   * @return id of the last instruction. Null if instruction was unchanged.
   */
  private static Operand.Basic splitInstruction(
      final Ir.Instruction originalInsn, final InstructionAccum insnAccum) {
    // Check if we have to do any changes to instruction.
    if (originalInsn.operation() instanceof Ir.Operation.Constant constant
        && constant.const1().compareTo(MAX_CONSTANT_BEFORE_SPLIT) > 0) {

      if (constant.const1().compareTo(SMALLEST_CONST_REQUIRING_CONCAT) >= 0) {
        // If constant > 2^32-1 then split into chunks
        return emitConcattedConstantInstructionSequence(
            insnAccum, originalInsn.resultType(), constant.const1());
      } else {
        // Otherwise constant is in the range [2^31, 2^32-1], and must be cast to signed (negative)
        // int.
        return castVariableToSigned(insnAccum, originalInsn.resultType(), constant.const1());
      }
    }
    return null;
  }

  /**
   * Emits instruction sequence that casts the given constant to the given resultType.
   *
   * @param insnAccum Instruction accumulator to add to.
   * @param resultType Type of the resulting constant.
   * @param resultValue Value of the resulting constant.
   * @return Operand of the final constant.
   */
  private static Operand.Basic castVariableToSigned(
      InstructionAccum insnAccum, Ir.Type resultType, BigInteger resultValue) {
    int typeWidthDifference = resultType.width() - SPLIT_BIT_SIZE;
    // The variable is contained in a type of larger bit-width.
    // Must zero-extend signed value to avoid sign-extending it.
    TmpName signedName = TmpName.root("signed_cast");
    TmpName zeroName = signedName.with("zero");

    Ir.Type signedValType = OperandUtil.ofSize(resultType, SPLIT_BIT_SIZE);
    Ir.Type zeroType = OperandUtil.ofSize(resultType, typeWidthDifference);

    List<Operand.Basic> consts =
        List.of(
            new Operand.Constant(resultValue, signedValType, signedName),
            new Operand.Constant(0, zeroType, zeroName));

    TmpName name = signedName.with("res");
    return IrAmalgamUtility.concatOperands(consts, insnAccum, name);
  }

  /**
   * Produces an instruction sequence that creates the given value, by creating a bunch of small
   * constants and concatting them.
   *
   * @param resultType Type of the resulting constant.
   * @param resultValue Value of the resulting constant.
   * @param insnAccum Instruction accumulator to add to.
   * @return Operand of the final constant.
   */
  private static Operand.Basic emitConcattedConstantInstructionSequence(
      final InstructionAccum insnAccum, final Ir.Type resultType, final BigInteger resultValue) {
    final List<Operand.Constant> constants =
        createSplitOperands(resultValue, resultType, SPLIT_BIT_SIZE);
    return IrAmalgamUtility.concatOperands(constants, insnAccum, TmpName.root("concat"));
  }

  /**
   * Splits the given {@link BigInteger} to a list of {@link Operand.Constant} that can be concatted
   * to produce the {@link BigInteger}.
   *
   * @param integerToSplit The {@link BigInteger} to split into constant operands.
   * @param originalType The original type of the {@link BigInteger}.
   * @param maxBitWidthPerOperand The maximum bitsize of the output operands.
   * @return List of {@link Operand.Constant}.
   */
  private static List<Operand.Constant> createSplitOperands(
      final BigInteger integerToSplit,
      final Ir.Type originalType,
      final int maxBitWidthPerOperand) {
    final List<Operand.Constant> splitOperands = new ArrayList<>();
    final int fullChunks = originalType.width() / maxBitWidthPerOperand;
    final int lastChunkSize = originalType.width() % maxBitWidthPerOperand;
    final Ir.Type splitType = OperandUtil.ofSize(originalType, maxBitWidthPerOperand);
    final Ir.Type lastSplitType = OperandUtil.ofSize(originalType, lastChunkSize);

    final TmpName splitName = TmpName.root("split_constant");

    BigInteger remaining = integerToSplit;
    for (int i = 0; i < fullChunks; ++i) {
      final BigInteger extractedNumber = remaining.and(U32MAX);
      splitOperands.add(new Operand.Constant(extractedNumber, splitType, splitName));
      remaining = remaining.shiftRight(maxBitWidthPerOperand);
    }

    final BigInteger extractedNumber = remaining.and(U32MAX);
    splitOperands.add(new Operand.Constant(extractedNumber, lastSplitType, splitName));

    return splitOperands;
  }
}
