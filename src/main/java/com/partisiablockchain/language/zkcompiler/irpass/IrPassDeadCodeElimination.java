package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.TranslationConfig;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.math.DataFlowAnalysis;
import com.partisiablockchain.language.zkcompiler.math.Mapping;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Interblock IR phase that removes dead instructions in the IR-circuit. Dead instructions are
 * instructions whose result does not have any effect on the outcome of the computation.
 */
public final class IrPassDeadCodeElimination implements IrPass {

  /**
   * A node in the variable effect analysis graph. This is either an variable or a special
   * "artificial" node which represents something that has an effect on the outcome of the
   * computation.
   */
  private sealed interface Node {
    record HasEffect() implements Node {
      static final HasEffect SINGLETON = new HasEffect();
    }

    record Var(Ir.VariableId variable) implements Node {}
  }

  @Override
  public Ir.Circuit applyPass(final Ir.Circuit originalCircuit, final TranslationConfig config) {
    final AnalysisSideEffects.Result sideEffectsResult =
        AnalysisSideEffects.analyseFunctionSideEffects(originalCircuit);

    return new Ir.Circuit(
        originalCircuit.functions().stream()
            .map(f -> applyPassToFunction(f, sideEffectsResult))
            .toList());
  }

  private static Ir.Function applyPassToFunction(
      final Ir.Function originalFunction, final AnalysisSideEffects.Result sideEffectsResult) {
    // Construct variable flow graph
    final DataFlowAnalysis.Graph<Node> graph =
        variableDataFlowGraphFromFunction(originalFunction, sideEffectsResult);

    // Determine which variables have an effect on the computation
    final Map<Node, Boolean> reachabilityResult =
        DataFlowAnalysis.backwardAnalysis(
            graph, IrPassDeadCodeElimination::isVariableRequiredForExecution);
    final Set<Ir.VariableId> variablesToRetain =
        reachabilityResult.entrySet().stream()
            .filter(Map.Entry::getValue)
            .map(Map.Entry::getKey)
            .filter(node -> node instanceof Node.Var)
            .map(node -> ((Node.Var) node).variable())
            .collect(Collectors.toUnmodifiableSet());

    // Determine block input mappings
    final Map<Ir.BlockId, Mapping> blockInputMappings =
        originalFunction.blocks().stream()
            .collect(
                Collectors.toMap(
                    Ir.Block::blockId, block -> determineInputMapping(block, variablesToRetain)));

    // End block outputs must be identical to the original circuit
    blockInputMappings.put(
        Ir.BlockId.RETURN, Mapping.identity(originalFunction.outputVariableTypes().size()));

    // Remove all variables and instructions without effect
    List<Ir.Block> newBlocks =
        originalFunction.blocks().stream()
            .map(
                block ->
                    removeInBlock(block, variablesToRetain, blockInputMappings, sideEffectsResult))
            .toList();
    return originalFunction.replaceBlocks(newBlocks);
  }

  @Override
  public String getName() {
    return "ir-dead-code-elim";
  }

  private static Mapping determineInputMapping(
      final Ir.Block block, final Set<Ir.VariableId> variablesToRetain) {
    final List<Ir.Input> newInputs =
        block.inputs().stream()
            .filter(x -> variablesToRetain.contains(x.assignedVariable()))
            .toList();
    return new Mapping(newInputs.stream().map(block.inputs()::indexOf).toList());
  }

  private static Ir.Block removeInBlock(
      final Ir.Block block,
      final Set<Ir.VariableId> variablesToRetain,
      final Map<Ir.BlockId, Mapping> blockInputMappings,
      final AnalysisSideEffects.Result sideEffectsResult) {
    return new Ir.Block(
        block.blockId(),
        blockInputMappings.get(block.blockId()).apply(block.inputs()),
        block.instructions().stream()
            .filter(insn -> variablesToRetain.contains(insn.resultId()))
            .toList(),
        removeInTerminator(block.terminator(), blockInputMappings, sideEffectsResult),
        block.isEntryPoint());
  }

  private static Ir.Terminator removeInTerminator(
      final Ir.Terminator uncheckedTerminator,
      final Map<Ir.BlockId, Mapping> blockInputMappings,
      final AnalysisSideEffects.Result sideEffectsResult) {
    // Branch always
    if (uncheckedTerminator instanceof Ir.BranchAlways terminator) {
      return new Ir.BranchAlways(removeInBranchInfo(terminator.branch(), blockInputMappings));

      // Call
    } else if (uncheckedTerminator instanceof Ir.CallAndBranch terminator) {
      final var branchOnReturnArguments =
          removeInBranchInfo(terminator.branchToAfterReturn(), blockInputMappings);
      if (terminator.numReturnValues() == 0
          && !sideEffectsResult.hasSideEffect(terminator.functionId())) {
        return new Ir.BranchAlways(branchOnReturnArguments);
      }
      return new Ir.CallAndBranch(
          terminator.functionId(),
          terminator.functionArguments(),
          branchOnReturnArguments,
          terminator.numReturnValues());

      // Branch if
    } else {
      final Ir.BranchIf terminator = (Ir.BranchIf) uncheckedTerminator;
      return OperationUtility.createBranchIf(
          terminator.isBranchOnSecret(),
          terminator.conditionVariable(),
          removeInBranchInfo(terminator.branchTrue(), blockInputMappings),
          removeInBranchInfo(terminator.branchFalse(), blockInputMappings));
    }
  }

  private static Ir.BranchInfo removeInBranchInfo(
      final Ir.BranchInfo branchInfo, final Map<Ir.BlockId, Mapping> blockInputMappings) {
    return new Ir.BranchInfo(
        branchInfo.targetBlock(),
        blockInputMappings.get(branchInfo.targetBlock()).apply(branchInfo.branchInputVariables()));
  }

  /**
   * Lattice fixpoint function for determining whether a given instruction is required for program
   * execution.
   *
   * <p>A node is required for execution, only if it is the artificial side effect node itself, or
   * if the node's result is used by another node which is required for execution.
   *
   * <p>This operation is monotone over a boolean lattice.
   */
  private static Boolean isVariableRequiredForExecution(
      final Node node, final Map<Node, Boolean> usageVariableEffect) {
    return node instanceof Node.HasEffect || usageVariableEffect.containsValue(true);
  }

  /**
   * Creates a variable data flow graph from the given circuit.
   *
   * <p>The graph models the flow relationship between different variables and their side effects.
   * For example, if an instruction assigning to variable A uses variable B, then an edge will be
   * connected from B to A, because the data flows from B to A.
   *
   * <p>Some instructions have a direct effect on the execution of the program. This could be inputs
   * on entry-point blocks, outputs, or branch condition variables. All of these will have an edge
   * going to {@link Node.HasEffect}.
   *
   * @param circuit Circuit to create flow control for.
   * @param sideEffectsResult Side-effect result, allowing the analysis to determine whether some
   *     function calls can be optimized away or not.
   * @return Data flow graph. Never null.
   */
  private static DataFlowAnalysis.Graph<Node> variableDataFlowGraphFromFunction(
      final Ir.Function circuit, final AnalysisSideEffects.Result sideEffectsResult) {

    // Block map for easy navigation
    final Map<Ir.BlockId, Ir.Block> blockMap = circuit.toBlockMap();

    // Initialize graph
    final DataFlowAnalysis.Graph<Node> graph = DataFlowAnalysis.Graph.empty();

    for (final Ir.Block block : circuit.blocks()) {

      // Inputs in entry-points are treated as side-effects, as removing them
      // would change the entry-point API
      if (block.isEntryPoint()) {
        for (final Ir.Input input : block.inputs()) {
          graph.addEdge(new Node.Var(input.assignedVariable()), Node.HasEffect.SINGLETON);
        }
      }

      // Instructions
      for (final Ir.Instruction insn : block.instructions()) {
        for (final Ir.VariableId usedVariable : usedVariableIds(insn.operation())) {
          graph.addEdge(new Node.Var(usedVariable), new Node.Var(insn.resultId()));
        }

        // Instructions with side effects must be annotated as such, to avoid
        // them disappearing.
        if (sideEffectsResult.hasSideEffect(insn)) {
          graph.addEdge(new Node.Var(insn.resultId()), Node.HasEffect.SINGLETON);
        }
      }

      // Terminators
      dataFlowFromTerminator(graph, block.terminator(), blockMap);
    }
    return graph;
  }

  private static void dataFlowFromTerminator(
      final DataFlowAnalysis.Graph<Node> graphAccum,
      final Ir.Terminator uncheckedTerminator,
      final Map<Ir.BlockId, Ir.Block> blockMap) {

    // Branch always
    if (uncheckedTerminator instanceof Ir.BranchAlways terminator) {
      dataFlowFromBranchInfo(graphAccum, terminator.branch(), blockMap);

      // Call
    } else if (uncheckedTerminator instanceof Ir.CallAndBranch terminator) {
      for (final Ir.VariableId arg : terminator.functionArguments()) {
        graphAccum.addEdge(new Node.Var(arg), Node.HasEffect.SINGLETON);
      }
      dataFlowFromBranchInfo(graphAccum, terminator.branchToAfterReturn(), blockMap);

      // Branch if
    } else {
      final Ir.BranchIf terminator = (Ir.BranchIf) uncheckedTerminator;
      graphAccum.addEdge(new Node.Var(terminator.conditionVariable()), Node.HasEffect.SINGLETON);
      dataFlowFromBranchInfo(graphAccum, terminator.branchTrue(), blockMap);
      dataFlowFromBranchInfo(graphAccum, terminator.branchFalse(), blockMap);
    }
  }

  private static void dataFlowFromBranchInfo(
      final DataFlowAnalysis.Graph<Node> graphAccum,
      final Ir.BranchInfo branchInfo,
      Map<Ir.BlockId, Ir.Block> blockMap) {

    if (branchInfo.targetBlock().equals(Ir.BlockId.RETURN)) {
      for (final Ir.VariableId variableId : branchInfo.branchInputVariables()) {
        graphAccum.addEdge(new Node.Var(variableId), Node.HasEffect.SINGLETON);
      }
      return;
    }

    final Ir.Block targetBlock = blockMap.get(branchInfo.targetBlock());
    for (int inputIdx = 0; inputIdx < targetBlock.inputs().size(); inputIdx++) {

      final Ir.VariableId blockArgumentVariable =
          inputIdx < branchInfo.branchInputVariables().size()
              ? branchInfo.branchInputVariables().get(inputIdx)
              : null;
      final Node blockParameter =
          new Node.Var(targetBlock.inputs().get(inputIdx).assignedVariable());

      if (blockArgumentVariable != null) {
        graphAccum.addEdge(new Node.Var(blockArgumentVariable), blockParameter);
      } else {
        graphAccum.addEdge(blockParameter, Node.HasEffect.SINGLETON);
      }
    }
  }

  /**
   * Determines variables used by the given operation.
   *
   * @param uncheckedOperation Operation to determine used variables from.
   * @return List of used variable ids. Can include duplicates.
   */
  public static List<Ir.VariableId> usedVariableIds(final Ir.Operation uncheckedOperation) {
    if (uncheckedOperation instanceof Ir.Operation.Constant) {
      return List.of();
    } else if (uncheckedOperation instanceof Ir.Operation.Nullary) {
      return List.of();
    } else if (uncheckedOperation instanceof Ir.Operation.Unary operation) {
      return List.of(operation.varIdx1());
    } else if (uncheckedOperation instanceof Ir.Operation.Binary operation) {
      return List.of(operation.varIdx1(), operation.varIdx2());
    } else if (uncheckedOperation instanceof Ir.Operation.PublicToSecret operation) {
      return List.of(operation.varIdx1());

    } else if (uncheckedOperation instanceof Ir.Operation.Extract operation) {
      return List.of(operation.varToExtractFrom());

    } else if (uncheckedOperation instanceof Ir.Operation.ExtractDyn operation) {
      return List.of(operation.varToExtractFrom(), operation.varOffsetFromLow());

    } else if (uncheckedOperation instanceof Ir.Operation.InsertDyn operation) {
      return List.of(
          operation.varTarget(), operation.varToInsert(), operation.varOffsetIntoTarget());

    } else if (uncheckedOperation instanceof Ir.Operation.CallDirectly operation) {
      return operation.functionArguments();
    } else {
      final Ir.Operation.Select operation = (Ir.Operation.Select) uncheckedOperation;
      return List.of(operation.varCondition(), operation.varIfTrue(), operation.varIfFalse());
    }
  }
}
