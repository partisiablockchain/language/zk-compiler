package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.math.BigInteger;
import java.util.Map;

/**
 * Utility class providing methods for creating new {@link Ir.Operation} and {@link Ir.Terminator},
 * possibly normalizing and optimizing them.
 *
 * <p>Normalizing is useful for the {@link IrPassCommonInstructionElimination} phase, as it might
 * reveal duplicated logic.
 */
public final class OperationUtility {

  private OperationUtility() {}

  /**
   * Creates a new terminator with the semantic behaviour of {@link Ir.BranchIf}, though it will
   * produce {@link Ir.BranchAlways} if they would have equivalent behaviour.
   *
   * @param isBranchOnSecret Whether the produced {@link Ir.BranchIf} is branching on a secret
   *     variable.
   * @param conditionVariable Variable to branch upon. Never null.
   * @param branchTrue Jump information for case when condition evaluates to true. Never null.
   * @param branchFalse Jump information for case when condition evaluates to false. Never null.
   * @return Newly created terminator. Never null.
   */
  public static Ir.Terminator createBranchIf(
      final boolean isBranchOnSecret,
      final Ir.VariableId conditionVariable,
      final Ir.BranchInfo branchTrue,
      final Ir.BranchInfo branchFalse) {
    // Identical branches
    if (branchTrue.equals(branchFalse)) {
      return new Ir.BranchAlways(branchTrue);
    }

    // No optimizations were possible.
    return new Ir.BranchIf(isBranchOnSecret, conditionVariable, branchTrue, branchFalse);
  }

  /**
   * Creates a new select operation, possibly optimizing it.
   *
   * @param resultType Type that operation should produce.
   * @param varCondition Variable id of the condition.
   * @param varIfTrue Variable id of the then clause.
   * @param varIfFalse Variable id of the else clause.
   * @return New operation. Never null, though possibly not an {@link Ir.Operation.Select}.
   */
  public static Ir.Operation createSelect(
      final Ir.Type resultType,
      final Ir.VariableId varCondition,
      final Ir.VariableId varIfTrue,
      final Ir.VariableId varIfFalse) {

    // Identical branches
    if (varIfTrue.equals(varIfFalse)) {
      return createIdentity(resultType, varIfTrue);

      // Condition is identical then
    } else if (varCondition.equals(varIfTrue)) {
      return createBinary(resultType, Ir.Operation.BinaryOp.BITWISE_OR, varIfTrue, varIfFalse);

      // Condition is identical else
    } else if (varCondition.equals(varIfFalse)) {
      return createBinary(resultType, Ir.Operation.BinaryOp.BITWISE_AND, varIfTrue, varIfFalse);
    }

    // No optimizations were possible.
    return new Ir.Operation.Select(varCondition, varIfTrue, varIfFalse);
  }

  /**
   * Creates an operation that preserves the type of the variable.
   *
   * @param resultType Type of variable.
   * @param variable Original variable.
   * @return Newly created operation
   */
  public static Ir.Operation createIdentity(
      final Ir.Type resultType, final Ir.VariableId variable) {
    return new Ir.Operation.Extract(variable, resultType, 0);
  }

  /**
   * Returns whether or not the given operation is an identity operation, such that {@code forall x:
   * f(x) = x}.
   *
   * @param uncheckedOperation Operation to determine for
   * @param variableTypes Types of the variables in the environment.
   * @return The variable id of the original variable, or null, if it couldn't determine that the
   *     operation is an identity operation.
   */
  public static Ir.VariableId isIdentityOperation(
      final Ir.Operation uncheckedOperation, final Map<Ir.VariableId, Ir.Type> variableTypes) {

    // Extract to identical type.
    if (uncheckedOperation instanceof Ir.Operation.Extract operation
        && operation.bitOffsetFromLow() == 0
        && operation.typeToExtract().equals(variableTypes.get(operation.varToExtractFrom()))) {
      return operation.varToExtractFrom();
    }

    return null;
  }

  /**
   * Mapping from operations to the constant result of two identical arguments.
   *
   * <p>The map only contains those operations which produce a constant value in the case where
   * {@code X o X}.
   *
   * <p>Note that idempotent operations ({@code X o X = X}) cannot be included in this map.
   */
  private static final Map<Ir.Operation.BinaryOp, BigInteger> IDENTICAL_ARGUMENTS_CONSTANT_RESULT =
      Map.of(
          Ir.Operation.BinaryOp.BITWISE_XOR,
          BigInteger.ZERO,
          Ir.Operation.BinaryOp.LESS_THAN_SIGNED,
          BigInteger.ZERO,
          Ir.Operation.BinaryOp.LESS_THAN_UNSIGNED,
          BigInteger.ZERO,
          Ir.Operation.BinaryOp.EQUAL,
          BigInteger.ONE,
          Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED,
          BigInteger.ONE,
          Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED,
          BigInteger.ONE);

  /**
   * Creates a new binary operation, possibly optimizing it.
   *
   * <p>The operation might be optimized if the argument operands are identical.
   *
   * @param resultType Type to produce for operation.
   * @param operation Specific operation to perform.
   * @param varIdx1 First argument to operation.
   * @param varIdx2 Second argument to operation.
   * @return New binop, possibly normalized.
   */
  public static Ir.Operation createBinary(
      final Ir.Type resultType,
      final Ir.Operation.BinaryOp operation,
      final Ir.VariableId varIdx1,
      final Ir.VariableId varIdx2) {

    // Left and right are equal
    if (varIdx1.equals(varIdx2)) {

      if (getBinopProperties(operation).isIdempotent()) {
        return createIdentity(resultType, varIdx1);
      }

      final BigInteger constantResult = IDENTICAL_ARGUMENTS_CONSTANT_RESULT.get(operation);
      if (constantResult != null) {
        return new Ir.Operation.Constant(resultType, constantResult);
      }
    }

    // Switch ids if operation is commutative
    return createBinary(operation, varIdx1, varIdx2);
  }

  /**
   * Creates a new binary operation, possibly normalizing it.
   *
   * <p>Normalization is accomplished by switching argument ids if the operation is {@link
   * BinopProperties#isCommutative}.
   *
   * @param operation Specific operation to perform.
   * @param varIdx1 First argument to operation.
   * @param varIdx2 Second argument to operation.
   * @return New binop, possibly normalized.
   */
  public static Ir.Operation.Binary createBinary(
      final Ir.Operation.BinaryOp operation,
      final Ir.VariableId varIdx1,
      final Ir.VariableId varIdx2) {
    final boolean shouldSwitch =
        getBinopProperties(operation).isCommutative() && varIdx1.id() > varIdx2.id();

    return shouldSwitch
        ? new Ir.Operation.Binary(operation, varIdx2, varIdx1)
        : new Ir.Operation.Binary(operation, varIdx1, varIdx2);
  }

  /**
   * Determines the <a
   * href="https://en.wikipedia.org/wiki/Category:Properties_of_binary_operations">standard
   * algebraic properties</a> for the given binop.
   *
   * @param binop Binop operand
   * @return The properties of the operand. Never null.
   */
  public static BinopProperties getBinopProperties(Ir.Operation.BinaryOp binop) {
    return BINOP_PROPERTIES.get(binop);
  }

  /**
   * Describes a few <a
   * href="https://en.wikipedia.org/wiki/Category:Properties_of_binary_operations">standard
   * algebraic properties</a> of the {@link Ir.Operation.BinaryOp}.
   *
   * @param isIdempotent Whether the operation is <a
   *     href="https://en.wikipedia.org/wiki/Idempotence">idempotent</a>: {@code idempotent(f) <=>
   *     forall x: f(x, x) = x}.
   * @param isCommutative Whether the operation is <a
   *     href="https://en.wikipedia.org/wiki/Commutative_property">commutative</a>: {@code
   *     commutative(f) <=> forall x,y: f(x, y) = f(y, x)}
   * @param identityLeft The left <a href="https://en.wikipedia.org/wiki/Identity_element">identity
   *     element</a> of the operation; possible null: {@code f(i, x) = x}
   * @param identityRight The right <a
   *     href="https://en.wikipedia.org/wiki/Identity_element">identity element</a> of the
   *     operation; possible null: {@code f(x, i) = x}
   * @param absorbingLeft The left <a
   *     href="https://en.wikipedia.org/wiki/Absorbing_element">absorbing element</a> of the
   *     operation; possible null: {@code f(0, x) = 0}
   * @param absorbingRight The right <a
   *     href="https://en.wikipedia.org/wiki/Absorbing_element">absorbing element</a> of the
   *     operation; possible null: {@code f(x, 0) = 0}
   * @see <a href="https://en.wikipedia.org/wiki/Category:Properties_of_binary_operations">standard
   *     algebraic properties</a>
   */
  public record BinopProperties(
      boolean isIdempotent,
      boolean isCommutative,
      Integer identityLeft,
      Integer identityRight,
      Integer absorbingLeft,
      Integer absorbingRight) {}

  /** Mapping of operations to their {@link BinopProperties}. Used for optimizing operations. */
  private static final Map<Ir.Operation.BinaryOp, BinopProperties> BINOP_PROPERTIES =
      Map.ofEntries(
          // Bit operations
          Map.entry(
              Ir.Operation.BinaryOp.BITWISE_AND, new BinopProperties(true, true, -1, -1, 0, 0)),
          Map.entry(
              Ir.Operation.BinaryOp.BITWISE_OR, new BinopProperties(true, true, 0, 0, -1, -1)),
          Map.entry(
              Ir.Operation.BinaryOp.BITWISE_XOR,
              new BinopProperties(false, true, 0, 0, null, null)),
          Map.entry(
              Ir.Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL,
              new BinopProperties(false, false, null, 0, 0, null)),
          Map.entry(
              Ir.Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL,
              new BinopProperties(false, false, null, 0, 0, null)),
          Map.entry(
              Ir.Operation.BinaryOp.BIT_CONCAT,
              new BinopProperties(false, false, null, null, null, null)),

          // Arithmetic
          Map.entry(
              Ir.Operation.BinaryOp.ADD_WRAPPING,
              new BinopProperties(false, true, 0, 0, null, null)),
          Map.entry(
              Ir.Operation.BinaryOp.SUBTRACT_WRAPPING,
              new BinopProperties(false, false, null, 0, null, null)),
          Map.entry(
              Ir.Operation.BinaryOp.MULT_SIGNED_WRAPPING,
              new BinopProperties(false, true, 1, 1, 0, 0)),
          Map.entry(
              Ir.Operation.BinaryOp.MULT_UNSIGNED_WRAPPING,
              new BinopProperties(false, true, 1, 1, 0, 0)),

          // Relational operations
          Map.entry(
              Ir.Operation.BinaryOp.EQUAL,
              new BinopProperties(false, true, null, null, null, null)),
          Map.entry(
              Ir.Operation.BinaryOp.LESS_THAN_SIGNED,
              new BinopProperties(false, false, null, null, null, null)),
          Map.entry(
              Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED,
              new BinopProperties(false, false, null, null, null, null)),
          Map.entry(
              Ir.Operation.BinaryOp.LESS_THAN_UNSIGNED,
              new BinopProperties(false, false, null, null, null, null)),
          Map.entry(
              Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED,
              new BinopProperties(false, false, null, null, null, null)));
}
