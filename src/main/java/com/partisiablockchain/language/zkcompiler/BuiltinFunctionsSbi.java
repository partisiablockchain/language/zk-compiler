package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Wrapper class for SBI-processing function bodies, of type {@link BuiltinFunctions.CallGenerator}.
 */
public final class BuiltinFunctionsSbi {

  private BuiltinFunctionsSbi() {}

  /**
   * {@link BuiltinFunctions.CallGenerator} for creating new secret variables. Can produce for all
   * {@link Ir.Type.SecretBinaryInteger}.
   */
  static Operand sbiFrom(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType resultOperandType) {

    final Ir.Type.SecretBinaryInteger irOutputType =
        (Ir.Type.SecretBinaryInteger) resultOperandType.expectBasic();

    return insnAccum.addCastFromPublicToSecret(
        irOutputType, tmpName, paramOperands.get(0).expectBasic());
  }

  /**
   * {@link BuiltinFunctions.CallGenerator} for loading metadata associated with secret variables.
   * Can produce for all {@link Ir.Type.SecretBinaryInteger}.
   */
  static Operand loadMetadata(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType resultOperandType) {
    final Ir.Type typeAmalgamAtInput =
        IrAmalgamUtility.typeAmalgam(resultOperandType, Ir.Type.UNIT);

    if (!(typeAmalgamAtInput instanceof Ir.Type.PublicInteger)) {
      throw new InternalCompilerException(
          "Cannot load metadata as secret type %s", resultOperandType);
    }

    final Operand.Basic operAmalgam =
        insnAccum.addUnop(
            typeAmalgamAtInput,
            tmpName,
            Ir.Operation.UnaryOp.LOAD_METADATA,
            secretVariableIdRaw(paramOperands.get(0)));

    return IrAmalgamUtility.emitAmalgamUnpacking(
        resultOperandType, operAmalgam, insnAccum, tmpName);
  }

  /** {@link BuiltinFunctions.CallGenerator} for determining the number of secret variables. */
  static Operand numSecretVariables(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType resultOperandType) {
    return insnAccum.addNullary(
        new Ir.Type.PublicInteger(32), tmpName, Ir.Operation.NullaryOp.NUM_VARIABLES);
  }

  private static Operand.Basic secretVariableIdRaw(Operand operand) {
    return operand.expectTuple().get(0).expectBasic();
  }

  /**
   * {@link BuiltinFunctions.CallGenerator} which wraps the initial argument as an array.
   *
   * <p>Used for {@link createSbiToLeBits}.
   */
  static Operand toArray(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final List<Operand> paramOperands,
      final OperandType resultOperandType) {
    return new Operand.Array(paramOperands.get(0).expectBasic(), resultOperandType.expectArray());
  }

  /**
   * {@link BuiltinFunctions.CallGenerator} which unwraps the initial array argument to its
   * underlying representation.
   *
   * <p>Used for {@link createSbiFromLeBits}.
   */
  static Operand fromArray(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final List<Operand> paramOperands,
      final OperandType resultOperandType) {
    return paramOperands.get(0).expectArray().underlyingRepresentation();
  }

  /**
   * {@link BuiltinFunctions.CallGenerator} for loading variables, including both amalgamed
   * variables, and simple variables.
   */
  static Operand loadSbi(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType resultOperandType) {
    final Ir.Type typeAmalgamAtInput =
        IrAmalgamUtility.typeAmalgam(resultOperandType, Ir.Type.UNIT_SBI);

    if (!(typeAmalgamAtInput instanceof Ir.Type.SecretBinaryInteger)) {
      throw new InternalCompilerException(
          "Cannot load variable as public type %s", resultOperandType);
    }

    final Operand.Basic operAmalgam =
        insnAccum.addUnop(
            typeAmalgamAtInput,
            tmpName,
            Ir.Operation.UnaryOp.LOAD_VARIABLE,
            secretVariableIdRaw(paramOperands.get(0)));

    return IrAmalgamUtility.emitAmalgamUnpacking(
        resultOperandType, operAmalgam, insnAccum, tmpName);
  }

  /**
   * {@link BuiltinFunctions.CallGenerator} for saving variables, including both amalgamed
   * variables, and simple variables. Saved variables will be produced by the resulting
   * meta-circuit, allowing the programmer to outputting a variable amount of variables.
   *
   * <p>This is generalized enough to support multiple variables, but will in practice follow
   * whatever usage is dictacted by {@link BuiltinFunctionsSbi#SAVE_SBI_GENERIC} type information.
   *
   * @see Ir.Operation.UnaryOp#NEXT_VARIABLE_ID
   */
  private static Operand saveSbi(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType resultOperandType) {
    final Ir.Type typeAmalgamAtInput =
        IrAmalgamUtility.typeAmalgam(resultOperandType, Ir.Type.UNIT);

    for (final Operand oper : paramOperands) {
      // Pack variable
      final Operand.Basic operandPacked =
          IrAmalgamUtility.emitAmalgamPacking(oper, insnAccum, Ir.Type.UNIT_SBI);

      if (!(operandPacked.operandType().expectBasic() instanceof Ir.Type.SecretBinaryInteger)) {
        throw new InternalCompilerException("Cannot save public operand: %s", oper);
      }

      insnAccum.addUnop(
          typeAmalgamAtInput, tmpName, Ir.Operation.UnaryOp.SAVE_VARIABLE, operandPacked);
    }

    return IrAmalgamUtility.zeroOperand(resultOperandType, tmpName);
  }

  /**
   * Create SecretVarId from usize.
   *
   * <p>Signature: {@code fn SecretVarId::new(raw_id: usize) -> SecretVarId}
   */
  @SuppressWarnings("unused")
  private static Operand secretVarIdNew(
      InstructionAccum insnAccum,
      TmpName tmpName,
      List<Operand> paramOperands,
      OperandType resultOperandType) {
    return new Operand.Tuple(paramOperands);
  }

  /**
   * Produces an operand matching given resultOperandType with all composite operands set to zero.
   * Ignores {@code paramOperands} completely.
   */
  static Operand constantZero(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final List<Operand> paramOperands,
      final OperandType resultOperandType) {
    return IrAmalgamUtility.zeroOperand(resultOperandType, tmpName);
  }

  /**
   * {@link BuiltinFunctions.CallGenerator} for determining the next iterator element for {@code
   * SecretVariableIterator}.
   */
  static Operand.Tuple variablesIteratorNext(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final List<Operand> paramOperands,
      final OperandType resultOperandType) {

    final List<Operand> operandIteratorComponents = paramOperands.get(0).expectTuple();

    final Ir.Type operandTypeItem = Ir.Type.I32;

    final Operand.Basic operandNextProducedRaw =
        insnAccum.addUnop(
            operandTypeItem,
            tmpName.with("next_variable"),
            Ir.Operation.UnaryOp.NEXT_VARIABLE_ID,
            operandIteratorComponents.get(0).expectBasic());

    final Operand.Basic operandShouldStop =
        insnAccum.addBinop(
            Ir.Type.PublicBool,
            tmpName.with("stop"),
            Ir.Operation.BinaryOp.EQUAL,
            operandNextProducedRaw,
            new Operand.Constant(0, operandTypeItem, tmpName));

    final Operand.Basic operandShouldContinue =
        insnAccum.addUnop(
            Ir.Type.PublicBool,
            tmpName.with("continue"),
            Ir.Operation.UnaryOp.BITWISE_NOT,
            operandShouldStop);

    final Operand.Tuple operandIteratorUpdated = new Operand.Tuple(List.of(operandNextProducedRaw));

    final Operand operandNextProduced =
        secretVarIdNew(insnAccum, tmpName, List.of(operandNextProducedRaw), resultOperandType);

    return new Operand.Tuple(
        List.of(operandIteratorUpdated, operandShouldContinue, operandNextProduced));
  }

  private static TypedAst.VariableId methodFor(TypedAst.Type.NameableType type, String methodName) {
    return new TypedAst.VariableId(
        TypedAst.EnvKind.FUNCTION_BUILTIN, type.typeId().fullNamePath().resolve(methodName));
  }

  /**
   * Produces a Sbi from a public integer.
   *
   * <p>Signature: {@code fn SbiN::from(value: iN) -> SbiN}
   *
   * <p>if N = 1, value is type u1.
   */
  static BuiltinFunctions.FnDefinition createSbiFromDefinition(TypedAst.Type.Sbi type) {
    return BuiltinFunctions.FnDefinition.from(
        methodFor(type, "from"),
        List.of(),
        List.of(new TypedAst.Type.Int(type.bitwidth(), type.isSigned())),
        type,
        BuiltinFunctionsSbi::sbiFrom,
        false);
  }

  /**
   * Converts Sbi to an array of Sbi1.
   *
   * <p>Signature: {@code fn SbiN::to_le_bits(self) -> [Sbi1; N]}
   */
  private static BuiltinFunctions.FnDefinition createSbiToLeBits(TypedAst.Type.Sbi type) {
    return BuiltinFunctions.FnDefinition.from(
        methodFor(type, "to_le_bits"),
        List.of(),
        List.of(type),
        new TypedAst.Type.Array(TypedAst.Type.Sbi.BOOL, type.bitwidth()),
        BuiltinFunctionsSbi::toArray,
        false);
  }

  /**
   * Converts an array of Sbi1 to Sbi.
   *
   * <p>Signature: {@code fn SbiN::from_le_bits(bits: [Sbi1; N]) -> SbiN}
   */
  private static BuiltinFunctions.FnDefinition createSbiFromLeBits(TypedAst.Type.Sbi type) {
    return BuiltinFunctions.FnDefinition.from(
        methodFor(type, "from_le_bits"),
        List.of(),
        List.of(new TypedAst.Type.Array(TypedAst.Type.Sbi.BOOL, type.bitwidth())),
        type,
        BuiltinFunctionsSbi::fromArray,
        false);
  }

  /**
   * Aggregation creation for SbiN methods.
   *
   * @param type Type to create methods for.
   * @return Stream of function definitions.
   */
  static Stream<BuiltinFunctions.FnDefinition> createSbiMethods(TypedAst.Type.Sbi type) {
    return Stream.of(
        createSbiFromDefinition(type), createSbiToLeBits(type), createSbiFromLeBits(type));
  }

  /** Pbc sdk: pbc_zk::Sbi32. */
  static TypeCheck.TypeAliasInfo createSbiTypeDefinition(TypedAst.Type.NameableType type) {
    return BuiltinFunctionsRustStd.typeAlias(type.typeId(), type);
  }

  private static final TypedAst.TypeId ID_SECRET_VAR_ID =
      TypedAst.TypeId.newFor("pbc_zk", "SecretVarId");

  private static final TypedAst.Type TYPE_VARIABLE_ID =
      new TypedAst.Type.NamedType(ID_SECRET_VAR_ID, List.of());

  /** The struct definition of {@code ID_SECRET_VAR_ID}. */
  public static final TypedAst.StructDefinition STRUCT_VARIABLE_ID =
      new TypedAst.StructDefinition(
          ID_SECRET_VAR_ID,
          List.of(),
          List.of(new TypedAst.StructDefinition.StructField("raw_id", TypedAst.Type.Int.USIZE)));

  /**
   * Retrieves secret variable with the given public id and type.
   *
   * <p>Signature: {@code fn load_sbi<T: SecretBinary>(id: i32) -> T}
   */
  public static final BuiltinFunctions.FnDefinition LOAD_SBI_GENERIC =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(TypedAst.EnvKind.FUNCTION_BUILTIN, "pbc_zk", "load_sbi"),
          List.of("SecretBinaryT"),
          List.of(TYPE_VARIABLE_ID),
          new TypedAst.Type.NamedType(TypedAst.TypeId.newFor("SecretBinaryT"), List.of()),
          BuiltinFunctionsSbi::loadSbi,
          false);

  /**
   * Retrieves metadata for the given public id and type.
   *
   * <p>Signature: {@code fn load_metadata<T: Public>(id: i32) -> T}
   */
  public static final BuiltinFunctions.FnDefinition LOAD_METADATA_GENERIC =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(TypedAst.EnvKind.FUNCTION_BUILTIN, "pbc_zk", "load_metadata"),
          List.of("PublicT"),
          List.of(TYPE_VARIABLE_ID),
          new TypedAst.Type.NamedType(TypedAst.TypeId.newFor("PublicT"), List.of()),
          BuiltinFunctionsSbi::loadMetadata,
          false);

  /**
   * Retrieves the number of secret variables.
   *
   * <p>Signature: {@code fn num_secret_variables() -> i32}
   */
  private static final BuiltinFunctions.FnDefinition NUM_SECRET_VARIABLES =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(
              TypedAst.EnvKind.FUNCTION_BUILTIN, "pbc_zk", "num_secret_variables"),
          List.of(),
          List.of(),
          TypedAst.Type.Int.USIZE,
          BuiltinFunctionsSbi::numSecretVariables,
          false);

  private static final TypedAst.TypeId ID_SECRET_VARIABLE_ITERATOR =
      TypedAst.TypeId.newFor("pbc_zk", "SecretVariableIterator");

  /**
   * Produces iterator for iterating all current variables.
   *
   * <p>Signature: {@code fn secret_variables_ids() -> SecretVariableIterator}
   */
  private static final BuiltinFunctions.FnDefinition SECRET_VARIABLE_ITERATOR_CONSTRUCT =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(
              TypedAst.EnvKind.FUNCTION_BUILTIN, "pbc_zk", "secret_variable_ids"),
          List.of(),
          List.of(),
          new TypedAst.Type.NamedType(ID_SECRET_VARIABLE_ITERATOR, List.of()),
          BuiltinFunctionsSbi::constantZero,
          false);

  private static final List<Integer> SBI_TYPE_SIZES = BuiltinFunctionsRustStd.INTEGER_SIZES;

  static Stream<TypedAst.Type.Sbi> sbiTypes() {
    return Stream.concat(
        SBI_TYPE_SIZES.stream().map(i -> new TypedAst.Type.Sbi(i, false)),
        SBI_TYPE_SIZES.stream().map(i -> new TypedAst.Type.Sbi(i, true)));
  }

  /** The struct definition of {@code SecretVariableIterator}. */
  public static final TypedAst.StructDefinition SECRET_VARIABLE_ITERATOR =
      new TypedAst.StructDefinition(
          ID_SECRET_VARIABLE_ITERATOR,
          List.of(),
          List.of(
              new TypedAst.StructDefinition.StructField("current_id", TypedAst.Type.Int.USIZE)));

  private static final BuiltinFunctions.FnDefinition SECRET_VARIABLE_ITERATOR_TO_ITERATOR =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(
              TypedAst.EnvKind.FUNCTION_BUILTIN, "pbc_zk", "SecretVariableIterator", "into_iter"),
          List.of(),
          List.of(new TypedAst.Type.NamedType(ID_SECRET_VARIABLE_ITERATOR, List.of())),
          new TypedAst.Type.NamedType(ID_SECRET_VARIABLE_ITERATOR, List.of()),
          BuiltinFunctionsRustStd::identity,
          false);

  private static final BuiltinFunctions.FnDefinition SECRET_VARIABLE_ITERATOR_NEXT =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(
              TypedAst.EnvKind.FUNCTION_BUILTIN, "pbc_zk", "SecretVariableIterator", "next"),
          List.of(),
          List.of(new TypedAst.Type.NamedType(ID_SECRET_VARIABLE_ITERATOR, List.of())),
          new TypedAst.Type.Tuple(
              List.of(
                  new TypedAst.Type.NamedType(ID_SECRET_VARIABLE_ITERATOR, List.of()),
                  TypedAst.Type.Int.BOOL,
                  TYPE_VARIABLE_ID)),
          BuiltinFunctionsSbi::variablesIteratorNext,
          false);

  private static final BuiltinFunctions.FnDefinition SAVE_SBI_GENERIC =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(TypedAst.EnvKind.FUNCTION_BUILTIN, "pbc_zk", "save_sbi"),
          List.of("SecretBinaryT"),
          List.of(new TypedAst.Type.NamedType(TypedAst.TypeId.newFor("SecretBinaryT"), List.of())),
          TypedAst.Type.Tuple.UNIT,
          BuiltinFunctionsSbi::saveSbi,
          false);

  private static final BuiltinFunctions.FnDefinition SECRET_VAR_ID_FROM =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(
              TypedAst.EnvKind.FUNCTION_BUILTIN, "pbc_zk", "SecretVarId", "new"),
          List.of(),
          List.of(TypedAst.Type.Int.USIZE),
          TYPE_VARIABLE_ID,
          BuiltinFunctionsSbi::secretVarIdNew,
          false);

  /** List of functions defined within the {@code pbc_zk} builtin module. */
  static final List<BuiltinFunctions.FnDefinition> FUNCTIONS_LIST =
      Stream.<Stream<BuiltinFunctions.FnDefinition>>of(
              Stream.of(
                  // General secret
                  SECRET_VAR_ID_FROM,
                  NUM_SECRET_VARIABLES,
                  LOAD_METADATA_GENERIC,
                  SECRET_VARIABLE_ITERATOR_CONSTRUCT,
                  SECRET_VARIABLE_ITERATOR_TO_ITERATOR,
                  SECRET_VARIABLE_ITERATOR_NEXT,

                  // Sbi/Sbu
                  LOAD_SBI_GENERIC,
                  SAVE_SBI_GENERIC),
              sbiTypes().flatMap(BuiltinFunctionsSbi::createSbiMethods))
          .flatMap(Function.identity())
          .toList();

  /** List of structs defined within the {@code pbc_zk} builtin module. */
  static final List<TypedAst.StructDefinition> STRUCTS =
      List.of(SECRET_VARIABLE_ITERATOR, STRUCT_VARIABLE_ID);

  /** List of types defined within the {@code pbc_zk} builtin module. */
  static final List<TypeCheck.TypeAliasInfo> TYPES =
      sbiTypes().map(BuiltinFunctionsSbi::createSbiTypeDefinition).toList();
}
