package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;
import com.google.errorprone.annotations.FormatString;

/**
 * Exception that occurs in the case of an internal inconsistency in the compiler, indicating for
 * example a missing type check.
 */
public final class InternalCompilerException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  /**
   * Creates a new exception with the given message.
   *
   * @param message Format string.
   * @param args Format objects.
   */
  @FormatMethod
  public InternalCompilerException(@FormatString final String message, final Object... args) {
    super(message.formatted(args));
  }
}
