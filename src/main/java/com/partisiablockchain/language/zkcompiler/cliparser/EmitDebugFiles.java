package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcompiler.PassInformation;
import com.partisiablockchain.language.zkcompiler.TranslationHooks;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.ir.IrPretty;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitPretty;
import java.nio.file.Path;

/**
 * Class responsible for emitting debugging information for the compilation, based on the specified
 * emit options.
 */
public record EmitDebugFiles(
    boolean emitInitialIr,
    boolean emitFinalIr,
    boolean emitUniqueIrs,
    boolean emitMetaCircuit,
    Path outDir,
    String debugFileBaseName)
    implements TranslationHooks {

  /**
   * Constructs an instance of the EmitDebugFiles class, capable of emitting debugging information
   * for the compilation.
   *
   * @param emitInitialIr decides whether debugging files for the initial ir should be emitted.
   * @param emitFinalIr decides whether debugging files for the final ir should be emitted.
   * @param emitUniqueIrs decides whether debugging files for all ir passes should be emitted.
   * @param emitMetaCircuit decides whether debugging files for the metacircuit should be emitted.
   * @param outDir the directory to output the debugging files to.
   * @param debugFileBaseName the base name of the input file, with no extension.
   */
  public EmitDebugFiles {
    requireNonNull(outDir);
    requireNonNull(debugFileBaseName);
  }

  @Override
  public void debugIrInitial(Ir.Circuit circuit) {
    if (emitInitialIr()) {
      emitIntermediateFormat("0-ir-initial", IrPretty.pretty(circuit));
    }
  }

  @Override
  public void debugMetaCircuit(MetaCircuit circuit) {
    if (emitMetaCircuit()) {
      emitIntermediateFormat("metacircuit", MetaCircuitPretty.pretty(circuit));
    }
  }

  @Override
  public void debugIrPass(
      PassInformation passInfo, Ir.Circuit circuit, boolean isDifferentFromPrevious) {
    final boolean shouldEmitBecauseDifferent = emitUniqueIrs() && isDifferentFromPrevious;
    final boolean shouldEmitBecauseFinal = emitFinalIr() && passInfo.isFinalPass();
    if (shouldEmitBecauseDifferent || shouldEmitBecauseFinal) {
      final String passName = passInfo.isFinalPass() ? "ir-final" : passInfo.irPass().getName();
      emitIntermediateFormat(
          "%d-%s".formatted(passInfo.passNumber(), passName), IrPretty.pretty(circuit));
    }
  }

  /**
   * Emits the intermediate format to the specified filename in the specified directory.
   *
   * @param irPassName name of the pass.
   * @param content pretty representation of intermediate format.
   */
  void emitIntermediateFormat(String irPassName, String content) {
    String intermediateFileBasename = String.format("%s-%s", debugFileBaseName(), irPassName);
    FileManager.writeFileInDir(
        outDir(), intermediateFileBasename, FileManager.INTERMEDIATE_EXTENSION, content);
  }
}
