package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/** Utility class for abstraction over file management. */
public final class FileManager {

  /** File extension for bytecode. */
  public static String BYTE_CODE_EXTENSION = ".zkbc";

  /** File extension for intermediate formats. */
  public static String INTERMEDIATE_EXTENSION = ".dbg";

  /** FIle extension for linked bytecode and wasm file. */
  public static String LINKED_EXTENSION = ".zkwa";

  /** Magic byte indicating start of wasm section in linked file. */
  public static byte WASM_MAGIC_BYTE = 0x02;

  /** Magic byte indicating start of zk section in linked file. */
  public static byte ZK_MAGIC_BYTE = 0x03;

  private FileManager() {}

  /**
   * Tries to create a directory and returns path to it. Throws a runtime error, if an error occurs
   * trying to create the directories.
   *
   * @param path path of directory.
   * @return the path to the directory
   */
  public static Path createDirectory(Path path) {
    return ExceptionConverter.call(
        () -> Files.createDirectories(path),
        String.format("An error occurred trying to create the directory %s", path));
  }

  /**
   * Reads the contents of a file as a String.
   *
   * @param pathToFile path of the file.
   * @return String representation of the contents in the file.
   */
  public static String readFileAsString(Path pathToFile) {
    return ExceptionConverter.call(
        () -> Files.readString(pathToFile),
        String.format("An error occurred while trying to read the file %s", pathToFile));
  }

  /**
   * Writes the provided bytes to the file present at the specified path.
   *
   * @param pathOfFile path of the file to write to.
   * @param content the bytes to write to the file.
   */
  public static void writeToFile(Path pathOfFile, byte[] content) {
    ExceptionConverter.call(
        () -> {
          // If parent exists, ensure directories are created.
          final Path parent = pathOfFile.getParent();

          if (parent != null) {
            Files.createDirectories(parent);
          }
          Files.write(pathOfFile, content);
          return null;
        },
        String.format("An error occurred while writing to the file %s", pathOfFile));
  }

  /**
   * Writes a string to a file in a directory.
   *
   * @param dir the path to the directory.
   * @param filenameBase the base name of the file to write.
   * @param filenameExtension the extension of the file to write.
   * @param content the contents to write to the file.
   */
  public static void writeFileInDir(
      Path dir, String filenameBase, String filenameExtension, String content) {
    writeFileInDir(dir, filenameBase, filenameExtension, content.getBytes(StandardCharsets.UTF_8));
  }

  /**
   * Writes bytes to a file in a directory.
   *
   * @param dir the path to the directory.
   * @param filenameBase the base name of the file to write.
   * @param filenameExtension the extension of the file to write.
   * @param content the contents to write to the file.
   */
  public static void writeFileInDir(
      Path dir, String filenameBase, String filenameExtension, byte[] content) {
    Path pathOfFile = Path.of(filenameBase + filenameExtension);
    writeToFile(dir.resolve(pathOfFile), content);
  }

  /**
   * Replaces file extension from given path.
   *
   * @param path Path to replace in.
   * @param newExtension New extension to use instead of already existing path.
   * @return Newly created path.
   */
  public static Path replaceExtension(Path path, String newExtension) {
    final String filename = stripExtensionAsString(path.getFileName().toString()) + newExtension;
    return path.getParent().resolve(filename);
  }

  /**
   * Strips the file extension of a given filename.
   *
   * @param filename String representation of filename to strip extension from.
   * @return String representation of filename, with extension removed
   */
  static String stripExtensionAsString(String filename) {
    int indexOfLastDot = filename.lastIndexOf('.');
    int indexOfLastSlash = Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\'));

    // Only strip extension if it is after all slashes.
    if (indexOfLastDot != -1 && indexOfLastDot > indexOfLastSlash + 1) {
      return filename.substring(0, indexOfLastDot);
    } else {
      return filename;
    }
  }
}
