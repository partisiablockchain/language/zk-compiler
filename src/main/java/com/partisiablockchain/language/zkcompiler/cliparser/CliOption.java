package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.zkcompiler.ResourceList;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Representation of an option in the command-line invocation. Provides the implementations of the
 * options as well. Options alter the behaviour of a command invocation, by specifying properties,
 * which the command is to be performed subject to. Options are specified in traditional convention,
 * using '-' or '--' - see the help message for more information.
 */
sealed interface CliOption {

  /**
   * Interface for sub-options, responsible for displaying the help message. A sub-option is an
   * option which is specified as argument to its parent option. Thus, they enable options, to only
   * accept specific arguments.
   */
  @Immutable
  interface SubOption {

    /**
     * Returns the name of a suboption as lower case, replacing '_' with '-'.
     *
     * @return name of a suboption.
     */
    default String getName() {
      return this.toString().toLowerCase(Locale.ENGLISH).replace("_", "-");
    }
  }

  /**
   * Parses a string argument into an OptionType. If the argument is unrecognizable, parse it as the
   * input file OptionType.
   *
   * @param arg argument to parse.
   * @return parsed option type.
   */
  static OptionType parseFromArgs(String arg) {
    String argNoDash = arg.substring(arg.lastIndexOf("-") + 1);
    return Arrays.stream(OptionType.values())
        .filter(o -> o.shortname.equals(argNoDash) || o.longname.equals(argNoDash))
        .findAny()
        .orElse(OptionType.INPUT_FILE);
  }

  /**
   * Parses a suboption from a string argument and throws an error if the argument was invalid.
   *
   * @param suboptions collection of valid suboptions.
   * @param optionName the option name.
   * @param errorPrefix prefix for presenting error message.
   * @param <T> Type of suboption.
   * @return the parsed suboption.
   */
  static <T extends SubOption> T parseSubOption(
      Collection<T> suboptions, String optionName, String errorPrefix) {
    return suboptions.stream()
        .filter(t -> optionName.equals(t.getName()))
        .findFirst()
        .orElseThrow(
            () ->
                new CliParseException(
                    String.format("Invalid option specified for %s: %s", errorPrefix, optionName)));
  }

  /**
   * Returns the type of option.
   *
   * @return the type of the option.
   */
  CliOption.OptionType getType();

  static String createHelpText(
      TerminalColors colors,
      String shortname,
      String longname,
      String argument,
      String description,
      List<SubOption> suboptions) {
    final boolean hasShortname = !shortname.isBlank();
    final boolean hasLongname = !longname.isBlank();

    if (!hasLongname && !hasShortname) {
      return "";
    } else {
      final String optionName;
      final StringBuilder builder = new StringBuilder("    ");
      if (hasShortname && hasLongname) {
        optionName = String.format("-%s, --%s", shortname, longname);
      } else if (hasShortname) {
        optionName = String.format("-%s", shortname);
      } else {
        optionName = String.format("--%s", longname);
      }

      // Set the color of the option names to green.
      builder.append(colors.options()).append(optionName).append(colors.normal());
      if (!argument.isBlank()) {
        builder.append(" ").append(argument);
      }
      if (!suboptions.isEmpty()) {
        builder.append(" [");
        for (Iterator<SubOption> iter = suboptions.iterator(); iter.hasNext(); ) {
          builder.append(iter.next().getName());
          if (iter.hasNext()) {
            builder.append("|");
          }
        }
        builder.append("]");
      }
      builder.append(": ").append(description);
      return builder.append(String.format("%n")).toString();
    }
  }

  /** Possible options in the command-line invocation. */
  enum OptionType {
    /** Display the help message. */
    HELP("h", "help", "", "Display this help message."),
    /** Show current version of compiler. */
    VERSION("v", "version", "", "Show the current version of the compiler."),
    /** Option representing the input file. */
    INPUT_FILE("", "", "", ""),
    /** Option representing the output file. */
    OUTPUT_FILE("o", "", "<file>", "Write output to file."),
    /** Option representing the output directory. */
    OUTPUT_DIR("odir", "", "<dir>", "Write output to directory."),
    /** Option representing an emit-option. */
    EMIT(
        "",
        "emit",
        "",
        "Emit intermediate formats.",
        List.of(
            Emit.EmitType.IR_INITIAL,
            Emit.EmitType.IR_FINAL,
            Emit.EmitType.METACIRCUIT,
            Emit.EmitType.IR_ALL)),
    /** Specify terminal color configuration. */
    COLOR(
        "",
        "color",
        "",
        "Colorize terminal output.",
        List.of(Color.ColorOption.AUTO, Color.ColorOption.ALWAYS, Color.ColorOption.NEVER)),
    ;

    private final String shortname;
    private final String longname;
    private final String description;
    private final String argument;

    @SuppressWarnings("ImmutableEnumChecker")
    private final List<SubOption> subcommands;

    OptionType(String shortname, String longname, String argument, String description) {
      this(shortname, longname, argument, description, List.of());
    }

    OptionType(
        String shortname,
        String longname,
        String argument,
        String description,
        List<SubOption> subcommands) {
      this.shortname = shortname;
      this.longname = longname;
      this.description = description;
      this.argument = argument;
      this.subcommands = Collections.unmodifiableList(subcommands);
    }

    /**
     * Returns a String representation of the option.
     *
     * @return Formatted String representation of the current option.
     */
    public String showOption(TerminalColors colors) {
      return createHelpText(colors, shortname, longname, argument, description, subcommands);
    }

    /**
     * Returns string representation of option type.
     *
     * @param type type of option.
     * @return String representation of option, as written in the command-line execution.
     */
    public static String getAsString(CliOption.OptionType type) {
      if (type == INPUT_FILE) {
        return "input";
      }
      final String shortname = type.shortname;
      if (!shortname.isBlank()) {
        return "-" + shortname;
      } else {
        return "--" + type.longname;
      }
    }
  }

  /** Represents the options for emitting intermediate formats. */
  record Emit(EmitType emitType) implements CliOption {

    public static Emit parseFromArgs(String argName) {
      return new CliOption.Emit(parseSubOption(List.of(EmitType.values()), argName, "--emit"));
    }

    @Override
    public CliOption.OptionType getType() {
      return CliOption.OptionType.EMIT;
    }

    /** Possible intermediate format emissions. */
    @Immutable
    enum EmitType implements SubOption {
      IR_INITIAL,
      IR_FINAL,
      METACIRCUIT,
      IR_ALL;
    }
  }

  /** Represents the input file specified in the command-line invocation. */
  record InputFile(Path path) implements CliOption {

    /**
     * Reads and returns String representation of the source code in the file specified by the path.
     *
     * @return Content of input file.
     */
    public String readSourceCode() {
      return FileManager.readFileAsString(path);
    }

    @Override
    public CliOption.OptionType getType() {
      return CliOption.OptionType.INPUT_FILE;
    }
  }

  /** Represents the output file option in the command-line invocation. */
  record OutputFile(Path path) implements CliOption {

    /**
     * Constructs a new output file option with output location specified by String representation.
     *
     * @param pathName String representation of the output file.
     */
    public OutputFile(String pathName) {
      this(Path.of(pathName));
    }

    @Override
    public CliOption.OptionType getType() {
      return CliOption.OptionType.OUTPUT_FILE;
    }
  }

  /** Represents the output directory option. */
  record OutputDir(Path path) implements CliOption {

    /**
     * Constructs a new output directory option from a specified String representation of the output
     * directory.
     *
     * @param value String representation of desired output directory path.
     */
    public OutputDir(String value) {
      this(Path.of(value));
    }

    @Override
    public CliOption.OptionType getType() {
      return CliOption.OptionType.OUTPUT_DIR;
    }
  }

  /** Allows users to specify the terminal color configuration. */
  record Color(ColorOption colorOption) implements CliOption {

    @Immutable
    enum ColorOption implements SubOption {
      /** Let the Parser determine if colors should be used automatically. */
      AUTO,
      /** Always use colors, regardless of terminal support. */
      ALWAYS,
      /** Never use colors, regardless of terminal support. */
      NEVER,
      ;
    }

    public static Color parseFromArgs(String argName) {
      return new Color(parseSubOption(List.of(ColorOption.values()), argName, "--color"));
    }

    @Override
    public OptionType getType() {
      return OptionType.COLOR;
    }
  }

  /** Displays the help message. */
  record Help() implements CliOption {

    @Override
    public OptionType getType() {
      return OptionType.HELP;
    }

    public void run(PrintStream out, TerminalColors colors) {
      out.println(
          colors.usage()
              + "USAGE:"
              + colors.normal()
              + " java -jar zkcompiler.jar [options]* [input] [options]*");
      out.println(colors.options() + "OPTIONS:" + colors.normal());
      for (CliOption.OptionType optionType : CliOption.OptionType.values()) {
        out.printf(optionType.showOption(colors));
      }
    }
  }

  /** Display the current version of the zk-compiler. */
  record Version() implements CliOption {

    public void run(PrintStream out) {
      ResourceList.consumePropertiesFromResources(
          (fileName, properties) -> {
            final Object versionNumber = properties.get("git.build.version");
            out.printf("Current compiler version: %s%n", versionNumber);
          },
          Pattern.compile(".*zkcompiler-git\\.properties"));
    }

    @Override
    public OptionType getType() {
      return OptionType.VERSION;
    }
  }
}
