package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ProgramInput;
import com.partisiablockchain.language.zkcompiler.TranslationConfig;
import com.partisiablockchain.language.zkcompiler.TranslationHooks;
import com.partisiablockchain.language.zkcompiler.ZkCompiler;
import java.io.PrintStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Command-line interface parser, capable of parsing command line arguments and invoking the
 * zk-compiler with the parsed arguments. The parsed arguments, called options, allow altering the
 * behaviour of the zk-compiler invocation.
 */
public final class CliParser {
  private final PrintStream err;
  private final TerminalFeatures terminalFeatures;

  /**
   * Main method for parsing and executing the parsed command. Uses System.out as output stream to
   * write to terminal.
   *
   * @param args command-line specified arguments.
   */
  public static void main(String[] args) {
    parseAndRun(
        new PrintStream(System.out),
        new PrintStream(System.err),
        Arrays.asList(args),
        new SystemInfo.OsDetection(),
        System::exit);
  }

  /**
   * Parses the command line arguments and executes the program, subject to the specified options.
   *
   * @param out stream to write output to. Use System.out to display in terminal.
   * @param err stream to write errors to.
   * @param args command line arguments.
   * @param systemInfo information about the running system.
   * @param terminationStrategy procedure by which to terminate the program.
   */
  static void parseAndRun(
      PrintStream out,
      PrintStream err,
      List<String> args,
      SystemInfo systemInfo,
      TerminationStrategy terminationStrategy) {
    var terminalFeatures = new TerminalFeatures(systemInfo);
    CliParser parser = new CliParser(err, terminalFeatures);
    CliOptions options;
    try {
      options = parser.parseCli(args);
      if (options.contains(CliOption.OptionType.HELP)) {
        new CliOption.Help().run(out, options.colors());
        return;
      }
      if (options.contains(CliOption.OptionType.VERSION)) {
        new CliOption.Version().run(out);
        return;
      }
      Path outputPath;

      if (options.contains(CliOption.OptionType.INPUT_FILE)) {
        List<CliOption.InputFile> inputFiles = options.inputFiles();

        outputPath = options.determineOutputFile();
        List<CliOption.Emit> emitOptions = options.emitOptions();
        parser.compile(inputFiles, emitOptions, outputPath, options.colors());
      } else {
        throw new CliParseException("No input filename given");
      }

    } catch (CliParseException e) {
      TerminalColors colors =
          terminalFeatures.supportsColor() ? TerminalColors.ANSI : TerminalColors.NONE;
      // If an error happens during parsing, print an error message and display the help message.
      err.println(
          colors.error()
              + "Error:"
              + String.format("%s%n", colors.normal())
              + e.getMessage()
              + ". See help message below:");
      new CliOption.Help().run(out, colors);
      terminationStrategy.exit(1);
    } catch (Exception e) {
      TerminalColors colors =
          terminalFeatures.supportsColor() ? TerminalColors.ANSI : TerminalColors.NONE;
      String errorMessage =
          colors.error() + "Compilation error" + colors.normal() + ": " + e.getMessage();
      err.println(errorMessage);
      terminationStrategy.exit(1);
    }
  }

  /**
   * Compiles the input file and outputs the result to the specified path. Also emits intermediate
   * formats if specified.
   *
   * @param inputFiles input files to compile together.
   * @param emitOptions options for emission of intermediate formats.
   * @param outputPath the path to the output file.
   */
  private void compile(
      List<CliOption.InputFile> inputFiles,
      List<CliOption.Emit> emitOptions,
      Path outputPath,
      TerminalColors colors) {
    TranslationConfig config = createTranslationConfig(emitOptions, outputPath, colors);

    final ProgramInput programInput =
        new ProgramInput(
            inputFiles.stream()
                .map(x -> new ProgramInput.FileInput(x.path(), x.readSourceCode()))
                .toList());

    byte[] zkBytes = ZkCompiler.compileRustToMetaCircuitBytecode(programInput, config);

    FileManager.writeToFile(outputPath, zkBytes);
  }

  private TranslationConfig createTranslationConfig(
      List<CliOption.Emit> emitOptions, Path outputPath, TerminalColors colors) {
    return new TranslationConfig(
        createTranslationHooks(emitOptions, outputPath), err, colors.colorful(), false);
  }

  TranslationHooks createTranslationHooks(List<CliOption.Emit> emitOptions, Path outputPath) {
    List<CliOption.Emit.EmitType> emitTypes =
        emitOptions.stream().map(CliOption.Emit::emitType).toList();

    boolean irInitial = emitTypes.contains(CliOption.Emit.EmitType.IR_INITIAL);
    boolean irFinal = emitTypes.contains(CliOption.Emit.EmitType.IR_FINAL);
    boolean irAll = false;
    boolean metaCirc = emitTypes.contains(CliOption.Emit.EmitType.METACIRCUIT);

    if (emitTypes.contains(CliOption.Emit.EmitType.IR_ALL)) {
      irInitial = true;
      irFinal = true;
      irAll = true;
    }

    // outputPath is the path to the file, so getParent to get the directory.
    final Path outDir = Path.of(".").resolve(outputPath).getParent();

    // Retrieve the filename, modify it with new extension.
    final String filename = outputPath.getFileName().toString();
    final String filenameNoExtension = FileManager.stripExtensionAsString(filename);

    return new EmitDebugFiles(irInitial, irFinal, irAll, metaCirc, outDir, filenameNoExtension);
  }

  /**
   * Constructs a new CliParser.
   *
   * @param err the output stream to write errors to.
   * @param terminalFeatures the features supported by the terminal
   */
  public CliParser(PrintStream err, TerminalFeatures terminalFeatures) {
    this.err = err;
    this.terminalFeatures = terminalFeatures;
  }

  /**
   * Parses the command-line arguments into a collection of options and throws errors if the
   * arguments were syntactically incorrect.
   *
   * @param args list of command-line arguments
   * @return List of parsed options wrapped in CliOptions object with several utility methods.
   * @throws CliParseException in case of illegal input.
   */
  CliOptions parseCli(List<String> args) {
    List<CliOption> options = new ArrayList<>();
    if (args.isEmpty()) {
      options.add(new CliOption.Help());
    }

    final Iterator<String> argIter = args.iterator();
    while (argIter.hasNext()) {
      options.add(parseCliArgument(argIter));
    }

    final CliOptions parsedOptions = new CliOptions(options, terminalFeatures);
    return parsedOptions;
  }

  private static CliOption parseCliArgument(Iterator<String> argIter) {
    final String nextArg = argIter.next();
    final CliOption.OptionType option = CliOption.parseFromArgs(nextArg);

    return switch (option) {
      case OUTPUT_FILE -> parseOutput(argIter);
      case OUTPUT_DIR -> parseOutputDir(argIter);
      case EMIT -> parseEmitOption(argIter);
      case COLOR -> parseColorOption(argIter);
      case HELP -> new CliOption.Help();
      case VERSION -> new CliOption.Version();
      default -> new CliOption.InputFile(Path.of(nextArg));
    };
  }

  private static CliOption.Emit parseEmitOption(Iterator<String> argIter) {
    return CliOption.Emit.parseFromArgs(getNextOrError(argIter, CliOption.OptionType.EMIT));
  }

  private static CliOption.OutputFile parseOutput(Iterator<String> iter) {
    final String outputFileName = getNextOrError(iter, CliOption.OptionType.OUTPUT_FILE);
    return new CliOption.OutputFile(outputFileName);
  }

  private static CliOption.OutputDir parseOutputDir(Iterator<String> argIter) {
    final String outputDirName = getNextOrError(argIter, CliOption.OptionType.OUTPUT_DIR);
    return new CliOption.OutputDir(outputDirName);
  }

  private static CliOption.Color parseColorOption(Iterator<String> argIter) {
    return CliOption.Color.parseFromArgs(getNextOrError(argIter, CliOption.OptionType.COLOR));
  }

  private static String getNextOrError(Iterator<String> iter, CliOption.OptionType option) {
    if (!iter.hasNext()) {
      throw new CliParseException(
          "Missing argument for option " + CliOption.OptionType.getAsString(option));
    }
    return iter.next();
  }

  /** Strategy that defines how the program should be terminated. */
  @FunctionalInterface
  interface TerminationStrategy {
    /**
     * Terminates the program provided an exit status.
     *
     * @param exitStatus The exit status associated with the termination.
     */
    void exit(int exitStatus);
  }
}
