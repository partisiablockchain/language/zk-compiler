package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;

/** Wraps a list of CliOption and provides utility methods for accessing and validating these. */
record CliOptions(List<CliOption> options, TerminalFeatures terminalFeatures) {

  boolean contains(CliOption.OptionType optionType) {
    return options.stream().map(CliOption::getType).anyMatch(Predicate.isEqual(optionType));
  }

  /**
   * Finds the only option with the specified option type, and returns null of no such option
   * existed. If more than one options were present, this throws an error.
   *
   * @param clazz the option type to get.
   * @return only option with provided type or null if none were present. Throws error if several
   *     options existed.
   */
  private <T extends CliOption> T findOnly(Class<T> clazz) {
    final List<T> matchingOptions = findAll(clazz);
    if (matchingOptions.isEmpty()) {
      return null;
    }
    if (matchingOptions.size() > 1) {
      throw new CliParseException(
          "Expected at most one %s option, but was given %d",
          clazz.getSimpleName(), matchingOptions.size());
    }
    return matchingOptions.get(0);
  }

  /**
   * Finds all options of the specified class.
   *
   * @param clazz the class of the option.
   * @param <T> type of CliOption.
   * @return list of found options with type T.
   */
  private <T extends CliOption> List<T> findAll(Class<T> clazz) {
    // Get everything assignable from clazz and cast to the actual type.
    return options.stream()
        .filter(t -> clazz.isAssignableFrom(t.getClass()))
        .map(clazz::cast)
        .toList();
  }

  //// Utility methods for verifying and getting options.

  List<CliOption.InputFile> inputFiles() {
    final List<CliOption.InputFile> inputFiles = findAll(CliOption.InputFile.class);
    final List<String> missingFileNames =
        inputFiles.stream()
            .map(CliOption.InputFile::path)
            .filter(p -> !Files.exists(p))
            .map(Object::toString)
            .toList();
    if (missingFileNames.size() > 0) {
      throw new CliParseException(
          "Could not find some of the provided source files: %s"
              .formatted(String.join(", ", missingFileNames)));
    }
    return inputFiles;
  }

  CliOption.OutputFile outputFile() {
    return findOnly(CliOption.OutputFile.class);
  }

  CliOption.OutputDir outputDir() {
    return findOnly(CliOption.OutputDir.class);
  }

  List<CliOption.Emit> emitOptions() {
    return findAll(CliOption.Emit.class);
  }

  /** Find provided color option or default to AUTO. */
  TerminalColors colors() {
    CliOption.Color colorOption = findOnly(CliOption.Color.class);
    CliOption.Color.ColorOption color =
        colorOption != null ? colorOption.colorOption() : CliOption.Color.ColorOption.AUTO;
    return determineColors(color, terminalFeatures.supportsColor());
  }

  /**
   * Determines the exact location of the file to write the compilation output to depending on the
   * specified options. If the output file is given in the options, this is trivial. If outdir is
   * specified, this resolves the directory with the file name, and returns the path hereof. If both
   * output options are specified, this throws an error.
   *
   * @return path of output file.
   */
  public Path determineOutputFile() {

    // Ensure -o and -odir are both not specified.
    if (contains(CliOption.OptionType.OUTPUT_DIR) && contains(CliOption.OptionType.OUTPUT_FILE)) {
      throw new CliParseException(
          "Could not validate options, as output file and directory both are specified");
    }

    final List<CliOption.InputFile> inputFiles = inputFiles();
    final CliOption.OutputFile outputFile = outputFile();
    final CliOption.OutputDir outputDir = outputDir();

    // Prefer output file
    if (outputFile != null) {
      return outputFile.path();
    }

    // If output dir is set, create directory and resolve output directory and file.
    final Path outPath =
        FileManager.replaceExtension(inputFiles.get(0).path(), FileManager.BYTE_CODE_EXTENSION);
    if (outputDir != null) {
      Path outDirPath = FileManager.createDirectory(outputDir.path());
      return outDirPath.resolve(outPath.getFileName());
    }

    // No output option was specified, default to path of where the input file lives.
    return outPath;
  }

  /**
   * Determines and returns the colors to use for presenting output.
   *
   * @return the colors to use for presenting output.
   */
  public static TerminalColors determineColors(
      CliOption.Color.ColorOption colorOption, boolean terminalSupportsColor) {
    return switch (colorOption) {
      case ALWAYS -> TerminalColors.ANSI;
      case NEVER -> TerminalColors.NONE;
      default -> terminalSupportsColor ? TerminalColors.ANSI : TerminalColors.NONE;
    };
  }
}
