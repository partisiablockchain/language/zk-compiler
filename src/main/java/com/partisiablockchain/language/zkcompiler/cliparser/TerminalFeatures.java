package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Determines which terminal features to enable for the current running system. */
public final class TerminalFeatures {

  private final SystemInfo systemInfo;

  /**
   * Constructs a new TerminalFeatures instance, capable of determining supported terminal features.
   *
   * @param systemInfo Strategy for gathering system information.
   */
  public TerminalFeatures(SystemInfo systemInfo) {
    this.systemInfo = systemInfo;
  }

  /**
   * Determines whether to use coloring for terminals, based on heuristics. Heuristics are: If TERM
   * environment variable contains "cygwin" or starts with "xterm", use coloring. If OSTYPE
   * environment variable is defined, use coloring. If OS is different from windows, use coloring.
   * If any of the properties could not be determined, default to no color.
   *
   * @return whether to use coloring or not.
   */
  public boolean supportsColor() {
    String termEnvVar = systemInfo.getEnvironmentVariable("TERM");
    if (termEnvVar == null) {
      return false;
    }

    final String osName = systemInfo.getSystemProperty("os.name");
    if (osName == null) {
      return false;
    }

    if (!osName.startsWith("Windows")) {
      return true;
    }

    return termEnvVar.contains("cygwin")
        || termEnvVar.startsWith("xterm")
        || systemInfo.getEnvironmentVariable("OSTYPE") != null;
  }
}
