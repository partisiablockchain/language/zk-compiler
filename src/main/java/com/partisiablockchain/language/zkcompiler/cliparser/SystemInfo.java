package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Gives access to information about the operating system running the program, such as environment
 * variables and system properties.
 */
public interface SystemInfo {

  /**
   * Reads a system property and returns the result as a String.
   *
   * @param key the property to read.
   * @return String representation of the property.
   */
  String getSystemProperty(String key);

  /**
   * Reads an environment variable and returns the result as a String.
   *
   * @param key the environment variable to read.
   * @return String representation of the environment variable.
   */
  String getEnvironmentVariable(String key);

  /** Class responsible for reading system properties and environment variables. */
  final class OsDetection implements SystemInfo {

    @Override
    public String getSystemProperty(String key) {
      return System.getProperty(key);
    }

    @Override
    public String getEnvironmentVariable(String key) {
      return System.getenv(key);
    }
  }
}
