package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Terminal color configuration. colorful is true if colors are used and false otherwise. error
 * presents messages in red boldface. usage presents messages in yellow boldface. options present
 * messages in green. normal presents messages without color.
 */
public record TerminalColors(
    boolean colorful, String error, String usage, String options, String normal) {

  /** Configuration with ANSI escaped colored output. */
  public static TerminalColors ANSI =
      new TerminalColors(true, "\033[31;1m", "\033[33;1m", "\033[32m", "\033[0m");

  /** Configuration with no color output. */
  public static TerminalColors NONE = new TerminalColors(false, "", "", "", "");
}
