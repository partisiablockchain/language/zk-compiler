package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import java.nio.file.Path;
import java.util.List;

/**
 * Wrapper for entire program input.
 *
 * @param files Files to compile together.
 */
public record ProgramInput(List<FileInput> files) {
  /**
   * Constructor for {@link ProgramInput}.
   *
   * @param files Files to compile together.
   */
  public ProgramInput {
    requireNonNull(files);
  }

  /**
   * Wrapper for program files.
   *
   * @param sourcePath Path of the source that provided the Rust code. Used for error messages.
   * @param rustSourceCode Rust source code to compile.
   */
  public record FileInput(Path sourcePath, String rustSourceCode) {

    /**
     * Constructor for {@link FileInput}.
     *
     * @param sourcePath Path of the source that provided the Rust code. Used for error messages.
     * @param rustSourceCode Rust source code to compile.
     */
    public FileInput {
      requireNonNull(sourcePath);
      requireNonNull(rustSourceCode);
    }
  }
}
