package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.util.ArrayList;
import java.util.List;

/** Wrapper class for Rust SDK module information. */
final class BuiltinFunctionsRustStd {

  private BuiltinFunctionsRustStd() {}

  static final TypedAst.TypeId STRUCT_DEFINITION_RANGE_ID =
      TypedAst.TypeId.newFor("std", "ops", "Range");

  static final List<Integer> INTEGER_SIZES = List.of(1, 8, 16, 32, 64, 128);

  static TypeCheck.TypeAliasInfo typeAlias(
      final TypedAst.TypeId typeId, TypedAst.Type typeWithoutTypeParameters) {
    return new TypeCheck.TypeAliasInfo(
        typeId, List.of(), typeWithoutTypeParameters, TypeCheck.POSITION_UNKNOWN);
  }

  /**
   * Primitive type environment from the standard library.
   *
   * @return Newly created primitive type environment.
   */
  static List<TypeCheck.TypeAliasInfo> createPrimitiveTypeEnvironment() {
    // Misc public
    final List<TypeCheck.TypeAliasInfo> types =
        new ArrayList<>(
            List.of(
                typeAlias(TypedAst.TypeId.newFor("bool"), TypedAst.Type.Int.BOOL),
                typeAlias(TypedAst.TypeId.newFor("usize"), TypedAst.Type.Int.USIZE)));

    // Integers
    for (final int bitSize : INTEGER_SIZES) {
      types.add(
          typeAlias(TypedAst.TypeId.newFor("u" + bitSize), new TypedAst.Type.Int(bitSize, false)));
      types.add(
          typeAlias(TypedAst.TypeId.newFor("i" + bitSize), new TypedAst.Type.Int(bitSize, true)));
    }
    return types;
  }

  /** Standard library types. */
  public static final List<TypeCheck.TypeAliasInfo> TYPES =
      List.of(
          // Rust sdk: std::cmp::Ordering
          typeAlias(
              TypedAst.TypeId.newFor("std", "cmp", "Ordering"),
              new TypedAst.Type.Int(2, true)), // Very limited implementation for coverage
          typeAlias(
              TypedAst.TypeId.newFor("create_type_spec_derive", "CreateTypeSpec"),
              new TypedAst.Type.Int(0, false))); // Jespers hacky stand-in

  private static final TypedAst.Type.NamedType TYPE_IDX =
      new TypedAst.Type.NamedType(TypedAst.TypeId.newFor("Idx"), List.of());
  private static final TypedAst.Type.NamedType TYPE_RANGE =
      new TypedAst.Type.NamedType(STRUCT_DEFINITION_RANGE_ID, List.of(TYPE_IDX));

  /** The struct definition of {@code std.ops.Range}. */
  public static final TypedAst.StructDefinition STRUCT_DEFINITION_RANGE =
      new TypedAst.StructDefinition(
          STRUCT_DEFINITION_RANGE_ID,
          List.of("Idx"),
          List.of(
              new TypedAst.StructDefinition.StructField("start", TYPE_IDX),
              new TypedAst.StructDefinition.StructField("end", TYPE_IDX)));

  private static final BuiltinFunctions.FnDefinition RANGE_TO_ITERATOR =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(
              TypedAst.EnvKind.FUNCTION_BUILTIN, "std", "ops", "Range", "into_iter"),
          List.of("Idx"),
          List.of(TYPE_RANGE),
          TYPE_RANGE,
          BuiltinFunctionsRustStd::identity,
          false);

  private static final BuiltinFunctions.FnDefinition RANGE_NEXT =
      BuiltinFunctions.FnDefinition.from(
          TypedAst.VariableId.newFor(
              TypedAst.EnvKind.FUNCTION_BUILTIN, "std", "ops", "Range", "next"),
          List.of("Idx"),
          List.of(TYPE_RANGE),
          new TypedAst.Type.Tuple(List.of(TYPE_RANGE, TypedAst.Type.Int.BOOL, TYPE_IDX)),
          BuiltinFunctionsRustStd::rangeIteratorNext,
          false);

  /** {@link BuiltinFunctions.CallGenerator} always returning the first argument. */
  static Operand identity(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final List<Operand> paramOperands,
      final OperandType resultOperandType) {
    return paramOperands.get(0);
  }

  static Operand.Tuple rangeIteratorNext(
      final InstructionAccum insnAccum,
      final TmpName tmpName,
      final List<Operand> paramOperands,
      final OperandType resultOperandType) {

    final List<Operand> operandIteratorComponents = paramOperands.get(0).expectTuple();

    final Operand.Basic operandStart = operandIteratorComponents.get(0).expectBasic();
    final Operand.Basic operandEnd = operandIteratorComponents.get(1).expectBasic();

    final Operand.Basic operandShouldStop =
        insnAccum.addBinop(
            Ir.Type.PublicBool,
            tmpName.with("within_range_not"),
            Ir.Operation.BinaryOp.EQUAL,
            operandStart,
            operandEnd);

    final Operand.Basic operandShouldContinue =
        insnAccum.addUnop(
            Ir.Type.PublicBool,
            tmpName.with("within_range"),
            Ir.Operation.UnaryOp.BITWISE_NOT,
            operandShouldStop);

    final Operand.Basic operandStartIncremented =
        OperandUtil.addAdd(
            insnAccum,
            tmpName.with("start"),
            operandStart,
            new Operand.Constant(
                1, operandStart.operandType().expectBasic(), tmpName.with("increment")));

    final Operand.Tuple operandIteratorUpdated =
        new Operand.Tuple(List.of(operandStartIncremented, operandEnd));

    return new Operand.Tuple(List.of(operandIteratorUpdated, operandShouldContinue, operandStart));
  }

  /** Standard library function definitions. */
  static final List<BuiltinFunctions.FnDefinition> FUNCTIONS_LIST =
      List.of(RANGE_TO_ITERATOR, RANGE_NEXT);

  /** Standard library struct definitions. */
  public static final List<TypedAst.StructDefinition> STRUCTS = List.of(STRUCT_DEFINITION_RANGE);
}
