package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Map.entry;

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkmetacircuit.Block;
import com.partisiablockchain.language.zkmetacircuit.BlockId;
import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkmetacircuit.Instruction;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.Operation;
import com.partisiablockchain.language.zkmetacircuit.Terminator;
import com.partisiablockchain.language.zkmetacircuit.Type;
import com.partisiablockchain.language.zkmetacircuit.VariableId;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility class for translating Ir.Circuit to a MetaCircuit.
 *
 * @see AstToIrTranslator
 */
public final class IrToMetaCircuitTranslator {

  private IrToMetaCircuitTranslator() {}

  /**
   * Translate entire Ir-Circuit to a Meta-Circuit.
   *
   * @param circuit circuit to translate.
   * @param config Translation config with various optimization configurations.
   * @return Produced Meta-Circuit.
   */
  public static MetaCircuit translate(final Ir.Circuit circuit, final TranslationConfig config) {
    final List<MetaCircuit.Function> functions = new ArrayList<>();

    for (final Ir.Function irFunction : circuit.functions()) {
      functions.add(translateFunction(irFunction));
    }
    return new MetaCircuit(List.copyOf(functions));
  }

  /**
   * Translate individual {@link Ir.Function} to a {@link MetaCircuit}.
   *
   * @param function Function to translate.
   * @return Produced Meta-Circuit.
   */
  private static MetaCircuit.Function translateFunction(final Ir.Function function) {
    final FunctionId functionId = new FunctionId(function.functionId().id());
    final Map<Ir.BlockId, BlockId> blockIdAssignments = blockIdAssignments(function);
    return new MetaCircuit.Function(
        functionId,
        function.blocks().stream().map(b -> translateBlock(b, blockIdAssignments)).toList(),
        function.outputVariableTypes().stream()
            .map(IrToMetaCircuitTranslator::translateType)
            .toList());
  }

  private static Map<Ir.BlockId, BlockId> blockIdAssignments(final Ir.Function function) {
    // Create environment
    final var env = new HashMap<Ir.BlockId, BlockId>();
    for (final Ir.Block block : function.blocks()) {
      env.put(block.blockId(), blockIdOf(block.blockId()));
    }

    // Remember to add End block to environment
    env.put(Ir.BlockId.RETURN, BlockId.RETURN);

    // Immutable
    return Map.copyOf(env);
  }

  private static Type translateType(final Ir.Type uncheckedType) {
    if (uncheckedType instanceof Ir.Type.SecretBinaryInteger type) {
      return new Type.SecretBinaryInteger(type.width());
    } else {
      final Ir.Type.PublicInteger type = (Ir.Type.PublicInteger) uncheckedType;
      return new Type.PublicInteger(type.width());
    }
  }

  /**
   * Translate Block to a Meta-Circuit block.
   *
   * @param block Block to translate.
   * @param blockIdAssignments Translation map from {@link Ir.BlockId} to MetaCircuit {@link
   *     BlockId}.
   * @return Produced block.
   */
  private static Block translateBlock(
      final Ir.Block block, final Map<Ir.BlockId, BlockId> blockIdAssignments) {

    final var environment = new Environment();
    final var inputs = new ArrayList<Block.Input>();
    final var instructions = new ArrayList<Instruction>();

    // Translate inputs
    for (final Ir.Input input : block.inputs()) {
      final VariableId inputId = environment.add(input.assignedVariable());
      inputs.add(new Block.Input(inputId, translateType(input.variableType())));
    }

    // Translate instructions
    for (final Ir.Instruction instruction : block.instructions()) {
      final Operation operation = translateOperation(instruction.operation(), environment);
      final VariableId resultId = environment.add(instruction.resultId());
      instructions.add(
          new Instruction(resultId, translateType(instruction.resultType()), operation));
    }

    // Translate terminator
    final Terminator terminator =
        translateTerminator(block.terminator(), environment, blockIdAssignments);

    return new Block(
        blockIdAssignments.get(block.blockId()),
        List.copyOf(inputs),
        List.copyOf(instructions),
        terminator);
  }

  private static Terminator translateTerminator(
      final Ir.Terminator uncheckedTerminator,
      final Environment environment,
      final Map<Ir.BlockId, BlockId> blockEnvironment) {
    // Branch if
    if (uncheckedTerminator instanceof Ir.BranchIf terminator) {
      return new Terminator.BranchIf(
          environment.get(terminator.conditionVariable()),
          translateBranchInfo(terminator.branchTrue(), environment, blockEnvironment),
          translateBranchInfo(terminator.branchFalse(), environment, blockEnvironment));

      // Call
    } else if (uncheckedTerminator instanceof Ir.CallAndBranch terminator) {
      return new Terminator.Call(
          new FunctionId(terminator.functionId().id()),
          translateVariableList(terminator.functionArguments(), environment),
          translateBranchInfo(terminator.branchToAfterReturn(), environment, blockEnvironment));

      // Branch always
    } else {
      final Ir.BranchAlways terminator = (Ir.BranchAlways) uncheckedTerminator;
      return new Terminator.BranchAlways(
          translateBranchInfo(terminator.branch(), environment, blockEnvironment));
    }
  }

  private static Terminator.BranchInfo translateBranchInfo(
      final Ir.BranchInfo branchInfo,
      final Environment environment,
      final Map<Ir.BlockId, BlockId> blockEnvironment) {
    return new Terminator.BranchInfo(
        blockEnvironment.get(branchInfo.targetBlock()),
        translateVariableList(branchInfo.branchInputVariables(), environment));
  }

  private static List<VariableId> translateVariableList(
      final List<Ir.VariableId> variables, final Environment environment) {
    return variables.stream().map(environment::get).toList();
  }

  private static Operation translateOperation(
      final Ir.Operation uncheckedOperation, final Environment environment) {

    // Unary operations
    if (uncheckedOperation instanceof Ir.Operation.Unary operation) {
      return new Operation.Unary(
          UNOP_MAPPING.get(operation.operation()), environment.get(operation.varIdx1()));

      // To secret operation
    } else if (uncheckedOperation instanceof Ir.Operation.PublicToSecret operation) {
      return new Operation.Cast(
          translateType(operation.type()), environment.get(operation.varIdx1()));

      // Nullary operation
    } else if (uncheckedOperation instanceof Ir.Operation.Nullary operation) {
      return new Operation.Nullary(NULOP_MAPPING.get(operation.operation()));

      // Binary operation
    } else if (uncheckedOperation instanceof Ir.Operation.Binary operation) {
      final Operation.BinaryOp binop = BINOP_MAPPING.get(operation.operation());
      return new Operation.Binary(
          binop, environment.get(operation.varIdx1()), environment.get(operation.varIdx2()));

      // Extract operation
    } else if (uncheckedOperation instanceof Ir.Operation.Extract operation) {
      return new Operation.Extract(
          environment.get(operation.varToExtractFrom()),
          operation.typeToExtract().width(),
          operation.bitOffsetFromLow());

      // ExtractDyn operation
    } else if (uncheckedOperation instanceof Ir.Operation.ExtractDyn operation) {
      return new Operation.ExtractDynamically(
          environment.get(operation.varToExtractFrom()),
          operation.typeToExtract().width(),
          environment.get(operation.varOffsetFromLow()));

      // InsertDyn operation
    } else if (uncheckedOperation instanceof Ir.Operation.InsertDyn operation) {
      return new Operation.InsertDynamically(
          environment.get(operation.varTarget()),
          environment.get(operation.varToInsert()),
          environment.get(operation.varOffsetIntoTarget()));

      // Select operation
    } else if (uncheckedOperation instanceof Ir.Operation.Select operation) {
      return new Operation.Select(
          environment.get(operation.varCondition()),
          environment.get(operation.varIfTrue()),
          environment.get(operation.varIfFalse()));

      // Constant operation
    } else {
      final Ir.Operation.Constant operation = (Ir.Operation.Constant) uncheckedOperation;
      final int value = unsignedBigIntegerToSignedInt(operation.const1());
      return new Operation.Constant(translateType(operation.type()), value);
    }
  }

  /**
   * Converts from an unsigned {@link BigInteger} into a signed {@link Integer} value.
   *
   * @param value Value to convert. Must be between 0 and 2^32-1 (inclusive.)
   * @return signed integer.
   * @throws IllegalArgumentException when value cannot fit into an integer.
   */
  static int unsignedBigIntegerToSignedInt(BigInteger value) {
    if (value.bitLength() > 32) {
      throw new IllegalArgumentException("Value does not fit into integer: %s".formatted(value));
    }
    return value.intValue();
  }

  private static final Map<Ir.Operation.BinaryOp, Operation.BinaryOp> BINOP_MAPPING =
      Map.ofEntries(
          entry(Ir.Operation.BinaryOp.BITWISE_AND, Operation.BinaryOp.BITWISE_AND),
          entry(Ir.Operation.BinaryOp.BITWISE_OR, Operation.BinaryOp.BITWISE_OR),
          entry(Ir.Operation.BinaryOp.BITWISE_XOR, Operation.BinaryOp.BITWISE_XOR),
          entry(
              Ir.Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL,
              Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL),
          entry(
              Ir.Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL,
              Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL),
          entry(Ir.Operation.BinaryOp.BIT_CONCAT, Operation.BinaryOp.BIT_CONCAT),
          entry(Ir.Operation.BinaryOp.ADD_WRAPPING, Operation.BinaryOp.ADD_WRAPPING),
          entry(
              Ir.Operation.BinaryOp.MULT_SIGNED_WRAPPING, Operation.BinaryOp.MULT_WRAPPING_SIGNED),
          entry(
              Ir.Operation.BinaryOp.MULT_UNSIGNED_WRAPPING,
              Operation.BinaryOp.MULT_WRAPPING_SIGNED),
          entry(Ir.Operation.BinaryOp.EQUAL, Operation.BinaryOp.EQUAL),
          entry(Ir.Operation.BinaryOp.LESS_THAN_SIGNED, Operation.BinaryOp.LESS_THAN_SIGNED),
          entry(Ir.Operation.BinaryOp.LESS_THAN_UNSIGNED, Operation.BinaryOp.LESS_THAN_UNSIGNED),
          entry(
              Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED,
              Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED),
          entry(
              Ir.Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED,
              Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED));

  private static final Map<Ir.Operation.UnaryOp, Operation.UnaryOp> UNOP_MAPPING =
      Map.ofEntries(
          entry(Ir.Operation.UnaryOp.BITWISE_NOT, Operation.UnaryOp.BITWISE_NOT),
          entry(Ir.Operation.UnaryOp.NEGATE, Operation.UnaryOp.NEGATE),
          entry(Ir.Operation.UnaryOp.LOAD_VARIABLE, Operation.UnaryOp.LOAD_VARIABLE),
          entry(Ir.Operation.UnaryOp.SAVE_VARIABLE, Operation.UnaryOp.OUTPUT),
          entry(Ir.Operation.UnaryOp.LOAD_METADATA, Operation.UnaryOp.LOAD_METADATA),
          entry(Ir.Operation.UnaryOp.NEXT_VARIABLE_ID, Operation.UnaryOp.NEXT_VARIABLE_ID));

  private static final Map<Ir.Operation.NullaryOp, Operation.NullaryOp> NULOP_MAPPING =
      Map.ofEntries(entry(Ir.Operation.NullaryOp.NUM_VARIABLES, Operation.NullaryOp.NUM_VARIABLES));

  private static BlockId blockIdOf(final Ir.BlockId blockId) {
    return new BlockId(blockId.id());
  }

  static final class Environment {

    final HashMap<Ir.VariableId, VariableId> variableEnvironment =
        new HashMap<Ir.VariableId, VariableId>();

    public VariableId add(final Ir.VariableId variableId) {
      final VariableId newId = new VariableId(variableId.id());
      variableEnvironment.put(variableId, newId);
      return newId;
    }

    public VariableId get(final Ir.VariableId variableId) {
      final VariableId newId = variableEnvironment.get(variableId);
      if (newId == null) {
        throw new InternalCompilerException(
            "Malformed IR: Could not find temporary variable '%s'", variableId);
      }
      return newId;
    }
  }
}
