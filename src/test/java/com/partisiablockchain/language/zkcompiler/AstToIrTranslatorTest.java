package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.zkparser.ZkAst;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Tests for {@link AstToIrTranslator}. */
public final class AstToIrTranslatorTest {

  final AstToIrTranslator.TemporaryVariableId idTemp = new AstToIrTranslator.TemporaryVariableId(1);
  final TypedAst.VariableId idVar =
      TypedAst.VariableId.newFor(TypedAst.EnvKind.VARIABLE_LET, "hello");

  @Test
  public void contextGetNull() {
    final var ctx = AstToIrTranslator.Context.empty();
    assertThat(ctx.get(idTemp)).isNull();
  }

  @Test
  public void contextInvalidateOnDefine() {
    final var ctx = AstToIrTranslator.Context.empty();
    assertThat(ctx.get(idTemp)).isNull();
    final var ctx2 = ctx.define(idTemp, Operand.Tuple.UNIT);
    assertThat(ctx2.get(idTemp)).isEqualTo(Operand.Tuple.UNIT);
    assertThatThrownBy(() -> ctx.get(idVar)).isInstanceOf(NullPointerException.class);
    assertThatThrownBy(() -> ctx.get(idTemp)).isInstanceOf(NullPointerException.class);
  }

  @Test
  public void contextInvalidateOnDefineVar() {
    final var ctx = AstToIrTranslator.Context.empty();
    assertThat(ctx.get(idVar)).isNull();
    final var ctx2 = ctx.define(idVar, Operand.Tuple.UNIT);
    assertThat(ctx2.get(idVar)).isEqualTo(Operand.Tuple.UNIT);
    assertThatThrownBy(() -> ctx.get(idVar)).isInstanceOf(NullPointerException.class);
    assertThatThrownBy(() -> ctx.get(idTemp)).isInstanceOf(NullPointerException.class);
  }

  @Test
  public void contextInvalidateOnAssign() {
    final var ctx = AstToIrTranslator.Context.empty().define(idVar, Operand.Tuple.UNIT);
    assertThat(ctx.get(idVar)).isEqualTo(Operand.Tuple.UNIT);
    final var ctx2 = ctx.assign(idVar, Operand.Tuple.UNIT);
    assertThat(ctx2.get(idVar)).isEqualTo(Operand.Tuple.UNIT);
    assertThatThrownBy(() -> ctx.get(idVar)).isInstanceOf(NullPointerException.class);
    assertThatThrownBy(() -> ctx.get(idTemp)).isInstanceOf(NullPointerException.class);
  }

  @Test
  public void contextInvalidateOnAssignInParent() {
    final var ctx =
        AstToIrTranslator.Context.empty()
            .define(idVar, Operand.Tuple.UNIT)
            .childContext(null, null);
    assertThat(ctx.get(idVar)).isEqualTo(Operand.Tuple.UNIT);
    final var ctx2 = ctx.assign(idVar, Operand.Tuple.UNIT);
    assertThat(ctx2.get(idVar)).isEqualTo(Operand.Tuple.UNIT);
    assertThatThrownBy(() -> ctx.get(idVar)).isInstanceOf(NullPointerException.class);
    assertThatThrownBy(() -> ctx.get(idTemp)).isInstanceOf(NullPointerException.class);
  }

  private static final TypedAst.PosInterval POSITION =
      new TypedAst.PosInterval(Path.of("TEST"), new ZkAst.PosInterval(-1, -1, -1, -1));

  @Test
  public void getTmpNameForPlaceExpressionCoverage() {
    final TypedAst.TypedExpression expr1 =
        new TypedAst.TypedExpression(
            new TypedAst.Expression.LocalVariable(
                TypedAst.VariableId.newFor(TypedAst.EnvKind.VARIABLE_LET, "my_var")),
            TypedAst.Type.Int.I32,
            false,
            false,
            POSITION);
    final TypedAst.Expression expr2 =
        new TypedAst.Expression.Binary(ZkAst.Binop.MULT, expr1, expr1);

    assertThatThrownBy(() -> AstToIrTranslator.getTmpNameForPlaceExpression(expr2))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining("Cannot determine TmpName for given expression");
  }

  @Test
  public void getTmpNameForPlaceExpressionCoverage2() {
    final TypedAst.Expression expr2 = new TypedAst.Expression.IntLiteral(BigInteger.ONE);

    assertThatThrownBy(() -> AstToIrTranslator.getTmpNameForPlaceExpression(expr2))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining("Cannot determine TmpName for given expression");
  }

  /** Integers can both be signed and unsigned. */
  @Test
  public void isSignedIntegerTest() {
    // Public integers
    assertThat(AstToIrTranslator.isSignedType(TypedAst.Type.Int.USIZE)).isFalse();
    assertThat(AstToIrTranslator.isSignedType(TypedAst.Type.Int.BOOL)).isFalse();
    assertThat(AstToIrTranslator.isSignedType(TypedAst.Type.Int.I32)).isTrue();

    // Secret-shared integers
    assertThat(AstToIrTranslator.isSignedType(new TypedAst.Type.Sbi(32, false))).isFalse();
    assertThat(AstToIrTranslator.isSignedType(new TypedAst.Type.Sbi(32, true))).isTrue();
    assertThat(AstToIrTranslator.isSignedType(TypedAst.Type.Sbi.BOOL)).isFalse();
  }

  /** Non-integer types can never be signed. */
  @Test
  public void isSignedTypeTest() {
    // Error types
    assertThat(AstToIrTranslator.isSignedType(new TypedAst.Type.ErrorType())).isFalse();

    // Tuple types
    assertThat(AstToIrTranslator.isSignedType(TypedAst.Type.Tuple.UNIT)).isFalse();
    assertThat(
            AstToIrTranslator.isSignedType(new TypedAst.Type.Tuple(List.of(TypedAst.Type.Int.I32))))
        .isFalse();
  }
}
