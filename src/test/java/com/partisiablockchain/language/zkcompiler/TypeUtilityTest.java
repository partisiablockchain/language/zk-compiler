package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Test;

/** Tests for {@link TypeUtility}. */
public final class TypeUtilityTest {

  private static final TypedAst.Type optionGeneric =
      new TypedAst.Type.NamedType(
          TypedAst.TypeId.newFor("Option"),
          List.of(TypedAst.Type.NamedType.simple(TypedAst.TypeId.newFor("OptionT"))));

  private static final TypedAst.Type optionSbool =
      new TypedAst.Type.NamedType(
          TypedAst.TypeId.newFor("Option"), List.of(TypedAst.Type.Sbi.BOOL));
  private static final TypedAst.Type optionI32 =
      new TypedAst.Type.NamedType(TypedAst.TypeId.newFor("Option"), List.of(TypedAst.Type.Int.I32));
  private static final TypedAst.Type vecSbool =
      new TypedAst.Type.NamedType(TypedAst.TypeId.newFor("Vec"), List.of(TypedAst.Type.Sbi.BOOL));

  private static final TypedAst.TypeId typeId1 = TypedAst.TypeId.newFor("T1");
  private static final TypedAst.TypeId typeId2 = TypedAst.TypeId.newFor("T2");

  private static final TypedAst.Type named1 = TypedAst.Type.NamedType.simple(typeId1);
  private static final TypedAst.Type named2 = TypedAst.Type.NamedType.simple(typeId2);

  @Test
  public void typeUnificationEmpty() {
    final var result = unify(List.of(), List.of());
    assertThat(result).isEmpty();
  }

  @Test
  public void mismatchLength() {
    final var result = unify(List.of(), List.of(TypedAst.Type.Sbi.BOOL));
    assertThat(result).isNull();
  }

  @Test
  public void typeUnificationIdentity() {
    final var result = unify(TypedAst.Type.Sbi.BOOL, TypedAst.Type.Sbi.BOOL);
    assertThat(result).isEmpty();
  }

  @Test
  public void typeUnificationFailsDueToTypeMismatch() {
    final var result = unify(TypedAst.Type.Sbi.BOOL, TypedAst.Type.Int.I32);
    assertThat(result).isNull();
  }

  @Test
  public void typeUnificationWithFreeVar() {
    final var result = unify(named1, TypedAst.Type.Int.I32);
    assertThat(result).hasSize(1).containsEntry(typeId1, TypedAst.Type.Int.I32);
  }

  @Test
  public void typeUnificationWithSeveralFreeVar() {
    final var result =
        unify(List.of(named1, named2), List.of(TypedAst.Type.Sbi.BOOL, TypedAst.Type.Int.I32));

    assertThat(result)
        .hasSize(2)
        .containsEntry(typeId1, TypedAst.Type.Sbi.BOOL)
        .containsEntry(typeId2, TypedAst.Type.Int.I32);
  }

  @Test
  public void typeUnificationWithSeveralToSameFreeVar() {
    final var result =
        unify(List.of(named1, named1), List.of(TypedAst.Type.Int.I32, TypedAst.Type.Int.I32));

    assertThat(result).hasSize(1).containsEntry(typeId1, TypedAst.Type.Int.I32);
  }

  @Test
  public void typeUnificationWithSeveralToSameFreeVarFail() {
    final var result =
        unify(List.of(named1, named1), List.of(TypedAst.Type.Sbi.BOOL, TypedAst.Type.Int.I32));

    assertThat(result).isNull();
  }

  @Test
  public void typeUnificationWithSeveralToSameFreeVarTuple() {
    final var result =
        unify(
            new TypedAst.Type.Tuple(List.of(named1, named1)),
            new TypedAst.Type.Tuple(List.of(TypedAst.Type.Int.I32, TypedAst.Type.Int.I32)));

    assertThat(result).hasSize(1).containsEntry(typeId1, TypedAst.Type.Int.I32);
  }

  @Test
  public void typeUnificationWithSeveralToSameFreeVarTupleFail() {
    final var result =
        unify(
            new TypedAst.Type.Tuple(List.of(named1, named1)),
            new TypedAst.Type.Tuple(List.of(TypedAst.Type.Sbi.BOOL, TypedAst.Type.Int.I32)));

    assertThat(result).isNull();
  }

  @Test
  public void typeUnificationTupleRecursive() {
    final var result =
        unify(named1, new TypedAst.Type.Tuple(List.of(named1, TypedAst.Type.Int.I32)));

    assertThat(result)
        .hasSize(1)
        .containsEntry(typeId1, new TypedAst.Type.Tuple(List.of(named1, TypedAst.Type.Int.I32)));
  }

  @Test
  public void typeUnificationArray() {
    final var result =
        unify(
            new TypedAst.Type.Array(named1, 10),
            new TypedAst.Type.Array(TypedAst.Type.Int.I32, 10));

    assertThat(result).hasSize(1).containsEntry(typeId1, TypedAst.Type.Int.I32);
  }

  @Test
  public void typeUnificationArrayArray() {
    final var result =
        unify(
            new TypedAst.Type.Array(named1, 10),
            new TypedAst.Type.Array(new TypedAst.Type.Array(TypedAst.Type.Int.I32, 10), 10));

    assertThat(result)
        .hasSize(1)
        .containsEntry(typeId1, new TypedAst.Type.Array(TypedAst.Type.Int.I32, 10));
  }

  @Test
  public void typeUnificationArrayRecursive() {
    final var result =
        unify(
            new TypedAst.Type.Array(named1, 10),
            new TypedAst.Type.Array(new TypedAst.Type.Array(named1, 10), 10));

    assertThat(result).hasSize(1).containsEntry(typeId1, new TypedAst.Type.Array(named1, 10));
  }

  @Test
  public void typeUnificationArrayFail() {
    final var result =
        unify(
            new TypedAst.Type.Array(TypedAst.Type.Int.I32, 10),
            new TypedAst.Type.Array(new TypedAst.Type.Array(named1, 10), 10));

    assertThat(result).isNull();
  }

  @Test
  public void typeUnificationExampleOption() {
    final var result = unify(optionGeneric, optionSbool);

    assertThat(result)
        .hasSize(1)
        .containsEntry(TypedAst.TypeId.newFor("OptionT"), TypedAst.Type.Sbi.BOOL);
  }

  @Test
  public void typeUnificationExampleOptionFail() {
    final var result = unify(optionSbool, optionI32);

    assertThat(result).isNull();
  }

  @Test
  public void typeUnificationExampleOptionAssign() {
    final var result = unify(named1, optionSbool, Set.of(new NamePath.Absolute("Option")));

    assertThat(result).hasSize(1).containsEntry(typeId1, optionSbool);
  }

  @Test
  public void typeUnificationNotIdenticalTypes() {
    final var result =
        unify(
            optionSbool,
            vecSbool,
            Set.of(new NamePath.Absolute("Option"), new NamePath.Absolute("Vec")));

    assertThat(result).isNull();
  }

  private static Map<TypedAst.TypeId, TypedAst.Type> unify(
      List<TypedAst.Type> t1, List<TypedAst.Type> t2, Set<NamePath.Absolute> boundNames) {

    // Check that unification is commutative.
    final Map<TypedAst.TypeId, TypedAst.Type> result12 =
        TypeUtility.performTypeUnification(t1, t2, boundNames);
    final Map<TypedAst.TypeId, TypedAst.Type> result21 =
        TypeUtility.performTypeUnification(t2, t1, boundNames);
    assertThat(result12).as("performTypeUnification must be commutative").isEqualTo(result21);

    // Check that individual arguments does not result in any assignments.
    final Map<TypedAst.TypeId, TypedAst.Type> result11 =
        TypeUtility.performTypeUnification(t1, t1, boundNames);
    final Map<TypedAst.TypeId, TypedAst.Type> result22 =
        TypeUtility.performTypeUnification(t2, t2, boundNames);
    assertThat(result11).as("Identical arguments should not give any assignments").isEmpty();
    assertThat(result22).as("Identical arguments should not give any assignments").isEmpty();

    // Return one of the results
    return result12;
  }

  private static Map<TypedAst.TypeId, TypedAst.Type> unify(
      List<TypedAst.Type> t1, List<TypedAst.Type> t2) {
    return unify(t1, t2, Set.of());
  }

  private static Map<TypedAst.TypeId, TypedAst.Type> unify(TypedAst.Type t1, TypedAst.Type t2) {
    return unify(List.of(t1), List.of(t2), Set.of());
  }

  private static Map<TypedAst.TypeId, TypedAst.Type> unify(
      TypedAst.Type t1, TypedAst.Type t2, Set<NamePath.Absolute> boundNames) {
    return unify(List.of(t1), List.of(t2), boundNames);
  }
}
