package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcircuit.ZkCircuitBigIntegerExecution;
import com.partisiablockchain.language.zkcircuit.ZkCircuitEngine;
import com.partisiablockchain.language.zkcircuit.ZkCircuitExecution;
import com.partisiablockchain.language.zkcircuitlowering.LoweringPublicExecutionEnvironment;
import com.partisiablockchain.language.zkcircuitlowering.MetaCircuitLowering;
import com.partisiablockchain.language.zkmetacircuit.Block;
import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.Type;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

record MetaCircuitEvaluator(MetaCircuit metaCircuit) {

  static MetaCircuitEvaluator of(final MetaCircuit metaCircuit) {
    return new MetaCircuitEvaluator(metaCircuit);
  }

  public record Result(List<BigInteger> publicOutputs, List<BigInteger> secretOutputs) {}

  private List<BigInteger> setupPublicInputs(FunctionId functionId, List<BigInteger> inputs) {
    final List<BigInteger> publicInputs = new ArrayList<BigInteger>();

    MetaCircuit.Function function =
        metaCircuit.functions().stream()
            .filter(f -> f.functionId().equals(functionId))
            .toList()
            .get(0);

    final List<Block.Input> blockInputs = function.blocks().get(0).inputs();

    for (int i = 0; i < blockInputs.size(); i++) {
      if (blockInputs.get(i).variableType() instanceof Type.PublicInteger) {
        publicInputs.add(inputs.get(i));
      }
    }

    return publicInputs;
  }

  /** Maximum amount of branches to take when evaluating the circuit. */
  private static final long MAX_CIRCUIT_BRANCHES = Long.MAX_VALUE;

  Result evaluate(FunctionId functionId, final List<BigInteger> inputs) {

    // Setup meta-circuit environment
    final List<BigInteger> publicInputs = setupPublicInputs(functionId, inputs);
    final Map<Integer, BigInteger> inputVariableMap = new HashMap<>();
    for (final BigInteger input : inputs) {
      inputVariableMap.put(inputVariableMap.size() + 1, input);
    }
    final LoweringPublicExecutionEnvironment env =
        new StaticLoweringPublicExecutionEnvironment(inputs);

    // Execute public part of meta circuit.
    final MetaCircuitLowering.Result result =
        MetaCircuitLowering.executeMetaCircuit(
            metaCircuit, functionId, env, publicInputs, MAX_CIRCUIT_BRANCHES);

    // Construct circuit engine
    final ZkCircuitExecution<ZkCircuitBigIntegerExecution.Value> execution =
        new ZkCircuitBigIntegerExecution(inputVariableMap);
    final ZkCircuitEngine<ZkCircuitBigIntegerExecution.Value> circuitEngine =
        new ZkCircuitEngine<>(execution, result.zkCircuit());

    // Execute circuit
    final List<ZkCircuitBigIntegerExecution.Value> outputValues = circuitEngine.executeZkCircuit();

    // Output values
    final List<BigInteger> secretOutputs =
        outputValues.stream().map(ZkCircuitBigIntegerExecution.Value::value).toList();
    return new Result(result.outputValues(), secretOutputs);
  }
}
