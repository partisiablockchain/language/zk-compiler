package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

final class ExamplePrograms {

  private ExamplePrograms() {}

  enum Stage {
    /** Will fail during parsing. */
    NotParse,

    /** Will succeed at parsing, but fail during type check. */
    NotTypeCheck,

    /** Will succeed up to compilation, but fail then. */
    NotCompile,

    /** Will succeed all the way through compilation. */
    Evaluate;
  }

  /** Denotes how far through the compilation pipeline a test case can make it before failing. */
  sealed interface TestAssertion {
    Stage stage();

    /** Will fail during parsing. */
    record NotParse(String message) implements TestAssertion {

      @Override
      public Stage stage() {
        return Stage.NotParse;
      }
    }

    /** Will succeed at parsing, but fail during type check. */
    record NotTypeCheck(String message) implements TestAssertion {
      @Override
      public Stage stage() {
        return Stage.NotTypeCheck;
      }
    }

    /** Will succeed up to compilation, but fail then. */
    record NotCompile(String message) implements TestAssertion {
      @Override
      public Stage stage() {
        return Stage.NotCompile;
      }
    }

    /** Will succeed all the way through compilation. */
    record Evaluate(List<BigInteger> input, List<BigInteger> output, boolean hexFormatErrors)
        implements TestAssertion {
      @Override
      public Stage stage() {
        return Stage.Evaluate;
      }
    }

    record EvaluateTests() implements TestAssertion {

      @Override
      public Stage stage() {
        return Stage.Evaluate;
      }
    }

    /** Special "assertion" to prevent any asserting. */
    record Ignore() implements TestAssertion {
      static final Ignore SINGLETON = new Ignore();

      @Override
      public Stage stage() {
        return null;
      }
    }
  }

  static final Map<Stage, Integer> STAGE_POSITION =
      Map.of(
          Stage.NotParse, 1,
          Stage.NotTypeCheck, 2,
          Stage.NotCompile, 3,
          Stage.Evaluate, 4);

  /**
   * Single ZkRust test case possibly involving multiple source files.
   *
   * @param sourceFilePaths Paths of source files.
   * @param stage The expected completion stage of the test.
   * @param assertions The set of {@link TestAssertion}s to check on the result.
   */
  record TestCase(List<Path> sourceFilePaths, Stage stage, List<TestAssertion> assertions) {
    TestCase {
      requireNonNull(sourceFilePaths);
      requireNonNull(stage);
      requireNonNull(assertions);
    }

    public String filename() {
      return sourceFilePaths.get(0).toString();
    }

    /**
     * Create new {@link TestCase} with the given set of source paths and assertions.
     *
     * @param sourceFilePaths Paths of source files.
     * @param assertions The set of {@link TestAssertion}s to check on the result.
     */
    public static TestCase of(
        final List<Path> sourceFilePaths, final List<TestAssertion> assertions) {
      final Stage stage =
          assertions.stream()
              .map(TestAssertion::stage)
              .max(Comparator.comparingInt(STAGE_POSITION::get))
              .orElse(null);
      if (stage == null) {
        throw new RuntimeException("Could not determine any assertions for " + sourceFilePaths);
      }
      return new TestCase(sourceFilePaths, stage, assertions);
    }

    @SuppressWarnings("unchecked")
    public <T extends TestAssertion> List<T> assertionsOfType(final Class<T> clazz) {
      final var list = new ArrayList<T>();
      for (final TestAssertion assertion : assertions) {
        if (assertion.getClass().equals(clazz)) {
          list.add((T) assertion);
        }
      }
      return list;
    }
  }

  /**
   * Returns programs that succeeded beyond the given stage, and is expected to succeed at this
   * stage.
   */
  public static List<TestCase> programsProgressPast(final Stage stage) {
    return TEST_CASES.stream()
        .filter(t -> STAGE_POSITION.get(stage) < STAGE_POSITION.get(t.stage()))
        .toList();
  }

  /**
   * Returns programs that succeeded up until the given stage, but is expected to fail at this
   * stage.
   */
  public static List<TestCase> programsAtStage(final Stage stage) {
    return TEST_CASES.stream().filter(c -> c.stage().equals(stage)).toList();
  }

  /**
   * Returns programs that succeeded up until the given stage, but is expected to fail at this
   * stage.
   */
  public static List<TestCase> programsFailingAtAnyStage() {
    return TEST_CASES.stream()
        .filter(t -> STAGE_POSITION.get(t.stage()) < STAGE_POSITION.get(Stage.Evaluate))
        .toList();
  }

  private static final List<TestCase> TEST_CASES;

  private static final List<Long> STANDARD_ARGUMENTS =
      List.of(0L, 1L, 2L, 3L, 4L, 9L, 32L, 63L, 99L, 3123L, 5322L);

  static {
    final List<String> testCaseFolders =
        List.of(
            "invalid_compile", "invalid_parse", "invalid_type", "valid", "wip_compile", "wip_type");

    final List<TestCase> tmpTestCases =
        testCaseFolders.stream()
            .flatMap(ExamplePrograms::filesInDirectory)
            .map(ExamplePrograms::parseTestCase)
            .filter(x -> x != null)
            .toList();

    TEST_CASES = List.copyOf(tmpTestCases);
  }

  //// Result utility

  // Test Case parsing

  public static TestCase parseTestCase(final Path primarySourceFilePath) {
    final String text = loadText(primarySourceFilePath);

    final List<TestAssertion> testAssertions =
        Pattern.compile("//!\\s*assert\\s+(.*)\\R")
            .matcher(text)
            .results()
            .map(result -> parseAssertLine(result.group(1), primarySourceFilePath))
            .flatMap(List::stream)
            .toList();

    if (testAssertions.contains(TestAssertion.Ignore.SINGLETON)) {
      return null;
    }

    final Stream<Path> secondarySourcePaths =
        Pattern.compile("//!\\s*use\\s+([\\w,/]*)\\R")
            .matcher(text)
            .results()
            .map(result -> primarySourceFilePath.getParent().resolve(result.group(1) + ".rs"));

    final List<Path> sourceFilePaths =
        Stream.concat(Stream.of(primarySourceFilePath), secondarySourcePaths).toList();
    return TestCase.of(sourceFilePaths, testAssertions);
  }

  public static String expectedMessage(String formattingMessage, final Path primarySourceFilePath) {
    if (formattingMessage == null) {
      return null;
    }
    return formattingMessage.replaceAll(
        "\\{\\{module\\}\\}",
        new TypeCheck.ProgramFile(null, primarySourceFilePath).modulePath().toString());
  }

  public static List<TestAssertion> parseAssertLine(
      final String assertText, final Path primarySourceFilePath) {
    if (assertText.equals("eval tests")) {
      return List.of(new TestAssertion.EvaluateTests());
    } else if (assertText.startsWith("eval")) {
      final var evalCase = EvalExprParser.parseEvalExpr(assertText);
      if (evalCase.boundNames().isEmpty()) {
        return List.of(testCaseFromEvalCase(evalCase, Map.of()));
      }

      return STANDARD_ARGUMENTS.stream()
          .<TestAssertion>map(
              v ->
                  testCaseFromEvalCase(
                      evalCase, standardTestArgumentEnvironment(v, evalCase.boundNames())))
          .toList();

      // Failure in various steps

    } else if (match(assertText, "fail_parse(?:\\s*\"(.*)\")?") instanceof MatchResult m) {
      final String msg = expectedMessage(m.group(1), primarySourceFilePath);
      return List.of(new TestAssertion.NotParse(msg));
    } else if (match(assertText, "fail_type_check(?:\\s*\"(.*)\")?") instanceof MatchResult m) {
      final String msg = expectedMessage(m.group(1), primarySourceFilePath);
      return List.of(new TestAssertion.NotTypeCheck(msg));
    } else if (match(assertText, "fail_compile(?:\\s*\"(.*)\")?") instanceof MatchResult m) {
      final String msg = expectedMessage(m.group(1), primarySourceFilePath);
      return List.of(new TestAssertion.NotCompile(msg));

      // Special formats

    } else if (match(assertText, "ignore") instanceof MatchResult m) {
      return List.of(TestAssertion.Ignore.SINGLETON);
      //

    } else {
      throw new RuntimeException("Unknown test assert format: " + assertText);
    }
  }

  private static Map<EvalExprParser.Expr.Name, BigInteger> standardTestArgumentEnvironment(
      final long v, final Iterable<EvalExprParser.Expr.Name> boundNames) {
    final Map<EvalExprParser.Expr.Name, BigInteger> env = new HashMap<>();
    long nameIdx = 0;
    for (final var name : boundNames) {
      env.put(name, BigInteger.valueOf(v + nameIdx));
    }
    return Map.copyOf(env);
  }

  private static TestAssertion.Evaluate testCaseFromEvalCase(
      final EvalExprParser.EvalCase evalCase, final Map<EvalExprParser.Expr.Name, BigInteger> env) {
    return new TestAssertion.Evaluate(
        evalCase.inputs().stream().map(e -> constEvalExpr(e, env)).toList(),
        evalCase.outputs().stream().map(e -> constEvalExpr(e, env)).toList(),
        evalCase.usesHex());
  }

  private static BigInteger constEvalExpr(
      final EvalExprParser.Expr uncheckedExpr,
      final Map<EvalExprParser.Expr.Name, BigInteger> env) {
    if (uncheckedExpr instanceof EvalExprParser.Expr.Name expr) {
      return env.get(expr);
    } else if (uncheckedExpr instanceof EvalExprParser.Expr.Constant expr) {
      return expr.value();
    } else if (uncheckedExpr instanceof EvalExprParser.Expr.Add expr) {
      return constEvalExpr(expr.left(), env).add(constEvalExpr(expr.right(), env));
    } else if (uncheckedExpr instanceof EvalExprParser.Expr.Mult expr) {
      return constEvalExpr(expr.left(), env).multiply(constEvalExpr(expr.right(), env));
    }
    return BigInteger.ZERO;
  }

  private static Object match(final String text, final String pattern) {
    return (Object) matchToResult(text, pattern);
  }

  private static MatchResult matchToResult(final String text, final String pattern) {
    final Matcher matcher = Pattern.compile(pattern).matcher(text);
    final boolean findAny = matcher.matches();
    return !findAny ? null : matcher.toMatchResult();
  }

  // Utility

  public static Stream<Path> filesInDirectory(final String directoryName) {
    final File directory =
        new File(TranslateToCircuitTest.class.getResource(directoryName).getPath());
    final int prefixLength = directory.getParentFile().getAbsolutePath().length() + 1;
    return Arrays.stream(directory.listFiles())
        .map(File::getAbsolutePath)
        .filter(filename -> filename.endsWith(".rs"))
        .map(filename -> filename.substring(prefixLength))
        .map(Path::of);
  }

  public static String loadText(final Path filePath) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> ExamplePrograms.class.getResourceAsStream(filePath.toString()),
            InputStream::readAllBytes,
            "Could not load file: " + filePath);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }

  /**
   * Creates an entire single-file program input from the given path and its source code.
   *
   * @param filePath Path for file.
   * @return Newly created program input.
   */
  public static ProgramInput loadTextAsProgramInput(final Path filePath) {
    return new ProgramInput(List.of(new ProgramInput.FileInput(filePath, loadText(filePath))));
  }

  /**
   * Creates an entire multi-file program input from the given {@link TestCase}.
   *
   * @param testCase Test case to create program input for.
   * @return Newly created program input.
   */
  public static ProgramInput loadProgramInput(final TestCase testCase) {
    return new ProgramInput(
        testCase.sourceFilePaths().stream()
            .map(filePath -> new ProgramInput.FileInput(filePath, loadText(filePath)))
            .toList());
  }
}
