package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

/** Tests for the CliOptions. */
final class CliOptionTest {

  TerminalColors colors = TerminalColors.ANSI;

  @Test
  void testShowOption() {
    var inputOption = CliOption.OptionType.INPUT_FILE;
    assertThat(inputOption.showOption(colors)).isEqualTo("");

    var outputOption = CliOption.OptionType.OUTPUT_FILE;
    assertThat(outputOption.showOption(colors))
        .isEqualTo(coloredOption("-o", "<file>", "Write output to file."));

    var emitOption = CliOption.OptionType.EMIT;
    assertThat(emitOption.showOption(colors))
        .startsWith(
            coloredOption(
                "--emit",
                "[ir-initial|ir-final|metacircuit|ir-all]",
                "Emit intermediate formats."));

    var helpOption = CliOption.OptionType.HELP;
    assertThat(helpOption.showOption(colors))
        .isEqualTo(coloredOption("-h, --help", "", "Display this help message."));

    var colorOption = CliOption.OptionType.COLOR;
    assertThat(colorOption.showOption(colors))
        .startsWith(coloredOption("--color", "[auto|always|never]", "Colorize terminal output."));
  }

  @Test
  void testEmitParseFromArgs() {
    assertThatThrownBy(() -> CliOption.Emit.parseFromArgs("nonsense"))
        .isInstanceOf(CliParseException.class)
        .hasMessage("Invalid option specified for --emit: nonsense");

    var validEmitOption = CliOption.Emit.parseFromArgs("ir-initial");
    assertThat(validEmitOption.emitType()).isEqualTo(CliOption.Emit.EmitType.IR_INITIAL);
  }

  @Test
  void testColorParseFromArgs() {
    assertThatThrownBy(() -> CliOption.Color.parseFromArgs("invalid"))
        .isInstanceOf(CliParseException.class)
        .hasMessage("Invalid option specified for --color: invalid");

    var noColorOption = CliOption.Color.parseFromArgs("never");
    assertThat(noColorOption.colorOption()).isEqualTo(CliOption.Color.ColorOption.NEVER);

    var alwaysColorOption = CliOption.Color.parseFromArgs("always");
    assertThat(alwaysColorOption.colorOption()).isEqualTo(CliOption.Color.ColorOption.ALWAYS);

    var autoColorOption = CliOption.Color.parseFromArgs("auto");
    assertThat(autoColorOption.colorOption()).isEqualTo(CliOption.Color.ColorOption.AUTO);
  }

  @Test
  void testParseOptionFromArgs() {
    assertThat(CliOption.parseFromArgs("--emit")).isEqualTo(CliOption.OptionType.EMIT);
    assertThat(CliOption.parseFromArgs("-o")).isEqualTo(CliOption.OptionType.OUTPUT_FILE);
    assertThat(CliOption.parseFromArgs("-odir")).isEqualTo(CliOption.OptionType.OUTPUT_DIR);
  }

  @Test
  void testColorOption() {
    var option = new CliOption.Color(CliOption.Color.ColorOption.ALWAYS);
    assertThat(option.getType()).isEqualTo(CliOption.OptionType.COLOR);
    assertThat(option.colorOption().getName()).isEqualTo("always");
  }

  private String coloredOption(String name, String argument, String description) {
    String coloredName = colors.options() + name + colors.normal();
    if (argument.isBlank()) {
      return String.format("    %s: %s%n", coloredName, description);
    } else {
      return String.format("    %s %s: %s%n", coloredName, argument, description);
    }
  }
}
