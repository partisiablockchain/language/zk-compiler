package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test cases of the CliParser#main method. */
public final class RealCliParserExecutionTest {

  private Path tmpDir;

  //// Paths used in the tests

  static final Path PATH_COMPUTATION_TEST_FILES =
      Path.of("src/test/resources/com/partisiablockchain/language/zkcompiler/valid/");

  private static final Path PATH_INVALID_TEST_FILES =
      Path.of("src/test/resources/com/partisiablockchain/language/zkcompiler/invalid_type/");

  static final Path PATH_INPUT_FILE = PATH_COMPUTATION_TEST_FILES.resolve("binop_add_i32.rs");
  static final Path PATH_TMP_DIR =
      Path.of("src/test/java/com/partisiablockchain/language/zkcompiler/cliparser/");

  static final String PARSE_ERROR_MESSAGE_PREFIX = "Error:";
  static final String COMPILATION_ERROR_MSG = "\033[31;1mCompilation error\033[0m: ";

  //// Test setup

  @BeforeEach
  void setUp() throws IOException {
    tmpDir = Files.createTempDirectory(PATH_TMP_DIR, "tmp");
  }

  @AfterEach
  void tearDown() {
    deleteDirectory(tmpDir.toFile());
  }

  static void deleteDirectory(File dir) {
    File[] contents = dir.listFiles();
    if (contents != null) {
      for (File file : contents) {
        deleteDirectory(file);
      }
    }
    dir.delete();
  }

  @Test
  void realOutputFileTest() {
    String[] args = {
      "-o", tmpDir.resolve("myOutputFile.zkbc").toString(), PATH_INPUT_FILE.toString()
    };
    CliParser.main(args);

    // Assert that file exists, and that it is non-empty
    assertFilesNotEmpty(tmpDir, "myOutputFile.zkbc");
  }

  @Test
  void testOutputDir() {
    Path testDirString = tmpDir.resolve("testDir");

    String[] args = {PATH_INPUT_FILE.toString(), "-odir", testDirString.toString()};
    CliParser.main(args);

    assertDirectoryContent(tmpDir, "testDir");
    assertDirectoryContent(testDirString, "binop_add_i32.zkbc");
    assertFilesNotEmpty(testDirString, "binop_add_i32.zkbc");
  }

  @Test
  void testSeveralDirs() {
    Path nestedDir = tmpDir.resolve("newDir");
    String[] args = {PATH_INPUT_FILE.toString(), "-o", nestedDir + "/out.zkbc"};
    CliParser.main(args);

    assertDirectoryContent(nestedDir, "out.zkbc");
  }

  @Test
  void testOutputCombinations() {
    final List<String> argsConflictingOutput =
        List.of(PATH_INPUT_FILE.toString(), "-o", "out.zkbc", "-odir", "outDir");
    final TerminalFeaturesTest.IndeterminableOsName systemInfo =
        new TerminalFeaturesTest.IndeterminableOsName();
    final CliParserTest.ParseAndRunResult result =
        CliParserTest.parseAndRunExpectFailure(argsConflictingOutput, systemInfo);
    assertThat(result.stderr())
        .contains("Could not validate options, as output file and directory both are specified.");

    Path nestedDir = tmpDir.resolve("newDir/anotherDir");

    String[] argsOutFileInDir = {PATH_INPUT_FILE.toString(), "-o", nestedDir + "/out.zkbc"};
    CliParser.main(argsOutFileInDir);

    assertDirectoryContent(nestedDir, "out.zkbc");
  }

  @Test
  void outDirExistingFile() {
    List<String> args = List.of(PATH_INPUT_FILE.toString(), "-odir", PATH_INPUT_FILE.toString());
    TerminalFeaturesTest.LinuxMockClass systemInfo = new TerminalFeaturesTest.LinuxMockClass();
    final CliParserTest.ParseAndRunResult result =
        CliParserTest.parseAndRunExpectFailure(args, systemInfo);
    assertThat(result.stderr()).contains("Compilation error");
  }

  @Test
  void testErrorInCompilation() {
    Path nonsenseFile = PATH_INVALID_TEST_FILES.resolve("assign_immutable_Sbi32.rs");
    List<String> args = List.of(nonsenseFile.toString());
    TerminalFeaturesTest.LinuxMockClass systemInfo = new TerminalFeaturesTest.LinuxMockClass();
    final CliParserTest.ParseAndRunResult result =
        CliParserTest.parseAndRunExpectFailure(args, systemInfo);
    assertThat(result.stderr()).contains(COMPILATION_ERROR_MSG);
  }

  @Test
  void testColorInCompileErrors() {
    Path nonsenseFile = PATH_INVALID_TEST_FILES.resolve("assign_immutable_Sbi32.rs");
    List<String> args = List.of(nonsenseFile.toString());
    TerminalFeaturesTest.LinuxMockClass systemInfo = new TerminalFeaturesTest.LinuxMockClass();
    CliParserTest.parseAndRunExpectFailure(args, systemInfo);
  }

  @Test
  void testNoColorInCompileError() {
    Path nonsenseFile = PATH_INVALID_TEST_FILES.resolve("assign_immutable_Sbi32.rs");
    List<String> args = List.of(nonsenseFile.toString());
    TerminalFeaturesTest.IndeterminableOsName systemInfo =
        new TerminalFeaturesTest.IndeterminableOsName();
    final CliParserTest.ParseAndRunResult result =
        CliParserTest.parseAndRunExpectFailure(args, systemInfo);
    assertThat(result.stderr()).contains("error").doesNotContain("\033[31;1merror");
  }

  @Test
  void testParseErrorWithNoColorSupport() {
    List<String> args = List.of("qwe");

    TerminalFeaturesTest.LinuxMockClass systemInfo = new TerminalFeaturesTest.LinuxMockClass();
    final CliParserTest.ParseAndRunResult result =
        CliParserTest.parseAndRunExpectFailure(args, systemInfo);

    assertThat(result.stderr()).contains("Could not find some of the provided source files: qwe");
  }

  @Test
  void testSeveralSameEmit() {
    List<String> args =
        List.of(
            PATH_INPUT_FILE.toString(),
            "--emit",
            "ir-initial",
            "--emit",
            "ir-initial",
            "-odir",
            tmpDir.toString());

    CliParserTest.parseAndRun(args, new TerminalFeaturesTest.LinuxMockClass());
    assertDirectoryContent(tmpDir, "binop_add_i32.zkbc", "binop_add_i32-0-ir-initial.dbg");
  }

  static void assertFilesNotEmpty(Path pathToDirectory, String... fileNames) {
    assertThat(nonEmptyFilesIn(pathToDirectory))
        .as("Non-empty files in " + pathToDirectory)
        .contains(fileNames);
  }

  static void assertDirectoryContent(Path pathToDirectory, String... expectedFiles) {
    String[] files = pathToDirectory.toFile().list();
    assertThat(files).as(pathToDirectory.toString()).contains(expectedFiles);
  }

  static Collection<String> nonEmptyFilesIn(Path pathToDirectory) {
    return Arrays.stream(pathToDirectory.toFile().list())
        .map(pathToDirectory::resolve)
        .filter(p -> p.toFile().isFile())
        .filter(p -> p.toFile().length() > 0)
        .map(pathToDirectory::relativize)
        .map(Path::toString)
        .sorted()
        .toList();
  }
}
