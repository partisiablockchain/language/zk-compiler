package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.nio.file.Path;
import org.junit.jupiter.api.Test;

final class FileManagerTest {

  private final Path pathToTestDir =
      Path.of("src/test/java/com/partisiablockchain/language/zkcompiler/cliparser/");

  @Test
  void testStripExtension() {
    String noExtension = "filename";
    assertThat(FileManager.stripExtensionAsString(noExtension)).isEqualTo(noExtension);

    String withExtension = "filename.extension";
    assertThat(FileManager.stripExtensionAsString(withExtension)).isEqualTo(noExtension);
    assertThat(FileManager.stripExtensionAsString(".")).isEqualTo(".");
  }

  @Test
  void testStripExtensionComplex() {
    String filename = "../../../test.txt";
    assertThat(FileManager.stripExtensionAsString(filename)).isEqualTo("../../../test");

    String filenameBackslash = "..\\..\\..\\test.txt";
    assertThat(FileManager.stripExtensionAsString(filenameBackslash)).isEqualTo("..\\..\\..\\test");
    assertThat(FileManager.stripExtensionAsString("test./")).isEqualTo("test./");
    assertThat(FileManager.stripExtensionAsString("./file.exe")).isEqualTo("./file");
  }

  @Test
  void testFileExists() {
    Path path = RealCliParserExecutionTest.PATH_INPUT_FILE;
    assertThat(path).exists();
    assertThat(Path.of("gibberish/file/path.extension")).doesNotExist();
  }

  @Test
  void testExistingDirectory() {
    Path existingPath =
        Path.of("src/test/java/com/partisiablockchain/language/zkcompiler/cliparser");
    assertThat(existingPath).exists();
    assertThat(FileManager.createDirectory(existingPath)).isEqualTo(existingPath);
  }

  @Test
  void testCreateDirectory() {
    assertThat(FileManager.createDirectory(pathToTestDir)).isEqualTo(pathToTestDir);

    assertThatThrownBy(
            () -> FileManager.createDirectory(RealCliParserExecutionTest.PATH_INPUT_FILE))
        .isInstanceOf(RuntimeException.class)
        .hasMessageStartingWith("An error occurred trying to create the directory");
  }
}
