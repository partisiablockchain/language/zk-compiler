package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Tests for TerminalFeatures. Several Mock classes are implemented, to mock different environments.
 */
final class TerminalFeaturesTest {

  static final List<SystemInfo> ALL_FEATURES =
      List.of(
          new LinuxMockClass(),
          new WindowsXterm(),
          new NoTermEnvClass(),
          new CygwinTerm(),
          new NoOsType(),
          new OsTypeSet(),
          new IndeterminableOsName());

  @Test
  void testLinuxOs() {
    var linuxFeatures = new TerminalFeatures(new LinuxMockClass());
    assertThat(linuxFeatures.supportsColor()).isTrue();
  }

  @Test
  void testXtermTerminal() {
    var terminalWithXterm = new TerminalFeatures(new WindowsXterm());
    assertThat(terminalWithXterm.supportsColor()).isTrue();
  }

  @Test
  void testNoTermEnv() {
    var term = new TerminalFeatures(new NoTermEnvClass());
    assertThat(term.supportsColor()).isFalse();
  }

  @Test
  void testCygwin() {
    var cygwin = new TerminalFeatures(new CygwinTerm());
    assertThat(cygwin.supportsColor()).isTrue();
  }

  @Test
  void testNoOsType() {
    var term = new TerminalFeatures(new NoOsType());
    assertThat(term.supportsColor()).isFalse();
  }

  @Test
  void testAutoWithColor() {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    var ps = new PrintStream(out);

    var colorOption = new CliOption.Color(CliOption.Color.ColorOption.AUTO);
    TerminalColors colors = CliOptions.determineColors(colorOption.colorOption(), true);
    ps.print(colors.error() + "Test" + colors.normal());
    assertThat(out.toString(StandardCharsets.UTF_8)).isEqualTo("\033[31;1mTest\033[0m");
  }

  @Test
  void testOsType() {
    var terminal = new TerminalFeatures(new OsTypeSet());
    assertThat(terminal.supportsColor()).isTrue();
  }

  @Test
  void testNullOsName() {
    var terminal = new TerminalFeatures(new IndeterminableOsName());
    assertThat(terminal.supportsColor()).isFalse();
  }

  @Test
  void testOsDetectionInfo() {
    SystemInfo.OsDetection osDetection = new SystemInfo.OsDetection();

    // Ensure test works on both linux and windows by forcing os.name to be Linux
    System.setProperty("os.name", "Linux");
    assertThat(osDetection.getSystemProperty("os.name")).isEqualTo("Linux");

    String env = osDetection.getEnvironmentVariable("term");
    if (env != null) {
      assertThat(env.isBlank()).isFalse();
    }
  }

  @Test
  void testRealInvocationWindowsXterm() {
    var ps = new PrintStream(new ByteArrayOutputStream());
    var errOut = new ByteArrayOutputStream();
    var err = new PrintStream(errOut);
    var systemInfo = new WindowsXterm();
    var testingTerminationStrategy = new TestingTerminationStrategy();
    CliParser.parseAndRun(ps, err, List.of("qwe"), systemInfo, testingTerminationStrategy);

    assertThat(errOut.toString(StandardCharsets.UTF_8)).startsWith("\033[31;1mError:");
  }

  /** Several mock classes for testing different System environments. */
  static final class LinuxMockClass implements SystemInfo {

    @Override
    public String getSystemProperty(String key) {
      return "Linux";
    }

    @Override
    public String getEnvironmentVariable(String key) {
      return "xterm";
    }
  }

  private static final class WindowsXterm implements SystemInfo {

    @Override
    public String getSystemProperty(String key) {
      return "Windows";
    }

    @Override
    public String getEnvironmentVariable(String key) {
      return "xterm";
    }
  }

  static final class NoTermEnvClass implements SystemInfo {

    @Override
    public String getSystemProperty(String key) {
      return "Windows";
    }

    @Override
    public String getEnvironmentVariable(String key) {
      if (key.equals("OSTYPE")) {
        return "WIN";
      }
      return null;
    }
  }

  private static final class CygwinTerm implements SystemInfo {

    @Override
    public String getSystemProperty(String key) {
      return "Windows";
    }

    @Override
    public String getEnvironmentVariable(String key) {
      return "cygwin";
    }
  }

  private static final class NoOsType implements SystemInfo {

    @Override
    public String getSystemProperty(String key) {
      return "Windows";
    }

    @Override
    public String getEnvironmentVariable(String key) {
      if (key.equals("TERM")) {
        return "";
      }
      return null;
    }
  }

  private static final class OsTypeSet implements SystemInfo {

    @Override
    public String getSystemProperty(String key) {
      return "Windows";
    }

    @Override
    public String getEnvironmentVariable(String key) {
      if (key.equals("TERM")) {
        return "";
      }
      return "OS-TYPE";
    }
  }

  static final class IndeterminableOsName implements SystemInfo {

    @Override
    public String getSystemProperty(String key) {
      return null;
    }

    @Override
    public String getEnvironmentVariable(String key) {
      return "";
    }
  }

  static final class TestingTerminationStrategy implements CliParser.TerminationStrategy {

    private int exitStatus = 0;

    @Override
    public void exit(int exitStatus) {
      this.exitStatus = exitStatus;
    }

    public int getExitStatus() {
      return exitStatus;
    }

    public boolean getExitStatusFailed() {
      return exitStatus != 0;
    }
  }
}
