package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.tools.coverage.ExceptionConverter;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.w3c.dom.Document;

/** Tests of the CliParser. */
public final class CliParserTest {

  private static final String VERSION_MESSAGE =
      String.format("Current compiler version: %s", readVersionNumber());

  static final String HELP_MESSAGE_PREFIX = "\033[33;1mUSAGE:\033[0m";

  private static final Path PATH_INPUT_FILE = RealCliParserExecutionTest.PATH_INPUT_FILE;

  static List<SystemInfo> provideSystemInfos() {
    return TerminalFeaturesTest.ALL_FEATURES;
  }

  @Test
  void testEmitSimple() {
    final List<String> cmdlineArgs = List.of(PATH_INPUT_FILE.toString(), "--emit", "ir-initial");
    final CliOptions resultOptions = parseCli(cmdlineArgs);
    var emitOption = resultOptions.emitOptions().get(0);
    assertThat(emitOption.getType()).isEqualTo(CliOption.OptionType.EMIT);
    assertThat(emitOption.emitType()).isEqualTo(CliOption.Emit.EmitType.IR_INITIAL);
  }

  @Test
  void testSeveralEmits() {
    final List<String> cmdlineArgs =
        List.of(
            PATH_INPUT_FILE.toString(),
            "--emit",
            "ir-initial",
            "--emit",
            "ir-final",
            "--emit",
            "metacircuit");
    final CliOptions resultOptions = parseCli(cmdlineArgs);
    final List<CliOption.Emit.EmitType> emitOptionTypes =
        resultOptions.emitOptions().stream().map(CliOption.Emit::emitType).toList();
    assertThat(emitOptionTypes)
        .containsExactly(
            CliOption.Emit.EmitType.IR_INITIAL,
            CliOption.Emit.EmitType.IR_FINAL,
            CliOption.Emit.EmitType.METACIRCUIT);
  }

  @Test
  void testIllegalEmit() {
    List<String> cmdlineArgs = List.of("--emit", "invalid");
    final ParseAndRunResult result =
        parseAndRunExpectFailure(cmdlineArgs, new TerminalFeaturesTest.LinuxMockClass());
    assertThat(result.stderr()).contains("Invalid option specified for --emit: invalid");
    assertThat(result.stdout()).contains(HELP_MESSAGE_PREFIX);
  }

  @Test
  void testOutputFile() {
    List<String> cmdlineArgs = List.of(PATH_INPUT_FILE.toString(), "-o", "myOutputFile.zkbc");
    final CliOptions resultOptions = parseCli(cmdlineArgs);
    assertThat(resultOptions.outputFile()).isNotNull();
    assertThat(resultOptions.outputFile().path()).isEqualTo(Path.of("myOutputFile.zkbc"));
  }

  @Test
  void testOutputDir() {
    List<String> cmdlineArgs = List.of(PATH_INPUT_FILE.toString(), "-odir", "myTestDir");
    final CliOptions resultOptions = parseCli(cmdlineArgs);

    assertThat(resultOptions.outputDir().path()).isEqualTo(Path.of("myTestDir"));
  }

  @ParameterizedTest
  @MethodSource("provideSystemInfos")
  void testOutputDirAndFile1(SystemInfo systemInfo) {
    final List<String> cmdlineArgs =
        List.of("-odir", "dir", "-o", "out.zkbc", PATH_INPUT_FILE.toString());

    final ParseAndRunResult result = parseAndRunExpectFailure(cmdlineArgs, systemInfo);

    assertThat(result.stderr())
        .contains("Could not validate options, as output file and directory both are specified");
  }

  @ParameterizedTest
  @MethodSource("provideSystemInfos")
  void testOutputDirAndFile2(SystemInfo systemInfo) {
    final var cmdlineArgs = List.of("-o", "out.zkbc", "-odir", "dir", PATH_INPUT_FILE.toString());
    final ParseAndRunResult result = parseAndRunExpectFailure(cmdlineArgs, systemInfo);
    assertThat(result.stderr())
        .contains("Could not validate options, as output file and directory both are specified");
  }

  @Test
  void testHelp() {
    List<String> cmdlineArgs = List.of("--help");
    final CliOptions options = parseCli(cmdlineArgs);
    assertThat(options.contains(CliOption.OptionType.HELP)).isTrue();
  }

  @Test
  void onlyHelpIfPresent() {
    List<String> cmdlineArgs = List.of("--version", "--help");
    final ParseAndRunResult result =
        parseAndRun(cmdlineArgs, new TerminalFeaturesTest.LinuxMockClass());
    assertThat(result.stderr()).isEmpty();
    assertThat(result.stdout())
        .startsWith(HELP_MESSAGE_PREFIX)
        .doesNotContain("Current compiler version: ");
  }

  @Test
  void testColors() {
    ParseAndRunResult result;

    //// Always with color support
    List<String> colorAlways = List.of("--help", "--color", "always");
    result = parseAndRun(colorAlways, new TerminalFeaturesTest.LinuxMockClass());
    assertThat(result.stdout()).startsWith(HELP_MESSAGE_PREFIX);

    //// Always with no color support
    result = parseAndRun(colorAlways, new TerminalFeaturesTest.IndeterminableOsName());
    assertThat(result.stdout()).startsWith(HELP_MESSAGE_PREFIX);

    //// Never with color support
    List<String> colorNever = List.of("--help", "--color", "never");
    result = parseAndRun(colorNever, new TerminalFeaturesTest.LinuxMockClass());
    assertThat(result.stdout()).startsWith("USAGE");

    //// Never with no color support
    result = parseAndRun(colorNever, new TerminalFeaturesTest.IndeterminableOsName());
    assertThat(result.stdout()).startsWith("USAGE");

    //// Auto with color support
    List<String> colorAuto = List.of("--help", "--color", "auto");
    result = parseAndRun(colorAuto, new TerminalFeaturesTest.LinuxMockClass());
    assertThat(result.stdout()).startsWith(HELP_MESSAGE_PREFIX);

    //// Auto with no color support
    result = parseAndRun(colorAuto, new TerminalFeaturesTest.IndeterminableOsName());
    assertThat(result.stdout()).startsWith("USAGE");
  }

  @Test
  void noArgsGivesHelp() {
    final ParseAndRunResult result =
        parseAndRun(List.of(), new TerminalFeaturesTest.LinuxMockClass());
    assertThat(result.stdout())
        .startsWith(HELP_MESSAGE_PREFIX)
        .contains("OPTIONS:")
        .contains("-h, --help")
        .contains("--emit");
  }

  @ParameterizedTest
  @MethodSource("provideSystemInfos")
  void testShowVersion(SystemInfo systemInfo) {
    final ParseAndRunResult result = parseAndRun(List.of("--version"), systemInfo);
    assertThat(result.stdout().strip()).contains(VERSION_MESSAGE);
  }

  @Test
  void testNoOutput() {
    final CliOptions options = parseCli(List.of(PATH_INPUT_FILE.toString()));
    Path expectedPath = Path.of("binop_add_i32.zkbc");
    assertThat(options.determineOutputFile().getFileName()).isEqualTo(expectedPath);

    final CliOptions options1 = parseCli(List.of("./" + PATH_INPUT_FILE));
    assertThat(options1.determineOutputFile().getFileName()).isEqualTo(expectedPath);
  }

  @Test
  void testInputFile() {
    CliOption.InputFile file = new CliOption.InputFile(PATH_INPUT_FILE);
    assertThat(file.path()).isEqualTo(PATH_INPUT_FILE);
    assertThat(file.getType()).isEqualTo(CliOption.OptionType.INPUT_FILE);
  }

  @Test
  void testCliOptions() {
    final CliOptions options = parseCli(List.of("--help"));
    assertThat(options.options()).containsExactly(new CliOption.Help());
    assertThat(options.terminalFeatures().supportsColor()).isTrue();
  }

  @ParameterizedTest
  @MethodSource("provideSystemInfos")
  void invalidInputFileGivesError(SystemInfo systemInfo) {
    final ParseAndRunResult result =
        parseAndRunExpectFailure(List.of("gibberish.filename"), systemInfo);
    assertThat(result.stderr())
        .contains("Could not find some of the provided source files: gibberish.filename");
  }

  @Test
  void missingArgumentGivesError() {
    Assertions.assertThatThrownBy(() -> parseCli(List.of(PATH_INPUT_FILE.toString(), "-o")))
        .isInstanceOf(CliParseException.class)
        .hasMessageContaining("Missing argument for option -o");

    Assertions.assertThatThrownBy(() -> parseCli(List.of(PATH_INPUT_FILE.toString(), "-odir")))
        .isInstanceOf(CliParseException.class)
        .hasMessageContaining("Missing argument for option -odir");

    Assertions.assertThatThrownBy(() -> parseCli(List.of(PATH_INPUT_FILE.toString(), "--emit")))
        .isInstanceOf(CliParseException.class)
        .hasMessageContaining("Missing argument for option --emit");
    assertThat(CliOption.OptionType.getAsString(CliOption.OptionType.INPUT_FILE))
        .isEqualTo("input");
  }

  void testErrorMessages1() {
    final List<String> cmdlineArgs = List.of("nonexistingfile.name");
    final ParseAndRunResult result =
        parseAndRunExpectFailure(cmdlineArgs, new TerminalFeaturesTest.LinuxMockClass());
    assertThat(result.stderr())
        .contains(RealCliParserExecutionTest.COMPILATION_ERROR_MSG)
        .contains("Could not find some of the provided source files: nonexistingfile.name");
  }

  @ParameterizedTest
  @MethodSource("provideSystemInfos")
  void testErrorMessages2(SystemInfo systemInfo) {
    final List<String> cmdlineArgs = List.of(PATH_INPUT_FILE.toString(), "-o");
    final ParseAndRunResult result = parseAndRunExpectFailure(cmdlineArgs, systemInfo);
    assertThat(result.stderr())
        .contains(RealCliParserExecutionTest.PARSE_ERROR_MESSAGE_PREFIX)
        .contains("Missing argument for option -o");
  }

  @ParameterizedTest
  @MethodSource("provideSystemInfos")
  void testErrorMessagesColor(SystemInfo systemInfo) {
    List<String> cmdlineArgs = List.of(PATH_INPUT_FILE.toString(), "-o");
    final ParseAndRunResult result = parseAndRun(cmdlineArgs, systemInfo);
    assertThat(result.stderr())
        .contains(RealCliParserExecutionTest.PARSE_ERROR_MESSAGE_PREFIX)
        .contains("Missing argument for option -o");
  }

  @ParameterizedTest
  @MethodSource("provideSystemInfos")
  void expectedAtMostOne(SystemInfo systemInfo) {
    final List<String> cmdlineArgs = List.of(PATH_INPUT_FILE.toString(), "-o", "foo", "-o", "bar");
    final ParseAndRunResult result = parseAndRun(cmdlineArgs, systemInfo);
    assertThat(result.stderr())
        .contains(RealCliParserExecutionTest.PARSE_ERROR_MESSAGE_PREFIX)
        .contains("Expected at most one OutputFile option, but was given 2");
  }

  @Test
  void testNoCommandOrInputFile() {
    final List<String> cmdlineArgs = List.of("--emit", "ir-initial");
    final ParseAndRunResult result =
        parseAndRunExpectFailure(cmdlineArgs, new TerminalFeaturesTest.LinuxMockClass());
    assertThat(result.stderr())
        .contains(RealCliParserExecutionTest.PARSE_ERROR_MESSAGE_PREFIX)
        .contains("No input filename given");
  }

  record ParseAndRunResult(String stdout, String stderr, int exitCode) {}

  static ParseAndRunResult parseAndRun(List<String> cmdlineArgs, SystemInfo systemInfo) {
    final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    final PrintStream outPrintStream = new PrintStream(outStream);

    final ByteArrayOutputStream errStream = new ByteArrayOutputStream();
    final PrintStream errPrintStream = new PrintStream(errStream);

    final TerminalFeaturesTest.TestingTerminationStrategy terminationStrategy =
        new TerminalFeaturesTest.TestingTerminationStrategy();

    CliParser.parseAndRun(
        outPrintStream, errPrintStream, cmdlineArgs, systemInfo, terminationStrategy);

    return new ParseAndRunResult(
        outStream.toString(StandardCharsets.UTF_8),
        errStream.toString(StandardCharsets.UTF_8),
        terminationStrategy.getExitStatus());
  }

  static ParseAndRunResult parseAndRunExpectFailure(
      List<String> cmdlineArgs, SystemInfo systemInfo) {
    final ParseAndRunResult result = parseAndRun(cmdlineArgs, systemInfo);
    assertThat(result.exitCode()).isNotEqualTo(0);
    return result;
  }

  static CliOptions parseCli(final List<String> cmdlineArgs) {
    final ByteArrayOutputStream bytesErrOut = new ByteArrayOutputStream();
    final PrintStream printErrOut = new PrintStream(bytesErrOut);
    final CliParser parser =
        new CliParser(printErrOut, new TerminalFeatures(new TerminalFeaturesTest.LinuxMockClass()));
    final CliOptions resultOptions = parser.parseCli(cmdlineArgs);
    return resultOptions;
  }

  private static String readVersionNumber() {
    return ExceptionConverter.call(
        () -> {
          Path pomPath = Path.of("pom.xml");

          DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
          DocumentBuilder builder = factory.newDocumentBuilder();
          Document document = builder.parse(Files.newInputStream(pomPath));

          XPath path = XPathFactory.newInstance().newXPath();
          XPathExpression expression = path.compile("/project/version");
          return expression.evaluate(document);
        },
        "An error occurred reading the pom");
  }
}
