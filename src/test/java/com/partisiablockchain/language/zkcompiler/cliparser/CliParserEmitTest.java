package com.partisiablockchain.language.zkcompiler.cliparser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.zkcompiler.TypeCheck;
import com.partisiablockchain.language.zkcompiler.ZkCompiler;
import com.partisiablockchain.language.zkcompiler.irpass.IrPass;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests of the --emit option. */
public final class CliParserEmitTest {

  private final String irAllFile =
      "src/test/resources/com/partisiablockchain/language/zkcompiler/valid/"
          + "example-second-price-auction-dynamic-loop.rs";

  private static String IR_PASS_SUFFIX_INITIAL = "0-ir-initial";
  private static String IR_PASS_SUFFIX_FINAL = "%d-ir-final".formatted(ZkCompiler.IR_PASSES.size());

  private Path tmpDir;

  private static final Path PATH_INPUT_FILE = RealCliParserExecutionTest.PATH_INPUT_FILE;
  private static final Path PATH_TMP_DIR = RealCliParserExecutionTest.PATH_TMP_DIR;

  @BeforeEach
  void setUp() throws IOException {
    tmpDir = Files.createTempDirectory(PATH_TMP_DIR, "tmp");
  }

  @AfterEach
  void tearDown() {
    RealCliParserExecutionTest.deleteDirectory(tmpDir.toFile());
  }

  @Test
  void testEmit() {
    String[] args = {
      "--emit",
      "ir-initial",
      "--emit",
      "ir-final",
      RealCliParserExecutionTest.PATH_INPUT_FILE.toString(),
      "-o",
      tmpDir.resolve("out.zkbc").toString()
    };

    final String[] expectedFiles = {
      "out.zkbc", "out-" + IR_PASS_SUFFIX_INITIAL + ".dbg", "out-" + IR_PASS_SUFFIX_FINAL + ".dbg"
    };
    CliParser.main(args);

    assertThat(RealCliParserExecutionTest.nonEmptyFilesIn(tmpDir)).contains(expectedFiles);

    // Assert only correct files created
    assertThat(tmpDir.resolve("out-2-metacircuit.dbg")).doesNotExist();
  }

  @Test
  void testEmitInDir() throws IOException {
    final Path outputDir = tmpDir.resolve("testDir");
    String[] args = {
      "--emit", "metacircuit", PATH_INPUT_FILE.toString(), "-odir", outputDir.toString(),
    };
    CliParser.main(args);
    RealCliParserExecutionTest.assertDirectoryContent(tmpDir, "testDir");
    RealCliParserExecutionTest.assertDirectoryContent(
        outputDir, "binop_add_i32.zkbc", "binop_add_i32-metacircuit.dbg");

    final String expected =
        """
        (metacircuit
         (function %0
          (output i32)
          (block #0
            (inputs
              (i32 $0)
              (i32 $1))
            (i32 $2 (add_wrapping $0 $1))
            (branch-always #return $2))))
        """
            .trim();
    assertThat(outputDir.resolve("binop_add_i32-metacircuit.dbg"))
        .content()
        .isEqualToNormalizingNewlines(expected);
  }

  @Test
  void testEmitCombinations() {
    String[] argsWithDir = {
      "--emit",
      "ir-all",
      "--emit",
      "metacircuit",
      PATH_INPUT_FILE.toString(),
      "-odir",
      tmpDir.toString()
    };
    CliParser.main(argsWithDir);
    RealCliParserExecutionTest.assertDirectoryContent(
        tmpDir,
        "binop_add_i32.zkbc",
        "binop_add_i32-" + IR_PASS_SUFFIX_INITIAL + ".dbg",
        "binop_add_i32-" + IR_PASS_SUFFIX_FINAL + ".dbg",
        "binop_add_i32-metacircuit.dbg");

    String[] argsWithFile = {
      "--emit",
      "ir-all",
      "--emit",
      "metacircuit",
      PATH_INPUT_FILE.toString(),
      "-o",
      tmpDir + "/out.zkbc"
    };
    CliParser.main(argsWithFile);
    RealCliParserExecutionTest.assertDirectoryContent(
        tmpDir,
        "out.zkbc",
        "binop_add_i32-" + IR_PASS_SUFFIX_INITIAL + ".dbg",
        "binop_add_i32-" + IR_PASS_SUFFIX_FINAL + ".dbg",
        "binop_add_i32-metacircuit.dbg");
  }

  @Test
  void testEmitInCurrentDir() {
    String[] args = {"--emit", "ir-initial", PATH_INPUT_FILE.toString(), "-o", "out.zkbc"};
    CliParser.main(args);

    // Remove generated files from the root.
    File outFile = new File("out.zkbc");
    outFile.delete();
    File debugFile = new File("out-0-ir-initial.dbg");
    debugFile.delete();
  }

  @Test
  void testEmitAll() throws IOException {
    String[] args = {
      "--emit", "ir-all", "-o", tmpDir.resolve("input.zkbc").toString(), PATH_INPUT_FILE.toString()
    };
    CliParser.main(args);

    final String expectedInitAndFinal =
        """
        (ircircuit
          (exported function %0_{{module}}::add_self
            (output i32)
            (entry block #0_add_self
              (inputs
                (i32 $0_x)
                (i32 $1_y))
              (i32    $2_body              (add_wrapping $0_x $1_y))
              (branch-always #return $2_body))))
        """
            .replace(
                "{{module}}",
                new TypeCheck.ProgramFile(null, PATH_INPUT_FILE).modulePath().toString())
            .trim();

    assertThat(tmpDir.resolve("input-" + IR_PASS_SUFFIX_INITIAL + ".dbg"))
        .content()
        .isEqualToNormalizingNewlines(expectedInitAndFinal);
    assertThat(tmpDir.resolve("input-" + IR_PASS_SUFFIX_FINAL + ".dbg"))
        .content()
        .isEqualToNormalizingNewlines(expectedInitAndFinal);
  }

  @Test
  void testEmitAllDeadCodeElim() {
    Path inputPath = PATH_TMP_DIR.resolve("unused_expr.rs");
    String[] args = {
      inputPath.toString(), "--emit", "ir-all", "-o", tmpDir.resolve("out.zkbc").toString()
    };
    CliParser.main(args);

    String expected =
        """
        (ircircuit
          (exported function %0_{{module}}::unused_expr_func
            (output i32)
            (entry block #0_unused_expr_func
              (inputs
                (i32 $0_x))
              (branch-always #return $0_x))))
        """
            .replace(
                "{{module}}", new TypeCheck.ProgramFile(null, inputPath).modulePath().toString())
            .trim();

    final String dceDebugFileName = "out-3-ir-dead-code-elim.dbg";

    RealCliParserExecutionTest.assertDirectoryContent(tmpDir, dceDebugFileName);

    assertThat(tmpDir.resolve(dceDebugFileName)).content().isEqualToNormalizingNewlines(expected);
  }

  @Test
  void testEmitAllPasses() {
    String[] args = {
      "src/test/resources/com/partisiablockchain/language/zkcompiler/valid/"
          + "example-second-price-auction-dynamic-loop.rs",
      "--emit",
      "ir-all",
      "-o",
      tmpDir.resolve("out.zkbc").toString(),
    };
    CliParser.main(args);

    assertThat(RealCliParserExecutionTest.nonEmptyFilesIn(tmpDir))
        .containsExactlyInAnyOrder(
            "out-" + IR_PASS_SUFFIX_INITIAL + ".dbg",
            "out-1-ir-short-circuit.dbg",
            "out-2-ir-merge-secret-branch.dbg",
            "out-3-ir-dead-code-elim.dbg",
            "out-4-ir-remove-unused-blocks.dbg",
            "out-6-ir-constant-fold.dbg",
            "out-8-ir-peephole-opt.dbg",
            "out-9-ir-common-instruction-elim.dbg",
            "out-" + IR_PASS_SUFFIX_FINAL + ".dbg",
            "out.zkbc");
  }

  @Test
  void allNames() {
    assertThat(ZkCompiler.IR_PASSES.stream().map(IrPass::getName))
        .containsExactly(
            "ir-short-circuit",
            "ir-merge-secret-branch",
            "ir-dead-code-elim",
            "ir-remove-unused-blocks",
            "ir-merge-linear-blocks",
            "ir-constant-fold",
            "ir-split-big-constants",
            "ir-peephole-opt",
            "ir-common-instruction-elim",
            "ir-peephole-opt",
            "ir-select-reduce",
            "ir-operator-call-to-terminator",
            "ir-dead-code-elim");
  }

  @Test
  void emitAllDisabled() {
    String[] args = {
      irAllFile,
      "--emit",
      "ir-initial",
      "--emit",
      "ir-final",
      "--emit",
      "metacircuit",
      "-o",
      tmpDir.resolve("out.zkbc").toString(),
    };
    CliParser.main(args);

    assertThat(RealCliParserExecutionTest.nonEmptyFilesIn(tmpDir))
        .contains(
            "out.zkbc",
            "out-metacircuit.dbg",
            "out-" + IR_PASS_SUFFIX_INITIAL + ".dbg",
            "out-" + IR_PASS_SUFFIX_FINAL + ".dbg");
  }
}
