package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Path;
import java.util.stream.Stream;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class FullCompileTest {

  @Test
  public void identityProgramToByteCode() {
    final Path sourcePath = Path.of("valid/identity.rs");
    final ProgramInput programInput = ExamplePrograms.loadTextAsProgramInput(sourcePath);
    final byte[] circuitBytes =
        ZkCompiler.compileRustToMetaCircuitBytecode(programInput, TranslationConfig.DEFAULT);
    assertThat(circuitBytes)
        .isEqualTo(
            Hex.decode(
                "5a4b4d43" // Magic
                    + "0001 0000" // Bytecode version
                    + "0000 0001" // Num output variables: 1
                    + "0120" // Output 0: i32
                    + "0000 0001" // Num blocks
                    + "0000 0001" // Num inputs: 1
                    + "0120" // Input 0: i32
                    + "0000 0000" // Num instructions: 0
                    + "FFFF FFFF" // Unconditional branch
                    + "FFFF FFFF" // Branch to end
                    + "0000 0001" // Num args to branch
                    + "0000 0000" // Branch variable 0: t0
                ));
  }

  public static final Path SUM_OF_THREE_VARIABLES_PATH =
      Path.of("valid/example-sum-of-three-variables.rs");

  public static final byte[] SUM_OF_THREE_VARIABLES_BYTE_CODE =
      Hex.decode(
          "5a4b4d43" // Magic
              + "0001 0000" // Bytecode version
              + "0000 0001" // Num output variables: 1
              + "0120" // Output 0: i32
              + "0000 0001" // Num blocks
              + "0000 0003" // Num inputs
              + "0120" // Input 0: i32
              + "0120" // Input 1: i32
              + "0120" // Input 2: i32
              + "0000 0002" // Num instructions
              + "0120 000e 0000 0000 0000 0001" // let t3: i32 = ADD t0 t1
              + "0120 000e 0000 0002 0000 0003" // let t4: i32 = ADD t2 t3
              + "FFFF FFFF" // Unconditional branch
              + "FFFF FFFF" // Branch to end
              + "0000 0001" // Num args to branch
              + "0000 0004" // Branch variable 0: t4
          );

  @Test
  public void sumOfThreeVariablesFullTest() {
    final ProgramInput programInput =
        ExamplePrograms.loadTextAsProgramInput(SUM_OF_THREE_VARIABLES_PATH);
    assertThat(ZkCompiler.compileRustToMetaCircuitBytecode(programInput, TranslationConfig.DEFAULT))
        .isEqualTo(SUM_OF_THREE_VARIABLES_BYTE_CODE);
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesCompilable")
  public void succeedToCompileValidPrograms(final ExamplePrograms.TestCase testCase) {
    final ProgramInput programInput = ExamplePrograms.loadProgramInput(testCase);
    final TranslationConfig config = TranslationConfig.DEFAULT;
    final byte[] circuitBytes =
        ExceptionConverter.call(
            () -> ZkCompiler.compileRustToMetaCircuitBytecode(programInput, config),
            testCase.filename());
    assertThat(circuitBytes).isNotNull();
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesFailingAtAnyStage")
  public void failToCompileInvalidPrograms(final ExamplePrograms.TestCase testCase) {
    final ProgramInput programInput = ExamplePrograms.loadProgramInput(testCase);
    assertThatCode(
            () ->
                ZkCompiler.compileRustToMetaCircuitBytecode(
                    programInput, TranslationConfig.DEFAULT))
        .as(testCase.filename())
        .isInstanceOf(RuntimeException.class);
  }

  // Argument providers

  static Stream<Arguments> provideTestCasesCompilable() {
    return ExamplePrograms.programsProgressPast(ExamplePrograms.Stage.NotCompile).stream()
        .map(Arguments::of);
  }

  static Stream<Arguments> provideTestCasesFailingAtAnyStage() {
    return ExamplePrograms.programsFailingAtAnyStage().stream().map(Arguments::of);
  }
}
