package com.partisiablockchain.language.zkcompiler.math;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

final class MappingTest {

  @Test
  public void identityMapping() {
    assertThat(Mapping.identity(0).permutation()).isEmpty();
    assertThat(Mapping.identity(1).permutation()).containsExactly(0);
    assertThat(Mapping.identity(7).permutation()).containsExactly(0, 1, 2, 3, 4, 5, 6);
  }
}
