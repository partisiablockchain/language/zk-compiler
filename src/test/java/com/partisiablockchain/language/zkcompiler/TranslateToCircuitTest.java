package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.language.immutabledetect.ImmutableDetect;
import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitValidationException;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitValidator;
import com.partisiablockchain.language.zkparser.ZkAst;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class TranslateToCircuitTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesEvaluatable")
  public void translateCircuits(final ExamplePrograms.TestCase testCase) {
    final TranslationConfig config =
        new TranslationConfig(new TranslationHooks.Default(), null, false, true);
    ExceptionConverter.call(
        () -> translateAndExecuteInt32Circuit(testCase, config), testCase.filename());
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesFailingInCompilationMostLikelyWip")
  public void attemptWipCircuits(final ExamplePrograms.TestCase testCase) {
    // Determine expected messages
    final List<String> expectedMessages =
        testCase.assertionsOfType(ExamplePrograms.TestAssertion.NotCompile.class).stream()
            .map(ExamplePrograms.TestAssertion.NotCompile::message)
            .filter(Objects::nonNull)
            .toList();

    // Determine code to compile
    final ProgramInput programInput = ExamplePrograms.loadProgramInput(testCase);
    final TranslationConfig config = TranslationConfig.DEFAULT;

    // Attempt compile and assert errors
    final var asserter =
        assertThatCode(() -> ZkCompiler.compileRustToMetaCircuit(programInput, config))
            .as(testCase.filename())
            .isInstanceOfAny(
                UnsupportedOperationException.class,
                InternalCompilerException.class,
                MetaCircuitValidationException.class);

    for (final String expectedMessage : expectedMessages) {
      System.out.println("    Expect: " + expectedMessage);
      asserter.hasMessageContaining(expectedMessage);
    }
  }

  public boolean translateAndExecuteInt32Circuit(
      final ExamplePrograms.TestCase testCase, final TranslationConfig config) {

    // Load code
    final ProgramInput programInput = ExamplePrograms.loadProgramInput(testCase);

    // Compile code
    ZkCompiler.Result compileResult = ZkCompiler.compileRustToMetaCircuit(programInput, config);
    final MetaCircuit circuit = compileResult.metaCircuit();

    // Validate compiled circuit is valid
    assertThat(circuit).matches(ImmutableDetect::isImmutable, "is structurally immutable");
    assertThat(MetaCircuitValidator.validateMetaCircuitNoThrow(circuit))
        .describedAs("Validate Meta-Circuit of " + testCase.filename())
        .isEmpty();

    // Evaluate code, and check behaviour is correct
    final var eval = MetaCircuitEvaluator.of(circuit);
    final var builder = new StringBuilder();

    for (final ExamplePrograms.TestAssertion.Evaluate evalCase :
        testCase.assertionsOfType(ExamplePrograms.TestAssertion.Evaluate.class)) {
      final List<BigInteger> inputs = evalCase.input();
      final List<BigInteger> expectedOutputs = evalCase.output();

      final String errorMessage =
          testEval(testCase.filename(), eval, inputs, expectedOutputs, evalCase.hexFormatErrors());
      if (errorMessage != null) {
        builder.append(
            "%n  For input %10s: %s"
                .formatted(formatPuts(inputs, evalCase.hexFormatErrors()), errorMessage));
      }
    }

    // Test the macro tests
    List<TestInformation> testsInformations = compileResult.testInformation();
    if (testCase.assertionsOfType(ExamplePrograms.TestAssertion.EvaluateTests.class).size() != 0
        && testsInformations.size() == 0) {
      builder.append("Expected tests but found none");
    }
    if (testCase.assertionsOfType(ExamplePrograms.TestAssertion.EvaluateTests.class).size() == 0
        && testsInformations.size() != 0) {
      builder.append("Expected no macro tests but found %d".formatted(testsInformations.size()));
    }

    for (TestInformation testInformation : testsInformations) {
      FunctionId id = testInformation.functionId();
      MetaCircuitEvaluator.Result result = eval.evaluate(id, testInformation.secretInputs());
      ZkAst.PosInterval position = testInformation.position();

      List<BigInteger> testOutputs = result.secretOutputs();
      if (testInformation.result() != null) {
        final BigInteger testResult;
        if (!result.publicOutputs().isEmpty()) {
          testResult = result.publicOutputs().get(0);
        } else {
          int size = result.secretOutputs().size();
          testResult = result.secretOutputs().get(size - 1);
          testOutputs = result.secretOutputs().subList(0, size - 1);
        }
        // Test the output of the function
        if (!testResult.equals(testInformation.result())) {
          builder.append(
              "%n  Test at position %d:%d Failed - Expected actual [%d] to equal expected [%d]"
                  .formatted(
                      position.lineIdxStart(),
                      position.columnIdxStart(),
                      testResult,
                      testInformation.result()));
        }
      }

      // Test the extra secret outputs
      for (int secretOutputNo = 0; secretOutputNo < testOutputs.size(); secretOutputNo++) {
        BigInteger actual = testOutputs.get(secretOutputNo);
        BigInteger expected = testInformation.secretOutputs().get(secretOutputNo);
        if (!actual.equals(expected)) {
          builder.append(
              ("%n  Test at position %d:%d Failed for secret output %d - "
                      + "Expected actual [%d] to equal expected [%d]")
                  .formatted(
                      position.lineIdxStart(),
                      position.columnIdxStart(),
                      secretOutputNo,
                      actual,
                      expected));
        }
      }
    }

    assertThat(builder.toString()).describedAs(testCase.filename()).isEmpty();

    return true;
  }

  /**
   * Evaluates a single test case.
   *
   * @param testName name of the test.
   * @param eval Meta circuit evaluator.
   * @param inputs Inputs for evaluator.
   * @param expectedOutputs Outputs expected.
   * @return Error message if any. Null if success.
   */
  private static String testEval(
      String testName,
      MetaCircuitEvaluator eval,
      List<BigInteger> inputs,
      List<BigInteger> expectedOutputs,
      final boolean hexFormatErrors) {
    final MetaCircuitEvaluator.Result evalResult;
    try {
      FunctionId functionId = eval.metaCircuit().functions().get(0).functionId();
      evalResult = ExceptionConverter.call(() -> eval.evaluate(functionId, inputs), testName);
    } catch (StackOverflowError e) {
      e.printStackTrace();
      return e.toString();
    }

    final List<BigInteger> outputs =
        !evalResult.secretOutputs().isEmpty()
            ? evalResult.secretOutputs()
            : evalResult.publicOutputs();

    if (!evalResult.publicOutputs().isEmpty() && !evalResult.secretOutputs().isEmpty()) {
      return "Produced both public and secret outputs. This is not supported in tests";
    } else if (!outputs.equals(expectedOutputs)) {
      return "Expected %10s, got %10s"
          .formatted(
              formatPuts(expectedOutputs, hexFormatErrors), formatPuts(outputs, hexFormatErrors));
    } else {
      return null; // Success
    }
  }

  private static String formatPuts(final List<BigInteger> puts, final boolean hexFormatErrors) {
    final int radix = hexFormatErrors ? 16 : 10;
    final String prefix = hexFormatErrors ? "0x" : "";
    return puts.stream()
        .map(i -> prefix + i.toString(radix))
        .collect(Collectors.joining(", ", "[", "]"));
  }

  // Argument providers

  static Stream<Arguments> provideTestCasesEvaluatable() {
    return ExamplePrograms.programsAtStage(ExamplePrograms.Stage.Evaluate).stream()
        .map(Arguments::of);
  }

  static Stream<Arguments> provideTestCasesFailingInCompilationMostLikelyWip() {
    return ExamplePrograms.programsAtStage(ExamplePrograms.Stage.NotCompile).stream()
        .map(Arguments::of);
  }
}
