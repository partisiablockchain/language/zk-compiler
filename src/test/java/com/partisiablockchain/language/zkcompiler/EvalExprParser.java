package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class EvalExprParser {

  private EvalExprParser() {}

  private static final String REGEX_INT = "(?:0x[0-9a-fA-F_]+|[0-9_]+)";
  private static final String REGEX_VAR = "(?:\\b[a-xA-X_]+\\b)";

  public sealed interface Expr {
    sealed interface Basic extends Expr {}

    record Name(String name) implements Basic {}

    record Constant(BigInteger value) implements Basic {}

    record Add(Expr left, Expr right) implements Expr {}

    record Mult(Expr left, Expr right) implements Expr {}
  }

  public record EvalCase(
      Set<Expr.Name> boundNames, List<Expr.Basic> inputs, List<Expr> outputs, boolean usesHex) {}

  public static EvalCase parseEvalExpr(String evalStr) {

    final boolean hexFormatErrors = evalStr.contains("0x");

    if (match(
            evalStr,
            "eval\\(x\\)\\s*=>\\s*x\\s*(?:\\*\\s*("
                + REGEX_INT
                + "))?\\s*(?:\\+\\s*("
                + REGEX_INT
                + "))?")
        instanceof MatchResult m) {
      final BigInteger multFactor =
          m.group(1) != null ? parsePrefixableInteger(m.group(1)) : BigInteger.ONE;
      final BigInteger addAmount =
          m.group(2) != null ? parsePrefixableInteger(m.group(2)) : BigInteger.ZERO;

      final Expr.Name X = new Expr.Name("x");
      final Expr X_result =
          new Expr.Add(
              new Expr.Mult(new Expr.Constant(multFactor), X), new Expr.Constant(addAmount));
      return new EvalCase(Set.of(X), List.of(X), List.of(X_result), hexFormatErrors);

    } else if (match(evalStr, "eval\\(\\s*([^)]*)\\s*\\)\\s*=>(.*)") instanceof MatchResult m) {

      final List<Expr.Basic> inputs = parseExpressionList(m.group(1));
      final List<Expr.Basic> outputs = parseExpressionList(m.group(2));
      final List<Expr.Name> boundNames =
          inputs.stream().filter(x -> x instanceof Expr.Name).map(x -> (Expr.Name) x).toList();

      return new EvalCase(
          Set.copyOf(boundNames), inputs, List.<Expr>copyOf(outputs), hexFormatErrors);
    }

    throw new RuntimeException("Unknown parser format: " + evalStr);
  }

  public static List<Expr.Basic> parseExpressionList(String exprListStr) {
    exprListStr = exprListStr.trim();
    if (exprListStr.startsWith("(") && exprListStr.endsWith(")")) {
      return parseExpressionList(exprListStr.substring(1, exprListStr.length() - 1));
    }
    if (exprListStr.isBlank()) {
      return List.of();
    }
    return Arrays.stream(exprListStr.split(","))
        .map(String::strip)
        .map(EvalExprParser::parseExpression)
        .toList();
  }

  public static Expr.Basic parseExpression(final String exprStr) {
    if (match(exprStr, REGEX_VAR) != null) {
      return new Expr.Name(exprStr);
    } else {
      return new Expr.Constant(parsePrefixableInteger(exprStr));
    }
  }

  public static BigInteger parsePrefixableInteger(String integerAsStr) {
    integerAsStr = integerAsStr.replaceAll("_", "");
    if (integerAsStr.startsWith("0x")) {
      return new BigInteger(integerAsStr.replaceFirst("0x", ""), 16);
    } else {
      return new BigInteger(integerAsStr);
    }
  }

  private static Object match(final String text, final String pattern) {
    final Matcher matcher = Pattern.compile(pattern).matcher(text);
    final boolean findAny = matcher.matches();
    return !findAny ? null : (Object) matcher.toMatchResult();
  }
}
