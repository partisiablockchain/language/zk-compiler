package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.zkcompiler.InternalCompilerException;
import java.util.function.Predicate;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Testing edge cases for {@link SymbolicValueSimple}. */
public final class SymbolicValueSimpleTest {

  @Test
  public void insert() {
    assertThat(
            SymbolicValueSimple.BOTTOM_VALUE.insert(
                SymbolicValueSimple.BOTTOM_VALUE, SymbolicValueSimple.BOTTOM_VALUE))
        .isEqualTo(SymbolicValueSimple.BOTTOM_VALUE);
  }

  @Test
  public void extract() {
    final SymbolicValueSimple val = SymbolicValueSimple.constant(10, 10);
    final SymbolicValueSimple offset = SymbolicValueSimple.constant(1, 10);
    assertThatThrownBy(() -> val.extract(10, offset))
        .isInstanceOf(InternalCompilerException.class)
        .hasMessage(
            "Constant fold extraction (10 bits at offset 1) extends outside operand size 10");
  }

  @Test
  public void testToString() {
    assertThat(SymbolicValueSimple.BOTTOM_VALUE)
        .matches(SymbolicValue::isBottom)
        .matches(not(SymbolicValue::isTop))
        .asString()
        .isEqualTo("⊥");
    assertThat(SymbolicValueSimple.TOP_VALUE)
        .matches(not(SymbolicValue::isBottom))
        .matches(SymbolicValue::isTop)
        .asString()
        .isEqualTo("⊤");
    for (int i = 0; i < 1000; i++) {
      assertThat(SymbolicValueSimple.constant(i, 32))
          .matches(not(SymbolicValue::isBottom))
          .matches(not(SymbolicValue::isTop))
          .asString()
          .isEqualTo(Integer.toString(i));
    }
  }

  @Test
  public void equalsAndHashCode() {
    EqualsVerifier.forClass(SymbolicValueSimple.class)
        .withPrefabValues(
            SymbolicValueSimple.class,
            SymbolicValueSimple.TOP_VALUE,
            SymbolicValueSimple.BOTTOM_VALUE)
        .verify();
  }

  private static <T> Predicate<T> not(Predicate<? super T> pred) {
    return Predicate.<T>not(pred);
  }
}
