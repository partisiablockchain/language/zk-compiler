package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Tests for {@link IrPassDeadCodeElimination} that cannot be covered by writing example programs.
 */
public final class IrPassDeadCodeEliminationTest {

  private static final Ir.FunctionId FUNCTION_ID = new Ir.FunctionId(0, "hello");
  private static final Ir.BlockId BLOCK_ID = new Ir.BlockId(0, "hello");
  private static final Ir.Circuit weirdProgram =
      new Ir.Circuit(
          List.of(
              new Ir.Function(
                  FUNCTION_ID,
                  List.of(
                      new Ir.Block(
                          BLOCK_ID,
                          List.of(),
                          List.of(),
                          new Ir.CallAndBranch(
                              FUNCTION_ID, List.of(), new Ir.BranchInfo(BLOCK_ID, List.of()), 0),
                          true)),
                  List.of(),
                  false)));

  @Test
  public void dceCallAndBranch() {
    final Ir.Circuit newProgram = new IrPassDeadCodeElimination().applyPass(weirdProgram, null);
    assertThat(newProgram).isNotEqualTo(weirdProgram);
    assertThat(newProgram.functions().get(0).blocks().get(0).terminator())
        .isEqualTo(new Ir.BranchAlways(new Ir.BranchInfo(BLOCK_ID, List.of())));
  }
}
