package com.partisiablockchain.language.zkcompiler.irpass;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import org.junit.jupiter.api.Test;

/** Namespace class for the Intermediary-Representation model. */
public record OperationUtilityTest() {

  @Test
  public void operationUtilityCreateBinary() {
    final var id1 = new Ir.VariableId(1, "some_id");
    final var id2 = new Ir.VariableId(1, "some_other_id");
    final Ir.Operation.Binary operation =
        OperationUtility.createBinary(Ir.Operation.BinaryOp.BITWISE_AND, id1, id2);
    assertThat(operation.varIdx1()).isEqualTo(id1);
    assertThat(operation.varIdx2()).isEqualTo(id2);
  }
}
