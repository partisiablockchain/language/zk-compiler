package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.List;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Test for {@link ErrorPresenter}. */
public final class ErrorPresenterTest {
  /**
   * If an invalid program is compiled, the correct error message is output, independently of the
   * linebreak character used in the source code.
   *
   * @param sourceWithError Record with invalid source code, and the expected error.
   */
  @ParameterizedTest
  @MethodSource("failingPrograms")
  public void errorsShouldSupportLineFeedOnWindows(SourceCodeWithError sourceWithError) {
    // Normalize to linefeed only (Linux newline)
    final String programLinux = sourceWithError.sourceCode.replace("\r\n", "\n");
    // Denormalize windows version (CRLF)
    final String programWindows = programLinux.replace("\n", "\r\n");

    compileSourceWithError(programLinux, sourceWithError.expectedError);
    compileSourceWithError(programWindows, sourceWithError.expectedError);
  }

  record SourceCodeWithError(String sourceCode, String expectedError) {}

  private void compileSourceWithError(String sourceCode, String expectedError) {
    final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    final TranslationConfig config =
        TranslationConfig.DEFAULT.withErrorOutput(new PrintStream(byteStream), false);

    final ProgramInput programInput =
        new ProgramInput(
            List.of(new ProgramInput.FileInput(Path.of("nonexistent-error_file"), sourceCode)));

    assertThatCode(() -> ZkCompiler.compileRustToMetaCircuit(programInput, config))
        .isInstanceOf(CompilationException.class);

    final String gottenErrorMessages = byteStream.toString(Charset.defaultCharset());

    assertThat(gottenErrorMessages).isEqualToNormalizingNewlines(expectedError);
  }

  private static List<SourceCodeWithError> failingPrograms() {
    SourceCodeWithError parseError =
        new SourceCodeWithError(
            """
            pub fn a(y: i32) -> i32 {
                x x
            }
            """,
            """
            error: extraneous input 'x' expecting '}'
             --> nonexistent-error_file
              |
            2 |     x x
              |       ^
              |
            """);
    SourceCodeWithError typeError =
        new SourceCodeWithError(
            """
            pub fn array() {
                let arr = [0i8; 4294967296];
            }
            """,
            """
            error: Value 4294967296 doesn't fit in u32
             --> nonexistent-error_file
              |
            2 |     let arr = [0i8; 4294967296];
              |                     ^^^^^^^^^^
              |
            """);
    SourceCodeWithError compileError =
        new SourceCodeWithError(
            """
            pub fn compute_55(value: i32) {
                pbc_zk::save_sbi::<(i32, i32)>((value, value));
            }
            """,
            """
            error: Unknown function pbc_zk::save_sbi::<(i32, i32)>
             --> nonexistent-error_file
              |
            2 |     pbc_zk::save_sbi::<(i32, i32)>((value, value));
              |     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
              |
            """);

    return List.of(parseError, typeError, compileError);
  }
}
