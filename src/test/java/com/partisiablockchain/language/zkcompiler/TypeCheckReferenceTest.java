package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class TypeCheckReferenceTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesInvalid")
  public void referenceTypeCheck(final ExamplePrograms.TestCase testCase) {
    referenceFailOutput(testCase, TypeCheckException.class);
  }

  static void referenceFailOutput(
      final ExamplePrograms.TestCase testCase,
      Class<? extends CompilationException> expectedClass) {

    final Path stderrFilepath =
        TranslateToReferenceTest.getResourceSourcePath(
            ExamplePrograms.class, "ref/" + testCase.filename() + ".stderr");

    final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    final TranslationConfig config =
        TranslationConfig.DEFAULT.withErrorOutput(new PrintStream(byteStream), false);

    Assertions.setMaxStackTraceElementsDisplayed(200);
    final ProgramInput programInput = ExamplePrograms.loadProgramInput(testCase);
    assertThatCode(() -> ZkCompiler.compileRustToMetaCircuit(programInput, config))
        .as(testCase.filename())
        .isInstanceOf(expectedClass);

    final String gottenErrorMessages = byteStream.toString(Charset.defaultCharset());

    if (TranslateToReferenceTest.shouldRegenerateReferences()) {
      TranslateToReferenceTest.writeText(stderrFilepath, gottenErrorMessages);
      System.err.println("Regenerated " + stderrFilepath);
    }

    final String expectedErrorMessages = TranslateToReferenceTest.loadText(stderrFilepath);

    assertThat(gottenErrorMessages).isEqualToNormalizingNewlines(expectedErrorMessages);
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesInvalid")
  public void checkAnsiOutputDifferentFromPlain(final ExamplePrograms.TestCase testCase) {

    final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    final TranslationConfig configPlain =
        TranslationConfig.DEFAULT.withErrorOutput(new PrintStream(byteStream), false);
    final TranslationConfig configAnsi =
        configPlain.withErrorOutput(new PrintStream(byteStream), true);

    // Error formatting for plain
    final ProgramInput programInput = ExamplePrograms.loadProgramInput(testCase);
    assertThatCode(() -> ZkCompiler.compileRustToMetaCircuit(programInput, configPlain))
        .as(testCase.filename())
        .isInstanceOf(TypeCheckException.class);

    final String gottenErrorMessagesPlain = byteStream.toString(Charset.defaultCharset());
    byteStream.reset();

    // Error formatting for ANSI
    assertThatCode(() -> ZkCompiler.compileRustToMetaCircuit(programInput, configAnsi))
        .as(testCase.filename())
        .isInstanceOf(TypeCheckException.class);

    final String gottenErrorMessagesAnsi = byteStream.toString(Charset.defaultCharset());

    // Compare
    assertThat(gottenErrorMessagesAnsi).isNotEqualTo(gottenErrorMessagesPlain);
  }

  // Argument providers

  static Stream<Arguments> provideTestCasesValid() {
    return ExamplePrograms.programsProgressPast(ExamplePrograms.Stage.NotTypeCheck).stream()
        .map(Arguments::of);
  }

  static Stream<Arguments> provideTestCasesInvalid() {
    return ExamplePrograms.programsAtStage(ExamplePrograms.Stage.NotTypeCheck).stream()
        .map(Arguments::of);
  }
}
