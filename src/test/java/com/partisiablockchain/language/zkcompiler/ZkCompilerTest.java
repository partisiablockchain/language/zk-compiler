package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link ZkCompiler} internals. */
public final class ZkCompilerTest {

  private static final MetaCircuit META_CIRCUIT_INVALID =
      new MetaCircuit(List.of(new MetaCircuit.Function(new FunctionId(1), List.of(), List.of())));

  /**
   * Produced {@link MetaCircuit}s must be valid, or they will be rejected by the {@link
   * ZkCompiler.Result}.
   */
  @Test
  public void zkCompilerResultInvariants() {
    Assertions.assertThatCode(() -> new ZkCompiler.Result(META_CIRCUIT_INVALID, List.of()))
        .isInstanceOf(
            com.partisiablockchain.language.zkmetacircuit.MetaCircuitValidationException.class);
  }
}
