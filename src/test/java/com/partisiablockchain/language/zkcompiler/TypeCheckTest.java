package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.language.zkparser.ZkAst;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class TypeCheckTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesInvalid")
  public void throwErrorsOnBadCircuits(final ExamplePrograms.TestCase testCase) {
    final List<ExamplePrograms.TestAssertion.NotTypeCheck> failTypeAssertions =
        testCase.assertionsOfType(ExamplePrograms.TestAssertion.NotTypeCheck.class);

    final ProgramInput programInput = ExamplePrograms.loadProgramInput(testCase);

    final var asserter =
        assertThatCode(
                () -> ZkCompiler.compileRustToMetaCircuit(programInput, TranslationConfig.DEFAULT))
            .as(
                "%s: Expecting compilation error during compilation, but no errors occurred!"
                    .formatted(testCase.filename()))
            .isInstanceOf(TypeCheckException.class)
            .hasMessageContaining("Errors in program");

    final List<String> remainingErrorMessages =
        new ArrayList<>(failTypeAssertions.stream().map(x -> x.message()).toList());

    asserter.isInstanceOfSatisfying(
        TypeCheckException.class,
        exception -> {
          // Check that messages are meaningful
          assertThat(exception.errors())
              .as(testCase.filename())
              .hasSize(failTypeAssertions.size())
              .allSatisfy(
                  errorLine -> {
                    assertThat(errorLine.errorLine().lineNumber()).isNotEqualTo(-1);
                    assertThat(errorLine.errorLine().column()).isNotEqualTo(-1);
                    assertThat(errorLine.errorLine().message())
                        .as("Message contained internal compiler representations")
                        .doesNotContain("java.util.Object")
                        .doesNotContain("ErrorType[]")
                        .doesNotContain("PosInterval");
                    assertThat(errorLine.errorLine().message())
                        .as("Message contained unwanted whitespace")
                        .doesNotContain("\t")
                        .doesNotContain("\r")
                        .doesNotContain("\n")
                        .doesNotContain("  ");
                    assertThat(errorLine.errorLine().message()).isIn(remainingErrorMessages);
                    remainingErrorMessages.remove(errorLine.errorLine().message());
                  });
        });

    assertThat(remainingErrorMessages).as(testCase.filename()).isEmpty();
  }

  // Argument providers

  static Stream<Arguments> provideTestCasesValid() {
    return ExamplePrograms.programsProgressPast(ExamplePrograms.Stage.NotTypeCheck).stream()
        .map(Arguments::of);
  }

  static Stream<Arguments> provideTestCasesInvalid() {
    return ExamplePrograms.programsAtStage(ExamplePrograms.Stage.NotTypeCheck).stream()
        .map(Arguments::of);
  }

  @Test
  public void padOrShrink() {
    assertThat(TypeCheck.shrinkOrPadToFixedSize(List.of(1, 2), 0, 9)).isEmpty();
    assertThat(TypeCheck.shrinkOrPadToFixedSize(List.of(1, 2), 1, 9)).containsExactly(1);
    assertThat(TypeCheck.shrinkOrPadToFixedSize(List.of(1, 2), 2, 9)).containsExactly(1, 2);
    assertThat(TypeCheck.shrinkOrPadToFixedSize(List.of(1, 2), 5, 9))
        .containsExactly(1, 2, 9, 9, 9);
    assertThat(TypeCheck.shrinkOrPadToFixedSize(List.of(), 5, 9)).containsExactly(9, 9, 9, 9, 9);
    assertThat(TypeCheck.shrinkOrPadToFixedSize(List.of(), 0, 9)).isEmpty();
  }

  @Test
  public void typeCheckBinopUnknown() {
    final ZkAst.PosInterval pos1 = new ZkAst.PosInterval(0, 0, 0, 0);
    final TypeCheck.ErrorAccum errorAccum =
        new TypeCheck.ErrorAccum(Path.of("test"), new ArrayList<>(), pos1);
    final TypeCheck.BinopPosData binopPostData = new TypeCheck.BinopPosData(pos1, pos1, pos1);
    final var result =
        TypeCheck.typeCheckBinop(ZkAst.Binop.RANGE, errorAccum, null, null, binopPostData);
    assertThat(result).isNotNull().matches(TypedAst.Type::isError);
    assertThat(errorAccum.errors().get(0).errorLine().message())
        .isEqualTo("Unsupported expression: RANGE");
  }

  @Test
  public void coverStaticDisallowed() {
    final TypeCheck.KnownAttribute known = TypeCheck.KnownAttribute.disallowed("my_attribute");
    assertThat(known.name().path().get(0)).isEqualTo("my_attribute");
    assertThat(known.isAllowedOnFunction()).isFalse();
    assertThat(known.isAllowedOnStruct()).isFalse();
    assertThat(known.isAllowed()).isFalse();
  }
}
