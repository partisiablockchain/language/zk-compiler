package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitPretty;
import java.nio.file.Path;
import java.util.List;
import org.junit.jupiter.api.Test;

final class TranslateSpecificProgramsTest {

  private static final Path PROGRAM_IF_PATH = Path.of("valid/example-if-xxx.rs");

  @Test
  public void variableNamesHaveNoEffectOnBytecode() {
    final TranslationConfig config = TranslationConfig.DEFAULT;
    final ProgramInput programInput = ExamplePrograms.loadTextAsProgramInput(PROGRAM_IF_PATH);
    final MetaCircuit circuitOriginal =
        ZkCompiler.compileRustToMetaCircuit(programInput, config).metaCircuit();

    final List<String> originalVariables = List.of("xxx", "yyy", "zzz");
    final List<String> replacedInVariables =
        List.of("aaa", "bbb", "zzzz", "ooo", "xxz", "yyz", "zzy");

    for (final String original : originalVariables) {
      for (final String replacedInVariable : replacedInVariables) {
        final ProgramInput programInputReplaced =
            replaceInProgram(programInput, original, replacedInVariable);

        final MetaCircuit circuitReplaced =
            ZkCompiler.compileRustToMetaCircuit(programInputReplaced, config).metaCircuit();
        assertThat(circuitReplaced)
            .as(MetaCircuitPretty.pretty(circuitReplaced))
            .isEqualTo(circuitOriginal);
      }
    }
  }

  private static ProgramInput replaceInProgram(
      ProgramInput programInput, String originalStr, String newStr) {
    final List<ProgramInput.FileInput> newFiles =
        programInput.files().stream()
            .map(
                x ->
                    new ProgramInput.FileInput(
                        x.sourcePath(), x.rustSourceCode().replace(originalStr, newStr)))
            .toList();
    return new ProgramInput(newFiles);
  }
}
