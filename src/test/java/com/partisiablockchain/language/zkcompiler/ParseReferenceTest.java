package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class ParseReferenceTest {

  @ParameterizedTest
  @MethodSource("provideTestCasesInvalid")
  public void referenceParse(final ExamplePrograms.TestCase testCase) {
    TypeCheckReferenceTest.referenceFailOutput(testCase, ParseException.class);
  }

  // Argument providers

  static Stream<Arguments> provideTestCasesInvalid() {
    return ExamplePrograms.programsAtStage(ExamplePrograms.Stage.NotParse).stream()
        .map(Arguments::of);
  }
}
