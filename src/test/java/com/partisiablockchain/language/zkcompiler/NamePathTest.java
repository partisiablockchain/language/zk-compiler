package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import org.junit.jupiter.api.Test;

final class NamePathTest {

  @Test
  public void relativize() {
    final NamePath.Absolute root = new NamePath.Absolute("hello", "world");
    final NamePath.Absolute subpath = new NamePath.Absolute("hello", "world", "what", "is", "up");

    assertThat(root.relativize(subpath)).isEqualTo(new NamePath.Relative("what", "is", "up"));
  }

  @Test
  public void relativizeFail() {

    final NamePath.Absolute root = new NamePath.Absolute("hello", "world");
    final NamePath.Absolute subpath = new NamePath.Absolute("goodbye", "world", "what", "is", "up");

    assertThatCode(() -> root.relativize(subpath))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Path hello::world is not a strict prefix of other path goodbye::world::what::is::up");
  }

  @Test
  public void relativizeFailOnSelf() {

    final NamePath.Absolute root = new NamePath.Absolute("hello", "world");

    assertThatCode(() -> root.relativize(root))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Path hello::world is not a strict prefix of other path hello::world");
  }

  @Test
  public void relativizeEntire() {
    assertThatCode(() -> new NamePath.Relative())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Relative path cannot be empty!");
  }
}
