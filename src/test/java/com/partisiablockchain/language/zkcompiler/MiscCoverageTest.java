package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkparser.ZkAst;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

final class MiscCoverageTest {

  private static final Operand.Constant TEST_CONSTANT =
      new Operand.Constant(1, Ir.Type.PublicBool, TmpName.root("a"));

  private static final Operand.Constant ZERO_CONSTANT =
      new Operand.Constant(0, Ir.Type.PublicBool, TmpName.root("zero"));

  private static final Operand.Variable TEST_VARIABLE =
      new Operand.Variable(new Ir.VariableId(4, "some_var"), Ir.Type.Sbit);

  private static final Operand.Tuple TEST_TUPLE =
      new Operand.Tuple(List.of(TEST_VARIABLE, TEST_CONSTANT));

  private static final Operand.Array TEST_ARRAY =
      new Operand.Array(TEST_VARIABLE, new OperandType.ArrayType(TEST_VARIABLE.operandType(), 1));

  private static final Operand.Tuple TEST_TUPLE_NESTED =
      new Operand.Tuple(List.of(TEST_VARIABLE, TEST_TUPLE, TEST_ARRAY));

  private static final AstToIrTranslator.Context TEST_CONTEXT =
      AstToIrTranslator.Context.empty()
          .define(TypedAst.VariableId.newFor(TypedAst.EnvKind.VARIABLE_LET, "a"), TEST_CONSTANT)
          .childContext(null, null)
          .define(TypedAst.VariableId.newFor(TypedAst.EnvKind.FUNCTION, "f"), TEST_VARIABLE);

  private static final TypedAst.PosInterval POSITION =
      new TypedAst.PosInterval(Path.of("TEST"), new ZkAst.PosInterval(-1, -1, -1, -1));

  @Test
  public void coverageForAstToIrTranslatorContextToString() {
    assertThat(TEST_CONTEXT).asString().isEqualTo("{ f: $4_some_var } -> { a: 1 }");
  }

  @Test
  public void contextGetNull() {
    final var variableId = TypedAst.VariableId.newFor(TypedAst.EnvKind.VARIABLE_LET, "some_var");
    assertThat(TEST_CONTEXT.get(variableId)).isNull();
  }

  @Test
  public void coverageForAstToIrTranslatorContextToString2() {
    final AstToIrTranslator.Context test_context = AstToIrTranslator.Context.empty();

    final AstToIrTranslator.Context test_context_2 =
        test_context
            .define(test_context.newTempId(), TEST_CONSTANT)
            .define(test_context.newTempId(), TEST_TUPLE_NESTED);
    assertThat(test_context_2)
        .asString()
        .isEqualTo("{ 0: 1, 1: ($4_some_var, ($4_some_var, 1), $4_some_var as [sbi1; 1]) }");
  }

  @Test
  public void coverageIr() {
    assertThat(new Ir.BlockId(1, "some_block")).asString().isEqualTo("#1_some_block");
    assertThat(Ir.BlockId.RETURN).asString().isEqualTo("#return");
    assertThatCode(
            () -> new Ir.Operation.Extract(new Ir.VariableId(9, "some_var"), Ir.Type.UNIT, -1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Bit offset (-1) must be non-negative");
  }

  @Test
  public void coverageForIrToMcEnvGet() {
    final var env = new IrToMetaCircuitTranslator.Environment();
    assertThatCode(() -> env.get(new Ir.VariableId(9, "some_var")))
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("Malformed IR")
        .hasMessageContaining("$9_some_var");
  }

  @Test
  public void coverageNextBlockInfo() {
    final var blockInfo = new AstToIrTranslator.BranchEnvInfo(List.of(), List.of(), TEST_CONTEXT);
    assertThat(blockInfo.context()).isEqualTo(TEST_CONTEXT);
  }

  @Test
  public void getStructDefinitionTest() {
    final TypedAst.TypeId t3 = TypedAst.TypeId.newFor("crate3", "SomeType");
    final var env = new AstToIrTranslator.Environments(Map.of(), Map.of(), Map.of());
    assertThatCode(() -> env.getStructDefinition(t3))
        .isInstanceOf(InternalCompilerException.class)
        .hasMessage("Could not find struct with type id 'crate3::SomeType'");
  }

  @Test
  public void hiddenVariableId() {
    final int rawId = 4321;
    final var v = new AstToIrTranslator.TemporaryVariableId(rawId);
    assertThat(v.rawId()).isEqualTo(rawId);
  }

  @Test
  public void coverageTupleOperand() {
    assertThat(TEST_TUPLE).asString().isEqualTo("($4_some_var, 1)");

    assertThatCode(() -> TEST_TUPLE.expectBasic())
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("($4_some_var, 1) is not Basic");
    assertThatCode(() -> TEST_TUPLE.expectArray())
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("($4_some_var, 1) is not Array");

    assertThatCode(() -> TEST_VARIABLE.expectTuple())
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("$4_some_var is not Tuple");

    assertThatCode(() -> TEST_VARIABLE.expectConstant())
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("$4_some_var is not Constant");
  }

  @Test
  public void operandType() {
    assertThat(TEST_CONSTANT.type()).isInstanceOf(Ir.Type.PublicInteger.class);
    assertThat(TEST_VARIABLE.type()).isInstanceOf(Ir.Type.SecretBinaryInteger.class);
    assertThatCode(() -> TEST_TUPLE.operandType().expectBasic())
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("(sbi1, i1) is not BasicType");
    assertThatCode(() -> TEST_ARRAY.operandType().expectBasic())
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("[sbi1; 1] is not BasicType");
    assertThatCode(() -> TEST_TUPLE.operandType().expectArray())
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("(sbi1, i1) is not ArrayType");
    assertThat(TEST_TUPLE.operandType()).asString().isEqualTo("(sbi1, i1)");
    assertThatCode(() -> new OperandType.ArrayType(new OperandType.BasicType(Ir.Type.Sbi32), -2))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("OperandType.ArrayType cannot have negative size -2");
  }

  @Test
  public void translateStructuralEquality() {
    assertThatCode(
            () ->
                AstToIrTranslator.translateStructuralEquality(
                    null, null, TEST_CONSTANT, TEST_VARIABLE, null))
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("Expected operands to possess same type");

    assertThatCode(
            () ->
                AstToIrTranslator.translateStructuralEquality(
                    null, null, TEST_CONSTANT, TEST_TUPLE, null))
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("Expected operands to possess same type");

    assertThatCode(
            () ->
                AstToIrTranslator.translateStructuralEquality(
                    null, null, TEST_TUPLE, TEST_TUPLE_NESTED, null))
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("Expected operands to possess same type");
  }

  @Test
  public void newBlockCoverage() {
    final InstructionAccum insnAccum = new InstructionAccum();
    insnAccum.newBlock(new Ir.BlockId(0, "some_block"), List.of(), true);
    assertThatCode(
            () -> insnAccum.newBlock(new Ir.BlockId(1, "some_other_block"), List.of(), false))
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("Starting new block before closing the previous block");

    final Ir.Terminator returnUnit =
        new Ir.BranchAlways(new Ir.BranchInfo(Ir.BlockId.RETURN, List.of()));

    insnAccum.addTerminator(returnUnit);
    assertThatCode(() -> insnAccum.addTerminator(returnUnit))
        .isInstanceOf(InternalCompilerException.class)
        .hasMessageContaining("Ending a block that haven't been started");
  }

  @Test
  public void typedAst() {
    new TypedAst();
  }

  @Test
  public void typedAstToString() {
    assertThat(TypedAst.VariableId.newFor(TypedAst.EnvKind.VARIABLE_LET, "abc", "xyz"))
        .asString()
        .isEqualTo("Variable abc::xyz");
    assertThat(TypedAst.TypeId.newFor("abc", "xyz")).asString().isEqualTo("abc::xyz");
  }

  @Test
  public void replaceGenericsInType() {
    final TypedAst.TypeId t2 = TypedAst.TypeId.newFor("crate2", "some_id");
    final TypedAst.TypeId vec = TypedAst.TypeId.newFor("std", "vec", "Vec");

    final TypeReplacementMap replacementMap =
        new TypeReplacementMap(Map.of(t2, TypedAst.Type.Int.USIZE));

    final TypedAst.Type inputTupleType =
        new TypedAst.Type.Tuple(
            List.of(
                new TypedAst.Type.NamedType(t2, List.of()),
                new TypedAst.Type.NamedType(
                    vec, List.of(new TypedAst.Type.NamedType(t2, List.of())))));

    final TypedAst.Type outputTupleType =
        new TypedAst.Type.Tuple(
            List.of(
                TypedAst.Type.Int.USIZE,
                new TypedAst.Type.NamedType(vec, List.of(TypedAst.Type.Int.USIZE))));

    assertThat(replacementMap.replaceParametersInType(inputTupleType)).isEqualTo(outputTupleType);
  }

  @Test
  public void addExtractOptimization() {
    final InstructionAccum insnAccum = new InstructionAccum();
    insnAccum.newBlock(new Ir.BlockId(0, "some_block"), List.of(), true);

    assertThat(
            OperandUtil.addExtract(
                insnAccum, TEST_CONSTANT.type(), TmpName.root(""), TEST_CONSTANT, TEST_CONSTANT))
        .isNotEqualTo(TEST_CONSTANT);
    assertThat(
            OperandUtil.addExtract(
                insnAccum, TEST_CONSTANT.type(), TmpName.root(""), TEST_VARIABLE, ZERO_CONSTANT))
        .isNotEqualTo(TEST_VARIABLE);
  }

  @Test
  public void translatePlaceUnknownBinop() {
    final var ast1 =
        new TypedAst.TypedExpression(
            TypedAst.Expression.TupleConstructor.UNIT,
            TypedAst.Type.Tuple.UNIT,
            false,
            false,
            POSITION);
    final var ast =
        new TypedAst.TypedExpression(
            new TypedAst.Expression.Binary(ZkAst.Binop.RANGE, ast1, ast1),
            TypedAst.Type.Tuple.UNIT,
            false,
            false,
            POSITION);
    assertThatCode(() -> AstToIrTranslator.translatePlace(ast, null, null, null, null))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining("translatePlace not implemented for Binary.");
  }

  @Test
  public void addAddOptimizationMiss() {
    final InstructionAccum insnAccum = new InstructionAccum();
    insnAccum.newBlock(new Ir.BlockId(0, "some_block"), List.of(), true);

    assertThat(OperandUtil.addAdd(insnAccum, TmpName.root("a"), TEST_VARIABLE, TEST_CONSTANT))
        .isInstanceOf(Operand.Variable.class);
    assertThat(OperandUtil.addAdd(insnAccum, TmpName.root("a"), TEST_CONSTANT, TEST_VARIABLE))
        .isInstanceOf(Operand.Variable.class);
  }

  @Test
  public void operandConstantNeverNegative() {
    assertThatCode(
            () -> new Operand.Constant(BigInteger.valueOf(-1), Ir.Type.I32, TmpName.root("what")))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Operand.Constant does not allow negative values: -1");
  }

  @Test
  public void irConstantNeverNegative() {
    assertThatCode(() -> new Ir.Operation.Constant(Ir.Type.I32, BigInteger.valueOf(-1)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Ir.Operation.Constant does not allow negative values: -1");
  }

  @Test
  public void unsignedBigIntegerToSignedIntTest() {
    assertThat(IrToMetaCircuitTranslator.unsignedBigIntegerToSignedInt(BigInteger.ZERO))
        .isEqualTo(0);
    assertThat(IrToMetaCircuitTranslator.unsignedBigIntegerToSignedInt(BigInteger.ONE))
        .isEqualTo(1);
    assertThat(
            IrToMetaCircuitTranslator.unsignedBigIntegerToSignedInt(new BigInteger("2147483647")))
        .isEqualTo(2147483647);
    assertThat(
            IrToMetaCircuitTranslator.unsignedBigIntegerToSignedInt(new BigInteger("2147483648")))
        .isEqualTo(-2147483648);
    assertThat(
            IrToMetaCircuitTranslator.unsignedBigIntegerToSignedInt(new BigInteger("4294967295")))
        .isEqualTo(-1);
  }

  @Test
  public void unsignedBigIntegerToSignedIntTooLargeTest() {
    assertThatCode(
            () ->
                IrToMetaCircuitTranslator.unsignedBigIntegerToSignedInt(
                    new BigInteger("4294967296")))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Value does not fit into integer: 4294967296");
  }
}
