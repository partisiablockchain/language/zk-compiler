package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests for ResourceList. */
final class TestResourceList {

  private static final Path TEST_DIRECTORY = Path.of("target/classes/test/");
  String propertiesName = "test-git.properties";
  File testDirectory;
  File propertiesFile;

  @BeforeEach
  void setUp() throws IOException {
    testDirectory = new File(TEST_DIRECTORY.toString());
    propertiesFile = new File(TEST_DIRECTORY + "/" + propertiesName);
    if (!Files.exists(TEST_DIRECTORY)) {
      Files.createDirectories(TEST_DIRECTORY);
    }
  }

  @AfterEach
  void tearDown() {
    propertiesFile.delete();
    testDirectory.delete();
  }

  @Test
  void testGetGitProperties() {
    WithResource.accept(
        () -> new FileOutputStream(propertiesFile),
        fileOutputStream -> new Properties().store(fileOutputStream, null),
        "Could not store properties in file: " + propertiesFile);

    ArrayList<String> propertiesVisited = new ArrayList<>();
    ResourceList.consumeGitProperties((name, properties) -> propertiesVisited.add(name));
    assertThat(propertiesVisited)
        .contains(propertiesName, "com.secata.tools.coverage-git.properties");
  }

  @Test
  void testConsumeProperties() {
    WithResource.accept(
        () -> new FileOutputStream(propertiesFile),
        fileOutputStream -> new Properties().store(fileOutputStream, null),
        "Could not store properties in file: " + propertiesFile);

    ArrayList<String> propertiesVisited = new ArrayList<>();
    ResourceList.consumePropertiesFromResources(
        (name, property) -> propertiesVisited.add(name), Pattern.compile(".*" + propertiesName));
    assertThat(propertiesVisited).containsExactly(propertiesName);
  }

  @Test
  void testPropertiesContainGitInfo() {
    List<Properties> propertiesNames = new ArrayList<>();
    ResourceList.consumeGitProperties((name, property) -> propertiesNames.add(property));

    Properties zkProperties = propertiesNames.get(0);
    // Expected entries in git.properties.
    assertThat(zkProperties.getProperty("git.build.time")).isNotNull();
    assertThat(zkProperties.getProperty("git.build.version")).isNotNull();
    assertThat(zkProperties.getProperty("git.commit.id.abbrev")).isNotNull();
  }
}
