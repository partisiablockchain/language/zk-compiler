package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcompiler.TypeCheckInformationFlow.SecurityLabel;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link TypeCheckInformationFlow}. */
public final class TypeCheckInformationFlowTest {

  /** {@link SecurityLabel#leastUpperBound} must satisfy {@link Lattice} properties. */
  @Test
  public void securityLabelLeastUpperBound() {
    Assertions.assertThat(SecurityLabel.PUBLIC.leastUpperBound(SecurityLabel.PUBLIC))
        .isEqualTo(SecurityLabel.PUBLIC);
    Assertions.assertThat(SecurityLabel.PUBLIC.leastUpperBound(SecurityLabel.SECRET))
        .isEqualTo(SecurityLabel.SECRET);
    Assertions.assertThat(SecurityLabel.UNKNOWN.leastUpperBound(SecurityLabel.SECRET))
        .isEqualTo(SecurityLabel.UNKNOWN);
    Assertions.assertThat(SecurityLabel.PUBLIC.leastUpperBound(SecurityLabel.UNKNOWN))
        .isEqualTo(SecurityLabel.UNKNOWN);
  }

  /**
   * {@link SecurityLabel#flowsTo} mostly satisfies flows to relation, with the exception of the
   * {@code UNKNOWN} label which has special behaviour.
   */
  @Test
  public void securityLabelFlowsTo() {
    Assertions.assertThat(SecurityLabel.PUBLIC.flowsTo(SecurityLabel.PUBLIC)).isTrue();
    Assertions.assertThat(SecurityLabel.PUBLIC.flowsTo(SecurityLabel.SECRET)).isTrue();
    Assertions.assertThat(SecurityLabel.SECRET.flowsTo(SecurityLabel.SECRET)).isTrue();
    Assertions.assertThat(SecurityLabel.SECRET.flowsTo(SecurityLabel.PUBLIC)).isFalse();
    Assertions.assertThat(SecurityLabel.UNKNOWN.flowsTo(SecurityLabel.SECRET)).isFalse();
    Assertions.assertThat(SecurityLabel.PUBLIC.flowsTo(SecurityLabel.UNKNOWN)).isFalse();
    Assertions.assertThat(SecurityLabel.UNKNOWN.flowsTo(SecurityLabel.UNKNOWN)).isFalse();
  }

  /** Error type must produce an unknwon security label. */
  @Test
  public void securityLabelOfErrorType() {
    Assertions.assertThat(
            TypeCheckInformationFlow.securityLabelOf(new TypedAst.Type.ErrorType(), null))
        .isEqualTo(SecurityLabel.UNKNOWN);
  }
}
