package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.zkcompiler.BuiltinFunctionsSbi.createSbiFromDefinition;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

/** Storage class for certain built-in functions. */
public final class BuiltinFunctionsTest {

  @Test
  public void coverageTest() {
    final TypedAst.Type.Sbi type = new TypedAst.Type.Sbi(5, true);
    assertThat(BuiltinFunctionsSbi.createSbiMethods(type).toList()).hasSize(3).doesNotContainNull();
    assertThat(BuiltinFunctionsSbi.createSbiTypeDefinition(type)).isNotNull();
    assertThat(BuiltinFunctionsSbi.sbiTypes()).hasSize(12).doesNotContainNull();
  }

  @Test
  public void checkThatStructsAreIncluded() {
    final var module = BuiltinFunctions.lookupModule(new NamePath.Absolute("std", "ops"));
    assertThat(module.structItems()).isNotEmpty();
  }

  /**
   * If we create an "SbiN::from" definition, with N = 1, the argument type is an unsigned integer.
   */
  @Test
  public void sbi1ArgumentUnsigned() {
    var actualType =
        createSbiFromDefinition(TypedAst.Type.Sbi.BOOL)
            .typeInfo()
            .valueParameterTypesWithTypeParameters()
            .get(0);

    assertThat(actualType).isEqualTo(TypedAst.Type.Int.BOOL);
  }

  /** If we create an "SbiN::from" definition, with N > 1, the argument type is a signed integer. */
  @Test
  public void sbiArgumentSigned() {
    final List<Integer> sizes = List.of(8, 16, 32, 64, 128);
    for (var size : sizes) {
      final var inputType = new TypedAst.Type.Sbi(size, true);
      final var actualType =
          createSbiFromDefinition(inputType)
              .typeInfo()
              .valueParameterTypesWithTypeParameters()
              .get(0);
      final var expectedType = new TypedAst.Type.Int(size, true);

      assertThat(actualType).isEqualTo(expectedType);
    }
  }
}
