package com.partisiablockchain.language.zkcompiler;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.immutabledetect.ImmutableDetect;
import com.partisiablockchain.language.zkcompiler.ir.Ir;
import com.partisiablockchain.language.zkcompiler.ir.IrPretty;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitPretty;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class TranslateToReferenceTest {

  static boolean shouldRegenerateReferences() {
    final var ref = System.getenv("REGENERATE_REFERENCES");
    return ref != null && !ref.isEmpty();
  }

  private static int sourceCodeSize(final ProgramInput programInput) {
    return programInput.files().stream()
        .map(ProgramInput.FileInput::rustSourceCode)
        .mapToInt(String::length)
        .sum();
  }

  @ParameterizedTest
  @MethodSource("provideTestCasesEvaluatable")
  public void translateCircuits(final ExamplePrograms.TestCase testCase) {
    // Debug consumer for hooking into ZkCompiler
    final var validationsCalled = new ArrayList<String>();

    // Load code
    final ProgramInput programInput = ExamplePrograms.loadProgramInput(testCase);

    final boolean assertImmutableDetect = sourceCodeSize(programInput) < 100_000;

    final TranslationConfig config =
        new TranslationConfig(
            new TestHook(validationsCalled, testCase, assertImmutableDetect), null, false, false);

    // Perform compilation
    ExceptionConverter.run(
        () -> ZkCompiler.compileRustToMetaCircuitBytecode(programInput, config),
        testCase.filename());

    assertThat(validationsCalled)
        .as("Expected all validation lambdas to be called")
        .containsExactly(
            "TypedAst Validation",
            "IR (Initial) Validation",
            "IR (Final) Validation",
            "Meta Circuit Validation");
  }

  /**
   * Validates compiled intermediate representations against their respective reference file and
   * adds information to validationsCalled to test the emissions from the translation hook.
   * Regenerates the reference files if needed.
   *
   * @param validationsCalled list of information for validation.
   * @param testCase the test to run.
   * @param assertImmutableDetect flag for detecting immutable structure.
   */
  record TestHook(
      ArrayList<String> validationsCalled,
      ExamplePrograms.TestCase testCase,
      boolean assertImmutableDetect)
      implements TranslationHooks {

    @Override
    public void debugTypedAst(TypedAst.Program program) {
      // Check that data structure is immutable
      if (assertImmutableDetect) {
        assertThat(program)
            .as("typed ast")
            .isNotNull()
            .matches(ImmutableDetect::isImmutable, "is structurally immutable");
      }

      validationsCalled.add("TypedAst Validation");
    }

    @Override
    public void debugIrInitial(Ir.Circuit circuit) {
      // Check that data structure is immutable
      if (assertImmutableDetect) {
        assertThat(circuit).matches(ImmutableDetect::isImmutable, "is structurally immutable");
      }

      // Test path for first ir.
      final Path irFilepath =
          getResourceSourcePath(ExamplePrograms.class, "ref/" + testCase.filename() + ".first.ir");
      final String irCircuitPretty = IrPretty.pretty(circuit);

      if (shouldRegenerateReferences()) {
        writeText(irFilepath, irCircuitPretty);
        System.err.println("Regenerated " + irFilepath);
      }

      // Validate compiled ir circuit is as expected
      final String irCircuitPrettyExpected = loadText(irFilepath);
      assertThat(irCircuitPretty)
          .describedAs(irFilepath.toString())
          .isEqualToNormalizingNewlines(irCircuitPrettyExpected);

      validationsCalled.add("IR (Initial) Validation");
    }

    @Override
    public void debugMetaCircuit(MetaCircuit metaCircuit) {

      // Check that data structure is immutable
      if (assertImmutableDetect) {
        assertThat(metaCircuit).matches(ImmutableDetect::isImmutable, "is structurally immutable");
      }

      // Test path for metacircuit.
      final Path mcFilepath =
          getResourceSourcePath(ExamplePrograms.class, "ref/" + testCase.filename() + ".mc");

      final String metaCircuitPretty = MetaCircuitPretty.pretty(metaCircuit);
      if (shouldRegenerateReferences()) {
        writeText(mcFilepath, metaCircuitPretty);
        System.err.println("Regenerated " + mcFilepath);
      }

      // Validate compiled metaCircuit is as expected
      final String metaCircuitPrettyExpected = loadText(mcFilepath);
      assertThat(metaCircuitPretty)
          .describedAs(mcFilepath.toString())
          .isEqualToNormalizingNewlines(metaCircuitPrettyExpected);

      validationsCalled.add("Meta Circuit Validation");
    }

    @Override
    public void debugIrPass(
        PassInformation pass, Ir.Circuit circuit, boolean isDifferentFromPrevious) {

      // Check that data structure is immutable
      if (assertImmutableDetect) {
        assertThat(circuit)
            .as("%d %s".formatted(pass.passNumber(), pass.irPass().getName()))
            .matches(ImmutableDetect::isImmutable, "is structurally immutable");
      }

      if (!pass.isFinalPass()) {
        return;
      }

      // Test path for final ir
      final Path irOptFilepath =
          getResourceSourcePath(ExamplePrograms.class, "ref/" + testCase.filename() + ".final.ir");

      final String irCircuitPretty = IrPretty.pretty(circuit);

      if (shouldRegenerateReferences()) {
        writeText(irOptFilepath, irCircuitPretty);
        System.err.println("Regenerated " + irOptFilepath);
      }

      // Validate compiled ir circuit is as expected
      final String irCircuitPrettyExpected = loadText(irOptFilepath);
      assertThat(irCircuitPretty)
          .describedAs(irOptFilepath.toString())
          .isEqualToNormalizingNewlines(irCircuitPrettyExpected);

      validationsCalled.add("IR (Final) Validation");
    }
  }

  // Argument providers

  static Stream<Arguments> provideTestCasesEvaluatable() {
    return ExamplePrograms.programsAtStage(ExamplePrograms.Stage.Evaluate).stream()
        .map(Arguments::of);
  }

  // Utility

  public static Path getResourceSourcePath(final Class<?> clazz, final String filename) {
    final String[] split = clazz.getPackageName().split("\\.");
    final Path referencePathDir = Paths.get("./src/test/resources/", split);
    return referencePathDir.resolve(filename);
  }

  public static void writeText(final Path filePath, final String text) {
    WithResource.accept(
        () -> {
          // Create any directories required
          Files.createDirectories(filePath.getParent());

          // Write file itself
          return new java.io.PrintWriter(filePath.toFile(), StandardCharsets.UTF_8);
        },
        outputStream -> outputStream.write(text),
        "Could not write to file " + filePath);
  }

  public static String loadText(final Path filePath) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> new FileInputStream(filePath.toFile()),
            InputStream::readAllBytes,
            "Could not load file: " + filePath);
    return new String(fileBytes, StandardCharsets.UTF_8);
  }

  public static void assertIrCircuitInvariants(final Ir.Circuit circuit, final String description) {
    assertThat(determineDuplicatedVariableIds(circuit)).as(description).isEmpty();
  }

  /**
   * Determines a collection of variable ids that are assigned to multiple times in the given
   * circuit.
   *
   * @param circuit Circuit to determine duplicates for.
   * @return Collection of duplicated variable ids, or empty.
   */
  static Collection<Ir.VariableId> determineDuplicatedVariableIds(final Ir.Circuit circuit) {
    // Determine all variable assignments by instructions
    final Stream<Ir.VariableId> idsAsInstructionResults =
        circuit.functions().stream()
            .map(Ir.Function::blocks)
            .flatMap(List::stream)
            .map(Ir.Block::instructions)
            .flatMap(List::stream)
            .map(Ir.Instruction::resultId);

    // Determine all variable assignments by inputs
    final Stream<Ir.VariableId> idsAsInputs =
        circuit.functions().stream()
            .map(Ir.Function::blocks)
            .flatMap(List::stream)
            .map(Ir.Block::inputs)
            .flatMap(List::stream)
            .map(Ir.Input::assignedVariable);

    // Determine number of times each variable have been assigned
    final Map<Ir.VariableId, Long> idsToOccuranceCount =
        Stream.concat(idsAsInstructionResults, idsAsInputs)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    // Emit list of variables that have been assigned multiple times.
    return idsToOccuranceCount.entrySet().stream()
        .filter(x -> x.getValue() >= 2)
        .map(Map.Entry::getKey)
        .toList();
  }
}
