//! assert fail_compile "Constant is too large for metacircuit: 1099511627776"
//! assert ignore
use pbc_zk::Sbi64;

pub fn f() -> Sbi64 {
    let a: i64 = 1048576 as i64;
    Sbi64::from(a * a)
}
