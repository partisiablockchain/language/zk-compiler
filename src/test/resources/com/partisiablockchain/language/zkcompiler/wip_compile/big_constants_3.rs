//! assert fail_compile "Constant is too large for metacircuit: 2147483648"
//! assert ignore
use pbc_zk::Sbi64;

pub fn f() -> Sbi64 {
    let a: i64 = 32768 as i64;
    let b = a + a;
    Sbi64::from(a * b)
}
