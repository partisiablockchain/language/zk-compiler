//! assert fail_compile "translatePlace not implemented for ArrayConstructorExplicit. Assign structures to variables before assignment."
use pbc_zk::Sbi32;

pub fn extract_one(x: Sbi32) -> Sbi32 {
    [x, x, x, x][1]
}
