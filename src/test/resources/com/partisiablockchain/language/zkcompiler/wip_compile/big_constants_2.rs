//! assert fail_compile "Constant is too large for metacircuit: 4294967297"
//! assert ignore
use pbc_zk::Sbi64;

pub fn f() -> Sbi64 {
    let a: i64 = 65536 as i64;
    Sbi64::from(a * a + (1 as i64))
}
