//! assert fail_compile
use pbc_zk::Sbi32;

pub fn div(x: Sbi32) -> Sbi32 {
    x / Sbi32::from(2)
}
