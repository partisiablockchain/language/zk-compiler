//! assert fail_compile "Malformed IR: Could not find temporary variable '$3_counts_IDX'"

use pbc_zk::*;

pub fn sum_everything(mut counts: [Sbi8; 8], val: bool) -> [Sbi8; 8] {
    counts[if val { 0 } else { 0 }] = Sbi8::from(0xffu8 as i8);
    counts[if val { 7 } else { 7 }] = Sbi8::from(0xffu8 as i8);
    counts
}
