//! assert fail_compile "All component types must be of the same type, but was: [PublicInteger[width=32], PublicInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32], SecretBinaryInteger[width=32]]"
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> u32 {
    let mut vec: Vec<Sbi32> = make_vec(x);
    Vec::len::<Sbi32>(vec)
}

fn make_vec(x: Sbi32) -> Vec<Sbi32> {
    let mut vec: Vec<Sbi32> = Vec::new::<Sbi32>();
    Vec::push::<Sbi32>(vec, x);
    Vec::push::<Sbi32>(vec, x);
    vec
}

pbc_zk::test_eq!(test(Sbi32::from(7)), 2);
