//! assert fail_compile "Calls with tuple return types: [SecretBinaryInteger[width=32], SecretBinaryInteger[width=32]]"
use pbc_zk::Sbi32;

pub fn test() -> Sbi32 {
    let max_min = maxmin(Sbi32::from(93), Sbi32::from(39));
    max_min.0 + max_min.1
}

fn maxmin(x: Sbi32, y: Sbi32) -> (Sbi32, Sbi32) {
    if x > y {
        (x, y)
    } else {
        (y, x)
    }
}
