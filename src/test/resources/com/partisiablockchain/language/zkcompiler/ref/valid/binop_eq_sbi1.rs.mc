(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi32 $111 (extract $1 32 0))
    (branch-always #return $111))))