(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi32 $1 (extract $0 32 0))
    (sbi32 $2 (extract $0 32 32))
    (sbi64 $3 (bit_concat $2 $1))
    (call
      (%1 $3)
      (#100)))
  (block #100
    (inputs
      (sbi32 $4))
    (branch-always #return $4)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi32 $1 (extract $0 32 0))
    (branch-always #return $1))))