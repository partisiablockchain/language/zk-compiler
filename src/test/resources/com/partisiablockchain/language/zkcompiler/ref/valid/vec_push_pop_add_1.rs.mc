(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (i32 $5 (constant 32))
    (sbi1024 $7 (constant 0))
    (sbi1024 $8 (insertdyn $7 $0 $1))
    (sbi32 $10 (constant 2))
    (sbi1024 $15 (insertdyn $8 $10 $5))
    (sbi32 $20 (extractdyn $15 32 $5))
    (sbi32 $25 (extractdyn $15 32 $1))
    (sbi32 $26 (add_wrapping $20 $25))
    (branch-always #return $26))))