(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (call
      (%1 $0 $1)
      (#100)))
  (block #100
    (inputs
      (sbi64 $2))
    (sbi32 $3 (extract $2 32 0))
    (branch-always #return $3)))
 (function %1
  (output sbi64)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (i32 $2 (constant 0))
    (sbi32 $3 (constant 0))
    (sbi1 $4 (equal $1 $3))
    (branch-always #1 $2 $4 $3 $3 $0 $1))
  (block #1
    (inputs
      (i32 $10)
      (sbi1 $13)
      (sbi32 $14)
      (sbi32 $15)
      (sbi32 $16)
      (sbi32 $17))
    (i32 $12 (constant 32))
    (i1 $18 (equal $10 $12))
    (i1 $19 (bitwise_not $18))
    (i32 $20 (constant 1))
    (i32 $21 (add_wrapping $10 $20))
    (branch-if $19
      (0 #3 $13 $14 $15)
      (1 #2 $10 $21 $13 $14 $15 $16 $17)))
  (block #2
    (inputs
      (i32 $22)
      (i32 $23)
      (sbi1 $25)
      (sbi32 $26)
      (sbi32 $27)
      (sbi32 $28)
      (sbi32 $29))
    (i32 $35 (constant 1))
    (i32 $38 (constant 31))
    (i32 $39 (negate $22))
    (i32 $40 (add_wrapping $38 $39))
    (sbi32 $42 (bitshift_left_logical $27 $35))
    (call
      (%3 $28 $40)
      (#103 $23 $25 $26 $28 $29 $40 $42)))
  (block #103
    (inputs
      (i32 $435)
      (sbi1 $437)
      (sbi32 $438)
      (sbi32 $440)
      (sbi32 $441)
      (i32 $447)
      (sbi32 $448)
      (sbi1 $43))
    (i32 $44 (constant 0))
    (call
      (%2 $448 $44 $43)
      (#104 $435 $437 $438 $440 $441 $447)))
  (block #104
    (inputs
      (i32 $450)
      (sbi1 $452)
      (sbi32 $453)
      (sbi32 $455)
      (sbi32 $456)
      (i32 $462)
      (sbi32 $45))
    (sbi1 $46 (less_than_signed $45 $456))
    (sbi32 $68 (negate $456))
    (sbi32 $69 (add_wrapping $45 $68))
    (sbi1 $72 (constant 1))
    (call
      (%2 $453 $462 $72)
      (#105 $450 $452 $453 $455 $456 $45 $46 $69)))
  (block #105
    (inputs
      (i32 $467)
      (sbi1 $469)
      (sbi32 $470)
      (sbi32 $472)
      (sbi32 $473)
      (sbi32 $483)
      (sbi1 $484)
      (sbi32 $487)
      (sbi32 $73))
    (sbi32 $225 (select $484 $470 $73))
    (sbi32 $226 (select $484 $483 $487))
    (branch-always #1 $467 $469 $225 $226 $472 $473))
  (block #3
    (inputs
      (sbi1 $30)
      (sbi32 $31)
      (sbi32 $32))
    (sbi32 $112 (constant 0))
    (sbi32 $227 (select $30 $112 $31))
    (sbi64 $125 (bit_concat $32 $227))
    (branch-always #return $125)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1)
      (sbi1 $2))
    (call
      (%4 $0)
      (#100 $1 $2)))
  (block #100
    (inputs
      (i32 $491)
      (sbi1 $492)
      (sbi32 $3))
    (sbi32 $6 (insertdyn $3 $492 $491))
    (call
      (%5 $6)
      (#101)))
  (block #101
    (inputs
      (sbi32 $7))
    (branch-always #return $7)))
 (function %3
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (call
      (%4 $0)
      (#100 $1)))
  (block #100
    (inputs
      (i32 $500)
      (sbi32 $2))
    (sbi1 $5 (extractdyn $2 1 $500))
    (branch-always #return $5)))
 (function %4
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $2 $0))
  (block #1
    (inputs
      (i32 $36)
      (sbi32 $40)
      (sbi32 $41))
    (i32 $38 (constant 32))
    (i1 $42 (equal $36 $38))
    (i1 $43 (bitwise_not $42))
    (i32 $44 (constant 1))
    (i32 $45 (add_wrapping $36 $44))
    (branch-if $43
      (0 #return $40)
      (1 #2 $36 $45 $40 $41)))
  (block #2
    (inputs
      (i32 $46)
      (i32 $47)
      (sbi32 $50)
      (sbi32 $51))
    (sbi32 $55 (bitshift_right_logical $51 $46))
    (sbi1 $56 (extract $55 1 0))
    (sbi32 $59 (insertdyn $50 $56 $46))
    (branch-always #1 $47 $59 $51)))
 (function %5
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (sbi32 $7)
      (sbi32 $8))
    (i32 $6 (constant 32))
    (i1 $9 (equal $4 $6))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $4 $11))
    (branch-if $10
      (0 #return $7)
      (1 #2 $4 $12 $7 $8)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi32 $16)
      (sbi32 $17))
    (sbi1 $22 (extractdyn $17 1 $13))
    (sbi31 $23 (constant 0))
    (sbi32 $24 (bit_concat $23 $22))
    (sbi32 $25 (bitshift_left_logical $24 $13))
    (sbi32 $26 (bitwise_xor $16 $25))
    (branch-always #1 $14 $26 $17))))