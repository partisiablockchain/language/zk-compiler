(metacircuit
 (function %0
  (output i8)
  (block #0
    (inputs
      (i32 $0))
    (i8 $1 (extract $0 8 0))
    (branch-always #return $1))))