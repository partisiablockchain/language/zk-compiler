(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi32 $3 (constant 0))
    (sbi1 $4 (equal $0 $3))
    (sbi1 $5 (bitwise_not $4))
    (sbi1 $8 (equal $1 $3))
    (sbi1 $9 (bitwise_not $8))
    (sbi1 $10 (bitwise_or $5 $9))
    (sbi32 $16 (constant 1))
    (sbi32 $121 (select $10 $16 $3))
    (branch-always #return $121))))