(metacircuit
 (function %0
  (output i64)
  (block #0
    (inputs
      (i256 $0))
    (i32 $13 (constant 0))
    (i64 $15 (constant 0))
    (branch-always #1 $13 $15))
  (block #1
    (inputs
      (i32 $17)
      (i64 $20))
    (i32 $19 (constant 32))
    (i1 $22 (equal $17 $19))
    (i1 $23 (bitwise_not $22))
    (i32 $24 (constant 1))
    (i32 $25 (add_wrapping $17 $24))
    (branch-if $23
      (0 #return $20)
      (1 #2 $25 $20)))
  (block #2
    (inputs
      (i32 $27)
      (i64 $29))
    (i32 $36 (constant 0))
    (branch-always #4 $36 $27 $29))
  (block #4
    (inputs
      (i32 $37)
      (i32 $43)
      (i64 $45))
    (i32 $39 (constant 8))
    (i1 $47 (equal $37 $39))
    (i1 $48 (bitwise_not $47))
    (i32 $49 (constant 1))
    (i32 $50 (add_wrapping $37 $49))
    (branch-if $48
      (0 #1 $43 $45)
      (1 #5 $37 $50 $43 $45)))
  (block #5
    (inputs
      (i32 $51)
      (i32 $52)
      (i32 $57)
      (i64 $59))
    (i32 $68 (constant 3))
    (i32 $69 (bitshift_left_logical $51 $68))
    (i8 $70 (extractdyn $59 8 $69))
    (i8 $71 (constant 1))
    (i8 $72 (add_wrapping $70 $71))
    (i64 $75 (insertdyn $59 $72 $69))
    (branch-always #4 $52 $57 $75))))