(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (i8 $0 (constant 0))
    (sbi8 $1 (constant 0))
    (branch-always #1 $0 $1))
  (block #1
    (inputs
      (i8 $3)
      (sbi8 $6))
    (i8 $5 (constant 11))
    (i1 $7 (equal $3 $5))
    (i1 $8 (bitwise_not $7))
    (i8 $9 (constant 1))
    (i8 $10 (add_wrapping $3 $9))
    (branch-if $8
      (0 #return $6)
      (1 #2 $3 $10 $6)))
  (block #2
    (inputs
      (i8 $11)
      (i8 $12)
      (sbi8 $14))
    (sbi8 $16 (cast $11))
    (sbi8 $17 (add_wrapping $14 $16))
    (branch-always #1 $12 $17))))