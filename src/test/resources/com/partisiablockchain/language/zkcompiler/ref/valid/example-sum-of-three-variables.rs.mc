(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1)
      (sbi32 $2))
    (sbi32 $3 (add_wrapping $0 $1))
    (sbi32 $4 (add_wrapping $2 $3))
    (branch-always #return $4))))