(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0)
      (i32 $1))
    (call
      (%1 $0 $1)
      (#100)))
  (block #100
    (inputs
      (i32 $2))
    (branch-always #return $2)))
 (function %1
  (output i32)
  (block #0
    (inputs
      (i32 $0)
      (i32 $1))
    (i32 $2 (constant 0))
    (i1 $3 (equal $1 $2))
    (branch-if $3
      (0 #3 $0 $1)
      (1 #1)))
  (block #1
    (i32 $10 (constant 1000000))
    (branch-always #return $10))
  (block #3
    (inputs
      (i32 $11)
      (i32 $12))
    (i32 $15 (constant 2))
    (i1 $16 (less_than_signed $11 $15))
    (branch-if $16
      (0 #6 $11 $12)
      (1 #return $11)))
  (block #6
    (inputs
      (i32 $19)
      (i32 $20))
    (i32 $24 (constant 1))
    (i32 $25 (constant -1))
    (i32 $26 (add_wrapping $19 $25))
    (i32 $29 (add_wrapping $20 $25))
    (call
      (%1 $26 $29)
      (#106 $24)))
  (block #106
    (inputs
      (i32 $136)
      (i32 $30))
    (i32 $32 (add_wrapping $30 $136))
    (branch-always #return $32))))