(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 1))
    (sbi32 $4 (constant 99))
    (sbi1 $5 (less_than_signed $0 $4))
    (sbi32 $11 (constant 91))
    (sbi1 $12 (less_than_signed $0 $11))
    (sbi32 $18 (constant 31))
    (sbi1 $19 (less_than_signed $18 $0))
    (sbi32 $25 (constant 3))
    (sbi32 $30 (constant 2))
    (sbi32 $143 (select $19 $25 $30))
    (sbi32 $38 (constant 4))
    (sbi32 $144 (select $12 $143 $38))
    (sbi32 $145 (select $5 $144 $2))
    (branch-always #return $145))))