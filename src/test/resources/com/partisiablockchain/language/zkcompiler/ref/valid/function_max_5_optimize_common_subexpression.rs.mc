(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 39))
    (call
      (%1 $2 $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $3))
    (sbi1 $7 (constant 1))
    (branch-always #return $7)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $110 (select $2 $1 $0))
    (branch-always #return $110)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $111 (select $2 $0 $1))
    (branch-always #return $111))))