(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (call
      (%1 $0 $1)
      (#100 $0 $1)))
  (block #100
    (inputs
      (sbi32 $118)
      (i32 $119))
    (i32 $3 (constant 1))
    (call
      (%1 $118 $3)
      (#101 $118 $119 $3)))
  (block #101
    (inputs
      (sbi32 $120)
      (i32 $121)
      (i32 $122))
    (call
      (%1 $120 $122)
      (#102 $120 $121)))
  (block #102
    (inputs
      (sbi32 $123)
      (i32 $124))
    (call
      (%1 $123 $124)
      (#103 $123 $124)))
  (block #103
    (inputs
      (sbi32 $126)
      (i32 $127))
    (i32 $9 (constant 5))
    (call
      (%1 $126 $9)
      (#104 $126 $127)))
  (block #104
    (inputs
      (sbi32 $129)
      (i32 $130))
    (call
      (%1 $129 $130)
      (#105)))
  (block #105
    (branch-always #return)))
 (function %1
  (output)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (i32 $2 (constant 0))
    (branch-always #1 $2 $1 $0))
  (block #1
    (inputs
      (i32 $3)
      (i32 $4)
      (sbi32 $5))
    (i1 $7 (equal $3 $4))
    (i1 $8 (bitwise_not $7))
    (i32 $9 (constant 1))
    (i32 $10 (add_wrapping $3 $9))
    (branch-if $8
      (0 #return)
      (1 #2 $10 $4 $5)))
  (block #2
    (inputs
      (i32 $12)
      (i32 $13)
      (sbi32 $14))
    (i0 $18 (output $14))
    (branch-always #1 $12 $13 $14))))