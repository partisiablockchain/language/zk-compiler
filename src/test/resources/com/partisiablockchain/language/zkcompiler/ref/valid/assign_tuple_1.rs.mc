(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 4))
    (branch-always #return $0 $2))))