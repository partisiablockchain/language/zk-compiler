(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $4 (constant 3))
    (sbi1 $5 (equal $0 $4))
    (sbi32 $12 (constant 0))
    (sbi32 $16 (constant 1337))
    (sbi32 $116 (select $5 $16 $12))
    (branch-always #return $116))))