(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $4 (constant 2))
    (sbi32 $6 (constant 3))
    (sbi1 $7 (equal $0 $6))
    (sbi32 $118 (select $7 $6 $4))
    (branch-always #return $118))))