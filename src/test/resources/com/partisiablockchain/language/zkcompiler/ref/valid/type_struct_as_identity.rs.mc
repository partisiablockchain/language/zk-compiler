(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi8 $1 (extract $0 8 0))
    (sbi8 $2 (extract $0 8 8))
    (sbi8 $3 (extract $0 8 16))
    (sbi8 $4 (extract $0 8 24))
    (sbi16 $5 (bit_concat $2 $1))
    (sbi16 $6 (bit_concat $4 $3))
    (sbi32 $7 (bit_concat $6 $5))
    (branch-always #return $7))))