(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (branch-always #2))
  (block #2
    (sbi32 $16 (constant 15))
    (branch-always #return $16))))