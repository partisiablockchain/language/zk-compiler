(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi8 $1 (extract $0 8 48))
    (branch-always #return $1))))