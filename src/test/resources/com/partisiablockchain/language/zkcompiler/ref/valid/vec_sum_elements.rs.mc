(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (i32 $5 (constant 32))
    (sbi1024 $7 (constant 0))
    (sbi1024 $8 (insertdyn $7 $0 $1))
    (sbi32 $10 (constant 2))
    (sbi1024 $15 (insertdyn $8 $10 $5))
    (i32 $19 (constant 64))
    (sbi1024 $20 (insertdyn $15 $0 $19))
    (i32 $24 (constant 96))
    (sbi1024 $25 (insertdyn $20 $0 $24))
    (i32 $26 (constant 5))
    (sbi32 $27 (constant 5))
    (i32 $31 (constant 128))
    (sbi1024 $32 (insertdyn $25 $27 $31))
    (sbi32 $34 (constant 0))
    (branch-always #1 $1 $26 $32 $34))
  (block #1
    (inputs
      (i32 $36)
      (i32 $38)
      (sbi1024 $41)
      (sbi32 $42))
    (i32 $37 (constant 5))
    (i1 $44 (equal $36 $37))
    (i1 $45 (bitwise_not $44))
    (i32 $46 (constant 1))
    (i32 $47 (add_wrapping $36 $46))
    (branch-if $45
      (0 #return $42)
      (1 #2 $47 $38 $41 $42)))
  (block #2
    (inputs
      (i32 $49)
      (i32 $51)
      (sbi1024 $53)
      (sbi32 $54))
    (i32 $61 (constant -1))
    (i32 $62 (add_wrapping $51 $61))
    (i32 $63 (constant 32))
    (i32 $64 (mult_wrapping_signed $62 $63))
    (sbi32 $65 (extractdyn $53 32 $64))
    (sbi32 $66 (add_wrapping $54 $65))
    (branch-always #1 $49 $62 $53 $66))))