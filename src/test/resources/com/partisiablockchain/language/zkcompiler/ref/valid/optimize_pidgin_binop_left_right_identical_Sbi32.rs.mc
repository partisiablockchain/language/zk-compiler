(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi1 $4 (constant 0))
    (sbi1 $5 (constant 1))
    (sbi1 $6 (bitwise_not $5))
    (sbi1 $10 (bitwise_not $4))
    (sbi32 $11 (add_wrapping $0 $0))
    (sbi1 $20 (bitwise_not $6))
    (sbi1 $21 (bitwise_and $10 $20))
    (sbi1 $22 (bitwise_and $5 $21))
    (sbi1 $24 (bitwise_and $5 $10))
    (sbi1 $25 (bitwise_and $22 $24))
    (i32 $26 (constant 1))
    (sbi32 $27 (bitshift_left_logical $0 $26))
    (sbi1 $28 (equal $11 $27))
    (sbi1 $29 (bitwise_and $25 $28))
    (branch-always #return $29))))