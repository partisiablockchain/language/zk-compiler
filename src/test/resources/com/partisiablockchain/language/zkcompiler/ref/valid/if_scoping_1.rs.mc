(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi32 $3 (constant 0))
    (sbi32 $5 (constant 10))
    (sbi1 $6 (less_than_signed $0 $5))
    (sbi32 $14 (constant 1))
    (sbi32 $136 (select $6 $14 $3))
    (sbi1 $24 (less_than_signed $1 $5))
    (sbi32 $237 (constant 0))
    (sbi32 $238 (select $24 $14 $237))
    (sbi32 $137 (add_wrapping $136 $238))
    (branch-always #return $137))))