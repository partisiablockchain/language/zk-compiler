(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi0 $0)
      (sbi0 $1))
    (sbi1 $4 (constant 1))
    (branch-always #return $4))))