(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i352 $0)
      (i352 $1))
    (i1 $2 (equal $0 $1))
    (branch-always #return $2))))