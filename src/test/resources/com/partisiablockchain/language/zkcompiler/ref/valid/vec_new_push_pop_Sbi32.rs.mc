(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (sbi1024 $7 (constant 0))
    (sbi1024 $8 (insertdyn $7 $0 $1))
    (sbi32 $13 (extractdyn $8 32 $1))
    (branch-always #return $13))))