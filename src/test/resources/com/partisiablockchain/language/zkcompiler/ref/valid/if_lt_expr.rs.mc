(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 3))
    (sbi1 $3 (less_than_signed $2 $0))
    (sbi32 $7 (constant 1337))
    (sbi32 $11 (constant 0))
    (sbi32 $111 (select $3 $7 $11))
    (branch-always #return $111))))