(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i128 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (sbi32 $7)
      (i128 $8))
    (i32 $6 (constant 8))
    (i1 $9 (equal $4 $6))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $4 $11))
    (branch-if $10
      (0 #return $7)
      (1 #2 $4 $12 $7 $8)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi32 $16)
      (i128 $17))
    (i32 $20 (constant 4))
    (i32 $21 (bitshift_left_logical $13 $20))
    (i16 $22 (extractdyn $17 16 $21))
    (i8 $23 (extract $22 8 0))
    (i8 $24 (extract $22 8 8))
    (i24 $25 (constant 0))
    (i32 $26 (bit_concat $25 $23))
    (sbi32 $27 (cast $26))
    (sbi32 $28 (add_wrapping $16 $27))
    (i32 $30 (bit_concat $25 $24))
    (sbi32 $31 (cast $30))
    (sbi32 $32 (add_wrapping $28 $31))
    (branch-always #1 $14 $32 $17))))