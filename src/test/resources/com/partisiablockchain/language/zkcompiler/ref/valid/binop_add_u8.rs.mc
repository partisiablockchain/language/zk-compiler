(metacircuit
 (function %0
  (output i8)
  (block #0
    (inputs
      (i8 $0)
      (i8 $1))
    (i8 $2 (add_wrapping $0 $1))
    (branch-always #return $2))))