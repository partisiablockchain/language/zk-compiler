(metacircuit
 (function %0
  (output sbi128)
  (block #0
    (sbi32 $108 (constant 322372640))
    (sbi32 $109 (constant 826353432))
    (sbi64 $110 (bit_concat $108 $109))
    (sbi32 $111 (constant 2147462093))
    (sbi32 $112 (constant 19083792))
    (sbi64 $113 (bit_concat $111 $112))
    (sbi128 $114 (bit_concat $113 $110))
    (branch-always #return $114))))