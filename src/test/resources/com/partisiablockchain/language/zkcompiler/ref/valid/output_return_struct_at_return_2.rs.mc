(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi32 $1 (extract $0 32 0))
    (sbi32 $2 (extract $0 32 32))
    (branch-always #1 $1 $2))
  (block #1
    (inputs
      (sbi32 $3)
      (sbi32 $4))
    (sbi64 $10 (bit_concat $4 $3))
    (branch-always #return $10))
  (block #3
    (sbi64 $19 (constant 0))
    (branch-always #return $19))))