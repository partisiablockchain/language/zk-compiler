(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0)
      (i32 $1))
    (i32 $2 (mult_wrapping_signed $0 $1))
    (branch-always #return $2))))