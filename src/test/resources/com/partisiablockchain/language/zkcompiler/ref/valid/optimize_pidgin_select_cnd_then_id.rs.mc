(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi1 $0)
      (sbi1 $1))
    (sbi1 $108 (bitwise_or $0 $1))
    (branch-always #return $108))))