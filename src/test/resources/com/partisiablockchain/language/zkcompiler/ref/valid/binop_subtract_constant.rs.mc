(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $3 (constant -9))
    (sbi32 $4 (add_wrapping $0 $3))
    (branch-always #return $4))))