(metacircuit
 (function %0
  (output sbi192)
  (block #0
    (i8 $0 (constant 0))
    (sbi192 $18 (constant 0))
    (branch-always #1 $0 $18))
  (block #1
    (inputs
      (i8 $20)
      (sbi192 $23))
    (i8 $22 (constant 8))
    (i1 $24 (equal $20 $22))
    (i1 $25 (bitwise_not $24))
    (i8 $26 (constant 1))
    (i8 $27 (add_wrapping $20 $26))
    (branch-if $25
      (0 #3 $23)
      (1 #2 $20 $27 $23)))
  (block #2
    (inputs
      (i8 $28)
      (i8 $29)
      (sbi192 $31))
    (sbi8 $33 (cast $28))
    (i24 $34 (constant 0))
    (i32 $35 (bit_concat $34 $28))
    (i32 $36 (constant 24))
    (i32 $37 (mult_wrapping_signed $35 $36))
    (sbi192 $38 (insertdyn $31 $33 $37))
    (branch-always #1 $29 $38))
  (block #3
    (inputs
      (sbi192 $32))
    (i32 $42 (constant 0))
    (branch-always #4 $42 $32))
  (block #4
    (inputs
      (i32 $43)
      (sbi192 $44))
    (i32 $45 (next_variable_id $43))
    (i32 $46 (constant 0))
    (i1 $47 (equal $45 $46))
    (i1 $48 (bitwise_not $47))
    (branch-if $48
      (0 #return $44)
      (1 #5 $45 $45 $44)))
  (block #5
    (inputs
      (i32 $49)
      (i32 $50)
      (sbi192 $51))
    (sbi8 $53 (load_variable $49))
    (i8 $54 (constant 0))
    (branch-always #7 $54 $53 $50 $51))
  (block #7
    (inputs
      (i8 $55)
      (sbi8 $58)
      (i32 $61)
      (sbi192 $62))
    (i8 $57 (constant 8))
    (i1 $63 (equal $55 $57))
    (i1 $64 (bitwise_not $63))
    (i8 $65 (constant 1))
    (i8 $66 (add_wrapping $55 $65))
    (branch-if $64
      (0 #4 $61 $62)
      (1 #8 $55 $66 $58 $61 $62)))
  (block #8
    (inputs
      (i8 $67)
      (i8 $68)
      (sbi8 $70)
      (i32 $73)
      (sbi192 $74))
    (i24 $80 (constant 0))
    (i32 $81 (bit_concat $80 $67))
    (sbi8 $82 (cast $67))
    (sbi1 $83 (equal $70 $82))
    (sbi15 $84 (constant 0))
    (sbi16 $85 (bit_concat $84 $83))
    (i32 $86 (constant 24))
    (i32 $87 (mult_wrapping_signed $81 $86))
    (i32 $88 (constant 8))
    (i32 $89 (add_wrapping $87 $88))
    (sbi16 $90 (extractdyn $74 16 $89))
    (sbi16 $91 (add_wrapping $85 $90))
    (sbi192 $96 (insertdyn $74 $91 $89))
    (branch-always #7 $68 $70 $73 $96))))