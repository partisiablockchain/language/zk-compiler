(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi1 $0))
    (sbi32 $5 (constant 1337))
    (sbi32 $9 (constant 0))
    (sbi32 $109 (select $0 $9 $5))
    (branch-always #return $109))))