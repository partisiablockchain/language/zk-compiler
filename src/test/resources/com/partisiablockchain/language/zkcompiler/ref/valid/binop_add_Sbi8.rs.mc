(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (i32 $0 (constant 1))
    (sbi8 $1 (load_variable $0))
    (i32 $2 (constant 2))
    (sbi8 $3 (load_variable $2))
    (sbi8 $4 (add_wrapping $1 $3))
    (branch-always #return $4))))