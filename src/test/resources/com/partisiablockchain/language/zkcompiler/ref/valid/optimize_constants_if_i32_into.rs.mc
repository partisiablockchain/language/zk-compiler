(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $5 (constant 40))
    (i1 $6 (less_than_signed $0 $5))
    (branch-if $6
      (0 #2)
      (1 #1)))
  (block #1
    (sbi32 $15 (constant 2))
    (branch-always #return $15))
  (block #2
    (sbi32 $21 (constant 1))
    (branch-always #return $21))))