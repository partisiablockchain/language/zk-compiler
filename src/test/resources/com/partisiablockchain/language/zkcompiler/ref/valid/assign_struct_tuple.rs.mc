(metacircuit
 (function %0
  (output sbi24)
  (block #0
    (inputs
      (sbi24 $0))
    (sbi8 $4 (extract $0 8 16))
    (sbi16 $9 (constant 260))
    (sbi24 $10 (bit_concat $4 $9))
    (branch-always #return $10))))