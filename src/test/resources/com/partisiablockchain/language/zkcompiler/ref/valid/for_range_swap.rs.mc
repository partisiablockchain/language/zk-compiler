(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1)
      (i32 $2))
    (i32 $3 (constant 0))
    (branch-always #1 $3 $2 $0 $1))
  (block #1
    (inputs
      (i32 $4)
      (i32 $5)
      (sbi32 $6)
      (sbi32 $7))
    (i1 $9 (equal $4 $5))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $4 $11))
    (branch-if $10
      (0 #return $6)
      (1 #1 $12 $5 $7 $6)))))