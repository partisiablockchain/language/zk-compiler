(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (sbi8 $1 (constant 0))
    (i32 $2 (constant 0))
    (branch-always #1 $2 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi8 $4))
    (i32 $5 (next_variable_id $3))
    (i32 $6 (constant 0))
    (i1 $7 (equal $5 $6))
    (i1 $8 (bitwise_not $7))
    (branch-if $8
      (0 #return $4)
      (1 #2 $5 $4)))
  (block #2
    (inputs
      (i32 $10)
      (sbi8 $11))
    (sbi8 $18 (constant 1))
    (sbi8 $19 (add_wrapping $11 $18))
    (branch-always #1 $10 $19))))