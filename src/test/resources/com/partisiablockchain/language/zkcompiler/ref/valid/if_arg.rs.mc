(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i1 $0))
    (branch-if $0
      (0 #2)
      (1 #1)))
  (block #1
    (sbi32 $4 (constant 2))
    (branch-always #return $4))
  (block #2
    (sbi32 $8 (constant 4))
    (branch-always #return $8))))