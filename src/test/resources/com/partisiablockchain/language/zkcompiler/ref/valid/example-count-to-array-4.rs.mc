(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (sbi64 $8 (constant 0))
    (i32 $9 (constant 0))
    (branch-always #1 $9 $8))
  (block #1
    (inputs
      (i32 $10)
      (sbi64 $11))
    (i32 $12 (next_variable_id $10))
    (i32 $13 (constant 0))
    (i1 $14 (equal $12 $13))
    (i1 $15 (bitwise_not $14))
    (branch-if $15
      (0 #return $11)
      (1 #2 $12 $12 $11)))
  (block #2
    (inputs
      (i32 $16)
      (i32 $17)
      (sbi64 $18))
    (sbi8 $20 (load_variable $16))
    (i8 $21 (constant 0))
    (branch-always #4 $21 $20 $17 $18))
  (block #4
    (inputs
      (i8 $22)
      (sbi8 $25)
      (i32 $28)
      (sbi64 $29))
    (i8 $24 (constant 8))
    (i1 $30 (equal $22 $24))
    (i1 $31 (bitwise_not $30))
    (i8 $32 (constant 1))
    (i8 $33 (add_wrapping $22 $32))
    (branch-if $31
      (0 #1 $28 $29)
      (1 #5 $22 $33 $25 $28 $29)))
  (block #5
    (inputs
      (i8 $34)
      (i8 $35)
      (sbi8 $37)
      (i32 $40)
      (sbi64 $41))
    (i24 $47 (constant 0))
    (i32 $48 (bit_concat $47 $34))
    (sbi8 $49 (cast $34))
    (sbi1 $50 (equal $37 $49))
    (sbi7 $51 (constant 0))
    (sbi8 $52 (bit_concat $51 $50))
    (i32 $53 (constant 3))
    (i32 $54 (bitshift_left_logical $48 $53))
    (sbi8 $55 (extractdyn $41 8 $54))
    (sbi8 $56 (add_wrapping $52 $55))
    (sbi64 $59 (insertdyn $41 $56 $54))
    (branch-always #4 $35 $37 $40 $59))))