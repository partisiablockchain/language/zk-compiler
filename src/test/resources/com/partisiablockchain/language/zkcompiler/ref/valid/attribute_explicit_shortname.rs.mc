(metacircuit
 (function %33
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (call
      (%34 $0 $1)
      (#100)))
  (block #100
    (inputs
      (sbi32 $2))
    (branch-always #return $2)))
 (function %34
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (equal $0 $1))
    (sbi32 $8 (constant 1))
    (sbi32 $13 (constant 2))
    (sbi32 $113 (select $2 $8 $13))
    (branch-always #return $113))))