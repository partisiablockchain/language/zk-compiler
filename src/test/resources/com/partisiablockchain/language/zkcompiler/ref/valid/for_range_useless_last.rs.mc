(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 0))
    (sbi32 $1 (constant 0))
    (branch-always #1 $0 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $6))
    (i32 $5 (constant 11))
    (i1 $7 (equal $3 $5))
    (i1 $8 (bitwise_not $7))
    (i32 $9 (constant 1))
    (i32 $10 (add_wrapping $3 $9))
    (branch-if $8
      (0 #return $6)
      (1 #2 $3 $10)))
  (block #2
    (inputs
      (i32 $11)
      (i32 $12))
    (i32 $16 (constant 5))
    (i32 $17 (mult_wrapping_signed $11 $16))
    (i32 $19 (add_wrapping $16 $17))
    (sbi32 $20 (cast $19))
    (branch-always #1 $12 $20))))