(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (add_wrapping $0 $0))
    (sbi32 $2 (add_wrapping $0 $1))
    (sbi32 $3 (add_wrapping $0 $2))
    (sbi32 $4 (add_wrapping $0 $3))
    (sbi32 $5 (add_wrapping $0 $4))
    (sbi32 $6 (add_wrapping $0 $5))
    (sbi32 $7 (add_wrapping $0 $6))
    (branch-always #return $7))))