(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 0))
    (sbi32 $1 (constant 0))
    (branch-always #1 $0 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $6))
    (i32 $5 (constant 11))
    (i1 $11 (equal $3 $5))
    (i1 $12 (bitwise_not $11))
    (i32 $13 (constant 1))
    (i32 $14 (add_wrapping $3 $13))
    (branch-if $12
      (0 #return $6)
      (1 #2 $3 $14 $6)))
  (block #2
    (inputs
      (i32 $15)
      (i32 $16)
      (sbi32 $18))
    (sbi32 $24 (cast $15))
    (sbi32 $25 (add_wrapping $18 $24))
    (branch-always #1 $16 $25))))