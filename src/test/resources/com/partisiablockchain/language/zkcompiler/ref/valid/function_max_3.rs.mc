(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 39))
    (sbi32 $4 (constant 321))
    (call
      (%1 $2 $4)
      (#100 $0)))
  (block #100
    (inputs
      (sbi32 $211)
      (sbi32 $5))
    (sbi32 $7 (constant 93))
    (call
      (%1 $7 $5)
      (#101 $211)))
  (block #101
    (inputs
      (sbi32 $216)
      (sbi32 $8))
    (sbi1 $9 (less_than_signed $8 $216))
    (branch-always #return $9)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $110 (select $2 $1 $0))
    (branch-always #return $110)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $111 (select $2 $0 $1))
    (branch-always #return $111))))