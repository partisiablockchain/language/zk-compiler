(metacircuit
 (function %0
  (output i64)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 9))
    (i64 $2 (bit_concat $1 $0))
    (branch-always #return $2))))