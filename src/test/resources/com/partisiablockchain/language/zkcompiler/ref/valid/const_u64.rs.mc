(metacircuit
 (function %0
  (output i64)
  (block #0
    (inputs
      (i64 $0))
    (i64 $1 (constant 23))
    (i64 $2 (add_wrapping $0 $1))
    (branch-always #return $2))))