(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (sbi32 $0))
    (branch-always #1))
  (block #1
    (i32 $4 (constant 1))
    (branch-always #3 $4))
  (block #2
    (i32 $7 (constant 3))
    (branch-always #3 $7))
  (block #3
    (inputs
      (i32 $5))
    (branch-always #5 $5 $5))
  (block #4
    (inputs
      (i32 $10)
      (i32 $11))
    (i32 $17 (constant 10))
    (i32 $18 (mult_wrapping_signed $11 $17))
    (branch-always #6 $18 $10))
  (block #5
    (inputs
      (i32 $13)
      (i32 $14))
    (i32 $23 (constant 30))
    (i32 $24 (mult_wrapping_signed $14 $23))
    (branch-always #6 $24 $13))
  (block #6
    (inputs
      (i32 $19)
      (i32 $20))
    (i32 $29 (add_wrapping $19 $20))
    (branch-always #return $29))))