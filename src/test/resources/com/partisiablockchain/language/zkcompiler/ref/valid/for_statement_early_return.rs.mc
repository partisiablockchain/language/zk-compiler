(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (branch-always #2))
  (block #2
    (sbi32 $15 (constant 9))
    (branch-always #return $15))
  (block #3
    (sbi32 $25 (constant 8))
    (branch-always #return $25))))