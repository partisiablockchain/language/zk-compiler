(metacircuit
 (function %0
  (output i128)
  (block #0
    (i32 $100 (constant -1))
    (i64 $102 (bit_concat $100 $100))
    (i128 $106 (bit_concat $102 $102))
    (branch-always #return $106))))