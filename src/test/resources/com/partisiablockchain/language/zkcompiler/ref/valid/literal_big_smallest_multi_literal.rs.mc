(metacircuit
 (function %0
  (output i64)
  (block #0
    (i32 $100 (constant 1))
    (i32 $101 (constant 0))
    (i64 $102 (bit_concat $100 $101))
    (branch-always #return $102))))