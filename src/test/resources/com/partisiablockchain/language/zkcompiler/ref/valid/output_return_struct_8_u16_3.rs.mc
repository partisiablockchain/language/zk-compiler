(metacircuit
 (function %0
  (output i128)
  (block #0
    (i16 $0 (constant 65535))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (inputs
      (i128 $1))
    (i16 $2 (extract $1 16 0))
    (i16 $3 (extract $1 16 16))
    (i16 $4 (extract $1 16 32))
    (i16 $5 (extract $1 16 48))
    (i16 $6 (extract $1 16 64))
    (i16 $7 (extract $1 16 80))
    (i16 $8 (extract $1 16 96))
    (i16 $9 (extract $1 16 112))
    (i32 $10 (bit_concat $3 $2))
    (i32 $11 (bit_concat $5 $4))
    (i32 $12 (bit_concat $7 $6))
    (i32 $13 (bit_concat $9 $8))
    (i64 $14 (bit_concat $11 $10))
    (i64 $15 (bit_concat $13 $12))
    (i128 $16 (bit_concat $15 $14))
    (branch-always #return $16)))
 (function %1
  (output i128)
  (block #0
    (inputs
      (i16 $0))
    (i32 $1 (bit_concat $0 $0))
    (i64 $5 (bit_concat $1 $1))
    (i128 $7 (bit_concat $5 $5))
    (branch-always #return $7))))