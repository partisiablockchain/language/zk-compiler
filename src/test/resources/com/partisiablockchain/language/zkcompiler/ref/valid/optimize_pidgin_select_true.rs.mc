(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $110 (extract $0 32 0))
    (branch-always #return $110))))