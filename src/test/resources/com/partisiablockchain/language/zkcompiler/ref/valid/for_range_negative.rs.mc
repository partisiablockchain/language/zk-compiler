(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (negate $0))
    (i32 $3 (constant 1))
    (i32 $4 (add_wrapping $1 $3))
    (i32 $7 (constant 0))
    (branch-always #1 $1 $4 $7 $0))
  (block #1
    (inputs
      (i32 $5)
      (i32 $6)
      (i32 $8)
      (i32 $9))
    (i1 $10 (equal $5 $6))
    (i1 $11 (bitwise_not $10))
    (i32 $12 (constant 1))
    (i32 $13 (add_wrapping $5 $12))
    (branch-if $11
      (0 #3 $8 $9)
      (1 #2 $5 $13 $6 $8 $9)))
  (block #2
    (inputs
      (i32 $14)
      (i32 $15)
      (i32 $16)
      (i32 $17)
      (i32 $18))
    (i32 $21 (add_wrapping $14 $17))
    (branch-always #1 $15 $16 $21 $18))
  (block #3
    (inputs
      (i32 $19)
      (i32 $20))
    (i32 $26 (negate $19))
    (i1 $27 (equal $20 $26))
    (branch-always #return $27))))