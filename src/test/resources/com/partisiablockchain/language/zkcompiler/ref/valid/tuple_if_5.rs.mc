(metacircuit
 (function %0
  (output i32 i32)
  (block #0
    (inputs
      (i1 $0))
    (branch-if $0
      (0 #2 $0)
      (1 #1 $0)))
  (block #1
    (inputs
      (i1 $1))
    (i32 $3 (constant 1))
    (branch-always #3 $3 $1))
  (block #2
    (inputs
      (i1 $2))
    (i32 $6 (constant 2))
    (branch-always #3 $6 $2))
  (block #3
    (inputs
      (i32 $4)
      (i1 $5))
    (i1 $9 (bitwise_not $5))
    (branch-if $9
      (0 #5 $4)
      (1 #4 $4)))
  (block #4
    (inputs
      (i32 $10))
    (i32 $14 (constant 3))
    (branch-always #return $10 $14))
  (block #5
    (inputs
      (i32 $12))
    (i32 $18 (constant 4))
    (branch-always #return $12 $18))))