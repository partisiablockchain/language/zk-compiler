(metacircuit
 (function %0
  (output i128)
  (block #0
    (i32 $0 (constant 1))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (inputs
      (i128 $1))
    (i32 $2 (extract $1 32 0))
    (i32 $3 (extract $1 32 32))
    (i32 $4 (extract $1 32 64))
    (i32 $5 (extract $1 32 96))
    (i64 $6 (bit_concat $3 $2))
    (i64 $7 (bit_concat $5 $4))
    (i128 $8 (bit_concat $7 $6))
    (branch-always #return $8)))
 (function %1
  (output i128)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (negate $0))
    (i64 $5 (bit_concat $1 $1))
    (i128 $7 (bit_concat $5 $5))
    (branch-always #return $7))))