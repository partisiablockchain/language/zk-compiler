(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 3))
    (sbi1 $3 (equal $0 $2))
    (sbi32 $109 (select $3 $0 $2))
    (branch-always #return $109))))