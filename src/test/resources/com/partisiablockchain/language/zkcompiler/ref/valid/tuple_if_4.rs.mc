(metacircuit
 (function %0
  (output sbi32 sbi32 sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1)
      (sbi32 $2))
    (sbi1 $3 (less_than_or_equal_signed $0 $1))
    (sbi32 $168 (select $3 $0 $1))
    (sbi32 $169 (select $3 $1 $0))
    (sbi1 $20 (less_than_or_equal_signed $168 $2))
    (sbi32 $170 (select $20 $168 $2))
    (sbi32 $171 (select $20 $2 $168))
    (sbi1 $45 (less_than_or_equal_signed $169 $171))
    (sbi32 $172 (select $45 $169 $171))
    (sbi32 $173 (select $45 $171 $169))
    (branch-always #return $170 $172 $173))))