(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi32 $1 (constant -1))
    (i32 $2 (constant 0))
    (branch-always #1 $2 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $4))
    (i32 $5 (next_variable_id $3))
    (i32 $6 (constant 0))
    (i1 $7 (equal $5 $6))
    (i1 $8 (bitwise_not $7))
    (branch-if $8
      (0 #return $4)
      (1 #2 $5 $5 $4)))
  (block #2
    (inputs
      (i32 $9)
      (i32 $10)
      (sbi32 $11))
    (sbi32 $13 (load_variable $9))
    (call
      (%1 $11 $13)
      (#102 $10)))
  (block #102
    (inputs
      (i32 $216)
      (sbi32 $14))
    (branch-always #1 $216 $14)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $114 (select $2 $1 $0))
    (branch-always #return $114)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $115 (select $2 $0 $1))
    (branch-always #return $115))))