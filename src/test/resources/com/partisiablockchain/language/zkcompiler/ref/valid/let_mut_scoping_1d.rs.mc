(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 1))
    (sbi32 $4 (constant 99))
    (sbi1 $5 (less_than_signed $0 $4))
    (sbi32 $11 (constant 91))
    (sbi1 $12 (less_than_signed $0 $11))
    (sbi32 $18 (constant 31))
    (sbi1 $19 (less_than_signed $18 $0))
    (sbi32 $25 (constant 3))
    (sbi32 $29 (constant 2))
    (sbi32 $139 (select $19 $25 $29))
    (sbi32 $35 (constant 4))
    (sbi32 $140 (select $12 $139 $35))
    (sbi32 $141 (select $5 $140 $2))
    (branch-always #return $141))))