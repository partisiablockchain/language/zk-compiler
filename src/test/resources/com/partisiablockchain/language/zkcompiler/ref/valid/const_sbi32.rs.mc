(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 13211))
    (sbi32 $3 (add_wrapping $0 $2))
    (branch-always #return $3))))