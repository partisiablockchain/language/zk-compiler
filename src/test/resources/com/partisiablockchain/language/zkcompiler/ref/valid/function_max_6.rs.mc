(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 99))
    (call
      (%2 $0 $2)
      (#100 $0)))
  (block #100
    (inputs
      (sbi32 $211)
      (sbi32 $3))
    (sbi32 $5 (constant 21))
    (call
      (%1 $211 $5)
      (#101 $3)))
  (block #101
    (inputs
      (sbi32 $217)
      (sbi32 $6))
    (branch-always #return $217 $6)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $110 (select $2 $1 $0))
    (branch-always #return $110)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $111 (select $2 $0 $1))
    (branch-always #return $111))))