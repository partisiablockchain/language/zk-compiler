(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi8 $2 (constant 0))
    (i32 $3 (constant 0))
    (branch-always #1 $3 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (sbi8 $7)
      (sbi64 $8))
    (i32 $6 (constant 8))
    (i1 $9 (equal $4 $6))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $4 $11))
    (branch-if $10
      (0 #return $7)
      (1 #2 $4 $12 $7 $8)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi8 $16)
      (sbi64 $17))
    (i32 $20 (constant 3))
    (i32 $21 (bitshift_left_logical $13 $20))
    (sbi8 $22 (extractdyn $17 8 $21))
    (sbi8 $23 (add_wrapping $16 $22))
    (branch-always #1 $14 $23 $17))))