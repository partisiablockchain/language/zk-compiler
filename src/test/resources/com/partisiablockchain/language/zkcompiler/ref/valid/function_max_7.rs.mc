(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1)
      (sbi32 $2)
      (sbi32 $3)
      (sbi32 $4))
    (call
      (%1 $0 $1)
      (#100 $2 $3 $4)))
  (block #100
    (inputs
      (sbi32 $213)
      (sbi32 $214)
      (sbi32 $215)
      (sbi32 $5))
    (call
      (%1 $5 $213)
      (#101 $214 $215)))
  (block #101
    (inputs
      (sbi32 $219)
      (sbi32 $220)
      (sbi32 $6))
    (call
      (%1 $6 $219)
      (#102 $220)))
  (block #102
    (inputs
      (sbi32 $226)
      (sbi32 $7))
    (call
      (%1 $7 $226)
      (#103)))
  (block #103
    (inputs
      (sbi32 $8))
    (branch-always #return $8)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $110 (select $2 $1 $0))
    (branch-always #return $110)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $111 (select $2 $0 $1))
    (branch-always #return $111))))