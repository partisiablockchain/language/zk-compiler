(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $9 (constant 1))
    (sbi32 $10 (add_wrapping $0 $9))
    (sbi32 $110 (extract $10 32 0))
    (branch-always #return $110))))