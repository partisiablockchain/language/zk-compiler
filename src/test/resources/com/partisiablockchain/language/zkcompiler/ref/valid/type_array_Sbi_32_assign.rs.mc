(metacircuit
 (function %0
  (output sbi128)
  (block #0
    (inputs
      (sbi128 $0))
    (sbi32 $2 (constant 34))
    (i32 $3 (constant 32))
    (sbi128 $4 (insertdyn $0 $2 $3))
    (branch-always #return $4))))