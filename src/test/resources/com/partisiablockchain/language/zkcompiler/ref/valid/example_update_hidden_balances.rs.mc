(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (i32 $2 (constant 0))
    (branch-always #1 $2 $0 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $4)
      (sbi32 $5))
    (i32 $6 (next_variable_id $3))
    (i32 $7 (constant 0))
    (i1 $8 (equal $6 $7))
    (i1 $9 (bitwise_not $8))
    (branch-if $9
      (0 #return)
      (1 #2 $6 $6 $4 $5)))
  (block #2
    (inputs
      (i32 $10)
      (i32 $11)
      (sbi32 $12)
      (sbi32 $13))
    (sbi64 $16 (load_variable $10))
    (sbi32 $17 (extract $16 32 0))
    (sbi32 $18 (extract $16 32 32))
    (sbi1 $19 (equal $12 $17))
    (sbi32 $254 (constant 0))
    (sbi32 $255 (select $19 $13 $254))
    (sbi32 $154 (add_wrapping $18 $255))
    (sbi64 $53 (bit_concat $154 $17))
    (i0 $54 (output $53))
    (branch-always #1 $11 $12 $13))))