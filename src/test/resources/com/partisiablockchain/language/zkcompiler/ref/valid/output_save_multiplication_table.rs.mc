(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (i32 $2 (constant 0))
    (branch-always #1 $2 $1 $0 $0))
  (block #1
    (inputs
      (i32 $3)
      (i32 $4)
      (sbi32 $5)
      (sbi32 $6))
    (i1 $8 (equal $3 $4))
    (i1 $9 (bitwise_not $8))
    (i32 $10 (constant 1))
    (i32 $11 (add_wrapping $3 $10))
    (branch-if $9
      (0 #return)
      (1 #2 $11 $4 $5 $6)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi32 $15)
      (sbi32 $16))
    (i0 $21 (output $15))
    (sbi32 $22 (add_wrapping $15 $16))
    (branch-always #1 $13 $14 $22 $16))))