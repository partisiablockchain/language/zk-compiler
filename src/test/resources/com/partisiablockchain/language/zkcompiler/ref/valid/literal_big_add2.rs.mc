(metacircuit
 (function %0
  (output i128)
  (block #0
    (i32 $103 (constant 0))
    (i32 $116 (constant -16004768))
    (i64 $118 (bit_concat $116 $103))
    (i32 $119 (constant -126217745))
    (i32 $120 (constant 707115302))
    (i64 $121 (bit_concat $119 $120))
    (i128 $122 (bit_concat $121 $118))
    (branch-always #return $122))))