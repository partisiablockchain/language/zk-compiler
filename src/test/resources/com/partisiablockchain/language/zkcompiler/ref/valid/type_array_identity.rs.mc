(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi64 $1 (bit_concat $0 $0))
    (sbi128 $3 (bit_concat $1 $1))
    (sbi32 $4 (extract $3 32 32))
    (branch-always #return $4))))