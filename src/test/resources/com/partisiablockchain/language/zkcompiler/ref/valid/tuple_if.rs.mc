(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $110 (select $2 $0 $1))
    (sbi32 $111 (select $2 $1 $0))
    (branch-always #return $110 $111))))