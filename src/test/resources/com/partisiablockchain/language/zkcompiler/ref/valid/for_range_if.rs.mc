(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i1 $0))
    (branch-if $0
      (0 #5 $0)
      (1 #4 $0)))
  (block #4
    (inputs
      (i1 $4))
    (i32 $7 (constant 1))
    (branch-always #6 $7 $4))
  (block #5
    (inputs
      (i1 $6))
    (i32 $11 (constant 6))
    (branch-always #6 $11 $6))
  (block #6
    (inputs
      (i32 $8)
      (i1 $10))
    (branch-if $10
      (0 #8 $8)
      (1 #7 $8)))
  (block #7
    (inputs
      (i32 $15))
    (sbi32 $16 (constant 0))
    (i32 $21 (constant 9))
    (branch-always #1 $15 $21 $16))
  (block #8
    (inputs
      (i32 $18))
    (sbi32 $19 (constant 0))
    (i32 $26 (constant 10))
    (branch-always #1 $18 $26 $19))
  (block #1
    (inputs
      (i32 $31)
      (i32 $32)
      (sbi32 $33))
    (i1 $35 (equal $31 $32))
    (i1 $36 (bitwise_not $35))
    (i32 $37 (constant 1))
    (i32 $38 (add_wrapping $31 $37))
    (branch-if $36
      (0 #return $33)
      (1 #2 $31 $38 $32 $33)))
  (block #2
    (inputs
      (i32 $39)
      (i32 $40)
      (i32 $41)
      (sbi32 $42))
    (sbi32 $46 (cast $39))
    (sbi32 $47 (add_wrapping $42 $46))
    (branch-always #1 $40 $41 $47))))