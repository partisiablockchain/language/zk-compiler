(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (i1024 $7 (constant 0))
    (i1024 $8 (insertdyn $7 $0 $1))
    (i32 $13 (extractdyn $8 32 $1))
    (branch-always #return $13))))