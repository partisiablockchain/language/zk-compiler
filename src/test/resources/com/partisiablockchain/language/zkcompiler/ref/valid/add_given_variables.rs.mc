(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i32 $0)
      (i32 $1))
    (sbi32 $2 (load_variable $0))
    (sbi32 $3 (load_variable $1))
    (sbi32 $4 (add_wrapping $2 $3))
    (branch-always #return $4))))