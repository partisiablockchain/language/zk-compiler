(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (branch-always #return)))
 (function %1
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%2 $0)
      (#100)))
  (block #100
    (branch-always #return)))
 (function %2
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (i0 $1 (output $0))
    (branch-always #return))))