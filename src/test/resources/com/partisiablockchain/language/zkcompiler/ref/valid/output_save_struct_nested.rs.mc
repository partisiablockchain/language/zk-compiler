(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi8 $0))
    (sbi16 $1 (bit_concat $0 $0))
    (sbi24 $2 (bit_concat $1 $0))
    (i0 $3 (output $2))
    (branch-always #return))))