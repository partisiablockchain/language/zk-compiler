(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 1))
    (sbi32 $1 (load_variable $0))
    (sbi32 $4 (add_wrapping $1 $1))
    (branch-always #return $4))))