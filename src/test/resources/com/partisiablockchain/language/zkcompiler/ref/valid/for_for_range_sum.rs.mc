(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 0))
    (sbi32 $1 (constant 0))
    (branch-always #1 $0 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $6))
    (i32 $5 (constant 11))
    (i1 $7 (equal $3 $5))
    (i1 $8 (bitwise_not $7))
    (i32 $9 (constant 1))
    (i32 $10 (add_wrapping $3 $9))
    (branch-if $8
      (0 #return $6)
      (1 #2 $3 $10 $6)))
  (block #2
    (inputs
      (i32 $11)
      (i32 $12)
      (sbi32 $14))
    (i32 $16 (constant 0))
    (branch-always #4 $16 $11 $12 $14))
  (block #4
    (inputs
      (i32 $17)
      (i32 $18)
      (i32 $21)
      (sbi32 $23))
    (i1 $24 (equal $17 $18))
    (i1 $25 (bitwise_not $24))
    (i32 $26 (constant 1))
    (i32 $27 (add_wrapping $17 $26))
    (branch-if $25
      (0 #1 $21 $23)
      (1 #5 $17 $27 $18 $21 $23)))
  (block #5
    (inputs
      (i32 $28)
      (i32 $29)
      (i32 $30)
      (i32 $33)
      (sbi32 $35))
    (sbi32 $41 (cast $28))
    (sbi32 $42 (add_wrapping $35 $41))
    (branch-always #4 $29 $30 $33 $42))))