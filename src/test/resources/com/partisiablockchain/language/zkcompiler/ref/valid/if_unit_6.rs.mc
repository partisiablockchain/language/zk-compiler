(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 1))
    (i1 $2 (equal $0 $1))
    (branch-if $2
      (0 #2 $0)
      (1 #1 $0)))
  (block #1
    (inputs
      (i32 $3))
    (i32 $5 (constant 2))
    (i1 $6 (equal $3 $5))
    (branch-if $6
      (0 #5 $3)
      (1 #4 $3)))
  (block #4
    (inputs
      (i32 $7))
    (i32 $9 (constant 3))
    (i1 $10 (equal $7 $9))
    (branch-if $10
      (0 #8 $7)
      (1 #7 $7)))
  (block #7
    (inputs
      (i32 $11))
    (i32 $13 (constant 4))
    (i1 $14 (equal $11 $13))
    (branch-if $14
      (0 #11 $11)
      (1 #10 $11)))
  (block #10
    (inputs
      (i32 $15))
    (i32 $17 (constant 5))
    (i1 $18 (equal $15 $17))
    (branch-if $18
      (0 #14 $15)
      (1 #13 $15)))
  (block #13
    (inputs
      (i32 $19))
    (i32 $21 (constant 6))
    (i1 $22 (equal $19 $21))
    (branch-if $22
      (0 #17 $19)
      (1 #16 $19)))
  (block #16
    (inputs
      (i32 $23))
    (i32 $25 (constant 7))
    (i1 $26 (equal $23 $25))
    (branch-if $26
      (0 #20 $23)
      (1 #19 $23)))
  (block #19
    (inputs
      (i32 $27))
    (branch-always #return $27))
  (block #20
    (inputs
      (i32 $28))
    (branch-always #return $28))
  (block #17
    (inputs
      (i32 $24))
    (i32 $51 (constant 10))
    (i1 $52 (equal $24 $51))
    (branch-if $52
      (0 #29 $24)
      (1 #28 $24)))
  (block #28
    (inputs
      (i32 $53))
    (branch-always #return $53))
  (block #29
    (inputs
      (i32 $54))
    (branch-always #return $54))
  (block #14
    (inputs
      (i32 $20))
    (i32 $79 (constant 13))
    (i1 $80 (equal $20 $79))
    (branch-if $80
      (0 #38 $20)
      (1 #37 $20)))
  (block #37
    (inputs
      (i32 $81))
    (i32 $83 (constant 14))
    (i1 $84 (equal $81 $83))
    (branch-if $84
      (0 #41 $81)
      (1 #40 $81)))
  (block #40
    (inputs
      (i32 $85))
    (branch-always #return $85))
  (block #41
    (inputs
      (i32 $86))
    (branch-always #return $86))
  (block #38
    (inputs
      (i32 $82))
    (i32 $109 (constant 17))
    (i1 $110 (equal $82 $109))
    (branch-if $110
      (0 #50 $82)
      (1 #49 $82)))
  (block #49
    (inputs
      (i32 $111))
    (branch-always #return $111))
  (block #50
    (inputs
      (i32 $112))
    (branch-always #return $112))
  (block #11
    (inputs
      (i32 $16))
    (i32 $139 (constant 20))
    (i1 $140 (equal $16 $139))
    (branch-if $140
      (0 #59 $16)
      (1 #58 $16)))
  (block #58
    (inputs
      (i32 $141))
    (i32 $143 (constant 21))
    (i1 $144 (equal $141 $143))
    (branch-if $144
      (0 #62 $141)
      (1 #61 $141)))
  (block #61
    (inputs
      (i32 $145))
    (i32 $147 (constant 22))
    (i1 $148 (equal $145 $147))
    (branch-if $148
      (0 #65 $145)
      (1 #64 $145)))
  (block #64
    (inputs
      (i32 $149))
    (branch-always #return $149))
  (block #65
    (inputs
      (i32 $150))
    (branch-always #return $150))
  (block #62
    (inputs
      (i32 $146))
    (i32 $173 (constant 25))
    (i1 $174 (equal $146 $173))
    (branch-if $174
      (0 #74 $146)
      (1 #73 $146)))
  (block #73
    (inputs
      (i32 $175))
    (branch-always #return $175))
  (block #74
    (inputs
      (i32 $176))
    (branch-always #return $176))
  (block #59
    (inputs
      (i32 $142))
    (i32 $201 (constant 28))
    (i1 $202 (equal $142 $201))
    (branch-if $202
      (0 #83 $142)
      (1 #82 $142)))
  (block #82
    (inputs
      (i32 $203))
    (i32 $205 (constant 29))
    (i1 $206 (equal $203 $205))
    (branch-if $206
      (0 #86 $203)
      (1 #85 $203)))
  (block #85
    (inputs
      (i32 $207))
    (branch-always #return $207))
  (block #86
    (inputs
      (i32 $208))
    (branch-always #return $208))
  (block #83
    (inputs
      (i32 $204))
    (i32 $231 (constant 32))
    (i1 $232 (equal $204 $231))
    (branch-if $232
      (0 #95 $204)
      (1 #94 $204)))
  (block #94
    (inputs
      (i32 $233))
    (branch-always #return $233))
  (block #95
    (inputs
      (i32 $234))
    (branch-always #return $234))
  (block #8
    (inputs
      (i32 $12))
    (i32 $263 (constant 35))
    (i1 $264 (equal $12 $263))
    (branch-if $264
      (0 #104 $12)
      (1 #103 $12)))
  (block #103
    (inputs
      (i32 $265))
    (i32 $267 (constant 36))
    (i1 $268 (equal $265 $267))
    (branch-if $268
      (0 #107 $265)
      (1 #106 $265)))
  (block #106
    (inputs
      (i32 $269))
    (i32 $271 (constant 37))
    (i1 $272 (equal $269 $271))
    (branch-if $272
      (0 #110 $269)
      (1 #109 $269)))
  (block #109
    (inputs
      (i32 $273))
    (i32 $275 (constant 38))
    (i1 $276 (equal $273 $275))
    (branch-if $276
      (0 #113 $273)
      (1 #112 $273)))
  (block #112
    (inputs
      (i32 $277))
    (branch-always #return $277))
  (block #113
    (inputs
      (i32 $278))
    (branch-always #return $278))
  (block #110
    (inputs
      (i32 $274))
    (i32 $301 (constant 41))
    (i1 $302 (equal $274 $301))
    (branch-if $302
      (0 #122 $274)
      (1 #121 $274)))
  (block #121
    (inputs
      (i32 $303))
    (branch-always #return $303))
  (block #122
    (inputs
      (i32 $304))
    (branch-always #return $304))
  (block #107
    (inputs
      (i32 $270))
    (i32 $329 (constant 44))
    (i1 $330 (equal $270 $329))
    (branch-if $330
      (0 #131 $270)
      (1 #130 $270)))
  (block #130
    (inputs
      (i32 $331))
    (i32 $333 (constant 45))
    (i1 $334 (equal $331 $333))
    (branch-if $334
      (0 #134 $331)
      (1 #133 $331)))
  (block #133
    (inputs
      (i32 $335))
    (branch-always #return $335))
  (block #134
    (inputs
      (i32 $336))
    (branch-always #return $336))
  (block #131
    (inputs
      (i32 $332))
    (i32 $359 (constant 48))
    (i1 $360 (equal $332 $359))
    (branch-if $360
      (0 #143 $332)
      (1 #142 $332)))
  (block #142
    (inputs
      (i32 $361))
    (branch-always #return $361))
  (block #143
    (inputs
      (i32 $362))
    (branch-always #return $362))
  (block #104
    (inputs
      (i32 $266))
    (i32 $389 (constant 51))
    (i1 $390 (equal $266 $389))
    (branch-if $390
      (0 #152 $266)
      (1 #151 $266)))
  (block #151
    (inputs
      (i32 $391))
    (i32 $393 (constant 52))
    (i1 $394 (equal $391 $393))
    (branch-if $394
      (0 #155 $391)
      (1 #154 $391)))
  (block #154
    (inputs
      (i32 $395))
    (i32 $397 (constant 53))
    (i1 $398 (equal $395 $397))
    (branch-if $398
      (0 #158 $395)
      (1 #157 $395)))
  (block #157
    (inputs
      (i32 $399))
    (branch-always #return $399))
  (block #158
    (inputs
      (i32 $400))
    (branch-always #return $400))
  (block #155
    (inputs
      (i32 $396))
    (i32 $423 (constant 56))
    (i1 $424 (equal $396 $423))
    (branch-if $424
      (0 #167 $396)
      (1 #166 $396)))
  (block #166
    (inputs
      (i32 $425))
    (branch-always #return $425))
  (block #167
    (inputs
      (i32 $426))
    (branch-always #return $426))
  (block #152
    (inputs
      (i32 $392))
    (i32 $451 (constant 59))
    (i1 $452 (equal $392 $451))
    (branch-if $452
      (0 #176 $392)
      (1 #175 $392)))
  (block #175
    (inputs
      (i32 $453))
    (i32 $455 (constant 60))
    (i1 $456 (equal $453 $455))
    (branch-if $456
      (0 #179 $453)
      (1 #178 $453)))
  (block #178
    (inputs
      (i32 $457))
    (branch-always #return $457))
  (block #179
    (inputs
      (i32 $458))
    (branch-always #return $458))
  (block #176
    (inputs
      (i32 $454))
    (i32 $481 (constant 63))
    (i1 $482 (equal $454 $481))
    (branch-if $482
      (0 #188 $454)
      (1 #187 $454)))
  (block #187
    (inputs
      (i32 $483))
    (branch-always #return $483))
  (block #188
    (inputs
      (i32 $484))
    (branch-always #return $484))
  (block #5
    (inputs
      (i32 $8))
    (i32 $515 (constant 66))
    (i1 $516 (equal $8 $515))
    (branch-if $516
      (0 #197 $8)
      (1 #196 $8)))
  (block #196
    (inputs
      (i32 $517))
    (i32 $519 (constant 67))
    (i1 $520 (equal $517 $519))
    (branch-if $520
      (0 #200 $517)
      (1 #199 $517)))
  (block #199
    (inputs
      (i32 $521))
    (i32 $523 (constant 68))
    (i1 $524 (equal $521 $523))
    (branch-if $524
      (0 #203 $521)
      (1 #202 $521)))
  (block #202
    (inputs
      (i32 $525))
    (i32 $527 (constant 69))
    (i1 $528 (equal $525 $527))
    (branch-if $528
      (0 #206 $525)
      (1 #205 $525)))
  (block #205
    (inputs
      (i32 $529))
    (i32 $531 (constant 70))
    (i1 $532 (equal $529 $531))
    (branch-if $532
      (0 #209 $529)
      (1 #208 $529)))
  (block #208
    (inputs
      (i32 $533))
    (branch-always #return $533))
  (block #209
    (inputs
      (i32 $534))
    (branch-always #return $534))
  (block #206
    (inputs
      (i32 $530))
    (i32 $557 (constant 73))
    (i1 $558 (equal $530 $557))
    (branch-if $558
      (0 #218 $530)
      (1 #217 $530)))
  (block #217
    (inputs
      (i32 $559))
    (branch-always #return $559))
  (block #218
    (inputs
      (i32 $560))
    (branch-always #return $560))
  (block #203
    (inputs
      (i32 $526))
    (i32 $585 (constant 76))
    (i1 $586 (equal $526 $585))
    (branch-if $586
      (0 #227 $526)
      (1 #226 $526)))
  (block #226
    (inputs
      (i32 $587))
    (i32 $589 (constant 77))
    (i1 $590 (equal $587 $589))
    (branch-if $590
      (0 #230 $587)
      (1 #229 $587)))
  (block #229
    (inputs
      (i32 $591))
    (branch-always #return $591))
  (block #230
    (inputs
      (i32 $592))
    (branch-always #return $592))
  (block #227
    (inputs
      (i32 $588))
    (i32 $615 (constant 80))
    (i1 $616 (equal $588 $615))
    (branch-if $616
      (0 #239 $588)
      (1 #238 $588)))
  (block #238
    (inputs
      (i32 $617))
    (branch-always #return $617))
  (block #239
    (inputs
      (i32 $618))
    (branch-always #return $618))
  (block #200
    (inputs
      (i32 $522))
    (i32 $645 (constant 83))
    (i1 $646 (equal $522 $645))
    (branch-if $646
      (0 #248 $522)
      (1 #247 $522)))
  (block #247
    (inputs
      (i32 $647))
    (i32 $649 (constant 84))
    (i1 $650 (equal $647 $649))
    (branch-if $650
      (0 #251 $647)
      (1 #250 $647)))
  (block #250
    (inputs
      (i32 $651))
    (i32 $653 (constant 85))
    (i1 $654 (equal $651 $653))
    (branch-if $654
      (0 #254 $651)
      (1 #253 $651)))
  (block #253
    (inputs
      (i32 $655))
    (branch-always #return $655))
  (block #254
    (inputs
      (i32 $656))
    (branch-always #return $656))
  (block #251
    (inputs
      (i32 $652))
    (i32 $679 (constant 88))
    (i1 $680 (equal $652 $679))
    (branch-if $680
      (0 #263 $652)
      (1 #262 $652)))
  (block #262
    (inputs
      (i32 $681))
    (branch-always #return $681))
  (block #263
    (inputs
      (i32 $682))
    (branch-always #return $682))
  (block #248
    (inputs
      (i32 $648))
    (i32 $707 (constant 91))
    (i1 $708 (equal $648 $707))
    (branch-if $708
      (0 #272 $648)
      (1 #271 $648)))
  (block #271
    (inputs
      (i32 $709))
    (i32 $711 (constant 92))
    (i1 $712 (equal $709 $711))
    (branch-if $712
      (0 #275 $709)
      (1 #274 $709)))
  (block #274
    (inputs
      (i32 $713))
    (branch-always #return $713))
  (block #275
    (inputs
      (i32 $714))
    (branch-always #return $714))
  (block #272
    (inputs
      (i32 $710))
    (i32 $737 (constant 95))
    (i1 $738 (equal $710 $737))
    (branch-if $738
      (0 #284 $710)
      (1 #283 $710)))
  (block #283
    (inputs
      (i32 $739))
    (branch-always #return $739))
  (block #284
    (inputs
      (i32 $740))
    (branch-always #return $740))
  (block #197
    (inputs
      (i32 $518))
    (i32 $769 (constant 98))
    (i1 $770 (equal $518 $769))
    (branch-if $770
      (0 #293 $518)
      (1 #292 $518)))
  (block #292
    (inputs
      (i32 $771))
    (i32 $773 (constant 99))
    (i1 $774 (equal $771 $773))
    (branch-if $774
      (0 #296 $771)
      (1 #295 $771)))
  (block #295
    (inputs
      (i32 $775))
    (i32 $777 (constant 100))
    (i1 $778 (equal $775 $777))
    (branch-if $778
      (0 #299 $775)
      (1 #298 $775)))
  (block #298
    (inputs
      (i32 $779))
    (i32 $781 (constant 101))
    (i1 $782 (equal $779 $781))
    (branch-if $782
      (0 #302 $779)
      (1 #301 $779)))
  (block #301
    (inputs
      (i32 $783))
    (branch-always #return $783))
  (block #302
    (inputs
      (i32 $784))
    (branch-always #return $784))
  (block #299
    (inputs
      (i32 $780))
    (i32 $807 (constant 104))
    (i1 $808 (equal $780 $807))
    (branch-if $808
      (0 #311 $780)
      (1 #310 $780)))
  (block #310
    (inputs
      (i32 $809))
    (branch-always #return $809))
  (block #311
    (inputs
      (i32 $810))
    (branch-always #return $810))
  (block #296
    (inputs
      (i32 $776))
    (i32 $835 (constant 107))
    (i1 $836 (equal $776 $835))
    (branch-if $836
      (0 #320 $776)
      (1 #319 $776)))
  (block #319
    (inputs
      (i32 $837))
    (i32 $839 (constant 108))
    (i1 $840 (equal $837 $839))
    (branch-if $840
      (0 #323 $837)
      (1 #322 $837)))
  (block #322
    (inputs
      (i32 $841))
    (branch-always #return $841))
  (block #323
    (inputs
      (i32 $842))
    (branch-always #return $842))
  (block #320
    (inputs
      (i32 $838))
    (i32 $865 (constant 111))
    (i1 $866 (equal $838 $865))
    (branch-if $866
      (0 #332 $838)
      (1 #331 $838)))
  (block #331
    (inputs
      (i32 $867))
    (branch-always #return $867))
  (block #332
    (inputs
      (i32 $868))
    (branch-always #return $868))
  (block #293
    (inputs
      (i32 $772))
    (i32 $895 (constant 114))
    (i1 $896 (equal $772 $895))
    (branch-if $896
      (0 #341 $772)
      (1 #340 $772)))
  (block #340
    (inputs
      (i32 $897))
    (i32 $899 (constant 115))
    (i1 $900 (equal $897 $899))
    (branch-if $900
      (0 #344 $897)
      (1 #343 $897)))
  (block #343
    (inputs
      (i32 $901))
    (i32 $903 (constant 116))
    (i1 $904 (equal $901 $903))
    (branch-if $904
      (0 #347 $901)
      (1 #346 $901)))
  (block #346
    (inputs
      (i32 $905))
    (branch-always #return $905))
  (block #347
    (inputs
      (i32 $906))
    (branch-always #return $906))
  (block #344
    (inputs
      (i32 $902))
    (i32 $929 (constant 119))
    (i1 $930 (equal $902 $929))
    (branch-if $930
      (0 #356 $902)
      (1 #355 $902)))
  (block #355
    (inputs
      (i32 $931))
    (branch-always #return $931))
  (block #356
    (inputs
      (i32 $932))
    (branch-always #return $932))
  (block #341
    (inputs
      (i32 $898))
    (i32 $957 (constant 122))
    (i1 $958 (equal $898 $957))
    (branch-if $958
      (0 #365 $898)
      (1 #364 $898)))
  (block #364
    (inputs
      (i32 $959))
    (i32 $961 (constant 123))
    (i1 $962 (equal $959 $961))
    (branch-if $962
      (0 #368 $959)
      (1 #367 $959)))
  (block #367
    (inputs
      (i32 $963))
    (branch-always #return $963))
  (block #368
    (inputs
      (i32 $964))
    (branch-always #return $964))
  (block #365
    (inputs
      (i32 $960))
    (i32 $987 (constant 126))
    (i1 $988 (equal $960 $987))
    (branch-if $988
      (0 #377 $960)
      (1 #376 $960)))
  (block #376
    (inputs
      (i32 $989))
    (branch-always #return $989))
  (block #377
    (inputs
      (i32 $990))
    (branch-always #return $990))
  (block #2
    (inputs
      (i32 $4))
    (i32 $1023 (constant 129))
    (i1 $1024 (equal $4 $1023))
    (branch-if $1024
      (0 #386 $4)
      (1 #385 $4)))
  (block #385
    (inputs
      (i32 $1025))
    (i32 $1027 (constant 130))
    (i1 $1028 (equal $1025 $1027))
    (branch-if $1028
      (0 #389 $1025)
      (1 #388 $1025)))
  (block #388
    (inputs
      (i32 $1029))
    (i32 $1031 (constant 131))
    (i1 $1032 (equal $1029 $1031))
    (branch-if $1032
      (0 #392 $1029)
      (1 #391 $1029)))
  (block #391
    (inputs
      (i32 $1033))
    (i32 $1035 (constant 132))
    (i1 $1036 (equal $1033 $1035))
    (branch-if $1036
      (0 #395 $1033)
      (1 #394 $1033)))
  (block #394
    (inputs
      (i32 $1037))
    (i32 $1039 (constant 133))
    (i1 $1040 (equal $1037 $1039))
    (branch-if $1040
      (0 #398 $1037)
      (1 #397 $1037)))
  (block #397
    (inputs
      (i32 $1041))
    (i32 $1043 (constant 134))
    (i1 $1044 (equal $1041 $1043))
    (branch-if $1044
      (0 #401 $1041)
      (1 #400 $1041)))
  (block #400
    (inputs
      (i32 $1045))
    (branch-always #return $1045))
  (block #401
    (inputs
      (i32 $1046))
    (branch-always #return $1046))
  (block #398
    (inputs
      (i32 $1042))
    (i32 $1069 (constant 137))
    (i1 $1070 (equal $1042 $1069))
    (branch-if $1070
      (0 #410 $1042)
      (1 #409 $1042)))
  (block #409
    (inputs
      (i32 $1071))
    (branch-always #return $1071))
  (block #410
    (inputs
      (i32 $1072))
    (branch-always #return $1072))
  (block #395
    (inputs
      (i32 $1038))
    (i32 $1097 (constant 140))
    (i1 $1098 (equal $1038 $1097))
    (branch-if $1098
      (0 #419 $1038)
      (1 #418 $1038)))
  (block #418
    (inputs
      (i32 $1099))
    (i32 $1101 (constant 141))
    (i1 $1102 (equal $1099 $1101))
    (branch-if $1102
      (0 #422 $1099)
      (1 #421 $1099)))
  (block #421
    (inputs
      (i32 $1103))
    (branch-always #return $1103))
  (block #422
    (inputs
      (i32 $1104))
    (branch-always #return $1104))
  (block #419
    (inputs
      (i32 $1100))
    (i32 $1127 (constant 144))
    (i1 $1128 (equal $1100 $1127))
    (branch-if $1128
      (0 #431 $1100)
      (1 #430 $1100)))
  (block #430
    (inputs
      (i32 $1129))
    (branch-always #return $1129))
  (block #431
    (inputs
      (i32 $1130))
    (branch-always #return $1130))
  (block #392
    (inputs
      (i32 $1034))
    (i32 $1157 (constant 147))
    (i1 $1158 (equal $1034 $1157))
    (branch-if $1158
      (0 #440 $1034)
      (1 #439 $1034)))
  (block #439
    (inputs
      (i32 $1159))
    (i32 $1161 (constant 148))
    (i1 $1162 (equal $1159 $1161))
    (branch-if $1162
      (0 #443 $1159)
      (1 #442 $1159)))
  (block #442
    (inputs
      (i32 $1163))
    (i32 $1165 (constant 149))
    (i1 $1166 (equal $1163 $1165))
    (branch-if $1166
      (0 #446 $1163)
      (1 #445 $1163)))
  (block #445
    (inputs
      (i32 $1167))
    (branch-always #return $1167))
  (block #446
    (inputs
      (i32 $1168))
    (branch-always #return $1168))
  (block #443
    (inputs
      (i32 $1164))
    (i32 $1191 (constant 152))
    (i1 $1192 (equal $1164 $1191))
    (branch-if $1192
      (0 #455 $1164)
      (1 #454 $1164)))
  (block #454
    (inputs
      (i32 $1193))
    (branch-always #return $1193))
  (block #455
    (inputs
      (i32 $1194))
    (branch-always #return $1194))
  (block #440
    (inputs
      (i32 $1160))
    (i32 $1219 (constant 155))
    (i1 $1220 (equal $1160 $1219))
    (branch-if $1220
      (0 #464 $1160)
      (1 #463 $1160)))
  (block #463
    (inputs
      (i32 $1221))
    (i32 $1223 (constant 156))
    (i1 $1224 (equal $1221 $1223))
    (branch-if $1224
      (0 #467 $1221)
      (1 #466 $1221)))
  (block #466
    (inputs
      (i32 $1225))
    (branch-always #return $1225))
  (block #467
    (inputs
      (i32 $1226))
    (branch-always #return $1226))
  (block #464
    (inputs
      (i32 $1222))
    (i32 $1249 (constant 159))
    (i1 $1250 (equal $1222 $1249))
    (branch-if $1250
      (0 #476 $1222)
      (1 #475 $1222)))
  (block #475
    (inputs
      (i32 $1251))
    (branch-always #return $1251))
  (block #476
    (inputs
      (i32 $1252))
    (branch-always #return $1252))
  (block #389
    (inputs
      (i32 $1030))
    (i32 $1281 (constant 162))
    (i1 $1282 (equal $1030 $1281))
    (branch-if $1282
      (0 #485 $1030)
      (1 #484 $1030)))
  (block #484
    (inputs
      (i32 $1283))
    (i32 $1285 (constant 163))
    (i1 $1286 (equal $1283 $1285))
    (branch-if $1286
      (0 #488 $1283)
      (1 #487 $1283)))
  (block #487
    (inputs
      (i32 $1287))
    (i32 $1289 (constant 164))
    (i1 $1290 (equal $1287 $1289))
    (branch-if $1290
      (0 #491 $1287)
      (1 #490 $1287)))
  (block #490
    (inputs
      (i32 $1291))
    (i32 $1293 (constant 165))
    (i1 $1294 (equal $1291 $1293))
    (branch-if $1294
      (0 #494 $1291)
      (1 #493 $1291)))
  (block #493
    (inputs
      (i32 $1295))
    (branch-always #return $1295))
  (block #494
    (inputs
      (i32 $1296))
    (branch-always #return $1296))
  (block #491
    (inputs
      (i32 $1292))
    (i32 $1319 (constant 168))
    (i1 $1320 (equal $1292 $1319))
    (branch-if $1320
      (0 #503 $1292)
      (1 #502 $1292)))
  (block #502
    (inputs
      (i32 $1321))
    (branch-always #return $1321))
  (block #503
    (inputs
      (i32 $1322))
    (branch-always #return $1322))
  (block #488
    (inputs
      (i32 $1288))
    (i32 $1347 (constant 171))
    (i1 $1348 (equal $1288 $1347))
    (branch-if $1348
      (0 #512 $1288)
      (1 #511 $1288)))
  (block #511
    (inputs
      (i32 $1349))
    (i32 $1351 (constant 172))
    (i1 $1352 (equal $1349 $1351))
    (branch-if $1352
      (0 #515 $1349)
      (1 #514 $1349)))
  (block #514
    (inputs
      (i32 $1353))
    (branch-always #return $1353))
  (block #515
    (inputs
      (i32 $1354))
    (branch-always #return $1354))
  (block #512
    (inputs
      (i32 $1350))
    (i32 $1377 (constant 175))
    (i1 $1378 (equal $1350 $1377))
    (branch-if $1378
      (0 #524 $1350)
      (1 #523 $1350)))
  (block #523
    (inputs
      (i32 $1379))
    (branch-always #return $1379))
  (block #524
    (inputs
      (i32 $1380))
    (branch-always #return $1380))
  (block #485
    (inputs
      (i32 $1284))
    (i32 $1407 (constant 178))
    (i1 $1408 (equal $1284 $1407))
    (branch-if $1408
      (0 #533 $1284)
      (1 #532 $1284)))
  (block #532
    (inputs
      (i32 $1409))
    (i32 $1411 (constant 179))
    (i1 $1412 (equal $1409 $1411))
    (branch-if $1412
      (0 #536 $1409)
      (1 #535 $1409)))
  (block #535
    (inputs
      (i32 $1413))
    (i32 $1415 (constant 180))
    (i1 $1416 (equal $1413 $1415))
    (branch-if $1416
      (0 #539 $1413)
      (1 #538 $1413)))
  (block #538
    (inputs
      (i32 $1417))
    (branch-always #return $1417))
  (block #539
    (inputs
      (i32 $1418))
    (branch-always #return $1418))
  (block #536
    (inputs
      (i32 $1414))
    (i32 $1441 (constant 183))
    (i1 $1442 (equal $1414 $1441))
    (branch-if $1442
      (0 #548 $1414)
      (1 #547 $1414)))
  (block #547
    (inputs
      (i32 $1443))
    (branch-always #return $1443))
  (block #548
    (inputs
      (i32 $1444))
    (branch-always #return $1444))
  (block #533
    (inputs
      (i32 $1410))
    (i32 $1469 (constant 186))
    (i1 $1470 (equal $1410 $1469))
    (branch-if $1470
      (0 #557 $1410)
      (1 #556 $1410)))
  (block #556
    (inputs
      (i32 $1471))
    (i32 $1473 (constant 187))
    (i1 $1474 (equal $1471 $1473))
    (branch-if $1474
      (0 #560 $1471)
      (1 #559 $1471)))
  (block #559
    (inputs
      (i32 $1475))
    (branch-always #return $1475))
  (block #560
    (inputs
      (i32 $1476))
    (branch-always #return $1476))
  (block #557
    (inputs
      (i32 $1472))
    (i32 $1499 (constant 190))
    (i1 $1500 (equal $1472 $1499))
    (branch-if $1500
      (0 #569 $1472)
      (1 #568 $1472)))
  (block #568
    (inputs
      (i32 $1501))
    (branch-always #return $1501))
  (block #569
    (inputs
      (i32 $1502))
    (branch-always #return $1502))
  (block #386
    (inputs
      (i32 $1026))
    (i32 $1533 (constant 193))
    (i1 $1534 (equal $1026 $1533))
    (branch-if $1534
      (0 #578 $1026)
      (1 #577 $1026)))
  (block #577
    (inputs
      (i32 $1535))
    (i32 $1537 (constant 194))
    (i1 $1538 (equal $1535 $1537))
    (branch-if $1538
      (0 #581 $1535)
      (1 #580 $1535)))
  (block #580
    (inputs
      (i32 $1539))
    (i32 $1541 (constant 195))
    (i1 $1542 (equal $1539 $1541))
    (branch-if $1542
      (0 #584 $1539)
      (1 #583 $1539)))
  (block #583
    (inputs
      (i32 $1543))
    (i32 $1545 (constant 196))
    (i1 $1546 (equal $1543 $1545))
    (branch-if $1546
      (0 #587 $1543)
      (1 #586 $1543)))
  (block #586
    (inputs
      (i32 $1547))
    (i32 $1549 (constant 197))
    (i1 $1550 (equal $1547 $1549))
    (branch-if $1550
      (0 #590 $1547)
      (1 #589 $1547)))
  (block #589
    (inputs
      (i32 $1551))
    (branch-always #return $1551))
  (block #590
    (inputs
      (i32 $1552))
    (branch-always #return $1552))
  (block #587
    (inputs
      (i32 $1548))
    (i32 $1575 (constant 200))
    (i1 $1576 (equal $1548 $1575))
    (branch-if $1576
      (0 #599 $1548)
      (1 #598 $1548)))
  (block #598
    (inputs
      (i32 $1577))
    (branch-always #return $1577))
  (block #599
    (inputs
      (i32 $1578))
    (branch-always #return $1578))
  (block #584
    (inputs
      (i32 $1544))
    (i32 $1603 (constant 203))
    (i1 $1604 (equal $1544 $1603))
    (branch-if $1604
      (0 #608 $1544)
      (1 #607 $1544)))
  (block #607
    (inputs
      (i32 $1605))
    (i32 $1607 (constant 204))
    (i1 $1608 (equal $1605 $1607))
    (branch-if $1608
      (0 #611 $1605)
      (1 #610 $1605)))
  (block #610
    (inputs
      (i32 $1609))
    (branch-always #return $1609))
  (block #611
    (inputs
      (i32 $1610))
    (branch-always #return $1610))
  (block #608
    (inputs
      (i32 $1606))
    (i32 $1633 (constant 207))
    (i1 $1634 (equal $1606 $1633))
    (branch-if $1634
      (0 #620 $1606)
      (1 #619 $1606)))
  (block #619
    (inputs
      (i32 $1635))
    (branch-always #return $1635))
  (block #620
    (inputs
      (i32 $1636))
    (branch-always #return $1636))
  (block #581
    (inputs
      (i32 $1540))
    (i32 $1663 (constant 210))
    (i1 $1664 (equal $1540 $1663))
    (branch-if $1664
      (0 #629 $1540)
      (1 #628 $1540)))
  (block #628
    (inputs
      (i32 $1665))
    (i32 $1667 (constant 211))
    (i1 $1668 (equal $1665 $1667))
    (branch-if $1668
      (0 #632 $1665)
      (1 #631 $1665)))
  (block #631
    (inputs
      (i32 $1669))
    (i32 $1671 (constant 212))
    (i1 $1672 (equal $1669 $1671))
    (branch-if $1672
      (0 #635 $1669)
      (1 #634 $1669)))
  (block #634
    (inputs
      (i32 $1673))
    (branch-always #return $1673))
  (block #635
    (inputs
      (i32 $1674))
    (branch-always #return $1674))
  (block #632
    (inputs
      (i32 $1670))
    (i32 $1697 (constant 215))
    (i1 $1698 (equal $1670 $1697))
    (branch-if $1698
      (0 #644 $1670)
      (1 #643 $1670)))
  (block #643
    (inputs
      (i32 $1699))
    (branch-always #return $1699))
  (block #644
    (inputs
      (i32 $1700))
    (branch-always #return $1700))
  (block #629
    (inputs
      (i32 $1666))
    (i32 $1725 (constant 218))
    (i1 $1726 (equal $1666 $1725))
    (branch-if $1726
      (0 #653 $1666)
      (1 #652 $1666)))
  (block #652
    (inputs
      (i32 $1727))
    (i32 $1729 (constant 219))
    (i1 $1730 (equal $1727 $1729))
    (branch-if $1730
      (0 #656 $1727)
      (1 #655 $1727)))
  (block #655
    (inputs
      (i32 $1731))
    (branch-always #return $1731))
  (block #656
    (inputs
      (i32 $1732))
    (branch-always #return $1732))
  (block #653
    (inputs
      (i32 $1728))
    (i32 $1755 (constant 222))
    (i1 $1756 (equal $1728 $1755))
    (branch-if $1756
      (0 #665 $1728)
      (1 #664 $1728)))
  (block #664
    (inputs
      (i32 $1757))
    (branch-always #return $1757))
  (block #665
    (inputs
      (i32 $1758))
    (branch-always #return $1758))
  (block #578
    (inputs
      (i32 $1536))
    (i32 $1787 (constant 225))
    (i1 $1788 (equal $1536 $1787))
    (branch-if $1788
      (0 #674 $1536)
      (1 #673 $1536)))
  (block #673
    (inputs
      (i32 $1789))
    (i32 $1791 (constant 226))
    (i1 $1792 (equal $1789 $1791))
    (branch-if $1792
      (0 #677 $1789)
      (1 #676 $1789)))
  (block #676
    (inputs
      (i32 $1793))
    (i32 $1795 (constant 227))
    (i1 $1796 (equal $1793 $1795))
    (branch-if $1796
      (0 #680 $1793)
      (1 #679 $1793)))
  (block #679
    (inputs
      (i32 $1797))
    (i32 $1799 (constant 228))
    (i1 $1800 (equal $1797 $1799))
    (branch-if $1800
      (0 #683 $1797)
      (1 #682 $1797)))
  (block #682
    (inputs
      (i32 $1801))
    (branch-always #return $1801))
  (block #683
    (inputs
      (i32 $1802))
    (branch-always #return $1802))
  (block #680
    (inputs
      (i32 $1798))
    (i32 $1825 (constant 231))
    (i1 $1826 (equal $1798 $1825))
    (branch-if $1826
      (0 #692 $1798)
      (1 #691 $1798)))
  (block #691
    (inputs
      (i32 $1827))
    (branch-always #return $1827))
  (block #692
    (inputs
      (i32 $1828))
    (branch-always #return $1828))
  (block #677
    (inputs
      (i32 $1794))
    (i32 $1853 (constant 234))
    (i1 $1854 (equal $1794 $1853))
    (branch-if $1854
      (0 #701 $1794)
      (1 #700 $1794)))
  (block #700
    (inputs
      (i32 $1855))
    (i32 $1857 (constant 235))
    (i1 $1858 (equal $1855 $1857))
    (branch-if $1858
      (0 #704 $1855)
      (1 #703 $1855)))
  (block #703
    (inputs
      (i32 $1859))
    (branch-always #return $1859))
  (block #704
    (inputs
      (i32 $1860))
    (branch-always #return $1860))
  (block #701
    (inputs
      (i32 $1856))
    (i32 $1883 (constant 238))
    (i1 $1884 (equal $1856 $1883))
    (branch-if $1884
      (0 #713 $1856)
      (1 #712 $1856)))
  (block #712
    (inputs
      (i32 $1885))
    (branch-always #return $1885))
  (block #713
    (inputs
      (i32 $1886))
    (branch-always #return $1886))
  (block #674
    (inputs
      (i32 $1790))
    (i32 $1913 (constant 241))
    (i1 $1914 (equal $1790 $1913))
    (branch-if $1914
      (0 #722 $1790)
      (1 #721 $1790)))
  (block #721
    (inputs
      (i32 $1915))
    (i32 $1917 (constant 242))
    (i1 $1918 (equal $1915 $1917))
    (branch-if $1918
      (0 #725 $1915)
      (1 #724 $1915)))
  (block #724
    (inputs
      (i32 $1919))
    (i32 $1921 (constant 243))
    (i1 $1922 (equal $1919 $1921))
    (branch-if $1922
      (0 #728 $1919)
      (1 #727 $1919)))
  (block #727
    (inputs
      (i32 $1923))
    (branch-always #return $1923))
  (block #728
    (inputs
      (i32 $1924))
    (branch-always #return $1924))
  (block #725
    (inputs
      (i32 $1920))
    (i32 $1947 (constant 246))
    (i1 $1948 (equal $1920 $1947))
    (branch-if $1948
      (0 #737 $1920)
      (1 #736 $1920)))
  (block #736
    (inputs
      (i32 $1949))
    (branch-always #return $1949))
  (block #737
    (inputs
      (i32 $1950))
    (branch-always #return $1950))
  (block #722
    (inputs
      (i32 $1916))
    (i32 $1975 (constant 249))
    (i1 $1976 (equal $1916 $1975))
    (branch-if $1976
      (0 #746 $1916)
      (1 #745 $1916)))
  (block #745
    (inputs
      (i32 $1977))
    (i32 $1979 (constant 250))
    (i1 $1980 (equal $1977 $1979))
    (branch-if $1980
      (0 #749 $1977)
      (1 #748 $1977)))
  (block #748
    (inputs
      (i32 $1981))
    (branch-always #return $1981))
  (block #749
    (inputs
      (i32 $1982))
    (branch-always #return $1982))
  (block #746
    (inputs
      (i32 $1978))
    (i32 $2005 (constant 253))
    (i1 $2006 (equal $1978 $2005))
    (branch-if $2006
      (0 #758 $1978)
      (1 #757 $1978)))
  (block #757
    (inputs
      (i32 $2007))
    (branch-always #return $2007))
  (block #758
    (inputs
      (i32 $2008))
    (branch-always #return $2008))))