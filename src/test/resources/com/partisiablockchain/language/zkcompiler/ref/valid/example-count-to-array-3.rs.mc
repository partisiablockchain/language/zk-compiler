(metacircuit
 (function %0
  (output sbi256)
  (block #0
    (i32 $0 (constant 0))
    (sbi256 $8 (constant 0))
    (branch-always #1 $0 $8))
  (block #1
    (inputs
      (i32 $10)
      (sbi256 $11))
    (i32 $12 (next_variable_id $10))
    (i32 $13 (constant 0))
    (i1 $14 (equal $12 $13))
    (i1 $15 (bitwise_not $14))
    (branch-if $15
      (0 #return $11)
      (1 #2 $12 $12 $11)))
  (block #2
    (inputs
      (i32 $16)
      (i32 $17)
      (sbi256 $18))
    (sbi32 $20 (load_variable $16))
    (i32 $21 (constant 0))
    (branch-always #4 $21 $20 $17 $18))
  (block #4
    (inputs
      (i32 $22)
      (sbi32 $25)
      (i32 $28)
      (sbi256 $29))
    (i32 $24 (constant 8))
    (i1 $30 (equal $22 $24))
    (i1 $31 (bitwise_not $30))
    (i32 $32 (constant 1))
    (i32 $33 (add_wrapping $22 $32))
    (branch-if $31
      (0 #1 $28 $29)
      (1 #5 $22 $33 $25 $28 $29)))
  (block #5
    (inputs
      (i32 $34)
      (i32 $35)
      (sbi32 $37)
      (i32 $40)
      (sbi256 $41))
    (i32 $47 (constant 5))
    (i32 $48 (bitshift_left_logical $34 $47))
    (sbi32 $49 (extractdyn $41 32 $48))
    (sbi32 $50 (cast $34))
    (sbi1 $51 (equal $37 $50))
    (sbi32 $75 (constant 1))
    (sbi32 $89 (constant 0))
    (sbi32 $205 (select $51 $75 $89))
    (sbi32 $102 (add_wrapping $49 $205))
    (sbi256 $105 (insertdyn $41 $102 $48))
    (branch-always #4 $35 $37 $40 $105))))