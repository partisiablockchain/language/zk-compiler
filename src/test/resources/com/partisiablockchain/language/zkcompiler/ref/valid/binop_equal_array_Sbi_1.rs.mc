(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi352 $0)
      (sbi352 $1))
    (sbi1 $2 (equal $0 $1))
    (branch-always #return $2))))