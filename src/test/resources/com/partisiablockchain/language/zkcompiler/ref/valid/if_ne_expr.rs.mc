(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 3))
    (sbi1 $3 (equal $0 $2))
    (sbi32 $8 (constant 1337))
    (sbi32 $12 (constant 0))
    (sbi32 $112 (select $3 $12 $8))
    (branch-always #return $112))))