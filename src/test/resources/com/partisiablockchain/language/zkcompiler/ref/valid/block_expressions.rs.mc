(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 2))
    (sbi32 $7 (constant 5))
    (sbi32 $10 (add_wrapping $0 $2))
    (sbi32 $11 (add_wrapping $7 $10))
    (sbi32 $12 (add_wrapping $2 $11))
    (branch-always #return $12))))