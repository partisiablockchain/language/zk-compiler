(metacircuit
 (function %0
  (output i64)
  (block #0
    (i32 $102 (constant 0))
    (i32 $103 (constant -2147483648))
    (i64 $104 (bit_concat $102 $103))
    (branch-always #return $104))))