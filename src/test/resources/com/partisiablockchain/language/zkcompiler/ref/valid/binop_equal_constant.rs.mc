(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 95))
    (i1 $2 (equal $0 $1))
    (branch-always #return $2))))