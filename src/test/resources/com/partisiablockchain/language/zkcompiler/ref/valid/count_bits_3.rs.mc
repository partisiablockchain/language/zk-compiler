(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (call
      (%1 $0)
      (#100 $1)))
  (block #100
    (inputs
      (sbi32 $341)
      (sbi32 $2))
    (call
      (%1 $341)
      (#101 $2)))
  (block #101
    (inputs
      (sbi32 $344)
      (sbi32 $3))
    (sbi32 $4 (add_wrapping $3 $344))
    (branch-always #return $4)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (sbi32 $7)
      (sbi32 $8))
    (i32 $6 (constant 32))
    (i1 $9 (equal $4 $6))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $4 $11))
    (branch-if $10
      (0 #return $7)
      (1 #2 $4 $12 $7 $8)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi32 $16)
      (sbi32 $17))
    (call
      (%2 $17 $13)
      (#102 $14 $16 $17)))
  (block #102
    (inputs
      (i32 $346)
      (sbi32 $348)
      (sbi32 $349)
      (sbi1 $20))
    (call
      (%3 $348)
      (#103 $346 $348 $349 $20)))
  (block #103
    (inputs
      (i32 $352)
      (sbi32 $354)
      (sbi32 $355)
      (sbi1 $357)
      (sbi32 $33))
    (sbi32 $139 (select $357 $33 $354))
    (branch-always #1 $352 $139 $355)))
 (function %2
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (i32 $2 (constant 1))
    (i32 $3 (bitshift_left_logical $2 $1))
    (sbi32 $4 (cast $3))
    (sbi32 $5 (bitwise_and $0 $4))
    (sbi32 $7 (constant 0))
    (sbi1 $8 (equal $5 $7))
    (sbi1 $9 (bitwise_not $8))
    (branch-always #return $9)))
 (function %3
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 1))
    (sbi32 $3 (add_wrapping $0 $2))
    (branch-always #return $3))))