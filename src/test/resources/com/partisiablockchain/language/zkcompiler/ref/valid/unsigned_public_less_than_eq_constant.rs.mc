(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i32 $0)
      (i32 $1))
    (i1 $2 (less_than_or_equal_unsigned $0 $1))
    (branch-if $2
      (0 #2)
      (1 #1)))
  (block #1
    (i1 $9 (constant 1))
    (branch-always #return $9))
  (block #2
    (i1 $15 (constant 0))
    (branch-always #return $15))))