(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 1))
    (sbi64 $1 (load_variable $0))
    (sbi32 $2 (extract $1 32 0))
    (branch-always #return $2))))