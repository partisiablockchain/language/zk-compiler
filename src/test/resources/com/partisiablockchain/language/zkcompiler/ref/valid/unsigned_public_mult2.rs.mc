(metacircuit
 (function %0
  (output i128)
  (block #0
    (i32 $102 (constant 0))
    (i32 $103 (constant 12288))
    (i64 $104 (bit_concat $102 $103))
    (i32 $106 (constant 3))
    (i64 $107 (bit_concat $102 $106))
    (i128 $108 (bit_concat $107 $104))
    (i32 $109 (constant 20480))
    (i64 $111 (bit_concat $109 $109))
    (i32 $113 (constant 2))
    (i64 $114 (bit_concat $102 $113))
    (i128 $115 (bit_concat $114 $111))
    (i128 $2 (mult_wrapping_signed $108 $115))
    (branch-always #return $2))))