(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 3))
    (i1 $2 (equal $0 $1))
    (branch-if $2
      (0 #2)
      (1 #1 $0)))
  (block #1
    (inputs
      (i32 $3))
    (sbi32 $5 (cast $3))
    (branch-always #return $5))
  (block #2
    (sbi32 $9 (constant 3))
    (branch-always #return $9))))