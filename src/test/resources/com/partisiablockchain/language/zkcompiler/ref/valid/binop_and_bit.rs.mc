(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (i32 $5 (constant 0))
    (branch-always #1 $5))
  (block #1
    (inputs
      (i32 $6))
    (i32 $8 (constant 10001))
    (sbi1 $9 (constant 1))
    (i1 $10 (equal $6 $8))
    (i1 $11 (bitwise_not $10))
    (i32 $12 (constant 1))
    (i32 $13 (add_wrapping $6 $12))
    (branch-if $11
      (0 #return $9)
      (1 #2 $13)))
  (block #2
    (inputs
      (i32 $15))
    (branch-always #1 $15))))