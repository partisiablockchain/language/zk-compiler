(metacircuit
 (function %0
  (output sbi16)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi8 $1 (extract $0 8 0))
    (sbi8 $4 (constant 9))
    (sbi16 $5 (bit_concat $4 $1))
    (branch-always #return $5))))