(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (sbi32 $0))
    (branch-always #1))
  (block #1
    (i32 $4 (constant 1))
    (branch-always #3 $4))
  (block #2
    (i32 $7 (constant 3))
    (branch-always #3 $7))
  (block #3
    (inputs
      (i32 $5))
    (branch-always #5 $5))
  (block #4
    (inputs
      (i32 $10))
    (i32 $15 (constant 10))
    (i32 $16 (mult_wrapping_signed $10 $15))
    (branch-always #6 $16 $10))
  (block #5
    (inputs
      (i32 $12))
    (i32 $20 (constant 30))
    (i32 $21 (mult_wrapping_signed $12 $20))
    (branch-always #6 $21 $12))
  (block #6
    (inputs
      (i32 $17)
      (i32 $18))
    (i32 $25 (add_wrapping $17 $18))
    (branch-always #return $25))))