(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (i32 $0 (constant 1))
    (sbi64 $1 (load_variable $0))
    (i32 $2 (constant 2))
    (sbi64 $3 (load_variable $2))
    (sbi64 $4 (add_wrapping $1 $3))
    (branch-always #return $4))))