(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi1 $0))
    (sbi1 $3 (bitwise_not $0))
    (sbi1 $105 (bitwise_and $0 $3))
    (branch-always #return $105))))