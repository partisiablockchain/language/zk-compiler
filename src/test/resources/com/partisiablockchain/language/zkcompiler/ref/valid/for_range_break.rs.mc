(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 0))
    (sbi32 $1 (constant 0))
    (branch-always #1 $0 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $6))
    (i32 $5 (constant 11))
    (i1 $7 (equal $3 $5))
    (i1 $8 (bitwise_not $7))
    (i32 $9 (constant 1))
    (i32 $10 (add_wrapping $3 $9))
    (branch-if $8
      (0 #return $6)
      (1 #2 $3 $10 $6)))
  (block #2
    (inputs
      (i32 $11)
      (i32 $12)
      (sbi32 $14))
    (i32 $16 (constant 5))
    (i1 $17 (equal $11 $16))
    (branch-if $17
      (0 #6 $11 $12 $14)
      (1 #return $14)))
  (block #6
    (inputs
      (i32 $30)
      (i32 $34)
      (sbi32 $38))
    (sbi32 $44 (cast $30))
    (sbi32 $45 (add_wrapping $38 $44))
    (branch-always #1 $34 $45))))