(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi32 $2 (extract $0 32 32))
    (sbi8 $3 (extract $2 8 16))
    (branch-always #return $3))))