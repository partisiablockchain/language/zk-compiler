(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (mult_wrapping_signed $0 $0))
    (branch-always #return $1))))