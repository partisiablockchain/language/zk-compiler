(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (i32 $0 (constant 1))
    (sbi32 $1 (constant 1))
    (sbi32 $3 (constant 0))
    (branch-always #1 $0 $1 $3 $3))
  (block #1
    (inputs
      (i32 $7)
      (sbi32 $10)
      (sbi32 $11)
      (sbi32 $12))
    (i32 $9 (constant 4))
    (i1 $13 (equal $7 $9))
    (i1 $14 (bitwise_not $13))
    (i32 $15 (constant 1))
    (i32 $16 (add_wrapping $7 $15))
    (branch-if $14
      (0 #return $10 $12)
      (1 #2 $7 $16 $10 $11 $12)))
  (block #2
    (inputs
      (i32 $17)
      (i32 $18)
      (sbi32 $20)
      (sbi32 $21)
      (sbi32 $22))
    (sbi32 $26 (load_variable $17))
    (sbi1 $27 (less_than_or_equal_signed $26 $21))
    (sbi32 $46 (cast $17))
    (sbi1 $56 (less_than_or_equal_signed $26 $22))
    (sbi32 $182 (select $56 $22 $26))
    (sbi32 $183 (select $27 $20 $46))
    (sbi32 $184 (select $27 $21 $26))
    (sbi32 $185 (select $27 $182 $21))
    (branch-always #1 $18 $183 $184 $185))))