(metacircuit
 (function %0
  (output sbi48)
  (block #0
    (sbi16 $16 (constant 4095))
    (sbi32 $123 (constant 554696464))
    (sbi48 $124 (bit_concat $16 $123))
    (branch-always #return $124)))
 (function %1
  (output sbi1)
  (block #0
    (inputs
      (i32 $0)
      (i8 $1)
      (i16 $2))
    (call
      (%0)
      (#100 $0 $1 $2)))
  (block #100
    (inputs
      (i32 $224)
      (i8 $225)
      (i16 $226)
      (sbi48 $3))
    (i32 $4 (constant 24))
    (i32 $5 (mult_wrapping_signed $4 $224))
    (sbi8 $6 (extractdyn $3 8 $5))
    (sbi8 $7 (cast $225))
    (sbi1 $8 (equal $6 $7))
    (i32 $11 (constant 8))
    (i32 $12 (add_wrapping $5 $11))
    (sbi16 $13 (extractdyn $3 16 $12))
    (sbi16 $14 (cast $226))
    (sbi1 $15 (equal $13 $14))
    (sbi1 $16 (bitwise_and $8 $15))
    (branch-always #return $16))))