(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 9))
    (branch-always #return $1))))