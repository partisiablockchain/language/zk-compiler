(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi128 $0))
    (sbi32 $1 (extract $0 32 32))
    (branch-always #return $1))))