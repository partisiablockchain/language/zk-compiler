(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (sbi32 $0))
    (branch-always #1))
  (block #1
    (i32 $4 (constant 1))
    (branch-always #3 $4))
  (block #2
    (i32 $7 (constant 3))
    (branch-always #3 $7))
  (block #3
    (inputs
      (i32 $5))
    (branch-always #4 $5))
  (block #4
    (inputs
      (i32 $10))
    (branch-always #6 $10))
  (block #5
    (inputs
      (i32 $13))
    (branch-always #6 $13))
  (block #6
    (inputs
      (i32 $19))
    (branch-always #return $19))))