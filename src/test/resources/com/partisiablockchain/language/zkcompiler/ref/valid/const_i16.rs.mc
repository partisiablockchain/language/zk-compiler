(metacircuit
 (function %0
  (output i16)
  (block #0
    (inputs
      (i16 $0))
    (i16 $1 (constant 23))
    (i16 $2 (add_wrapping $0 $1))
    (branch-always #return $2))))