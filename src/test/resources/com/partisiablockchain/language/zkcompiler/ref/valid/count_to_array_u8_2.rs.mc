(metacircuit
 (function %0
  (output i64)
  (block #0
    (inputs
      (i256 $0))
    (i32 $13 (constant 0))
    (i64 $15 (constant 0))
    (branch-always #1 $13 $15 $0))
  (block #1
    (inputs
      (i32 $17)
      (i64 $20)
      (i256 $21))
    (i32 $19 (constant 32))
    (i1 $22 (equal $17 $19))
    (i1 $23 (bitwise_not $22))
    (i32 $24 (constant 1))
    (i32 $25 (add_wrapping $17 $24))
    (branch-if $23
      (0 #return $20)
      (1 #2 $17 $25 $20 $21)))
  (block #2
    (inputs
      (i32 $26)
      (i32 $27)
      (i64 $29)
      (i256 $30))
    (i32 $33 (constant 3))
    (i32 $34 (bitshift_left_logical $26 $33))
    (i8 $35 (extractdyn $30 8 $34))
    (i32 $36 (constant 0))
    (branch-always #4 $36 $35 $27 $29 $30))
  (block #4
    (inputs
      (i32 $37)
      (i8 $40)
      (i32 $43)
      (i64 $45)
      (i256 $46))
    (i32 $39 (constant 8))
    (i1 $47 (equal $37 $39))
    (i1 $48 (bitwise_not $47))
    (i32 $49 (constant 1))
    (i32 $50 (add_wrapping $37 $49))
    (branch-if $48
      (0 #1 $43 $45 $46)
      (1 #5 $37 $50 $40 $43 $45 $46)))
  (block #5
    (inputs
      (i32 $51)
      (i32 $52)
      (i8 $54)
      (i32 $57)
      (i64 $59)
      (i256 $60))
    (i8 $68 (extract $51 8 0))
    (i1 $69 (equal $54 $68))
    (branch-if $69
      (0 #8 $51 $52 $54 $57 $59 $60)
      (1 #7 $51 $52 $54 $57 $59 $60)))
  (block #7
    (inputs
      (i32 $70)
      (i32 $72)
      (i8 $74)
      (i32 $77)
      (i64 $79)
      (i256 $80))
    (i8 $92 (constant 1))
    (branch-always #9 $92 $70 $72 $74 $77 $79 $80))
  (block #8
    (inputs
      (i32 $81)
      (i32 $83)
      (i8 $85)
      (i32 $88)
      (i64 $90)
      (i256 $91))
    (i8 $105 (constant 0))
    (branch-always #9 $105 $81 $83 $85 $88 $90 $91))
  (block #9
    (inputs
      (i8 $93)
      (i32 $94)
      (i32 $96)
      (i8 $98)
      (i32 $101)
      (i64 $103)
      (i256 $104))
    (i32 $118 (constant 3))
    (i32 $119 (bitshift_left_logical $94 $118))
    (i8 $120 (extractdyn $103 8 $119))
    (i8 $121 (add_wrapping $93 $120))
    (i64 $124 (insertdyn $103 $121 $119))
    (branch-always #4 $96 $98 $101 $124 $104))))