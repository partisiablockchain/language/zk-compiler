(metacircuit
 (function %0
  (output sbi16)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi8 $2 (extract $0 8 8))
    (sbi8 $4 (constant 9))
    (sbi16 $5 (bit_concat $2 $4))
    (branch-always #return $5))))