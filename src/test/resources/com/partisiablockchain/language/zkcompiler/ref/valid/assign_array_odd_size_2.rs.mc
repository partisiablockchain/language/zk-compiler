(metacircuit
 (function %0
  (output sbi192)
  (block #0
    (sbi32 $141 (constant 50462976))
    (sbi32 $143 (constant 1284))
    (sbi64 $145 (bit_concat $143 $141))
    (sbi32 $146 (constant 0))
    (sbi64 $153 (bit_concat $146 $146))
    (sbi128 $157 (bit_concat $153 $145))
    (sbi192 $158 (bit_concat $153 $157))
    (branch-always #return $158)))
 (function %1
  (output sbi1)
  (block #0
    (inputs
      (i32 $0)
      (i8 $1)
      (i8 $2)
      (i8 $3))
    (call
      (%0)
      (#100 $0 $1 $2 $3)))
  (block #100
    (inputs
      (i32 $258)
      (i8 $259)
      (i8 $260)
      (i8 $261)
      (sbi192 $4))
    (i32 $5 (constant 24))
    (i32 $6 (mult_wrapping_signed $5 $258))
    (sbi8 $7 (extractdyn $4 8 $6))
    (sbi8 $8 (cast $259))
    (sbi1 $9 (equal $7 $8))
    (i32 $12 (constant 8))
    (i32 $13 (add_wrapping $6 $12))
    (sbi8 $14 (extractdyn $4 8 $13))
    (sbi8 $15 (cast $260))
    (sbi1 $16 (equal $14 $15))
    (sbi1 $17 (bitwise_and $9 $16))
    (i32 $20 (constant 16))
    (i32 $21 (add_wrapping $6 $20))
    (sbi8 $22 (extractdyn $4 8 $21))
    (sbi8 $23 (cast $261))
    (sbi1 $24 (equal $22 $23))
    (sbi1 $25 (bitwise_and $17 $24))
    (branch-always #return $25))))