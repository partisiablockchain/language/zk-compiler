(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (i32 $3 (constant 1))
    (i32 $4 (add_wrapping $0 $3))
    (branch-always #1 $1 $4 $2))
  (block #1
    (inputs
      (i32 $6)
      (i32 $7)
      (sbi32 $8))
    (i1 $10 (equal $6 $7))
    (i1 $11 (bitwise_not $10))
    (i32 $12 (constant 1))
    (i32 $13 (add_wrapping $6 $12))
    (branch-if $11
      (0 #return $8)
      (1 #2 $6 $13 $7 $8)))
  (block #2
    (inputs
      (i32 $14)
      (i32 $15)
      (i32 $16)
      (sbi32 $17))
    (sbi32 $22 (constant 10))
    (sbi1 $23 (less_than_or_equal_signed $17 $22))
    (sbi32 $36 (cast $14))
    (sbi32 $243 (constant 0))
    (sbi32 $244 (select $23 $36 $243))
    (sbi32 $143 (add_wrapping $17 $244))
    (branch-always #1 $15 $16 $143))))