(metacircuit
 (function %0
  (output sbi192)
  (block #0
    (sbi32 $153 (constant 50462976))
    (sbi32 $155 (constant 0))
    (sbi64 $157 (bit_concat $155 $155))
    (sbi32 $163 (constant 1284))
    (sbi64 $165 (bit_concat $163 $153))
    (sbi128 $172 (bit_concat $157 $165))
    (sbi192 $173 (bit_concat $157 $172))
    (branch-always #return $173)))
 (function %1
  (output sbi1)
  (block #0
    (inputs
      (i32 $0)
      (i8 $1)
      (i8 $2)
      (i8 $3))
    (call
      (%0)
      (#100 $0 $1 $2 $3)))
  (block #100
    (inputs
      (i32 $273)
      (i8 $274)
      (i8 $275)
      (i8 $276)
      (sbi192 $4))
    (i32 $5 (constant 24))
    (i32 $6 (mult_wrapping_signed $5 $273))
    (sbi8 $7 (extractdyn $4 8 $6))
    (sbi8 $8 (cast $274))
    (sbi1 $9 (equal $7 $8))
    (i32 $12 (constant 8))
    (i32 $13 (add_wrapping $6 $12))
    (sbi8 $14 (extractdyn $4 8 $13))
    (sbi8 $15 (cast $275))
    (sbi1 $16 (equal $14 $15))
    (sbi1 $17 (bitwise_and $9 $16))
    (i32 $20 (constant 16))
    (i32 $21 (add_wrapping $6 $20))
    (sbi8 $22 (extractdyn $4 8 $21))
    (sbi8 $23 (cast $276))
    (sbi1 $24 (equal $22 $23))
    (sbi1 $25 (bitwise_and $17 $24))
    (branch-always #return $25))))