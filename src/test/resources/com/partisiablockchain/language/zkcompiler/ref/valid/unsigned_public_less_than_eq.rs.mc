(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i32 $0)
      (i32 $1))
    (i1 $2 (less_than_or_equal_unsigned $0 $1))
    (branch-always #return $2))))