(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (branch-always #return)))
 (function %1
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%2 $0)
      (#100)))
  (block #100
    (branch-always #return)))
 (function %2
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 10))
    (call
      (%3 $0 $1)
      (#100)))
  (block #100
    (branch-always #return)))
 (function %3
  (output)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (i32 $2 (constant 0))
    (i1 $3 (equal $1 $2))
    (branch-if $3
      (0 #2 $0 $1)
      (1 #1 $0)))
  (block #1
    (inputs
      (sbi32 $4))
    (i0 $8 (output $4))
    (branch-always #return))
  (block #2
    (inputs
      (sbi32 $6)
      (i32 $7))
    (i32 $11 (constant -1))
    (i32 $12 (add_wrapping $7 $11))
    (call
      (%3 $6 $12)
      (#102)))
  (block #102
    (branch-always #return))))