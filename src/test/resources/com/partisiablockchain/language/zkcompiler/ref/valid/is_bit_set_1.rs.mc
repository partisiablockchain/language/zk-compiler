(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (i32 $2 (constant 1))
    (i32 $3 (bitshift_left_logical $2 $1))
    (sbi32 $4 (cast $3))
    (sbi32 $5 (bitwise_and $0 $4))
    (sbi32 $7 (constant 0))
    (sbi1 $8 (equal $5 $7))
    (sbi1 $9 (bitwise_not $8))
    (branch-always #return $9))))