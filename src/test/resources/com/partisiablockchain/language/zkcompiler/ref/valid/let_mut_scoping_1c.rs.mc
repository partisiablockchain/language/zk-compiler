(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $4 (constant 91))
    (sbi1 $5 (less_than_signed $0 $4))
    (sbi32 $11 (constant 2))
    (sbi32 $13 (constant 31))
    (sbi1 $14 (less_than_signed $13 $0))
    (sbi32 $22 (constant 3))
    (sbi32 $148 (select $14 $22 $11))
    (sbi32 $33 (constant 4))
    (sbi32 $150 (select $5 $148 $33))
    (sbi32 $38 (constant 99))
    (sbi1 $39 (less_than_signed $0 $38))
    (sbi32 $46 (constant 1))
    (sbi32 $149 (select $39 $150 $46))
    (branch-always #return $149))))