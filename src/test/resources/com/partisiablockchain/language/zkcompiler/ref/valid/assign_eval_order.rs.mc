(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (i32 $5 (constant 32))
    (i1024 $9 (constant 1))
    (i1024 $14 (insertdyn $9 $0 $5))
    (i64 $17 (constant 0))
    (i32 $22 (extractdyn $14 32 $5))
    (i32 $27 (extractdyn $14 32 $1))
    (i32 $28 (constant 5))
    (i32 $29 (bitshift_left_logical $27 $28))
    (i64 $30 (insertdyn $17 $22 $29))
    (i32 $31 (extract $30 32 32))
    (branch-always #return $31))))