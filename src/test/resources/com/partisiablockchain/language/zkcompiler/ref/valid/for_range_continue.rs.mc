(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 0))
    (sbi32 $1 (constant 0))
    (branch-always #1 $0 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $6))
    (i32 $5 (constant 11))
    (i1 $7 (equal $3 $5))
    (i1 $8 (bitwise_not $7))
    (i32 $9 (constant 1))
    (i32 $10 (add_wrapping $3 $9))
    (branch-if $8
      (0 #return $6)
      (1 #2 $3 $10 $6)))
  (block #2
    (inputs
      (i32 $11)
      (i32 $12)
      (sbi32 $14))
    (i32 $16 (constant 1))
    (i32 $17 (bitwise_and $11 $16))
    (i32 $18 (constant 0))
    (i1 $19 (equal $17 $18))
    (branch-if $19
      (0 #6 $11 $12 $14)
      (1 #1 $12 $14)))
  (block #6
    (inputs
      (i32 $34)
      (i32 $38)
      (sbi32 $42))
    (sbi32 $48 (cast $34))
    (sbi32 $49 (add_wrapping $42 $48))
    (branch-always #1 $38 $49))))