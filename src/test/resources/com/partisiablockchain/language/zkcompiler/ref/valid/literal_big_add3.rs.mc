(metacircuit
 (function %0
  (output i128)
  (block #0
    (i32 $109 (constant -1))
    (i64 $111 (bit_concat $109 $109))
    (i128 $122 (bit_concat $111 $111))
    (branch-always #return $122))))