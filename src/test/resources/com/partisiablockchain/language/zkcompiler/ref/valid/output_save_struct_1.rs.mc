(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi8 $0))
    (sbi16 $1 (bit_concat $0 $0))
    (i0 $2 (output $1))
    (branch-always #return))))