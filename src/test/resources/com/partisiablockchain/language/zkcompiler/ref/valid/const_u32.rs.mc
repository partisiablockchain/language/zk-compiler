(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 23))
    (i32 $2 (add_wrapping $0 $1))
    (branch-always #return $2))))