(metacircuit
 (function %0
  (output i64)
  (block #0
    (inputs
      (i64 $0))
    (i32 $1 (constant 0))
    (branch-always #1 $1 $0))
  (block #1
    (inputs
      (i32 $2)
      (i64 $5))
    (i32 $4 (constant 4))
    (i1 $6 (equal $2 $4))
    (i1 $7 (bitwise_not $6))
    (i32 $8 (constant 1))
    (i32 $9 (add_wrapping $2 $8))
    (branch-if $7
      (0 #return $5)
      (1 #2 $2 $9 $5)))
  (block #2
    (inputs
      (i32 $10)
      (i32 $11)
      (i64 $13))
    (i32 $12 (constant 4))
    (i32 $16 (bitshift_left_logical $10 $12))
    (i8 $17 (constant 17))
    (i64 $18 (insertdyn $13 $17 $16))
    (branch-always #1 $11 $18))))