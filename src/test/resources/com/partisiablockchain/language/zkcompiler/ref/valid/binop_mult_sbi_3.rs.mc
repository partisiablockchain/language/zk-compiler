(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 3))
    (sbi32 $3 (mult_wrapping_signed $0 $2))
    (branch-always #return $3))))