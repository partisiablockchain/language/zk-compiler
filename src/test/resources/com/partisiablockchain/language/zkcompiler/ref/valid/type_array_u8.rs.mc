(metacircuit
 (function %0
  (output i8)
  (block #0
    (inputs
      (i64 $0))
    (i32 $1 (constant 0))
    (i8 $5 (constant 0))
    (branch-always #1 $1 $5 $0))
  (block #1
    (inputs
      (i32 $2)
      (i8 $6)
      (i64 $7))
    (i32 $4 (constant 8))
    (i1 $8 (equal $2 $4))
    (i1 $9 (bitwise_not $8))
    (i32 $10 (constant 1))
    (i32 $11 (add_wrapping $2 $10))
    (branch-if $9
      (0 #return $6)
      (1 #2 $2 $11 $6 $7)))
  (block #2
    (inputs
      (i32 $12)
      (i32 $13)
      (i8 $15)
      (i64 $16))
    (i32 $19 (constant 3))
    (i32 $20 (bitshift_left_logical $12 $19))
    (i8 $21 (extractdyn $16 8 $20))
    (i8 $22 (add_wrapping $15 $21))
    (branch-always #1 $13 $22 $16))))