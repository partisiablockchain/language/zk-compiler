(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (i1 $2 (equal $0 $1))
    (branch-if $2
      (0 #2)
      (1 #1)))
  (block #1
    (i32 $13 (constant 1))
    (branch-always #return $13))
  (block #2
    (i32 $18 (constant 2))
    (branch-always #return $18))))