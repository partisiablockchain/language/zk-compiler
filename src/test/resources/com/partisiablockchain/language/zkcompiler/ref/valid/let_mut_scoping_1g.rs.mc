(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $4 (constant 91))
    (sbi1 $5 (less_than_signed $0 $4))
    (sbi32 $11 (constant 2))
    (sbi32 $13 (constant 31))
    (sbi1 $14 (less_than_signed $13 $0))
    (sbi32 $22 (constant 3))
    (sbi32 $146 (select $14 $22 $11))
    (sbi32 $32 (constant 4))
    (sbi32 $148 (select $5 $146 $32))
    (sbi32 $36 (constant 99))
    (sbi1 $37 (less_than_signed $0 $36))
    (sbi32 $44 (constant 1))
    (sbi32 $147 (select $37 $148 $44))
    (branch-always #return $147))))