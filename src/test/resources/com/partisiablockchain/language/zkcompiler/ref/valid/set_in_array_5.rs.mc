(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (i1 $0))
    (sbi32 $121 (constant -256))
    (sbi32 $123 (constant 16777215))
    (sbi64 $125 (bit_concat $123 $121))
    (branch-always #return $125))))