(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (sbi32 $1 (constant -1))
    (sbi64 $106 (bit_concat $1 $1))
    (branch-always #return $106))))