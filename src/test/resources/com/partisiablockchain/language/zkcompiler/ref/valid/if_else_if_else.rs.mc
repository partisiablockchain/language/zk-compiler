(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 3))
    (sbi1 $3 (less_than_signed $0 $2))
    (sbi32 $7 (constant 1))
    (sbi32 $11 (constant 5))
    (sbi1 $12 (less_than_signed $0 $11))
    (sbi32 $16 (constant 2))
    (sbi32 $120 (select $12 $16 $2))
    (sbi32 $121 (select $3 $7 $120))
    (branch-always #return $121))))