(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (i1 $2 (less_than_or_equal_signed $1 $0))
    (i1 $4 (less_than_or_equal_unsigned $1 $0))
    (i1 $5 (equal $2 $4))
    (branch-always #return $5))))