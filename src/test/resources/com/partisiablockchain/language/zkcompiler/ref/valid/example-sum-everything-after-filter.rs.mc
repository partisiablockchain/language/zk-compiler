(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (sbi32 $5)
      (i32 $6))
    (i32 $7 (next_variable_id $4))
    (i32 $8 (constant 0))
    (i1 $9 (equal $7 $8))
    (i1 $10 (bitwise_not $9))
    (branch-if $10
      (0 #return $5)
      (1 #2 $7 $7 $5 $6)))
  (block #2
    (inputs
      (i32 $11)
      (i32 $12)
      (sbi32 $13)
      (i32 $14))
    (sbi32 $17 (load_variable $11))
    (sbi32 $18 (cast $14))
    (sbi1 $19 (less_than_signed $17 $18))
    (sbi32 $238 (constant 0))
    (sbi32 $239 (select $19 $17 $238))
    (sbi32 $138 (add_wrapping $13 $239))
    (branch-always #1 $12 $138 $14))))