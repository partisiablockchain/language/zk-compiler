(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i64 $0))
    (i32 $1 (extract $0 32 0))
    (branch-always #return $1))))