(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (inputs
      (sbi128 $0))
    (sbi8 $1 (extract $0 8 40))
    (branch-always #return $1))))