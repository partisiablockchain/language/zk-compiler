(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (branch-always #2))
  (block #2
    (i32 $19 (constant 1))
    (sbi32 $20 (load_variable $19))
    (branch-always #return $20))
  (block #3
    (sbi32 $32 (constant 0))
    (branch-always #return $32))))