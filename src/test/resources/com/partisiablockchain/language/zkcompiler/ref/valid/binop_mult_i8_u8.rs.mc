(metacircuit
 (function %0
  (output i8 i8)
  (block #0
    (inputs
      (i8 $0))
    (i8 $1 (mult_wrapping_signed $0 $0))
    (i8 $2 (mult_wrapping_signed $0 $0))
    (branch-always #return $1 $2))))