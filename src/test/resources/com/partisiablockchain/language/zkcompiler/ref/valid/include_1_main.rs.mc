(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi8 $1 (extract $0 8 0))
    (sbi8 $2 (extract $0 8 8))
    (sbi1 $3 (equal $1 $2))
    (branch-always #return $3))))