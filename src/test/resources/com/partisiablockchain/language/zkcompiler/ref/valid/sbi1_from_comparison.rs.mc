(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (i32 $0)
      (i32 $1))
    (i1 $2 (equal $0 $1))
    (sbi1 $3 (cast $2))
    (branch-always #return $3))))