(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1)
      (sbi32 $2))
    (sbi1 $3 (equal $1 $2))
    (sbi32 $11 (constant 1))
    (sbi32 $12 (add_wrapping $1 $11))
    (sbi1 $13 (equal $0 $12))
    (sbi1 $18 (equal $0 $1))
    (sbi1 $118 (select $3 $13 $18))
    (branch-always #return $118))))