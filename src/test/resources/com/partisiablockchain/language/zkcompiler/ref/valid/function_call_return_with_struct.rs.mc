(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (inputs
      (sbi64 $1))
    (sbi32 $2 (extract $1 32 0))
    (branch-always #return $2)))
 (function %1
  (output sbi64)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi64 $1 (bit_concat $0 $0))
    (branch-always #return $1))))