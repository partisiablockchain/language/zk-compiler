(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i1 $0))
    (i1 $1 (constant 0))
    (i1 $2 (equal $0 $1))
    (branch-if $2
      (0 #2 $0)
      (1 #1)))
  (block #1
    (sbi32 $6 (constant 54))
    (branch-always #return $6))
  (block #2
    (inputs
      (i1 $4))
    (i1 $9 (constant 1))
    (i1 $10 (equal $4 $9))
    (branch-if $10
      (0 #5)
      (1 #4)))
  (block #4
    (sbi32 $14 (constant 74))
    (branch-always #return $14))
  (block #5
    (sbi32 $18 (constant 312))
    (branch-always #return $18))))