(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (inputs
      (i32 $1))
    (branch-always #return $1)))
 (function %1
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 2))
    (i1 $2 (less_than_signed $0 $1))
    (branch-if $2
      (0 #2 $0)
      (1 #return $0)))
  (block #2
    (inputs
      (i32 $4))
    (i32 $8 (constant -1))
    (i32 $9 (add_wrapping $4 $8))
    (call
      (%1 $9)
      (#102 $4)))
  (block #102
    (inputs
      (i32 $116)
      (i32 $10))
    (i32 $12 (constant -2))
    (i32 $13 (add_wrapping $12 $116))
    (call
      (%1 $13)
      (#103 $10)))
  (block #103
    (inputs
      (i32 $124)
      (i32 $14))
    (i32 $15 (add_wrapping $14 $124))
    (branch-always #return $15))))