(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (branch-always #return $0)))
 (function %1
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (branch-always #return $0)))
 (function %2
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (inputs
      (i32 $1))
    (call
      (%0 $1)
      (#101)))
  (block #101
    (inputs
      (i32 $2))
    (branch-always #return $2))))