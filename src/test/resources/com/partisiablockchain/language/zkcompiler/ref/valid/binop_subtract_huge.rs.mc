(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $4 (constant -2147483648))
    (sbi32 $6 (add_wrapping $0 $4))
    (branch-always #return $6))))