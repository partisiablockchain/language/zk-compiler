(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (sbi32 $1 (constant 0))
    (i32 $6 (num_variables))
    (i32 $7 (constant 1))
    (i32 $8 (add_wrapping $6 $7))
    (branch-always #1 $7 $8 $1 $1 $1))
  (block #1
    (inputs
      (i32 $10)
      (i32 $11)
      (sbi32 $12)
      (sbi32 $13)
      (sbi32 $14))
    (i1 $15 (equal $10 $11))
    (i1 $16 (bitwise_not $15))
    (i32 $17 (constant 1))
    (i32 $18 (add_wrapping $10 $17))
    (branch-if $16
      (0 #return $12 $14)
      (1 #2 $10 $18 $11 $12 $13 $14)))
  (block #2
    (inputs
      (i32 $19)
      (i32 $20)
      (i32 $21)
      (sbi32 $22)
      (sbi32 $23)
      (sbi32 $24))
    (sbi32 $28 (load_variable $19))
    (sbi1 $29 (less_than_or_equal_signed $28 $23))
    (i32 $48 (load_metadata $19))
    (sbi32 $49 (cast $48))
    (sbi1 $59 (less_than_or_equal_signed $28 $24))
    (sbi32 $185 (select $59 $24 $28))
    (sbi32 $186 (select $29 $22 $49))
    (sbi32 $187 (select $29 $23 $28))
    (sbi32 $188 (select $29 $185 $23))
    (branch-always #1 $20 $21 $186 $187 $188))))