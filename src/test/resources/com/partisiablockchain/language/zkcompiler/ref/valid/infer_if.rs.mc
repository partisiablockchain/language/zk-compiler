(metacircuit
 (function %0
  (output i16)
  (block #0
    (inputs
      (i1 $0))
    (branch-if $0
      (0 #2 $0)
      (1 #1 $0)))
  (block #1
    (inputs
      (i1 $1))
    (i16 $3 (constant 4))
    (branch-always #3 $3 $1))
  (block #2
    (inputs
      (i1 $2))
    (i16 $6 (constant 42))
    (branch-always #3 $6 $2))
  (block #3
    (inputs
      (i16 $4)
      (i1 $5))
    (i1 $9 (bitwise_not $5))
    (branch-if $9
      (0 #5 $4)
      (1 #4 $4)))
  (block #4
    (inputs
      (i16 $10))
    (i16 $14 (constant 79))
    (branch-always #6 $14 $10))
  (block #5
    (inputs
      (i16 $12))
    (i16 $18 (constant 7))
    (branch-always #6 $18 $12))
  (block #6
    (inputs
      (i16 $15)
      (i16 $16))
    (i16 $22 (add_wrapping $15 $16))
    (branch-always #return $22))))