(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi8 $0))
    (sbi24 $1 (constant 0))
    (sbi32 $2 (bit_concat $1 $0))
    (branch-always #return $2))))