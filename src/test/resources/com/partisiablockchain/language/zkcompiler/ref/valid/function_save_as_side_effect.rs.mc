(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $1))
    (branch-always #return)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%2 $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $1))
    (branch-always #return $1)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 10))
    (call
      (%3 $0 $1)
      (#100)))
  (block #100
    (inputs
      (sbi32 $2))
    (branch-always #return $2)))
 (function %3
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (i32 $2 (constant 0))
    (i1 $3 (equal $1 $2))
    (branch-if $3
      (0 #2 $0 $1)
      (1 #1 $0)))
  (block #1
    (inputs
      (sbi32 $4))
    (i0 $8 (output $4))
    (branch-always #return $4))
  (block #2
    (inputs
      (sbi32 $6)
      (i32 $7))
    (i32 $12 (constant -1))
    (i32 $13 (add_wrapping $7 $12))
    (call
      (%3 $6 $13)
      (#102)))
  (block #102
    (inputs
      (sbi32 $14))
    (branch-always #return $14))))