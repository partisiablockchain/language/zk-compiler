(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 1))
    (sbi32 $4 (constant 99))
    (sbi1 $5 (less_than_signed $0 $4))
    (sbi32 $11 (constant 91))
    (sbi1 $12 (less_than_signed $0 $11))
    (sbi32 $20 (constant 31))
    (sbi1 $21 (less_than_signed $20 $0))
    (sbi32 $29 (constant 2))
    (sbi32 $148 (select $21 $29 $2))
    (sbi32 $38 (add_wrapping $2 $148))
    (sbi32 $43 (constant 4))
    (sbi32 $149 (select $12 $38 $43))
    (sbi32 $150 (select $5 $149 $2))
    (branch-always #return $150))))