(metacircuit
 (function %0
  (output i128)
  (block #0
    (i32 $0 (constant -1))
    (i64 $124 (bit_concat $0 $0))
    (i128 $134 (bit_concat $124 $124))
    (branch-always #return $134))))