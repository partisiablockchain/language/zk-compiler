(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 99))
    (sbi32 $4 (constant 3))
    (sbi1 $5 (equal $0 $4))
    (sbi32 $12 (constant 0))
    (sbi32 $127 (select $5 $2 $12))
    (sbi32 $25 (constant 1337))
    (sbi32 $128 (select $5 $25 $127))
    (branch-always #return $128))))