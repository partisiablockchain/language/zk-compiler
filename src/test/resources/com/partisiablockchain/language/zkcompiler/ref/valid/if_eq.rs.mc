(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (equal $0 $1))
    (sbi32 $8 (constant 1))
    (sbi32 $13 (constant 2))
    (sbi32 $113 (select $2 $8 $13))
    (branch-always #return $113))))