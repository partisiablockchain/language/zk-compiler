(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (sbi32 $2 (constant 10000))
    (branch-always #return $2))))