(metacircuit
 (function %0
  (output i128)
  (block #0
    (inputs
      (i128 $0))
    (i32 $7 (constant 67305985))
    (i32 $8 (constant 32))
    (i128 $9 (insertdyn $0 $7 $8))
    (branch-always #return $9))))