(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i8 $1 (extract $0 8 0))
    (i24 $2 (constant 0))
    (i32 $3 (bit_concat $2 $1))
    (branch-always #return $3))))