(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $7 (constant 1))
    (sbi32 $111 (extract $7 32 0))
    (branch-always #return $111))))