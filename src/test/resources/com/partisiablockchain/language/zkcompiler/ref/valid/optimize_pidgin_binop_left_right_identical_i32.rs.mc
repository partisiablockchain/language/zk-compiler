(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i32 $0))
    (i1 $4 (constant 0))
    (i1 $5 (constant 1))
    (i1 $6 (bitwise_not $5))
    (i1 $10 (bitwise_not $4))
    (i32 $11 (add_wrapping $0 $0))
    (i1 $19 (bitwise_not $6))
    (i1 $20 (bitwise_and $10 $19))
    (i1 $21 (bitwise_and $5 $20))
    (i1 $23 (bitwise_and $5 $10))
    (i1 $24 (bitwise_and $21 $23))
    (i32 $25 (constant 1))
    (i32 $26 (bitshift_left_logical $0 $25))
    (i1 $27 (equal $11 $26))
    (i1 $28 (bitwise_and $24 $27))
    (branch-always #return $28))))