(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (i1 $2 (equal $0 $1))
    (branch-if $2
      (0 #3 $0)
      (1 #1)))
  (block #1
    (i1 $6 (constant 0))
    (branch-always #return $6))
  (block #3
    (inputs
      (i32 $7))
    (i32 $10 (constant -1))
    (i32 $11 (add_wrapping $7 $10))
    (call
      (%1 $11)
      (#103)))
  (block #103
    (inputs
      (i1 $12))
    (branch-always #return $12)))
 (function %1
  (output i1)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (i1 $2 (equal $0 $1))
    (branch-if $2
      (0 #3 $0)
      (1 #1)))
  (block #1
    (i1 $6 (constant 1))
    (branch-always #return $6))
  (block #3
    (inputs
      (i32 $7))
    (i32 $10 (constant -1))
    (i32 $11 (add_wrapping $7 $10))
    (call
      (%0 $11)
      (#103)))
  (block #103
    (inputs
      (i1 $12))
    (branch-always #return $12))))