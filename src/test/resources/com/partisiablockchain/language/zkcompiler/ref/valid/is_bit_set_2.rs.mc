(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (call
      (%1 $1)
      (#100 $0)))
  (block #100
    (inputs
      (sbi32 $107)
      (sbi32 $2))
    (sbi32 $3 (bitwise_and $2 $107))
    (sbi32 $5 (constant 0))
    (sbi1 $6 (equal $3 $5))
    (sbi1 $7 (bitwise_not $6))
    (branch-always #return $7)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 1))
    (i32 $2 (bitshift_left_logical $1 $0))
    (sbi32 $3 (cast $2))
    (branch-always #return $3))))