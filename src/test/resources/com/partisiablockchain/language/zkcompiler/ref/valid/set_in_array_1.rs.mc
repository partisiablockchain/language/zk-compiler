(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (inputs
      (sbi8 $0))
    (i32 $1 (constant 0))
    (branch-always #1 $1 $0))
  (block #1
    (inputs
      (i32 $2)
      (sbi8 $5))
    (i32 $4 (constant 8))
    (i1 $6 (equal $2 $4))
    (i1 $7 (bitwise_not $6))
    (i32 $8 (constant 1))
    (i32 $9 (add_wrapping $2 $8))
    (branch-if $7
      (0 #return $5)
      (1 #2 $2 $9 $5)))
  (block #2
    (inputs
      (i32 $10)
      (i32 $11)
      (sbi8 $13))
    (sbi1 $17 (constant 1))
    (sbi8 $20 (insertdyn $13 $17 $10))
    (branch-always #1 $11 $20))))