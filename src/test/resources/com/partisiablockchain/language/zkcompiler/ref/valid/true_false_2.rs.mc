(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (branch-always #1))
  (block #1
    (sbi32 $5 (constant 1))
    (branch-always #3 $5))
  (block #2
    (sbi32 $9 (constant 3))
    (branch-always #3 $9))
  (block #3
    (inputs
      (sbi32 $6))
    (branch-always #5 $6))
  (block #4
    (inputs
      (sbi32 $12))
    (sbi32 $18 (constant 10))
    (branch-always #6 $18 $12))
  (block #5
    (inputs
      (sbi32 $14))
    (sbi32 $23 (constant 30))
    (branch-always #6 $23 $14))
  (block #6
    (inputs
      (sbi32 $19)
      (sbi32 $20))
    (sbi32 $27 (add_wrapping $19 $20))
    (branch-always #return $27))))