(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 1))
    (sbi32 $6 (constant 40))
    (sbi1 $7 (less_than_signed $0 $6))
    (sbi32 $16 (constant 2))
    (sbi32 $123 (select $7 $16 $2))
    (branch-always #return $123))))