(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $0 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (i32 $5)
      (sbi32 $6)
      (i32 $7))
    (i1 $8 (equal $4 $5))
    (i1 $9 (bitwise_not $8))
    (i32 $10 (constant 1))
    (i32 $11 (add_wrapping $4 $10))
    (branch-if $9
      (0 #return $6)
      (1 #2 $4 $11 $5 $6 $7)))
  (block #2
    (inputs
      (i32 $12)
      (i32 $13)
      (i32 $14)
      (sbi32 $15)
      (i32 $16))
    (i32 $19 (constant 0))
    (branch-always #4 $19 $16 $12 $13 $14 $15 $16))
  (block #4
    (inputs
      (i32 $20)
      (i32 $21)
      (i32 $22)
      (i32 $24)
      (i32 $25)
      (sbi32 $26)
      (i32 $27))
    (i1 $28 (equal $20 $21))
    (i1 $29 (bitwise_not $28))
    (i32 $30 (constant 1))
    (i32 $31 (add_wrapping $20 $30))
    (branch-if $29
      (0 #1 $24 $25 $26 $27)
      (1 #5 $20 $31 $21 $22 $24 $25 $26 $27)))
  (block #5
    (inputs
      (i32 $32)
      (i32 $33)
      (i32 $34)
      (i32 $35)
      (i32 $37)
      (i32 $38)
      (sbi32 $39)
      (i32 $40))
    (i1 $47 (equal $32 $35))
    (branch-if $47
      (0 #9 $32 $33 $34 $35 $37 $38 $39 $40)
      (1 #4 $33 $34 $35 $37 $38 $39 $40)))
  (block #9
    (inputs
      (i32 $77)
      (i32 $81)
      (i32 $83)
      (i32 $85)
      (i32 $89)
      (i32 $91)
      (sbi32 $93)
      (i32 $95))
    (sbi32 $106 (cast $77))
    (sbi32 $107 (add_wrapping $93 $106))
    (branch-always #4 $81 $83 $85 $89 $91 $107 $95))))