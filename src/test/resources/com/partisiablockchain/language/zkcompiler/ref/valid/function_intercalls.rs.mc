(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $1))
    (branch-always #return $1)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (branch-always #return $0))))