(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0)
      (i1 $1))
    (branch-if $1
      (0 #2 $0)
      (1 #1 $0)))
  (block #1
    (inputs
      (sbi64 $2))
    (branch-always #3 $2))
  (block #2
    (inputs
      (sbi64 $4))
    (branch-always #3 $4))
  (block #3
    (inputs
      (sbi64 $8))
    (sbi8 $15 (constant 255))
    (i32 $17 (constant 24))
    (sbi64 $18 (insertdyn $8 $15 $17))
    (branch-always #return $18))))