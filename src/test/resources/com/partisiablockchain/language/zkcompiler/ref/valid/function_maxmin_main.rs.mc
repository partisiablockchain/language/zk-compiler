(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi32 $1 (constant 93))
    (sbi32 $3 (constant 39))
    (call
      (%3 $1 $3)
      (#100)))
  (block #100
    (inputs
      (sbi64 $4))
    (sbi32 $5 (extract $4 32 0))
    (sbi32 $6 (extract $4 32 32))
    (sbi32 $7 (add_wrapping $5 $6))
    (branch-always #return $7)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $110 (select $2 $1 $0))
    (branch-always #return $110)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (sbi32 $111 (select $2 $0 $1))
    (branch-always #return $111)))
 (function %3
  (output sbi64)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (call
      (%1 $0 $1)
      (#100 $0 $1)))
  (block #100
    (inputs
      (sbi32 $215)
      (sbi32 $216)
      (sbi32 $2))
    (call
      (%2 $215 $216)
      (#101 $2)))
  (block #101
    (inputs
      (sbi32 $219)
      (sbi32 $3))
    (sbi64 $4 (bit_concat $3 $219))
    (branch-always #return $4))))