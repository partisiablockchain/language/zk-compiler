(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i16 $1 (extract $0 16 0))
    (i16 $2 (extract $0 16 16))
    (i32 $3 (bit_concat $2 $1))
    (branch-always #return $3))))