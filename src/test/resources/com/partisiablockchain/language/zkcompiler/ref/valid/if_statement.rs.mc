(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 2))
    (sbi32 $4 (constant 5))
    (sbi1 $5 (less_than_signed $0 $4))
    (sbi32 $11 (constant 0))
    (sbi32 $15 (constant 10))
    (sbi1 $16 (less_than_signed $0 $15))
    (sbi32 $22 (constant 1))
    (sbi32 $124 (select $16 $22 $2))
    (sbi32 $125 (select $5 $11 $124))
    (branch-always #return $125))))