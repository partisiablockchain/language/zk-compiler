(metacircuit
 (function %0
  (output i16)
  (block #0
    (inputs
      (i16 $0))
    (i16 $1 (constant 0))
    (i16 $7 (constant 1))
    (branch-always #1 $1 $0 $7))
  (block #1
    (inputs
      (i16 $2)
      (i16 $3)
      (i16 $8))
    (i1 $10 (equal $2 $3))
    (i1 $11 (bitwise_not $10))
    (i16 $12 (constant 1))
    (i16 $13 (add_wrapping $2 $12))
    (branch-if $11
      (0 #return $8)
      (1 #2 $13 $3 $8)))
  (block #2
    (inputs
      (i16 $15)
      (i16 $16)
      (i16 $19))
    (i16 $25 (constant 1))
    (i16 $26 (add_wrapping $19 $25))
    (branch-always #1 $15 $16 $26))))