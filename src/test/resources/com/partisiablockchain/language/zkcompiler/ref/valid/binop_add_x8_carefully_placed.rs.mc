(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (add_wrapping $0 $0))
    (sbi32 $3 (add_wrapping $1 $1))
    (sbi32 $7 (add_wrapping $3 $3))
    (branch-always #return $7))))