(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1)
      (sbi32 $2)
      (sbi32 $3))
    (sbi1 $4 (equal $0 $2))
    (sbi1 $5 (equal $1 $3))
    (sbi1 $6 (bitwise_and $4 $5))
    (sbi32 $16 (constant 1))
    (sbi32 $23 (constant 0))
    (sbi32 $123 (select $6 $16 $23))
    (branch-always #return $123))))