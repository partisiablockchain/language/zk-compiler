(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 1))
    (sbi1 $1 (load_variable $0))
    (sbi32 $3 (constant 1337))
    (sbi32 $6 (constant 0))
    (sbi32 $106 (select $1 $3 $6))
    (branch-always #return $106))))