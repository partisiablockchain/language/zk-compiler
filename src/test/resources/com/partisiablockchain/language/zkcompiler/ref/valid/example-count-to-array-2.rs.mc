(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (sbi64 $22 (constant 0))
    (i32 $23 (constant 0))
    (branch-always #1 $23 $22))
  (block #1
    (inputs
      (i32 $24)
      (sbi64 $25))
    (i32 $26 (next_variable_id $24))
    (i32 $27 (constant 0))
    (i1 $28 (equal $26 $27))
    (i1 $29 (bitwise_not $28))
    (branch-if $29
      (0 #return $25)
      (1 #2 $26 $26 $25)))
  (block #2
    (inputs
      (i32 $30)
      (i32 $31)
      (sbi64 $32))
    (sbi8 $34 (load_variable $30))
    (i32 $35 (constant 0))
    (branch-always #4 $35 $34 $31 $32))
  (block #4
    (inputs
      (i32 $36)
      (sbi8 $39)
      (i32 $42)
      (sbi64 $43))
    (i32 $38 (constant 8))
    (i1 $44 (equal $36 $38))
    (i1 $45 (bitwise_not $44))
    (i32 $46 (constant 1))
    (i32 $47 (add_wrapping $36 $46))
    (branch-if $45
      (0 #1 $42 $43)
      (1 #5 $36 $47 $39 $42 $43)))
  (block #5
    (inputs
      (i32 $48)
      (i32 $49)
      (sbi8 $51)
      (i32 $54)
      (sbi64 $55))
    (i8 $61 (extract $48 8 0))
    (sbi8 $62 (cast $61))
    (sbi1 $63 (equal $51 $62))
    (sbi8 $83 (constant 1))
    (sbi8 $95 (constant 0))
    (sbi8 $212 (select $63 $83 $95))
    (i32 $106 (constant 3))
    (i32 $107 (bitshift_left_logical $48 $106))
    (sbi8 $108 (extractdyn $55 8 $107))
    (sbi8 $109 (add_wrapping $108 $212))
    (sbi64 $112 (insertdyn $55 $109 $107))
    (branch-always #4 $49 $51 $54 $112))))