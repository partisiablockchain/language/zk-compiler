(metacircuit
 (function %0
  (output i1 i1)
  (block #0
    (inputs
      (i8 $0)
      (i8 $1))
    (i1 $2 (less_than_or_equal_signed $0 $1))
    (i1 $3 (less_than_or_equal_unsigned $0 $1))
    (branch-always #return $2 $3))))