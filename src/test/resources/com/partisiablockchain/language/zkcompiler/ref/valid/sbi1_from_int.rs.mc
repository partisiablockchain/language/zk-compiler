(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (i1 $0))
    (sbi1 $2 (constant 1))
    (sbi1 $3 (cast $0))
    (sbi1 $4 (equal $2 $3))
    (branch-always #return $4))))