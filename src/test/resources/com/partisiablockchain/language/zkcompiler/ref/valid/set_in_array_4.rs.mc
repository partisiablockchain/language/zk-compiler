(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (i1 $0))
    (sbi32 $117 (constant -16777216))
    (sbi32 $118 (constant 255))
    (sbi64 $119 (bit_concat $117 $118))
    (branch-always #return $119))))