(metacircuit
 (function %0
  (output sbi16)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi8 $2 (extract $0 8 8))
    (sbi8 $4 (constant 4))
    (sbi16 $5 (bit_concat $2 $4))
    (branch-always #return $5)))
 (function %1
  (output sbi1)
  (block #0
    (inputs
      (i8 $0)
      (i8 $1))
    (sbi8 $2 (cast $0))
    (sbi8 $3 (cast $1))
    (sbi16 $4 (bit_concat $3 $2))
    (call
      (%0 $4)
      (#100 $3)))
  (block #100
    (inputs
      (sbi8 $115)
      (sbi16 $5))
    (sbi8 $6 (extract $5 8 0))
    (sbi8 $7 (extract $5 8 8))
    (sbi8 $9 (constant 4))
    (sbi1 $10 (equal $6 $9))
    (sbi1 $11 (equal $7 $115))
    (sbi1 $12 (bitwise_and $10 $11))
    (branch-always #return $12))))