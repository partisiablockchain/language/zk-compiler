(metacircuit
 (function %0
  (output sbi32 sbi32 sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1)
      (sbi32 $2))
    (sbi1 $3 (less_than_or_equal_signed $0 $1))
    (sbi1 $4 (less_than_or_equal_signed $1 $2))
    (sbi1 $5 (bitwise_and $3 $4))
    (sbi1 $18 (less_than_or_equal_signed $0 $2))
    (sbi1 $19 (less_than_or_equal_signed $2 $1))
    (sbi1 $20 (bitwise_and $18 $19))
    (sbi1 $33 (less_than_or_equal_signed $1 $0))
    (sbi1 $35 (bitwise_and $18 $33))
    (sbi1 $49 (less_than_or_equal_signed $2 $0))
    (sbi1 $50 (bitwise_and $4 $49))
    (sbi1 $65 (bitwise_and $19 $33))
    (sbi32 $177 (select $65 $1 $0))
    (sbi32 $178 (select $65 $0 $1))
    (sbi32 $179 (select $50 $1 $2))
    (sbi32 $180 (select $50 $2 $177))
    (sbi32 $181 (select $50 $0 $178))
    (sbi32 $182 (select $35 $1 $179))
    (sbi32 $183 (select $35 $0 $180))
    (sbi32 $184 (select $35 $2 $181))
    (sbi32 $185 (select $20 $0 $182))
    (sbi32 $186 (select $20 $2 $183))
    (sbi32 $187 (select $20 $1 $184))
    (sbi32 $188 (select $5 $0 $185))
    (sbi32 $189 (select $5 $1 $186))
    (sbi32 $190 (select $5 $2 $187))
    (branch-always #return $188 $189 $190))))