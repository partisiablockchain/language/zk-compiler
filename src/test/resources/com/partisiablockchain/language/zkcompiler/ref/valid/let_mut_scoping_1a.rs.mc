(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 1))
    (sbi32 $4 (constant 99))
    (sbi1 $5 (less_than_signed $0 $4))
    (sbi32 $11 (constant 91))
    (sbi1 $12 (less_than_signed $0 $11))
    (sbi32 $18 (constant 2))
    (sbi32 $20 (constant 31))
    (sbi1 $21 (less_than_signed $20 $0))
    (sbi32 $29 (constant 3))
    (sbi32 $145 (select $21 $29 $18))
    (sbi32 $40 (constant 4))
    (sbi32 $146 (select $12 $145 $40))
    (sbi32 $147 (select $5 $146 $2))
    (branch-always #return $147))))