(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi32 $1 (constant 0))
    (i32 $2 (num_variables))
    (i32 $3 (constant 1))
    (i32 $4 (add_wrapping $2 $3))
    (branch-always #1 $3 $4 $1))
  (block #1
    (inputs
      (i32 $6)
      (i32 $7)
      (sbi32 $8))
    (i1 $9 (equal $6 $7))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $6 $11))
    (branch-if $10
      (0 #return $8)
      (1 #2 $6 $12 $7 $8)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (i32 $15)
      (sbi32 $16))
    (sbi32 $18 (load_variable $13))
    (sbi32 $19 (add_wrapping $16 $18))
    (i32 $20 (load_metadata $13))
    (sbi32 $21 (cast $20))
    (sbi32 $22 (add_wrapping $19 $21))
    (branch-always #1 $14 $15 $22))))