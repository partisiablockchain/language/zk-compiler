(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 0))
    (i1 $2 (less_than_or_equal_signed $0 $1))
    (i1 $3 (bitwise_not $2))
    (branch-if $3
      (0 #return)
      (1 #1)))
  (block #1
    (branch-always #return))))