(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi32 $1 (constant 666))
    (i32 $2 (constant 0))
    (branch-always #1 $2 $1))
  (block #1
    (inputs
      (i32 $3)
      (sbi32 $6))
    (i32 $5 (constant 0))
    (i1 $7 (equal $3 $5))
    (i1 $8 (bitwise_not $7))
    (i32 $9 (constant 1))
    (i32 $10 (add_wrapping $3 $9))
    (branch-if $8
      (0 #return $6)
      (1 #2 $10)))
  (block #2
    (inputs
      (i32 $12))
    (sbi32 $17 (constant 999))
    (branch-always #1 $12 $17))))