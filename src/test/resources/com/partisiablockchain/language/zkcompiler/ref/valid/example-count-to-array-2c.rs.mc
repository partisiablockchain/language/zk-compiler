(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (i32 $23 (constant 0))
    (branch-always #1 $23))
  (block #1
    (inputs
      (i32 $24))
    (sbi64 $25 (constant 0))
    (i32 $26 (next_variable_id $24))
    (i32 $27 (constant 0))
    (i1 $28 (equal $26 $27))
    (i1 $29 (bitwise_not $28))
    (branch-if $29
      (0 #return $25)
      (1 #2 $26)))
  (block #2
    (inputs
      (i32 $31))
    (branch-always #1 $31))))