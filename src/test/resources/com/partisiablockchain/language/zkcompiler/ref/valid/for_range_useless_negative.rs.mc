(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (negate $0))
    (i32 $2 (constant 1))
    (i32 $3 (add_wrapping $0 $2))
    (i32 $6 (constant 0))
    (branch-always #1 $1 $3 $6))
  (block #1
    (inputs
      (i32 $4)
      (i32 $5)
      (i32 $7))
    (i1 $9 (equal $4 $5))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $4 $11))
    (branch-if $10
      (0 #return $7)
      (1 #2 $4 $12 $5 $7)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (i32 $15)
      (i32 $16))
    (i32 $20 (add_wrapping $13 $16))
    (branch-always #1 $14 $15 $20))))