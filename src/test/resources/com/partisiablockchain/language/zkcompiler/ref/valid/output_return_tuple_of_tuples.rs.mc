(metacircuit
 (function %0
  (output sbi16 sbi16)
  (block #0
    (inputs
      (sbi8 $0))
    (sbi16 $1 (bit_concat $0 $0))
    (branch-always #return $1 $1))))