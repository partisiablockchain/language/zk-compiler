(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 1))
    (sbi1 $3 (equal $0 $2))
    (sbi32 $9 (constant 0))
    (sbi32 $111 (select $3 $9 $0))
    (branch-always #return $111))))