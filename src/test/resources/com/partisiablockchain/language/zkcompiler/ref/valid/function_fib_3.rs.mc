(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (call
      (%1 $0 $1)
      (#100)))
  (block #100
    (inputs
      (sbi32 $2))
    (branch-always #return $2)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (i32 $2 (constant 0))
    (i1 $3 (equal $1 $2))
    (branch-if $3
      (0 #3 $0 $1)
      (1 #1)))
  (block #1
    (sbi32 $9 (constant 10000000))
    (branch-always #return $9))
  (block #3
    (inputs
      (sbi32 $12)
      (i32 $13))
    (sbi32 $17 (constant 2))
    (sbi1 $18 (less_than_signed $12 $17))
    (i32 $26 (constant -1))
    (sbi32 $27 (constant -1))
    (sbi32 $28 (add_wrapping $12 $27))
    (i32 $31 (add_wrapping $13 $26))
    (call
      (%1 $28 $31)
      (#103 $12 $18 $31)))
  (block #103
    (inputs
      (sbi32 $242)
      (sbi1 $246)
      (i32 $251)
      (sbi32 $32))
    (sbi32 $34 (constant -2))
    (sbi32 $35 (add_wrapping $34 $242))
    (call
      (%1 $35 $251)
      (#104 $242 $246 $32)))
  (block #104
    (inputs
      (sbi32 $252)
      (sbi1 $256)
      (sbi32 $262)
      (sbi32 $39))
    (sbi32 $40 (add_wrapping $39 $262))
    (sbi32 $140 (select $256 $252 $40))
    (branch-always #return $140))))