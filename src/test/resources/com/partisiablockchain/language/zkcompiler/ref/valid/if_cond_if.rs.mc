(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (equal $0 $1))
    (sbi32 $20 (constant 1))
    (sbi32 $25 (constant 2))
    (sbi32 $126 (select $2 $20 $25))
    (branch-always #return $126))))