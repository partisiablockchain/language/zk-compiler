(metacircuit
 (function %0
  (output sbi8 sbi16 sbi8 sbi16)
  (block #0
    (inputs
      (sbi48 $0))
    (sbi8 $1 (extract $0 8 0))
    (sbi16 $2 (extract $0 16 8))
    (sbi8 $3 (extract $0 8 24))
    (sbi16 $4 (extract $0 16 32))
    (branch-always #return $1 $2 $3 $4))))