(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi32 $1 (extract $0 32 0))
    (sbi32 $2 (extract $0 32 32))
    (sbi64 $3 (bit_concat $2 $1))
    (branch-always #return $3))))