(metacircuit
 (function %0
  (output i128)
  (block #0
    (inputs
      (i128 $0))
    (i32 $102 (constant 232))
    (i32 $103 (constant -727379968))
    (i64 $104 (bit_concat $102 $103))
    (i32 $105 (constant 0))
    (i64 $107 (bit_concat $105 $105))
    (i128 $108 (bit_concat $107 $104))
    (i128 $2 (mult_wrapping_signed $0 $108))
    (branch-always #return $2))))