(metacircuit
 (function %18
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%49 $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $1))
    (branch-always #return)))
 (function %48
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $2 (constant 2))
    (sbi32 $3 (mult_wrapping_signed $0 $2))
    (call
      (%49 $3)
      (#100)))
  (block #100
    (inputs
      (sbi32 $4))
    (branch-always #return)))
 (function %49
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%50 $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $1))
    (branch-always #return $1)))
 (function %50
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 10))
    (call
      (%51 $0 $1)
      (#100)))
  (block #100
    (inputs
      (sbi32 $2))
    (branch-always #return $2)))
 (function %51
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (i32 $2 (constant 0))
    (i1 $3 (equal $1 $2))
    (branch-if $3
      (0 #2 $0 $1)
      (1 #1 $0)))
  (block #1
    (inputs
      (sbi32 $4))
    (i0 $8 (output $4))
    (branch-always #return $4))
  (block #2
    (inputs
      (sbi32 $6)
      (i32 $7))
    (i32 $12 (constant -1))
    (i32 $13 (add_wrapping $7 $12))
    (call
      (%51 $6 $13)
      (#102)))
  (block #102
    (inputs
      (sbi32 $14))
    (branch-always #return $14))))