(metacircuit
 (function %0
  (output i128)
  (block #0
    (inputs
      (i128 $0))
    (i32 $102 (constant 160047680))
    (i32 $103 (constant 0))
    (i64 $104 (bit_concat $102 $103))
    (i32 $105 (constant 1262177448))
    (i32 $106 (constant 1518781562))
    (i64 $107 (bit_concat $105 $106))
    (i128 $108 (bit_concat $107 $104))
    (i128 $2 (add_wrapping $0 $108))
    (branch-always #return $2))))