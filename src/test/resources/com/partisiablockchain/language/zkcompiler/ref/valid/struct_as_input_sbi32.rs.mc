(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi32 $1 (extract $0 32 0))
    (branch-always #return $1))))