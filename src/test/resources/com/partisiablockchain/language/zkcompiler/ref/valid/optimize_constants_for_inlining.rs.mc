(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $8 (constant 0))
    (sbi32 $9 (constant 0))
    (branch-always #1 $8 $0 $9))
  (block #1
    (inputs
      (i32 $11)
      (i32 $12)
      (sbi32 $14))
    (i1 $16 (equal $11 $12))
    (i1 $17 (bitwise_not $16))
    (i32 $18 (constant 1))
    (i32 $19 (add_wrapping $11 $18))
    (branch-if $17
      (0 #return $14)
      (1 #2 $19 $12 $14)))
  (block #2
    (inputs
      (i32 $21)
      (i32 $22)
      (sbi32 $24))
    (sbi32 $23 (constant 9))
    (sbi32 $29 (add_wrapping $23 $24))
    (branch-always #1 $21 $22 $29))))