//! assert fail_type_check "Not a place expression"
use pbc_zk::*;

pub fn identity(x: Sbi32) -> Sbi32 {
    (x + x) = x
}
