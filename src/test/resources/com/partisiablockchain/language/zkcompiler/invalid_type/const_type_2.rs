//! assert fail_type_check "Expected i32, but got (i32, i32, i32)"
//! assert fail_type_check "Constant initializer expression must be constant"
const MY_CONST: i32 = (1, 2, 3);
