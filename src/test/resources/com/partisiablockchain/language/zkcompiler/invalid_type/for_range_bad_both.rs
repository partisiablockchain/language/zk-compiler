//! assert fail_type_check "Expected integer type, but got ()"
use pbc_zk::*;

pub fn compute_55() {
    let x: () = ();
    for idx in ()..x {}
}
