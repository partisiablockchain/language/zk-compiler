//! assert fail_type_check "Value 128 doesn't fit in i8"
//! assert fail_type_check "Value 32768 doesn't fit in i16"
//! assert fail_type_check "Value 2147483648 doesn't fit in i32"
//! assert fail_type_check "Value 9223372036854775808 doesn't fit in i64"
//! assert fail_type_check "Value 170141183460469231731687303715884105728 doesn't fit in i128"

pub fn signed_boundary() {
    let _i8 = 0x80i8;
    let _i16 = 0x8000i16;
    let _i32 = 0x80000000i32;
    let _i64 = 0x8000000000000000i64;
    let _i128 = 0x80000000000000000000000000000000i128;
}
