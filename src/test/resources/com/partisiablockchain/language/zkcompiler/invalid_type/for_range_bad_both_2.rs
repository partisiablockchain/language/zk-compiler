//! assert fail_type_check "Expected integer type, but got ()"

use pbc_zk::Sbi32;

pub fn compute_55() -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx in ()..() {
        sum = sum + Sbi32::from(1);
    }
    sum
}
