//! assert fail_type_check "zero is a function"
use pbc_zk::Sbi32;

fn zero() -> Sbi32 {
    Sbi32::from(0)
}

fn fn_as_var(x: Sbi32) -> Sbi32 {
    x + zero
}
