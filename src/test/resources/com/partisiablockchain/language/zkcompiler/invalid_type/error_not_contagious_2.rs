//! assert fail_type_check "Variable x not in scope"
use pbc_zk::*;

pub fn identity() -> i32 {
    1 + x
}
