//! assert fail_type_check "Function 'my_fn' has duplicated parameters 'arg'"
use pbc_zk::*;

fn my_fn(arg: Sbi32, arg: Sbi32) {}
