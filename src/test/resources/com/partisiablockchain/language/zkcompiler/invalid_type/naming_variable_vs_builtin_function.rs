//! assert fail_type_check "num_secret_variables is not a function"
use pbc_zk::*;

fn reuse_builtin_name(x: Sbi32) -> Sbi32 {
    let num_secret_variables: Sbi32 = Sbi32::from(7);
    num_secret_variables();
    num_secret_variables
}
