//! assert fail_type_check "Cannot assign to immutable variable"
use pbc_zk::*;

pub fn identity(x: usize, y: [Sbi32; 4]) -> usize {
    y[0] = x;
    x
}
