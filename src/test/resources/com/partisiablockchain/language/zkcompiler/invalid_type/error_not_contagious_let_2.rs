//! assert fail_type_check "Expected identical types, but got u8 and i8"
//! assert fail_type_check "Expected identical types, but got u8 and i8"
//! assert fail_type_check "Expected identical types, but got u8 and i8"
//! assert fail_type_check "Expected identical types, but got u8 and i8"
//! assert fail_type_check "Expected u8, but got i8"
//! assert fail_type_check "Expected u8, but got i8"
//! assert fail_type_check "Expected u8, but got i8"
//! assert fail_type_check "Expected u8, but got i8"

pub fn compute_55() {
    let error_1: () = 1u8 + 1i8;
    let error_2: () = 1u8 | 1i8;
    let error_3: () = 1u8 < 1i8;
    let error_4: () = 1u8 == 1i8;
}
