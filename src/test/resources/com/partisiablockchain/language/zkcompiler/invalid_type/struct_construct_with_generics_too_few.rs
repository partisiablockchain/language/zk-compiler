//! assert fail_type_check "Wrong type argument count: Expected 1, but got 0"
//! assert fail_type_check "Wrong type argument count: Expected 1, but got 0"

use std::ops::Range;

fn identity(x: Range) -> Range {
    x
}
