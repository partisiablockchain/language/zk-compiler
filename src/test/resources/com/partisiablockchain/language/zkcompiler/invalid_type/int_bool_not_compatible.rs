//! assert fail_type_check "Expected identical types, but got i32 and u1"
//! assert fail_type_check "Expected i32, but got u1"

pub fn add_int_bool() -> i32 {
    5 + true
}
