//! assert fail_type_check "Wrong argument count for builtin function pbc_zk::Sbi32::from: Expected 1, but given 0"
use pbc_zk::*;

fn mult_4(x: Sbi32) -> Sbi32 {
    Sbi32::from()
}
