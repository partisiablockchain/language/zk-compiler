//! assert fail_type_check "Expected pbc_zk::Sbi32, but got ()"
use pbc_zk::*;

fn wrong_type(x: Sbi32) {
    let mut y: Sbi32 = Sbi32::from(0);
    y = {}
}
