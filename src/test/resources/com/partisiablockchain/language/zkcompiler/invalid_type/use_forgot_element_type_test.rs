//! assert fail_type_check "Unknown type Sbi32"
//! use use_forgot_element_type
use crate::invalid_type::use_forgot_element_type::sbi32_from;

pub fn my_sbi32_from(x: i32) -> Sbi32 {
    sbi32_from(x)
}
