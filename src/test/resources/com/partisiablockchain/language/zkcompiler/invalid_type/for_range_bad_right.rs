//! assert fail_type_check "Expected i32, but got ()"
//! assert fail_type_check "Expected (), but got i32"

use pbc_zk::*;

pub fn compute_55() -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx in 11..() {
        sum = sum + Sbi32::from(idx);
    }
    sum
}
