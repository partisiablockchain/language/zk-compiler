//! assert fail_type_check "num_secret_variables is not a function"
// Built-in function name as parameter name
use pbc_zk::*;

pub fn param_overlap(num_secret_variables: i32) -> i32 {
    num_secret_variables + num_secret_variables()
}
