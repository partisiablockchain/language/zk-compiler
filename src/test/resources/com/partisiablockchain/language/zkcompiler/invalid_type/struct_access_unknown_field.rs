//! assert fail_type_check "Unknown field 'v43' in type {{module}}::SomeStruct"

struct SomeStruct {
    v1: i32,
}

pub fn test(s: SomeStruct) -> i32 {
    s.v43
}
