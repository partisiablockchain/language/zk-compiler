//! assert fail_type_check "Unknown type MaxMin"
//! use function_lib_max
use crate::invalid_type::function_lib_max::maxmin;
use pbc_zk::Sbi32;

fn test(x: Sbi32, y: Sbi32) -> MaxMin {
    maxmin(x, y)
}
