//! assert fail_type_check "Unknown type MyType"

use pbc_zk::*;

fn something() -> i32 {
    let x: MyType = 1;
    if x == 2 {
        1
    } else {
        2
    }
}
