//! assert fail_type_check "pbc_zk::Sbi32 is not castable to i32"
use pbc_zk::*;

fn stuff(x: Sbi32) {
    x as i32;
}
