//! assert fail_type_check "Expected i32, but got pbc_zk::Sbi32"

struct MyPair {
    v1: i32,
    v2: i32,
}

use pbc_zk::*;

fn do_stuff() -> MyPair {
    MyPair {
        v1: Sbi32::from(1),
        v2: 45,
    }
}
