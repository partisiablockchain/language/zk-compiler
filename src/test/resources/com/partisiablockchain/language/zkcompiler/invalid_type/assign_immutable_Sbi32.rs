//! assert fail_type_check "Cannot assign to immutable variable"
use pbc_zk::Sbi32;

pub fn identity(x: Sbi32) -> Sbi32 {
    let y: Sbi32 = x;
    y = x;
    y
}
