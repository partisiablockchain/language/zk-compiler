//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"

const CYCLE_1: i32 = CYCLE_2;
const CYCLE_2: i32 = CYCLE_1;

fn identity(x: i32) -> i32 {
    x
}
