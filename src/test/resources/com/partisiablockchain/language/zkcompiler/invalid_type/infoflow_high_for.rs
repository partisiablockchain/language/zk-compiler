//! assert fail_type_check "For-expressions requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
//! assert fail_type_check "Assignments require a context level at most the level of the place expression. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

struct MyStruct {
    a: i32,
}

pub fn infoflow_failure(secret: Sbi32) -> MyStruct {
    let mut value: MyStruct = MyStruct { a: 0 };
    if secret == Sbi32::from(1) {
        for a in 10..20 {
            value = MyStruct { a };
        }
    }
    value
}
