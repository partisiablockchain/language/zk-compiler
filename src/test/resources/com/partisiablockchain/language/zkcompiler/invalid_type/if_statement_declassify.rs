//! assert fail_type_check "Assignments require a context level at most the level of the place expression. Context level was SECRET, but expected PUBLIC"
//! assert fail_type_check "Assignments require a context level at most the level of the place expression. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn if_statement(x: Sbi32) -> i32 {
    let mut output = 2;
    if x < Sbi32::from(5) {
        output = 0;
    } else if x < Sbi32::from(10) {
        output = 1;
    }

    output
}
