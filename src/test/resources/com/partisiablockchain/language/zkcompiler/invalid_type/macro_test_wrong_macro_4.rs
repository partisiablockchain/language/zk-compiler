//! assert fail_type_check "Unknown macro invocation"
use pbc_zk::*;

pub fn add_self(x: i32, y: i32) -> i32 {
    x + y
}

wrong!(add_self(1, 2), 3);
