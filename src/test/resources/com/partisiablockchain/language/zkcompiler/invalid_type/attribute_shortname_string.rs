//! assert fail_type_check "Attribute zk_compute shortname must be an integer"
use pbc_zk::*;

#[zk_compute(shortname = "21")]
pub fn if_eq(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x == y {
        Sbi32::from(1)
    } else {
        Sbi32::from(2)
    }
}
