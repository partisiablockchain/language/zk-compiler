//! assert fail_type_check "Unknown type MyOtherType"

use pbc_zk::*;

fn something() -> i32 {
    let y: MyOtherType = 2;
    if 1 == y {
        1
    } else {
        2
    }
}
