//! assert fail_type_check "Expected identical types, but got i32 and u32"
//! assert fail_type_check "Expected i32, but got u32"
use pbc_zk::Sbi32;

pub fn do_nothing_useful() -> Sbi32 {
    let error = 2i32 + 2u32;
    error[2]
}
