//! assert fail_type_check "Value 256 doesn't fit in u8"
//! assert fail_type_check "Value 65536 doesn't fit in u16"
//! assert fail_type_check "Value 4294967296 doesn't fit in u32"
//! assert fail_type_check "Value 18446744073709551616 doesn't fit in u64"
//! assert fail_type_check "Value 340282366920938463463374607431768211456 doesn't fit in u128"

pub fn unsigned_maxes() {
    let _u8 = 0x100u8;
    let _u16 = 0x10000u16;
    let _u32 = 0x100000000u32;
    let _u64 = 0x10000000000000000u64;
    let _u128 = 0x100000000000000000000000000000000u128;
}
