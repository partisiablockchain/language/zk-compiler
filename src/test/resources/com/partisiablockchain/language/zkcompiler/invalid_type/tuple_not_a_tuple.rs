//! assert fail_type_check "Unknown field 9 in type pbc_zk::Sbi32"
use pbc_zk::*;

pub fn test(v1: Sbi32) -> Sbi32 {
    v1.9
}
