//! assert fail_type_check "Expected pbc_zk::Sbi32, but got i32"
//! assert fail_type_check "Expected pbc_zk::Sbi32, but got i32"
use pbc_zk::*;

pub fn for_expr(x: Sbi32) {
    for i in x..100 {
        i
    }
}
