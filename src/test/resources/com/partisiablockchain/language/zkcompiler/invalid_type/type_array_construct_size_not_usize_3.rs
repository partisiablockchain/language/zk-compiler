//! assert fail_type_check "Expected u32, but got u8"
//! assert fail_type_check "Expected u32, but got u8"
use pbc_zk::Sbi32;

pub fn sum_everything() -> Sbi32 {
    // Initialize state
    let counts: [i32; 2u8] = [1i32; 2u8];
    Sbi32::from(0)
}
