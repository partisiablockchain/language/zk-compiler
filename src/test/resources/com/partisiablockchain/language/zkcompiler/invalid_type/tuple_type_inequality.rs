//! assert fail_type_check "Expected identical types, but got (i32, u32) and (i32, u8)"
//! assert fail_type_check "Expected (i32, u32), but got (i32, u8)"

fn sbi() -> bool {
    (5, 4u32) == (5, 9u8)
}
