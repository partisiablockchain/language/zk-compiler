//! assert fail_type_check "Expected identical types, but got () and i32"
//! assert fail_type_check "Expected (), but got i32"
use pbc_zk::*;

pub fn do_nothing_useful() {
    if 1 == 1 {
        ()
    } else {
        2
    }
}
