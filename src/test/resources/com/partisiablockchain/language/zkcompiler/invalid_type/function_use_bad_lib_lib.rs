//! assert ignore

fn min(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x > y {
        y
    } else {
        x
    }
}
