//! assert fail_type_check "Unknown type MyCleverType<MyCleverType<u32, u32>, u32>"

pub fn identity(x: MyCleverType<MyCleverType<u32, u32>, u32>) -> u32 {
    x.left.left + x.left.right + x.right
}
