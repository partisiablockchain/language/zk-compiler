//! assert fail_type_check "Constant initializer expression must be constant"
const MY_CONST: () = { MY_CONST };

fn identity(x: i32) -> i32 {
    x
}
