//! assert fail_type_check "Wrong type argument count: Expected 0, but got 1"

use pbc_zk::Sbi32;

struct MyPair {
    v1: Sbi32,
}

pub fn test() -> MyPair {
    MyPair::<Sbi32> { v1: Sbi32::from(1) }
}
