//! assert fail_type_check "User-defined type 'MyPair' conflicts with user-defined type {{module}}::MyPair"

struct MyPair {
    v1: i32,
    v2: i32,
}

struct MyPair {}

fn do_nothing() {}
