//! assert fail_type_check "Break requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"

use pbc_zk::Sbi32;

pub fn compute_55(max: i32) -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx in 0..max + 1 {
        if sum > Sbi32::from(10) {
            break;
        }
        sum = sum + Sbi32::from(idx);
    }
    sum
}
