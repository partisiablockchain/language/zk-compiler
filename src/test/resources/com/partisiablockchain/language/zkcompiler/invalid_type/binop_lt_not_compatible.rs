//! assert fail_type_check "Expected identical types, but got pbc_zk::Sbi32 and u32"
//! assert fail_type_check "Expected pbc_zk::Sbi32, but got u32"
use pbc_zk::*;

fn do_nothing(x: Sbi32, y: usize) -> Sbi32 {
    (x < y);
    x
}
