//! assert fail_type_check "Array size expression must be constant"
//! assert fail_type_check "Non-literal sizes for array types are not supported"
use pbc_zk::Sbi32;

fn assignment(x: Sbi32, y: [Sbi32; if true { 9usize } else { 11usize }]) -> Sbi32 {
    y[0]
}
