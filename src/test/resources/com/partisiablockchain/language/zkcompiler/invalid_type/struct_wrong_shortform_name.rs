//! assert fail_type_check "Unknown field 'n1' in type crate::invalid_type::struct_wrong_shortform_name::MyStruct"
//! assert fail_type_check "crate::invalid_type::struct_wrong_shortform_name::MyStruct constructor missing fields 'f1'"

struct MyStruct {
    f1: i32,
}

pub fn identity(x: i32) -> MyStruct {
    let n1 = x;

    MyStruct { n1 }
}
