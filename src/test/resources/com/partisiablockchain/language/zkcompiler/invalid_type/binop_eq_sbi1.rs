//! assert fail_type_check "Expected identical types, but got pbc_zk::Sbu1 and u1"
//! assert fail_type_check "Expected pbc_zk::Sbu1, but got u1"
use pbc_zk::*;

pub fn add_self(x: Sbi32, y: Sbi32) -> Sbi32 {
    if (x == y) == (1 == 2) {
        x
    } else {
        y
    }
}
