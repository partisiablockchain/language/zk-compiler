//! assert fail_type_check "Expected identical types, but got i32 and ()"
//! assert fail_type_check "Expected (), but got i32"
use pbc_zk::*;

pub fn identity() -> bool {
    1 == ()
}
