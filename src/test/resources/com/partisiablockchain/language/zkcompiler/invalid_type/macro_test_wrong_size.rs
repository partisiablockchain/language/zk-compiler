//! assert fail_type_check "pbc_zk::test_eq! requires 2-4 arguments - got 1"
use pbc_zk::*;

pub fn add_self(x: i32, y: i32) -> i32 {
    x + y
}

pbc_zk::test_eq!(add_self(1, 2));
