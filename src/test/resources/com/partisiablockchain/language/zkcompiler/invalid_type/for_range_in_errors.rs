//! assert fail_type_check "Expected integer type, but got ()"
//! assert fail_type_check "Expected (), but got i32"

use pbc_zk::Sbi8;

pub fn compute_55() -> Sbi8 {
    let error = () + 1;
    let mut sum = Sbi8::from(0);
    for idx in error..error {
        sum = sum + Sbi8::from(idx);
    }
    sum
}
