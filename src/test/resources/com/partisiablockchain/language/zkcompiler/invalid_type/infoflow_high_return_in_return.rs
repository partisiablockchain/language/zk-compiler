//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn infoflow_failure(secret: Sbi32) -> Sbi32 {
    return if secret == Sbi32::from(1) {
        return secret;
        Sbi32::from(1)
    } else {
        Sbi32::from(2)
    };
    secret
}
