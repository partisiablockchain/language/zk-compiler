//! assert fail_type_check "Unsupported expression: Indirect function call"
use pbc_zk::*;

fn call_literal(x: Sbi32) -> Sbi32 {
    Sbi32::from(7)(x)
}
