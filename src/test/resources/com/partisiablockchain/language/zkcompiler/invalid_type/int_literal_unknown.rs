//! assert fail_type_check "Unknown type o64"
//! assert ignore

#[rustfmt::skip]
pub fn too_big() -> i64 {
    42o64
}
