//! assert fail_type_check "Expected pbc_zk::Sbi32, but got pbc_zk::Sbu32"
//! assert fail_type_check "Expected identical types, but got pbc_zk::Sbi32 and pbc_zk::Sbu32"
use pbc_zk::*;

fn do_nothing(x: Sbi32, y: Sbu32) -> Sbi32 {
    x + y
}
