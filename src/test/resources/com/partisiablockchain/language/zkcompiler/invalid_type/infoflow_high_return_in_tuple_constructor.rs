//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn infoflow_failure(secret: Sbi32) {
    if secret == Sbi32::from(1) {
        let tuple = (Sbi32::from(1), {
            return ();
        });
    }
}
