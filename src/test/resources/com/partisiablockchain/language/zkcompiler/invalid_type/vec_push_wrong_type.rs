//! assert fail_type_check "Type unification of function arguments failed"
//! assert fail_type_check "Expected std::vec::Vec<???>, but got std::vec::Vec<u32>"
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> u32 {
    let mut vec = Vec::new::<u32>();
    vec.push(x);
    vec.pop()
}
