//! assert ignore
//!
//! This is a small library with simple max and min function implementations.
//!
//! NOTE: There is nothing wrong with this library in itself.
use pbc_zk::Sbi32;

pub(crate) struct MaxMin {
    max: Sbi32,
    min: Sbi32,
}

pub(crate) fn maxmin(x: Sbi32, y: Sbi32) -> MaxMin {
    MaxMin {
        max: max(x, y),
        min: min(x, y),
    }
}

pub(crate) fn max(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x > y {
        x
    } else {
        y
    }
}

pub(crate) fn min(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x > y {
        y
    } else {
        x
    }
}
