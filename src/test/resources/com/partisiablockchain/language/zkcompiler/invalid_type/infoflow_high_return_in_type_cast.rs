//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn infoflow_failure(secret: Sbi16) -> Sbi32 {
    if secret == Sbi16::from(1) {
        ({
            return secret as Sbi32;
            secret
        }) as Sbi32
    } else {
        secret as Sbi32
    }
}
