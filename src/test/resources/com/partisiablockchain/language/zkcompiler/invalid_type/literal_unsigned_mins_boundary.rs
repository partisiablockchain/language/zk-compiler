//! assert fail_type_check "Value -1 doesn't fit in u8"
//! assert fail_type_check "Value -1 doesn't fit in u16"
//! assert fail_type_check "Value -1 doesn't fit in u32"
//! assert fail_type_check "Value -1 doesn't fit in u64"
//! assert fail_type_check "Value -1 doesn't fit in u128"

pub fn unsigned_mins() {
    let _u8 = -1u8;
    let _u16 = -1u16;
    let _u32 = -1u32;
    let _u64 = -1u64;
    let _u128 = -1u128;
}
