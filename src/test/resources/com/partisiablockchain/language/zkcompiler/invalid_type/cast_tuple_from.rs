//! assert fail_type_check "(u16, i16) is not castable to u32"
use pbc_zk::*;

fn stuff(x: (u16, i16)) {
    x as u32;
}
