//! assert fail_type_check "Variable unknown not in scope"
use pbc_zk::Sbi32;

pub fn do_nothing_useful() -> Sbi32 {
    unknown[3]
}
