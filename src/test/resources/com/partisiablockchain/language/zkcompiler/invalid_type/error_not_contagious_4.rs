//! assert fail_type_check "Unknown function sbi32_input"
use pbc_zk::{load_sbi, Sbi32, Sbu1, SecretVarId};

fn sbi() -> Sbu1 {
    let variable_id = SecretVarId::new(1);
    sbi32_input(variable_id) == load_sbi::<Sbi32>(variable_id)
}
