//! assert fail_type_check "{{module}}::MyPair constructor missing fields 'v1', 'v2', 'v3'"

use pbc_zk::Sbi32;

struct MyPair {
    v1: Sbi32,
    v2: Sbi32,
    v3: Sbi32,
}

pub fn test() -> MyPair {
    MyPair {}
}
