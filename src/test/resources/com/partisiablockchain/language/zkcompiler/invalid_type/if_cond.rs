//! assert fail_type_check "Expected boolean type, but got ()"
use pbc_zk::*;

pub fn do_nothing_useful(y: ()) -> i32 {
    if y {
        1
    } else {
        2
    }
}
