//! assert fail_type_check "Unknown function maxmin"
//! use function_lib_max
use crate::invalid_type::function_lib_max::MaxMin;
use pbc_zk::Sbi32;

pub fn test(x: Sbi32, y: Sbi32) -> MaxMin {
    maxmin(x, y)
}
