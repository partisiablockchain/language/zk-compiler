//! use function_use_bad_lib_lib
//! assert fail_type_check "Unknown type Sbi32"
//! assert fail_type_check "Unknown type Sbi32"
//! assert fail_type_check "Unknown type Sbi32"
use crate::invalid_type::function_use_bad_lib_lib::min;
use pbc_zk::Sbi32;

pub fn main(x: Sbi32, y: Sbi32) -> Sbi32 {
    min(x, y)
}
