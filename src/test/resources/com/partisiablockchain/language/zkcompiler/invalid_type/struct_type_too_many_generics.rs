//! assert fail_type_check "Wrong type argument count: Expected 1, but got 2"
//! assert fail_type_check "Expected std::ops::Range<i16>, but got u32"

use std::ops::Range;

fn identity(x: u32) -> Range<i16, u32> {
    x
}
