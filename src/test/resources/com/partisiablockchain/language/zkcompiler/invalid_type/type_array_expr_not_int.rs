//! assert fail_type_check "Expected u32, but got ()"
//! assert fail_type_check "Array size expression must be constant"
//! assert fail_type_check "Non-literal sizes for array types are not supported"
use pbc_zk::Sbi32;

pub fn do_nothing_useful(x: [Sbi32; ()]) {}
