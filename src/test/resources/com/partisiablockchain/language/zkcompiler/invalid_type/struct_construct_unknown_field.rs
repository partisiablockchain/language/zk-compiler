//! assert fail_type_check "Unknown field 'v2' in type {{module}}::MyPair"

struct MyPair {
    v1: Sbi32,
}

use pbc_zk::*;

pub fn test() -> MyPair {
    MyPair {
        v1: Sbi32::from(1),
        v2: Sbi32::from(2),
    }
}
