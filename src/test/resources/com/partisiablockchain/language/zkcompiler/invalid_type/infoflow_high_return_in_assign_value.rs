//! assert fail_type_check "Assignments require a context level at most the level of the place expression. Context level was SECRET, but expected PUBLIC"
//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn infoflow_failure(secret: Sbi32) {
    let mut arr = [(), ()];
    if secret == Sbi32::from(1) {
        arr[0] = {
            return ();
        };
    }
}
