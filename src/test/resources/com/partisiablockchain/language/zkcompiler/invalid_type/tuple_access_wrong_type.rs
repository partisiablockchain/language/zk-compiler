//! assert fail_type_check "Expected boolean type, but got pbc_zk::Sbi32"
use pbc_zk::*;

pub fn test(v1: Sbi32) -> Sbi32 {
    if (v1, v1).1 {
        v1
    } else {
        Sbi32::from(7)
    }
}
