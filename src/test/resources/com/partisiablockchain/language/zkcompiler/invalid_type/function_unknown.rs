//! assert fail_type_check "Unknown function some_unknown_function"
use pbc_zk::*;

fn mult_4(x: Sbi32) -> Sbi32 {
    some_unknown_function(x)
}
