//! assert fail_type_check "pbc_zk::Sbi32::some_unknown_method is not in scope"

use pbc_zk::Sbi32;

pub fn method_call_abs(x: Sbi32) {
    x.some_unknown_method()
}
