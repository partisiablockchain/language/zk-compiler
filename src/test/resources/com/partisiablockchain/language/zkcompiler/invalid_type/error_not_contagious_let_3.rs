//! assert fail_type_check "Expected boolean type, but got i32"
//! assert fail_type_check "Expected u1, but got i32"

pub fn compute_55() {
    let tru = 1 == 1;
    let error_2: () = 1 || tru;
}
