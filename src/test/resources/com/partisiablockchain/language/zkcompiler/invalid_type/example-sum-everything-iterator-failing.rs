//! assert fail_type_check "Could not find items 'secret_variables_ids' in pbc_zk"
//! assert fail_type_check "Unknown function secret_variable_ids"
use pbc_zk;
use pbc_zk::{load_sbi, secret_variables_ids, Sbi32};

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
pub fn sum_everything() -> Sbi32 {
    // Initialize state
    let mut sum: Sbi32 = Sbi32::from(0);

    // Sum each variable
    for variable_id in secret_variable_ids() {
        sum = sum + load_sbi::<Sbi32>(variable_id);
    }

    sum
}
