//! assert fail_type_check "Expected integer type, but got ()"
//! assert fail_type_check "Expected (), but got i32"

use pbc_zk::Sbi8;

pub fn compute_55() -> Sbi8 {
    let error_1 = () + 1;
    let error_2 = error_1 | 1;
    let error_3 = error_2 < 1;
    let error_4 = error_3 == 1;
    error_4
}
