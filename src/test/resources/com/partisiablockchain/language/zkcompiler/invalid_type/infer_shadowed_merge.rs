//! assert fail_type_check "Expected identical types, but got i32 and u8"
//! assert fail_type_check "Expected identical types, but got i32 and u16"
//! assert fail_type_check "Expected i32, but got u8"
//! assert fail_type_check "Expected i32, but got u16"

pub fn infer_chain_valid() {
    let a = 4;
    let b1 = a;
    let c1 = b1;

    let a = 4;

    b1 + 8u8;
    c1 + 8u16;
}
