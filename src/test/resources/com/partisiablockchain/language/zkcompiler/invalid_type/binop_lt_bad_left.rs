//! assert fail_type_check "Expected integer type, but got ()"
//! assert fail_type_check "Expected (), but got pbc_zk::Sbi32"
use pbc_zk::*;

fn do_nothing(x: Sbi32) -> Sbi32 {
    ({} < x);
    x
}
