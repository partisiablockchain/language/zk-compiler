//! assert fail_type_check "Expected u32, but got u8"
use pbc_zk::Sbi32;

pub fn do_nothing_useful(x: [Sbi32; 9u8]) {}
