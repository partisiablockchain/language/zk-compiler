//! assert fail_type_check "Expected identical types, but got (pbc_zk::Sbi32, pbc_zk::Sbi32) and (pbc_zk::Sbi32, pbc_zk::Sbi32, pbc_zk::Sbi32)"
//! assert fail_type_check "Expected (pbc_zk::Sbi32, pbc_zk::Sbi32), but got (pbc_zk::Sbi32, pbc_zk::Sbi32, pbc_zk::Sbi32)"
use pbc_zk::*;

pub fn double_identity(v1: Sbi32) -> Sbi32 {
    if (v1, v1) == (v1, v1, v1) {
        Sbi32::from(1)
    } else {
        Sbi32::from(0)
    }
}
