//! assert fail_type_check "Variable unknown_variable not in scope"
//! assert fail_type_check "Variable unknown_variable not in scope"

fn sbi() -> bool {
    (5, unknown_variable) == (unknown_variable, 5)
}
