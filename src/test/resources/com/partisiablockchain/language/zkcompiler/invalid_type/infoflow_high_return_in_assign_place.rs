//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn infoflow_failure(secret: Sbi32) -> Sbi32 {
    let mut arr = [secret, secret];
    if secret == Sbi32::from(1) {
        arr[{
            return arr[0];
            1
        }] = secret;
    }
    arr[1]
}
