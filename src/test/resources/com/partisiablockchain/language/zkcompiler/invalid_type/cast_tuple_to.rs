//! assert fail_type_check "u32 is not castable to (u16, i16)"
use pbc_zk::*;

fn stuff(x: u32) {
    x as (u16, i16);
}
