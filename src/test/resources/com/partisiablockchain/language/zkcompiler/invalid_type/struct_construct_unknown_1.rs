//! assert fail_type_check "Unknown type MyStruct"

use pbc_zk::*;

pub fn test() -> Sbi32 {
    MyStruct {}
}
