//! assert fail_type_check "Expected integer type, but got ()"
use pbc_zk::*;

pub fn do_nothing_useful(x: Sbi32) -> Sbi32 {
    !{};
    x
}
