//! assert fail_type_check "Expected integer type, but got ()"
//! assert fail_type_check "Expected pbc_zk::Sbi32, but got ()"
use pbc_zk::*;

fn what(x: Sbi32) -> Sbi32 {
    let x: Sbi32 = Sbi32::from(6) + {};
    x
}
