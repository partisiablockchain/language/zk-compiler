//! assert fail_type_check "Expected boolean type, but got i32"
//! assert fail_type_check "Expected i32, but got u1"
use pbc_zk::*;

pub fn xor(x: i32) -> Sbi32 {
    if x && (x == x) {
        Sbi32::from(1)
    } else {
        Sbi32::from(0)
    }
}
