//! assert fail_type_check "Expected pbc_zk::Sbi32, but got ()"
//! assert fail_type_check "Expected pbc_zk::Sbi32, but got ()"
//! assert fail_type_check "Expected pbc_zk::Sbi32, but got ()"
use pbc_zk::*;

fn mult_4(x: Sbi32) -> Sbi32 {
    let mut y1: Sbi32 = Sbi32::from(1);
    let mut y2: Sbi32 = Sbi32::from(2);
    let mut y3: Sbi32 = Sbi32::from(3);
    let mut y4: Sbi32 = Sbi32::from(4);
    y1 = (y2 = (y3 = (y4 = x)));
    y1 + y2 + y3 + y4
}
