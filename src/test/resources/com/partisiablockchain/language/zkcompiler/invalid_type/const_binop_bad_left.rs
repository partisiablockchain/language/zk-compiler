//! assert fail_type_check "Constant initializer expression must be constant"
const MY_ADD: i32 = identity(1) + 3;

fn identity(x: i32) -> i32 {
    x
}
