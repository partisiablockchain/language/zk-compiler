//! assert fail_type_check "Value -129 doesn't fit in i8"
//! assert fail_type_check "Value -32769 doesn't fit in i16"
//! assert fail_type_check "Value -2147483649 doesn't fit in i32"
//! assert fail_type_check "Value -9223372036854775809 doesn't fit in i64"
//! assert fail_type_check "Value -170141183460469231731687303715884105729 doesn't fit in i128"

pub fn signed_boundary() {
    let _i8 = -0x81i8;
    let _i16 = -0x8001i16;
    let _i32 = -0x80000001i32;
    let _i64 = -0x8000000000000001i64;
    let _i128 = -0x80000000000000000000000000000001i128;
}
