//! assert fail_type_check "i32 is not castable to pbc_zk::Sbi32"

use pbc_zk::Sbi32;

fn identity(x: i32) -> Sbi32 {
    x as Sbi32
}
