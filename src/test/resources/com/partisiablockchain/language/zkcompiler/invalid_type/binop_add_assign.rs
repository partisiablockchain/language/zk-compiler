//! assert fail_type_check "Expected integer type, but got ()"
//! assert fail_type_check "Expected pbc_zk::Sbi32, but got ()"
use pbc_zk::*;

fn mult_4(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = Sbi32::from(4);
    x + (y = Sbi32::from(3))
}
