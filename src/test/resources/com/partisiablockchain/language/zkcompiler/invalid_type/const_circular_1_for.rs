//! assert fail_type_check "Constant initializer expression must be constant"
const MY_CONST: () = for i in 1..2 {
    MY_CONST
};

fn identity(x: i32) -> i32 {
    x
}
