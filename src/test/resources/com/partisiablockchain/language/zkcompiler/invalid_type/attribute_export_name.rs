//! assert fail_type_check "Attribute export_name is not allowed"
//! assert fail_type_check "Attribute export_name is not allowed"

use pbc_zk::{save_sbi, Sbi32, Sbu1};

#[export_name = "Blah"]
pub fn main(val: Sbi32) {}

#[export_name("Invalid Format but who is keeping track?")]
pub fn main2(val: Sbi32) {}
