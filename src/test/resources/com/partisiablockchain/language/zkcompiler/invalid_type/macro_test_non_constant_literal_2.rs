//! assert fail_type_check "Expected integer literal"
use pbc_zk::*;

pub fn add_self(x: i32, y: i32) -> i32 {
    x + y
}

pbc_zk::test_eq!(add_self(1, 2), 3, [(4, 3)]);
