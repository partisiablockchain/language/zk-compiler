//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

fn my_fun_function(unit: ()) {}

pub fn infoflow_failure(secret: Sbi32) {
    if secret == Sbi32::from(1) {
        my_fun_function({
            return ();
        })
    }
}
