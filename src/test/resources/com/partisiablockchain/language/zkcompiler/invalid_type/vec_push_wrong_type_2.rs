//! assert fail_type_check "Expected u32, but got pbc_zk::Sbi32"
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> u32 {
    let mut vec = Vec::new::<u32>();
    Vec::push::<u32>(vec, x);
    Vec::pop::<u32>(vec)
}
