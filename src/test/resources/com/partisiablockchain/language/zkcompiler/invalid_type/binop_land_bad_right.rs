//! assert fail_type_check "Expected pbc_zk::Sbu1, but got pbc_zk::Sbi32"
//! assert fail_type_check "Expected boolean type, but got pbc_zk::Sbi32"
use pbc_zk::*;

pub fn xor(x: Sbi32) -> Sbi32 {
    if (x == x) && x {
        Sbi32::from(1)
    } else {
        Sbi32::from(0)
    }
}
