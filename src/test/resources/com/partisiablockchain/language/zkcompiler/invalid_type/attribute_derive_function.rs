//! assert fail_type_check "Attribute derive cannot be used here"

use pbc_zk::Sbi32;

#[derive(Something)]
pub fn main(val: Sbi32) {}
