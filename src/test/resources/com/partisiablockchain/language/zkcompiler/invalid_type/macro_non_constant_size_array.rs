//! assert fail_type_check "Array size expression must be constant"
//! assert fail_type_check "Non-literal sizes for array types are not supported"
use pbc_zk::*;

pub fn add_self(x: i32, y: i32) -> i32 {
    x + y
}

pbc_zk::test_eq!(add_self(1, 2), 3, [2; 2u32 + 2u32]);
