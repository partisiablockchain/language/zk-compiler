//! assert fail_type_check "Expected i32, but got (i32, i32)"
use pbc_zk::*;

fn max(a: i32, b: i32) -> i32 {
    if (a > b) {
        a
    } else {
        b
    }
}

fn try_max(x: i32) -> i32 {
    max(x, (x, x))
}
