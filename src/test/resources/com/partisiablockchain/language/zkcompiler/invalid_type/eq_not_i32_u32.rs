//! assert fail_type_check "Expected identical types, but got u32 and i32"
//! assert fail_type_check "Expected u32, but got i32"
use pbc_zk::*;

pub fn do_nothing_useful(x: u32, y: i32) -> bool {
    x == y
}
