//! assert fail_type_check "Constant initializer expression must be constant"
const MY_CONST: i32 = 32;

const MY_FOR_CONST: () = for MY_CONST in 1..5 {
    MY_CONST
};

fn identity(x: i32) -> i32 {
    x
}
