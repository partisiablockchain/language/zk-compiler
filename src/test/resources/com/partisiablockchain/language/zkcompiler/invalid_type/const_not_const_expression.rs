//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Variable MY_UNKNOWN_CONST not in scope"
const MY_CONST: i32 = if MY_UNKNOWN_CONST { 1 } else { 2 };
