//! use function_lib_max
//! assert fail_type_check "Could not find items 'fjper' in crate::invalid_type::function_lib_max"
//! assert fail_type_check "Unknown function fjper"
//!
//! This test uses an unknown function from an existing library file.
use crate::invalid_type::function_lib_max::fjper;
use pbc_zk::Sbi32;

pub fn main(x: Sbi32, y: Sbi32) -> Sbi32 {
    fjper(x, y)
}
