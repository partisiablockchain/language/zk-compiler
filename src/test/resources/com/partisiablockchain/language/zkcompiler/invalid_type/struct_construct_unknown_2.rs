//! assert fail_type_check "Unknown type MyStruct"
//! assert fail_type_check "Unknown type MyStruct"
//! assert fail_type_check "Expected i32, but got u32"
//! assert fail_type_check "Expected identical types, but got i32 and u32"

use pbc_zk::*;

pub fn test() -> MyStruct {
    MyStruct {
        field1: 3i32 + 3u32,
        field2: 3,
    }
}
