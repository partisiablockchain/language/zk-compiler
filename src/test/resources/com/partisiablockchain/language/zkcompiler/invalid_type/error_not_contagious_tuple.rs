//! assert fail_type_check "Variable unknown_variable not in scope"
//! assert fail_type_check "Variable unknown_variable not in scope"

fn sbi() -> (i32, i32) {
    let mut t = (5, unknown_variable);
    t = (unknown_variable, 5);
    t
}
