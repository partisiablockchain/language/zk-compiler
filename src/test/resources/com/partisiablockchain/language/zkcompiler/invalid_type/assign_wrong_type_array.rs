//! assert fail_type_check "Expected pbc_zk::Sbi32, but got [pbc_zk::Sbi32; 1]"
use pbc_zk::*;

fn wrong_type(x: Sbi32) -> Sbi32 {
    [x]
}
