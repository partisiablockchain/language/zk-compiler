//! assert fail_type_check "Expected std::ops::Range<i32>, but got std::ops::Range<u32>"
use pbc_zk::Sbi32;

pub fn range_expr() -> Sbi32 {
    let mut r = 1..100;
    r = 1u32..100u32;
    Sbi32::from(0)
}
