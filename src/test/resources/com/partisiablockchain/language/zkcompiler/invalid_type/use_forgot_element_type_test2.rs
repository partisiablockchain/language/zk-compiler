//! assert fail_type_check "Unknown type Sbi32"
//! assert fail_type_check "Unknown type Sbi32"
use pbc_zk::{load_sbi, SecretVarId};

pub fn my_sbi32_from(x: SecretVarId) -> Sbi32 {
    load_sbi::<Sbi32>(x)
}
