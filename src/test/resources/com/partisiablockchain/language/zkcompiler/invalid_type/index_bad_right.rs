//! assert fail_type_check "Expected u32, but got ()"
use pbc_zk::*;

pub fn do_nothing_useful(x: [Sbi32; 4]) -> Sbi32 {
    x[{}];
    x[0]
}
