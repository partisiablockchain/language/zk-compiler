//! assert fail_type_check "Struct 'MyRecursivePair' is recursive."
//! assert ignore

use pbc_zk::Sbi32;

struct MyRecursivePair {
    v1: Sbi32,
    v2: MyRecursivePair,
}

fn something(a: MyRecursivePair) -> MyRecursivePair {
    a
}
