//! assert fail_type_check "Expected identical types, but got pbc_zk::Sbu1 and u1"
//! assert fail_type_check "Expected pbc_zk::Sbu1, but got u1"
use pbc_zk::*;

pub fn xor(x: Sbi32) -> Sbi32 {
    if (x == Sbi32::from(1)) && (1 != 1) {
        Sbi32::from(1)
    } else {
        Sbi32::from(0)
    }
}
