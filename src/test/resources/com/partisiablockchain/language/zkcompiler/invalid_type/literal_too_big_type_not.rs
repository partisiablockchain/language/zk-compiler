//! assert fail_type_check "Value 1000000000000000000000000000000 doesn't fit in i64"

pub fn too_big() -> i64 {
    let a = !1000000000000000000000000000000i64;
    a
}
