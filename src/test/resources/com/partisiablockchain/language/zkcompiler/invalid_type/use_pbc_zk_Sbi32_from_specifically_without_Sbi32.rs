//! assert fail_type_check "Unknown type Sbi32"
use pbc_zk::Sbi32::from;

fn identity(x: i32) -> Sbi32 {
    from(x)
}
