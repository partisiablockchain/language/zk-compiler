//! assert fail_type_check "Unknown type MyCleverType<u32>"

pub fn identity(x: MyCleverType<u32>) -> u32 {
    x.inner
}
