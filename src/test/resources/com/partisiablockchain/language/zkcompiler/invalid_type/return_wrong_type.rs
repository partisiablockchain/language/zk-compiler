//! assert fail_type_check "Expected pbc_zk::Sbi32, but got i32"
use pbc_zk::*;

pub fn identity(x: i32) -> Sbi32 {
    if x == 0 {
        return x;
    }
    Sbi32::from(x)
}
