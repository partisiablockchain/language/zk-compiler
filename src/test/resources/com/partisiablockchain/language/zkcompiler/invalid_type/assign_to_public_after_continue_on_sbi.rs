//! assert fail_type_check "Continue requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"

use pbc_zk::Sbi32;

pub fn compute_55(max: i32) -> (Sbi32, i32) {
    let mut sum: Sbi32 = Sbi32::from(0);
    let mut summed_variables = 0;
    for idx in 0..max + 1 {
        if sum >= Sbi32::from(10) {
            continue;
        }
        sum = sum + Sbi32::from(idx);
        summed_variables = summed_variables + 1;
    }
    (sum, summed_variables)
}
