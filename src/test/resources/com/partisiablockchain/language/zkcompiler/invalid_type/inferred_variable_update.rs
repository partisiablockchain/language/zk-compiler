//! assert fail_type_check "Expected u8, but got i32"
// assert fail_type_check "Expected identical types, but got u8 and i32"

pub fn infer_var_update() -> i32 {
    let a = 4;

    let b: u8 = a;
    let c: i32 = 4;

    a + c
}
