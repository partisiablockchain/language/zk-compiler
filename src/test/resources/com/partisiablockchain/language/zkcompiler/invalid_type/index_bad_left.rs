//! assert fail_type_check "pbc_zk::Sbi32 is not an array type"
use pbc_zk::Sbi32;

pub fn do_nothing_useful(x: Sbi32) -> Sbi32 {
    x[1];
    x
}
