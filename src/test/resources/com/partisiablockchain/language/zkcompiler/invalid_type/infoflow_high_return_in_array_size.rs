//! assert fail_type_check "Array size expression must be constant"
//! assert fail_type_check "Non-literal sizes for array types are not supported"
//! assert fail_type_check "Expected [i32; 5], but got [i32; 0]"
//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn infoflow_failure(secret: Sbi32) {
    if secret == Sbi32::from(1) {
        let arr: [i32; 5] = [5; {
            if true {
                return ();
            };
            5
        }];
    }
}
