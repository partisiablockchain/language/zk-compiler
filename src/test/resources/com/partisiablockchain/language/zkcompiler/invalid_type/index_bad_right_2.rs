//! assert fail_type_check "Expected u32, but got (i32, i32)"
use pbc_zk::*;

pub fn do_nothing_useful(x: [Sbi32; 4]) -> Sbi32 {
    x[(1, 2)]
}
