//! assert fail_type_check "Cannot assign to immutable variable"

pub fn main() {}

fn identity(x: i32) -> i32 {
    let y: i32 = x;
    y = x;
    y
}
