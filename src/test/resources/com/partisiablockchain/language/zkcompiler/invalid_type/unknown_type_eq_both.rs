//! assert fail_type_check "Unknown type MyType"
//! assert fail_type_check "Unknown type MyOtherType"

fn something() -> i32 {
    let x: MyType = 1;
    let y: MyOtherType = 2;
    if x == y {
        1
    } else {
        2
    }
}
