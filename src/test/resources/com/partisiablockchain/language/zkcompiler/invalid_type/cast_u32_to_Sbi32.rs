//! assert fail_type_check "u32 is not castable to pbc_zk::Sbi32"
use pbc_zk::*;

fn stuff(x: u32) {
    x as Sbi32;
}
