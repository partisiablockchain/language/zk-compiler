//! assert fail_type_check "Value 4294967296 doesn't fit in u32"

use pbc_zk::*;

#[zk_compute(shortname = 0x100000000)]
pub fn big_shortname() -> Sbi32 {
    Sbi32::from(2)
}
