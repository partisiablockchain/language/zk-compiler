//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn identity(x: Sbi32) -> Sbi32 {
    if x == Sbi32::from(0) {
        return Sbi32::from(9);
    }
    x
}
