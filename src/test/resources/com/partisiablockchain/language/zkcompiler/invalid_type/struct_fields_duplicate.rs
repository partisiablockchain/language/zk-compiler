//! assert fail_type_check "Struct 'MyPair' has duplicated field names 'v1'"
use pbc_zk::Sbi32;

struct MyPair {
    v1: Sbi32,
    v1: Sbi32,
}
