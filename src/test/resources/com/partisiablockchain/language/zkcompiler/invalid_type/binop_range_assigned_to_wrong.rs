//! assert fail_type_check "Expected i32, but got std::ops::Range<i32>"
use pbc_zk::Sbi32;

pub fn range_expr() -> Sbi32 {
    let r: i32 = 1..100;
    Sbi32::from(0)
}
