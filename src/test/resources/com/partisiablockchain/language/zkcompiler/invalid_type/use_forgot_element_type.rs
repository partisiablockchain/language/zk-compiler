//! assert ignore
use pbc_zk::Sbi32;

pub fn sbi32_from(x: i32) -> Sbi32 {
    Sbi32::from(x)
}
