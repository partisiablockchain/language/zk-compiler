//! assert fail_type_check "Expected struct type, but was pbc_zk::Sbi32"

use pbc_zk::Sbi32;

pub fn test() -> Sbi32 {
    Sbi32::from(3).some_field
}
