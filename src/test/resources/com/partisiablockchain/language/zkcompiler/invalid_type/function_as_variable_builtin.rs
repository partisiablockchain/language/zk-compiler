//! assert fail_type_check "Sbi32::from is a function"
// Use built-in function as a variable
use pbc_zk::*;

fn builtin_as_var(x: Sbi32) -> Sbi32 {
    x + Sbi32::from
}
