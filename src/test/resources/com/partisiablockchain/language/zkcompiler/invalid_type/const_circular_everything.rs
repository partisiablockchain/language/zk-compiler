//! assert fail_type_check "Return must not occur in return-less context"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"

struct MyStruct {
    a: i32,
    b: (i32, i32),
}

const UNIT: () = ();

const CONST_1: () = for i in 1..2 {
    CONST_2
};
const CONST_2: () = for CONST_1 in CONST_3..10 {
    CONST_1
};
const CONST_3: i32 = (
    for i in 11..CONST_4 {
        i
    },
    3,
)
    .1;

const CONST_4: i32 = -CONST_5.0;
const CONST_5: (i32, i32) = CONST_6.b;
const CONST_6: MyStruct = MyStruct {
    a: CONST_7.0,
    b: CONST_7,
};
const CONST_7: (i32, i32) = (99i32, CONST_8);
const CONST_8: i32 = identity(CONST_9);
const CONST_9: i32 = CONST_10 as i32;
const CONST_10: usize = {
    let mut CONST_2: i32 = 10101i32;
    CONST_2 = CONST_11;
    UNIT;
    CONST_2 as usize
};

const CONST_11: i32 = CONST_12 + 4;
const CONST_12: i32 = 9 + CONST_13;
const CONST_13: i32 = if CONST_14 { 1 } else { 0 };
const CONST_14: bool = if TRUE { CONST_15 } else { FALSE };
const CONST_15: bool = if TRUE { FALSE } else { CONST_16 };
const CONST_16: bool = {
    let x: i32 = CONST_17;
    UNIT;
    x == 52
};

const CONST_17: i32 = {
    return 9;
    UNIT;
    CONST_END
};

const CONST_END: i32 = (CONST_1, 3).1;

const TRUE: bool = 0 == 0;
const FALSE: bool = !TRUE;

fn identity(x: i32) -> i32 {
    x
}
