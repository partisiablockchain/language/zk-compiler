//! assert fail_type_check "Expected std::vec::Vec<pbc_zk::Sbi32>, but got std::vec::Vec<u32>"
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> u32 {
    let mut vec = Vec::new::<u32>();
    Vec::push::<Sbi32>(vec, x);
    Vec::pop::<u32>(vec)
}
