//! assert fail_type_check "{{module}}::MyStruct::into_iter is not in scope"
use pbc_zk::{load_sbi, Sbi32};

struct MyStruct {}

pub fn sum_everything() {
    for variable_id in (MyStruct {}) {}
}
