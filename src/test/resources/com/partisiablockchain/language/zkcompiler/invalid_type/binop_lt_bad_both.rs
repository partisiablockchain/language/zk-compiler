//! assert fail_type_check "Expected integer type, but got ()"
//! assert fail_type_check "Expected integer type, but got ()"
use pbc_zk::*;

fn do_nothing(x: Sbi32) -> Sbi32 {
    ({} < {});
    x
}
