//! assert fail_type_check "Non-integer type crate::invalid_type::literal_wrong_type::i96 not allowed in integer literal"

struct i96 {
    v1: i32,
    v2: i32,
}

#[rustfmt::skip]
pub fn literal() {
    let a = 42i96;
}
