//! assert fail_type_check "Attribute inline cannot be used here"

use pbc_zk::{save_sbi, Sbi32, Sbu1};

#[inline]
struct SomeStruct {}

pub fn main(val: Sbi32) {}
