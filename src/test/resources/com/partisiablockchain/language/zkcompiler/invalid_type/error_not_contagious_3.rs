//! assert fail_type_check "Variable x not in scope"
pub fn identity() -> i32 {
    1 >> x
}
