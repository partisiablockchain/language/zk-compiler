//! assert fail_type_check "Expected identical types, but got () and pbc_zk::Sbi32"
//! assert fail_type_check "Expected (), but got pbc_zk::Sbi32"
use pbc_zk::*;

fn what(x: Sbi32) -> Sbi32 {
    ({} == x);
    x
}
