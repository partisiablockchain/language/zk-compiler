//! assert fail_type_check "Return requires PUBLIC context level. Context level was SECRET, but expected PUBLIC"
use pbc_zk::*;

pub fn infoflow_failure(secret: Sbi32) {
    let tuple = (secret, secret, secret);
    if secret == Sbi32::from(1) {
        ({
            (
                {
                    return ();
                },
                (),
                (),
            )
        })
        .1
    }
}
