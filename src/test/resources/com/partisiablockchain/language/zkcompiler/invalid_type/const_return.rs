//! assert fail_type_check "Return must not occur in return-less context"
//! assert fail_type_check "Constant initializer expression must be constant"
const MY_CONST: i32 = {
    return 1;
    1
};
