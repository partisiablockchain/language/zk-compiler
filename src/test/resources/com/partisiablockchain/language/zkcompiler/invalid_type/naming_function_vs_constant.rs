//! assert fail_type_check "Function 'overlap' conflicts with constant {{module}}::overlap"
const overlap: i32 = 1;

fn overlap() {}
