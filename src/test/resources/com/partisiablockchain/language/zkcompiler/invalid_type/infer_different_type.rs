//! assert fail_type_check "Expected identical types, but got i32 and u8"
//! assert fail_type_check "Expected identical types, but got i32 and u16"
//! assert fail_type_check "Expected i32, but got u8"
//! assert fail_type_check "Expected i32, but got u16"

pub fn infer() {
    let a = 4;

    let b = a + 8u8;
    let c = a + 8u16;
}
