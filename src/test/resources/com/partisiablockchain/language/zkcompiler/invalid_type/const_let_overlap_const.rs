//! assert fail_type_check "Constant initializer expression must be constant"
const MY_CONST: i32 = 32;

const MY_CONST_2: i32 = {
    let MY_CONST: i32 = 321;
    MY_CONST
};

fn identity(x: i32) -> i32 {
    x
}
