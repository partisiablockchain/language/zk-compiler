//! assert fail_type_check "Unsupported expression: Bitshift with anything other than public integer as right-hand side"
//! assert fail_type_check "Unsupported expression: Bitshift with anything other than public integer as right-hand side"
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    (x >> Sbi32::from(32)) << Sbi32::from(32)
}
