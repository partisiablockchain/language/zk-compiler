//! assert eval(2, 3) => 1
//! assert eval(65, 65) => 1
//! assert eval(0xFFFFFFFF, 1) => 0
//! assert eval(-1, 1) => 0
//! assert eval(-1, -1) => 1
//! assert eval(-2, -1) => 1
//! assert eval(0xFFFFFFFF, 0xFFFFFFFF) => 1
//! assert eval(0xFFFFFFFD, 0xFFFFFFFF) => 1
//! assert eval(0xFFFFFFFF, 0x7FFFFFFF) => 0

use pbc_zk::*;

fn unsigned_lt(x: u32, y: u32) -> bool {
    x <= y
}
