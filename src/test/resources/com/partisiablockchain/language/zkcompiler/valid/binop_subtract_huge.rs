//! assert eval(0) => 2147483648
//! assert eval(9) => 2147483657
//! assert eval(18) => 2147483666
//! assert eval(100) => 2147483748
//! assert eval(2147483648) => 0
use pbc_zk::*;

pub fn subtract(x: Sbi32) -> Sbi32 {
    let huge = Sbi32::from(1 << 31);
    x - huge
}

// Big integers error
// test_eq!(subtract(Sbi32::from(0)), 2147483648);
// test_eq!(subtract(Sbi32::from(9)), 2147483657);
// test_eq!(subtract(Sbi32::from(18)), 2147483666);
// test_eq!(subtract(Sbi32::from(100)), 2147483748);
// test_eq!(subtract(Sbi32::from(2147483648)), 0);
