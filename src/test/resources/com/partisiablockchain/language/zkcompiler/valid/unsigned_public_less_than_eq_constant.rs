//! assert eval(2, 3) => 1
//! assert eval(15, 15) => 1
//! assert eval(3, 2) => 0

use pbc_zk::*;

fn unsigned_lt(x: u32, y: u32) -> bool {
    if x <= y {
        ((-1i8) as u8) <= 255u8
    } else {
        ((-1i8) as u8) <= 0u8
    }
}
