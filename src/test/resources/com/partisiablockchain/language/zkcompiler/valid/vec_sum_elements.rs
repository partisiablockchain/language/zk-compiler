//! assert eval(x) => x * 3 + 7
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> Sbi32 {
    let mut vec: Vec<Sbi32> = Vec::new::<Sbi32>();
    Vec::push::<Sbi32>(vec, x);
    Vec::push::<Sbi32>(vec, Sbi32::from(2));
    Vec::push::<Sbi32>(vec, x);
    Vec::push::<Sbi32>(vec, x);
    Vec::push::<Sbi32>(vec, Sbi32::from(5));

    // Sum
    let mut sum = Sbi32::from(0);
    for i in 0usize..Vec::len::<Sbi32>(vec) {
        sum = sum + Vec::pop::<Sbi32>(vec);
    }
    sum
}
