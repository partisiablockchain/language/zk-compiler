//! assert eval(x) => x
use pbc_zk::*;

pub fn double_identity(x: Sbi32) -> (Sbi32,) {
    (x,)
}
