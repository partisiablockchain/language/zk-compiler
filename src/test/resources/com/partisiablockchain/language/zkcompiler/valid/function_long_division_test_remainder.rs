//! Test of a classical long division implementation.
//!
//! assert eval(x, 0) => x
//! assert eval(0, x) => 0
//! assert eval(x, 1) => 0
//!
//! assert eval(0, 3) => 0
//! assert eval(1, 3) => 1
//! assert eval(2, 3) => 2
//! assert eval(3, 3) => 0
//! assert eval(4, 3) => 1
//! assert eval(5, 3) => 2
//! assert eval(6, 3) => 0
//! assert eval(7, 3) => 1
//! assert eval(8, 3) => 2
//! assert eval(9, 3) => 0
//! assert eval(3212, 3) => 2
//! assert eval(3213, 3) => 0
//! assert eval(3214, 3) => 1
//! assert eval(x, x) => 0
//!
//! assert eval(2147483648, 3) => 2
//! assert eval(2147888888, 3) => 2
//! assert eval(4294967295, 3) => 0
//! assert eval(4294967295, 255) => 0
//!
//! use function_long_division

use crate::valid::function_long_division::long_division_with_remainder;
use pbc_zk::Sbi32;

pub fn remainder(numerator: Sbi32, denominator: Sbi32) -> Sbi32 {
    let result = long_division_with_remainder(numerator, denominator);
    result.remainder
}
