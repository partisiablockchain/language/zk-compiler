//! assert eval(1) => 11
//! assert eval(0) => 121

pub fn infer_if(test: bool) -> i16 {
    let a = if test { 4 } else { 42i16 };

    let b = if !test { 79i16 } else { 7 };

    a + b
}
