//! assert eval(x) => x
use pbc_zk::*;

pub fn return_expr(x: Sbi32) -> Sbi32 {
    return x;
    Sbi32::from(0)
}
