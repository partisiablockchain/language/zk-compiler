//! assert eval(0, 2, 1, 3, 0, 2, 3, 0, 1, 5, 0, 6, 0, 1, 2, 0, 3, 0, 5, 6, 0, 3, 2, 6, 0, 3, 0, 7, 0, 3, 5, 2) => 0x01_03_03_00_06_05_03_0b

use pbc_zk::*;

pub fn sum_everything() -> [Sbi8; 8] {
    // Initialize state
    let mut counts: [Sbi8; 8] = [Sbi8::from(0); 8];

    // Count each variable
    for variable_id in secret_variable_ids() {
        let var = load_sbi::<Sbi8>(variable_id);
        for value in 0usize..8usize {
            let mut c = counts[value];
            if var == Sbi8::from(value as i8) {
                c = c + Sbi8::from(1)
            }
            counts[value] = c;
        }
    }
    counts
}
