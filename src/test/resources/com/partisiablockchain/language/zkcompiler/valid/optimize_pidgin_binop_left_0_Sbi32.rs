//! assert eval(x) => 1

use pbc_zk::{Sbi32, Sbu1};

pub fn tautologic(x: Sbi32) -> Sbu1 {
    let zero: Sbi32 = Sbi32::from(0);

    // Produce zero
    let and: Sbi32 = zero & x;
    let mul: Sbi32 = zero * x;

    // Produce right
    let or: Sbi32 = zero | x;
    let xor: Sbi32 = zero ^ x;
    let add: Sbi32 = zero + x;

    (and == mul && mul == zero) && (or == xor && xor == add && add == x)
}
