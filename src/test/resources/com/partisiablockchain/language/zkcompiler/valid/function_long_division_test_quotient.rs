//! Test of a classical long division implementation.
//!
//! assert eval(x, 0) => 0
//! assert eval(0, x) => 0
//! assert eval(x, 1) => x
//!
//! assert eval(0, 3) => 0
//! assert eval(1, 3) => 0
//! assert eval(2, 3) => 0
//! assert eval(3, 3) => 1
//! assert eval(4, 3) => 1
//! assert eval(5, 3) => 1
//! assert eval(6, 3) => 2
//! assert eval(7, 3) => 2
//! assert eval(8, 3) => 2
//! assert eval(9, 3) => 3
//! assert eval(3213, 3) => 1071
//!
//! assert eval(4123, 4123) => 1
//! assert eval(4123, 4123) => 1
//! assert eval(231123, 231123) => 1
//! assert eval(6124312, 6124312) => 1
//! assert eval(51234123, 51234123) => 1
//! assert eval(2147483647, 2147483647) => 1
//! assert eval(2147483648, 2147483648) => 1
//!
//! assert eval(2147483648, 3) => 715827882
//! assert eval(2147888888, 3) => 715962962
//! assert eval(4294967295, 3) => 1431655765
//! assert eval(4294967295, 255) => 16843009
//! assert eval(4294967295, 2147483647) => 2
//! assert eval(4294967295, 2147483648) => 1
//! assert eval(4294967295, 2147483649) => 1
//!
//! use function_long_division

use crate::valid::function_long_division::long_division_with_remainder;
use pbc_zk::Sbi32;

pub fn long_division(numerator: Sbi32, denominator: Sbi32) -> Sbi32 {
    let result = long_division_with_remainder(numerator, denominator);
    result.quotient
}
