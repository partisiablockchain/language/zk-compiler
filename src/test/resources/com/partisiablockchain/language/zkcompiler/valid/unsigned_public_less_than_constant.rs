//! assert eval(2, 3) => 1
//! assert eval(15, 15) => 0
//! assert eval(3, 2) => 0

use pbc_zk::*;

fn unsigned_lt(x: u32, y: u32) -> bool {
    if x < y {
        1u8 < ((-1i8) as u8)
    } else {
        1u8 < 1u8
    }
}
