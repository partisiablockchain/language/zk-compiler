//! assert eval(0) => 2
//! assert eval(1) => 2
//! assert eval(2) => 2
//! assert eval(3) => 3
//! assert eval(4) => 2
//! assert eval(1337) => 2
use pbc_zk::*;

pub fn mut_if(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = Sbi32::from(1);
    y = {
        let mut y: Sbi32 = Sbi32::from(2);
        if x == Sbi32::from(3) {
            y = Sbi32::from(3)
        }
        y
    };
    y
}
