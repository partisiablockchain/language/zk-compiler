//! assert eval(5, 8, 0) => 13
//! assert eval(5, 8, 1) => 13
//! assert eval(5, 8, 2) => 13
//! assert eval(5, 8, 99) => 13

use pbc_zk::*;

pub fn swap(mut x: Sbi32, mut y: Sbi32, swaps: i32) -> Sbi32 {
    for idx in 0..swaps {
        let tmp: Sbi32 = x;
        x = y;
        y = tmp;
    }
    x + y
}
