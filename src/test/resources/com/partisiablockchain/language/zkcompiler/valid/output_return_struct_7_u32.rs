//! assert eval() => 0xFFFFFFFF_FFFFFFFF

struct MyPair {
    v1: u32,
    v2: u32,
}

pub fn big_constant() -> MyPair {
    MyPair {
        v1: (-1) as u32,
        v2: (-1) as u32,
    }
}
