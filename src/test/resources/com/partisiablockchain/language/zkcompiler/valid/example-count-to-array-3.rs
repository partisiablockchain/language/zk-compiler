//! assert eval(0, 2, 1, 3, 0, 2, 3, 0, 1, 5, 0, 6, 0, 1, 2, 0, 3, 0, 5, 6, 0, 3, 2, 6, 0, 3, 0, 7, 0, 3, 5, 2) => 0x00000001_00000003_00000003_00000000_00000006_00000005_00000003_0000000b
//! assert eval tests

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything() -> [Sbi32; 8] {
    // Initialize state
    let mut counts: [Sbi32; 8] = [Sbi32::from(0); 8];

    // Count each variable
    for variable_id in secret_variable_ids() {
        let var = load_sbi::<Sbi32>(variable_id);
        for value in 0..8 {
            let idx = value as usize;
            counts[idx] = counts[idx]
                + if var == Sbi32::from(value) {
                    Sbi32::from(1)
                } else {
                    Sbi32::from(0)
                }
        }
    }

    counts
}

test_eq!(
    sum_everything()
        == [
            Sbi32::from(0x0b),
            Sbi32::from(0x03),
            Sbi32::from(0x05),
            Sbi32::from(0x06),
            Sbi32::from(0x00),
            Sbi32::from(0x03),
            Sbi32::from(0x03),
            Sbi32::from(0x01)
        ],
    true,
    [
        0i32, 2i32, 1i32, 3i32, 0i32, 2i32, 3i32, 0i32, 1i32, 5i32, 0i32, 6i32, 0i32, 1i32, 2i32,
        0i32, 3i32, 0i32, 5i32, 6i32, 0i32, 3i32, 2i32, 6i32, 0i32, 3i32, 0i32, 7i32, 0i32, 3i32,
        5i32, 2i32
    ]
);
