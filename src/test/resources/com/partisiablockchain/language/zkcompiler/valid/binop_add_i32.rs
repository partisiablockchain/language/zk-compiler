//! assert eval(1, 5) => 6
//! assert eval(5, 1) => 6
//! assert eval(0, 0) => 0
//! assert eval tests
use pbc_zk::*;

pub fn add_self(x: i32, y: i32) -> i32 {
    x + y
}

pbc_zk::test_eq!(add_self(1, 5), 6);
pbc_zk::test_eq!(add_self(5, 1), 6);
pbc_zk::test_eq!(add_self(0, 0), 0);
