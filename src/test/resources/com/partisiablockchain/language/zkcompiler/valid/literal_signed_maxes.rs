//! assert eval() => ()

pub fn signed_maxes() {
    let _i8 = 0x7Fi8;
    let _i16 = 0x7FFFi16;
    let _i32 = 0x7FFFFFFFi32;
    let _i64 = 0x7FFFFFFFFFFFFFFFi64;
    let _i128 = 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFi128;
}
