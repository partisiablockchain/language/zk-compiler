//! assert eval(x) => 0
use pbc_zk::Sbu1;

fn what(x: Sbu1) -> Sbu1 {
    if x {
        !x
    } else {
        x
    }
}
