//! assert eval(x) => 31
use pbc_zk::Sbi32;

pub fn circuit_1(x: Sbi32) -> i32 {
    let a = if true { 1 } else { 3 };
    a + (if false { 10 } else { 30 })
}
