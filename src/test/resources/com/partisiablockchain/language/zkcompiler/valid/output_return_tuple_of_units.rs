//! assert eval(0) => (0, 0, 0, 0)
//! assert eval(99) => (0, 0, 0, 0)
//! assert eval(200) => (0, 0, 0, 0)

use pbc_zk::Sbi8;

pub fn identity(a: Sbi8) -> ((), (), (), ()) {
    ((), (), (), ())
}
