//! assert eval(0, 1) => 0
//! assert eval(0, 0) => 1_000_000
//! assert eval(1, 1) => 1
//! assert eval(1, 2) => 1
//! assert eval(2, 1) => 1_000_001
//! assert eval(2, 2) => 2
//! assert eval(2, 3) => 2
//! assert eval(3, 1) => 1_000_001
//! assert eval(3, 2) => 1_000_002
//! assert eval(3, 3) => 3

pub fn test(n: i32, rounds: i32) -> i32 {
    identity(n, rounds)
}

fn identity(n: i32, rounds: i32) -> i32 {
    if rounds == 0 {
        return 1000000;
    }
    if n < 2 {
        n
    } else {
        1 + identity(n - 1, rounds - 1)
    }
}
