//! assert eval(x) => x
//! assert eval(16777217) => 16777217
//! assert eval(0x12_00_00_34) => 0x12_00_00_34
use pbc_zk::*;

pub fn identity(x: Sbi32) -> Sbi32 {
    x
}
