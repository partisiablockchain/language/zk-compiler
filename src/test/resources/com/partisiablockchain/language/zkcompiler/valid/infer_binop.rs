//! assert eval() => 22

pub fn infer_binop() -> i16 {
    let a = 5i16 + 6;
    let b = 5 + 6i16;

    a + b
}
