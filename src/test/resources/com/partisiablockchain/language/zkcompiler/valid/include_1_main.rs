//! use include_1_data
//! assert eval(0x00_00) => 0x1
//! assert eval(0x03_03) => 0x1
//! assert eval(0x03_09) => 0x0
//! assert eval(0x09_09) => 0x1
//! assert eval(0x09_03) => 0x0

use crate::valid::include_1_data::MyStruct;
use pbc_zk::Sbu1;

pub fn main(s: MyStruct) -> Sbu1 {
    s.a == s.b
}
