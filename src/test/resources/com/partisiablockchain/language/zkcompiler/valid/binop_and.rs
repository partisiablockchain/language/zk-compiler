//! assert eval(0, 1) => 0
//! assert eval(1, 0) => 0
//! assert eval(0, 312) => 0
//! assert eval(15, 0) => 0
//! assert eval(15, 7) => 7
//! assert eval(10, 5) => 0
//! assert eval tests
use pbc_zk::*;

pub fn and(x: Sbi32, y: Sbi32) -> Sbi32 {
    x & y
}

test_eq!(and(Sbi32::from(0), Sbi32::from(1)), 0);
test_eq!(and(Sbi32::from(1), Sbi32::from(0)), 0);
test_eq!(and(Sbi32::from(0), Sbi32::from(312)), 0);
test_eq!(and(Sbi32::from(15), Sbi32::from(0)), 0);
test_eq!(and(Sbi32::from(15), Sbi32::from(7)), 7);
test_eq!(and(Sbi32::from(10), Sbi32::from(5)), 0);
