//! assert eval(x) => 1

use pbc_zk::{Sbi32, Sbu1};

pub fn tautologic(x: Sbi32) -> Sbu1 {
    // Produce zero
    let and: Sbi32 = x & Sbi32::from(0);
    let mul: Sbi32 = x * Sbi32::from(0);

    // Produce left
    let shl: Sbi32 = x << 0;
    let shr: Sbi32 = x >> 0;
    let or: Sbi32 = x | Sbi32::from(0);
    let xor: Sbi32 = x ^ Sbi32::from(0);
    let add: Sbi32 = x + Sbi32::from(0);

    (and == mul && mul == Sbi32::from(0))
        && (shl == shl && shl == shr && shr == or && or == xor && xor == xor && add == x)
}
