//! assert eval() => 340282366920938463463374607431768211455

struct MyStruct {
    v1: u32,
    v2: u32,
    v3: u32,
    v4: u32,
}

pub fn big() -> MyStruct {
    let uc = MyStruct {
        v1: 0xFFFFFFFFu32,
        v2: 0xFFFFFFFFu32,
        v3: 0xFFFFFFFFu32,
        v4: 0xFFFFFFFFu32,
    };
    uc
}
