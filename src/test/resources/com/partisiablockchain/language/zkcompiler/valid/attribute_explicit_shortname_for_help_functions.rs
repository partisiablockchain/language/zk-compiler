//! assert eval(x) => x
use pbc_zk::{save_sbi, Sbi32, Sbu1};

#[zk_compute(shortname = 0x12)]
pub fn main1(input: Sbi32) {
    save_1(input);
}

#[zk_compute(shortname = 0x30)]
pub fn main2(input: Sbi32) {
    save_1(input * Sbi32::from(2));
}

fn save_1(input: Sbi32) -> Sbi32 {
    save_2(input)
}

fn save_2(input: Sbi32) -> Sbi32 {
    save_rec(input, 10)
}

fn save_rec(val: Sbi32, reps: i32) -> Sbi32 {
    if reps == 0 {
        save_sbi::<Sbi32>(val);
        val
    } else {
        save_rec(val, reps + -1)
    }
}

// pbc_zk::test!(main1(Sbi32::from(42)) == Sbi32::from(42));
// pbc_zk::test!(main2(Sbi32::from(42)) == Sbi32::from(84));
