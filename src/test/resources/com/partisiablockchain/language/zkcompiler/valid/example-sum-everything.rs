//! assert eval(99, 33, 55) => 187
//! assert eval(55, 99, 33) => 187
//! assert eval(33, 55, 99) => 187
//! assert eval(33, 99, 55) => 187
//! assert eval(55, 99, 55) => 209
//! assert eval(55, 55, 11) => 121
//! assert eval(11, 44, 44) => 99
//! assert eval(44, 11, 11) => 66
//! assert eval(1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1) => 81
//! assert eval(312, 4123, 532, 34, 324, 34224, 432) => 39981
//! assert eval(3020, 23130, 320) => 26470

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::{load_sbi, secret_variable_ids, Sbi32};

pub fn sum_everything() -> Sbi32 {
    // Initialize state
    let mut sum: Sbi32 = Sbi32::from(0);

    // Sum each variable
    for variable_id in secret_variable_ids() {
        sum = sum + load_sbi::<Sbi32>(variable_id);
    }

    sum
}
