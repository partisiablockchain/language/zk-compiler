//! assert eval(0) => 0
//! assert eval(1) => 1
//! assert eval(255) => 255
//! assert eval(256) => 0

pub fn cast(v1: u32) -> u32 {
    (v1 as u8) as u32
}
