//! assert eval(0, 1) => 0
//! assert eval(1, 0) => 1
//! assert eval(1, 1) => 0
//! assert eval(4, 4) => 0
//! assert eval(15, 0) => 15
//! assert eval(15, 3) => 1
//! assert eval(10, 3) => 1
//! assert eval tests
use pbc_zk::*;

pub fn shr(x: Sbi32, y: usize) -> Sbi32 {
    x >> y
}

test_eq!(shr(Sbi32::from(0), 1), 0);
test_eq!(shr(Sbi32::from(1), 0), 1);
test_eq!(shr(Sbi32::from(1), 1), 0);
test_eq!(shr(Sbi32::from(4), 4), 0);
test_eq!(shr(Sbi32::from(15), 0), 15);
test_eq!(shr(Sbi32::from(15), 3), 1);
test_eq!(shr(Sbi32::from(10), 3), 1);
