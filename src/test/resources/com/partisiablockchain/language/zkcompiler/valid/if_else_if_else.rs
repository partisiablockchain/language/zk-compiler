//! assert eval(0) => 1
//! assert eval(1) => 1
//! assert eval(2) => 1
//! assert eval(3) => 2
//! assert eval(4) => 2
//! assert eval(5) => 3
//! assert eval(6) => 3
//! assert eval(7) => 3
use pbc_zk::*;

pub fn f(x: Sbi32) -> Sbi32 {
    if x < Sbi32::from(3) {
        Sbi32::from(1)
    } else if x < Sbi32::from(5) {
        Sbi32::from(2)
    } else {
        Sbi32::from(3)
    }
}
