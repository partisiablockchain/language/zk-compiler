//! assert eval(x) => 1

pub fn tautologic(x: i32) -> bool {
    // Produce x
    let and: i32 = x & x;
    let or: i32 = x | x;

    // Produce zero
    let xor: i32 = x ^ x;
    let lt: bool = x < x;
    let gt: bool = x > x;

    // Produce one
    let eq: bool = x == x;
    let lte: bool = x <= x;
    let gte: bool = x >= x;

    // Produce other
    let add: i32 = x + x;

    (and == or && or == x) && (xor == 0) && (!lt && !gt) && (eq && lte && gte) && (add == x << 1)
}
