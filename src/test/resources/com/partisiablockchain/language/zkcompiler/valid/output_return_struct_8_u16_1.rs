//! assert eval() => 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF

struct MyPair {
    v1: u16,
    v2: u16,
    v3: u16,
    v4: u16,
    v5: u16,
    v6: u16,
    v7: u16,
    v8: u16,
}

pub fn big_constant() -> MyPair {
    MyPair {
        v1: (-1) as u16,
        v2: (-1) as u16,
        v3: (-1) as u16,
        v4: (-1) as u16,
        v5: (-1) as u16,
        v6: (-1) as u16,
        v7: (-1) as u16,
        v8: (-1) as u16,
    }
}
