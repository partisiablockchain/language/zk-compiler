//! assert eval(0x01) => 0x01_01
//! assert eval(0x09) => 0x09_09

use pbc_zk::Sbi8;

struct MyPair {
    v1: Sbi8,
    v2: Sbi8,
}

pub fn identity(a: Sbi8) -> MyPair {
    MyPair { v1: a, v2: a }
}
