//! assert eval(0, 1) => 1
//! assert eval(1, 0) => 1
//! assert eval(0, 312) => 312
//! assert eval(15, 0) => 15
//! assert eval(15, 7) => 15
//! assert eval(10, 5) => 15
//! assert eval tests
use pbc_zk::*;

pub fn or(x: Sbi32, y: Sbi32) -> Sbi32 {
    x | y
}

test_eq!(or(Sbi32::from(0), Sbi32::from(1)), 1);
test_eq!(or(Sbi32::from(1), Sbi32::from(0)), 1);
test_eq!(or(Sbi32::from(0), Sbi32::from(312)), 312);
test_eq!(or(Sbi32::from(15), Sbi32::from(0)), 15);
test_eq!(or(Sbi32::from(15), Sbi32::from(7)), 15);
test_eq!(or(Sbi32::from(10), Sbi32::from(5)), 15);
