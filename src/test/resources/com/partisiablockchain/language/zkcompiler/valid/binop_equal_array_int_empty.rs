//! assert eval(0, 0) => 1
//! assert eval tests
use pbc_zk::*;

pub fn equal(v1: [i32; 0], v2: [i32; 0]) -> bool {
    v1 == v2
}

test_eq!(equal([0; 0], [0; 0]), true);
