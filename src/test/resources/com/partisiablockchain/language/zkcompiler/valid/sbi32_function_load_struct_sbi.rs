//! assert eval(0x00000001_00000009) => 9
//! assert eval(0x00000009_00000001) => 1

use pbc_zk::{load_sbi, Sbi32, SecretVarId};

struct MyPair {
    v1: Sbi32,
    v2: Sbi32,
}

pub fn identity() -> Sbi32 {
    let id = SecretVarId::new(1);
    let pair: MyPair = load_sbi::<MyPair>(id);
    pair.v1
}
