//! assert eval(0, 0) => (0, 0)
//! assert eval(1, 0) => (0, 0)
//! assert eval(0, 1) => (1, 1)
//! assert eval(1, 1) => (0, 0)
//! assert eval(100, 100) => (0, 0)
//! assert eval(100, 200) => (0, 1)
//! assert eval(200, 100) => (1, 0)
//! assert eval(127, 127) => (0, 0)
//! assert eval(127, 128) => (0, 1)
//! assert eval(128, 128) => (0, 0)
//! assert eval(128, 129) => (1, 1)
//! assert eval(0, 255) => (0, 1)
//! assert eval(200, 255) => (1, 1)
//! assert eval(255, 255) => (0, 0)
use pbc_zk::*;

fn lt(x: u8, y: u8) -> (bool, bool) {
    let signed: bool = (x as i8) < (y as i8);
    let unsigned: bool = x < y;
    (signed, unsigned)
}
