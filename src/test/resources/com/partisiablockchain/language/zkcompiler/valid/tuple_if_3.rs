//! assert eval(1, 1, 1) => (1, 1, 1)
//! assert eval(1, 9, 1) => (1, 1, 9)
//! assert eval(1, 3, 6) => (1, 3, 6)
//! assert eval(1, 6, 3) => (1, 3, 6)
//! assert eval(6, 3, 1) => (1, 3, 6)
//! assert eval(3, 6, 1) => (1, 3, 6)
use pbc_zk::*;

pub fn least_first(v1: Sbi32, v2: Sbi32, v3: Sbi32) -> (Sbi32, Sbi32, Sbi32) {
    if v1 <= v2 && v2 <= v3 {
        (v1, v2, v3)
    } else if v1 <= v3 && v3 <= v2 {
        (v1, v3, v2)
    } else if v2 <= v1 && v1 <= v3 {
        (v2, v1, v3)
    } else if v2 <= v3 && v3 <= v1 {
        (v2, v3, v1)
    } else if v3 <= v2 && v2 <= v1 {
        (v3, v2, v1)
    } else {
        (v3, v1, v2)
    }
}
