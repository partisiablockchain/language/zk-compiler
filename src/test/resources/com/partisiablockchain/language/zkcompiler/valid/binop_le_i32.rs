//! assert eval(1) => 1
//! assert eval(0) => 1
//! assert eval(0xFFFFFFFF) => 0
use pbc_zk::*;

pub fn le(x: i32) -> bool {
    0 <= x
}
