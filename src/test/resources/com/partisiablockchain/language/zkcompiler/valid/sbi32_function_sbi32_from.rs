//! assert eval(x) => 521
use pbc_zk::*;

pub fn sbi() -> Sbi32 {
    Sbi32::from(521)
}
