//! assert eval(1) => (1, 4)
//! assert eval(123) => (123, 4)
//! assert eval(412223) => (412223, 4)
//! assert eval(23141) => (23141, 4)
use pbc_zk::Sbi32;

fn assignment(x: Sbi32) -> (Sbi32, Sbi32) {
    let mut y = (x, x);
    y.1 = Sbi32::from(4);
    y
}
