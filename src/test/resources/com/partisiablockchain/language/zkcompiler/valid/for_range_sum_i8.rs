//! assert eval() => 55

use pbc_zk::Sbi8;

pub fn compute_55() -> Sbi8 {
    let mut sum: Sbi8 = Sbi8::from(0);
    for idx in 0i8..11i8 {
        sum = sum + Sbi8::from(idx);
    }
    sum
}
