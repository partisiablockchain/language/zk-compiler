//! assert eval tests
use pbc_zk::*;

pub fn test_binop(x: i32, y: i32) -> bool {
    x < y
}

test_eq!(test_binop(-231, -1), true);
test_eq!(test_binop(-1, 0), true);
test_eq!(test_binop(0, -1), false);
test_eq!(test_binop(1, 0), false);
test_eq!(test_binop(1, 1), false);
test_eq!(test_binop(1, 2), true);
test_eq!(test_binop(1, 99), true);
test_eq!(test_binop(90, 99), true);
test_eq!(test_binop(99, 99), false);
test_eq!(test_binop(99, 100), true);
test_eq!(test_binop(100, 99), false);
