//! assert eval() => 0
//! assert eval tests
use pbc_zk::*;

pub fn range_expr() -> Sbi32 {
    0..9;
    Sbi32::from(0)
}

test_eq!(range_expr(), 0);
