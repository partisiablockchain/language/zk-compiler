//! assert eval() => 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF

struct MyPair {
    v1: u32,
    v2: u32,
    v3: u32,
    v4: u32,
}

pub fn big_constant() -> MyPair {
    create_my_pair(0xFFFFFFFFu32)
}

fn create_my_pair(value: u32) -> MyPair {
    MyPair {
        v1: value,
        v2: value,
        v3: value,
        v4: value,
    }
}
