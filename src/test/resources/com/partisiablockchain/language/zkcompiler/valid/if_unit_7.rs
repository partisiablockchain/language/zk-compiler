//! assert eval(0) => 0
//! assert eval(1) => 0
use pbc_zk::*;

fn identity(c: Sbu1) {
    let id = SecretVarId::new(0);
    let x1 = load_sbi::<()>(id);
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    let x1 = if c { x1 } else { load_sbi::<()>(id) };
    save_sbi::<()>(x1)
}
