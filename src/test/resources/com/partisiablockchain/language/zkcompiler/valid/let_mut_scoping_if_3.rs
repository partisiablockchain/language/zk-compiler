//! assert eval(x) => 4
use pbc_zk::*;

pub fn mut_if(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = x;
    let mut z: Sbi32 = x;
    if () == (y = Sbi32::from(4)) {
        z = y;
    }
    z
}
