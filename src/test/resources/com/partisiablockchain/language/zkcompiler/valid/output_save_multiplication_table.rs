//! assert eval(9, 0) => ()
//! assert eval(9, 1) => (9)
//! assert eval(9, 8) => (9, 18, 27, 36, 45, 54, 63, 72)
//! assert eval(3, 3) => (3, 6, 9)
//! assert eval(1, 6) => (1, 2, 3, 4, 5, 6)
use pbc_zk::save_sbi;
use pbc_zk::Sbi32;

pub fn zk_compute(a: Sbi32, num: i32) {
    let mut x: Sbi32 = a;
    for _ in 0..num {
        save_sbi::<Sbi32>(x);
        x = x + a;
    }
}
