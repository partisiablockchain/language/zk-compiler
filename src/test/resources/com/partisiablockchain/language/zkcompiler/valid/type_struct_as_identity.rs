//! assert eval(x) => x

use pbc_zk::Sbi8;

struct AlmostSbi32 {
    v1: Sbi8,
    v2: Sbi8,
    v3: Sbi8,
    v4: Sbi8,
}

fn identity(a: AlmostSbi32) -> AlmostSbi32 {
    a
}
