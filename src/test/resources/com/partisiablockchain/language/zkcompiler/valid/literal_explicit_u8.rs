//! assert eval() => 7
use pbc_zk::*;

fn literal() -> u8 {
    7u8
}
