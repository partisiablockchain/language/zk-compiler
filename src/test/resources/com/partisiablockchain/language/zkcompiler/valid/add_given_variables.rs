//! assert eval(1, 1) => 2
//! assert eval(1, 2) => 3
//! assert eval(2, 1) => 3
use pbc_zk::{load_sbi, Sbi32, SecretVarId};

pub fn zk_compute(left: SecretVarId, right: SecretVarId) -> Sbi32 {
    load_sbi::<Sbi32>(left) + load_sbi::<Sbi32>(right)
}
