//! assert eval(x) => (x, x, x, x, x, x, x)
use pbc_zk::{save_sbi, Sbi32, Sbu1};

pub fn main(input: Sbi32) {
    save_repeat(input, 0);
    save_repeat(input, 1);
    save_repeat(input, 1);
    save_repeat(input, 0);
    save_repeat(input, 5);
    save_repeat(input, 0);
}

fn save_repeat(val: Sbi32, reps: i32) {
    for i in 0..reps {
        save_sbi::<Sbi32>(val)
    }
}
