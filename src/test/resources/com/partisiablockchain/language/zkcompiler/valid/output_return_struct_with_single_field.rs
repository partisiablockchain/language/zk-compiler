//! assert eval(x) => x

use pbc_zk::Sbi32;

struct MyPair {
    v1: Sbi32,
}

pub fn identity(a: Sbi32) -> MyPair {
    MyPair { v1: a }
}
