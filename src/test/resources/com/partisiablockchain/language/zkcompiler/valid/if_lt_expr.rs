//! assert eval(0) => 0
//! assert eval(1) => 0
//! assert eval(2) => 0
//! assert eval(3) => 0
//! assert eval(4) => 1337
//! assert eval(1337) => 1337
use pbc_zk::*;

pub fn if_expr(x: Sbi32) -> Sbi32 {
    (if Sbi32::from(3) < x {
        Sbi32::from(1337)
    } else {
        Sbi32::from(0)
    })
}
