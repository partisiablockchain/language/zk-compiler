//! assert eval(0, 0) => 0
//! assert eval(0, 1) => 1
//! assert eval(1, 0) => 1
//! assert eval(1, 1) => 1

use pbc_zk::{Sbi32, Sbu1};

pub fn identity(x: Sbu1, y: Sbu1) -> Sbu1 {
    if x {
        x
    } else {
        y
    }
}
