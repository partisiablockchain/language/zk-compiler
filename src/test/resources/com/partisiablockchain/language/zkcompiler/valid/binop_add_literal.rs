//! assert eval(5) => 14
//! assert eval(93) => 102
//! assert eval(0) => 9
//! assert eval(9) => 18
//! assert eval tests
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    x + Sbi32::from(9)
}

test_eq!(circuit_1(Sbi32::from(5)), 14);
test_eq!(circuit_1(Sbi32::from(93)), 102);
test_eq!(circuit_1(Sbi32::from(0)), 9);
test_eq!(circuit_1(Sbi32::from(9)), 18);
