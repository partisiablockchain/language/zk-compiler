//! assert eval(0, 1) => 1
//! assert eval(1, 0) => 1
//! assert eval(0, 312) => 312
//! assert eval(15, 0) => 15
//! assert eval(15, 7) => 8
//! assert eval(10, 5) => 15
use pbc_zk::*;

pub fn xor(x: Sbi32, y: Sbi32) -> Sbi32 {
    x ^ y
}
