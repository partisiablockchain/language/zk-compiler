//! assert eval(0) => 0
//! assert eval(1) => 0
use pbc_zk;

pub fn compute() {
    let id = pbc_zk::SecretVarId::new(0);
    if false {
    } else if true {
        pbc_zk::save_sbi::<()>(pbc_zk::load_sbi::<()>(id))
    }
}
