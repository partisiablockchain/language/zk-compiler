//! assert eval(0, 2, 1, 3, 0, 2, 3, 0, 1, 5, 0, 6, 0, 1, 2, 0, 3, 0, 5, 6, 0, 3, 2, 6, 0, 3, 0, 7, 0, 3, 5, 2) => 0
//! assert eval tests

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything() -> [Sbi8; 8] {
    // Initialize state
    let mut counts: [Sbi8; 8] = [
        Sbi8::from(0),
        Sbi8::from(0),
        Sbi8::from(0),
        Sbi8::from(0),
        Sbi8::from(0),
        Sbi8::from(0),
        Sbi8::from(0),
        Sbi8::from(0),
    ];

    // Count each variable
    for variable_id in secret_variable_ids() {
        let var = load_sbi::<Sbi8>(variable_id);
        for value in 0..8 {
            counts = counts;
        }
    }

    counts
}

test_eq!(
    sum_everything() == [Sbi8::from(0); 8],
    true,
    [
        0i8, 2i8, 1i8, 3i8, 0i8, 2i8, 3i8, 0i8, 1i8, 5i8, 0i8, 6i8, 0i8, 1i8, 2i8, 0i8, 3i8, 0i8,
        5i8, 6i8, 0i8, 3i8, 2i8, 6i8, 0i8, 3i8, 0i8, 7i8, 0i8, 3i8, 5i8, 2i8
    ]
);
