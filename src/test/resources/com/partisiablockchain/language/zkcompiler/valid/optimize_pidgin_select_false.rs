//! assert eval(x) => x + 1

use pbc_zk::{Sbi32, Sbu1};

pub fn identity(x: Sbi32) -> Sbi32 {
    if x & x < x | x {
        x
    } else {
        x + Sbi32::from(1)
    }
}
