//! assert eval(2, 3) => 6
//! assert eval(0xFFFFFFFF, 1) => 0xFFFFFFFF
//! assert eval(-1, 1) => 0xFFFFFFFF
//! assert eval(0xFFFFFFFF, 0xFFFFFFFF) => 1
//! assert eval(0xFFFFFFFF, 0xFFFFFFFD) => 3
//! assert eval(0xFFFFFFFF, 0x7FFFFFFF) => 0x80000001

use pbc_zk::*;

fn unsigned_mult(x: u32, y: u32) -> u32 {
    x * y
}
