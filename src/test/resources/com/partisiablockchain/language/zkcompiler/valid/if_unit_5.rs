//! assert eval(x) => x
use pbc_zk::*;

fn identity(x: Sbi32) -> Sbi32 {
    if x == Sbi32::from(1) {
        if x == Sbi32::from(2) {
            if x == Sbi32::from(3) {
                if x == Sbi32::from(4) {
                    if x == Sbi32::from(5) {
                        if x == Sbi32::from(6) {
                            if x == Sbi32::from(7) {
                                if x == Sbi32::from(8) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(9) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(10) {
                                if x == Sbi32::from(11) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(12) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(13) {
                            if x == Sbi32::from(14) {
                                if x == Sbi32::from(15) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(16) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(17) {
                                if x == Sbi32::from(18) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(19) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                } else {
                    if x == Sbi32::from(20) {
                        if x == Sbi32::from(21) {
                            if x == Sbi32::from(22) {
                                if x == Sbi32::from(23) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(24) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(25) {
                                if x == Sbi32::from(26) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(27) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(28) {
                            if x == Sbi32::from(29) {
                                if x == Sbi32::from(30) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(31) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(32) {
                                if x == Sbi32::from(33) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(34) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                }
            } else {
                if x == Sbi32::from(35) {
                    if x == Sbi32::from(36) {
                        if x == Sbi32::from(37) {
                            if x == Sbi32::from(38) {
                                if x == Sbi32::from(39) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(40) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(41) {
                                if x == Sbi32::from(42) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(43) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(44) {
                            if x == Sbi32::from(45) {
                                if x == Sbi32::from(46) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(47) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(48) {
                                if x == Sbi32::from(49) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(50) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                } else {
                    if x == Sbi32::from(51) {
                        if x == Sbi32::from(52) {
                            if x == Sbi32::from(53) {
                                if x == Sbi32::from(54) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(55) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(56) {
                                if x == Sbi32::from(57) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(58) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(59) {
                            if x == Sbi32::from(60) {
                                if x == Sbi32::from(61) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(62) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(63) {
                                if x == Sbi32::from(64) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(65) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if x == Sbi32::from(66) {
                if x == Sbi32::from(67) {
                    if x == Sbi32::from(68) {
                        if x == Sbi32::from(69) {
                            if x == Sbi32::from(70) {
                                if x == Sbi32::from(71) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(72) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(73) {
                                if x == Sbi32::from(74) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(75) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(76) {
                            if x == Sbi32::from(77) {
                                if x == Sbi32::from(78) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(79) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(80) {
                                if x == Sbi32::from(81) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(82) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                } else {
                    if x == Sbi32::from(83) {
                        if x == Sbi32::from(84) {
                            if x == Sbi32::from(85) {
                                if x == Sbi32::from(86) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(87) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(88) {
                                if x == Sbi32::from(89) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(90) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(91) {
                            if x == Sbi32::from(92) {
                                if x == Sbi32::from(93) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(94) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(95) {
                                if x == Sbi32::from(96) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(97) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                }
            } else {
                if x == Sbi32::from(98) {
                    if x == Sbi32::from(99) {
                        if x == Sbi32::from(100) {
                            if x == Sbi32::from(101) {
                                if x == Sbi32::from(102) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(103) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(104) {
                                if x == Sbi32::from(105) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(106) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(107) {
                            if x == Sbi32::from(108) {
                                if x == Sbi32::from(109) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(110) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(111) {
                                if x == Sbi32::from(112) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(113) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                } else {
                    if x == Sbi32::from(114) {
                        if x == Sbi32::from(115) {
                            if x == Sbi32::from(116) {
                                if x == Sbi32::from(117) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(118) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(119) {
                                if x == Sbi32::from(120) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(121) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(122) {
                            if x == Sbi32::from(123) {
                                if x == Sbi32::from(124) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(125) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(126) {
                                if x == Sbi32::from(127) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(128) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        if x == Sbi32::from(129) {
            if x == Sbi32::from(130) {
                if x == Sbi32::from(131) {
                    if x == Sbi32::from(132) {
                        if x == Sbi32::from(133) {
                            if x == Sbi32::from(134) {
                                if x == Sbi32::from(135) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(136) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(137) {
                                if x == Sbi32::from(138) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(139) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(140) {
                            if x == Sbi32::from(141) {
                                if x == Sbi32::from(142) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(143) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(144) {
                                if x == Sbi32::from(145) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(146) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                } else {
                    if x == Sbi32::from(147) {
                        if x == Sbi32::from(148) {
                            if x == Sbi32::from(149) {
                                if x == Sbi32::from(150) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(151) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(152) {
                                if x == Sbi32::from(153) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(154) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(155) {
                            if x == Sbi32::from(156) {
                                if x == Sbi32::from(157) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(158) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(159) {
                                if x == Sbi32::from(160) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(161) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                }
            } else {
                if x == Sbi32::from(162) {
                    if x == Sbi32::from(163) {
                        if x == Sbi32::from(164) {
                            if x == Sbi32::from(165) {
                                if x == Sbi32::from(166) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(167) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(168) {
                                if x == Sbi32::from(169) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(170) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(171) {
                            if x == Sbi32::from(172) {
                                if x == Sbi32::from(173) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(174) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(175) {
                                if x == Sbi32::from(176) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(177) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                } else {
                    if x == Sbi32::from(178) {
                        if x == Sbi32::from(179) {
                            if x == Sbi32::from(180) {
                                if x == Sbi32::from(181) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(182) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(183) {
                                if x == Sbi32::from(184) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(185) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(186) {
                            if x == Sbi32::from(187) {
                                if x == Sbi32::from(188) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(189) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(190) {
                                if x == Sbi32::from(191) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(192) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if x == Sbi32::from(193) {
                if x == Sbi32::from(194) {
                    if x == Sbi32::from(195) {
                        if x == Sbi32::from(196) {
                            if x == Sbi32::from(197) {
                                if x == Sbi32::from(198) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(199) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(200) {
                                if x == Sbi32::from(201) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(202) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(203) {
                            if x == Sbi32::from(204) {
                                if x == Sbi32::from(205) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(206) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(207) {
                                if x == Sbi32::from(208) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(209) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                } else {
                    if x == Sbi32::from(210) {
                        if x == Sbi32::from(211) {
                            if x == Sbi32::from(212) {
                                if x == Sbi32::from(213) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(214) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(215) {
                                if x == Sbi32::from(216) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(217) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(218) {
                            if x == Sbi32::from(219) {
                                if x == Sbi32::from(220) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(221) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(222) {
                                if x == Sbi32::from(223) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(224) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                }
            } else {
                if x == Sbi32::from(225) {
                    if x == Sbi32::from(226) {
                        if x == Sbi32::from(227) {
                            if x == Sbi32::from(228) {
                                if x == Sbi32::from(229) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(230) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(231) {
                                if x == Sbi32::from(232) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(233) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(234) {
                            if x == Sbi32::from(235) {
                                if x == Sbi32::from(236) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(237) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(238) {
                                if x == Sbi32::from(239) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(240) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                } else {
                    if x == Sbi32::from(241) {
                        if x == Sbi32::from(242) {
                            if x == Sbi32::from(243) {
                                if x == Sbi32::from(244) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(245) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(246) {
                                if x == Sbi32::from(247) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(248) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    } else {
                        if x == Sbi32::from(249) {
                            if x == Sbi32::from(250) {
                                if x == Sbi32::from(251) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(252) {
                                    x
                                } else {
                                    x
                                }
                            }
                        } else {
                            if x == Sbi32::from(253) {
                                if x == Sbi32::from(254) {
                                    x
                                } else {
                                    x
                                }
                            } else {
                                if x == Sbi32::from(255) {
                                    x
                                } else {
                                    x
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
