//! assert eval(0) => 0
//! assert eval(1) => 1
//! assert eval(2) => 3
//! assert eval(3) => 6
//! assert eval(4) => 10
//! assert eval(10) => 55

use pbc_zk::Sbi32;

pub fn compute_55(max: i32) -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx in 0..max + 1 {
        sum = sum + Sbi32::from(idx);
    }
    sum
}
