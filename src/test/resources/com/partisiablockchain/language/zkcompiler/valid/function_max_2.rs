//! assert eval() => 321
//! use function_lib_max
use crate::valid::function_lib_max::max;
use pbc_zk::Sbi32;

pub fn test() -> Sbi32 {
    max(Sbi32::from(93), max(Sbi32::from(39), Sbi32::from(321)))
}
