//! assert eval(1, 1, 1) => (1, 1, 1)
//! assert eval(1, 9, 1) => (1, 1, 9)
//! assert eval(1, 3, 6) => (1, 3, 6)
//! assert eval(1, 6, 3) => (1, 3, 6)
//! assert eval(6, 3, 1) => (1, 3, 6)
//! assert eval(3, 6, 1) => (1, 3, 6)
use pbc_zk::*;

pub fn least_first(v1: Sbi32, v2: Sbi32, v3: Sbi32) -> (Sbi32, Sbi32, Sbi32) {
    let r1: (Sbi32, Sbi32) = if v1 <= v2 { (v1, v2) } else { (v2, v1) };
    let r2: (Sbi32, Sbi32) = if r1.0 <= v3 { (r1.0, v3) } else { (v3, r1.0) };
    let r3: (Sbi32, Sbi32) = if r1.1 <= r2.1 {
        (r1.1, r2.1)
    } else {
        (r2.1, r1.1)
    };

    (r2.0, r3.0, r3.1)
}
