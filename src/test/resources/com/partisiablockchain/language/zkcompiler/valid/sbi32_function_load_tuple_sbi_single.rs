//! assert eval(x) => x

use pbc_zk::{load_sbi, Sbi32, SecretVarId};

pub fn identity() -> Sbi32 {
    let id = SecretVarId::new(1);
    let pair: (Sbi32,) = load_sbi::<(Sbi32,)>(id);
    pair.0
}
