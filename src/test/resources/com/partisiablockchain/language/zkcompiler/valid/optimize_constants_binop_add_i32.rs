//! assert eval(x) => 15
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> i32 {
    1 + 2 + 3 + 4 + 5
}
