//! assert eval(9) => 0
//! assert eval(18) => 9
//! assert eval(100) => 91
//! assert eval tests
use pbc_zk::*;

pub fn subtract(x: Sbi32) -> Sbi32 {
    x - Sbi32::from(9)
}

test_eq!(subtract(Sbi32::from(9)), 0);
test_eq!(subtract(Sbi32::from(18)), 9);
test_eq!(subtract(Sbi32::from(100)), 91);
