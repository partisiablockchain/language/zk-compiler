//! assert eval(x) => 99
use pbc_zk::*;

pub fn access(x: Sbi32) -> Sbi32 {
    (x, Sbi32::from(99)).1
}
