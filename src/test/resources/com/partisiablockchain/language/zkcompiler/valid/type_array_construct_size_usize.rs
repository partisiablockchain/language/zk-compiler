//! assert eval() => 0
use pbc_zk::Sbi32;

pub fn sum_everything() -> Sbi32 {
    // Initialize state
    let counts: [i32; 2] = [1; 2usize];
    Sbi32::from(0)
}
