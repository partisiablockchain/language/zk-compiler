//! assert eval(0) => 0
//! assert eval(123) => 0
//! assert eval(320) => 0
//! assert eval(321) => 0
//! assert eval(322) => 1
//! assert eval(342132) => 1
//! use function_lib_max
use crate::valid::function_lib_max::max;
use pbc_zk::{Sbi32, Sbu1};

pub fn test(input: Sbi32) -> Sbu1 {
    let a = max(Sbi32::from(39), Sbi32::from(321));
    let b = max(Sbi32::from(93), a);
    b < input
}
