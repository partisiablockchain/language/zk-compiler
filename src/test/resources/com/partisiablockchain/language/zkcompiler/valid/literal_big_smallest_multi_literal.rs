//! assert eval() => 4294967296
pub fn big() -> u64 {
    4294967296u64
}
