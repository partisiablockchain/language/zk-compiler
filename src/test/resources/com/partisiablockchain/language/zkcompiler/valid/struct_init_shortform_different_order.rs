//! assert eval(x) => x

struct MyPair {
    v1: i32,
    v2: i32,
}

pub fn identity(x: i32) -> i32 {
    let v1 = x;
    let v2 = 9;

    let m = MyPair { v2, v1 };

    m.v1
}
