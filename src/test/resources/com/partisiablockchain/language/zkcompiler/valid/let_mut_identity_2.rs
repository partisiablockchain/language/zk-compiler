//! assert eval(x) => x
use pbc_zk::*;

fn assignment(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = Sbi32::from(0);
    y = x;
    y
}
