//! assert eval() => 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF

struct MyPair {
    v1: u32,
    v2: u32,
    v3: u32,
    v4: u32,
}

pub fn big_constant() -> MyPair {
    create_my_pair(1)
}

fn create_my_pair(value: i32) -> MyPair {
    MyPair {
        v1: (-value) as u32,
        v2: (-value) as u32,
        v3: (-value) as u32,
        v4: (-value) as u32,
    }
}
