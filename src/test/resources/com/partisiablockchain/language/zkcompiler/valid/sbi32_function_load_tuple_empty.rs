//! assert eval(0) => ()
//! assert eval(0) => ()

use pbc_zk::{load_sbi, SecretVarId};

fn identity() -> () {
    let id = SecretVarId::new(1);
    load_sbi::<()>(id)
}
