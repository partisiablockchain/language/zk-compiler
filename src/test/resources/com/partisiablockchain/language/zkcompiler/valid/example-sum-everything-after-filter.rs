//! assert eval(60, 99, 33, 55) => 88
//! assert eval(60, 55, 99, 33) => 88
//! assert eval(60, 33, 55, 99) => 88
//! assert eval(60, 33, 99, 55) => 88
//! assert eval(60, 55, 99, 55) => 110
//! assert eval(60, 55, 55, 11) => 121
//! assert eval(60, 11, 44, 44) => 99
//! assert eval(60, 44, 11, 11) => 66
//! assert eval(5, 1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1) => 20
//! assert eval(1000, 312, 4123, 532, 34, 324, 34224, 432) => 1634
//! assert eval(1000, 3020, 23130, 320) => 320
use pbc_zk;
use pbc_zk::{load_sbi, secret_variable_ids, Sbi32};

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
pub fn sum_everything(max: i32) -> Sbi32 {
    // Initialize state
    let mut sum: Sbi32 = Sbi32::from(0);

    // Sum each variable
    for variable_id in secret_variable_ids() {
        let value = load_sbi::<Sbi32>(variable_id);
        if value < Sbi32::from(max) {
            sum = sum + value;
        }
    }

    sum
}
