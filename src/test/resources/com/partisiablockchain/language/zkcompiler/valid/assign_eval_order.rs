//! assert eval(x) => x
use pbc_zk::Sbi32;
use std::vec::Vec;

fn incredibly_complicated_identity(x: u32) -> u32 {
    let mut vec: Vec<u32> = Vec::new::<u32>();
    Vec::push::<u32>(vec, 1);
    Vec::push::<u32>(vec, x);

    let mut arr: [u32; 2] = [0; 2];
    arr[Vec::pop::<u32>(vec)] = Vec::pop::<u32>(vec);
    arr[1]
}
