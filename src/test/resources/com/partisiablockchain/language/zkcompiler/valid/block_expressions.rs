//! assert eval(5) => 14
//! assert eval(93) => 102
//! assert eval(0) => 9
//! assert eval(9) => 18
use pbc_zk::*;

fn nested_blocks(x: Sbi32) -> Sbi32 {
    Sbi32::from(2) + {
        let y: Sbi32 = Sbi32::from(3);
        ({ y + Sbi32::from(2) }) + ({ Sbi32::from(2) + x })
    }
}
