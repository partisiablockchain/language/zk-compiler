//! assert eval() => 9
use pbc_zk::*;

fn f() -> usize {
    9usize
}
