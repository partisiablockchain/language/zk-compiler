//! assert eval(0, 2, 1, 3, 0, 2, 3, 0, 1, 5, 0, 6, 0, 1, 2, 0, 3, 0, 5, 6, 0, 3, 2, 6, 0, 3, 0, 7, 0, 3, 5, 2) => 0x01_03_03_00_06_05_03_0b

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

struct Counter {
    count: Sbi8,
}

pub fn sum_everything() -> [Counter; 8] {
    // Initialize state
    let mut counts: [Counter; 8] = [Counter {
        count: Sbi8::from(0),
    }; 8];

    // Count each variable
    for variable_id in secret_variable_ids() {
        let var = load_sbi::<Sbi8>(variable_id);
        for value in 0i8..8i8 {
            let idx = value as usize;
            let inc = ((var == Sbi8::from(value)) as Sbi8);
            counts[idx].count = counts[idx].count + inc;
        }
    }

    counts
}
