//! assert eval(x) => 15
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    if x == Sbi32::from(9) {
        Sbi32::from(15)
    } else {
        Sbi32::from(15)
    }
}
