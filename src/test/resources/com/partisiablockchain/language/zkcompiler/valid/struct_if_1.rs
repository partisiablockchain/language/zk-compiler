//! assert eval(0) => (2, 3)
//! assert eval(1) => (1, 4)

struct MyStruct {
    a: i32,
    b: i32,
}

pub fn select(p: bool) -> (i32, i32) {
    let s = MyStruct {
        a: (if p { 1 } else { 2 }),
        b: (if !p { 3 } else { 4 }),
    };
    (s.a, s.b)
}
