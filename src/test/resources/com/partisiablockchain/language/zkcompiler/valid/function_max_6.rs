//! assert eval(0) => (0, 21)
//! assert eval(50) => (50, 50)
//! assert eval(100) => (99, 100)
//! use function_lib_max
use crate::valid::function_lib_max::{max, min};
use pbc_zk::{Sbi32, Sbu1};

pub fn test(input: Sbi32) -> (Sbi32, Sbi32) {
    let a = min(input, Sbi32::from(99));
    let b = max(input, Sbi32::from(21));
    (a, b)
}
