//! assert eval(0) => 30
//! assert eval(1) => 36

use pbc_zk::Sbi32;

pub fn compute_55(b: bool) -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx in (if b { 1 } else { 6 })..(if b { 9 } else { 10 }) {
        sum = sum + Sbi32::from(idx);
    }
    sum
}
