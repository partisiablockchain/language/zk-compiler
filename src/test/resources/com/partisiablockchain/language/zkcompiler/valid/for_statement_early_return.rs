//! assert eval(x) => 9

use pbc_zk::*;

fn for_statement(x: Sbi32) -> Sbi32 {
    for i in 9..4123 {
        return Sbi32::from(i);
    }
    Sbi32::from(8)
}
