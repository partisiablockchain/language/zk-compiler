//! assert eval(x) => x
use pbc_zk::Sbi32;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    let c: i32 = (6 * 3) >> 1;
    if c < 10 {
        x
    } else {
        Sbi32::from(15)
    }
}
