//! assert eval(x) => x + 23

const MY_CONST: i32 = 23;

fn plus_const(x: i32) -> i32 {
    x + MY_CONST
}
