//! assert eval(1, 1) => (1, 1)
//! assert eval(1, 9) => (1, 9)
//! assert eval(9, 1) => (1, 9)
//! assert eval(9, 9) => (9, 9)
use pbc_zk::*;

pub fn least_first(v1: Sbi32, v2: Sbi32) -> (Sbi32, Sbi32) {
    if v1 <= v2 {
        (v1, v2)
    } else {
        (v2, v1)
    }
}
