//! assert eval(99, 33, 55) => (1, 55)
//! assert eval(55, 99, 33) => (2, 55)
//! assert eval(33, 55, 99) => (3, 55)
//! assert eval(33, 99, 55) => (2, 55)
//! assert eval(55, 99, 55) => (2, 55)
//! assert eval(55, 55, 11) => (1, 55)
//! assert eval(11, 44, 44) => (2, 44)
//! assert eval(44, 11, 11) => (1, 11)

/// Perform a zk computation on secret-shared data.
/// Find the winner of the second-price auction with exactly Sbi32::from(3)  bidders.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn zk_compute() -> (Sbi32, Sbi32) {
    // Initialize state
    let mut highest_bidder: Sbi32 = Sbi32::from(1);
    let mut highest_amount: Sbi32 = Sbi32::from(0);
    let mut second_highest_amount: Sbi32 = Sbi32::from(0);

    // Determine max
    for variable_id_raw in 1usize..4usize {
        let variable_id = SecretVarId::new(variable_id_raw);
        if load_sbi::<Sbi32>(variable_id) > highest_amount {
            second_highest_amount = highest_amount;
            highest_amount = load_sbi::<Sbi32>(variable_id);
            highest_bidder = Sbi32::from(variable_id_raw as i32);
        } else if load_sbi::<Sbi32>(variable_id) > second_highest_amount {
            second_highest_amount = load_sbi::<Sbi32>(variable_id);
        }
    }

    // Return highest bidder index, and second highest amount
    (highest_bidder, second_highest_amount)
}
