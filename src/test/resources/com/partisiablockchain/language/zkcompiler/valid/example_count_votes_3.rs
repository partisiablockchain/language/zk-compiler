//! assert eval(0) => 0
//! assert eval(0,1,3) => 2
//! assert eval(0,1,2,1,2) => 4
//! assert eval(9,8,1,4,3) => 5
use pbc_zk::*;

/// Perform a zk computation on secret-shared data to count the number
/// of accepting votes (non-zero).
///
/// ### Returns:
///
/// The number of accepting votes.
#[zk_compute(shortname = 0x61)]
pub fn count_votes() -> Sbi32 {
    // Initialize votes
    let mut num_votes_for: Sbi32 = Sbi32::from(0);

    // Count votes
    for variable_id in secret_variable_ids() {
        let vote = load_sbi::<Sbi32>(variable_id);
        let is_vote_for: Sbu1 = vote != Sbi32::from(0);
        num_votes_for = num_votes_for + (is_vote_for as Sbi32);
    }
    num_votes_for
}
