//! assert eval(x) => x*8
use pbc_zk::*;

pub fn mult_8(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = x;
    y = y + y;
    y = y + y;
    y = y + y;
    y
}
