//! assert eval() => 0
use pbc_zk::*;

pub fn le() -> bool {
    0 <= 2147483648u32 as i32
}
