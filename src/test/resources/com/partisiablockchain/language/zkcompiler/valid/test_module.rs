//! assert eval(x) => x
//! use test_module/module

mod module;
use crate::valid::test_module::module::{id, id2};

pub fn use_module(x: i32) -> i32 {
    id(id2(x))
}
