//! assert eval(0) => 0
//! assert eval(1) => 1
//! assert eval(2) => 1
//! assert eval(3) => 2
//! assert eval(4) => 3
//! assert eval(5) => 5
//! assert eval(6) => 8
//! assert eval(7) => 13
//! assert eval(8) => 21
//! assert eval(9) => 34
//! assert eval(10) => 55

pub fn test(n: i32) -> i32 {
    fib(n)
}

fn fib(n: i32) -> i32 {
    if n < 2 {
        n
    } else {
        fib(n - 1) + fib(n - 2)
    }
}
