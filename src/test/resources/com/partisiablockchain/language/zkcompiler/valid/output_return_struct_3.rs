//! assert eval(x) => x

use pbc_zk::Sbi8;

struct MyPair {
    v1: Sbi8,
    v2: Sbi8,
    v3: Sbi8,
    v4: Sbi8,
}

pub fn identity(pair: MyPair) -> MyPair {
    pair
}
