//! assert eval(x) => 31
use pbc_zk::Sbi32;

pub fn circuit_1(x: Sbi32) -> i32 {
    let a = (if true { (1) } else { (3) });
    let b = (if false { 10 * a } else { 30 * a });
    a + b
}
