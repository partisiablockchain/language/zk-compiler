//! assert eval(0x00_00_00_00_00_00_00_00) => 0
//! assert eval(0x00_01_00_00_03_00_00_08) => 12
//! assert eval(0x01_02_04_08_00_00_00_00) => 15
//! assert eval(0x01_02_04_08_08_04_02_01) => 30
use pbc_zk::Sbi8;

pub fn compute(values: [u8; 8]) -> u8 {
    let mut sum: u8 = 0;
    for idx in 0usize..8usize {
        sum = sum + values[idx];
    }
    sum
}
