//! assert eval(0) => 0x20

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything(arr: [u8; 32]) -> [u8; 8] {
    // Initialize state
    let mut counts: [u8; 8] = [0; 8];

    // Count each variable
    for variable_id in 0usize..32usize {
        let var = arr[variable_id];
        for value in 0usize..8usize {
            let m: u8 = if var == (value as u8) { 1 } else { 0 };
            counts[value] = counts[value] + m;
        }
    }

    counts
}
