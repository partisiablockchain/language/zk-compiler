//! assert eval(x) => x*2
//! assert eval tests
use pbc_zk::*;

pub fn add_self(x: Sbi32) -> Sbi32 {
    x + x
}

test_eq!(add_self(Sbi32::from(42)), 84);
