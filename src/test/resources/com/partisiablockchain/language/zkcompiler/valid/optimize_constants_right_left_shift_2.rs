//! assert eval(x) => 0
use pbc_zk::Sbi32;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    (x >> 32) << 32
}
