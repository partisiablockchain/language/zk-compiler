//! assert eval(x) => x
use pbc_zk::*;

pub fn test(v1: Sbi32) -> Sbi32 {
    (v1, v1, v1).2
}
