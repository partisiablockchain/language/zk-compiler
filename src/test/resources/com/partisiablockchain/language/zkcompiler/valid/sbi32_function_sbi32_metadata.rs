//! assert eval(x) => x
use pbc_zk::*;

pub fn sbi() -> Sbi32 {
    let id = SecretVarId::new(1);
    Sbi32::from(load_metadata::<i32>(id))
}
