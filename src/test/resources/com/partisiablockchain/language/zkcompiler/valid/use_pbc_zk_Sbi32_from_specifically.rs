//! assert eval(x) => 9

use pbc_zk::Sbi32;
use pbc_zk::Sbi32::from;

fn identity(x: Sbi32) -> Sbi32 {
    from(9)
}
