//! assert eval(x) => 1
use pbc_zk::*;

fn summing(limit: i32) -> bool {
    let mut sum: i32 = 0;
    for x in (-limit)..(-limit + 1) {
        sum = sum + x;
    }
    -sum == limit
}
