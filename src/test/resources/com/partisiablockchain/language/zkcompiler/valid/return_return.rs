//! assert eval(x) => x
use pbc_zk::*;

pub fn ret(x: Sbi32) -> Sbi32 {
    return {
        return {
            return {
                return {
                    return {
                        return { x };
                        x
                    };
                    x
                };
                x
            };
            x
        };
        x
    };
    x
}
