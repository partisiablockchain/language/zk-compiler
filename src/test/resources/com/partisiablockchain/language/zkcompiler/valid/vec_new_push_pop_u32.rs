//! assert eval(x) => x
use std::vec::Vec;

pub fn test(x: u32) -> u32 {
    let vec: Vec<u32> = Vec::new::<u32>();
    Vec::push::<u32>(vec, x);
    Vec::pop::<u32>(vec)
}
