//! assert eval() => 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF

struct MyPair {
    v1: u32,
    v2: u32,
    v3: u32,
    v4: u32,
}

pub fn big_constant() -> MyPair {
    MyPair {
        v1: (-1) as u32,
        v2: (-1) as u32,
        v3: (-1) as u32,
        v4: (-1) as u32,
    }
}
