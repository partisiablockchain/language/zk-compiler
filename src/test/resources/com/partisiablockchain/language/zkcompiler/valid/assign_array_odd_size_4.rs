//! assert eval() => 0x_0fff_21_0fff_10
//! assert eval tests
//!
//! Tests array assignment with difficult index sizes.

use pbc_zk::*;

struct Input {
    value: Sbi8,
}

#[derive(Copy, Clone)]
pub struct Counter {
    value: Sbi8,
    count: Sbi16,
}

pub fn sum_everything() -> [Counter; 2] {
    // Initialize state
    let mut counts: [Counter; 2] = [Counter {
        value: Sbi8::from(0),
        count: Sbi16::from(0),
    }; 2];

    counts[0].value = Sbi8::from(0x10i8);
    counts[1].value = Sbi8::from(0x21i8);
    counts[0].count = Sbi16::from(0xfffi16);
    counts[1].count = Sbi16::from(0xfffi16);

    counts
}

fn test_index(index: usize, v1: i8, v2: i16) -> Sbu1 {
    let counts = sum_everything();
    counts[index].value == Sbi8::from(v1) && counts[index].count == Sbi16::from(v2)
}

pbc_zk::test_eq!(test_index(0, 0x10i8, 0xfffi16), true);
pbc_zk::test_eq!(test_index(1, 0x21i8, 0xfffi16), true);
