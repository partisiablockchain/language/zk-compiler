//! assert eval(0, 2, 1, 3, 0, 2, 3, 0, 1, 5, 0, 6, 0, 1, 2, 0, 3, 0, 5, 6, 0, 3, 2, 6, 0, 3, 0, 7, 0, 3, 5, 2) => 32

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

struct Input {
    internal: [Sbi32; 0],
}

struct Counter {
    count: Sbi8,
}

pub fn sum_everything() -> [Counter; 1] {
    // Initialize state
    let mut counts: [Counter; 1] = [Counter {
        count: Sbi8::from(0),
    }; 1];

    // Count each variable
    for variable_id in secret_variable_ids() {
        let input: Input = load_sbi::<Input>(variable_id);
        let inc: Sbi8 = (input.internal == input.internal) as Sbi8;
        let prev: Sbi8 = counts[0].count;
        counts[0].count = prev + inc;
    }

    counts
}
