//! assert eval(0, 0) => 1
//! assert eval(0, 9) => 0
//! assert eval(9, 9) => 1
//! assert eval(9, 0) => 0

use pbc_zk::{Sbi32, Sbu1};

pub fn redundant(x: Sbi32, y: Sbi32) -> Sbu1 {
    (x == y) && (y == x)
}
