//! assert eval(0, 0) => 1
//! assert eval tests
use pbc_zk::*;

pub fn equal(v1: [Sbi32; 0], v2: [Sbi32; 0]) -> Sbu1 {
    v1 == v2
}

test_eq!(equal([Sbi32::from(0); 0], [Sbi32::from(0); 0]), true);
