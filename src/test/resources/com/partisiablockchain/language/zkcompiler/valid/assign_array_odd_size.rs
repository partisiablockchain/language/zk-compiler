//! assert eval() => 0x02_01_00
//! assert eval tests
//!
//! Tests array assignment with difficult index sizes.

use pbc_zk::Sbi8;

pub struct Tuple {
    v1: Sbi8,
    v2: Sbi8,
    v3: Sbi8,
}

pub fn sum_everything() -> [Tuple; 1] {
    // Initialize state
    let mut counts: [Tuple; 1] = [Tuple {
        v1: Sbi8::from(0),
        v2: Sbi8::from(0),
        v3: Sbi8::from(0),
    }; 1];
    counts[0].v1 = Sbi8::from(0);
    counts[0].v2 = Sbi8::from(1);
    counts[0].v3 = Sbi8::from(2);
    counts
}

pbc_zk::test_eq!(
    {
        let c = sum_everything();
        c[0].v1
    },
    0
);
pbc_zk::test_eq!(
    {
        let c = sum_everything();
        c[0].v2
    },
    1
);
pbc_zk::test_eq!(
    {
        let c = sum_everything();
        c[0].v3
    },
    2
);
