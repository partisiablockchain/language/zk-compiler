//! assert eval(x) => ()
use pbc_zk::*;

pub fn f(i: i32) {
    if (i > 0) {
        f(i - 1)
    }
}
