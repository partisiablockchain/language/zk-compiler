//! assert eval(0) => (2, 3)
//! assert eval(1) => (1, 4)

pub fn select(p: bool) -> (i32, i32) {
    ((if p { 1 } else { 2 }), (if !p { 3 } else { 4 }))
}
