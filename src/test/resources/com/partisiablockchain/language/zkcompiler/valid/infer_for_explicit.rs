//! assert eval() => 10

use pbc_zk::Sbi8;

pub fn infer_for_var() -> Sbi8 {
    let mut sum = Sbi8::from(0);

    for i in 1..5i8 {
        sum = sum + Sbi8::from(i);
    }

    sum
}
