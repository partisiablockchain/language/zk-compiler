//! assert eval(x) => x
use pbc_zk::*;

fn f(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = x;
    if x == x {
        let y: Sbi32 = Sbi32::from(6);
    }
    y
}
