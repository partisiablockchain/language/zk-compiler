//! assert eval(x) => 0
//! use function_lib_max
use crate::valid::function_lib_max::max;
use pbc_zk::{Sbi32, Sbu1};

pub fn test(input: Sbi32) -> Sbu1 {
    let a = max(Sbi32::from(39), Sbi32::from(321));
    let b = max(input, a);
    b < a
}
