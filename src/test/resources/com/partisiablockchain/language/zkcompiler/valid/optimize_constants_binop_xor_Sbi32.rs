//! assert eval() => 12
use pbc_zk::Sbi32;

pub fn circuit_1() -> Sbi32 {
    Sbi32::from(5) ^ Sbi32::from(9)
}
