//! assert eval() => 1
use pbc_zk::*;

pub fn return_expr() -> Sbu1 {
    Sbi32::from(1) as Sbu1
}
