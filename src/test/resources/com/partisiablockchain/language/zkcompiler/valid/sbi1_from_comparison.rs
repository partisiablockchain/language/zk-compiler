//! assert eval(1, 1) => 1
//! assert eval(1, 2) => 0
use pbc_zk::Sbu1;

pub fn comp_sbi(x: i32, y: i32) -> Sbu1 {
    Sbu1::from(x == y)
}
