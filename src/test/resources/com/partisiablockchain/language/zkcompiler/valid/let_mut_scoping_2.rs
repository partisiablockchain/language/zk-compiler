//! assert eval(0) => 0
//! assert eval(1) => 0
//! assert eval(2) => 0
//! assert eval(3) => 1337
//! assert eval(4) => 0
//! assert eval(1337) => 0
use pbc_zk::*;

pub fn mut_if(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = Sbi32::from(0);
    if x == Sbi32::from(3) {
        y = Sbi32::from(1337);
        let y: Sbi32 = Sbi32::from(3);
    }
    y
}
