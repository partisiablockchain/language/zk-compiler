//! assert eval(x) => x*3
//! assert eval tests
use pbc_zk::*;

pub fn mult(x: Sbi32) -> Sbi32 {
    x * Sbi32::from(3)
}

test_eq!(mult(Sbi32::from(1)), 3);
test_eq!(mult(Sbi32::from(2)), 6);
test_eq!(mult(Sbi32::from(3)), 9);
test_eq!(mult(Sbi32::from(42)), 126);
test_eq!(mult(Sbi32::from(95)), 285);
