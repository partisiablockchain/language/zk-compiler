//! assert eval() => 10

use pbc_zk::*;

pub fn compute_55() -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx in 0..11 {
        if idx == 5 {
            break;
        }
        sum = sum + Sbi32::from(idx);
    }
    sum
}
