//! assert eval(x) => 1

use pbc_zk::{Sbi32, Sbu1};

pub fn tautologic(x: Sbi32) -> Sbu1 {
    let zero: Sbi32 = Sbi32::from(0);

    // Produce zero
    let and: Sbi32 = x & zero;
    let mul: Sbi32 = x * zero;

    // Produce left
    let shl: Sbi32 = x << 0;
    let shr: Sbi32 = x >> 0;
    let or: Sbi32 = x | zero;
    let xor: Sbi32 = x ^ zero;
    let add: Sbi32 = x + zero;

    (and == mul && mul == zero)
        && (shl == shl && shl == shr && shr == or && or == xor && xor == xor && add == x)
}
