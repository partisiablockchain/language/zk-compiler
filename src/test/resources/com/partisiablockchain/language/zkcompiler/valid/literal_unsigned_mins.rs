//! assert eval() => ()

pub fn unsigned_mins() {
    let _u8 = 0u8;
    let _u16 = 0u16;
    let _u32 = 0u32;
    let _u64 = 0u64;
    let _u128 = 0u128;
}
