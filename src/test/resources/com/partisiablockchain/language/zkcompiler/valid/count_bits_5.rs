//! assert eval(0) => 0
//! assert eval(65536) => 1
//! assert eval(933212333) => 19
//! assert eval(2147483647) => 31
//! assert eval(-2147483648) => 1
//! assert eval(-1) => 32
use pbc_zk::{Sbu1, Sbu32};

pub fn test(i1: Sbu32) -> Sbu32 {
    count_bits(i1)
}

fn count_bits(x: Sbu32) -> Sbu32 {
    let mut r: Sbu32 = Sbu32::from(0);
    for idx in 0usize..32usize {
        r = r + (is_bit_set(x, idx) as Sbu32);
    }
    r
}

fn is_bit_set(x: Sbu32, idx: usize) -> Sbu1 {
    let bit_mask = Sbu32::from(get_bit_mask(idx));
    (bit_mask & x) != Sbu32::from(0)
}

fn get_bit_mask(i: usize) -> usize {
    1 << i
}
