//! assert eval(x) => x
use pbc_zk::*;

pub fn f(x: Sbi32) -> Sbi32 {
    x
}

fn g(x: Sbi32) -> Sbi32 {
    x
}
