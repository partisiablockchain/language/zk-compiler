//! assert eval(9, 4) => 2
//! assert eval(9, 9) => 1
//! assert eval(0, 0) => 1
//! assert eval(0, 1) => 2
//! assert eval(1, 0) => 2
//! assert eval tests
use pbc_zk::*;

#[zk_compute(shortname = 0x21)]
pub fn if_eq(x: Sbi32, y: Sbi32) -> Sbi32 {
    if_eq_inner(x, y)
}

fn if_eq_inner(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x == y {
        Sbi32::from(1)
    } else {
        Sbi32::from(2)
    }
}

pbc_zk::test_eq!(if_eq_inner(Sbi32::from(9), Sbi32::from(4)), 2);
pbc_zk::test_eq!(if_eq_inner(Sbi32::from(9), Sbi32::from(9)), 1);
pbc_zk::test_eq!(if_eq_inner(Sbi32::from(0), Sbi32::from(0)), 1);
pbc_zk::test_eq!(if_eq_inner(Sbi32::from(0), Sbi32::from(1)), 2);
pbc_zk::test_eq!(if_eq_inner(Sbi32::from(1), Sbi32::from(0)), 2);
