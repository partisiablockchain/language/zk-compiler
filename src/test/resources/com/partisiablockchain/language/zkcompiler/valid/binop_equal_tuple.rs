//! assert eval(1,2,3,4) => 0
//! assert eval(1,2,1,2) => 1
//! assert eval(1,3,1,2) => 0
//! assert eval(9,3,9,2) => 0
//! assert eval(9,9,9,9) => 1
//! assert eval tests
use pbc_zk::*;

pub fn double_identity(v1: Sbi32, v2: Sbi32, v3: Sbi32, v4: Sbi32) -> Sbi32 {
    if (v1, v2) == (v3, v4) {
        Sbi32::from(1)
    } else {
        Sbi32::from(0)
    }
}

test_eq!(
    double_identity(
        Sbi32::from(1),
        Sbi32::from(2),
        Sbi32::from(3),
        Sbi32::from(4)
    ),
    0
);
test_eq!(
    double_identity(
        Sbi32::from(1),
        Sbi32::from(2),
        Sbi32::from(1),
        Sbi32::from(2)
    ),
    1
);
test_eq!(
    double_identity(
        Sbi32::from(1),
        Sbi32::from(3),
        Sbi32::from(1),
        Sbi32::from(2)
    ),
    0
);
test_eq!(
    double_identity(
        Sbi32::from(9),
        Sbi32::from(3),
        Sbi32::from(9),
        Sbi32::from(2)
    ),
    0
);
test_eq!(
    double_identity(
        Sbi32::from(9),
        Sbi32::from(9),
        Sbi32::from(9),
        Sbi32::from(9)
    ),
    1
);
