//! assert eval(x) => 1
use pbc_zk::{load_sbi, Sbi32, Sbu1, SecretVarId};

pub fn sbi() -> Sbu1 {
    let id = SecretVarId::new(1);
    load_sbi::<Sbi32>(id) == load_sbi::<Sbi32>(id)
}
