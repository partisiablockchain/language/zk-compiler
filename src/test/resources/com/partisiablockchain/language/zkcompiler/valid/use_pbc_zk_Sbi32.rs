//! assert eval(x) => x

use pbc_zk::Sbi32;

fn identity(x: Sbi32) -> Sbi32 {
    x
}
