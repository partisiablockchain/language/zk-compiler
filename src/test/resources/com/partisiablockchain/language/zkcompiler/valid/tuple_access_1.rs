//! assert eval(x) => x
use pbc_zk::*;

pub fn access(x: Sbi32) -> Sbi32 {
    (x, Sbi32::from(99)).0
}
