//! assert eval(1) => (9, 4)
//! assert eval(123) => (9, 4)
//! assert eval(412223) => (9, 4)
//! assert eval(23141) => (9, 4)
use pbc_zk::Sbi32;

fn assignment(x: Sbi32) -> (Sbi32, Sbi32) {
    let mut y = (x, x);
    y.1 = Sbi32::from(421);
    y.0 = Sbi32::from(9);
    y.1 = Sbi32::from(4);
    y
}
