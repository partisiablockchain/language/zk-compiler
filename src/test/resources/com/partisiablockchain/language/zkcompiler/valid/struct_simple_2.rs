//! assert eval(x) => 9

struct MyPair {
    first: Sbi32,
    second: i32,
}

use pbc_zk::*;

pub fn something(a: Sbi32) -> i32 {
    let pair: MyPair = MyPair {
        first: a,
        second: 9,
    };
    pair.second
}
