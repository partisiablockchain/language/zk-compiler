//! assert eval(0x00_00_00_00_00_00_00_00) => 0x00_11_00_11_00_11_00_11
//! assert eval(0x00_01_00_00_03_00_00_08) => 0x00_11_00_11_03_11_00_11
//! assert eval(0x01_02_04_08_00_00_00_00) => 0x01_11_04_11_00_11_00_11
//! assert eval(0x01_02_04_08_08_04_02_01) => 0x01_11_04_11_08_11_02_11
use pbc_zk::Sbi32;

struct MyStruct {
    a: u8,
    b: u8,
}

pub fn compute(mut values: [MyStruct; 4]) -> [MyStruct; 4] {
    for idx in 0usize..4usize {
        values[idx].a = 0x11u8;
    }
    values
}
