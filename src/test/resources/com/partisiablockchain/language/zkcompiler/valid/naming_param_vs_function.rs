//! assert eval(0, 0) => 7
//! assert eval(3, 0) => 10
//! assert eval(0, 3) => 7
//! assert eval(3, 9) => 10
use pbc_zk::*;

pub fn param_overlap(x: Sbi32, zero: Sbi32) -> Sbi32 {
    x + Sbi32::from(7)
}

fn zero() {}
