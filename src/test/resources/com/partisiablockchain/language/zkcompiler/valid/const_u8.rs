//! assert eval(0) => 23
//! assert eval(5) => 28
//! assert eval(121) => 144
//! assert eval(255) => 22

type mytype = u8;

const MY_CONST: mytype = 23;

fn plus_const(x: mytype) -> mytype {
    x + MY_CONST
}
