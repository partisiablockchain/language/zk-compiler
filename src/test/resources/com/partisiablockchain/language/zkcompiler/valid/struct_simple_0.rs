//! assert eval(x) => x

struct Pair {
    v1: Sbi32,
    v2: Sbi32,
}

use pbc_zk::*;

pub fn try_stuff(x: Sbi32) -> Sbi32 {
    x
}
