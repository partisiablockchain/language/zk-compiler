//! assert eval(x) => x * 2
use pbc_zk::*;

pub fn circuit_1(x: usize) -> usize {
    x + x
}
