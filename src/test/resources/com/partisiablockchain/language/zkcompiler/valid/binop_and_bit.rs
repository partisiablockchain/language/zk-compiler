//! assert eval() => 1
//! assert eval tests
use pbc_zk::*;

pub fn return_expr() -> Sbu1 {
    let mut bit = Sbi8::from(1) == Sbi8::from(1);
    for i in 0..10_001 {
        bit = bit & bit;
    }
    bit
}

test_eq!(return_expr(), true);
