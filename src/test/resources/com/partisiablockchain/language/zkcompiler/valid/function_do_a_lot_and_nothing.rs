//! assert eval(0) => ()
//! assert eval(0,1,2) => ()
//! assert eval(0,1,2,3,4) => ()
//! assert eval(5,4,3,2,1) => ()
//! assert eval(39766,-117223,31886,90122,-1233) => ()
//! assert eval(5,4,3,2,1, 0, 0, 0, 0, 0, 0) => ()
use pbc_zk::{load_sbi, secret_variable_ids, Sbi32};

pub fn test() {
    test_2();
}

fn test_2() -> Sbi32 {
    let mut r = Sbi32::from(-1);
    for id in secret_variable_ids() {
        let arg = load_sbi::<Sbi32>(id);
        r = max(r, arg);
    }
    r
}

fn max(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x > y {
        x
    } else {
        y
    }
}
