//! assert eval() => 0xF000000150000F0000000F000000
use pbc_zk::*;

fn unsigned_mult_wrap() -> u128 {
    0x3_0000_0000_0000_3000u128 * 0x2_0000_5000_0000_5000u128
}
