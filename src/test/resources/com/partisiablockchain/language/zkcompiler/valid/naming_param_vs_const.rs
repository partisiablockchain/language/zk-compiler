//! assert eval(x) => x + 7
use pbc_zk::Sbi32;

const my_const: i32 = 12;

pub fn param_overlap(my_const: Sbi32) -> Sbi32 {
    my_const + Sbi32::from(7)
}
