//! assert eval(x) => x
use pbc_zk::Sbi32;

fn u8_constants(x: i32) -> Sbi32 {
    Sbi32::from(x)
}
