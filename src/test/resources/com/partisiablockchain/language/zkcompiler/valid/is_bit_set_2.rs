//! assert eval(0, 0) => 0
//! assert eval(1, 0) => 1
//! assert eval(1, 1) => 0
//! assert eval(7, 0) => 1
//! assert eval(7, 1) => 1
//! assert eval(7, 2) => 1
//! assert eval(8, 0) => 0
//! assert eval(8, 1) => 0
//! assert eval(8, 2) => 0
//! assert eval(8, 3) => 1
//! assert eval(-1, 0) => 1
//! assert eval(-1, 1) => 1
//! assert eval(-1, 2) => 1
//! assert eval(-1, 3) => 1
//! assert eval(-1, 4) => 1
//! assert eval(-1, 5) => 1
use pbc_zk::{Sbi32, Sbu1};

fn get_bit_mask(i: i32) -> Sbi32 {
    Sbi32::from(1 << i)
}

pub fn is_bit_set(x: Sbi32, idx: i32) -> Sbu1 {
    let bit_mask = get_bit_mask(idx);
    (bit_mask & x) != Sbi32::from(0)
}
