//! assert eval(0, 0) => 0
//! assert eval(0, 1) => 1
//! assert eval(1, 0) => 1
//! assert eval(1, 1) => 1
//! assert eval(999, 0) => 1
//! assert eval(0, 999) => 1
//! assert eval tests
use pbc_zk::*;

pub fn xor(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x != Sbi32::from(0) || y != Sbi32::from(0) {
        Sbi32::from(1)
    } else {
        Sbi32::from(0)
    }
}

test_eq!(xor(Sbi32::from(0), Sbi32::from(0)), 0);
test_eq!(xor(Sbi32::from(0), Sbi32::from(1)), 1);
test_eq!(xor(Sbi32::from(1), Sbi32::from(0)), 1);
test_eq!(xor(Sbi32::from(1), Sbi32::from(1)), 1);
test_eq!(xor(Sbi32::from(999), Sbi32::from(0)), 1);
test_eq!(xor(Sbi32::from(0), Sbi32::from(999)), 1);
test_eq!(xor(Sbi32::from(999), Sbi32::from(999)), 1);
