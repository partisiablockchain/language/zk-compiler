//! assert eval(x) => x
use pbc_zk::*;

pub fn identity(x: Sbi32) -> Sbi32 {
    Sbi32::from(1);
    x;
    x + x + Sbi32::from(3);
    x
}
