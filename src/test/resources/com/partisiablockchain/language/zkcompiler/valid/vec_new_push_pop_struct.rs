//! assert eval(x) => x
use pbc_zk::*;
use std::vec::Vec;

#[derive(SecretBinary)]
struct MyStruct {
    elem: Sbi32,
}

pub fn test(x: MyStruct) -> MyStruct {
    let vec: Vec<MyStruct> = Vec::new::<MyStruct>();
    Vec::push::<MyStruct>(vec, x);
    Vec::pop::<MyStruct>(vec)
}
