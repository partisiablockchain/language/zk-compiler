//! assert eval(x) => 0
use pbc_zk::*;

fn summing(limit: i32) -> i32 {
    let mut sum: i32 = 0;
    for x in (-limit)..limit + 1 {
        sum = sum + x;
    }
    sum
}
