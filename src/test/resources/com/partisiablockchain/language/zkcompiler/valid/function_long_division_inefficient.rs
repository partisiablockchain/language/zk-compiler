//! Long division implementation using some overly inefficient methods. Useful for testing certain
//! optimizations. See file `function_long_division` for a useful implementation.
//!
//! assert eval(0, 0) => 0
//! assert eval(0, x) => 0
//! assert eval(x, 1) => x
//! assert eval(0, 3) => 0
//! assert eval(1, 3) => 0
//! assert eval(2, 3) => 0
//! assert eval(3, 3) => 1
//! assert eval(4, 3) => 1
//! assert eval(5, 3) => 1
//! assert eval(6, 3) => 2
//! assert eval(7, 3) => 2
//! assert eval(8, 3) => 2
//! assert eval(9, 3) => 3

use pbc_zk::{Sbi32, Sbu1};

pub fn long_division(numerator: Sbi32, denominator: Sbi32) -> Sbi32 {
    let result = long_division_with_remainder(numerator, denominator);
    result.quotient
}

struct LongDivResult {
    quotient: Sbi32,
    remainder: Sbi32,
}

/// Classic division implementation.
///
/// Performs unsigned long division over the given numerator and denominator, rounding down towards
/// zero. Division by zero is defined as zero.
///
/// ## Arguments
///
/// - `numerator`: Numerator of division.
/// - `denominator`: Denominator of division. Assumed be positive.
///
/// ## Return
///
/// An instance of [`LongDivResult`] which contains both the quotient and the remainder.
///
/// Invariant:
/// ```
/// let result = long_division_with_remainder(x, y);
/// assert_eq!(x, result.quotient * y + result.remainder);
/// ```
fn long_division_with_remainder(numerator: Sbi32, denominator: Sbi32) -> LongDivResult {
    let div_by_zero = denominator == Sbi32::from(0);
    let mut quotient: Sbi32 = Sbi32::from(0);
    let mut remainder: Sbi32 = Sbi32::from(0);
    for i_0 in 0..32 {
        let i = 32 - 1 - i_0; // i in (n-1)..=0
        remainder = remainder << 1;
        remainder = replace_bit(remainder, 0, get_bit(numerator, i as usize));
        if remainder >= denominator {
            remainder = remainder - denominator;
            let one = Sbi32::from(1) as Sbu1;
            quotient = replace_bit(quotient, i as usize, one);
        }
    }
    LongDivResult {
        quotient: if div_by_zero {
            Sbi32::from(0)
        } else {
            quotient
        },
        remainder: remainder,
    }
}

fn replace_bit(input: Sbi32, index: usize, bit: Sbu1) -> Sbi32 {
    let mut bits = explode(input);
    bits[index] = bit;
    implode(bits)
}

fn get_bit(input: Sbi32, index: usize) -> Sbu1 {
    let exploded = explode(input);
    exploded[index]
}

fn explode(input: Sbi32) -> [Sbu1; 32] {
    let zero = Sbi32::from(0) as Sbu1;
    let mut out = [zero; 32];
    for i in 0..32 {
        out[i as usize] = ((input >> i) as Sbu1);
    }
    out
}

fn implode(input: [Sbu1; 32]) -> Sbi32 {
    let mut out = Sbi32::from(0);
    for i in 0..32 {
        out = out ^ ((input[i as usize] as Sbi32) << i);
    }
    out
}
