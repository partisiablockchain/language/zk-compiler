//! assert eval(0) => (0, 0)
//! assert eval(9) => (9, 9)
//! assert eval(123) => (123, 123)
//! assert eval tests
use pbc_zk::*;

pub fn zk_compute(a: Sbi32) -> Sbi32 {
    save_sbi::<Sbi32>(a);
    a
}

test_eq!(zk_compute(Sbi32::from(2)), 2, [0; 0], [2i32]);
test_eq!(zk_compute(Sbi32::from(42)), 42, [0; 0], [42i32]);
test_eq!(
    zk_compute(Sbi32::from(42)) == Sbi32::from(1),
    false,
    [0; 0],
    [42i32]
);
