//! assert eval(x) => x
use pbc_zk::*;

fn assignment(x: usize) -> usize {
    x
}
