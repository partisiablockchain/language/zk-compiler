//! assert eval(0, 2, 1, 3, 0, 2, 3, 0, 1, 5, 0, 6, 0, 1, 2, 0, 3, 0, 5, 6, 0, 3, 2, 6, 0, 3, 0, 7, 0, 3, 5, 2) => 0x000107_000106_000105_000004_000103_000102_000101_000100

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk;
use pbc_zk::*;

struct Input {
    value: Sbi8,
}

struct Counter {
    value: Sbi8,
    count: Sbi16,
}

pub fn sum_everything() -> [Counter; 8] {
    // Initialize state
    let mut counts: [Counter; 8] = [Counter {
        value: Sbi8::from(0),
        count: Sbi16::from(0),
    }; 8];
    for value in 0i8..8i8 {
        counts[value as usize].value = Sbi8::from(value);
    }

    // Count each variable
    for variable_id in secret_variable_ids() {
        let input = load_sbi::<Input>(variable_id);
        for value in 0i8..8i8 {
            if input.value == Sbi8::from(value) {
                counts[value as usize].count = Sbi16::from(1);
            }
        }
    }

    counts
}
