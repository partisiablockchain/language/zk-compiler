//! assert eval(x) => 0
use pbc_zk::*;

pub fn subtract(x: Sbi32) -> Sbi32 {
    x - x
}
