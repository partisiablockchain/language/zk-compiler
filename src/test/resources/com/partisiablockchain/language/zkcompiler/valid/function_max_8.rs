//! assert eval(0) => 0
//! assert eval(0,1,2) => 2
//! assert eval(0,1,2,3,4) => 4
//! assert eval(5,4,3,2,1) => 5
//! assert eval(39766,-117223,31886,90122,-1233) => 90122
//! assert eval(5,4,3,2,1, 0, 0, 0, 0, 0, 0) => 5
//! use function_lib_max
use crate::valid::function_lib_max::max;
use pbc_zk::{load_sbi, secret_variable_ids, Sbi32};

pub fn test() -> Sbi32 {
    let mut r = Sbi32::from(-1);
    for id in secret_variable_ids() {
        let arg = load_sbi::<Sbi32>(id);
        r = max(r, arg);
    }
    r
}
