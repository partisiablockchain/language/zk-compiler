//! assert eval(x) => 10000

use pbc_zk::Sbi32;

pub fn compute_55(max_idx: i32) -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(10000);
    for idx1 in 0..max_idx {
        // Note that this loop is identical to `for idx2 in 0..idx1`
        for idx2 in 0..((break, max_idx).1) {
            sum = sum + Sbi32::from(1);
        }
        sum = sum + Sbi32::from(100);
    }
    sum
}
