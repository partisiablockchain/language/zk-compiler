//! assert eval(0, 0) => 1
//! assert eval(3412311, 41231312) => 0
//! assert eval(3412311, 3412311) => 1
//! assert eval tests
use pbc_zk::*;

pub fn equal(v1: [Sbi32; 11], v2: [Sbi32; 11]) -> Sbu1 {
    v1 == v2
}

test_eq!(equal([Sbi32::from(0); 11], [Sbi32::from(0); 11]), true);
test_eq!(!equal([Sbi32::from(0); 11], [Sbi32::from(1); 11]), true);
