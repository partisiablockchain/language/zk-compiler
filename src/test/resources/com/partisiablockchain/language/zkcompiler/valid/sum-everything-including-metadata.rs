//! assert eval(99, 33, 55) => 374
//! assert eval(55, 99, 33) => 374
//! assert eval(33, 55, 99) => 374
//! assert eval(33, 99, 55) => 374
//! assert eval(55, 99, 55) => 418
//! assert eval(55, 55, 11) => 242
//! assert eval(11, 44, 44) => 198
//! assert eval(44, 11, 11) => 132
//! assert eval(1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1) => 162
//! assert eval(312, 4123, 532, 34, 324, 34224, 432) => 79962
//! assert eval(3020, 23130, 320) => 52940

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything() -> Sbi32 {
    // Initialize state
    let mut sum: Sbi32 = Sbi32::from(0);

    // Sum each variable
    for variable_id_raw in 1usize..(num_secret_variables() + 1usize) {
        let variable_id = SecretVarId::new(variable_id_raw);
        sum = sum + load_sbi::<Sbi32>(variable_id) + Sbi32::from(load_metadata::<i32>(variable_id));
    }

    sum
}
