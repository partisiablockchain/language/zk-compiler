//! assert eval(0x00_00_00_00_00_00_00_00) => 0
//! assert eval(0x00_01_00_00_03_00_00_08) => 12
//! assert eval(0x01_02_04_08_00_00_00_00) => 15
//! assert eval(0x01_02_04_08_08_04_02_01) => 30
use pbc_zk::Sbi32;

struct MyStruct {
    a: u8,
    b: u8,
}

pub fn compute(values: [MyStruct; 8]) -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx in 0usize..8usize {
        let v: MyStruct = values[idx];
        sum = sum + Sbi32::from(v.a as i32);
        sum = sum + Sbi32::from(v.b as i32);
    }
    sum
}
