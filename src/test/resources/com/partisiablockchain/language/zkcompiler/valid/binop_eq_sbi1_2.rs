//! assert eval(1, 0) => 0
//! assert eval(1, 1) => 1
//! assert eval(1, 2) => 2
//! assert eval(1, 99) => 99
//! assert eval(90, 99) => 99
//! assert eval(99, 99) => 99
//! assert eval(100, 99) => 99
//! assert eval tests
use pbc_zk::*;

pub fn add_self(x: Sbi32, y: Sbi32) -> Sbi32 {
    if ((x, x) == (y, y)) == ((x, x) == (y, y)) {
        y
    } else {
        x
    }
}

test_eq!(add_self(Sbi32::from(1), Sbi32::from(0)), 0);
test_eq!(add_self(Sbi32::from(1), Sbi32::from(1)), 1);
test_eq!(add_self(Sbi32::from(1), Sbi32::from(2)), 2);
test_eq!(add_self(Sbi32::from(1), Sbi32::from(99)), 99);
test_eq!(add_self(Sbi32::from(90), Sbi32::from(99)), 99);
test_eq!(add_self(Sbi32::from(99), Sbi32::from(99)), 99);
test_eq!(add_self(Sbi32::from(100), Sbi32::from(99)), 99);
