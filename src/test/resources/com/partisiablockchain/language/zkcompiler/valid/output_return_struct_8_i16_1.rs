//! assert eval() => 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF

struct MyPair {
    v1: i16,
    v2: i16,
    v3: i16,
    v4: i16,
    v5: i16,
    v6: i16,
    v7: i16,
    v8: i16,
}

pub fn big_constant() -> MyPair {
    MyPair {
        v1: (-1) as i16,
        v2: (-1) as i16,
        v3: (-1) as i16,
        v4: (-1) as i16,
        v5: (-1) as i16,
        v6: (-1) as i16,
        v7: (-1) as i16,
        v8: (-1) as i16,
    }
}
