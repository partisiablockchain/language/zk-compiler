//! assert eval(0, 0) => 0
//! assert eval(1, 0) => 1
//! assert eval(0, 1) => 1
//! assert eval(1, 1) => 2
//! assert eval(100, 100) => 200
//! assert eval(100, 200) => 44
//! assert eval(200, 100) => 44
//! assert eval(127, 127) => 254
//! assert eval(128, 128) => 0
//! assert eval(128, 129) => 1
//! assert eval(0, 255) => 255
//! assert eval(255, 255) => 254
//! assert eval tests
use pbc_zk::*;

pub fn add(x: u8, y: u8) -> i8 {
    (x as i8) + (y as i8)
}

test_eq!(add(0, 0), 0i8);
test_eq!(add(1, 0), 1i8);
test_eq!(add(0, 1), 1i8);
test_eq!(add(1, 1), 2i8);
test_eq!(add(100, 200), 44i8);
test_eq!(add(200, 100), 44i8);
