//! assert eval(1) => 0
//! assert eval(4) => 0
//! assert eval(5) => 1
//! assert eval(10) => 2
//! assert eval(11) => 2
use pbc_zk::*;

pub fn if_statement(x: Sbi32) -> Sbi32 {
    let mut output = Sbi32::from(2);
    if x < Sbi32::from(5) {
        output = Sbi32::from(0);
    } else if x < Sbi32::from(10) {
        output = Sbi32::from(1);
    }

    output
}
