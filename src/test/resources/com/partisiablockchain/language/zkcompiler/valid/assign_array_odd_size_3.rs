//! assert eval() => 0x050403020100
//! assert eval tests
//!
//! Tests array assignment with difficult index sizes.

use pbc_zk::{Sbi8, Sbu1};

#[derive(Copy, Clone)]
pub struct Tuple {
    v1: Sbi8,
    v2: Sbi8,
    v3: Sbi8,
}

pub fn sum_everything() -> [Tuple; 8] {
    // Initialize state
    let mut counts: [Tuple; 8] = [Tuple {
        v1: Sbi8::from(0),
        v2: Sbi8::from(0),
        v3: Sbi8::from(0),
    }; 8];
    counts[0].v1 = Sbi8::from(0);
    counts[0].v2 = Sbi8::from(1);
    counts[0].v3 = Sbi8::from(2);
    counts[1].v1 = Sbi8::from(3);
    counts[1].v2 = Sbi8::from(4);
    counts[1].v3 = Sbi8::from(5);
    counts
}

fn test_index(index: usize, v1: i8, v2: i8, v3: i8) -> Sbu1 {
    let counts = sum_everything();
    counts[index].v1 == Sbi8::from(v1)
        && counts[index].v2 == Sbi8::from(v2)
        && counts[index].v3 == Sbi8::from(v3)
}

pbc_zk::test_eq!(test_index(0, 0, 1, 2), true);
pbc_zk::test_eq!(test_index(1, 3, 4, 5), true);
pbc_zk::test_eq!(test_index(2, 0, 0, 0), true);
pbc_zk::test_eq!(test_index(3, 0, 0, 0), true);
pbc_zk::test_eq!(test_index(4, 0, 0, 0), true);
pbc_zk::test_eq!(test_index(5, 0, 0, 0), true);
pbc_zk::test_eq!(test_index(6, 0, 0, 0), true);
pbc_zk::test_eq!(test_index(7, 0, 0, 0), true);
