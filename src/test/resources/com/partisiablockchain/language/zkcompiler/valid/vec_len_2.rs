//! assert eval(x) => 5
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> u32 {
    let mut vec: Vec<Sbi32> = Vec::new::<Sbi32>();
    vec.push(x);
    vec.push(x);
    vec.push(Sbi32::from(2));
    vec.push(x);
    vec.push(Sbi32::from(5));

    // Length
    vec.len()
}
