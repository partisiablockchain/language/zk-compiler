//! assert eval(0) => 54
//! assert eval(1) => 74

use pbc_zk::Sbi32;

pub fn circuit_1(b: bool) -> Sbi32 {
    if b == false {
        Sbi32::from(54)
    } else if b == true {
        Sbi32::from(74)
    } else {
        Sbi32::from(312)
    }
}
