//! assert eval() => 0x80

pub fn not_literal() -> u8 {
    !0x7fu8
}
