//! assert eval(0) => 0
//! assert eval(1) => 1
use pbc_zk::Sbu1;

pub fn true_sbi(x: u1) -> Sbu1 {
    Sbu1::from(1) == Sbu1::from(x)
}
