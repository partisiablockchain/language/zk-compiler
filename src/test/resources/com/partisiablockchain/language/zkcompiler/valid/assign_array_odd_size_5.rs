//! assert eval(0x_0fff_21_0fff_10) => (0x10, 0x0fff, 0x21, 0x0fff)
//!
//! Tests array assignment with difficult index sizes.

use pbc_zk::*;

struct Counter {
    value: Sbi8,
    count: Sbi16,
}

pub fn sum_everything(c: [Counter; 2]) -> (Sbi8, Sbi16, Sbi8, Sbi16) {
    (c[0].value, c[0].count, c[1].value, c[1].count)
}
