//! assert eval(0, 0) => (0, 0)
use pbc_zk::{load_sbi, save_sbi, secret_variable_ids, Sbi32};

#[repr(C)]
#[derive(SecretBinary)]
struct Balance {
    id: Sbi32,
    balance: Sbi32,
}

pub fn main(id_to_update: Sbi32, amount: Sbi32) {
    for var_id in secret_variable_ids() {
        // Fetch current balance
        let mut balance = load_sbi::<Balance>(var_id);
        // Determine new balance
        if balance.id == id_to_update {
            balance = Balance {
                id: balance.id,
                balance: balance.balance + amount,
            }
        };
        // Save updated balance
        save_sbi::<Balance>(balance);
    }
}
