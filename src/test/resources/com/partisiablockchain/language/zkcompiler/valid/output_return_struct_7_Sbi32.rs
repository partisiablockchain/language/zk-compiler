//! assert eval() => 0xFFFFFFFF_FFFFFFFF

use pbc_zk::Sbi32;

#[derive(Secret)]
struct MyPair {
    v1: Sbi32,
    v2: Sbi32,
}

pub fn big_constant() -> MyPair {
    MyPair {
        v1: Sbi32::from(-1),
        v2: Sbi32::from(-1),
    }
}
