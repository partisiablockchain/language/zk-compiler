//! assert eval(0, 2, 1, 3, 0, 2, 3, 0, 1, 5, 0, 6, 0, 1, 2, 0, 3, 0, 5, 6, 0, 3, 2, 6, 0, 3, 0, 7, 0, 3, 5, 2) => 0x01_03_03_00_06_05_03_0b
//! assert eval tests

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything() -> [Sbi8; 8] {
    // Initialize state
    let mut counts: [Sbi8; 8] = [Sbi8::from(0); 8];

    // Count each variable
    for variable_id in secret_variable_ids() {
        let var = load_sbi::<Sbi8>(variable_id);
        for value in 0i8..8i8 {
            let idx = value as usize;
            if var == Sbi8::from(value) {
                counts[idx] = counts[idx] + Sbi8::from(1);
            }
        }
    }

    counts
}

test_eq!(
    sum_everything()
        == [
            Sbi8::from(0x0b),
            Sbi8::from(0x03),
            Sbi8::from(0x05),
            Sbi8::from(0x06),
            Sbi8::from(0x00),
            Sbi8::from(0x03),
            Sbi8::from(0x03),
            Sbi8::from(0x01)
        ],
    true,
    [
        0i8, 2i8, 1i8, 3i8, 0i8, 2i8, 3i8, 0i8, 1i8, 5i8, 0i8, 6i8, 0i8, 1i8, 2i8, 0i8, 3i8, 0i8,
        5i8, 6i8, 0i8, 3i8, 2i8, 6i8, 0i8, 3i8, 0i8, 7i8, 0i8, 3i8, 5i8, 2i8
    ]
);
