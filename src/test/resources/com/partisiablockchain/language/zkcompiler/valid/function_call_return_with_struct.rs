//! assert eval(x) => x
use pbc_zk::*;

struct Point {
    x: Sbi32,
    y: Sbi32,
}

pub fn main(x: Sbi32) -> Sbi32 {
    let p = line(x);
    p.x
}

fn line(x: Sbi32) -> Point {
    Point { x: x, y: x }
}
