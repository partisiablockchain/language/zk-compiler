//! assert eval(0x44_66) => 0x44_09
//! assert eval(0x44_45) => 0x44_09
//! assert eval(0x73_45) => 0x73_09

use pbc_zk::Sbi8;

struct MyPair {
    v1: Sbi8,
    v2: Sbi8,
}

pub fn identity(pair: MyPair) -> MyPair {
    MyPair {
        v1: Sbi8::from(9),
        v2: pair.v2,
    }
}
