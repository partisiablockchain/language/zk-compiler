//! assert eval(x) => x + 23

type mytype = i64;

const MY_CONST: mytype = 23;

fn plus_const(x: mytype) -> mytype {
    x + MY_CONST
}
