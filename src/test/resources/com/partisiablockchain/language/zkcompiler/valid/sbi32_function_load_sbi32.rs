//! assert eval(x) => x
//! assert eval(16777217) => 16777217
//! assert eval(0x12_00_00_34) => 0x12_00_00_34
//! assert eval tests
use pbc_zk::{load_sbi, Sbi32, SecretVarId};

pub fn sbi() -> Sbi32 {
    let id = SecretVarId::new(1);
    load_sbi::<Sbi32>(id)
}

pbc_zk::test_eq!(sbi(), 3, [3i32]);
pbc_zk::test_eq!(sbi(), 6, [6i32]);
//pbc_zk::test_eq!(sbi(), -2, [-2i32]);
pbc_zk::test_eq!(sbi(), 42, [42i32]);
pbc_zk::test_eq!(sbi(), 16777217, [16777217i32]);
pbc_zk::test_eq!(sbi(), 0x12_00_00_34, [0x12_00_00_34i32]);
