//! assert eval(x) => x
//! assert eval(16777217) => 16777217
//! assert eval(0x12_00_00_34) => 0x12_00_00_34
use pbc_zk::*;

type SbiN = Sbi128;

pub fn identity(x: SbiN) -> SbiN {
    Sbi128::from_le_bits(x.to_le_bits())
}
