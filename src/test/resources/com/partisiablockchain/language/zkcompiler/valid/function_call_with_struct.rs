//! assert eval(x) => x
use pbc_zk::*;

struct Point {
    x: Sbi32,
    y: Sbi32,
}

pub fn main(p: Point) -> Sbi32 {
    extract_x(p)
}

fn extract_x(p: Point) -> Sbi32 {
    p.x
}
