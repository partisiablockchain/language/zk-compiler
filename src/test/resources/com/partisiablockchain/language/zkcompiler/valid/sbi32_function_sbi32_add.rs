//! assert eval(x) => x * 2
use pbc_zk::*;

pub fn sbi() -> Sbi32 {
    let id = SecretVarId::new(1);
    load_sbi::<Sbi32>(id) + load_sbi::<Sbi32>(id)
}
