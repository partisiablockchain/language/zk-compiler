//! assert eval() => 0x0c_09

use pbc_zk::Sbi8;

struct MyPair {
    v1: Sbi8,
    v2: Sbi8,
}

pub fn identity() -> MyPair {
    MyPair {
        v1: Sbi8::from(9),
        v2: Sbi8::from(12),
    }
}
