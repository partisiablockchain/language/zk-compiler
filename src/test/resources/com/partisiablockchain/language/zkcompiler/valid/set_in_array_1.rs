//! assert eval(x) => 0xff

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything(mut counts: [Sbu1; 8]) -> [Sbu1; 8] {
    // Count each variable
    for idx in 0usize..8usize {
        counts[idx] = (Sbi8::from(1i8) as Sbu1);
    }

    counts
}
