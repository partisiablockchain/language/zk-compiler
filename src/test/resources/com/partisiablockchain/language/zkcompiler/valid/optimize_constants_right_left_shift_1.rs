//! assert eval(x) => 16
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> i32 {
    ((6 * 3) >> 2) << 2
}
