//! assert eval(0x01_10) => 0x01_04
//! assert eval(0x02_12) => 0x02_04
//! assert eval(0x03_41) => 0x03_04
//! assert eval(0x04_23) => 0x04_04
//! assert eval tests
use pbc_zk::*;

#[derive(SecretBinary, Copy, Clone)]
pub struct Obj {
    f1: Sbi8,
    f2: Sbi8,
}

pub fn assignment(mut y: Obj) -> Obj {
    y.f1 = Sbi8::from(4);
    y
}

fn test_assignment(x: i8, y: i8) -> Sbu1 {
    let obj1: Obj = Obj {
        f1: Sbi8::from(x),
        f2: Sbi8::from(y),
    };
    let obj2 = assignment(obj1);
    obj2.f1 == Sbi8::from(4) && obj1.f2 == obj2.f2
}

pbc_zk::test_eq!(test_assignment(10, 1), true);
pbc_zk::test_eq!(test_assignment(12, 2), true);
pbc_zk::test_eq!(test_assignment(41, 3), true);
pbc_zk::test_eq!(test_assignment(23, 4), true);
