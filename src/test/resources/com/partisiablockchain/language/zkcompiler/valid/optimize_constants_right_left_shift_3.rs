//! assert eval(x) => 0

pub fn circuit_1(x: i32) -> i32 {
    (x >> 32) << 32
}
