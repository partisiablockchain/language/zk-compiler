//! assert eval(x) => x
use pbc_zk::*;

pub fn g(x: Sbi32) -> Sbi32 {
    f(x)
}

fn f(x: Sbi32) -> Sbi32 {
    x
}
