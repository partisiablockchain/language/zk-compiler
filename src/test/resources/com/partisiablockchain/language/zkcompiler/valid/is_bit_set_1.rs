//! assert eval(0, 0) => 0
//! assert eval(1, 0) => 1
//! assert eval(1, 1) => 0
//! assert eval(7, 0) => 1
//! assert eval(7, 1) => 1
//! assert eval(7, 2) => 1
//! assert eval(8, 0) => 0
//! assert eval(8, 1) => 0
//! assert eval(8, 2) => 0
//! assert eval(8, 3) => 1
//! assert eval(-1, 0) => 1
//! assert eval(-1, 1) => 1
//! assert eval(-1, 2) => 1
//! assert eval(-1, 3) => 1
//! assert eval(-1, 4) => 1
//! assert eval(-1, 5) => 1
use pbc_zk::{Sbu1, Sbu32};

fn is_bit_set(x: Sbu32, idx: usize) -> Sbu1 {
    let bit_mask = Sbu32::from(1 << idx);
    (bit_mask & x) != Sbu32::from(0)
}
