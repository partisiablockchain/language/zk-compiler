//! assert eval(x) => 15
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    Sbi32::from(1) + Sbi32::from(2) + Sbi32::from(3) + Sbi32::from(4) + Sbi32::from(5)
}
