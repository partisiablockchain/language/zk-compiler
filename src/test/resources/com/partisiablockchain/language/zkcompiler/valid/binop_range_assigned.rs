//! assert eval(x) => x
//! assert eval tests
use pbc_zk::*;

pub fn range_expr() -> Sbi32 {
    let r = 1..100;
    for i in r {
        return load_sbi::<Sbi32>(SecretVarId::new(1));
    }
    Sbi32::from(0)
}

test_eq!(range_expr(), 1, [1i32]);
test_eq!(range_expr(), 2, [2i32]);
test_eq!(range_expr(), 3, [3i32]);
test_eq!(range_expr(), 42, [42i32]);
test_eq!(range_expr(), 95, [95i32]);
