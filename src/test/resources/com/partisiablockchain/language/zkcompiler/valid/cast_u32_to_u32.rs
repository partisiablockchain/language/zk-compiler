//! assert eval(x) => x
pub fn cast(v1: u32) -> u32 {
    v1 as u32
}
