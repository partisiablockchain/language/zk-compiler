//! assert eval tests
use pbc_zk::*;

pub fn test_binop(x: u32, y: u32) -> bool {
    x < y
}

test_eq!(test_binop(1, 0), false);
test_eq!(test_binop(1, 1), false);
test_eq!(test_binop(1, 2), true);
test_eq!(test_binop(1, 99), true);
test_eq!(test_binop(90, 99), true);
test_eq!(test_binop(99, 99), false);
test_eq!(test_binop(100, 99), false);
test_eq!(test_binop(0xFFFFFFF, 0), false);
test_eq!(test_binop(0, 0xFFFFFFF), true);
test_eq!(test_binop(0x7FFFFFFF, 0), false);
test_eq!(test_binop(0, 0x7FFFFFFF), true);
test_eq!(test_binop(0x80000000, 0), false);
test_eq!(test_binop(0, 0x80000000), true);
test_eq!(test_binop(0xFFFFFFFF, 0), false);
test_eq!(test_binop(0, 0xFFFFFFFF), true);
