//! assert eval(1) => 1
//! assert eval(1,2,3) => 3
//! assert eval(9,9,9,9,9,9,9,9,9,9) => 10
use pbc_zk::*;

pub fn sbi() -> Sbi32 {
    Sbi32::from(num_secret_variables() as i32)
}
