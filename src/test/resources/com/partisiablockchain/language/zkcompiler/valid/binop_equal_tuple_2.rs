//! assert eval(x) => 1
//! assert eval tests
use pbc_zk::*;

pub fn double_identity(v1: Sbi32) -> Sbi32 {
    if (v1, v1) == (v1, v1) {
        Sbi32::from(1)
    } else {
        Sbi32::from(0)
    }
}

test_eq!(double_identity(Sbi32::from(1)), 1);
test_eq!(double_identity(Sbi32::from(2)), 1);
test_eq!(double_identity(Sbi32::from(3)), 1);
test_eq!(double_identity(Sbi32::from(42)), 1);
test_eq!(double_identity(Sbi32::from(95)), 1);
