//! assert eval(0x01) => (0x01_01, 0x01_01)
//! assert eval(0x09) => (0x09_09, 0x09_09)

use pbc_zk::save_sbi;
use pbc_zk::Sbi8;

struct MyStruct {
    hidden_1: (),
    a: Sbi8,
    hidden_2: (),
    b: Sbi8,
    hidden_3: ((), (), ((), (), ()), ()),
}

pub fn identity(x: Sbi8) {
    let v = MyStruct {
        a: x,
        b: x,
        hidden_1: (),
        hidden_2: (),
        hidden_3: ((), (), ((), (), ()), ()),
    };
    save_sbi::<MyStruct>(v);
    save_sbi::<MyStruct>(v);
}
