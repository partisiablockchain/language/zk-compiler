//! assert eval(0) => 0
//! assert eval(1) => 0
//! assert eval(2) => 2
//! assert eval(10) => 10
//! assert eval(1337) => 1337
use pbc_zk::*;

struct MyStruct {
    a: Sbi32,
}

pub fn almost_identity(secret: Sbi32) -> MyStruct {
    let mut value: MyStruct = MyStruct { a: secret };
    if secret == Sbi32::from(1) {
        value = MyStruct { a: Sbi32::from(0) };
    }
    value
}
