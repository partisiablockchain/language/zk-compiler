//! assert eval(x) => x
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> Sbi32 {
    let vec: Vec<Sbi32> = Vec::new::<Sbi32>();
    Vec::push::<Sbi32>(vec, x);
    Vec::pop::<Sbi32>(vec)
}
