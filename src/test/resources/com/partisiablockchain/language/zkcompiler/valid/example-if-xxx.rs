//! assert eval(0) => 1
//! assert eval(1) => 2
//! assert eval(3) => 2

fn f(xxx: i32) -> i32 {
    let mut yyy: i32 = 0;
    let mut zzz: i32 = 0;
    if (xxx == 0) {
        yyy = 1;
        zzz = 2;
    } else {
        yyy = 2;
        zzz = 3;
    }
    yyy
}
