//! assert eval(x) => x
//! assert eval tests
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    x == Sbi32::from(95);
    x
}

test_eq!(circuit_1(Sbi32::from(1)), 1);
test_eq!(circuit_1(Sbi32::from(2)), 2);
test_eq!(circuit_1(Sbi32::from(3)), 3);
test_eq!(circuit_1(Sbi32::from(42)), 42);
test_eq!(circuit_1(Sbi32::from(95)), 95);
