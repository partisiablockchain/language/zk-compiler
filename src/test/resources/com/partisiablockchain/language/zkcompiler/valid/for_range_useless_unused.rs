//! assert eval() => 666

use pbc_zk::*;

pub fn compute_55() -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(666);
    for idx in 0..0 {
        sum = Sbi32::from(999);
    }
    sum
}
