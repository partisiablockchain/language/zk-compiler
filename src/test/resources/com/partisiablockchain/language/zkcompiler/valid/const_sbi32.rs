//! assert eval(x) => x + 13211

use pbc_zk::Sbi32;

const MY_CONST: i32 = 13211;

fn do_something(x: Sbi32) -> Sbi32 {
    x + Sbi32::from(MY_CONST)
}
