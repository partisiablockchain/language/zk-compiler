//! assert eval(x) => 1

use pbc_zk::{Sbi32, Sbu1};

pub fn tautologic(x: Sbi32) -> Sbu1 {
    // Produce x
    let and: Sbi32 = x & x;
    let or: Sbi32 = x | x;

    // Produce zero
    let xor: Sbi32 = x ^ x;
    let lt: Sbu1 = x < x;
    let gt: Sbu1 = x > x;

    // Produce one
    let eq: Sbu1 = x == x;
    let lte: Sbu1 = x <= x;
    let gte: Sbu1 = x >= x;

    // Produce other
    let add: Sbi32 = x + x;

    (and == or && or == x)
        && (xor == Sbi32::from(0))
        && (!lt && !gt)
        && (eq && lte && gte)
        && (add == x << 1)
}
