//! assert eval(0) => 0
//! assert eval(9) => 9
//! assert eval(21) => 21
//! assert eval(112) => 112
use pbc_zk::*;

pub fn try_cast(v1: Sbi8) -> Sbi32 {
    v1 as Sbi32
}
