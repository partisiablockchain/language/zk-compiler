//! assert eval(x) => x + 2
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> Sbi32 {
    let vec: Vec<Sbi32> = Vec::new::<Sbi32>();
    Vec::push::<Sbi32>(vec, x);
    Vec::push::<Sbi32>(vec, Sbi32::from(2));
    Vec::pop::<Sbi32>(vec) + Vec::pop::<Sbi32>(vec)
}
