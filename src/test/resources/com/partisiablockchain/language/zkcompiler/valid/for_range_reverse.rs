//! assert eval(x) => 0
//! assert ignore

use std::ops::Range;

fn count(x: i32) -> i32 {
    let mut res = 0;
    for _i in x..0 {
        res = res + 1;
    }
    res
}
