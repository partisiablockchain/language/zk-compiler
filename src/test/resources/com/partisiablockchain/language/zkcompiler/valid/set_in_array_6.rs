//! assert eval() => 0x00_ff_00

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything() -> [Sbi8; 3] {
    let mut counts: [Sbi8; 3] = [Sbi8::from(0xffu8 as i8); 3];
    counts[0] = Sbi8::from(0x00i8);
    counts[2] = Sbi8::from(0x00i8);
    counts
}
