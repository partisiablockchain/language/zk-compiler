//! assert eval(3) => 3
//! assert eval(0) => 0
//! assert eval(1) => 1

use std::cmp::Ordering;

fn identity(x: Ordering) -> Ordering {
    x
}
