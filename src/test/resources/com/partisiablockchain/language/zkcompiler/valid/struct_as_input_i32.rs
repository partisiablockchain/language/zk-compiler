//! assert eval(0x00000001_00000009) => 9
//! assert eval(0x00000009_00000001) => 1

struct MyPair {
    v1: i32,
    v2: i32,
}

pub fn identity(pair: MyPair) -> i32 {
    pair.v1
}
