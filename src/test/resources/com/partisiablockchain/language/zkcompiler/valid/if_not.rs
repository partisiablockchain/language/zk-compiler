//! assert eval(0) => 1337
//! assert eval(1) => 0
use pbc_zk::*;

pub fn if_expr(b: Sbu1) -> Sbi32 {
    if !b {
        Sbi32::from(1337)
    } else {
        Sbi32::from(0)
    }
}
