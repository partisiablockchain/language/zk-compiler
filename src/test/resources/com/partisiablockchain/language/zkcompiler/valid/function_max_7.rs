//! assert eval(0,1,2,3,4) => 4
//! assert eval(5,4,3,2,1) => 5
//! assert eval(39766,-117223,31886,90122,-1233) => 90122
//! use function_lib_max
use crate::valid::function_lib_max::max;
use pbc_zk::{Sbi32, Sbu1};

pub fn test(i1: Sbi32, i2: Sbi32, i3: Sbi32, i4: Sbi32, i5: Sbi32) -> Sbi32 {
    let mut r = max(i1, i2);
    r = max(r, i3);
    r = max(r, i4);
    r = max(r, i5);
    r
}
