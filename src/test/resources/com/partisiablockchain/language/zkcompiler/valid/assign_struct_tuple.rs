//! assert eval(0x01_11_10) => 0x01_01_04
//! assert eval(0x02_31_12) => 0x02_01_04
//! assert eval(0x03_81_41) => 0x03_01_04
//! assert eval(0x04_f1_23) => 0x04_01_04
use pbc_zk::*;

#[derive(SecretBinary)]
struct Obj {
    f1: (Sbi8, Sbi8),
    f2: Sbi8,
}

pub fn assignment(mut y: Obj) -> Obj {
    y.f1 = (Sbi8::from(4), Sbi8::from(1));
    y
}
