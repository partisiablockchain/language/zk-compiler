//! assert eval() => 0x7FFF_ABCD_0123_3210_1337_0420_3141_2718
use pbc_zk::*;

pub fn constant() -> Sbi128 {
    Sbi128::from(0x7FFF_ABCD_0123_3210_1337_0420_3141_2718i128)
}
