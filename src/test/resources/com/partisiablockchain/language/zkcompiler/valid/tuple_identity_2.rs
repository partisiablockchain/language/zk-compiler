//! assert eval(0) => (0, 0)
//! assert eval(9) => (9, 9)
//! assert eval(11) => (11, 11)
//! assert eval(1321) => (1321, 1321)
use pbc_zk::*;

pub fn double_identity(x: Sbi32) -> (Sbi32, Sbi32) {
    (x, x)
}
