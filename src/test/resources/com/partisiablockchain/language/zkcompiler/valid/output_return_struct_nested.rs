//! assert eval(0x01) => 0x01_01_01
//! assert eval(0x09) => 0x09_09_09
//! assert eval(0x6f) => 0x6f_6f_6f
//! assert eval(0xff) => 0xff_ff_ff

use pbc_zk::Sbi8;

struct MyPair {
    v1: Sbi8,
    v2: MyOtherPair,
}

struct MyOtherPair {
    v1: Sbi8,
    v2: Sbi8,
}

pub fn something(a: Sbi8) -> MyPair {
    MyPair {
        v1: a,
        v2: MyOtherPair { v1: a, v2: a },
    }
}
