//! assert eval(x) => x

use pbc_zk;

fn identity(x: pbc_zk::Sbi32) -> pbc_zk::Sbi32 {
    x
}
