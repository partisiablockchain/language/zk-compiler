//! assert eval(x) => x*2 + 6
//! assert eval tests
use pbc_zk::*;

pub fn add_self(x: Sbi32) -> Sbi32 {
    (x + Sbi32::from(3)) + (Sbi32::from(3) + x)
}

test_eq!(add_self(Sbi32::from(4)), 14);
test_eq!(add_self(Sbi32::from(10)), 26);
