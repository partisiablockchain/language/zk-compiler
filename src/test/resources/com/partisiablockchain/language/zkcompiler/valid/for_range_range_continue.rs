//! assert eval(1) => 0
//! assert eval(2) => 1
//! assert eval(3) => 6
//! assert eval(4) => 18

use pbc_zk::Sbi32;

pub fn compute_55(max_idx: i32) -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx1 in 0..max_idx {
        for idx2 in 0..max_idx {
            if idx1 == idx2 {
                continue;
            }
            sum = sum + Sbi32::from(idx2);
        }
    }
    sum
}
