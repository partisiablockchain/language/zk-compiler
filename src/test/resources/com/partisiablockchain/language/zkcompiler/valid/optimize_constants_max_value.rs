//! assert eval() => 2147483648

pub fn circuit_1() -> u64 {
    0x7F_FF_FF_FFu64 + 1u64
}
