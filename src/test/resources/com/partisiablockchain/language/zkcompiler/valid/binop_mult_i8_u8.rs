//! assert eval(0) => (0, 0)
//! assert eval(1) => (1, 1)
//! assert eval(2) => (4, 4)
//! assert eval(254) => (4, 4)
//! assert eval(255) => (1, 1)
use pbc_zk::*;

pub fn mult_self(x: u8) -> (i8, u8) {
    let signed: i8 = (x as i8) * (x as i8);
    let unsigned: u8 = x * x;
    (signed, unsigned)
}
