//! assert eval() => 13594154

pub fn infer_block() -> i128 {
    ({ 12995995i128 }) + { 598159 }
}
