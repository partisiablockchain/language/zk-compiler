//! assert eval(x) => 3
use pbc_zk::Sbi32;

fn what(x: Sbi32) -> Sbi32 {
    if x == Sbi32::from(3) {
        x
    } else {
        Sbi32::from(3)
    }
}
