//! assert eval() => 0
use pbc_zk::Sbi1;

pub fn false_sbi() -> Sbi1 {
    Sbi1::from(0i1)
}
