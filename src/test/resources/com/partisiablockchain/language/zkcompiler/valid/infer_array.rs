//! assert eval() => 0x08060402

pub fn infer_array() -> [i8; 4] {
    [2, 4, 6i8, 8]
}
