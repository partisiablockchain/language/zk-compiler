//! assert eval(x) => x
use pbc_zk::Sbi32;

pub fn extract_one(x: Sbi32) -> Sbi32 {
    let arr = [x, x, x, x];
    arr[1]
}
