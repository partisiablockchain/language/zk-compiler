//! assert eval(9, 4) => 2
//! assert eval(9, 9) => 2
//! assert eval(0, 0) => 2
//! assert eval(12, 1) => 1
//! assert eval(99, 0) => 1
//! assert eval(9, 21) => 1
//! assert eval(1, 10) => 1
//! assert eval(10, 10) => 0
//! assert eval(99, 21) => 0
use pbc_zk::*;

fn count_small(x: Sbi32, y: Sbi32) -> Sbi32 {
    let mut z: Sbi32 = Sbi32::from(0);
    if x < Sbi32::from(10) {
        z = z + Sbi32::from(1);
    }
    if y < Sbi32::from(10) {
        z = z + Sbi32::from(1);
    }
    z
}
