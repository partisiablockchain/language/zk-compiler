//! assert eval(0x00000001_00000009) => 0x00000001_00000009
//! assert eval(0x00000009_00000001) => 0x00000009_00000001
//! assert eval(x) => x

use pbc_zk::Sbi32;

struct MyPair {
    v1: Sbi32,
    v2: Sbi32,
}

pub fn identity(pair: MyPair) -> MyPair {
    return pair;
    pair
}
