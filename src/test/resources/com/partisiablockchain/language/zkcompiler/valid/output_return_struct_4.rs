//! assert eval(0x44_66) => 0x09_66
//! assert eval(0x44_45) => 0x09_45

use pbc_zk::Sbi8;

#[derive(Secret)]
struct MyPair {
    v1: Sbi8,
    v2: Sbi8,
}

pub fn identity(pair: MyPair) -> MyPair {
    MyPair {
        v1: pair.v1,
        v2: Sbi8::from(9),
    }
}
