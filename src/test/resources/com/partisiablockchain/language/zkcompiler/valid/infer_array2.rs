//! assert eval() => 0x08060302

pub fn infer_array() -> [i8; 4] {
    let mut a = [2, 4, 6i8, 8];
    a[1] = 3;
    a
}
