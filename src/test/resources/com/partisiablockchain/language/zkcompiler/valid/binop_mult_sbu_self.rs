//! assert eval(0) => 0
//! assert eval(1) => 1
//! assert eval(2) => 4
//! assert eval(3) => 9
//! assert eval(8) => 64
//! assert eval(10) => 100
//! assert eval(100) => 10000
//! assert eval tests
use pbc_zk::*;

pub fn mult_self(x: Sbu32) -> Sbu32 {
    x * x
}

test_eq!(mult_self(Sbu32::from(0)), 0);
test_eq!(mult_self(Sbu32::from(1)), 1);
test_eq!(mult_self(Sbu32::from(2)), 4);
test_eq!(mult_self(Sbu32::from(3)), 9);
test_eq!(mult_self(Sbu32::from(8)), 64);
test_eq!(mult_self(Sbu32::from(10)), 100);
test_eq!(mult_self(Sbu32::from(100)), 10000);
