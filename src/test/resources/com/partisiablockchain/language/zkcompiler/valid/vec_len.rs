//! assert eval(x) => 5
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> u32 {
    let mut vec: Vec<Sbi32> = Vec::new::<Sbi32>();
    Vec::push::<Sbi32>(vec, x);
    Vec::push::<Sbi32>(vec, Sbi32::from(2));
    Vec::push::<Sbi32>(vec, x);
    Vec::push::<Sbi32>(vec, x);
    Vec::push::<Sbi32>(vec, Sbi32::from(5));

    // Length
    Vec::len::<Sbi32>(vec)
}
