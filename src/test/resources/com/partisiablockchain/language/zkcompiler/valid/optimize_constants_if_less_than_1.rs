//! assert eval(x) => 15
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    let c: i32 = (6 * 3) >> 1;
    if c < 9 {
        x
    } else {
        Sbi32::from(15)
    }
}
