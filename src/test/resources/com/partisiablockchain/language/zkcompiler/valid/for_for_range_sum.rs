//! assert eval() => 165

use pbc_zk::Sbi32;

pub fn compute() -> Sbi32 {
    let mut sum = Sbi32::from(0);
    for idx1 in 0..11 {
        for idx2 in 0..idx1 {
            sum = sum + Sbi32::from(idx2);
        }
    }
    sum
}
