//! assert eval(0) => 0
//! assert eval(1) => 0
//! assert eval(2) => 0
//! assert eval(94) => 0
//! assert eval(95) => 1
//! assert eval(96) => 0
//! assert eval(1337) => 0
//! assert eval tests
use pbc_zk::*;

pub fn circuit_1(x: i32) -> bool {
    x == 95
}

test_eq!(circuit_1(0), false);
test_eq!(circuit_1(1), false);
test_eq!(circuit_1(2), false);
test_eq!(circuit_1(94), false);
test_eq!(circuit_1(95), true);
test_eq!(circuit_1(96), false);
test_eq!(circuit_1(1337), false);
