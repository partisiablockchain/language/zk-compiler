//! assert eval(0) => 2
//! assert eval(30) => 2
//! assert eval(39) => 2
//! assert eval(40) => 1
//! assert eval(100) => 1
use pbc_zk::Sbi32;

pub fn circuit_1(x: i32) -> Sbi32 {
    let t: Sbi32 = Sbi32::from(1);
    let f: Sbi32 = Sbi32::from(0);
    if x < 40 {
        t + Sbi32::from(1)
    } else {
        f + Sbi32::from(1)
    }
}
