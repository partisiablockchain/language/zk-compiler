//! assert ignore
//!
//! This is a small library with simple max and min function implementations.
use pbc_zk::Sbi32;

fn max(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x > y {
        x
    } else {
        y
    }
}

fn min(x: Sbi32, y: Sbi32) -> Sbi32 {
    if x > y {
        y
    } else {
        x
    }
}
