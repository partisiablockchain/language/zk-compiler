//! assert eval(x) => x * 2 + 3

struct MyPair {
    v1: Sbi32,
    v2: Sbi32,
}

use pbc_zk::*;

pub fn something(a: Sbi32) -> Sbi32 {
    let mut x: Sbi32 = a;
    let pair: MyPair = MyPair {
        v2: {
            x = x + Sbi32::from(3);
            a
        },
        v1: x,
    };
    pair.v1 + pair.v2
}
