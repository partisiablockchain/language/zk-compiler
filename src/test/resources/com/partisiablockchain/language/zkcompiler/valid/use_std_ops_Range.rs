//! assert eval(x) => x

use std::ops::Range;

fn identity(x: Range<u16>) -> Range<u16> {
    x
}
