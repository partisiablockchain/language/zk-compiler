//! assert eval(x) => x
use pbc_zk::*;

pub fn func_call(x: Sbi32) -> Sbi32 {
    identity(x)
}

fn identity(x: Sbi32) -> Sbi32 {
    x
}
