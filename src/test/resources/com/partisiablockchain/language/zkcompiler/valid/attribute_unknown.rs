//! assert eval(x) => x

#[some_unknown_attribute]
use pbc_zk::{save_sbi, Sbi32, Sbu1};

pub fn main(val: Sbi32) {
    save_1(val);
}

#[some_other_unknown_attribute]
fn save_1(val: Sbi32) {
    save_2(val)
}

#[what]
fn save_2(val: Sbi32) {
    save_sbi::<Sbi32>(val)
}
