//! assert eval(x) => x*8
//! assert eval tests
use pbc_zk::*;

pub fn mult_8(x1: Sbi32) -> Sbi32 {
    let x2: Sbi32 = x1 + x1;
    let x4: Sbi32 = x2 + x2;
    let x8: Sbi32 = x4 + x4;
    x8
}

test_eq!(mult_8(Sbi32::from(42)), 336);
