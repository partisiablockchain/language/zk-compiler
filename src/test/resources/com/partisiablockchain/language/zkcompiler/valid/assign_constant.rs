//! assert eval(x) => 4
//! assert eval tests
use pbc_zk::*;

pub fn assignment(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = x;
    y = Sbi32::from(4);
    y
}

pbc_zk::test_eq!(assignment(Sbi32::from(0)), 4);
