//! assert eval(0) => 0
//! assert eval(1) => 1337
use pbc_zk::*;

pub fn if_expr() -> Sbi32 {
    if load_sbi::<Sbu1>(SecretVarId::new(1)) {
        Sbi32::from(1337)
    } else {
        Sbi32::from(0)
    }
}
