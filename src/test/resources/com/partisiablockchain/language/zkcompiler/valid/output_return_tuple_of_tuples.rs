//! assert eval(0x01) => (0x01_01, 0x01_01)
//! assert eval(0x09) => (0x09_09, 0x09_09)

use pbc_zk::Sbi8;

pub fn identity(a: Sbi8) -> ((Sbi8, Sbi8), (Sbi8, Sbi8)) {
    ((a, a), (a, a))
}
