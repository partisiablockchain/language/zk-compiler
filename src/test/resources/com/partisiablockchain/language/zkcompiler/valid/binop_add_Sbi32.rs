//! assert eval(9, 4) => 13
//! assert eval(9, 9) => 18
//! assert eval(0, 0) => 0
//! assert eval(0, 1) => 1
//! assert eval(1, 0) => 1
//! assert eval tests
use pbc_zk::*;

pub fn add_self() -> Sbi32 {
    load_sbi::<Sbi32>(SecretVarId::new(1)) + load_sbi::<Sbi32>(SecretVarId::new(2))
}

test_eq!(add_self(), 13, [9i32, 4i32]);
test_eq!(add_self(), 18, [9i32, 9i32]);
test_eq!(add_self(), 0, [0i32, 0i32]);
test_eq!(add_self(), 1, [0i32, 1i32]);
test_eq!(add_self(), 1, [1i32, 0i32]);
test_eq!(add_self(), 4, [-6i32, 10i32]);
test_eq!(add_self(), 4, [2i32; 2]);
