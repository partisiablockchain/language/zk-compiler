//! assert eval() => 2147483648

pub fn circuit_1() -> u64 {
    1u64 << 31u64
}
