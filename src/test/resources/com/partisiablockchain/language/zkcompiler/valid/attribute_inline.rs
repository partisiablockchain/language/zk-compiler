//! assert eval(x) => x

#[allow(unused)]
use pbc_zk::{save_sbi, Sbi32, Sbu1};

pub fn main(val: Sbi32) {
    save_1(val);
}

#[inline]
fn save_1(val: Sbi32) {
    save_2(val)
}

#[inline]
fn save_2(val: Sbi32) {
    save_sbi::<Sbi32>(val)
}
