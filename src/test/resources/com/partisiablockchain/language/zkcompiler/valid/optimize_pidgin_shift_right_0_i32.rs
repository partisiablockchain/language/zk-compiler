//! assert eval(x) => x

pub fn identity(x: i32) -> i32 {
    x >> 0
}
