//! assert eval(0, 0, 0) => 0
//! assert eval(1, 0, 1) => 0
//! assert eval(1, 1, 1) => 0
//! assert eval(1, 2, 1) => 0
//! assert eval(1, 99, 99) => 0
//! assert eval(90, 99, 90) => 0
//! assert eval(99, 99, 99) => 0
//! assert eval(100, 99, 100) => 0
//! assert eval(1, 1, 0) => 1
//! assert eval(2, 2, 1) => 1
//! assert eval(99, 99, 90) => 1
//! assert eval(100, 100, 99) => 1
use pbc_zk::*;

pub fn add_self(x: Sbi32, y: Sbi32, z: Sbi32) -> Sbu1 {
    if z == y {
        (y + Sbi32::from(1)) == x
    } else {
        y == x
    }
}
