//! assert eval(x) => x+1

use std::ops::Range;

fn count(x: u16) -> u16 {
    let range: Range<u16> = 0u16..x;
    let mut res: u16 = 1u16;
    for _i in range {
        res = res + 1u16;
    }
    res
}
