//! assert eval(0, 2, 1, 3, 0, 2, 3, 0, 1, 5, 0, 6, 0, 1, 2, 0, 3, 0, 5, 6, 0, 3, 2, 6, 0, 3, 0, 7, 0, 3, 5, 2) => 0x2020202020202020

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything(arr: [u8; 32]) -> [u8; 8] {
    // Initialize state
    let mut counts: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];

    // Count each variable
    for variable_id in 0usize..32usize {
        let var = arr[variable_id];
        for value in 0usize..8usize {
            let m: u8 = { 1 };
            counts[value] = counts[value] + m;
        }
    }

    counts
}
