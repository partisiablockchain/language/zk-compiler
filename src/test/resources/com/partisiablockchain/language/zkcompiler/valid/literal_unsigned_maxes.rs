//! assert eval() => ()

pub fn unsigned_maxes() {
    let _u8 = 0xFFu8;
    let _u16 = 0xFFFFu16;
    let _u32 = 0xFFFFFFFFu32;
    let _u64 = 0xFFFFFFFFFFFFFFFFu64;
    let _u128 = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFu128;
}
