//! assert eval(9, 4) => 13
//! assert eval(9, 9) => 18
//! assert eval(0, 0) => 0
//! assert eval(0, 1) => 1
//! assert eval(1, 0) => 1
//! assert eval tests
use pbc_zk::*;

pub fn add_self() -> Sbi128 {
    load_sbi::<Sbi128>(SecretVarId::new(1)) + load_sbi::<Sbi128>(SecretVarId::new(2))
}

test_eq!(add_self(), 13, [9i128, 4i128]);
test_eq!(add_self(), 18, [9i128, 9i128]);
test_eq!(add_self(), 0, [0i128, 0i128]);
test_eq!(add_self(), 1, [0i128, 1i128]);
test_eq!(add_self(), 1, [1i128, 0i128]);
