//! assert eval(0, 1) => 0
//! assert eval(1, 0) => 1
//! assert eval(1, 1) => 2
//! assert eval(4, 4) => 64
//! assert eval(15, 0) => 15
//! assert eval(15, 7) => 1920
//! assert eval(10, 5) => 320
//! assert eval(0xfff, 1) => 0x1ffe
//! assert eval(0xfff, 4) => 0xfff0
//! assert eval(0x983e515, 4) => 0x983e5150
//! assert eval tests
use pbc_zk::*;

pub fn shl(x: Sbi32, y: usize) -> Sbi32 {
    x << y
}

test_eq!(shl(Sbi32::from(0), 1), 0);
test_eq!(shl(Sbi32::from(1), 0), 1);
test_eq!(shl(Sbi32::from(1), 1), 2);
test_eq!(shl(Sbi32::from(4), 4), 64);
test_eq!(shl(Sbi32::from(15), 0), 15);
test_eq!(shl(Sbi32::from(15), 7), 1920);
test_eq!(shl(Sbi32::from(10), 5), 320);
test_eq!(shl(Sbi32::from(0xfff), 1), 0x1ffe);
test_eq!(shl(Sbi32::from(0xfff), 4), 0xfff0);
test_eq!(shl(Sbi32::from(0x783e515), 4), 0x783e5150);
