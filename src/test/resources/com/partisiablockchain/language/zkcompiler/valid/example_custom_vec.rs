//! assert fail_compile "All component types must be of the same type"
use pbc_zk::*;

struct Vec {
    length: usize,
    capacity: usize,
    elements: [Sbi32; 32],
}

fn vec_new() -> Vec {
    Vec {
        length: 0,
        capacity: 32,
        elements: [Sbi32::from(0); 32],
    }
}

fn vec_push(mut self: Vec, element: Sbi32) -> Vec {
    if self.length < self.capacity {
        self.elements[self.length] = element;
        self.length = self.length + 1;
    }
    self
}

fn vec_pop(mut self: Vec) -> (Vec, Sbi32) {
    if self.length > 0usize {
        self.length = self.length - 1;
    }
    (self, self.elements[self.length - 1])
}

pub fn assignment(x: Sbi32) -> Sbi32 {
    let mut vec = vec_new();
    vec = vec_push(vec, x);
    vec = vec_push(vec, Sbi32::from(0));
    vec = vec_push(vec, x + Sbi32::from(1));
    vec = vec_push(vec, Sbi32::from(5234));
    vec = vec_push(vec, x + Sbi32::from(2));
    vec = vec_push(vec, Sbi32::from(23));
    vec = vec_push(vec, x + Sbi32::from(3));
    let tup = vec_pop(vec);
    vec = tup.0;
    let tup = vec_pop(vec);
    vec = tup.0;
    let tup = vec_pop(vec);
    vec = tup.0;
    tup.1
}
