//! assert eval(x) => x
use pbc_zk::*;

pub fn identity(x: Sbi32) -> Sbi32 {
    let y: Sbi32 = x;
    let z: Sbi32 = y;
    let w: Sbi32 = z;
    w
}
