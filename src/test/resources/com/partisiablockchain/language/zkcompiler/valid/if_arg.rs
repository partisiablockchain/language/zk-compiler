//! assert eval(0) => 4
//! assert eval(1) => 2
use pbc_zk::*;

fn cnd(b: bool) -> Sbi32 {
    if b {
        Sbi32::from(2)
    } else {
        Sbi32::from(4)
    }
}
