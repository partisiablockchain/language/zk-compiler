//! assert eval(0x01) => 0x01_01
//! assert eval(0x09) => 0x09_09
//! assert eval tests

use pbc_zk::*;

#[derive(SecretBinary)]
struct MyPair {
    v1: Sbi8,
    v2: Sbi8,
}

pub fn identity(a: Sbi8) {
    let value = MyPair { v1: a, v2: a };
    save_sbi::<MyPair>(value)
}

test_eq!(identity(Sbi8::from(2)), (), [0; 0], [0x0202i16]);
test_eq!(identity(Sbi8::from(9)), (), [0; 0], [0x0909i16]);
