//! assert eval(x) => 99
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    let c: Sbi32 = Sbi32::from((6 * 3) >> 1);
    if c <= Sbi32::from(9) {
        Sbi32::from(99)
    } else {
        Sbi32::from(777)
    }
}
