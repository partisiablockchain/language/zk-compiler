//! assert eval(x) => x
use pbc_zk::*;

fn what(x: Sbi32) -> Sbi32 {
    if x == x {
        x
    } else {
        Sbi32::from(0)
    }
}
