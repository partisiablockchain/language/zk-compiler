//! assert eval(0) => 1
//! assert eval(1) => 1
//! assert eval(2) => 1
//! assert eval(94) => 1
//! assert eval(95) => 0
//! assert eval(96) => 1
//! assert eval(1337) => 1
//! assert eval tests
use pbc_zk::*;

pub fn circuit_1(x: i32) -> bool {
    x != 95
}

test_eq!(circuit_1(0), true);
test_eq!(circuit_1(1), true);
test_eq!(circuit_1(2), true);
test_eq!(circuit_1(94), true);
test_eq!(circuit_1(95), false);
test_eq!(circuit_1(96), true);
test_eq!(circuit_1(1337), true);
