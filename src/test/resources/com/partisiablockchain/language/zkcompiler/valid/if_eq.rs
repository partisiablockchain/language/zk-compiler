//! assert eval(9, 4) => 2
//! assert eval(9, 9) => 1
//! assert eval(0, 0) => 1
//! assert eval(0, 1) => 2
//! assert eval(1, 0) => 2
use pbc_zk::*;

fn what(x: Sbi32, y: Sbi32) -> Sbi32 {
    (if x == y {
        Sbi32::from(1)
    } else {
        Sbi32::from(2)
    })
}
