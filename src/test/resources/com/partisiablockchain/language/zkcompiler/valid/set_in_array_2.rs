//! assert eval(0, x) => 0x00_00_00_ff_00_00_00

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything(mut counts: [Sbi8; 8], val: bool) -> [Sbi8; 8] {
    let idx: usize = if val { 3usize } else { 3usize };
    counts[idx] = Sbi8::from(0xffu8 as i8);
    counts
}
