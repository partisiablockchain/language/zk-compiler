//! assert eval(1) => 0
//! assert eval(2) => 0
//! assert eval(3) => 1
//! assert eval(4) => 4

use pbc_zk::Sbi32;

pub fn compute_55(max_idx: i32) -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx1 in 0..max_idx {
        // Note that this loop is identical to `for idx2 in 0..idx1`
        for idx2 in 0..max_idx {
            if idx1 == idx2 {
                break;
            }
            sum = sum + Sbi32::from(idx2);
        }
    }
    sum
}
