//! assert eval(0x01) => 0x01_01_01
//! assert eval(0x09) => 0x09_09_09
//! assert eval(0x6f) => 0x6f_6f_6f
//! assert eval(0xff) => 0xff_ff_ff
//! assert eval tests

use pbc_zk::*;

#[derive(SecretBinary)]
struct MyPair {
    v1: Sbi8,
    v2: MyOtherPair,
}

#[derive(SecretBinary)]
struct MyOtherPair {
    v1: Sbi8,
    v2: Sbi8,
}

pub fn something(a: Sbi8) {
    let value = MyPair {
        v1: a,
        v2: MyOtherPair { v1: a, v2: a },
    };
    save_sbi::<MyPair>(value);
}

pbc_zk::test_eq!(something(Sbi8::from(2)), (), [0; 0], [0x020202i32]);
