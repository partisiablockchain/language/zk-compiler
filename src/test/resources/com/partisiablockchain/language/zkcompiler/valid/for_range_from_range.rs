//! assert eval() => 55

use pbc_zk::Sbi32;

pub fn binop_range() -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(0);
    let range = 0..11;
    for idx in range {
        sum = sum + Sbi32::from(idx);
    }
    sum
}
