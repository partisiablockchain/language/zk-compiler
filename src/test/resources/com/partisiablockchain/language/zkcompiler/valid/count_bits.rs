//! assert eval(0) => 0
//! assert eval(65536) => 1
//! assert eval(933212333) => 19
//! assert eval(2147483647) => 31
//! assert eval(-2147483648) => 1
//! assert eval(-1) => 32
use pbc_zk::{Sbi32, Sbu1};

pub fn test(i1: Sbi32) -> Sbi32 {
    count_bits(i1)
}

fn count_bits(x: Sbi32) -> Sbi32 {
    let mut r: Sbi32 = Sbi32::from(0);
    for idx in 0..32 {
        if is_bit_set(x, idx) {
            r = r + Sbi32::from(1);
        }
    }
    r
}

fn is_bit_set(x: Sbi32, idx: i32) -> Sbu1 {
    let bit_mask = Sbi32::from(1 << idx);
    (bit_mask & x) != Sbi32::from(0)
}
