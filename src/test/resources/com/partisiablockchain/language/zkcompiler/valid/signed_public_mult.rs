//! assert eval(2, 3) => 6
//! assert eval(0xFFFFFFFF, 1) => 0xFFFFFFFF
//! assert eval(-1, 1) => 0xFFFFFFFF
//! assert eval(0xFFFFFFFF, 0xFFFFFFFF) => 1
//! assert eval(0xFFFFFFFF, 0xFFFFFFFD) => 3
//! assert eval(0xFFFFFFFF, 0x7FFFFFFF) => 0x80000001

use pbc_zk::*;

fn signed_mult(x: i32, y: i32) -> i32 {
    x * y
}
