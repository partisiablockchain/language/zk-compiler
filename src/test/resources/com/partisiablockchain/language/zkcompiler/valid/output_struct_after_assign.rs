//! assert eval(0x00_00) => 0x22_00
//! assert eval(0x10_00) => 0x22_00
//! assert eval(0x00_10) => 0x22_10
//! assert eval(0x10_10) => 0x22_10

use pbc_zk::Sbi8;

struct MyPair {
    v1: Sbi8,
    v2: Sbi8,
}

fn something(mut a: MyPair) -> MyPair {
    a.v2 = Sbi8::from(34);
    a
}
