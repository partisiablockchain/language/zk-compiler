//! assert eval() => ()

pub fn signed_mins() {
    let _i8 = -0x80i8;
    let _i16 = -0x8000i16;
    let _i32 = -0x80000000i32;
    let _i64 = -0x8000000000000000i64;
    let _i128 = -0x80000000000000000000000000000000i128;
}
