//! assert eval(0x00000001_00000009) => 9
//! assert eval(0x00000009_00000001) => 1

use pbc_zk::{load_sbi, Sbi32, SecretVarId};

pub fn identity() -> Sbi32 {
    let id = SecretVarId::new(1);
    let pair: (Sbi32, Sbi32) = load_sbi::<(Sbi32, Sbi32)>(id);
    pair.0
}
