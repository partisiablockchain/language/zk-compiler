//! assert eval(0) => (0)
//! assert eval(1) => (1)
//! assert eval(255) => (255)
//! assert eval(256) => (0)
use pbc_zk::*;

pub fn cast(v1: u32) -> u8 {
    v1 as u8
}
