//! assert eval(x) => x

use pbc_zk::Sbi32;

pub fn identity(x: Sbi32) -> Sbi32 {
    x >> 0
}
