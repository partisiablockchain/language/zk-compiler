//! assert eval(1, 0) => 1
//! assert eval(1, 1) => 1
//! assert eval(1, 2) => 1
//! assert eval(1, 99) => 1
//! assert eval(90, 99) => 1
//! assert eval(99, 99) => 1
//! assert eval(100, 99) => 1
//! assert eval tests
use pbc_zk::*;

pub fn add_self(x: i32, y: i32) -> bool {
    ((x, x) == (y, y)) == ((x, x) == (y, y))
}

test_eq!(add_self(1, 0), true);
test_eq!(add_self(1, 1), true);
test_eq!(add_self(1, 2), true);
test_eq!(add_self(1, 99), true);
test_eq!(add_self(90, 99), true);
test_eq!(add_self(99, 99), true);
test_eq!(add_self(100, 99), true);
