//! assert eval(0) => 0
//! assert eval(1) => 1
//! assert eval(41) => 1
//! assert eval(123) => 1

pub fn odd(i: i32) -> bool {
    if i == 0 {
        return false;
    }
    even(i - 1)
}

fn even(i: i32) -> bool {
    if i == 0 {
        return true;
    }
    odd(i - 1)
}
