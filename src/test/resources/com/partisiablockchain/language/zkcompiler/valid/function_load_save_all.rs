//! assert eval(x) => (x)
//! assert eval(1, 2, 3) => (1, 2, 3)
//! assert eval(1, 6, 2, 3) => (1, 6, 2, 3)
use pbc_zk::{load_sbi, save_sbi, secret_variable_ids, Sbi32};

pub fn main(input: Sbi32) {
    for var_id in secret_variable_ids() {
        let value = load_sbi::<Sbi32>(var_id);
        save_sbi::<Sbi32>(value);
    }
}
