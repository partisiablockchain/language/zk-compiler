//! assert eval(x) => x
pub fn cast(v1: u32) -> i32 {
    v1 as i32
}
