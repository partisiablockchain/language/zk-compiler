//! assert eval() => 0
use pbc_zk::Sbu1;

pub fn false_sbi() -> Sbu1 {
    Sbu1::from(false)
}
