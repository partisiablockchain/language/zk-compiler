//! assert eval() => 0xFFFFFFFF_FFFFFFFF_FFFFFFFF_FFFFFFFF

struct MyPair {
    v1: u16,
    v2: u16,
    v3: u16,
    v4: u16,
    v5: u16,
    v6: u16,
    v7: u16,
    v8: u16,
}

pub fn big_constant() -> MyPair {
    create_my_pair(0xFFFFu16)
}

fn create_my_pair(value: u16) -> MyPair {
    MyPair {
        v1: value,
        v2: value,
        v3: value,
        v4: value,
        v5: value,
        v6: value,
        v7: value,
        v8: value,
    }
}
