//! assert eval(0x00000001_00000002_06050403_00000004) => 0x04
//! assert eval(0x00000005_00000006_0a090807_00000008) => 0x08
//! assert eval(0x00000009_0000000a_0e0d0c0b_0000000c) => 0x0c
//! assert eval(0x0000000d_0000000e_0201000f_00000000) => 0x00
use pbc_zk::Sbi8;

pub fn extract_one(x: [[Sbi8; 4]; 4]) -> Sbi8 {
    x[1][1]
}
