//! assert eval(0) => 2
//! assert eval(1) => 2
//! assert eval(2) => 2
//! assert eval(31) => 2
//! assert eval(32) => 3
//! assert eval(90) => 3
//! assert eval(91) => 4
//! assert eval(98) => 4
//! assert eval(99) => 1
//! assert eval(999) => 1
use pbc_zk::*;

pub fn mut_if(x: Sbi32) -> Sbi32 {
    let mut y: Sbi32 = Sbi32::from(9);

    if x < Sbi32::from(91) {
        y = {
            let mut y: Sbi32 = Sbi32::from(2);
            if Sbi32::from(31) < x {
                y = Sbi32::from(3)
            }
            y
        }
    } else {
        y = Sbi32::from(4)
    }

    if x >= Sbi32::from(99) {
        y = Sbi32::from(1)
    }
    y
}
