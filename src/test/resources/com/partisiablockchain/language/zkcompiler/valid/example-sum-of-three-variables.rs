//! assert eval(4, 6, 1) => 11
use pbc_zk::Sbi32;
pub fn sum(s1: Sbi32, s2: Sbi32, s3: Sbi32) -> Sbi32 {
    s1 + s2 + s3
}
