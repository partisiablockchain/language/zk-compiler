//! assert eval(x) => 3
use pbc_zk::Sbi32;

fn what(x: i32) -> Sbi32 {
    if x == 3 {
        Sbi32::from(x)
    } else {
        Sbi32::from(3)
    }
}
