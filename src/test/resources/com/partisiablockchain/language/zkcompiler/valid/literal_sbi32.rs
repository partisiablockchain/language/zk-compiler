//! assert eval(x) => 1337
use pbc_zk::*;

pub fn identity(x: Sbi32) -> Sbi32 {
    Sbi32::from(1337)
}
