//! assert eval(0x00000001_00000002_00000003_00000004) => 0x3
//! assert eval(0x00000005_00000006_00000007_00000008) => 0x7
//! assert eval(0x00000009_0000000a_0000000b_0000000c) => 0xb
//! assert eval(0x0000000d_0000000e_0000000f_00000000) => 0xf
use pbc_zk::Sbi32;

pub fn extract_one(x: [Sbi32; 4]) -> Sbi32 {
    x[1]
}
