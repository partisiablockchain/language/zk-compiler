//! assert eval(0, x) => 0x00_ff_ff_ff_ff_ff_ff_00

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub fn sum_everything(val: bool) -> [Sbi8; 8] {
    let mut counts: [Sbi8; 8] = [Sbi8::from(0xffu8 as i8); 8];
    counts[0] = Sbi8::from(0x00i8);
    counts[7] = Sbi8::from(0x00i8);
    counts
}
