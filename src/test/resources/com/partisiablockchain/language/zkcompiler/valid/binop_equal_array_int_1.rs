//! assert eval(0, 0) => 1
//! assert eval(3412311, 41231312) => 0
//! assert eval(3412311, 3412311) => 1
//! assert eval tests
use pbc_zk::*;

pub fn equal(v1: [i32; 11], v2: [i32; 11]) -> bool {
    v1 == v2
}

test_eq!(equal([0; 11], [0; 11]), true);
test_eq!(
    equal(
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    ),
    true
);
test_eq!(equal([0; 11], [1; 11]), false);
