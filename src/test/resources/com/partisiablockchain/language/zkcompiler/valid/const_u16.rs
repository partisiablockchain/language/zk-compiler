//! assert eval(x) => x + 23

type mytype = u16;

const MY_CONST: mytype = 23;

fn plus_const(x: mytype) -> mytype {
    x + MY_CONST
}
