//! assert eval tests
use pbc_zk::*;

pub fn test_binop(x: Sbi32, y: Sbi32) -> Sbu1 {
    x < y
}

test_eq!(test_binop(Sbi32::from(-231), Sbi32::from(-1)), true);
test_eq!(test_binop(Sbi32::from(-1), Sbi32::from(0)), true);
test_eq!(test_binop(Sbi32::from(0), Sbi32::from(-1)), false);
test_eq!(test_binop(Sbi32::from(1), Sbi32::from(0)), false);
test_eq!(test_binop(Sbi32::from(1), Sbi32::from(1)), false);
test_eq!(test_binop(Sbi32::from(1), Sbi32::from(2)), true);
test_eq!(test_binop(Sbi32::from(1), Sbi32::from(99)), true);
test_eq!(test_binop(Sbi32::from(90), Sbi32::from(99)), true);
test_eq!(test_binop(Sbi32::from(99), Sbi32::from(99)), false);
test_eq!(test_binop(Sbi32::from(100), Sbi32::from(99)), false);
