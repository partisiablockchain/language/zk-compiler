//! assert eval(1) => 1
//! assert eval(0) => 1
//! assert eval(0xFFFFFFFF) => 0

use pbc_zk::*;

fn le_divergence(x: i32) -> bool {
    let b1 = 0i32 <= x;
    let b2 = 0u32 <= (x as u32);

    b1 == b2
}
