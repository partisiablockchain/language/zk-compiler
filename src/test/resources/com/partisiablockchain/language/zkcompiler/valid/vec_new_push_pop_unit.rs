//! assert eval(x) => 0
use std::vec::Vec;

struct Empty {}

pub fn test(x: Empty) -> Empty {
    let vec: Vec<Empty> = Vec::new::<Empty>();
    Vec::push::<Empty>(vec, x);
    Vec::pop::<Empty>(vec)
}
