//! assert eval(x) => x
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    let c: Sbi32 = Sbi32::from((6 * 3) >> 1);
    if c <= Sbi32::from(8) {
        Sbi32::from(99)
    } else {
        x
    }
}
