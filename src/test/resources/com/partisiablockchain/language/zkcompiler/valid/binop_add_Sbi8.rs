//! assert eval(0, 0) => 0
//! assert eval(1, 0) => 1
//! assert eval(0, 1) => 1
//! assert eval(1, 1) => 2
//! assert eval(100, 100) => 200
//! assert eval(100, 200) => 44
//! assert eval(200, 100) => 44
//! assert eval(127, 127) => 254
//! assert eval(128, 128) => 0
//! assert eval(128, 129) => 1
//! assert eval(0, 255) => 255
//! assert eval(255, 255) => 254
//! assert eval tests
use pbc_zk::*;

pub fn add_self() -> Sbi8 {
    load_sbi::<Sbi8>(SecretVarId::new(1)) + load_sbi::<Sbi8>(SecretVarId::new(2))
}

test_eq!(add_self(), 13, [9i8, 4i8]);
test_eq!(add_self(), 18, [9i8, 9i8]);
test_eq!(add_self(), 0, [0i8, 0i8]);
test_eq!(add_self(), 1, [0i8, 1i8]);
test_eq!(add_self(), 1, [1i8, 0i8]);
