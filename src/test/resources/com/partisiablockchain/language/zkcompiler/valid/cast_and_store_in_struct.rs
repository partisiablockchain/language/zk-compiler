//! assert eval() => 0x8899

use pbc_zk::{Sbi32, Sbi8};

struct MyStruct {
    f1: Sbi8,
    f2: Sbi8,
}

pub fn cast() -> MyStruct {
    let a: Sbi32 = Sbi32::from(0x999);
    let b: Sbi32 = Sbi32::from(0x888);
    MyStruct {
        f1: a as Sbi8,
        f2: b as Sbi8,
    }
}
