//! assert eval(0) => 0
//! assert eval(0,1,3) => 2
//! assert eval(0,1,2,1,2) => 2
//! assert eval(9,8,1,4,3) => 3
use pbc_zk::{load_sbi, secret_variable_ids, Sbi32, Sbu1};

pub fn test() -> Sbi32 {
    let mut c = Sbi32::from(0);
    for id in secret_variable_ids() {
        let vote = load_sbi::<Sbi32>(id);
        c = c + ((vote as Sbu1) as Sbi32);
    }
    c
}
