//! assert eval(x) => 31
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    (if true { Sbi32::from(1) } else { Sbi32::from(3) })
        + (if false {
            Sbi32::from(10)
        } else {
            Sbi32::from(30)
        })
}
