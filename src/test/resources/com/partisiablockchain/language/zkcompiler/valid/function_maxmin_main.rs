//! assert eval() => 132
//! use function_maxmin_lib
//! use function_lib_max
use crate::valid::function_maxmin_lib::maxmin;
use pbc_zk::Sbi32;

pub fn test() -> Sbi32 {
    let max_min = maxmin(Sbi32::from(93), Sbi32::from(39));
    max_min.max + max_min.min
}
