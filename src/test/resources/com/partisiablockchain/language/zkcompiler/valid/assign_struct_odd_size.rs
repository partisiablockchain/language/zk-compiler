//! assert eval() => 0x02_01_00
//! assert eval tests

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk::*;

pub struct Tuple {
    v1: Sbi8,
    v2: Sbi8,
    v3: Sbi8,
}

pub fn sum_everything() -> Tuple {
    // Initialize state
    let mut counts = Tuple {
        v1: Sbi8::from(0),
        v2: Sbi8::from(0),
        v3: Sbi8::from(0),
    };
    counts.v1 = Sbi8::from(0);
    counts.v2 = Sbi8::from(1);
    counts.v3 = Sbi8::from(2);
    counts
}

pbc_zk::test_eq!(
    {
        let t = sum_everything();
        t.v1 == Sbi8::from(0) && t.v2 == Sbi8::from(1) && t.v3 == Sbi8::from(2)
    },
    true
);
