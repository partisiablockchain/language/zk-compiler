//! assert eval(0x00000001_00000009) => 9
//! assert eval(0x00000009_00000001) => 1

use pbc_zk::Sbi32;

struct MyPair {
    v1: Sbi32,
    v2: Sbi32,
}

pub fn identity(pair: MyPair) -> Sbi32 {
    pair.v1
}
