//! assert ignore
//!
//! This is a small library with maxmin function.
use crate::valid::function_lib_max::{max, min};
use pbc_zk::Sbi32;

struct MaxMin {
    max: Sbi32,
    min: Sbi32,
}

fn maxmin(x: Sbi32, y: Sbi32) -> MaxMin {
    MaxMin {
        max: max(x, y),
        min: min(x, y),
    }
}
