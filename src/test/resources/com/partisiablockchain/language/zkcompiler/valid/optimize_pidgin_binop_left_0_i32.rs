//! assert eval(x) => 1

pub fn tautologic(x: i32) -> bool {
    let zero: i32 = 0;

    // Produce zero
    let and: i32 = zero & x;
    let shl: i32 = zero << x;
    let shr: i32 = zero >> x;
    let mul: i32 = zero * x;

    // Produce right
    let or: i32 = zero | x;
    let xor: i32 = zero ^ x;
    let add: i32 = zero + x;

    (and == shl && shl == shr && shr == mul && mul == zero) && (or == xor && xor == add && add == x)
}
