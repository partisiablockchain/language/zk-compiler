//! assert eval() => 9

pub fn constant_i8() -> i8 {
    let y: i8 = 9;
    y
}
