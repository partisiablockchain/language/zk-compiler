//! assert eval() => 1
use pbc_zk::Sbi1;

pub fn true_sbi() -> Sbi1 {
    Sbi1::from(-1i1)
}
