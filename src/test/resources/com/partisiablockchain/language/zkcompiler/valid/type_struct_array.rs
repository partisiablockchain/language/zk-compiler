//! assert eval(0x00_00_00_00_00_00_00_00) => 0x0
//! assert eval(0x00_01_00_00_03_00_00_08) => 0x1
//! assert eval(0x01_02_04_08_00_00_00_00) => 0x2
//! assert eval(0x01_02_04_08_08_04_02_01) => 0x2
use pbc_zk::Sbi8;

struct MyStruct {
    a: [Sbi8; 4],
    b: [Sbi8; 4],
}

pub fn compute(v: MyStruct) -> Sbi8 {
    v.b[2]
}
