//! assert eval() => 1
use pbc_zk::Sbu1;

pub fn true_sbi() -> Sbu1 {
    Sbu1::from(true)
}
