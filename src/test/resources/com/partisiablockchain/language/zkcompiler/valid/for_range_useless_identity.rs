//! assert eval() => 55

use pbc_zk::*;

pub fn compute_55() -> Sbi32 {
    let mut sum: Sbi32 = Sbi32::from(55);
    for idx in 0..11 {
        sum = sum;
    }
    sum
}
