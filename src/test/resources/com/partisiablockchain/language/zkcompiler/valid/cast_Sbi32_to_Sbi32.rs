//! assert eval(x) => x
use pbc_zk::*;

pub fn try_cast(v1: Sbi32) -> Sbi32 {
    v1 as Sbi32
}
