//! assert eval(x) => 6
use pbc_zk::*;

fn f(x: Sbi32) -> Sbi32 {
    let mut z: Sbi32 = x;
    if x == x {
        let y: Sbi32 = Sbi32::from(6);
        z = y;
    } else {
        let y: Sbi32 = Sbi32::from(9);
        z = y;
    }
    z
}
