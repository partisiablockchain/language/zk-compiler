//! assert eval() => 0xFFFFFFFF_FFFFFFFF

struct MyPair {
    v1: i32,
    v2: i32,
}

pub fn big_constant() -> MyPair {
    MyPair { v1: -1, v2: -1 }
}
