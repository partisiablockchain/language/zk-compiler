//! assert eval(x) => x
use pbc_zk::*;

struct Point {
    x: Sbi32,
    y: Sbi32,
}

pub fn main(p: Point) -> Sbi32 {
    extract_0([p.x, p.y])
}

fn extract_0(p: [Sbi32; 2]) -> Sbi32 {
    p[0]
}
