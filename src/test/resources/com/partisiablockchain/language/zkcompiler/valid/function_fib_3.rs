//! assert eval(x, 0) => 10_000_000
//! assert eval(0, 1) => 0
//! assert eval(0,10) => 0
//! assert eval(1,10) => 1
//! assert eval(2,10) => 1
//! assert eval(3,10) => 2
//! assert eval(4,10) => 3
//! assert eval(5,10) => 5
//! assert eval(6,10) => 8
//! assert eval(7,10) => 13
//! assert eval(8,10) => 21
//! assert eval(9,10) => 34
//! assert eval(10,10) => 55
use pbc_zk::{Sbi32, Sbu1};

pub fn test(n: Sbi32, rounds: i32) -> Sbi32 {
    fib(n, rounds)
}

fn fib(n: Sbi32, rounds: i32) -> Sbi32 {
    if rounds == 0 {
        return Sbi32::from(10_000_000);
    }
    if n < Sbi32::from(2) {
        n
    } else {
        fib(n + Sbi32::from(-1), rounds - 1) + fib(n + Sbi32::from(-2), rounds - 1)
    }
}
