//! assert eval(1) => 2

pub fn assign_as(mut x: i32) -> i32 {
    x = 2i16 as i32;
    x
}
