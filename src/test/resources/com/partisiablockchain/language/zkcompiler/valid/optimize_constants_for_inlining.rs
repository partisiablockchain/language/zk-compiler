//! assert eval(0) => 0
//! assert eval(1) => 9
//! assert eval(2) => 18
//! assert eval(300) => 2700
use pbc_zk::*;

pub fn circuit_1(mut iterations: i32) -> Sbi32 {
    let c: Sbi32 = (Sbi32::from(6) * Sbi32::from(3)) >> 1;
    let mut sum: Sbi32 = Sbi32::from(0);
    for idx in 0..iterations {
        sum = sum + c;
    }
    sum
}
