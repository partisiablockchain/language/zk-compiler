//! Implementation of a classical long division.
//!
//! assert ignore
//!
//! See file `function_long_division_test` for tests of this implementation.

use pbc_zk::{Sbi32, Sbu1};

/// Result of running [`long_division_with_remainder`], containing both the quotient and the
/// remainder.
pub(crate) struct LongDivResult {
    quotient: Sbi32,
    remainder: Sbi32,
}

/// Classic division implementation.
///
/// Performs unsigned long division over the given numerator and denominator, rounding down towards
/// zero. Division by zero is defined as zero.
///
/// ## Arguments
///
/// - `numerator`: Numerator of division.
/// - `denominator`: Denominator of division. Assumed be positive.
///
/// ## Return
///
/// An instance of [`LongDivResult`] which contains both the quotient and the remainder.
///
/// Invariant:
/// ```
/// let result = long_division_with_remainder(x, y);
/// assert_eq!(x, result.quotient * y + result.remainder);
/// ```
pub(crate) fn long_division_with_remainder(numerator: Sbi32, denominator: Sbi32) -> LongDivResult {
    let bitlength = 32;

    let div_by_zero = denominator == Sbi32::from(0);
    let mut quotient: Sbi32 = Sbi32::from(0);
    let mut remainder: Sbi32 = Sbi32::from(0);
    for i_0 in 0..bitlength {
        let i = bitlength - 1 - i_0; // i in (n-1)..=0
        remainder = remainder << 1;
        remainder = replace_bit(remainder, 0, get_bit(numerator, i as usize));
        let new_remainder = remainder - denominator;
        if !get_bit(new_remainder, (bitlength - 1) as usize) {
            remainder = new_remainder;
            let one = Sbi32::from(1) as Sbu1;
            quotient = replace_bit(quotient, i as usize, one);
        }
    }
    if div_by_zero {
        LongDivResult {
            quotient: Sbi32::from(0),
            remainder: numerator,
        }
    } else {
        LongDivResult {
            quotient: quotient,
            remainder: remainder,
        }
    }
}

/// Inserts the given bit at the given index of input.
fn replace_bit(input: Sbi32, index: usize, bit: Sbu1) -> Sbi32 {
    let mut bits: [Sbu1; 32] = input.to_le_bits();
    bits[index] = bit;
    Sbi32::from_le_bits(bits)
}

/// Gets the bit at the given index in input.
fn get_bit(input: Sbi32, index: usize) -> Sbu1 {
    let bits = input.to_le_bits();
    bits[index]
}
