//! assert eval(1) => 0x00000009_00000001
//! assert eval(5) => 0x00000009_00000005

struct MyPair {
    v1: i32,
    v2: i32,
}

pub fn set_field(x: i32) -> MyPair {
    let v1 = x;
    let v2 = 9;

    MyPair { v1, v2 }
}
