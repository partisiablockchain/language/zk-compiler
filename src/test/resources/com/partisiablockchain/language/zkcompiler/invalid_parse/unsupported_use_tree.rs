//! assert fail_parse
use crate::pbc_zk::{
    other_submodule::{sbi32_of},
    submodule::Sbi32,
};

fn identity(x: Sbi32) -> Sbi32 {
    x
}
