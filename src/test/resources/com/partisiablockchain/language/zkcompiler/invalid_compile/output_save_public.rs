//! assert fail_compile "Cannot save public operand: ($0_value, $0_value)"

use pbc_zk;

pub fn compute_55(value: i32) {
    pbc_zk::save_sbi::<(i32, i32)>((value, value));
}
