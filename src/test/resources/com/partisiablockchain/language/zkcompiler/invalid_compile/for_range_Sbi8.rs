//! assert fail_compile "Expected integer type, but got ()"
//! assert ignore

use pbc_zk::Sbi8;

pub fn compute_55() -> Sbi8 {
    let mut sum: Sbi8 = Sbi8::from(0);
    let range = Sbi8::from(0)..Sbi8::from(11);
    for idx in range {
        sum = sum + idx;
    }
    sum
}
