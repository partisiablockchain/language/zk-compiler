//! assert fail_compile "Cannot load metadata as secret type (sbi32, sbi32)"

use pbc_zk::{load_metadata, load_sbi, Sbi32, SecretVarId};

struct MyPair {
    v1: Sbi32,
    v2: Sbi32,
}

fn identity() -> Sbi32 {
    let id = SecretVarId::new(1);
    let pair: MyPair = load_metadata::<MyPair>(id);
    pair.v1
}
