//! assert fail_compile "Cannot load variable as public type (i32, i32)"

use pbc_zk::{load_sbi, Sbi32, SecretVarId};

struct MyPair {
    v1: i32,
    v2: i32,
}

fn identity() -> i32 {
    let id = SecretVarId::new(1);
    let pair: MyPair = load_sbi::<MyPair>(id);
    pair.v1
}
