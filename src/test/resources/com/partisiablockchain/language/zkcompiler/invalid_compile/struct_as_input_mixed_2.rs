//! assert fail_compile "All component types must be of the same type, but was: [SecretBinaryInteger[width=32], PublicInteger[width=32]]"
//! assert fail_compile "All component types must be of the same type, but was: [SecretBinaryInteger[width=32], PublicInteger[width=32]]"
//! assert fail_compile "All component types must be of the same type, but was: [SecretBinaryInteger[width=32], PublicInteger[width=32]]"

use pbc_zk::{load_metadata, load_sbi, Sbi32, SecretVarId};

struct MyPair {
    v1: Sbi32,
    v2: i32,
}

fn identity(v1: MyPair) {
    let id = SecretVarId::new(1);
    let v2: MyPair = load_sbi::<MyPair>(id);
    let v3: MyPair = load_metadata::<MyPair>(id);
}
