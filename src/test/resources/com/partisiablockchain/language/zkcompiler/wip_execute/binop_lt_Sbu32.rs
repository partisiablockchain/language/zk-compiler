//! assert eval tests
use pbc_zk::*;

pub fn test_binop(x: Sbu32, y: Sbu32) -> Sbu1 {
    x < y
}

test_eq!(test_binop(Sbu32::from(1), Sbu32::from(0)), 0);
test_eq!(test_binop(Sbu32::from(1), Sbu32::from(1)), 1);
test_eq!(test_binop(Sbu32::from(1), Sbu32::from(2)), 1);
test_eq!(test_binop(Sbu32::from(1), Sbu32::from(99)), 1);
test_eq!(test_binop(Sbu32::from(90), Sbu32::from(99)), 1);
test_eq!(test_binop(Sbu32::from(99), Sbu32::from(99)), 0);
test_eq!(test_binop(Sbu32::from(100), Sbu32::from(99)), 0);
test_eq!(test_binop(Sbu32::from(0xFFFFFFFF), Sbu32::from(0)), 0);
test_eq!(test_binop(Sbu32::from(0), Sbu32::from(0xFFFFFFFF)), 1);
