//! assert eval(0) => 0
//! assert eval(1) => 1337
use pbc_zk::*;

pub fn if_expr(b: Sbu1) -> Sbi32 {
    if -b {
        Sbi32::from(1337)
    } else {
        Sbi32::from(0)
    }
}
