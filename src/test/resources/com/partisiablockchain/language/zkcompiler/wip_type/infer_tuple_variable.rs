//! assert fail_type_check "Expected identical types, but got i32 and u64"
//! assert fail_type_check "Expected identical types, but got i32 and u8"
//! assert fail_type_check "Expected i32, but got u64"
//! assert fail_type_check "Expected i32, but got u8"

pub fn infer_tuple() -> u8 {
    let a = (2, 3);
    let b = a.1 + 10u64;
    a.0 + 40u8
}
