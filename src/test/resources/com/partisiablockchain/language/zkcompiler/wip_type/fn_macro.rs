//! assert fail_type_check "Unsupported expression: MacroInvocation"
use pbc_zk::*;

fn f() {
    some_macro!()
}
