//! assert fail_type_check "Unsupported type: &Sbi32"
//! assert fail_type_check "Unsupported type: &mut Sbi32"
use pbc_zk::*;

pub fn everything(x: Sbi32) -> Sbi32 {
    let t0: Sbi32 = x;
    let mut t1: Sbi32 = -x;
    t1;
    let t2: Sbi32 = t0 * (t1 + Sbi32::from(4));
    t2
}

pub fn misc_types(arr: [Sbi32; 9], ptr: &Sbi32, mptr: &mut Sbi32) -> Sbi32 {
    Sbi32::from(0)
}

fn unit_fn() {}
