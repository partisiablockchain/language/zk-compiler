//! assert fail_type_check "pbc_zk::Sbi32::into_iter is not in scope"
use pbc_zk::Sbi32;

fn for_statement(x: Sbi32) {
    for i in x + x {}
}
