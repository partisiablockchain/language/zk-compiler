//! assert fail_type_check "Expected u64, but got i32"
//! //! assert fail_type_check "Expected identical types, but got i32 and u64"

pub fn infer_tuple() -> u64 {
    (2, 3).1 + 5u64
}
