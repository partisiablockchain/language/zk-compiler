//! assert fail_type_check "Defining type generic structs is not supported"
//! assert fail_type_check "Unknown type V1"
//! assert fail_type_check "Unknown type V2"

struct Pair<V1, V2> {
    v1: V1,
    v2: V2,
}
