//! assert fail_type_check "Unknown type Secret<Sbi32>"
//! assert fail_type_check "Unknown type Secret<Sbi32>"
//! assert fail_type_check "Unknown type Vec<Secret<Sbi32>>"
use pbc_zk::Sbi32;

fn second_price_auction(bids: Vec<Secret<Sbi32>>) -> (Sbi32, Sbi32) {
    let mut bid_highest: Secret<Sbi32> = bids[0];
    let mut bid_2_highest: Secret<Sbi32> = bids[0];
    for bid in bids {
        if bid.value > bid_highest.value {
            bid_2_highest = bid_highest;
            bid_highest = bid;
        } else if bid.value > bid_2_highest.value {
            bid_2_highest = bid;
        }
    }
    (bid_highest.value, bid_2_highest.value)
}
