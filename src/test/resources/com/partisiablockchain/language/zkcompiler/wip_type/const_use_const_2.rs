//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"
//! assert fail_type_check "Constant initializer expression must be constant"

const CONST_1: i32 = CONST_2 + CONST_4;
const CONST_5: i32 = 5;
const CONST_3: i32 = CONST_4 - CONST_5;
const CONST_2: i32 = CONST_3 * CONST_3;
const CONST_4: i32 = CONST_5 * 2;

fn identity(x: i32) -> i32 {
    x + CONST_1
}
