//! assert fail_type_check "Unsupported expression: MacroInvocation"
use pbc_zk::*;

pub fn macro_invoke(x: Sbi32) -> Sbi32 {
    some_macro!(x)
}
