//! assert fail_type_check "Cannot assign to immutable variable"
use pbc_zk::*;

fn assignment(x: Sbi32, y: [Sbi32; 4]) -> Sbi32 {
    y[0] = x
}
