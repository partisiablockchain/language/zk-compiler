//! assert fail_type_check "Constant initializer expression must be constant"

const MY_ADD: (i32, i32, i32) = (1, 2, 3);

fn plus_const(x: i32) -> i32 {
    x + MY_ADD.0 + MY_ADD.1 + MY_ADD.2
}
