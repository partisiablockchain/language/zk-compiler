//! assert fail_type_check "Expected identical types, but got i32 and u8"
//! assert fail_type_check "Expected identical types, but got i32 and u16"
//! assert fail_type_check "Expected i32, but got u8"
//! assert fail_type_check "Expected i32, but got u16"

pub fn infer_shadow_valid() -> u16 {
    let a = 4;
    let b = a;

    let a = 4;
    let c = a;

    b + 8u8;
    c + 8u16
}
