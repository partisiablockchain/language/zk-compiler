//! assert fail_compile "Named type comparison not supported"

/// Perform a zk computation on secret-shared data.
/// Finds the highest bidder and the amount of the second-highest bid
use pbc_zk;
use pbc_zk::*;

struct Input {
    internal: [Sbi32; 0],
}

struct Counter {
    count: Sbi8,
}

pub fn sum_everything() -> [Counter; 1] {
    // Initialize state
    let mut counts: [Counter; 1] = [Counter {
        count: Sbi8::from(0),
    }; 1];

    // Count each variable
    for variable_id in secret_variable_ids() {
        let input = load_sbi::<Input>(variable_id);
        let inc = (input == input) as Sbi8;
        counts[0].count = counts[0].count + inc;
    }

    counts
}
