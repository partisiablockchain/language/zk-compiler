//! assert fail_type_check "Expected identical types, but got i32 and u8"
//! assert fail_type_check "Expected identical types, but got i32 and u16"
//! assert fail_type_check "Expected identical types, but got i32 and u8"
//! assert fail_type_check "Expected i32, but got u8"
//! assert fail_type_check "Expected i32, but got u16"
//! assert fail_type_check "Expected i32, but got u8"

pub fn infer_shadow_valid() -> u8 {
    let a = 4;
    let b1 = a;
    let c1 = b1;

    let a = 4;
    let b2 = a;

    let d = b1 + 8u8;
    let e = c1 + 5; // 5u8

    let f = b2 + 42u16;

    e + 2u8
}
