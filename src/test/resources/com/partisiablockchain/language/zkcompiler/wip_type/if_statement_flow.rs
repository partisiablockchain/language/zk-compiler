//! assert fail_type_check "Expected i32, but got ()"
use pbc_zk::*;

pub fn if_statement(x: i32) -> i32 {
    if x < 5 {
        return 0;
    } else {
        return 1;
    }
}
