//! assert fail_type_check "Defining type generic structs is not supported"
//! assert fail_type_check "Unknown type V"

struct Wrapper<V> {
    v: V,
}
