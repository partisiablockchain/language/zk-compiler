//! assert fail_type_check "Expected u128, but got i32"

pub fn infer_for() -> u128 {
    let mut sum = 0;
    for i in 0..5 {
        sum = sum + i;
    }
    sum
}
