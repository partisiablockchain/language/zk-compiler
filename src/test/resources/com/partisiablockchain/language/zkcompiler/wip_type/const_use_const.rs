//! assert fail_type_check "Constant initializer expression must be constant"

use pbc_zk::Sbi32;

const MY_CONST: i32 = 23;
const MY_ADD: i32 = MY_CONST + MY_CONST;

fn plus_const(x: Sbi32) -> Sbi32 {
    x + Sbi32::from(MY_ADD)
}
