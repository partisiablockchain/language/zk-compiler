//! assert fail_type_check "Variable u8::MAX not in scope"
//! assert fail_type_check "Variable u8::MIN not in scope"
//! assert fail_type_check "Variable u8::BITS not in scope"
fn u8_constants(x: u8) -> u8 {
    u8::MAX - u8::MIN - u8::BITS
}
