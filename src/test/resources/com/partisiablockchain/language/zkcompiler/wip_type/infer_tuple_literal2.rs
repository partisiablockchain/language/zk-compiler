//! assert fail_type_check "Expected u128, but got i32"

pub fn infer_tuple() -> u128 {
    (2, 3).1 + (5, 8, 11).2
}
