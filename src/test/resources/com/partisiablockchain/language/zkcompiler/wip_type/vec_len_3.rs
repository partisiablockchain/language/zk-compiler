//! assert fail_type_check "Wrong type argument count: Expected 1, but got 0"
//! assert fail_type_check "Wrong type argument count: Expected 1, but got 0"
//! assert fail_type_check "Wrong type argument count: Expected 1, but got 0"
//! assert fail_type_check "Wrong type argument count: Expected 1, but got 0"
//! assert fail_type_check "Wrong type argument count: Expected 1, but got 0"
//! assert fail_type_check "Wrong type argument count: Expected 1, but got 0"
//! assert fail_type_check "Expected std::vec::Vec<???>, but got std::vec::Vec<pbc_zk::Sbi32>"
//! assert fail_type_check "Expected std::vec::Vec<???>, but got std::vec::Vec<pbc_zk::Sbi32>"
//! assert fail_type_check "Expected std::vec::Vec<???>, but got std::vec::Vec<pbc_zk::Sbi32>"
//! assert fail_type_check "Expected std::vec::Vec<???>, but got std::vec::Vec<pbc_zk::Sbi32>"
//! assert fail_type_check "Expected std::vec::Vec<???>, but got std::vec::Vec<pbc_zk::Sbi32>"
//! assert fail_type_check "Expected std::vec::Vec<???>, but got std::vec::Vec<pbc_zk::Sbi32>"
use pbc_zk::*;
use std::vec::Vec;

pub fn test(x: Sbi32) -> u32 {
    let mut vec: Vec<Sbi32> = Vec::new::<Sbi32>();
    Vec::push(vec, x);
    Vec::push(vec, Sbi32::from(2));
    Vec::push(vec, x);
    Vec::push(vec, x);
    Vec::push(vec, Sbi32::from(5));

    // Length
    Vec::len(vec)
}
