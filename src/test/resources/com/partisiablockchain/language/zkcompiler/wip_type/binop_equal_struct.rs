//! assert fail_compile "Named type comparison not supported"
// assert eval(1,2,3,4) => 0
// assert eval(1,2,1,2) => 1
// assert eval(1,3,1,2) => 0
// assert eval(9,3,9,2) => 0
// assert eval(9,9,9,9) => 1
use pbc_zk::*;

struct Id {
    id: Sbi32,
}

pub fn double_identity(v1: Id, v2: Id, v3: Id, v4: Id) -> Sbu1 {
    (v1, v2) == (v3, v4)
}
