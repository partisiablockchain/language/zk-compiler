//! assert fail_type_check "Unsupported expression: Indirect function call"
use pbc_zk::*;

pub fn circuit_1(x: Sbi32) -> Sbi32 {
    (if x == Sbi32::from(0) {
        Sbi32::from
    } else {
        Sbi32::from
    })(1)
}
