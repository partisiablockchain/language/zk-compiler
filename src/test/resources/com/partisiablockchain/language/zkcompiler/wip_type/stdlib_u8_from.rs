//! assert fail_type_check "Unknown function u8::from"

fn u8_constants(x: u32) -> u8 {
    u8::from(x == x)
}
